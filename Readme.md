## How can I get the source code?

If you just want to explore the source code, you can download it from [this page](https://bitbucket.org/2xmax/barsic/downloads). By the way, it is STRONGLY not recommended to modify this archive 
because it will be extremely hard to merge and maintain different copies from various team members (check out more about [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself),
[SSoT](https://en.wikipedia.org/wiki/Single_Source_of_Truth), [Big Bang](http://users.csc.calpoly.edu/~jdalbey/206/Lectures/IntegrationStrategies.html)).

If you are going to modify the source code, open a command line/terminal and follow the instructions:

1. Install a git client:
```
Windows:
@powershell -NoProfile -ExecutionPolicy unrestricted -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin
cinst -y git
-------
Linux/OS X:
sudo apt-get install -y git or sudo yum install -y git
```

2. Clone ("download", "check out") the repository
```
git clone https://kirillzubov@bitbucket.org/kirillzubov/barsic.git
```

