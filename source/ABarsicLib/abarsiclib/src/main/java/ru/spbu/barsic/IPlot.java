package ru.spbu.barsic;

public interface IPlot {

    void setTitle(String value);

    void setFunction(String expr);
}
