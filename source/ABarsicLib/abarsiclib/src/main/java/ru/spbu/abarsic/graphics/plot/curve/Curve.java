/*
 * Curve.java
 */
package ru.spbu.abarsic.graphics.plot.curve;

/**
 *
 * @author Max
 */

/**
 * Porting to Android performed by Martynyuk
 */

import ru.spbu.abarsic.graphics.awtandroid.BasicStroke;

import android.graphics.Canvas;
import android.graphics.Paint;

import ru.spbu.abarsic.graphics.awtandroid.geom.*;
import ru.spbu.abarsic.graphics.plot.points.PointListAr;

/**
 * Curve class
 */
public class Curve {

    /**
     * Points set of the curve
     */
    public PointListAr points;
    private String title;
    private CurveStyle style = new CurveStyle();

    /**
     * Sets the style of the curve
     * @param value 
     */
    public void setStyle(CurveStyle value) {
        style = value;
    }

    /**
     * Returns the style of the curve
     * @return the style of the curve
     */
    public CurveStyle getStyle() {
        return style;
    }

    /**
     * 
     */
    public Curve() {
        this.points = new PointListAr();
    }

    /**
     * 
     * @param x
     * @param y
     */
    public Curve(double[] x, double[] y) {
        this.points = new PointListAr(x, y);
    }

    /**
     * Returns the alias of the curve
     * @return the alias of the curve
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the alias of the curve
     * @param title 
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Calculates scale factors(stretch multiplies) by x and y
     * @param dataArea
     * @param renderArea
     * @return x-coordinate of the returned Point2D instance:scaleFaxcor by x, y-coordinate:scaleFaxcor by x
     */
    private Point2D calcScaleFactors(Rectangle2D dataArea, Rectangle2D renderArea) {
        double scaleFactorX = (dataArea.getWidth() != 0) ? (renderArea.getWidth() / dataArea.getWidth()) : 1000000000;//double.MaxValue - 10f;
        double scaleFactorY = (dataArea.getHeight() != 0) ? (renderArea.getHeight() / dataArea.getHeight()) : 1000000000;
        return new Point2D(scaleFactorX, scaleFactorY);
    }

    /**
     * Main Paint event
     *
     * @param g Graphics context
     * @param dataArea area of the values that we want to display
     * @param renderArea visualization area       
     */
    public void draw(Canvas g, Paint p, Rectangle2D dataArea, Rectangle2D renderArea) {
        g.save();
        
        Point2D scaleFactor = calcScaleFactors(dataArea, renderArea);
        g.clipRect((int) renderArea.getMinX(), (int) renderArea.getMinY(), (int) renderArea.getMaxX(), (int) renderArea.getMaxY());
        double dx = renderArea.getMinX() - dataArea.getMinX() * scaleFactor.getX();
        double dy = renderArea.getMaxY() + dataArea.getMinY() * scaleFactor.getY();
        g.translate((float) dx, (float) dy);
        if (style.getShowLines()) {
            drawLines(g, p, dataArea, scaleFactor.getX(), scaleFactor.getY());
        }
        
        if (style.getShowPoints()) {
            drawPoints(g, p, dataArea, scaleFactor.getX(), scaleFactor.getY());
        }

        g.restore();
    }

    /**
     * Draws the lines of the curve
     * @param g Graphics context
     * @param dataArea area of the values
     * @param scaleFactorX stretch multiplier by x
     * @param scaleFactorY stretch multiplier by y
     */
    private void drawLines(Canvas g, Paint p, Rectangle2D dataArea, double scaleFactorX, double scaleFactorY) {
        
        g.save();

        int countOfCurvesPoints = this.points.getSize();
        int saveColor = p.getColor();
        p.setColor(this.style.getLineColor());
        BasicStroke saveStroke = BasicStroke.get(p);

        switch (style.getLineStyle()) {
            case SOLID:
                new BasicStroke((float) style.getLineWidth()).set(p);
                break;
            case DASH:
                float[] dash = {10.0f};
                new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f).set(p);
                break;
            case DASHDOT:
                float[] dashdot = {10.0f, 9.0f, 2.0f, 9.0f};
                new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dashdot, 0.0f).set(p);
                break;
            case DOT:
                float[] dot = {2.0f};
                new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dot, 0.0f).set(p);
                break;
        }

        if (countOfCurvesPoints > 1) {
            for (int i = 0; i < countOfCurvesPoints - 1; i++) {
                new Line2D(this.points.x[i]*scaleFactorX,
                          -this.points.y[i]*scaleFactorY,
                           this.points.x[i + 1]*scaleFactorX,
                          -this.points.y[i + 1]*scaleFactorY).draw(g, p);
            }
        }

        p.setColor(saveColor);
        saveStroke.set(p);
        g.restore();
    }

    /**
     * Draws the points of the curve
     * @param g Graphics context
     * @param dataArea area of the values
     * @param scaleFactorX stretch multiplier by x
     * @param scaleFactorY stretch multiplier by y
     */
    private void drawPoints(Canvas g, Paint p, Rectangle2D dataArea, double scaleFactorX, double scaleFactorY) {
        int N_points = points.getSize();
        int save = p.getColor();

        if (N_points > 0) {
            double size = style.getPointsSize();
            p.setColor(style.getPointsColor());
            switch (style.getPointStyle()) {
                case CIRCLE:
                    for (int i = 0; i < N_points; i++) // if (inBox[i])
                    {
                        new Ellipse2D(
                                points.x[i]*scaleFactorX - size,
                               -points.y[i]*scaleFactorY - size,
                                2.0*size, 2.0*size).draw(g, p); //fill
                    }
                    break;

                case RECTANGLE:
                    for (int i = 0; i < N_points; i++) // if (inBox[i])
                    {
                        new Rectangle2D(
                                points.x[i]*scaleFactorX - size,
                               -points.y[i]*scaleFactorY - size,
                                2.0*size, 2.0*size).draw(g, p); //fill
                    }
                    break;
            }
        }
        p.setColor(save);

    }

    /**
     *
     * @return minimal data area that contain all data points
     */
    public Rectangle2D inscribeToRectangle() {
        if (points.getSize() > 1) {
            double _ymax = -Double.MAX_VALUE + 10.0;
            double _ymin = Double.MAX_VALUE - 10.0;
            double _xmax = -Double.MAX_VALUE + 10.0;
            double _xmin = Double.MAX_VALUE - 10.0;
            int len = points.getSize();

            for (int i = 0; i < len; i++) {
                double y = points.y[i];
                if (y > _ymax) {
                    _ymax = y;
                }
                if (y < _ymin) {
                    _ymin = y;
                }
            }

            for (int i = 0; i < len; i++) {
                double x = points.x[i];
                if (x > _xmax) {
                    _xmax = x;
                }
                if (x < _xmin) {
                    _xmin = x;
                }
            }


            return new Rectangle2D(_xmin-Math.abs(_xmin/10), _ymin-Math.abs(_ymin/10), _xmax - _xmin+2*Math.abs(_xmin/10), _ymax - _ymin+2*Math.abs(_ymin/10));
        }

        /*if (points.getSize() == 1) {
            return new Rectangle2D(
                    points.x[0] - (Plot.ABarsicGraph.DEFAULT_DATA_AREA.getWidth() / 2),
                    points.y[0] - (Plot.ABarsicGraph.DEFAULT_DATA_AREA.getHeight() / 2),
                    Plot.ABarsicGraph.DEFAULT_DATA_AREA.getWidth(),
                    Plot.ABarsicGraph.DEFAULT_DATA_AREA.getHeight());
        }*/

        //return Plot.ABarsicGraph.DEFAULT_DATA_AREA;
        return new Rectangle2D(0.0, 10.0, 10.0, 10.0);
    }

    /**
     * 
     * @param g 
     * @param scaleFactor
     * @author Anita
     */
    public void drawNewPartsOfCurve(Canvas g, Paint p, Point2D scaleFactor) {
        if (style.getShowLines()) {
            drawPartsOfLine(g, p, scaleFactor);
        }
        if (style.getShowPoints()) {
            drawNewPoints(g, p, scaleFactor);
        }
    }

    /**
     * 
     * @param g
     * @param scaleFactor  
     * @author Anita
     */
    public void drawNewPoints(Canvas g, Paint p, Point2D scaleFactor) {
        g.save();
        g.scale(1, -1);
        int save = p.getColor();
        p.setColor(style.getLineColor());
        double sizex = style.getPointsSize();
        double sizey = style.getPointsSize();
        double width = 2 * sizex;
        double height = 2 * sizey;
        p.setColor(style.getPointsColor());
        if (points.numN == 0) {
            switch (style.getPointStyle()) {
                case CIRCLE:
                    new Ellipse2D(
                            points.x[0] * scaleFactor.getX() - sizex,
                            points.y[0] * scaleFactor.getY() - sizey,
                            width, height).draw(g, p); //fill
                    break;
                case RECTANGLE:
                    new Rectangle2D(
                            points.x[0] * scaleFactor.getX() - sizex,
                            points.y[0] * scaleFactor.getY() - sizey,
                            width, height).draw(g, p); //fill
                    break;
            }
        }
        if (points.numN > 0) {

            
            switch (style.getPointStyle()) {
                case CIRCLE:
                    for (int i = 0; i < points.numN; i++) // if (inBox[i])
                    {
                        new Ellipse2D(
                                points.x[i] * scaleFactor.getX() - sizex,
                                points.y[i] * scaleFactor.getY() - sizey,
                                width, height).draw(g, p); //fill
                    }
                    break;

                case RECTANGLE:
                    for (int i = 0; i < points.numN; i++) // if (inBox[i])
                    {
                        new Rectangle2D(
                                points.x[i] * scaleFactor.getX() - sizex,
                                points.y[i] * scaleFactor.getY() - sizey,
                                width, height).draw(g, p); //fill
                    }
                    break;
            }
        }
        g.restore();
        p.setColor(save);
    }

    /**
     * draws lines from last points to new points
     * @param g
     * @param scaleFactor 
     * @author Anita
     **/
    public void drawPartsOfLine(Canvas g, Paint p, Point2D scaleFactor) {
        g.save();
        g.scale(1, -1);
        int saveColor = p.getColor();
        p.setColor(style.getLineColor());
        BasicStroke saveStroke = BasicStroke.get(p);

        switch (style.getLineStyle()) {
            case SOLID:
                new BasicStroke((float) style.getLineWidth()).set(p);
                break;
            case DASH:
                float[] dash = {10.0f};
                new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f).set(p);
                break;
            case DASHDOT:
                float[] dashdot = {10.0f, 9.0f, 2.0f, 9.0f};
                new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dashdot, 0.0f).set(p);
                break;
            case DOT:
                float[] dot = {2.0f};
                new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dot, 0.0f).set(p);
                break;
        }


        if (this.points.numN == 0) {

            double x1 = this.points.x[0] * scaleFactor.getX();
            double y1 = this.points.y[0] * scaleFactor.getY();
            double x2 = this.points.x[0] * scaleFactor.getX();
            double y2 = this.points.y[0] * scaleFactor.getY();

            new Line2D(x1, y1, x2, y2).draw(g, p);

        } else {
            //for (int j = 0; j < this.points.numN; j++) {
            for (int j = 0; j < this.points.numN - 1; j++) {
                double x1 = this.points.x[j] * scaleFactor.getX();
                double y1 = this.points.y[j] * scaleFactor.getY();
                double x2 = this.points.x[j + 1] * scaleFactor.getX();
                double y2 = this.points.y[j + 1] * scaleFactor.getY();
                new Line2D(x1, y1, x2, y2).draw(g, p);
            }
        }

        g.restore();
        p.setColor(saveColor);
        saveStroke.set(p);
    }
}
