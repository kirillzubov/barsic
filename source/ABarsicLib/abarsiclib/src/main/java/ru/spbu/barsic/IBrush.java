package ru.spbu.barsic;

public interface IBrush {
    
    public String color();
    
    public void color(String value);
    
    public String style();
    
    public void style(String style);
    
}
