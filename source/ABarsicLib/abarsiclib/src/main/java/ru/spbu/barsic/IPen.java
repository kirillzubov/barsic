package ru.spbu.barsic;

public interface IPen {
    
    public String color();
    
    public void color(String value);
    
    public double width();
    
    public void width(double value);
    
}
