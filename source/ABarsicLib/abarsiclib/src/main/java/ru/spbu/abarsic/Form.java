package ru.spbu.abarsic;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import ru.spbu.barsic.IElement;
import ru.spbu.barsic.IForm;
import ru.spbu.barsic.ISubwindowManager;
//import ru.barsic.model.FormsActivity;
//import ru.barsic.model.R;

public class Form implements IForm {

    private ViewGroup mLayout;
    private ViewGroup getLayout(){
        if(mLayout !=  null){
            return mLayout;
        }

        Activity activity = null;
        //Activity activity = FormsActivity.instance;
        //activity.setContentView(R.layout.form);
        ViewGroup layout = null; //(ViewGroup) activity.findViewById(R.id.form);
        mLayout = layout;
        return layout;
    }

    @Override
    public void addElement(IElement element) {
        getLayout().addView((View) element);
    }


    public void showForm() {

    }

    @Override
    public void setTitle(String value) {

    }

    @Override
    public int width() {
        return 0;
    }

    @Override
    public void width(int value) {

    }

    @Override
    public int height() {
        return 0;
    }

    @Override
    public void height(int value) {

    }

    @Override
    public void setAlign(String value) {

    }

    @Override
    public void setOnHelp(final Runnable eventHandler) {

    }

    @Override
    public void bindWithCommonObjects(IModel model) {

    }

    @Override
    public void setName(String value) {

    }

    @Override
    public void setLeftPos(int value) {

    }

    @Override
    public void setTopPos(int value) {

    }

    @Override
    public void setLeft(int value) {

    }

    @Override
    public void setTop(int value) {

    }

    @Override
    public void setClientWidth(int value) {

    }

    @Override
    public void setHint(String value) {

    }

    @Override
    public void setIsEnabled(boolean value) {

    }

    @Override
    public void setIsVisible(boolean value) {

    }

    @Override
    public void setClientHeight(int value) {

    }

    @Override
    public void setBackColor(String value) {

    }

    @Override
    public void setIsResizeable(Boolean value) {

    }

    @Override
    public void setFont(String value) {

    }

    @Override
    public void setIsBorderCloseIcon(boolean value) {

    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {

    }

    @Override
    public ISubwindowManager getElementById(String id) {
        return null;
    }
}