package ru.spbu.abarsic.graphics.graph;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import static ru.spbu.abarsic.graphics.CartTransform.*;

/**
 * Created by Мартынюк on 24.02.2016.
 */
public final class Arc extends GraphicPrimitive {

    double w;
    double h;
    double startAngle;
    double endAngle;

    RectF boundRect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);

    public Arc(double xCenter, double yCenter, double width, double height,
               double startAngle, double endAngle, int fillColor, int strokeColor, double strokeWidth) {
        super(xCenter, yCenter, fillColor, strokeColor, strokeWidth);
        this.w = width;
        this.h = height;
        this.startAngle = startAngle;
        this.endAngle = endAngle;
    }

    @Override
    public void draw(Canvas c, Paint fillPaint, Paint strokePaint, double[] coeffs) {
        boundRect.set((float)(coeffs[AX]*(x0-w*0.5)+coeffs[BX]), (float)(coeffs[AY]*(y0+h*0.5)+coeffs[BY]),
                      (float)(coeffs[AX]*(x0+w*0.5)+coeffs[BX]), (float)(coeffs[AY]*(y0-h*0.5)+coeffs[BY]));
        setStrokePaint(strokePaint, coeffs[AX]);
        c.drawArc(boundRect, (float)(startAngle), (float)(endAngle-startAngle), false, strokePaint);
    }
}
