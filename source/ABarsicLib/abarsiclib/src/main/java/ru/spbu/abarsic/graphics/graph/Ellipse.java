package ru.spbu.abarsic.graphics.graph;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import static ru.spbu.abarsic.graphics.CartTransform.*;

/**
 * Created by Мартынюк on 17.04.2015.
 */
public final class Ellipse extends GraphicPrimitive {

    double w;
    double h;

    private RectF boundRect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);

    public Ellipse(double xCenter, double yCenter, double width, double height, int fillColor, int strokeColor, double strokeWidth) {
        super(xCenter, yCenter, fillColor, strokeColor, strokeWidth);
        this.w = width;
        this.h = height;
    }

    @Override
    public void draw(Canvas c, Paint fillPaint, Paint strokePaint, double[] coeffs) {
        boundRect.set((float)(coeffs[AX]*(x0-w*0.5)+coeffs[BX]), (float)(coeffs[AY]*(y0+h*0.5)+coeffs[BY]),
                      (float)(coeffs[AX]*(x0+w*0.5)+coeffs[BX]), (float)(coeffs[AY]*(y0-h*0.5)+coeffs[BY]));
        setFillPaint(fillPaint);
        c.drawOval(boundRect, fillPaint);
        setStrokePaint(strokePaint, coeffs[AX]);
        c.drawOval(boundRect, strokePaint);
    }
}
