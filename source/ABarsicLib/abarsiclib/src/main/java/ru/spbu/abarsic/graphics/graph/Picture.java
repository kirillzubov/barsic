package ru.spbu.abarsic.graphics.graph;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Мартынюк on 24.02.2016.
 */
public final class Picture extends GraphicPrimitive {

    public Picture(double xCenter, double yCenter, int fillColor, int strokeColor, double strokeWidth) {
        super(xCenter, yCenter, fillColor, strokeColor, strokeWidth);
    }

    @Override
    public void draw(Canvas c, Paint fillPaint, Paint strokePaint, double[] coeffs) {

    }
}
