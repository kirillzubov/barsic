package ru.spbu.abarsic;

import java.util.concurrent.locks.ReentrantLock;

import ru.spbu.barsic.IApplication;
import ru.spbu.barsic.IElement;
import ru.spbu.barsic.IModule;

/**
 * Created by Мартынюк on 23.04.2016.
 */
public final class Application implements IApplication {

    ReentrantLock mMainLock;

    Application(Model model) {
        super();
        mMainLock = model.mMainLock;
    }

    @Override
    public IElement[] elements() {
        return new IElement[0];
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getPath() {
        return null;
    }

    @Override
    public int maxArrayLength() {
        return 106;
    }

    @Override
    public void maximize() {

    }

    @Override
    public int maxRecoursionDepth() {
        return 0;
    }

    @Override
    public void minimize() {

    }

    @Override
    public IModule[] modules() {
        return new IModule[0];
    }

    @Override
    public String[] parameters() {
        return new String[0];
    }

    @Override
    public void processMessages() {

    }

    @Override
    public void quit() {

    }

    @Override
    public void sleep(long time) {
        try {
            if (mMainLock.isHeldByCurrentThread()) {
                mMainLock.unlock();
            }
            if (time > 0) {
                Thread.sleep(time);
            } else {
                Thread.sleep(1);
            }
            mMainLock.lock();
        } catch (InterruptedException e) {}
    }

    @Override
    public boolean watchesVisible() {
        return false;
    }

}
