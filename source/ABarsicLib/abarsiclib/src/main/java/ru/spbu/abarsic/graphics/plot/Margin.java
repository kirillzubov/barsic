/*
 * Margin.java
 */
package ru.spbu.abarsic.graphics.plot;

/**
 *
 * @author Max
 */
public class Margin {
    /**
     * 
     * @param left
     * @param top
     * @param right
     * @param bottom
     */
    public Margin(int left, int top, int right, int bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    /**
     * 
     * @param left
     * @param right
     */
    public Margin(int left, int right) {
        this.left = left;
        this.right = right;
    }

    private int left;
    /**
     * 
     * @return
     */
    public int getLeft() {
        return this.left;
    }

    /**
     * 
     * @param value
     */
    public void setLeft(int value) {
        this.left = value;
    }

    private int top;
    /**
     * 
     * @return
     */
    public int getTop() {
        return this.top;
    }
    /**
     * 
     * @param value
     */
    public void setTop(int value) {
        this.top = value;
    }

    private int right;
    /**
     * 
     * @return
     */
    public int getRight() {
        return this.right;
    }

    /**
     * 
     * @param value
     */
    public void setRight(int value) {
        this.right = value;
    }
    
    private int bottom;
    /**
     * 
     * @return
     */
    public int getBottom() {
        return this.bottom;
    }

    /**
     * 
     * @param value
     */
    public void setBottom(int value) {
        this.bottom = value;
    }
    
    
}
