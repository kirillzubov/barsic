package ru.spbu.abarsic;

import android.content.Context;
import android.os.Looper;
import android.util.AttributeSet;
import android.widget.*;

//import ru.barsic.model.FormsActivity;
import ru.spbu.barsic.ITextField;


/**
 * Created by nik on 18.04.2015.
 */
public class TextEdit extends EditText implements ITextField {

    int width1=0,height1=0,x1=0,y1=0;

    /*public TextEdit(){
        //this(FormsActivity.instance);
    }*/

    public TextEdit(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public int width=100, height=100, x=300, y=500;


    public void reload(){

    }

    public void executeJavascript(String code){

    }

    public void setName(String value){

    }

    @Override
    public void setLeftPos(int value) {

    }

    @Override
    public void setTopPos(int value) {

    }

    public void setX(int value){
        x1=value;
        AbsoluteLayout.LayoutParams params =
                new AbsoluteLayout.LayoutParams(width1, height1, x1, y1);
        super.setLayoutParams(params);

    }

    public void setY(int value){
        y1=value;
        AbsoluteLayout.LayoutParams params =
                new AbsoluteLayout.LayoutParams(width1, height1, x1, y1);
        super.setLayoutParams(params);
    }

    @Override
    public int width() {
        return getWidth();
    }

    @Override
    public void width(int value){
        height1=value;
        AbsoluteLayout.LayoutParams parampampam =
                new AbsoluteLayout.LayoutParams(width1, height1, x1, y1);
        super.setLayoutParams(parampampam);
    }

    @Override
    public int height() {
        return getHeight();
    }

    public void height(int value){
        width1=value;
        AbsoluteLayout.LayoutParams params =
                new AbsoluteLayout.LayoutParams(width1, height1, x1, y1);
        super.setLayoutParams(params);
    }

    @Override
    public void setHint(String value){
        int hintColor = android.R.color.holo_blue_dark;
        super.setHintTextColor(getResources().getColor(hintColor));
        super.setHint(value);
    }

    public void setIsEnabled(boolean value){
        setEnabled(value);
    }

    public void setIsVisible(boolean value){
        if (value==true) {
            this.setVisibility(this.VISIBLE);
        }
        else
            this.setVisibility(this.INVISIBLE);
    }


    public void setAlign(String value){
        switch(value) {
            case "left":
                AbsoluteLayout.LayoutParams parampampam1 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam1);
                break;
            case "right":
                AbsoluteLayout.LayoutParams parampampam2 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam2);
                break;
            case "top":
                AbsoluteLayout.LayoutParams parampampam3 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam3);
                break;
            case "bottom":
                AbsoluteLayout.LayoutParams parampampam4 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam4);
                break;
            default:
                AbsoluteLayout.LayoutParams parampampam5 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam5);
                break;
        }
    }

    @Override
    public void setOnHelp(final Runnable eventHandler) {

    }

    @Override
    public void bindWithCommonObjects(IModel model) {

    }
    public int textSize1;
    @Override
    public void setTextSize(int value){
        textSize1=value;
    }

    public void setBackColor(){
        //int color = android.R.color.value;
        int color = android.R.color.holo_red_light;
        setBackgroundColor(getResources().getColor(color));
    }

    @Override
    public void setText(final String value) {
        final TextEdit textEdit = this;
        if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
            post(new Runnable() {
                @Override
                public void run() {
                    textEdit.setTextSize(textSize1);
                    textEdit.setText(value);
                }
            });
        } else {
            if (textSize1!=0) {
                super.setTextSize(textSize1);
            }
            super.setText(value);
            int backgroundcolor = android.R.color.background_dark;
            int textcolor = android.R.color.background_light;
            super.setBackgroundColor(getResources().getColor(backgroundcolor));
            super.setTextColor(getResources().getColor(textcolor));
        }
    }

    @Override
    public void setIsSourceCode(boolean value) {

    }

    @Override
    public void setIsParentBackColor(boolean value) {

    }

    @Override
    public void setIsReadOnly(boolean value) {

    }

    @Override
    public void setIsParentFont(boolean value) {

    }

    @Override
    public void setAssociatedVariable(String value) {

    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {

    }
}
