package ru.spbu.abarsic.graphics.graph;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import static ru.spbu.abarsic.graphics.CartTransform.*;

/**
 * Created by Мартынюк on 17.04.2015.
 */
public final class Polygon extends GraphicPrimitive {

    double[] xyPoints; //odd elements - x, even elements - y
    Path path = new Path();

    public Polygon(double[] xyPoints, int fillColor, int strokeColor, double strokeWidth) {
        super(0.0, 0.0, fillColor, strokeColor, strokeWidth); //TODO add determining of polygon center
        this.xyPoints = xyPoints;
    }

    @Override
    public void draw(Canvas c, Paint fillPaint, Paint strokePaint, double[] coeffs) {
        path.moveTo((float)(coeffs[AX]*xyPoints[0]+coeffs[BX]), (float)(coeffs[AY]*xyPoints[1]+coeffs[BY]));
        for (int i=2; i<xyPoints.length-1; i+=2) {
            path.lineTo((float)(coeffs[AX]*xyPoints[i]+coeffs[BX]), (float)(coeffs[AY]*xyPoints[i+1]+coeffs[BY]));
        }
        path.close();

        setFillPaint(fillPaint);
        c.drawPath(path, fillPaint);
        setStrokePaint(strokePaint, coeffs[AX]);
        c.drawPath(path, strokePaint);
    }
}
