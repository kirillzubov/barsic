package ru.spbu.abarsic.graphics;

import android.os.Looper;

import static ru.spbu.abarsic.graphics.CartTransform.*;

import ru.spbu.barsic.ISubwindowManager;

import static java.lang.Math.round;

/**
 * Created by Мартынюк on 09.09.2015.
 */

public final class SubwindowManager implements ISubwindowManager {

    Subwindow mSubwindowView;

    @Override
    public double xMin() {
        return mSubwindowView.mXMin;
    }

    @Override
    public double xMax() {
        return mSubwindowView.mXMax;
    }

    @Override
    public double yMin() {
        return mSubwindowView.mYMin;
    }

    @Override
    public double yMax() {
        return mSubwindowView.mYMax;
    }

    @Override
    public int pixels(char coordinate, double value) {
        switch (coordinate) {
            case 'x':
                return (int)round(mSubwindowView.mCoeffs[AX]*value + mSubwindowView.mCoeffs[BX]);
            case 'y':
                return (int)round(mSubwindowView.mCoeffs[AY]*value + mSubwindowView.mCoeffs[BY]);
            default:
                return 0;
        }
    }

    @Override
    public double x(int pixels) {
        return (pixels - mSubwindowView.mCoeffs[BX])/ mSubwindowView.mCoeffs[AX];
    }

    @Override
    public double y(int pixels) {
        return (pixels - mSubwindowView.mCoeffs[BY])/ mSubwindowView.mCoeffs[AY];
    }

    @Override
    public void clipping(String value) {

    }

    @Override
    public void context(String value) {
        if (mSubwindowView == null) return;
        if (value.equals("objects")) {
            mSubwindowView.mGraph.mContext = Graph.CONTEXT_OBJECTS;
        } else if (value.equals("canvas")) {
            mSubwindowView.mGraph.mContext = Graph.CONTEXT_CANVAS;
        } else {
            throw new IllegalArgumentException("Context name must be \"objects\" or \"canvas\"");
        }
    }

    @Override
    public void isParentBackColor(boolean value) {

    }

    @Override
    public void clear() {
        mSubwindowView.mPlot.clear();
        mSubwindowView.mGraph.canvas.clear();
    }

    @Override
    public void update() {
        mSubwindowView.mUpdating = true;
        //mPlot.calculateParams(); //TODO
        mSubwindowView.update();
    }

}
