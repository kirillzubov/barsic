/*
 * Figure2D.java
 */

package ru.spbu.abarsic.graphics.awtandroid.geom;

/**
 * @author Martynyuk
 * */

import android.graphics.Canvas;
import android.graphics.Paint;

public abstract class Figure2D {
	
	Figure2D() {
		super();
	}
	
	public abstract void draw(Canvas g, Paint p);

}
