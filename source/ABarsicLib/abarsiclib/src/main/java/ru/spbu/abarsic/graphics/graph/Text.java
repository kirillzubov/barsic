package ru.spbu.abarsic.graphics.graph;

import android.graphics.Canvas;
import android.graphics.Paint;

import static ru.spbu.abarsic.graphics.CartTransform.*;

/**
 * Created by Мартынюк on 10.10.2015.
 */
public final class Text extends GraphicPrimitive {

    String text;
    double xLeft;
    double yTop;

    public Text(String text, double xLeft, double yTop, int fillColor, int strokeColor, double strokeWidth) {
        super(0.0, 0.0, fillColor, strokeColor, strokeWidth);
        this.text = text;
        this.xLeft = xLeft;
        this.yTop = yTop;
    }

    @Override
    public void draw(Canvas c, Paint fillPaint, Paint strokePaint, double[] coeffs) {
        float xs = (float)(coeffs[AX]*xLeft + coeffs[BX]);
        float ys = (float)(coeffs[AY]*yTop + coeffs[BY]) + fillPaint.getTextSize();
        setFillPaint(fillPaint);
        c.drawText(text, xs, ys, fillPaint);
        setStrokePaint(strokePaint, coeffs[AX]);
        c.drawText(text, xs, ys, strokePaint);
    }
}
