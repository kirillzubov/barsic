package ru.spbu.barsic;

/**
 * Created by Мартынюк on 23.04.2016.
 */
public interface ITimer {

    void clearTimeOut();

    void reset();

    void setTimeOut(double timeOut);

    double time();

}
