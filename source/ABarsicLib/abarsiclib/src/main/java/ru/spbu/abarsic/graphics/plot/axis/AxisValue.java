/*
 * AxisValue.java
 */

package ru.spbu.abarsic.graphics.plot.axis;

/**
 * Porting to Android performed by Martynyuk
 */

import android.graphics.Color;

import android.graphics.Canvas;
import android.graphics.Paint;
import ru.spbu.abarsic.graphics.awtandroid.geom.Point2D;
import ru.spbu.abarsic.graphics.awtandroid.geom.Rectangle2D;

import ru.spbu.abarsic.graphics.awtandroid.Font;


/**
 * 
 * @author Max
 */
public class AxisValue {
    
    /**
     * 
     */
    public AxisValue() {
        
    }

    /**
     * 
     * @param value
     * @param deg
     */
    public AxisValue(float value, int deg) {
        this.value = String.format("{0:F" + deg + "}", value);
    }

    /**
     * 
     * @param value
     */
    public AxisValue(String value) {
        this.value = value;
    }

    /**
     * 
     */
    public enum Orientation {

        /**
         * 
         */
        Horizontal,
        /**
         * 
         */
        Vertical,
        /**
         * 
         */
        VerticalAlt,
        /**
         * 
         */
        HorizontalAlt
    }

    /**
     * 
     */
    public enum AlignW {

        /**
         * 
         */
        Left,
        /**
         * 
         */
        Center,
        /**
         * 
         */
        Right
    }

    /**
     * 
     */
    public enum AlignH {

        /**
         * 
         */
        Top,
        /**
         * 
         */
        Center,
        /**
         * 
         */
        Bottom,
        /**
         * 
         */
        Baseline
    }

    /**
     * 
     */
    public AlignW alignW = AlignW.Center;
    /**
     * 
     */
    public AlignH alignH = AlignH.Bottom;
    //
    private Orientation orientation = Orientation.Horizontal;

    /**
     * Returns the orientation (angle) of the value
     * @return the orientation (angle) of the value
     */
    public Orientation getOrientation() {
        return this.orientation;
    }

    /**
     * Sets the orientation (angle) of the value. The default value is Horizontal
     * @param value the orientation of the value
     */
    public void setOrientation(Orientation value) {
        this.orientation = value;
    }

    private String value = "";

    /**
     * Returns the value that we want to display
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the value that we want to display
     * @param value 
     */
    public void setValue(String value) {
        this.value = value;
    }
    
    private Font font = new Font(Font.MONOSPACED, Font.PLAIN, 15);

    /**
     * Returns the font of the value
     * @return the font of the value
     */
    public Font getFont() {
        return this.font;
    }

    /**
     * Sets the font of the value
     * @param value the font of the value
     */
    public void setFont(Font value) {
        this.font = value;
    }

    private int foreground = Color.BLACK;

    /**
     * Returns the font color of the value
     * @return the font color of the value
     */
    public int getForeground() {
        return this.foreground;
    }

    /**
     * Sets the font color of the value
     * @param value the font color of the value
     */
    public void setForeground(int value) {
        this.foreground = value;
    }

    /**
     * Returns the render width of the value shape
     * @return the width of the value shape
     */
    public double getWidth(Paint p) {
        return p.measureText(value);
    }

    /**
     * Returns the render height of the value shape
     * @return the height of the value shape
     */
    public double getHeight(Paint p) {
        return p.getTextSize();
    }

    /** Returns the render bound rectangle of the value shape
     * @return the bound of the value shape
     */
    public Rectangle2D getBounds(Paint p)
    {
        return new Rectangle2D(0.0f, 0.0f, p.measureText(value), p.getTextSize()*0.75);
    }

    /**
     * Draws the value
     * @param g Graphics context
     * @param pos position of the value(also you can set the alignment using alignH and alighW properties)
     */
    public void draw(Canvas g, Paint p, Point2D pos) {
        font.set(p);
        p.setColor(this.foreground);

        String val = this.value;

        double splitX = 0f;
        double splitY = 0f;

        Rectangle2D rect = this.getBounds(p);

        switch (orientation) {

            case Horizontal:

                switch (alignW) {
                    case Right:
                        splitX = -rect.getMinX();
                        break;
                    case Center:
                        splitX = -rect.getMinX() - rect.getWidth() * 0.5;
                        break;
                    case Left:
                        splitX = -rect.getMinX() - rect.getWidth();
                        break;
                }
                switch (alignH) {
                    case Top:
                        splitY = -rect.getMaxY();
                        break;
                    case Center:
                        splitY = -rect.getMaxY() + rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitY = -rect.getMinY();
                        break;
                    case Baseline:
                        splitY = 0;
                        break;
                }

                g.drawText(val, (float) (pos.getX() + splitX), (float) (pos.getY() + splitY), p);

                break;

            case Vertical:

                switch (alignW) {
                    case Left:
                        splitY = -rect.getMaxX();
                        break;
                    case Center:
                        splitY = -rect.getMaxX() + rect.getWidth() * 0.5;
                        break;
                    case Right:
                        splitY = -rect.getMinX();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitX = -rect.getMaxY();
                        break;
                    case Center:
                        splitX = -rect.getMaxY() + rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitX = -rect.getMinY();
                        break;
                    case Baseline:
                        splitX = 0;
                        break;
                }

                g.rotate(90.0f);
                g.drawText(val, (float) (pos.getY() + splitY), (float) (-pos.getX() + splitX), p);
                g.rotate(-90.0f);

                break;

            case VerticalAlt:

                switch (alignW) {
                    case Left:
                        splitY = rect.getMaxX();
                        break;
                    case Center:
                        splitY = rect.getMaxX() - rect.getWidth() * 0.5;
                        break;
                    case Right:
                        splitY = rect.getMinX();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitX = rect.getMaxY();
                        break;
                    case Center:
                        splitX = rect.getMaxY() - rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitX = rect.getMinY();
                        break;
                    case Baseline:
                        splitX = 0;
                        break;
                }

                g.rotate(-90.0f);
                g.drawText(val, (float) (-pos.getY() - splitY), (float) (pos.getX() - splitX), p);
                g.rotate(90.0f);

                break;

            case HorizontalAlt:

                switch (alignW) {
                    case Left:
                        splitX = -rect.getMinX();
                        break;
                    case Center:
                        splitX = -rect.getMinX() - rect.getWidth() * 0.5;
                        break;
                    case Right:
                        splitX = -rect.getMinX() - rect.getWidth();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitY = -rect.getMaxY();
                        break;
                    case Center:
                        splitY = -rect.getMaxY() + rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitY = -rect.getMinY();
                        break;
                    case Baseline:
                        splitY = 0;
                        break;
                }

                g.rotate(180.0f);
                g.drawText(val, (float) (-pos.getX() + splitX), (float) (-pos.getY() + splitY), p);
                g.rotate(-180.0f);

                break;
        }
    }

    /**
     * Draws the value. Note that all the data about string value and font stores in the as argument
     * @param g Graphics context
     * @param pos pos position of the value(also you can set the alignment using alignH and alighW properties)
     */
    public void draw(Canvas g, Paint p, AxisTitle s, Point2D pos) {

        font.set(p);
        p.setColor(this.foreground);

        //String val=this.value;

        double splitX = 0f;
        double splitY = 0f;

        Rectangle2D rect = new Rectangle2D(0, 0, s.measureWidth(p), p.getTextSize() * 0.75);

        switch (orientation) {
            case Horizontal:

                switch (alignW) {
                    case Right:
                        splitX = -rect.getMinX();
                        break;
                    case Center:
                        splitX = -rect.getMinX() - rect.getWidth() * 0.5;
                        break;
                    case Left:
                        splitX = -rect.getMinX() - rect.getWidth();
                        break;
                }
                switch (alignH) {
                    case Top:
                        splitY = -rect.getMaxY();
                        break;
                    case Center:
                        splitY = -rect.getMaxY() + rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitY = -rect.getMinY();
                        break;
                    case Baseline:
                        splitY = 0;
                        break;
                }

                s.draw(g, p, (float) (pos.getX() + splitX), (float) (pos.getY() + splitY));

                break;

            case Vertical:

                switch (alignW) {
                    case Left:
                        splitY = -rect.getMaxX();
                        break;
                    case Center:
                        splitY = -rect.getMaxX() + rect.getWidth() * 0.5;
                        break;
                    case Right:
                        splitY = -rect.getMinX();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitX = -rect.getMaxY();
                        break;
                    case Center:
                        splitX = -rect.getMaxY() + rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitX = -rect.getMinY();
                        break;
                    case Baseline:
                        splitX = 0;
                        break;
                }

                g.rotate(90.0f);
                s.draw(g, p, (float) (pos.getY() + splitY), (float) (-pos.getX() + splitX));
                g.rotate(-90.0f);

                break;

            case VerticalAlt:

                switch (alignW) {
                    case Left:
                        splitY = rect.getMaxX();
                        break;
                    case Center:
                        splitY = rect.getMaxX() - rect.getWidth() * 0.5;
                        break;
                    case Right:
                        splitY = rect.getMinX();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitX = rect.getMaxY();
                        break;
                    case Center:
                        splitX = rect.getMaxY() - rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitX = rect.getMinY();
                        break;
                    case Baseline:
                        splitX = 0;
                        break;
                }

                g.rotate(-90.0f);
                s.draw(g, p, (float) (-pos.getY() - splitY), (float) (pos.getX() - splitX));
                g.rotate(90.0f);

                break;

            case HorizontalAlt:

                switch (alignW) {
                    case Left:
                        splitX = -rect.getMinX();
                        break;
                    case Center:
                        splitX = -rect.getMinX() - rect.getWidth() * 0.5;
                        break;
                    case Right:
                        splitX = -rect.getMinX() - rect.getWidth();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitY = -rect.getMaxY();
                        break;
                    case Center:
                        splitY = -rect.getMaxY() + rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitY = -rect.getMinY();
                        break;
                    case Baseline:
                        splitY = 0;
                        break;
                }

                g.rotate(180.0f);
                s.draw(g, p, (float) (-pos.getX() + splitX), (float) (-pos.getY() + splitY));
                g.rotate(-180.0f);

                break;
        }
    }

    /**
     * Draws the value if his bounds by x greater than leftlimit and less than rightlimit. The method is convenient for drawing value on axis
     * @param g Graphics context
     * @param pos position of the value(also you can set the alignment using alignH and alighW properties)
     * @param leftlimit the left limit(see the description)
     * @param rightlimit the rightlimit(see the description)
     * @param reverse leftlimit and rightlimit changes
     */
    public void draw(Canvas g, Paint p, Point2D pos, double leftlimit, double rightlimit, boolean reverse) {
        font.set(p);
        p.setColor(this.foreground);

        String val = this.value;

        double splitX = 0.0;
        double splitY = 0.0;

        Rectangle2D rect = getBounds(p);

        switch (orientation) {

            case Horizontal:

                switch (alignW) {
                    case Right:
                        splitX = -rect.getMinX();
                        break;
                    case Center:
                        splitX = -rect.getMinX() - rect.getWidth() * 0.5;
                        break;
                    case Left:
                        splitX = -rect.getMinX() - rect.getWidth();
                        break;
                }
                switch (alignH) {
                    case Top:
                        splitY = -rect.getMaxY();
                        break;
                    case Center:
                        splitY = -rect.getMaxY() + rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitY = -rect.getMinY();
                        break;
                    case Baseline:
                        splitY = 0;
                        break;
                }


                /*if (!reverse) {
                    if (pos.getX() + splitX + rect.getMinX() < leftlimit) {
                        return;
                    }
                    if (pos.getX() + splitX + rect.getMaxX() > rightlimit) {
                        return;
                    }
                } else {
                    if (rightlimit > pos.getX() + splitX + rect.getMinX()) {
                        return;
                    }
                    if (leftlimit < pos.getX() + splitX + rect.getMaxX()) {
                        return;
                    }
                }*/

                g.drawText(val, (float) (pos.getX() + splitX), (float) (pos.getY() + splitY), p);

                break;

            case Vertical:

                switch (alignW) {
                    case Left:
                        splitY = -rect.getMaxX();
                        break;
                    case Center:
                        splitY = -rect.getMaxX() + rect.getWidth() * 0.5;
                        break;
                    case Right:
                        splitY = -rect.getMinX();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitX = -rect.getMaxY();
                        break;
                    case Center:
                        splitX = -rect.getMaxY() + rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitX = -rect.getMinY();
                        break;
                    case Baseline:
                        splitX = 0;
                        break;
                }


                /*if (!reverse) {
                    if (-pos.getX() + splitX + rect.getMaxY() > -leftlimit) {
                        return;
                    }
                    if (-pos.getX() + splitX + rect.getMinY() < -rightlimit) {
                        return;
                    }
                } else {

                    if (-pos.getX() + splitX + rect.getMinY() < -leftlimit) {
                        return;
                    }

                    if (-pos.getX() + splitX + rect.getMaxY() > -rightlimit) {
                        return;
                    }
                }*/

                g.rotate(90.0f);
                g.drawText(val, (float) (pos.getY() + splitY), (float) (-pos.getX() + splitX), p);
                g.rotate(-90.0f);

                break;

            case VerticalAlt:

                switch (alignW) {
                    case Left:
                        splitY = rect.getMaxX();
                        break;
                    case Center:
                        splitY = rect.getMaxX() - rect.getWidth() * 0.5;
                        break;
                    case Right:
                        splitY = rect.getMinX();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitX = rect.getMaxY();
                        break;
                    case Center:
                        splitX = rect.getMaxY() - rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitX = rect.getMinY();
                        break;
                    case Baseline:
                        splitX = 0;
                        break;
                }

                /*if (!reverse) {
                    if (pos.getX() - splitX + rect.getMaxY() > rightlimit) {
                        return;
                    }
                    if (pos.getX() - splitX + rect.getMinY() < leftlimit) {
                        return;
                    }
                } else {
                    if (pos.getX() - splitX + rect.getMinY() < rightlimit) {
                        return;
                    }
                    if (pos.getX() - splitX + rect.getMaxY() > leftlimit) {
                        return;
                    }
                }*/

                g.rotate(-90.0f);
                g.drawText(val, (float) (-pos.getY() - splitY), (float) (pos.getX() + splitX), p);
                g.rotate(90.0f);

                break;

            case HorizontalAlt:

                switch (alignW) {
                    case Left:
                        splitX = -rect.getMinX();
                        break;
                    case Center:
                        splitX = -rect.getMinX() - rect.getWidth() * 0.5;
                        break;
                    case Right:
                        splitX = -rect.getMinX() - rect.getWidth();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitY = -rect.getMaxY();
                        break;
                    case Center:
                        splitY = -rect.getMaxY() + rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitY = -rect.getMinY();
                        break;
                    case Baseline:
                        splitY = 0;
                        break;
                }


                /*if (!reverse) {
                    if (-pos.getX() + splitX + rect.getMaxX() > -leftlimit) {
                        return;
                    }
                    if (-pos.getX() + splitX + rect.getMinX() < -rightlimit) {
                        return;
                    }
                } else {
                    if (-rightlimit < -pos.getX() + splitX + rect.getMaxX()) {
                        return;
                    }
                    if (-leftlimit > -pos.getX() + splitX + rect.getMinX()) {
                        return;
                    }
                }*/

                g.rotate(180.0f);
                g.drawText(val, (float) (-pos.getX() + splitX), (float) (-pos.getY() + splitY), p);
                g.rotate(-180.0f);

                break;
        }
    }

}
