package ru.spbu.abarsic.graphics;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;

import ru.spbu.abarsic.graphics.awtandroid.BasicStroke;
import ru.spbu.abarsic.graphics.awtandroid.geom.Line2D;
import ru.spbu.abarsic.graphics.awtandroid.geom.Point2D;
import ru.spbu.abarsic.graphics.awtandroid.geom.Rectangle2D;
import ru.spbu.abarsic.graphics.plot.DefFunction;
import ru.spbu.abarsic.graphics.plot.Margin;
import ru.spbu.abarsic.graphics.plot.Pane;
import ru.spbu.abarsic.graphics.plot.Range;
import ru.spbu.abarsic.graphics.plot.axis.AxisValue;
import ru.spbu.abarsic.graphics.plot.axis.SciAxis;
import ru.spbu.abarsic.graphics.plot.curve.Curve;
import ru.spbu.abarsic.graphics.plot.legend.Legend;
import ru.spbu.abarsic.graphics.plot.points.Point;
import ru.spbu.abarsic.graphics.plot.points.PointListAr;
import ru.spbu.abarsic.graphics.plot.points.PointStyle;

import static java.lang.Math.abs;
import static java.lang.Math.max;

/**
 * Created by Мартынюк on 15.09.2015.
 */

public final class Plot /*implements IPlot*/ {

    /*
    "axes"
    "bars"
    "bars3D"
    "bounds"
    "couple"
    "erase"
    "function"
    "lines"
    "parametricCurve"
    "parametricTabledCurve"
    "point"
    "points"
    "style"
    "tabledFunction"
    */

    Subwindow mSubwindowView;

    ABarsicPlot mBarsicPlot;
    
    public Axes axes;
    public Bounds bounds;
    public Lines lines;
    public Points points;

    int mCurPointIndex = 0;

    Plot() {
        super();
        mBarsicPlot = new ABarsicPlot();
        axes = new Axes(mBarsicPlot.axisX, mBarsicPlot.axisY);
        bounds = new Bounds();
        lines = new Lines();
        points = new Points();
    }

    //@Override
    /*public void title(String value) {

    }*/
    
    /*public void bounds(String xLims, String yLims) {
        
    }*/
    
    public void bounds(double xLeft, double xRight, double yBottom, double yTop) {
        mBarsicPlot.dataArea.setRect(xLeft, yBottom, xRight - xLeft, yTop - yBottom);
        mBarsicPlot.initializeSeries();
    }

    //@Override
    public void function(String fx, double a, double b, int n) {
        mBarsicPlot.setFunction(fx, a, b, n);
    }
    
    public void parametricCurve(String xt, String yt, double ta, double tb, int n) {
        
    }
    
    public void tabledFunction(/*TabledFunction fx*/) {
        
    }
    
    public void tabledFunction(/*TabledFunction fx,*/ double a, double b) {
        
    }
    
    public void parametricTabledCurve(/*TabledFunction xt, TabledFunction yt,*/) {
        
    }
    
    public void parametricTabledCurve(/*TabledFunction xt,*/ String yt, double ta, double tb) {
        
    }
    
    public void parametricTabledCurve(/*TabledFunction xt,*/ String yt) {
        
    }

    public void point(double x, double y) {
        if (mBarsicPlot.isSeriesEmpty()) {
            mBarsicPlot.addCurve(new double[]{x}, new double[]{y});
        } else {
            mBarsicPlot.points.addNewPointTo(x, y, 0, mCurPointIndex);
            mCurPointIndex++;
        }
    }

    public void erase(int index) {

    }

    void clear() {
        mBarsicPlot.cleanCurves();
        mCurPointIndex = 0;
    }

    void drawTo(Canvas c)
    {
        mBarsicPlot.plot(c);
    }

    public final class Axes {
        public Axis x;
        public Axis y;

        Axes(SciAxis xAxis, SciAxis yAxis) {
            x = new Axis(xAxis);
            y = new Axis(yAxis);
        }

        public void gridMode(String value) {
        
        }

        public void setDefault() {

        }
    }

    public final class Axis {
        
        /*
        "color"
        "isOffset"
        "labelLength"
        "max"
        "min"
        "power"
        "scale"
        "title"
        "titleColor"
         */

        SciAxis mAxis;

        Axis(SciAxis axis) {
            super();
            mAxis = axis;
        }
        
        public String color() {
            return "#000000";
        }
        
        public void color(String value) {
            
        }
        
        public double min() {
            return 0.0;
        }
        
        public double max() {
            return 0.0;
        }
        
        public void power(int value) {

        }
        
        public String title() {
            return mAxis.getTitle();
        }
        
        public void title(String value) {
            mAxis.setTitle(value);
        }

    }
    
    public final class Bounds
    {
        public void setDefault() {
            mBarsicPlot.dataArea.setRect(mBarsicPlot.DEFAULT_DATA_AREA.getMinX(),
                                         mBarsicPlot.DEFAULT_DATA_AREA.getMinY(),
                                         mBarsicPlot.DEFAULT_DATA_AREA.getWidth(),
                                         mBarsicPlot.DEFAULT_DATA_AREA.getHeight());
            mBarsicPlot.initializeSeries();
        }
    }
    
    public final class Colors {
        
        public void setDefault() {
            
        }
        
    }
    
    public final class Lines {
        
        public Colors colors = new Colors();
        
        public void width(String value) {
            
        }
        
        public void color(String value) {
            
        }
        
        public void colors(String[] values) {
            
        }
        
    }

    public final class Points {
        
        public Colors colors = new Colors();
        
        public void color(String value) {
            
        }
        
        public void colors(String[] values) {
            
        }
        
        public void size(String value) {
            
        }
        
        /*public void style(String value) {
            
        }*/
        
    }

    /**
     * Plot Control class
     */

    final class ABarsicPlot /*extends View implements View.OnTouchListener*/ {

        //private ArrayList<GraphicObject> graphicObjects = new ArrayList<GraphicObject>(128);

        /**
         * boolean value indicates need of scale
         */
        public boolean needScale = false;
        /**
         *
         */
        public PointListAr points;

        //private Bitmap b;
        //private Canvas g;
        private Paint p;
        //private CartTransform ct;

        //private Graph.Brush brush;
        //private Graph.Pen pen;

        //private int width;
        //private int height;
        //private int bitmapSize;

        /**
         *constructor of ABarsicPlot
         */
        public ABarsicPlot() {
            this(null, null);
        }

        /**
         * constructor of ABarsicPlot
         * @param x x-coordinate values
         * @param y y-coordinate values
         */
        public ABarsicPlot(double[] x, double[] y) {
            this("", x, y);
        }

        /**
         * @author Anita
         * constructor of ABarsicPlot
         * @param title
         * @param x x-coordinate values
         * @param y y-coordinate values
         */
        public ABarsicPlot(String title, double[] x, double[] y) {
            initializeGraphics();
            if ((x != null) && (y != null)) {
                addCurve(title, x, y);
            }
            initializeComponent();

            //this.setOnTouchListener(this);
        }

        private void initializeGraphics() {
            //determining size of a bitmap
            DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
            //bitmapSize = max(metrics.widthPixels, metrics.heightPixels);

            //ct = new CartTransform(-5.0, 5.0, -7.0, 7.0, 1024, 768);
            p = new Paint(Paint.ANTI_ALIAS_FLAG);
            //brush = new Graph.Brush(new Graph());
            //pen = new Graph.Pen(new Graph());
            //creating a square bitmap independent on screen orientation
            //b = Bitmap.createBitmap(bitmapSize, bitmapSize, Bitmap.Config.ARGB_8888);
            //g = new Canvas(b); //getting canvas of the bitmap
        }

        private void initializeComponent() {
            axisX = new SciAxis(SciAxis.AxisType.Horizontal);
            axisX.setUnits("");
            axisX.setTitle("");
            axisY = new SciAxis(SciAxis.AxisType.Vertical);
            axisY.setTitle("");
            axisY.setUnits("");
        }

        /*@Override
        protected void onDraw(Canvas g) {
            if (plotMode) {
                plot(g);
            } else {
                for (int i=0; i<graphicObjects.size(); i++) {
                    graphicObjects.get(i).draw(g, p, ct.getCoeffs());
                }
            }
            //g.drawBitmap(b, 0.0f, 0.0f, null);
        }*/

        private void plot(Canvas g) {

            if (isSeriesEmpty()) return;

            pane.draw(g, p, getWidth(), getHeight(), margin);

            if ((points.needCalcDataArea == true) && (needScale == true)) {
                dataArea = calcDataRange(dataArea);
            }
            drawLegend(g);
            if (isSelecting) {
                drawSelectionRect(g, p, selectOffset, clickOffset);
            }
            p.setColor(Color.BLACK);

            axisX.setGridLinesLength(getHeight() - margin.getTop() - margin.getBottom());
            axisX.setMargin(new Margin(15, 15));
            axisX.setWidth(getWidth() - margin.getLeft() - margin.getRight()
                    + axisX.getMargin().getLeft() + axisX.getMargin().getRight());
            axisX.setHeight(margin.getBottom());
            axisX.setTopLeft(new Point2D(margin.getLeft() - axisX.getMargin().getLeft(),
                    this.getHeight() - margin.getBottom()));
            axisX.setDataRange(new Range(dataArea.getMinX(), dataArea.getMaxX()));
            axisX.draw(g, p);
            axisY.setGridLinesLength(getWidth() - margin.getLeft() - margin.getRight());
            axisY.setMargin(new Margin(0, 0));
            axisY.setWidth(getHeight() - margin.getTop() - margin.getBottom()
                    + axisY.getMargin().getLeft() + axisY.getMargin().getRight());
            axisY.setHeight(margin.getLeft());
            axisY.setTopLeft(new Point2D(0, margin.getTop() - axisY.getMargin().getLeft()));
            axisY.setDataRange(new Range(dataArea.getMinY(), dataArea.getMaxY()));
            axisY.draw(g, p);
            p.setStyle(Paint.Style.STROKE);
            renderArea = new Rectangle2D(margin.getLeft(), margin.getTop(),
                    getWidth() - margin.getLeft() - margin.getRight(),
                    getHeight() - margin.getTop() - margin.getBottom());
            g.drawRect(renderArea.getMinX(), renderArea.getMinY(),
                       renderArea.getMaxX(), renderArea.getMaxY(), p);
            p.setStyle(Paint.Style.FILL_AND_STROKE);
            if (points.needCalcDataArea) {
                addPartsOfCurves(g, p);
            } else {
                int size = series.size();
                for (int i = 0; i < size; i++) {
                    series.get(i).draw(g, p, dataArea, renderArea);
                }
            }
        }

        int getWidth() {
            return mSubwindowView.mWidth;
        }

        int getHeight() {
            return mSubwindowView.mHeight;
        }

        /*@Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            if ((w > 0) && (h > 0)) {
                width = w;
                height = h;
                ct.setWindowSizes(width, height);
            }
        }*/

        /*@Override
        public Dimension getMinimumSize() {
            return new Dimension(350, 250);
        }*/

        /**
         *
         * @return
         */
        /*public Dimension getPrefferedSize() {
            return new Dimension(350, 250);
        }*/

        boolean isSeriesEmpty() {
            return (series == null) || (points == null);
        }

        void initializeSeries() {
            if (series == null) {
                series = new ArrayList<Curve>();
            }
            if (points == null) {
                points = new PointListAr();
            }
        }

        /**
         * Sets (x,y)-values that we want to display
         *
         * @param title
         * @param x x-coordinate values
         * @param y y-coordinate values
         */
        void addCurve(String title, double[] x, double[] y) {
            Curve curve = new Curve();
            curve.points = new PointListAr(x, y);
            curve.setTitle(title);
            initializeSeries();
            series.add(curve);
            //dataArea = inscribeToRectangle();
            //seriesChangedPerformed();
        }

        /**
         *
         * @param x
         * @param y
         */
        void addCurve(double[] x, double[] y) {
            addCurve("not set", x, y);
        }

        /**
         * Sets (x,y)-values that we want to display
         *
         * @param title
         * @param x x-coordinate values
         * @param y y-coordinate values
         */
        void addCurve(String title, java.util.LinkedList<Double> x, java.util.LinkedList<Double> y) {
            Curve curve = new Curve();
            curve.points = new PointListAr(x, y);
            curve.points.sort();
            curve.setTitle(title);
            initializeSeries();
            series.add(curve);
            //dataArea = inscribeToRectangle();
            //seriesChangedPerformed();
        }

        /**
         *
         * @param x
         * @param y
         */
        void addCurve(java.util.LinkedList<Double> x, java.util.LinkedList<Double> y) {
            addCurve("not set", x, y);
        }

        /**
         *
         * @param g
         * @author Anita
         */
        void addPartsOfCurves(Canvas g, Paint p) {
            g.clipRect(renderArea.getMinX(), renderArea.getMinY(),
                      renderArea.getMaxX(),renderArea.getMaxY());
            Point2D scaleFactor;
            scaleFactor = calcScaleFactors(dataArea, renderArea);
            g.save();
            double dx = renderArea.getMinX() - dataArea.getMinX() * scaleFactor.getX();
            double dy = renderArea.getMaxY() + dataArea.getMinY() * scaleFactor.getY();
            //[ 1.0  0.0  dx ]
            //[ 0.0 -1.0  dy ]
            //[ 0.0  0.0 1.0 ]
            float[] sv =
                {1.0f,  0.0f, (float)dx,
                 0.0f, -1.0f, (float)dy,
                 0.0f,  0.0f,      1.0f};
            g.translate(sv[Matrix.MTRANS_X], sv[Matrix.MTRANS_Y]);
            series.clear();
            for (int i = 0; i < points.numCurve + 1; i++) {
                Curve curve = new Curve();
                curve.points = new PointListAr(points.X.get(i), points.Y.get(i));
                series.add(curve);
                /*seriesChangedPerformed();*/

                int len = points.ps.get(i).size();
                curve.points.pointStyle = new PointStyle[len];
                for (int m = 0; m < len; m++) {
                    curve.points.pointStyle[m] = points.ps.get(i).get(m);
                }
                curve.points.numN = points.numN;
                //curve.points.partsVisible = true;

                //curve.points.numN = points.Xi[i].length - 1;
                curve.drawNewPartsOfCurve(g, p, scaleFactor);
            }
            g.restore();

        }

    //    calculates optimised dataArea from all dataArea of points
        private Rectangle2D calcDataRange(Rectangle2D dataArea) {
            Rectangle2D[] dataAr = new Rectangle2D[points.numCurve + 1];
            double dmaxX = dataArea.getMaxX();
            double dminX = dataArea.getMinX();
            double dmaxY = dataArea.getMaxY();
            double dminY = dataArea.getMinY();
            for (int i = 0; i < points.numCurve + 1; i++) {
                dataAr[i] = dataArea;
            }

            for (int i = 0; i < points.numCurve+1; i++) {
                Curve curve = new Curve();
                curve.points = new PointListAr(points.X.get(i), points.Y.get(i));
                curve.points.numN = points.X.get(i).size() - 1;
                for (int j = 0; j < curve.points.numN; j++) {
                    double sx = curve.points.x[j];
                    double sy = curve.points.y[j];
                    if (curve.points.x[j] <= dataAr[i].getMinX()) {
                        double dAminX = dataAr[i].getMinX() - dataAr[i].getWidth() / 2;
                        double dAmaxX = dataAr[i].getMaxX();
                        double dAminY = dataAr[i].getMinY();
                        double dAmaxY = dataAr[i].getMaxY();
                        dataAr[i] = new Rectangle2D(dAminX, dAminY, dAmaxX - dAminX, dAmaxY - dAminY);
                    }
                    if (curve.points.x[j] >= dataAr[i].getMaxX()) {
                        double dAminX = dataAr[i].getMinX();
                        double dAmaxX = dataAr[i].getMaxX() + dataAr[i].getWidth() / 2;
                        double dAminY = dataAr[i].getMinY();
                        double dAmaxY = dataAr[i].getMaxY();
                        dataAr[i] = new Rectangle2D(dAminX, dAminY, dAmaxX - dAminX, dAmaxY - dAminY);

                    }
                    if (curve.points.y[j] <= dataAr[i].getMinY()) {
                        double dAminX = dataAr[i].getMinX();
                        double dAmaxX = dataAr[i].getMaxX();
                        double dAminY = dataAr[i].getMinY() - dataAr[i].getHeight() / 2;
                        double dAmaxY = dataAr[i].getMaxY();
                        dataAr[i] = new Rectangle2D(dAminX, dAminY, dAmaxX - dAminX, dAmaxY - dAminY);

                    }
                    if (curve.points.y[j] >= dataAr[i].getMaxY()) {
                        double dAminX = dataAr[i].getMinX();
                        double dAmaxX = dataAr[i].getMaxX();
                        double dAminY = dataAr[i].getMinY();
                        double dAmaxY = dataAr[i].getMaxY() + dataAr[i].getHeight() / 2;
                        dataAr[i] = new Rectangle2D(dAminX, dAminY, dAmaxX - dAminX, dAmaxY - dAminY);

                    }

                }
                if (dataAr[i].getMaxX() > dmaxX) {
                    dmaxX = dataAr[i].getMaxX();
                }
                if (dataAr[i].getMinX() < dminX) {
                    dminX = dataAr[i].getMinX();
                }
                if (dataAr[i].getMaxY() > dmaxY) {
                    dmaxY = dataAr[i].getMaxY();
                }
                if (dataAr[i].getMinY() < dminY) {
                    dminY = dataAr[i].getMinY();
                }

            }
            dataArea = new Rectangle2D(dminX, dminY, dmaxX - dminX, dmaxY - dminY);
            return dataArea;
        }

        /**
         *
         * @param dataArea
         * @param renderArea
         * @return
         */
        protected Point2D calcScaleFactors(Rectangle2D dataArea, Rectangle2D renderArea) {
            double scaleFactorX = (dataArea.getWidth() != 0) ? (renderArea.getWidth() / dataArea.getWidth()) : 1000000000;//double.MaxValue - 10f;
            double scaleFactorY = (dataArea.getHeight() != 0) ? (renderArea.getHeight() / dataArea.getHeight()) : 1000000000;
            return new Point2D(scaleFactorX, scaleFactorY);
        }

        /**
         * cleans arrays of points
         */
        private void cleanPoints() {
            if (points != null) {
                points.numCurve = 0;
                points.numN = 0;
                points.needCalcDataArea = false;
                points.X.clear();
                points.Y.clear();
                points.ps.clear();
            }
        }

        /**
         * cleans series of curves and arrays of points
         */
        void cleanCurves() {
            if (series != null) {
                series.clear();
            }
            this.cleanPoints();
        }
        /**
         *
         */
        public ArrayList<Curve> series;

        /**
         *
         * @return
         */
        public ArrayList<Curve> getSeries() {
            return series;
        }

        /**
         *
         * @param index
         * @return
         */
        public Curve getSeries(int index) {
            return series.get(index);
        }
        //private EventListenerList listenerList = new EventListenerList();

        /**
         *
         * @param l
         */
        /*public void addSeriesChangedListener(java.awt.event.ActionListener l) {
            listenerList.add(java.awt.event.ActionListener.class, l);
        }*/

        /**
         *
         * @param l
         */
        /*public void removeSeriesChangedListener(java.awt.event.ActionListener l) {
            listenerList.remove(java.awt.event.ActionListener.class, l);
        }*/

        /**
         *
         */
        /*public void seriesChangedPerformed() {
            Object[] listeners = listenerList.getListenerList();

            for (int i = 0; i < listeners.length; i++) {
                if (listeners[i] instanceof java.awt.event.ActionListener) {
                    ((java.awt.event.ActionListener) listeners[i]).actionPerformed(null);
                }
            }
        }*/
        /**
         * margin of the plot area on the pane
         */
        private Margin margin = new Margin(80, 20, 20, 60);

        /**
         *
         * @param m
         */
        public void setMargin(Margin m) {
            renderArea = new Rectangle2D(margin.getLeft(), margin.getTop(),
                    getWidth() - margin.getLeft() - margin.getRight(), getHeight() - margin.getTop() - margin.getBottom());
        }

        /**
         *
         * @return
         */
        public Margin getMargin() {
            return margin;
        }

        /**
         *
         * @param value
         */
        public void setAntialising(boolean value) {
            p.setAntiAlias(value);
        }
        /**
         * default field of the values that we want to display
         */
        public final Rectangle2D DEFAULT_DATA_AREA = new Rectangle2D(-10, -10, 20, 20);
        /**
         * field of the values that we want to display
         */
        public Rectangle2D dataArea = new Rectangle2D(-10, -10, 20, 20);
        /**
         * field on the display that we want to display
         */
        private Rectangle2D renderArea = new Rectangle2D(-10, -10, 20, 20);
        private SciAxis axisX;

        /**
         *
         * @return
         */
        public SciAxis getAxisX() {
            return axisX;
        }
        private SciAxis axisY;

        /**
         *
         * @return
         */
        public SciAxis getAxisY() {
            return axisY;
        }

        /**
         *
         * @param XMin
         * @param XMax
         * @param NPoints
         * @return
         */
        public boolean validateTestData(double XMin, double XMax, int NPoints) {
            return (NPoints >= 1 && (XMin <= XMax));
        }

        /**
         *
         * @param xMin
         * @param xMax
         * @param nPoints
         */
        public void setFunction(String functionExpression,
                                double xMin, double xMax, int nPoints) {

            DefFunction f;
            try {
                f = new DefFunction(functionExpression, 'x');
            } catch (Exception e) {
                //Toast.makeText(this.getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                return;
            }

            if (!validateTestData(xMin, xMax, nPoints)) {
                //Toast.makeText(this.getContext(), "Entered data is incorrect", Toast.LENGTH_LONG).show();
                return;
            }

            double delta = 0;

            if (nPoints > 1) {
                delta = (xMax - xMin) / (nPoints - 1);
            }

            double currX = xMin;

            double[] x = new double[nPoints];
            double[] y = new double[nPoints];


            for (int i = 0; i < nPoints; i++) {
                x[i] = currX;
                y[i] = f.getValue(currX);
                currX += delta;
            }

            addCurve(functionExpression, x, y);

            //dataArea = inscribeToRectangle();
        }

        //(debug)
        /**
         *
         * @param xMin
         * @param xMax
         * @param nPoints
         */
        public void setTestDataSinDivX(double xMin, double xMax, int nPoints) {
            setFunction("sin(x)/x", xMin, xMax, nPoints);
        }

        //(debug)
        /**
         *
         * @param xMin
         * @param xMax
         * @param nPoints
         */
        public void setTestDataSqrX(double xMin, double xMax, int nPoints) {
            setFunction("x^2", xMin, xMax, nPoints);
        }

        //(debug)
        /**
         *
         * @param xMin
         * @param xMax
         * @param nPoints
         */
        public void setTestDataSinMulX(double xMin, double xMax, int nPoints) {
            setFunction("x*sin(x)", xMin, xMax, nPoints);
        }

        /**
         *
         * @return minimal data area that contains all data points
         */
        Rectangle2D inscribeToRectangle() {
            int size = (series != null) ? series.size() : 0;

            if (size == 0) {
                return DEFAULT_DATA_AREA;
            }

            Rectangle2D ans = series.get(0).inscribeToRectangle();

            for (int i = 0; i < size; i++) {
                Rectangle2D buf = series.get(i).inscribeToRectangle();
                ans = ans.createUnion(buf);
            }

            return ans;
        }

        /**
         * Sets values that we want to display for the custom function
         *
         * @param expression string presentation of the function
         * @param xmin left limit of x-coordinate
         * @param xmax left limit of x-coordinate
         * @param NPoints number of the points
         */
        /* public void addCurve(String function, double xmin, double xmax, int NPoints) {

        if (!validateTestData(xmin, xmax, NPoints)) {
        return;
        }

        //curve.Points = new PointListAr(function, xmin, xmax, NPoints);

        Curve c=new Curve();
        c.Points= new PointListAr(function, xmin, xmax, NPoints);
        c.setTitle(function);
        series.add(c);
        seriesChangedPerformed();

        c=null;
        dataArea=InscribeToRectangle();
        }*/
        /**
         * (debug)
         * Draw the borders of the whole control
         * @param g Graphics context
         */
        private void drawBorders(Canvas g, Paint p) {
            BasicStroke save = BasicStroke.get(p);

            float dash[] = {2.0f};
            new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
                    BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f).set(p);
            g.drawRect(1, 1, getWidth() - 2, getHeight() - 2, p);
            save.set(p);
        }
        /**
         * is it needs to cancel previous zoom on backward selection
         * (Monahov's order)
         */
        private final boolean CancelZoom = true;

        /**
         *
         * @param p1 first selection point
         * @param p2 second selection point
         */
        private void onSelectionEnd(Point p1, Point p2) {
            System.out.println("onSelectionEnded");


            if ((p2.getX() >= p1.getX()) && CancelZoom) {
                //initiates original size (if the option CancleZoom is enabled)
                //dataArea = inscribeToRectangle();
                //repaint();
                return;
            }

            int x1, y1, x2, y2;

            if (p1.getX() <= p2.getX()) {
                x1 = (int) p1.getX();
                x2 = (int) p2.getX();
            } else {
                x2 = (int) p1.getX();
                x1 = (int) p2.getX();
            }

            if (p1.getY() <= p2.getY()) {
                y1 = (int) p1.getY();
                y2 = (int) p2.getY();
            } else {
                y2 = (int) p1.getY();
                y1 = (int) p2.getY();
            }

            x1 = (int) Math.max(renderArea.getMinX(), x1);
            y1 = (int) Math.max(renderArea.getMinY(), y1);

            x2 = (int) Math.min(renderArea.getMaxX(), x2);
            y2 = (int) Math.min(renderArea.getMaxY(), y2);

            zoomIn(new Rectangle2D(x1, y1, x2 - x1, y2 - y1));
        }

        /**
         *
         * @return
         */
        public String[] getSeriesTitles() {
            int size = series.size();
            String[] titles = new String[size];

            for (int i = 0; i < size; i++) {
                titles[i] = series.get(i).getTitle();
            }

            return titles;
        }
        private Pane pane = new Pane();

        /**
         *
         * @return
         */
        public Pane getPane() {
            return pane;
        }
        private Legend legend = new Legend();

        /**
         *
         * @return
         */
        public Legend getLegend() {
            return legend;
        }
        AxisValue.AlignW alignLegendX = AxisValue.AlignW.Right;

        /**
         *
         * @param value
         */
        public void setLegendAlignmentX(AxisValue.AlignW value) {
            this.alignLegendX = value;
        }
        AxisValue.AlignH alignLegendY = AxisValue.AlignH.Top;

        /**
         *
         * @param value
         */
        public void setLegendAlignmentY(AxisValue.AlignH value) {
            this.alignLegendY = value;
        }

        /**
         *
         * @param g
         */
        public void drawLegend(Canvas g) {
            int size = series.size();
            double legendX = 0;
            double legendY = 0;
            double gap = 5;

            switch (alignLegendX) {
                case Left:
                    legendX = margin.getLeft() + gap;
                    legend.setAlignW(AxisValue.AlignW.Right);
                    break;
                case Center:
                    legendX = margin.getLeft() + (this.getWidth() - margin.getLeft() - margin.getRight()) * 0.5;
                    legend.setAlignW(AxisValue.AlignW.Center);
                    break;
                case Right:
                    legendX = this.getWidth() - margin.getRight() - gap;
                    legend.setAlignW(AxisValue.AlignW.Left);
                    break;
            }

            switch (alignLegendY) {
                case Bottom:
                    legendY = this.getHeight() - margin.getBottom() - gap;
                    legend.setAlignH(AxisValue.AlignH.Top);
                    break;
                case Center:
                    legendY = margin.getTop() + (this.getHeight() - margin.getTop() - margin.getBottom()) * 0.5;
                    legend.setAlignH(AxisValue.AlignH.Center);
                    break;
                case Top:
                    legendY = margin.getTop() + gap;
                    legend.setAlignH(AxisValue.AlignH.Bottom);
                    break;
            }

            legend.setPos(new Point2D(legendX, legendY));

            //if(legend.getItemsCount()!=size)
            //{
            legend.clear();
            for (int i = 0; i < size; i++) {
                legend.addCurve(series.get(i).getStyle(), series.get(i).getTitle());
            }
            // }
    //
    //        if (size != 0) {
    //            legend.draw(g);
    //        }

        }

        /**
         * Moves plot on dx,dy
         * @param delta translation vector
         */
        private void dragPlot(Point2D delta) {
            points.needCalcDataArea = false;
            double w = dataArea.getWidth();
            double h = dataArea.getHeight();

            double scaleFactorX = (dataArea.getWidth() != 0) ? (renderArea.getWidth() / dataArea.getWidth()) : 1000000000;//double.MaxValue - 10f;
    //        scaleFactorX = java.lang.Math.max(scaleFactorX, MIN_SCALE_FACTOR);
            //       scaleFactorX = java.lang.Math.min(scaleFactorX, MAX_SCALE_FACTOR);

            double scaleFactorY = (dataArea.getHeight() != 0) ? (renderArea.getHeight() / dataArea.getHeight()) : 1000000000;


            //System.err.println(String.format("s=(%f,%f)", scaleFactorX,scaleFactorY));
            //       scaleFactorY = java.lang.Math.max(scaleFactorY, MIN_SCALE_FACTOR);
            //       scaleFactorY = java.lang.Math.min(scaleFactorY, MAX_SCALE_FACTOR);

            dataArea.setRect(dataArea.getMinX() - (delta.getX()) / scaleFactorX, dataArea.getMinY() + (delta.getY()) / scaleFactorY, w, h);
        }

        /**
         * Zooms plot
         * @param center mouse position-the center of zoom
         * @param scale scale factor
         */
        public void zoomIn(Point2D center, double scale) {
            points.needCalcDataArea = false;
            double nx = (renderArea.getWidth() != 0) ? ((center.getX() - renderArea.getMinX()) / renderArea.getWidth()) : 0.5;
            double ny = (renderArea.getHeight() != 0) ? ((center.getY() - renderArea.getMinY()) / renderArea.getHeight()) : 0.5;

            double scaleFactorX = (dataArea.getWidth() != 0) ? (renderArea.getWidth() / dataArea.getWidth()) : 1000000000;//double.MaxValue - 10f;
    //        scaleFactorX = java.lang.Math.max(scaleFactorX, MIN_SCALE_FACTOR);
    //        scaleFactorX = java.lang.Math.min(scaleFactorX, MAX_SCALE_FACTOR);


            double scaleFactorY = (dataArea.getHeight() != 0) ? (renderArea.getHeight() / dataArea.getHeight()) : 1000000000;
    //        scaleFactorY = java.lang.Math.max(scaleFactorY, MIN_SCALE_FACTOR);
            //       scaleFactorY = java.lang.Math.min(scaleFactorY, MAX_SCALE_FACTOR);
            //disable scaling to bypass overflow exception
    /*        if ((scaleFactorX >= MAX_SCALE_FACTOR || scaleFactorY >= MAX_SCALE_FACTOR) && scale < 1) {
            return;
            }
            if ((scaleFactorX <= MIN_SCALE_FACTOR || scaleFactorY <= MIN_SCALE_FACTOR) && scale > 1) {
            return;
            }*/

            double dx = renderArea.getMinX() - dataArea.getMinX() * scaleFactorX;
            double dy = renderArea.getMaxY() + dataArea.getMinY() * scaleFactorY;


            double mouse_center_x = (center.getX() - dx) / scaleFactorX;
            double mouse_center_y = -(center.getY() - dy) / scaleFactorY;

            dataArea.setRect(mouse_center_x - nx * dataArea.getWidth() * scale, mouse_center_y - (1 - ny) * dataArea.getHeight() * scale, dataArea.getWidth() * scale, dataArea.getHeight() * scale);
        }

        /**
         * Zooms the plot
         * @param newRenderArea new render area
         */
        public void zoomIn(Rectangle2D newRenderArea) {
             points.needCalcDataArea =false;
            System.out.println("zoomIn()");
            double nx = (newRenderArea.getMinX() - renderArea.getMinX()) / renderArea.getWidth();
            double ny = (newRenderArea.getMinY() - renderArea.getMinY()) / renderArea.getHeight();

            double sx = (newRenderArea.getWidth() / renderArea.getWidth());
            double sy = (newRenderArea.getHeight() / renderArea.getHeight());

            double w = dataArea.getWidth();
            double h = dataArea.getHeight();

            dataArea.setRect(dataArea.getMinX() + w * nx, dataArea.getMinY() + h * (1 - (ny + sy)), sx * w, h * sy);
        }

        /**
         *
         * @param scale
         */
        public void zoomIn(double scale) {
             points.needCalcDataArea =false;
            double deltaX = dataArea.getWidth() * (1 - scale) * 0.5;
            double deltaY = dataArea.getHeight() * (1 - scale) * 0.5;

            double newWidth = dataArea.getWidth() * scale;
            double newHeight = dataArea.getHeight() * scale;

            dataArea.setRect(dataArea.getMinX() + deltaX, dataArea.getMinY() + deltaY, newWidth, newHeight);

        }

        /**
         *
         * @param p mouse cursor point on the control
         * @return is the point lies on vertical axis
         */
        private boolean isOnVerticalAxis(Point2D p) {
            if (p.getX() >= 0 && p.getX() <= margin.getLeft() && p.getY() >= 0 && p.getY() <= this.getHeight()) {
                return true;
            }

            return false;
        }

        /**
         *
         * @param p mouse cursor point on the control
         * @return is the point lies on horizontal axis
         */
        private boolean isOnHorizontalAxis(Point2D p) {
            if (p.getX() >= margin.getLeft() && p.getX() <= this.getWidth() && p.getY() >= (this.getHeight() - margin.getBottom()) && p.getY() <= this.getHeight()) {
                return true;
            }

            return false;
        }

        /**
         *
         * @param p mouse cursor point on the control
         * @return is the point lies on plot area
         */
        private boolean isOnPlot(Point2D p) {
            if (p.getX() >= margin.getLeft() && p.getX() <= (this.getWidth() - margin.getRight()) && p.getY() >= margin.getTop() && p.getY() <= (this.getHeight() - margin.getBottom())) {
                return true;
            }

            return false;
        }

        /**
         * updates cursor type that respects cursor position and key modifiers
         * @param e Mouse event
         */
        /*private void updateCursor(MouseEvent e) {
            //System.out.println("updateCursor");

            Point2D currPoint = new Point2D(e.getX(), e.getY());

            if ((e.getModifiersEx() & MouseEvent.ALT_DOWN_MASK) != 0) {
                if (isOnPlot(currPoint)) {
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                    return;
                }
            }


            if ((e.getModifiersEx() & MouseEvent.CTRL_DOWN_MASK) != 0) {
                if (isOnPlot(currPoint)) {
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
                    return;
                }
            }

            if (isOnVerticalAxis(currPoint) || isDraggingY) {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));//HAND_CURSOR
                return;
            }

            if (isOnHorizontalAxis(currPoint) || isDraggingX) {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                return;
            }

            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        }

        private void updateCursor(Point2D p, int modifiers) {
            //System.out.println("updateCursor");

            if (p == null) {
                return;
            }

            Point2D currPoint = new Point2D(p.getX(), p.getY());

            if ((modifiers & MouseEvent.ALT_DOWN_MASK) != 0) {
                if (isOnPlot(currPoint)) {
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                    return;
                }
            }

            if ((modifiers & MouseEvent.CTRL_DOWN_MASK) != 0) {
                if (isOnPlot(currPoint)) {
                    this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
                    return;
                }
            }

            if (isOnVerticalAxis(currPoint)) {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                return;
            }

            if (isOnHorizontalAxis(currPoint)) {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                return;
            }

            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

        }*/
        private Point clickOffset = new Point(0, 0);//initial selection rect point
        private Point selectOffset = new Point(0, 0);//travelling selection rect point
        private boolean isSelecting = false;//is the selection mode enabled
        private boolean isDraggingX = false;//is the plot dragging by x axis using hand tool on axis
        private boolean isDraggingY = false;//is the plot dragging by y axis using hand tool on axis

        /**
         * Enables or disables selection mode
         * @param bSelection will be the selection mode enabled?
         */
        private void enableSelectionMode(boolean bSelection) {
            if (bSelection) {
                isSelecting = true;
                isDraggingX = false;
                isDraggingY = false;
            } else {
                selectOffset = clickOffset;
                isSelecting = false;
            }
        }

        /**
         * Draws the selection rect using paint(non-xor) mode
         * @param g Graphics context
         * @param p1 a selection point
         * @param p2 a selection point
         */
        private void drawSelectionRect(Canvas g, Paint p, Point p1, Point p2) {
            BasicStroke save = BasicStroke.get(p);

            float dash[] = {2.0f, 2.0f};
            new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
                    BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f).set(p);

            int x1, y1, x2, y2;

            if (p1.getX() <= p2.getX()) {
                x1 = (int) p1.getX();
                x2 = (int) p2.getX();
            } else {
                x2 = (int) p1.getX();
                x1 = (int) p2.getX();
            }

            if (p1.getY() <= p2.getY()) {
                y1 = (int) p1.getY();
                y2 = (int) p2.getY();
            } else {
                y2 = (int) p1.getY();
                y1 = (int) p2.getY();
            }

            x1 = (int) Math.max(renderArea.getMinX(), x1);
            y1 = (int) Math.max(renderArea.getMinY(), y1);

            x2 = (int) Math.min(renderArea.getMaxX(), x2);
            y2 = (int) Math.min(renderArea.getMaxY(), y2);

            p.setColor(Color.DKGRAY);

            p.setStyle(Paint.Style.STROKE);
            g.drawRect(x1, y1, x2, y2, p);
            p.setStyle(Paint.Style.FILL_AND_STROKE);
            save.set(p);
        }

        /*public void mouseWheelMoved(MouseWheelEvent e) {
             points.needCalcDataXYRange=false;
            if (e.getModifiers() == InputEvent.CTRL_MASK) {
                if (GeomUtils.isInBox(e.getPoint(), this.renderArea)) {
                    double scale = java.lang.Math.pow(0.9, -e.getWheelRotation());
                    this.zoomIn(e.getPoint(), scale);
                    this.repaint();
                }
            }
        }*/

        /*public void mouseClicked(MouseEvent e) {
             points.needCalcDataXYRange=false;
             updateCursor(e);
        }*/

        /*public void mousePressed(MouseEvent e) {
            updateCursor(e);
            //System.out.println("mousePressed");
            //g_save=(Graphics2D)this.getGraphics();
        }*/

        //@Override
        public boolean onTouch(View view, MotionEvent event) {

            switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:

                points.needCalcDataArea=false;
                clickOffset = new Point(event.getX(), event.getY());
                selectOffset = new Point(event.getX(), event.getY());

                if ((!isOnPlot(new Point2D(event.getX(), event.getY()))) && (!isDraggingX) && (!isDraggingY)) {
                    enableSelectionMode(false);
                }
                enableSelectionMode(true);
                break;

            case MotionEvent.ACTION_MOVE:

                points.needCalcDataArea = false;

                Point2D currPoint = new Point2D(event.getX(), event.getY());

                //if (((event.getModifiersEx() & MouseEvent.ALT_DOWN_MASK) == 0) && isOnPlot(currPoint) && (!isDraggingX) && (!isDraggingY)) {
                if (isOnPlot(currPoint) && (!isDraggingX) && (!isDraggingY)) {
                    enableSelectionMode(true);
                }

                //if ((e.getModifiersEx() & MouseEvent.ALT_DOWN_MASK) != 0) {
                if (isOnPlot(currPoint)) {
                    dragPlot(new Point2D(event.getX() - clickOffset.getX(), event.getY() - clickOffset.getY()));
                }
                enableSelectionMode(false);
                //}

                if (!isSelecting) {
                    if (isOnVerticalAxis(currPoint) || isDraggingY) {
                        dragPlot(new Point2D(0, event.getY() - clickOffset.getY()));
                        enableSelectionMode(false);
                        isDraggingY = true;
                    }

                    if (isOnHorizontalAxis(currPoint) || isDraggingX) {
                        dragPlot(new Point2D(event.getX() - clickOffset.getX(), 0));
                        enableSelectionMode(false);
                        isDraggingX = true;
                    }
                }

                clickOffset = new Point(event.getX(), event.getY());

                //repaint();
                //long time=System.currentTimeMillis();

                /*if (isSelecting) {
                    //repaint();
                    long time = System.currentTimeMillis();
                    //if(g_save==null)g_save=(Graphics2D)this.getGraphics();
                    //drawSelectionRect_xor(g_save,selectOffset,clickOffset);
                    //System.out.println("time=" + (System.currentTimeMillis() - time));
                }*/
                break;

            case MotionEvent.ACTION_UP:

                points.needCalcDataArea=false;
                if (isSelecting && abs(clickOffset.getX() - selectOffset.getX()) >= 5 && abs(clickOffset.getY() - selectOffset.getY()) >= 5) {
                    onSelectionEnd(clickOffset, selectOffset);
                }

                enableSelectionMode(false);
                isDraggingX = false;
                isDraggingY = false;

                break;
            }
            //repaint();
            return true;
        }

    }

    /**
     * Plot Control class
     */
    final class AxisControl /*extends View*/ {
        /**
         *
         */
        Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);

        public AxisControl(Context context, AttributeSet attrs) {
           //super(context, attrs);
           InitializeComponent();
        }


        /**
         *
         */
        public SciAxis axisX;


        /**
         * Margin of the plot field borders on the display that we want to display
         */
        private Margin margin = new Margin(75, 20, 25, 45);

        /**
         *
         * @param m
         */
        public void setMargin(Margin m) {
            renderArea = new Rectangle2D(margin.getLeft(), margin.getTop(),
                    getWidth() - margin.getLeft() - margin.getRight(), getHeight() - margin.getTop() - margin.getBottom());
        }

        /**
         *
         * @return
         */
        public Margin getMargin() {
            return margin;
        }
        /**
         * default field of the values that we want to display
         */
        public final Rectangle2D DEFAULT_DATA_AREA = new Rectangle2D(-10, -10, 20, 20);
        /**
         *
         */
        public static final double MAX_SCALE_FACTOR = 5E7;
        /**
         *
         */
        public static final double MIN_SCALE_FACTOR = 1E-7;
        /**
         * field of the values that we want to display
         */
        private Rectangle2D DataArea = new Rectangle2D(-10, -10, 20, 20);
        /**
         * field on the display that we want to display
         */
        private Rectangle2D renderArea = new Rectangle2D(-10, -10, 20, 20);

        private void InitializeComponent() {

            axisX = new SciAxis();

        }

        //debug
        /**
         *
         * @param theMessage
         */
        public void showException(String theMessage) {
            //AxisControl.showException(theMessage);
            //javax.swing.JOptionPane.showMessageDialog((java.awt.Component) null, theMessage, "alert", javax.swing.JOptionPane.ERROR_MESSAGE);
        }

        //@Override
        public void draw(Canvas g) {

         long time = System.currentTimeMillis();
            //System.out.println("timer started");
            //super.draw(g);

            renderArea = new Rectangle2D(margin.getLeft(), margin.getTop(),
                    getWidth() - margin.getLeft() - margin.getRight(), getHeight() - margin.getTop() - margin.getBottom());


            double x=400;
            double y=400;

            axisX.setWidth(350);
            axisX.setHeight(50);
            axisX.setTopLeft(new Point2D(x,y));
            axisX.setDataRange(new Range(DataArea.getMinX(), DataArea.getMaxX()));

            //System.out.println("time_0=" + (System.currentTimeMillis() - time));

            axisX.draw(g, p);

            p.setColor(Color.BLUE);
            new Line2D(x, 0, x, 1000).draw(g, p);
            new Line2D(0, y, 1000, y).draw(g, p);

            p.setColor(Color.RED);
            axisX.getBorders().draw(g, p);
            //System.out.println("time_2=" + (System.currentTimeMillis() - time));

        }

        int getWidth() {
            return mSubwindowView.mWidth;
        }

        int getHeight() {
            return mSubwindowView.mHeight;
        }

        //public Dimension getMinimumSize() {
        //    return new Dimension(350, 250);
        //}

        /**
         *
         * @return
         */
        //public Dimension getPrefferedSize() {
        //    return new Dimension(350, 250);
        //}
    }
}
