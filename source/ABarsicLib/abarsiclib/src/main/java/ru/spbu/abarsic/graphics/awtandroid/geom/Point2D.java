/*
 * Point2D.java
 */

package ru.spbu.abarsic.graphics.awtandroid.geom;

/**
 * @author Martynyuk
 * */

import android.graphics.Canvas;
import android.graphics.Paint;

public class Point2D extends Figure2D {
	
	float x;
	float y;
	
	public Point2D(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public Point2D(double x, double y) {
		super();
		this.x = (float)x;
		this.y = (float)y;
	}
	
	@Override
	public void draw(Canvas g, Paint p) {
		g.drawPoint(x, y, p);
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}

}
