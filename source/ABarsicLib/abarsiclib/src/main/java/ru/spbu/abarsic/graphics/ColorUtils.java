package ru.spbu.abarsic.graphics;

import java.util.Arrays;
import java.lang.InstantiationException;

/**
 * Created by Мартынюк on 09.02.2015.
 */
public final class ColorUtils {

    private ColorUtils() throws InstantiationException {
        throw new InstantiationException("Attempt to create an object of \"ColorUtils\" class");
    }

    public static String formatColorAsString(int color) {
        String result;
        result = Integer.toString(color & 0x00FFFFFF, 16);
        result = '#'+fillString('0', 6-result.length())+result;
        return result;
    }

    public static int formatColorAsInt(String color) {
        int result;
        if ((color.charAt(0) != '#') || (color.length() != 7)) {
            throw new NumberFormatException("Illegal color format");
        }
        result = Integer.parseInt(color.substring(1), 16) | 0xFF000000;
        return result;
    }

    public static String fillString(char ch, int len) {
        if (len > 0) {
            char chars[] = new char[len];
            Arrays.fill(chars, ch);
            return new String(chars);
        } else {
            return "";
        }
    }

    public static int makeColorTransparent(int color) {
        return color & 0x00FFFFFF;
    }

    public static int makeColorOpaque(int color) {
        return color | 0xFF000000;
    }

    public static boolean isOpaque(int color) {
        return (color >>> 24) == (0xFF);
    }

    public static boolean isTransparent(int color) {
        return (color >>> 24) == (0x00);
    }

}
