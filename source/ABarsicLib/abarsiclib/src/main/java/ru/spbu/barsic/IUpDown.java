package ru.spbu.barsic;

public interface IUpDown extends IElement {
    void setIsEnabledOnCalc(boolean value);
}
