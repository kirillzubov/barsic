package ru.spbu.abarsic.graphics;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.AbsoluteLayout;

import java.util.Arrays;

import static ru.spbu.abarsic.graphics.CartTransform.*;

import ru.spbu.abarsic.IModel;
import ru.spbu.abarsic.Model;
import ru.spbu.barsic.ISubwindow;

/**
 * Created by Мартынюк on 20.09.2015.
*/
public final class Subwindow extends SurfaceView implements ISubwindow {

    Model mModel;
    SubwindowManager mSubwindow = new SubwindowManager();
    Graph mGraph = new Graph();
    Plot mPlot = new Plot();

    double mXMin, mXMax, mYMin, mYMax;
    int mWidth, mHeight;
    double[] mCoeffs = new double[4];

    boolean mUpdating, mEndOutputCalled;

    SurfaceHolder mSurfaceHolder;
    Canvas mCanvas;
    boolean mCanvasLocked = false;

    int width1=0,height1=0,x1=0,y1=0;

    public Subwindow(Context context, AttributeSet attrs) {
        super(context, attrs);
        mXMin = DEF_BOUNDS[0];
        mXMax = DEF_BOUNDS[1];
        mYMin = DEF_BOUNDS[2];
        mYMax = DEF_BOUNDS[3];
        mWidth = DEF_VIEW_SIZES[0];
        mHeight = DEF_VIEW_SIZES[1];
        calculateCoeffs(mXMin, mXMax, mYMin, mYMax, mWidth, mHeight, mCoeffs);
        mGraph.mCoeffs = Arrays.copyOf(mCoeffs, 4);
        mSurfaceHolder = getHolder();
    }

    //necessary method which must be called after creation of subwindow, plot and graph objects.
    /*public void bindWithCommonObjects(SubwindowManager subwindow, Plot plot, Graph graph) {
        if ((subwindow == null) || (plot == null) || (graph == null))
            throw new IllegalArgumentException("SubwindowManager, plot and graph objects must be created in advance.");
        mSubwindow = subwindow;
        mPlot = plot;
        mGraph = graph;
    }*/

    //necessary method which must be called after creation of the model object.
    public void bindWithCommonObjects(IModel model) {
        if (model == null) {
            throw new IllegalArgumentException("Model object must be created in advance.");
        }
        mModel = (Model)model;
    }

    Canvas lockCanvas() {
        if (mCanvasLocked) return mCanvas;
        mCanvas = mSurfaceHolder.lockCanvas();
        if (mCanvas != null) {
            mCanvasLocked = true;
        }
        return mCanvas;
    }

    void unlockCanvasAndPost() {
        if (!mCanvasLocked) return;
        mSurfaceHolder.unlockCanvasAndPost(mCanvas);
        mCanvasLocked = false;
    }

    void update() {
        lockCanvas();
        if (mCanvas != null) {
            mPlot.drawTo(mCanvas);
            mGraph.drawTo(mCanvas);
            unlockCanvasAndPost();
        }

        mUpdating = false;
        if (mEndOutputCalled) {
            endOutput();
            mEndOutputCalled = false;
        }
    }

    @Override
    public void output() {
        mSubwindow.mSubwindowView = this;
        mGraph.mSubwindowView = this;
        mPlot.mSubwindowView = this;
        mModel.subwindow = mSubwindow;
        mModel.graph = mGraph;
        mModel.plot = mPlot;
    }

    @Override
    public void endOutput() {
        if (mUpdating) {
            mEndOutputCalled = true;
        } else {
            //mSubwindow.mSubwindowView = null;
            //mGraph.mSubwindowView = null;
            //mPlot.mSubwindowView = null;
        }
    }

    //this method is called in onWindowFocusChanged of Activity class for every view to
    //determine transformation coefficients correctly.
    public void setViewSizes(int width, int height) {
        mWidth = width;
        mHeight = height;
        calculateCoeffs(mXMin, mXMax, mYMin, mYMax, mWidth, mHeight, mCoeffs);
    }

    @Override
    public void setX(int value) {
        x1=value;
        AbsoluteLayout.LayoutParams params =
                new AbsoluteLayout.LayoutParams(width1, height1, x1, y1);
        super.setLayoutParams(params);
    }

    @Override
    public void setY(int value) {
        y1=value;
        AbsoluteLayout.LayoutParams params =
                new AbsoluteLayout.LayoutParams(width1, height1, x1, y1);
        super.setLayoutParams(params);
    }

    @Override
    public void setHeight(int value) {
        /*if (value <= 0) return;
        setMeasuredDimension(getWidth(), value);
        mHeight = value;*/
        height1=value;
        AbsoluteLayout.LayoutParams parampampam =
                new AbsoluteLayout.LayoutParams(width1, height1, x1, y1);
        super.setLayoutParams(parampampam);
    }


    @Override
    public void setWidth(int value) {
        width1=value;
        AbsoluteLayout.LayoutParams params =
                new AbsoluteLayout.LayoutParams(width1, height1, x1, y1);
        super.setLayoutParams(params);
    }


    @Override
    public void setName(String value) {

    }

    @Override
    public void setLeftPos(int value) {
        setLeft(value);
    }

    @Override
    public void setTopPos(int value) {
        setTop(value);
    }

    public int width() {
        return mWidth;
    }

    @Override
    public void width(int value) {
        if (value <= 0) return;
        setMeasuredDimension(value, getHeight());
        mWidth = value;
    }

    public int height() {
        return mHeight;
    }

    @Override
    public void height(int value) {
        if (value <= 0) return;
        setMeasuredDimension(getWidth(), value);
        mHeight = value;
    }

    @Override
    public void setHint(String value) {
        //setTag(value);
    }

    @Override
    public void setIsEnabled(boolean value) {
        setEnabled(value);
    }

    @Override
    public void setIsVisible(boolean value) {
        //setVisibility();
    }

    @Override
    public void setAlign(String value) {
        //setLayoutParams();
    }

    @Override
    public void setIsParentBackColor(boolean value) {

    }

    @Override
    public void setBackColor(String value) {
        //setBackgroundColor();
    }

    @Override
    public void setOnHelp(final Runnable eventHandler) {

    }

    @Override
    public void setOnMouseDown(final Runnable eventHandler) {
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    eventHandler.run();
                }
                return true;
            }
        });
    }

    @Override
    public void setOnDrag(final Runnable eventHandler) {
        setOnDragListener(new OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                if (event.getAction() == DragEvent.ACTION_DRAG_STARTED)
                    eventHandler.run();
                return true;
            }
        });
    }

    @Override
    public void setOnDragDrop(final Runnable eventHandler) {
        setOnDragListener(new OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                if (event.getAction() == DragEvent.ACTION_DROP)
                    eventHandler.run();
                return true;
            }
        });
    }

    @Override
    public void setOnMouseOver(final Runnable eventHandler) {

    }

    @Override
    public void setOnMouseScale(final Runnable eventHandler) {

    }

}
