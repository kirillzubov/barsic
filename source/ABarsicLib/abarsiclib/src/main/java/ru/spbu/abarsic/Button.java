package ru.spbu.abarsic;

import android.content.Context;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.*;

import java.util.concurrent.locks.ReentrantLock;

import ru.spbu.barsic.IButton;

/**
 * Created by nik on 18.04.2015.
 */
public class Button extends android.widget.Button implements IButton {

    int width1=0,height1=0,x1=0,y1=0;

    private ReentrantLock mMainLock;

    /*public Button() {
        super(FormsActivity.instance);
    }*/

    public Button(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //public int width=100, height=100, x=300, y=500;

    public void reload(){
        this.refreshDrawableState();
    }

    public void executeJavascript(String code){

    }

    public void setName(String value){

    }

    @Override
    public void setLeftPos(int value) {
        setLeft(value); //use setX
    }

    @Override
    public void setTopPos(int value) {
        setTop(value); //use setY
    }



    @Override
    public void setX(int value) {
        x1=value;
        AbsoluteLayout.LayoutParams params =
                new AbsoluteLayout.LayoutParams(width1, height1, x1, y1);
        super.setLayoutParams(params);
    }


    @Override
    public void setY(int value) {
        y1=value;
        AbsoluteLayout.LayoutParams params =
                new AbsoluteLayout.LayoutParams(width1, height1, x1, y1);
        super.setLayoutParams(params);
    }

    @Override
    public int width() {
        return getWidth();
    }

    @Override
    public void width(int value) {
        width1=value;
        AbsoluteLayout.LayoutParams params =
                new AbsoluteLayout.LayoutParams(width1, height1, x1, y1);
        super.setLayoutParams(params);
    }


    @Override
    public int height() {
        return getHeight();
    }

    public void height(int value){
        height1=value;
        AbsoluteLayout.LayoutParams parampampam =
                new AbsoluteLayout.LayoutParams(width1, height1, x1, y1);
        super.setLayoutParams(parampampam);
    }

    @Override
    public void setHint(String value){
        int hintColor = android.R.color.holo_blue_dark;
        super.setHintTextColor(getResources().getColor(hintColor));
        super.setHint(value);
    }

    public void setIsEnabled(boolean value){
        setEnabled(value);
    }

    public void setIsVisible(boolean value){
        if (value==true) {
            this.setVisibility(this.VISIBLE);
        }
        else
            this.setVisibility(this.INVISIBLE);
    }

    public void setAlign(String value){
        switch(value) {
            case "left":
                AbsoluteLayout.LayoutParams parampampam1 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam1);
                break;
            case "right":
                AbsoluteLayout.LayoutParams parampampam2 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam2);
                break;
            case "top":
                AbsoluteLayout.LayoutParams parampampam3 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam3);
                break;
            case "bottom":
                AbsoluteLayout.LayoutParams parampampam4 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam4);
                break;
            default:
                AbsoluteLayout.LayoutParams parampampam5 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam5);
                break;
        }

    }

    @Override
    public void setOnHelp(Runnable eventHandler) {

    }

    public int textSize;
    @Override
    public void setTextSize(int value){
        textSize=value;
    }

    @Override
    public void setBackColor(String value) {
        //int color = android.R.color.value;
        int color = android.R.color.holo_blue_light;
        setBackgroundColor(getResources().getColor(color));
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void setTitle(String value) {
        this.setText(value);
    }

    @Override
    public void setText(final String value) {
        final Button button = this;
            if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
                post(new Runnable() {
                    @Override
                    public void run() {
                        button.setTextSize(textSize);
                        button.setText(value);
                    }
                });
            } else {
                if (textSize!=0) {
                    super.setTextSize(textSize);
                }
                super.setText(value);
                int backgroundcolor = android.R.color.background_dark;
                int textcolor = android.R.color.background_light;
                super.setBackgroundColor(getResources().getColor(backgroundcolor));
                super.setTextColor(getResources().getColor(textcolor));
        }
    }

    @Override
    public void setIsDefaultOK(boolean value) {

    }

    @Override
    public void setIsParentFont(boolean value) {

    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {

    }

    @Override
    public void setFont(String value) {

    }

    @Override
    public void bindWithCommonObjects(IModel model) {
        mMainLock = ((Model)model).mMainLock;
    }

    @Override
    public void setOnClick(final Runnable eventHandler) {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mMainLock.lock();
                        try {
                            eventHandler.run();
                        } finally {
                            mMainLock.unlock();
                        }
                    }
                }).start();
            }
        });
    }

}
