package ru.spbu.barsic;

/**
 * Created by Мартынюк on 23.04.2016.
 */
public interface IApplication {

    IElement[] elements();

    String getName();

    String getPath();

    int maxArrayLength();

    void maximize();

    int maxRecoursionDepth();

    void minimize();

    IModule[] modules();

    String[] parameters();

    void processMessages();

    void quit();

    void sleep(long time);

    boolean watchesVisible();

}
