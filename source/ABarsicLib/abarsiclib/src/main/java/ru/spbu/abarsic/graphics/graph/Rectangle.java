package ru.spbu.abarsic.graphics.graph;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import static ru.spbu.abarsic.graphics.CartTransform.*;

/**
 * Created by Мартынюк on 17.04.2015.
 */
public final class Rectangle extends GraphicPrimitive {

    double w;
    double h;

    private RectF rect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);

    public Rectangle(double xCenter, double yCenter, double width, double height, int fillColor, int strokeColor, double strokeWidth) {
        super(xCenter, yCenter, fillColor, strokeColor, strokeWidth);
        if ((width < 0.0) || (height < 0.0)) {
            throw new IllegalArgumentException(String.format("Width and height must be" +
                    "larger or equal to zero: w=%f, h=%f", width, height));
        }
        this.w = width;
        this.h = height;
    }

    @Override
    public void draw(Canvas c, Paint fillPaint, Paint strokePaint, double[] coeffs) {
        rect.set((float)(coeffs[AX]*(x0-w*0.5)+coeffs[BX]), (float)(coeffs[AY]*(y0+h*0.5)+coeffs[BY]),
                 (float)(coeffs[AX]*(x0+w*0.5)+coeffs[BX]), (float)(coeffs[AY]*(y0-h*0.5)+coeffs[BY]));
        setFillPaint(fillPaint);
        c.drawRect(rect, fillPaint);
        setStrokePaint(strokePaint, coeffs[AX]);
        c.drawRect(rect, strokePaint);
    }
}
