package ru.spbu.abarsic;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.CookieManager;

import java.util.ArrayList;

import ru.spbu.abarsic.graphics.Graph;
import ru.spbu.abarsic.graphics.Plot;
import ru.spbu.abarsic.graphics.SubwindowManager;
import ru.spbu.abarsic.graphics.Subwindow;
import ru.spbu.barsic.IElement;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Мартынюк on 30.09.2015.
 */
public abstract class Model extends Fragment implements IModel {

    protected ViewGroup mRootLayout;

    ArrayList<View> mViews;

    ReentrantLock mMainLock;

    public Application application;
    public System system;

    public SubwindowManager subwindow;
    public Plot plot;
    public Graph graph;

    public Model() {
        super();
        mViews = new ArrayList<View>(128);
        mMainLock = new ReentrantLock(true);
        application = new Application(this);
        system = new System();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        createComponents();
        container.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                View view;
                for (int i = 0; i < mViews.size(); i++) {
                    view = mViews.get(i);
                    if ((view.getWidth() > 0) && (view.getHeight() > 0)) {
                        if (view instanceof Subwindow) {
                            ((Subwindow) view).setViewSizes(view.getWidth(), view.getHeight());
                        }
                    }
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mMainLock.lock();
                        try {
                            initializeComponents();
                        } finally {
                            mMainLock.unlock();
                        }
                    }
                }).start();
            }
        });
        return mRootLayout;
    }

    protected abstract void createComponents();

    protected abstract void initializeComponents();

    protected void addElement(View view) {
        mViews.add(view);
        ((IElement)view).bindWithCommonObjects(this);
    }

    protected void addElements(View... views) {
        for (int i=0; i<views.length; i++) {
            mViews.add(views[i]);
            ((IElement)views[i]).bindWithCommonObjects(this);
        }
    }

    @Override
    public void onDestroy() {
        mViews.clear();
        super.onDestroy();
    }

    /**
     * Initialization section of model
     *
     *
     */

    public Map<String, Double> variables;

    public String modelName="";

    public boolean isRemote = true;

    public String serverUrlParam ="";
    public String serverUrlMain ="";



    // public String serverUrlParam ="http://distolymp.spbu.ru/phys/olymp/p-model/param/";
    // public String serverUrlMain ="http://distolymp.spbu.ru/";

    @Override
    public void setServerUrl(String serverUrl) {

        try {
            this.serverUrlMain =serverUrl;
            this.serverUrlParam = serverUrl + "p-model/param/";
            Log.d("setservURL=",this.serverUrlParam);
        }catch (Exception e){
            Log.e("setServerUrl",e.toString());
            return;
        }
        if (isRemote) {new AsyncParamLoad().execute(this.serverUrlParam);}
    }

    public abstract void paramVariableSet();
    //Inlineclass: loading params from server
    public class AsyncParamLoad extends AsyncTask<String, Void, String> {

        final int CONNECTION_OK=200;
        @Override
        protected String doInBackground(String... urls) {
            Log.d("Start Async", "Yes");

            InputStream inputStream = null;

            HttpURLConnection urlConnection = null;

            String s="0";

            try {
                Log.d("Try block Async","OK");

                URL url = new URL(urls[0]);

                urlConnection = (HttpURLConnection) url.openConnection();
//TODO split after domain name

                //  String tmp[]=url..split("/",4);
                String urlString = url.toString();
                String tmp[]=urlString.split("/",4);

                urlConnection.setRequestProperty("Cookie",getCookieValue(tmp[2]));

                urlConnection.setRequestMethod("GET");

                int statusCode = urlConnection.getResponseCode();

                Log.d("Try if block Async","OK");

                if (statusCode==CONNECTION_OK){
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    s=readStream(inputStream);
                    Log.d("Try if block true Async","OK");
                }

                urlConnection.disconnect();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return s;
        }


        @Override
        protected void onPostExecute(String result) {

            parsParam(result);
            paramVariableSet();

            Log.d("Result= ", result);
        }

        private String readStream(InputStream is) {
            try {
                ByteArrayOutputStream bo = new ByteArrayOutputStream();
                int i = is.read();
                while(i != -1) {
                    bo.write(i);
                    i = is.read();
                }
                return bo.toString();
            } catch (IOException e) {
                return "";
            }
        }

        private void parsParam(String input){

            Document document = Jsoup.parse(input);

            Elements var = document.select("td");

            Log.d("var= ",var.text());

            int max = Integer.parseInt(var.last().text());

            variables = new LinkedHashMap<String, Double>();

            int k=0,l=1;
            try {
                for (int i = 0; i <= max - 1; i++) {
                    variables.put(var.get(k).text(), Double.parseDouble(var.get(l).text()));
                    k+=2;
                    l+=2;
                }
            }catch (NumberFormatException e){
                Log.d("for",e.toString());
            }
//begin debug
            StringBuilder stringBuilder = new StringBuilder();
            for( Map.Entry<String, Double> entry : variables.entrySet() ) {
                stringBuilder.append(entry.getKey());
                stringBuilder.append("=");
                stringBuilder.append(entry.getValue());
                stringBuilder.append("\n");
                Log.d("strb =",stringBuilder.toString());
            }
            Log.d("strbF =", stringBuilder.toString());
//end debug

        }

    }

    private static String getCookieValue(String siteName){
        String cookieValue = "";

        CookieManager cookieManager = CookieManager.getInstance();
        String cookies = cookieManager.getCookie(siteName);
        int i=cookies.lastIndexOf("=");
        cookieValue="PHPSESSID"+cookies.substring(i);

        Log.d("Cookie= ", cookieValue);

        return cookieValue;
    }


}
