package ru.spbu.barsic;

/**
 * Created by Мартынюк on 23.04.2016.
 */
public interface ISystem {

    void beep();

    //ICOMPort COM();

    String currentFolder();

    String currentWinFolder();

    String getDate();

    int getDateAsNumber();

    String[] getDriveNames();

    //String getGUID();

    String getTime();

    int getTimeAsNumber();

    long memoryUsed();

    //IPort Port();

    //String programGroup();

    //IRegistry registry();

    //IScreen screen();

    //void smd();

    //ISoundBlaster();

    ITimer timer();

    //IWindows windows();
}
