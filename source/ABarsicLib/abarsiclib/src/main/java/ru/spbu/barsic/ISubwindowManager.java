package ru.spbu.barsic;

public interface ISubwindowManager {

    double xMin();

    double xMax();

    double yMin();

    double yMax();

    int pixels(char coordinate, double value);

    double x(int pixels);

    double y(int pixels);

    void clipping(String value);

    void context(String value);

    void isParentBackColor(boolean value);

    //Bitmap getBitmap();

    //IMouse mouse();

    void clear();

    void update();

}
