/*
 * DefFunction.java
 */
package ru.spbu.abarsic.graphics.plot;

/**
 *
 * @author Анита
 */
public class DefFunction {

    /**
     * 
     */
    /**
     * 
     */
    /**
     * 
     */
    private double x, fun = 0.0, argfun = 0.0;
    String s;
    char t;

    /**
     * 
     * @param s
     * @param t
     */
    public DefFunction(String s, char t) {
        this.x = 0.0;
        this.s = s;
        this.t = t;
        try {
            defFunction();
        } catch (Exception e) {
            throw new IllegalArgumentException("Syntax error in the function expression");
        }
    }
    
    public double getValue(double x) {
        this.x = x;
        defFunction();
        return fun;
    }

    /**
     * 
     */
    public void defFunction() {
        int l = s.length();
        double[] f = new double[l];
        double[] wheref = new double[l];
        char[] buf = new char[l + 1];
        // делается на единицу больше чем длина строки, чтобы если на последнем месте было t то при проверки на tg не было бы ошибки
        s.getChars(0, l, buf, 0);

        // определение всех функций в выражении и их аргументов
        for (int i = 0; i < l; i++) {
            String z = "";


            if (buf[i] == '(') {
                wheref[i] = 2;
            }

            if (buf[i] == ')') {
                wheref[i] = 3;
            }
            // pi
            if (buf[i] == 'p') {
                if (buf[i + 1] == 'i') {
                    f[i] = Math.PI;
                    wheref[i] = 1;
                }
            }

            // ch
            if (buf[i] == 'c') {
                if (buf[i + 1] == 'h') {
                    if (buf[i + 2] == '(') {
                        int j = i + 3;
                        int bracketcount = 0;
                        while ((buf[j] != ')') || (bracketcount != 0)) {
                            if (buf[j] == '(') {
                                bracketcount = bracketcount + 1;
                            }
                            if (buf[j] == ')') {
                                bracketcount = bracketcount - 1;
                            }
                            j = j + 1;
                        }
                        defArgument(x, buf, i + 3, j - 1);
                        f[i] = Math.cosh(argfun);
                        wheref[i] = 1;
                        i = j;
                    }
                }
            }
            // tg
            if (buf[i] == 't') {
                if (buf[i + 1] == 'g') {
                    if (buf[i + 2] == '(') {
                        int j = i + 3;
                        int bracketcount = 0;
                        while ((buf[j] != ')') || (bracketcount != 0)) {
                            if (buf[j] == '(') {
                                bracketcount = bracketcount + 1;
                            }
                            if (buf[j] == ')') {
                                bracketcount = bracketcount - 1;
                            }
                            j = j + 1;
                        }
                        defArgument(x, buf, i + 3, j - 1);
                        f[i] = Math.tan(argfun);
                        wheref[i] = 1;
                        i = j;
                    }
                }
            }
            // cos
            if (buf[i] == 'c') {
                if (buf[i + 1] == 'o') {
                    if (buf[i + 2] == 's') {
                        if (buf[i + 3] == '(') {
                            int j = i + 4;

                            int bracketcount = 0;
                            while ((buf[j] != ')') || (bracketcount != 0)) {
                                if (buf[j] == '(') {
                                    bracketcount = bracketcount + 1;
                                }
                                if (buf[j] == ')') {
                                    bracketcount = bracketcount - 1;
                                }
                                j = j + 1;
                            }
                            defArgument(x, buf, i + 4, j - 1);
                            f[i] = Math.cos(argfun);
                            wheref[i] = 1;
                            i = j;
                        }
                    }
                }
            }
            // abs
            if (buf[i] == 'a') {
                if (buf[i + 1] == 'b') {
                    if (buf[i + 2] == 's') {
                        if (buf[i + 3] == '(') {
                            int j = i + 4;
                            int bracketcount = 0;
                            while ((buf[j] != ')') || (bracketcount != 0)) {
                                if (buf[j] == '(') {
                                    bracketcount = bracketcount + 1;
                                }
                                if (buf[j] == ')') {
                                    bracketcount = bracketcount - 1;
                                }
                                j = j + 1;
                            }
                            defArgument(x, buf, i + 4, j - 1);
                            f[i] = Math.abs(argfun);
                            wheref[i] = 1;
                            i = j;
                        }
                    }
                }
            }

            //exp
            if (buf[i] == 'e') {
                if (buf[i + 1] == 'x') {
                    if (buf[i + 2] == 'p') {
                        if (buf[i + 3] == '(') {
                            int j = i + 4;
                            int bracketcount = 0;
                            while ((buf[j] != ')') || (bracketcount != 0)) {
                                if (buf[j] == '(') {
                                    bracketcount = bracketcount + 1;
                                }
                                if (buf[j] == ')') {
                                    bracketcount = bracketcount - 1;
                                }
                                j = j + 1;
                            }
                            defArgument(x, buf, i + 4, j - 1);
                            f[i] = Math.exp(argfun);
                            wheref[i] = 1;
                            i = j;
                        }
                    }
                }
            }
            // sh
            if (buf[i] == 's') {
                if (buf[i + 1] == 'h') {
                    if (buf[i + 2] == '(') {

                        int j = i + 3;
                        int bracketcount = 0;
                        while ((buf[j] != ')') || (bracketcount != 0)) {
                            if (buf[j] == '(') {
                                bracketcount = bracketcount + 1;
                            }
                            if (buf[j] == ')') {
                                bracketcount = bracketcount - 1;
                            }
                            j = j + 1;
                        }
                        defArgument(x, buf, i + 3, j - 1);
                        f[i] = Math.sinh(argfun);
                        wheref[i] = 1;
                        i = j;
                    }
                }
            }

            // ln
            if (buf[i] == 'l') {
                if (buf[i + 1] == 'n') {
                    if (buf[i + 2] == '(') {

                        int j = i + 3;
                        int bracketcount = 0;
                        while ((buf[j] != ')') || (bracketcount != 0)) {
                            if (buf[j] == '(') {
                                bracketcount = bracketcount + 1;
                            }
                            if (buf[j] == ')') {
                                bracketcount = bracketcount - 1;
                            }
                            j = j + 1;
                        }
                        defArgument(x, buf, i + 3, j - 1);
                        f[i] = Math.log(argfun);
                        wheref[i] = 1;
                        i = j;
                    }
                }
            }

            // ln
            if (buf[i] == 'l') {
                if (buf[i + 1] == 'g') {
                    if (buf[i + 2] == '(') {
                        int j = i + 3;
                        int bracketcount = 0;
                        while ((buf[j] != ')') || (bracketcount != 0)) {
                            if (buf[j] == '(') {
                                bracketcount = bracketcount + 1;
                            }
                            if (buf[j] == ')') {
                                bracketcount = bracketcount - 1;
                            }
                            j = j + 1;
                        }
                        defArgument(x, buf, i + 3, j - 1);
                        f[i] = Math.log10(argfun);
                        wheref[i] = 1;
                        i = j;
                    }
                }
            }

            // sin
            if (buf[i] == 's') {
                if (buf[i + 1] == 'i') {
                    if (buf[i + 2] == 'n') {
                        if (buf[i + 3] == '(') {

                            int j = i + 4;
                            int bracketcount = 0;
                            while ((buf[j] != ')') || (bracketcount != 0)) {
                                if (buf[j] == '(') {
                                    bracketcount = bracketcount + 1;
                                }
                                if (buf[j] == ')') {
                                    bracketcount = bracketcount - 1;
                                }
                                j = j + 1;
                            }
                            defArgument(x, buf, i + 4, j - 1);
                            f[i] = Math.sin(argfun);
                            wheref[i] = 1;
                            i = j;
                        }
                    }
                }
            }
            // arcsin
            if (buf[i] == 'a') {
                if (buf[i + 1] == 'r') {
                    if (buf[i + 2] == 'c') {
                        if (buf[i + 3] == 's') {
                            if (buf[i + 4] == 'i') {
                                if (buf[i + 5] == 'n') {
                                    if (buf[i + 6] == '(') {

                                        int j = i + 7;
                                        int bracketcount = 0;
                                        while ((buf[j] != ')') || (bracketcount != 0)) {
                                            if (buf[j] == '(') {
                                                bracketcount = bracketcount + 1;
                                            }
                                            if (buf[j] == ')') {
                                                bracketcount = bracketcount - 1;
                                            }
                                            j = j + 1;
                                        }
                                        defArgument(x, buf, i + 7, j - 1);
                                        f[i] = Math.asin(argfun);
                                        wheref[i] = 1;
                                        i = j;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // arccos
            if (buf[i] == 'a') {
                if (buf[i + 1] == 'r') {
                    if (buf[i + 2] == 'c') {
                        if (buf[i + 3] == 'c') {
                            if (buf[i + 4] == 'o') {
                                if (buf[i + 5] == 's') {
                                    if (buf[i + 6] == '(') {

                                        int j = i + 7;
                                        int bracketcount = 0;
                                        while ((buf[j] != ')') || (bracketcount != 0)) {
                                            if (buf[j] == '(') {
                                                bracketcount = bracketcount + 1;
                                            }
                                            if (buf[j] == ')') {
                                                bracketcount = bracketcount - 1;
                                            }
                                            j = j + 1;
                                        }
                                        defArgument(x, buf, i + 7, j - 1);
                                        f[i] = Math.acos(argfun);
                                        wheref[i] = 1;
                                        i = j;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // arctg
            if (buf[i] == 'a') {
                if (buf[i + 1] == 'r') {
                    if (buf[i + 2] == 'c') {
                        if (buf[i + 3] == 't') {
                            if (buf[i + 4] == 'g') {
                                if (buf[i + 5] == '(') {

                                    int j = i + 6;
                                    int bracketcount = 0;
                                    while ((buf[j] != ')') || (bracketcount != 0)) {
                                        if (buf[j] == '(') {
                                            bracketcount = bracketcount + 1;
                                        }
                                        if (buf[j] == ')') {
                                            bracketcount = bracketcount - 1;
                                        }
                                        j = j + 1;
                                    }
                                    defArgument(x, buf, i + 6, j - 1);
                                    f[i] = Math.atan(argfun);
                                    wheref[i] = 1;
                                    i = j;
                                }
                            }
                        }
                    }
                }
            }
            // number
            if ((buf[i] == '1') || (buf[i] == '2') || (buf[i] == '3') || (buf[i] == '4') || (buf[i] == '5')
                    || (buf[i] == '6') || (buf[i] == '7') || (buf[i] == '8') || (buf[i] == '9') || (buf[i] == '0')) {
                int k = i;

                while ((k < l) && ((buf[k] == '1') || (buf[k] == '2') || (buf[k] == '3') || (buf[k] == '4') || (buf[k] == '5')
                        || (buf[k] == '6') || (buf[k] == '7') || (buf[k] == '8') || (buf[k] == '9') || (buf[k] == '0') || (buf[k] == '.') || (buf[k] == ','))) {
                    z = z + buf[k];
                    k = k + 1;
                }
                f[i] = Double.parseDouble(z);
                wheref[i] = 1;
                i = k - 1;

            }
            // x
            if (buf[i] == t) {
                f[i] = x;
                wheref[i] = 1;
            }

        }
        // нахождение всех скобок которые не являются скобками аргументов

// Раскрывает все скобки (самые внешние)
        for (int i = l - 1; i >= 0; i--) {
            if (wheref[i] == 2) {
                int k = i + 1;
                while (wheref[k] != 3) {
                    k = k + 1;
                }
                defOperation(buf, wheref, f, i + 1, k - 1);
                wheref[i] = 0;
                wheref[k] = 0;
            }
        }
// После того как убрали все скобки просто *,+,-,/ оставшиеся функции
        fun = 0;
        defOperation(buf, wheref, f, 0, l - 1);
        for (int i = 0; i < l; i++) {
            if (wheref[i] == 1) {
                fun = f[i];
            }

        }
    }

    private void defOperation(char[] buf, double[] wheref, double[] f, int p, int q) { //числа p,q введены для того чтобы контролировать цикл который проверяет где находятся знаки, это надо для того чтобы можно было применить эту же функции в том месте где определяется аргумент

        // первая прогонка для того что возвести в степени если они есть
        for (int i = 0; i < q - p + 1; i++) {
            if (buf[i + p] == '^') {
                int k = i;
                int j = i;
                while (wheref[k + p] == 0) {
                    k = k - 1;
                }
                while (wheref[j + p] == 0) {
                    j = j + 1;
                }
                // извлечение кубического корня
                if (f[j + p] == 0.3333333333333333) {
                    f[i + p] = Math.cbrt(f[k + p]);
                    wheref[i + p] = 1;
                    wheref[j + p] = 0;
                    wheref[k + p] = 0;
                    buf[i + p] = ' ';
                    i = j;
                }
                // извлечение квадратного корня
                if (f[j + p] == 0.5) {
                    f[i + p] = Math.sqrt(f[k + p]);
                    wheref[i + p] = 1;
                    wheref[j + p] = 0;
                    wheref[k + p] = 0;
                    buf[i + p] = ' ';
                    i = j;
                }
                // возведение в квадрат
                if (f[j + p] == 2) {
                    f[i + p] = f[k + p] * f[k + p];
                    wheref[i + p] = 1;
                    wheref[j + p] = 0;
                    wheref[k + p] = 0;
                    buf[i + p] = ' ';
                    i = j;
                } else {
                    // возведение в любую другую степень
                    f[i + p] = Math.pow(f[k + p], f[j + p]);
                    wheref[i + p] = 1;
                    wheref[j + p] = 0;
                    wheref[k + p] = 0;
                    buf[i + p] = ' ';
                    i = j;
                }
            }
        }
        // вторая прогонка того что в скобке чтобы сделать все умножение и деление
        for (int i = 0; i < q - p + 1; i++) {
            // умножение
            if (buf[i + p] == '*') {
                int k = i;
                int j = i;
                while (wheref[k + p] == 0) {
                    k = k - 1;
                }
                while (wheref[j + p] == 0) {
                    j = j + 1;
                }
                f[i + p] = f[j + p] * f[k + p];
                wheref[i + p] = 1;
                wheref[j + p] = 0;
                wheref[k + p] = 0;
                buf[i + p] = ' ';
                i = j;
            }
            // деление
            if (buf[i + p] == '/') {
                int k = i;
                int j = i;
                while (wheref[k + p] == 0) {
                    k = k - 1;
                }
                while (wheref[j + p] == 0) {
                    j = j + 1;
                }
                f[i + p] = f[k + p] / f[j + p];
                wheref[i + p] = 1;
                wheref[j + p] = 0;
                wheref[k + p] = 0;
                buf[i + p] = ' ';
                i = j;
            }
        }
        // вторая прогонка того что в скобках, чтобы сделать сложение и вычитание
        for (int i = 0; i < q - p + 1; i++) {
            // +
            if (buf[i + p] == '+') {
                int k = i;
                int j = i;
                while (wheref[k + p] != 1) {
                    k = k - 1;
                }
                while (wheref[j + p] != 1) {
                    j = j + 1;
                }

                f[i + p] = f[j + p] + f[k + p];
                wheref[i + p] = 1;
                wheref[j + p] = 0;
                wheref[k + p] = 0;
                buf[i + p] = ' '; // для того чтобы лишний раз не отвлекаться на этот знак, 
                //потому что раз мы один раз его использовали он нам больше не пригодится 
                i = j;
            }

            // -
            if (buf[i + p] == '-') {
                int k = i;
                int j = i;
                int o = 0; // параметр о помогает со случаем когда перед "-" нет никакой функции
                while ((o == 0) && (wheref[k + p] == 0)) {
                    if ((k == 0) && (wheref[p] == 0)) {
                        o = 1;
                        k = k + 1;
                    }
                    k = k - 1;
                }
                while (wheref[j + p] == 0) {
                    j = j + 1;
                }
                f[i + p] = f[k + p] - f[j + p];
                wheref[k + p] = 0;
                wheref[i + p] = 1;
                wheref[j + p] = 0;
                buf[i + p] = ' ';
                i = j;
            }
        }
    }

    private void defArgument(double x, char[] buf, int p, int q) {

        int l = buf.length - 1;
        double[] argf = new double[l]; // Оба массива создаются избыточной длины чтобы все было согласовано с фун defOperation
        double[] IndicatorArgf = new double[l];


        for (int m = 0; m < q - p + 1; m++) {
            String z = "";

            // нахождение скобок внутри аргумента функции
            if (buf[m + p] == '(') {
                IndicatorArgf[m + p] = 2;
            }

            if (buf[m + p] == ')') {
                IndicatorArgf[m + p] = 3;
            }


            // pi in argument
            if (buf[m + p] == 'p') {
                if (buf[m + p + 1] == 'i') {
                    argf[m + p] = Math.PI;
                    IndicatorArgf[m + p] = 1;
                }
            }

            // проверка наличия числа в аргументе    
            if ((buf[m + p] == '1') || (buf[m + p] == '2') || (buf[m + p] == '3') || (buf[m + p] == '4') || (buf[m + p] == '5')
                    || (buf[m + p] == '6') || (buf[m + p] == '7') || (buf[m + p] == '8') || (buf[m + p] == '9') || (buf[m + p] == '0')) {

                int k = m + p;

                while ((k < q + 1) && ((buf[k] == '1') || (buf[k] == '2') || (buf[k] == '3') || (buf[k] == '4') || (buf[k] == '5')
                        || (buf[k] == '6') || (buf[k] == '7') || (buf[k] == '8') || (buf[k] == '9') || (buf[k] == '0') || (buf[k] == '.') || (buf[k] == ','))) {
                    z = z + buf[k];
                    k = k + 1;
                }
                argf[m + p] = Double.parseDouble(z);
                IndicatorArgf[m + p] = 1;
                m = k - p - 1;
            }

            // sin in argument
            if (buf[m + p] == 's') {
                if (buf[m + p + 1] == 'i') {
                    if (buf[m + p + 2] == 'n') {
                        if (buf[m + p + 3] == '(') {

                            int j = m + p + 4;
                            int bracketcount = 0;
                            while ((buf[j] != ')') || (bracketcount != 0)) {
                                if (buf[j] == '(') {
                                    bracketcount = bracketcount + 1;
                                }
                                if (buf[j] == ')') {
                                    bracketcount = bracketcount - 1;
                                }
                                j = j + 1;
                            }
                            defArgument(x, buf, m + p + 4, j - 1);
                            argf[m + p] = Math.sin(argfun);
                            IndicatorArgf[m + p] = 1;
                            m = j - p;
                        }
                    }
                }
            }

            // sh in argument
            if (buf[m + p] == 's') {
                if (buf[m + p + 1] == 'h') {
                    if (buf[m + p + 2] == '(') {

                        int j = m + p + 3;
                        int bracketcount = 0;
                        while ((buf[j] != ')') || (bracketcount != 0)) {
                            if (buf[j] == '(') {
                                bracketcount = bracketcount + 1;
                            }
                            if (buf[j] == ')') {
                                bracketcount = bracketcount - 1;
                            }
                            j = j + 1;
                        }
                        defArgument(x, buf, m + p + 3, j - 1);
                        argf[m + p] = Math.sinh(argfun);
                        IndicatorArgf[m + p] = 1;
                        m = j - p;
                    }
                }
            }

            // ln in argument
            if (buf[m + p] == 'l') {
                if (buf[m + p + 1] == 'n') {
                    if (buf[m + p + 2] == '(') {

                        int j = m + p + 3;
                        int bracketcount = 0;
                        while ((buf[j] != ')') || (bracketcount != 0)) {
                            if (buf[j] == '(') {
                                bracketcount = bracketcount + 1;
                            }
                            if (buf[j] == ')') {
                                bracketcount = bracketcount - 1;
                            }
                            j = j + 1;
                        }
                        defArgument(x, buf, m + p + 3, j - 1);
                        argf[m + p] = Math.log(argfun);
                        IndicatorArgf[m + p] = 1;
                        m = j - p;
                    }
                }
            }

            // lg in argument
            if (buf[m + p] == 'l') {
                if (buf[m + p + 1] == 'g') {
                    if (buf[m + p + 2] == '(') {

                        int j = m + p + 3;
                        int bracketcount = 0;
                        while ((buf[j] != ')') || (bracketcount != 0)) {
                            if (buf[j] == '(') {
                                bracketcount = bracketcount + 1;
                            }
                            if (buf[j] == ')') {
                                bracketcount = bracketcount - 1;
                            }
                            j = j + 1;
                        }
                        defArgument(x, buf, m + p + 3, j - 1);
                        argf[m + p] = Math.log10(argfun);
                        IndicatorArgf[m + p] = 1;
                        m = j - p;
                    }
                }
            }

            // tg in argument
            if (buf[m + p] == 't') {
                if (buf[m + p + 1] == 'g') {
                    if (buf[m + p + 2] == '(') {

                        int j = m + p + 3;
                        int bracketcount = 0;
                        while ((buf[j] != ')') || (bracketcount != 0)) {
                            if (buf[j] == '(') {
                                bracketcount = bracketcount + 1;
                            }
                            if (buf[j] == ')') {
                                bracketcount = bracketcount - 1;
                            }
                            j = j + 1;
                        }
                        defArgument(x, buf, m + p + 3, j - 1);
                        argf[m + p] = Math.tan(argfun);
                        IndicatorArgf[m + p] = 1;
                        m = j - p;
                    }
                }
            }

            // exp in argument
            if (buf[m + p] == 'e') {
                if (buf[m + p + 1] == 'x') {
                    if (buf[m + p + 2] == 'p') {
                        if (buf[m + p + 3] == '(') {
                            int j = m + p + 4;
                            int bracketcount = 0;
                            while ((buf[j] != ')') || (bracketcount != 0)) {
                                if (buf[j] == '(') {
                                    bracketcount = bracketcount + 1;
                                }
                                if (buf[j] == ')') {
                                    bracketcount = bracketcount - 1;
                                }
                                j = j + 1;
                            }
                            defArgument(x, buf, m + p + 4, j - 1);
                            argf[m + p] = Math.exp(argfun);
                            IndicatorArgf[m + p] = 1;
                            m = j - p;
                        }
                    }
                }
            }
            // abs in argument
            if (buf[m + p] == 'a') {
                if (buf[m + p + 1] == 'b') {
                    if (buf[m + p + 2] == 's') {
                        if (buf[m + p + 3] == '(') {
                            int j = m + p + 4;
                            int bracketcount = 0;
                            while ((buf[j] != ')') || (bracketcount != 0)) {
                                if (buf[j] == '(') {
                                    bracketcount = bracketcount + 1;
                                }
                                if (buf[j] == ')') {
                                    bracketcount = bracketcount - 1;
                                }
                                j = j + 1;
                            }
                            defArgument(x, buf, m + p + 4, j - 1);
                            argf[m + p] = Math.abs(argfun);
                            IndicatorArgf[m + p] = 1;
                            m = j - p;
                        }
                    }
                }
            }
            // ch in argument
            if (buf[m + p] == 'c') {
                if (buf[m + p + 1] == 'h') {
                    if (buf[m + p + 2] == '(') {
                        int j = m + p + 3;
                        int bracketcount = 0;
                        while ((buf[j] != ')') || (bracketcount != 0)) {
                            if (buf[j] == '(') {
                                bracketcount = bracketcount + 1;
                            }
                            if (buf[j] == ')') {
                                bracketcount = bracketcount - 1;
                            }
                            j = j + 1;
                        }
                        defArgument(x, buf, m + p + 3, j - 1);
                        argf[m + p] = Math.cosh(argfun);
                        IndicatorArgf[m + p] = 1;
                        m = j - p;
                    }
                }
            }
            //cos in argument
            if (buf[m + p] == 'c') {
                if (buf[m + p + 1] == 'o') {
                    if (buf[m + p + 2] == 's') {
                        if (buf[m + p + 3] == '(') {
                            int j = m + p + 4;
                            int bracketcount = 0;
                            while ((buf[j] != ')') || (bracketcount != 0)) {
                                if (buf[j] == '(') {
                                    bracketcount = bracketcount + 1;
                                }
                                if (buf[j] == ')') {
                                    bracketcount = bracketcount - 1;
                                }
                                j = j + 1;
                            }
                            defArgument(x, buf, m + p + 4, j - 1);
                            argf[m + p] = Math.cos(argfun);
                            IndicatorArgf[m + p] = 1;
                            m = j - p;
                        }
                    }
                }
            }
            // arcsin in argument
            if (buf[m + p] == 'a') {
                if (buf[m + p + 1] == 'r') {
                    if (buf[m + p + 2] == 'c') {
                        if (buf[m + p + 3] == 's') {
                            if (buf[m + p + 4] == 'i') {
                                if (buf[m + p + 5] == 'n') {
                                    if (buf[m + p + 6] == '(') {

                                        int j = m + p + 7;
                                        int bracketcount = 0;
                                        while ((buf[j] != ')') || (bracketcount != 0)) {
                                            if (buf[j] == '(') {
                                                bracketcount = bracketcount + 1;
                                            }
                                            if (buf[j] == ')') {
                                                bracketcount = bracketcount - 1;
                                            }
                                            j = j + 1;
                                        }
                                        defArgument(x, buf, m + p + 7, j - 1);
                                        argf[m + p] = Math.asin(argfun);
                                        IndicatorArgf[m + p] = 1;
                                        m = j - p;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // arccos in argument
            if (buf[m + p] == 'a') {
                if (buf[m + p + 1] == 'r') {
                    if (buf[m + p + 2] == 'c') {
                        if (buf[m + p + 3] == 'c') {
                            if (buf[m + p + 4] == 'o') {
                                if (buf[m + p + 5] == 's') {
                                    if (buf[m + p + 6] == '(') {

                                        int j = m + p + 7;
                                        int bracketcount = 0;
                                        while ((buf[j] != ')') || (bracketcount != 0)) {
                                            if (buf[j] == '(') {
                                                bracketcount = bracketcount + 1;
                                            }
                                            if (buf[j] == ')') {
                                                bracketcount = bracketcount - 1;
                                            }
                                            j = j + 1;
                                        }
                                        defArgument(x, buf, m + p + 7, j - 1);
                                        argf[m + p] = Math.acos(argfun);
                                        IndicatorArgf[m + p] = 1;
                                        m = j - p;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // arctg in argument
            if (buf[m + p] == 'a') {
                if (buf[m + p + 1] == 'r') {
                    if (buf[m + p + 2] == 'c') {
                        if (buf[m + p + 3] == 't') {
                            if (buf[m + p + 4] == 'g') {
                                if (buf[m + p + 5] == '(') {

                                    int j = m + p + 6;
                                    int bracketcount = 0;
                                    while ((buf[j] != ')') || (bracketcount != 0)) {
                                        if (buf[j] == '(') {
                                            bracketcount = bracketcount + 1;
                                        }
                                        if (buf[j] == ')') {
                                            bracketcount = bracketcount - 1;
                                        }
                                        j = j + 1;
                                    }
                                    defArgument(x, buf, m + p + 6, j - 1);
                                    argf[m + p] = Math.atan(argfun);
                                    IndicatorArgf[m + p] = 1;
                                    m = j - p;
                                }
                            }
                        }
                    }
                }
            }

            // x in argument
            if (buf[m + p] == t) {
                argf[m + p] = x;
                IndicatorArgf[m + p] = 1;
            }
        }

        argfun = 0;

        for (int m = q - p; m >= 0; m--) {
            if (IndicatorArgf[m + p] == 2) {
                int k = m + 1;
                while (IndicatorArgf[k + p] != 3) {
                    k = k + 1;
                }
                defOperation(buf, IndicatorArgf, argf, m + p + 1, k - 1);
                IndicatorArgf[p + m] = 0;
                IndicatorArgf[k + p] = 0;
            }
        }

        defOperation(buf, IndicatorArgf, argf, p, q);
        for (int m = 0; m < q - p + 1; m++) {
            if (IndicatorArgf[m + p] != 0) {
                argfun = argf[m + p];
            }
        }

    }
}
