package ru.spbu.barsic;

public interface IPanel extends IElement {
    //todo:WTF? angle?
    void setBevelInner(String lowered);

    void setIsParentFont(boolean value);

    void setBevelOuter(String raised);

    void setIsParentBackColor(boolean value);
}