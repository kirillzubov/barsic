package ru.spbu.abarsic;

import ru.spbu.barsic.ITextArea;

public class TextArea implements ITextArea {

    @Override
    public int width() {
        return 0;
    }

    @Override
    public void width(int value) {

    }

    @Override
    public int height() {
        return 0;
    }

    @Override
    public void height(int value) {

    }

    @Override
    public void setAlign(String value) {

    }

    @Override
    public void setOnHelp(final Runnable eventHandler) {

    }

    @Override
    public void bindWithCommonObjects(IModel model) {

    }

    @Override
    public void setName(String value) {

    }

    @Override
    public void setLeftPos(int value) {

    }

    @Override
    public void setTopPos(int value) {

    }

    @Override
    public void setHint(String value) {

    }

    @Override
    public void setIsEnabled(boolean value) {

    }

    @Override
    public void setIsVisible(boolean value) {

    }

    @Override
    public void setText(String value) {

    }

    @Override
    public void setIsParentBackColor(boolean value) {

    }

    @Override
    public void setIsScrollBars(boolean value) {

    }

    @Override
    public void setIsSourceCode(boolean value) {

    }

    @Override
    public void setIsWordWrap(boolean value) {

    }

    @Override
    public void setIsReadOnly(boolean value) {

    }

    @Override
    public void setIsParentFont(boolean value) {

    }

    @Override
    public void setAssociatedVariable(String value) {

    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {

    }

}