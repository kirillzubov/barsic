package ru.spbu.barsic;

/**
 * Created by Мартынюк on 19.03.2016.
 */
public interface IGroup {

    public void output();

    public void endOutput();

    public int minIndex();

    public int maxIndex();

    public int count();

    public void delete();

    public boolean hasIndex();

    public void moveBy(double deltaX, double deltaY);

}
