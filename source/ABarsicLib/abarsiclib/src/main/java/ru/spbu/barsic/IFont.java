package ru.spbu.barsic;

public interface IFont {
    
    /*
    "name"
    "script"
    */
    
    public String color();
    
    public void color(String value);
    
    public String face();
    
    public void face(String value);
    
    public double height();
    
    public void height(double value);
    
    public boolean isBold();
    
    public void bold(boolean value);
    
    public boolean isItalic();
    
    public void italic(boolean value);
    
    public boolean isStrikeout();
    
    public void strikeout(boolean value);
    
    public boolean isUnderline();
    
    public void underline(boolean value);
    
}
