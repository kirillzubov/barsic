/*
 * Segment.java
 */

package ru.spbu.abarsic.graphics.plot;

/**
 *
 * @author Max
 */
public class Range {
    /**
     * 
     * @param left
     * @param right
     */
    public Range(double left, double right) {
        this.left = left;
        this.right = right;
    }

    private double top = 0;

    /**
     * 
     * @return
     */
    public double getTop() {
        return this.top;
    }

    /**
     * 
     * @param value
     */
    public void setTop(double value) {
        this.top = value;
    }
    //
    private double left = 0;

    /**
     * 
     * @return
     */
    public double getLeft() {
        return this.left;
    }

    /**
     * 
     * @param value
     */
    public void setLeft(double value) {
        this.left = value;
    }
    //
    private double right = 0;

    /**
     * 
     * @return
     */
    public double getRight() {
        return this.right;
    }

    /**
     * 
     * @param value
     */
    public void setRight(double value) {
        this.right = value;
    }

    /// <remarks>without absolute check</remarks>
    /**
     * 
     * @return
     */
    public double getWidth() {
        return right - left;
    }

    /**
     * 
     * @param dx
     */
    public void translateTransform(double dx) {
        left += dx;
        right += dx;
    }
    

    @Override
    public String toString()
    {
        return String.format("|(%f, %f)|=%f", left,right,this.getWidth());
    }
}
