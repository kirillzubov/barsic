/*
 * GeomUtils.java
 */

package ru.spbu.abarsic.graphics.plot;

/**
 *
 * @author Max
 */

/**
 * Porting to Android performed by Martynyuk
 */

import ru.spbu.abarsic.graphics.awtandroid.geom.Rectangle2D;
import ru.spbu.abarsic.graphics.awtandroid.geom.Point2D;

/**
 * 
 * @author Анита
 */
public class GeomUtils {
    /// <summary>
        /// Checks is the point p in the rect
        /// </summary>
    /**
     * 
     * @param p
     * @param rect
     * @return
     */
    public static boolean isInBox(Point2D p, Rectangle2D rect)
        {
            return (p.getX() >= rect.getMinX()) && (p.getX() <= rect.getMaxX()) && (p.getY() >= rect.getMinY()) && (p.getY() <= rect.getMaxY());
        }

        /// <summary>
        /// Returns rectangle which center is the center point
        /// </summary>
        /**
         * 
         * @param center
         * @param rect
         * @return
         */
        public static Rectangle2D MoveRectOnCenter(Point2D center, Rectangle2D rect)
        {
            return new Rectangle2D(center.getX() - rect.getWidth() * 0.5, center.getY() - rect.getHeight() * 0.5, rect.getWidth(), rect.getHeight());
        }

}
