package ru.spbu.abarsic.graphics.graph;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import static ru.spbu.abarsic.graphics.CartTransform.*;

/**
 * Created by Мартынюк on 17.04.2015.
 */
public final class Circle extends GraphicPrimitive {

    double r;

    private RectF boundRect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);

    public Circle(double xCenter, double yCenter, double radius, int fillColor, int strokeColor, double strokeWidth) {
        super(xCenter, yCenter, fillColor, strokeColor, strokeWidth);
        this.r = radius;
    }

    @Override
    public void draw(Canvas c, Paint fillPaint, Paint strokePaint, double[] coeffs) {
        boundRect.set((float)(coeffs[AX]*(x0-r)+coeffs[BX]), (float)(coeffs[AY]*(y0+r)+coeffs[BY]),
                      (float)(coeffs[AX]*(x0+r)+coeffs[BX]), (float)(coeffs[AY]*(y0-r)+coeffs[BY]));
        setFillPaint(fillPaint);
        c.drawOval(boundRect, fillPaint);
        setStrokePaint(strokePaint, coeffs[AX]);
        c.drawOval(boundRect, strokePaint);
    }
}
