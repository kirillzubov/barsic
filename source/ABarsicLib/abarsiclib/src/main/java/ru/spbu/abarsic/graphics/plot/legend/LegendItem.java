/*
 * LegendItem.java
 */

package ru.spbu.abarsic.graphics.plot.legend;

/**
 *
 * @author Max
 */

/**
 * Porting to Android performed by Martynyuk
 */

import ru.spbu.abarsic.graphics.plot.curve.CurveStyle;

/**
 * 
 * @author Анита
 */
public class LegendItem {
    /**
     * 
     * @param curveStyle
     * @param title
     */
    public LegendItem(CurveStyle curveStyle,String title)
    {
        this.curveStyle=curveStyle;
        this.title=title;
    }

    private ru.spbu.abarsic.graphics.plot.curve.CurveStyle curveStyle=new CurveStyle();

    /**
     * 
     * @return
     */
    public CurveStyle getCurveStyle()
    {
        return curveStyle;
    }

    /**
     * 
     * @param value
     */
    public void setCurveStyle(CurveStyle value)
    {
        curveStyle=value;
    }

    private String title="not set";

    /**
     * 
     * @return
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * 
     * @param value
     */
    public void setTitle(String value)
    {
        title=value;
    }

    
}

/*
 * switch (Style.getLineStyle()) {
            case SOLID:
                g.setStroke(new BasicStroke((float) Style.getLineWidth()));
                break;
            case DASH:
                float dash[] = {10.0f};
                g.setStroke(new BasicStroke((float) Style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));
                break;
            case DASHDOT:
                float dashdot[] = {10.0f, 9.0f, 2.0f, 9.0f};
                g.setStroke(new BasicStroke((float) Style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dashdot, 0.0f));
                break;
            case DOT:
                float dot[] = {2.0f};
                g.setStroke(new BasicStroke((float) Style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dot, 0.0f));
                break;
        }*/