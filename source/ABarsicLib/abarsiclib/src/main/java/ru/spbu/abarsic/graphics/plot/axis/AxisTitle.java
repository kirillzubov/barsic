/*
 * AxisTitle.java 
 */

/**
 * @author Martynyuk
 */

package ru.spbu.abarsic.graphics.plot.axis;

import android.graphics.Canvas;
import android.graphics.Paint;

public class AxisTitle {

    String sTitle;
    int base;
    String sBase;
    int exponent;
    String sExponent;
    String sUnits;
    
    public AxisTitle(String title, int exponent, String units) {
        super();
        this.base = 10;
        this.exponent = exponent;
        if (exponent != 0) {
            this.sBase = String.valueOf(base);
            this.sExponent = String.valueOf(exponent);
            this.sTitle = title+", ";
            if (!units.equals("")) {
                this.sUnits = " "+units;
            } else {
                this.sUnits = units;
            }
        } else {
            this.sBase = "";
            this.sExponent = "";
            this.sUnits = units;
            if (!units.equals("")) {
                this.sTitle = title+", ";
            } else {
                this.sTitle = title;
            }
        }
    }
    
    public AxisTitle(String title, String units) {
        this(title, 0, units);
    }
    
    public AxisTitle(String title) {
        this(title, "");
    }
    
    public AxisTitle() {
        this("");
    }
    
    public void draw(Canvas g, Paint p, float x, float y) {
        
        Paint pSup = new Paint(Paint.ANTI_ALIAS_FLAG);
        pSup.set(p);
        pSup.setTextSize(p.getTextSize()*0.7f);
        
        float textWidth = 0f;
        g.drawText(sTitle+sBase, x+textWidth, y, p);
        textWidth += p.measureText(sTitle+sBase);
        g.drawText(sExponent, x+textWidth, y-p.getTextSize()*0.4f, pSup);
        textWidth += pSup.measureText(sExponent);
        g.drawText(sUnits, x+textWidth, y, p);

    }
    
    public float measureWidth(Paint p) {
        
        Paint pSup = new Paint(Paint.ANTI_ALIAS_FLAG);
        pSup.set(p);
        pSup.setTextSize(p.getTextSize()*0.7f);

        return p.measureText(sTitle+sBase)+
               pSup.measureText(sExponent)+
               p.measureText(sUnits);
    }
    
}
