/*
 * PointListAr.java
 */

package ru.spbu.abarsic.graphics.plot.points;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.LinkedList;
/*
 * Point repository
 */

/**
 *
 * @author Max
 * changed by Anita
 */
public class PointListAr {

    /**
     * Array of coordinate x of point
     */
    public double[] x;
    /**
     * Array of coordinate y of point
     */
    public double[] y;
    /**
     * number of curve
     */
    public int numCurve;
    /**
     * number of point in the curve
     */
    public int numN;
    //    /**
//     * array of x of points
//     */
//    public double[][] Xi = new double[0][0];
//    /**
//     * array of y of points
//     */
//    public double[][] Yi = new double[0][0];
    public LinkedList<LinkedList<Double>> X = new LinkedList<LinkedList<Double>>();
    public LinkedList<LinkedList<Double>> Y = new LinkedList<LinkedList<Double>>();
    /**
     * array of style of every points
     */
//    public PointStyle[][] ps = new PointStyle[0][0];
    public LinkedList<LinkedList<PointStyle>> ps = new LinkedList<LinkedList<PointStyle>>();
    /**
     *
     */
    public PointStyle[] pointStyle;
    /**
     * count of curves
     */
//    public int k;
    /**
     * responsibles for the appearance of parts of the curves
     */
    public boolean needCalcDataArea = false;
    private PointStyle pointstyle = new PointStyle();

    /**
     * creats Array of points
     */
    public PointListAr() {
    }

    /**
     * creats LinkedList of points
     * @param x
     * @param y
     */
    public PointListAr(java.util.LinkedList<Double> x, java.util.LinkedList<Double> y) {
        int len = x.size();
        this.x = new double[len];
        this.y = new double[len];

//        if(len!=y.size())throw new Exception("The lengths of x and y lists must be equal");

        for (int i = 0; i < len; i++) {
            this.x[i] = x.get(i);
            this.y[i] = y.get(i);
        }
    }

    /**
     * creats Array of points
     * @param x
     * @param y
     */
    public PointListAr(double[] x, double[] y) {
        int len = x.length;
        this.x = new double[len];
        this.y = new double[len];
        for (int i = 0; i < len; i++) {
            this.x[i] = x[i];
            this.y[i] = y[i];
        }
    }
    final double EPSILON = 1E-100;

    /**
     * returns size of x
     * @return
     */
    public int getSize() {
        if (x != null) {
            return x.length;
        } else {
            return 0;
        }
    }

    /**
     * sorts Array
     */
    public void sort() {
        int len = x.length;
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < i; j++) {
                if (x[i] < x[j]) {
                    double buf = x[i];
                    x[i] = x[j];
                    x[j] = buf;

                    buf = y[i];
                    y[i] = y[j];
                    y[j] = buf;
                }
            }
        }
    }

    /**
     * adds new point to Arrays of the curves
     * @param x
     * @param y
     * @param numCurve- number of Curve
     * @param numN-number of point in the curve
     * @param pointstyle-style of new point
     * @author Anita
     */
//    public void addNewPointTo(double x, double y, int numCurve, int numN, PointStyle pointstyle) {
//        this.needCalcDataArea = true;
//        this.numN = numN;
//        this.numCurve = numCurve;
//
//        if (this.numN == 0) {
//            if (this.numCurve == 0) {
//                this.Xi = new double[1][1];
//                this.Yi = new double[1][1];
//                this.Xi[this.numCurve][this.numN] = x;
//                this.Yi[this.numCurve][this.numN] = y;
//                this.ps = new PointStyle[1][1];
//                this.ps[this.numCurve][this.numN] = pointstyle;
//            } else {
//                double[] oldX = new double[this.numCurve];
//                double[] oldY = new double[this.numCurve];
//                PointStyle[] oldPS = new PointStyle[this.numCurve];
//                for (int i = 0; i < this.numCurve; i++) {
//                    oldX[i] = this.Xi[i][0];
//                    oldY[i] = this.Yi[i][0];
//                    oldPS[i] = this.ps[i][0];
//                }
//
//                this.Xi = new double[this.numCurve + 1][1];
//                this.Yi = new double[this.numCurve + 1][1];
//                this.ps = new PointStyle[this.numCurve + 1][1];
//                for (int i = 0; i < this.numCurve; i++) {
//                    this.Xi[i][0] = oldX[i];
//                    this.Yi[i][0] = oldY[i];
//                    this.ps[i][0] = oldPS[i];
//                }
//                this.Xi[this.numCurve][0] = x;
//                this.Yi[this.numCurve][0] = y;
//                this.ps[this.numCurve][0] = pointstyle;
//            }
//        } else {
//
//            double[] oldX = this.Xi[this.numCurve];
//            double[] oldY = this.Yi[this.numCurve];
//            PointStyle[] oldPS = this.ps[this.numCurve];
//            this.Xi[this.numCurve] = Arrays.copyOf(oldX, this.numN + 1);
//            this.Yi[this.numCurve] = Arrays.copyOf(oldY, this.numN + 1);
//            this.ps[this.numCurve] = Arrays.copyOf(oldPS, this.numN + 1);
//            this.Xi[this.numCurve][this.numN] = x;
//            this.Yi[this.numCurve][this.numN] = y;
//            this.ps[this.numCurve][this.numN] = pointstyle;
//
//        }
//        this.k = this.numCurve + 1;
//    }


    public void addNewPointTo(double x, double y, int numCurve, int numN, PointStyle pointstyle) {
        this.needCalcDataArea = true;
        this.numN = numN;
        this.numCurve = numCurve;
        LinkedList <Double> mx= new LinkedList();
        LinkedList <Double> my= new LinkedList();
        LinkedList <PointStyle> mps= new LinkedList();
        if((this.numN==0)){
            mx.add(numN,x);
            this.X.add(numCurve, mx);
            my.add(numN,y);
            this.Y.add(numCurve, my);
            mps.add(numN,pointstyle);
            this.ps.add(numCurve, mps);
        } else {
            this.X.get(numCurve).add(numN,x);
            this.Y.get(numCurve).add(numN,y);
            this.ps.get(numCurve).add(numN,pointstyle);
        }

    }

    /**
     * adds new point to Arrays of the curves,style of point by default
     * @param x
     * @param y
     * @param numN
     * @param numCurve
     * @author Anita
     */
    public void addNewPointTo(double x, double y, int numCurve, int numN) {
        addNewPointTo(x, y, numCurve, numN, pointstyle);
    }

    /**
     * adds new point to Arrays of the curve, if count of curves is one, style of point by default
     * @param x
     * @param y
     * @param numN-number of Curve
     * @author Anita
     */
    public void addNewPointTo(double x, double y, int numN) {

        addNewPointTo(x, y, 0, numN);
    }

    /**
     * adds new point to Arrays of the curve, if count of curves is one
     * @param x
     * @param y
     * @param numN-number of Curve
     * @param pointstyle-style of new point
     * @author Anita
     */
    public void addNewPointTo(double x, double y, int numN, PointStyle pointstyle) {
        addNewPointTo(x, y, 0, numN, pointstyle);
    }
}
