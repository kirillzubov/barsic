package ru.spbu.abarsic.graphics.graph;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import static ru.spbu.abarsic.graphics.CartTransform.*;

/**
 * Created by Мартынюк on 24.02.2016.
 */
public final class RoundRectangle extends GraphicPrimitive {

    double w;
    double h;
    double rx;
    double ry;

    private RectF rect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);

    public RoundRectangle(double xCenter, double yCenter, double width, double height,
                          double radiusX, double radiusY, int fillColor, int strokeColor, double strokeWidth) {
        super(xCenter, yCenter, fillColor, strokeColor, strokeWidth);
        this.w = width;
        this.h = height;
        this.rx = radiusX;
        this.ry = radiusY;
    }

    @Override
    public void draw(Canvas c, Paint fillPaint, Paint strokePaint, double[] coeffs) {
        rect.set((float)(coeffs[AX]*(x0-w*0.5)+coeffs[BX]), (float)(coeffs[AY]*(y0+h*0.5)+coeffs[BY]),
                (float)(coeffs[AX]*(x0+w*0.5)+coeffs[BX]), (float)(coeffs[AY]*(y0-h*0.5)+coeffs[BY]));
        setFillPaint(fillPaint);
        c.drawRoundRect(rect, (float)(coeffs[AX]*rx), (float)(-coeffs[AY]*ry), fillPaint);
        setStrokePaint(strokePaint, coeffs[AX]);
        c.drawRoundRect(rect, (float)(coeffs[AX]*rx), (float)(-coeffs[AY]*ry), strokePaint);
    }
}
