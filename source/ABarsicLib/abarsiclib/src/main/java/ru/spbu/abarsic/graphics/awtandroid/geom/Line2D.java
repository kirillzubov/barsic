/*
 * Line2D.java
 */

package ru.spbu.abarsic.graphics.awtandroid.geom;

/**
 * @author Martynyuk
 * */

import android.graphics.Canvas;
import android.graphics.Paint;

public class Line2D extends Figure2D {
	
	float x1;
	float y1;
	float x2;
	float y2;
	
	public Line2D(float x1, float y1, float x2, float y2) {
		super();
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
	
	public Line2D(double x1, double y1, double x2, double y2) {
		super();
		this.x1 = (float)x1;
		this.y1 = (float)y1;
		this.x2 = (float)x2;
		this.y2 = (float)y2;
	}
	
	@Override
	public void draw(Canvas g, Paint p) {
		g.drawLine(x1, y1, x2, y2, p);
	}

}
