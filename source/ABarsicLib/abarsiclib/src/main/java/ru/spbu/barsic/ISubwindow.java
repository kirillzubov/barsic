package ru.spbu.barsic;

/**
 * Created by Мартынюк on 20.09.2015.
 */
public interface ISubwindow extends IElement {

    void setX(int value);

    void setY(int value);

    void setWidth(int value);

    void setHeight(int value);

    void output();

    void endOutput();

    void setIsParentBackColor(boolean value);

    void setBackColor(String value);

    void setOnMouseDown(final Runnable eventHandler);

    void setOnDrag(final Runnable eventHandler);

    void setOnDragDrop(final Runnable eventHandler);

    void setOnMouseOver(final Runnable eventHandler);

    void setOnMouseScale(final Runnable eventHandler);

    /*
    "copyToMaximizedForm"
    "getBitmap"
    "getHandle"
    "pixelHeight"
    "pixelLeft"
    "pixelTop"
    "pixelWidth"
    */

}
