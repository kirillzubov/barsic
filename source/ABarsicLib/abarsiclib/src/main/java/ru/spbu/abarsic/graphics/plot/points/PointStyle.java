package ru.spbu.abarsic.graphics.plot.points;

import android.graphics.Color;

/**
 * class of point style
 * @author Anita
 */
public class PointStyle {

    /**
     * Shape of the points
     */
    private PointShape pointShape = PointShape.CIRCLE;
    /**
     * Size of the points
     */
    private double pointSize = 3.0;
    /**
     * Color of the points
     */
    private int pointColor = Color.RED;
    /**
     * Enables points visualisation
     */
    private boolean showPoint = true;

    /**
     * Constructor by default
     */
    public PointStyle() {
    }

    /**
     * Constructor
     * @param pointShape
     * @param pointSize
     * @param pointColor
     * @param showPoint
     */
    public PointStyle(PointShape pointShape, double pointSize,
            int pointColor, boolean showPoint) {
        this.pointShape = pointShape;
        this.pointSize = pointSize;
        this.pointColor = pointColor;
        this.showPoint = showPoint;
    }

    /**
     * Constructor without showPoint
     * @param pointShape
     * @param pointSize
     * @param pointColor
     */
    public PointStyle(PointShape pointShape, double pointSize,
            int pointColor) {
        this.pointShape = pointShape;
        this.pointSize = pointSize;
        this.pointColor = pointColor;
        this.showPoint = true;
    }

    /**
     * enum shape of point
     */
    public enum PointShape {

        /**
         * 
         */
        CIRCLE,
        /**
         * 
         */
        RECTANGLE,}

    /**
     * gets shape of point
     * @return
     */
    public PointShape getPointShape() {
        return this.pointShape;
    }

    /**
     * sets shape of point
     * @param value
     */
    public void setPointShape(PointShape value) {
        this.pointShape = value;
    }

    /**
     * gets size of point
     * @return
     */
    public double getPointSize() {
        return this.pointSize;
    }

    /**
     * sets size of point
     * @param value
     */
    public void setPointSize(double value) {
        this.pointSize = value;
    }

    /**
     * gets color of point
     * @return
     */
    public int getPointColor() {
        return this.pointColor;
    }

    /**
     * sets color of point
     * @param value
     */
    public void setPointColor(int value) {
            this.pointColor = value;
    }

    /**
     * gets showPoint
     * @return
     */
    public boolean getShowPoint() {
        return this.showPoint;
    }

    /**
     * sets showPoint
     * @param value
     */
    public void setShowPoint(boolean value) {
        this.showPoint = value;
    }
}
