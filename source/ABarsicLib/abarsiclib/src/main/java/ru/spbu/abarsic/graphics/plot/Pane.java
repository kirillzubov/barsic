/*
 * Pane.java
 */

package ru.spbu.abarsic.graphics.plot;

/**
 * Porting to Android performed by Martynyuk
 */

import android.graphics.Color;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.LinearGradient;
import android.graphics.Paint.Style;
import android.graphics.Shader.TileMode;

/**
 *
 * @author Max
 */
public class Pane {

    /**
     * 
     */
    public enum GradientDirection {

        /**
         * 
         */
        Horizontal,
        /**
         * 
         */
        Vertical,
        /**
         * 
         */
        Sloped,
        /**
         * 
         */
        SlopedAlt
    }
    GradientDirection paneGradientDirection = GradientDirection.Sloped;
    GradientDirection fieldGradientDirection = GradientDirection.Sloped;
    private int paneColor1 = Color.WHITE;
    private int paneColor2 = Color.WHITE;
    private int fieldColor1 = Color.WHITE;
    private int fieldColor2 = Color.WHITE;
     /**
     * boolean value indicates addition of pane
     */ 
    
    private boolean transparentPane = false;

    /**
     * 
     */
    public Pane() {
        /*this.setPaneGradientDirection(Pane.GradientDirection.Vertical);
        this.setPaneColor1(new Color(200,219,238));
        this.setPaneColor2(Color.white);*/
    }

    /**
     * 
     * @param fieldColor1
     * @param fieldColor2
     * @param paneColor1
     * @param paneColor2
     */
    public Pane(int fieldColor1, int fieldColor2, int paneColor1, int paneColor2) {
        this.fieldColor1 = fieldColor1;
        this.fieldColor2 = fieldColor2;
        this.paneColor1 = paneColor1;
        this.paneColor2 = paneColor2;
    }

    /**
     * 
     * @param value
     */
    public void setPaneGradientDirection(GradientDirection value) {
        this.paneGradientDirection = value;
    }

    /**
     * 
     * @return
     */
    public GradientDirection getPaneGradientDirection() {
        return paneGradientDirection;
    }

    /**
     * 
     * @param value
     */
    public void setFieldGradientDirection(GradientDirection value) {
        this.fieldGradientDirection = value;
    }

    /**
     * 
     * @return
     */
    public GradientDirection getFieldGradientDirection() {
        return fieldGradientDirection;
    }

    /**
     * 
     * @return
     */
    public int getPaneColor1() {
        return paneColor1;
    }

    /**
     * 
     * @param value
     */
    public void setPaneColor1(int value) {
        this.paneColor1 = value;
    }

    /**
     * 
     * @return
     */
    public int getPaneColor2() {
        return paneColor2;
    }

    /**
     * 
     * @param value
     */
    public void setPaneColor2(int value) {
        this.paneColor2 = value;
    }

    /**
     * 
     * @param color
     */
    public void setPaneColor(int color) {
        this.paneColor1 = color;
        this.paneColor2 = color;
    }

    /**
     * 
     * @param color1
     * @param color2
     */
    public void setPaneColor(int color1, int color2) {
        this.paneColor1 = color1;
        this.paneColor2 = color2;
    }

    /**
     * 
     * @return
     */
    public int getFieldColor1() {
        return fieldColor1;
    }

    /**
     * 
     * @param value
     */
    public void setFieldColor1(int value) {
        this.fieldColor1 = value;
    }

    /**
     * 
     * @return
     */
    public int getFieldColor2() {
        return fieldColor2;
    }

    /**
     * 
     * @param value
     */
    public void setFieldColor2(int value) {
        this.fieldColor2 = value;
    }

    /**
     * 
     * @param color
     */
    public void setFieldColor(int color) {
        this.fieldColor1 = color;
        this.fieldColor2 = color;
    }

    /**
     * 
     * @param color1
     * @param color2
     */
    public void setFieldColor(int color1, int color2) {
        this.fieldColor1 = color1;
        this.fieldColor2 = color2;
    }

    /**
     * 
     * @param isTransparentPane
     */
    public void setTransparentField(boolean isTransparentPane) {
        fieldColor1 = 0;
        fieldColor2 = 0;
        this.transparentPane = isTransparentPane;
    }

    /**
     * делает оси прозрачными
     */
    public void setTransparentPane() {
        paneColor1 = 0;
        paneColor2 = 0;
    }

    /**
     * делает всю панель прозрачной
     */
    public void setTransparent() {
        fieldColor1 = 0;
        fieldColor2 = 0;
        paneColor1 = 0;
        paneColor2 = 0;
    }

    /**
     * 
     * @param width
     * @param height
     * @return
     */
    public LinearGradient getPaneGradient(float width, float height) {
        switch (paneGradientDirection) {
            case Sloped:
                return new LinearGradient(0, 0, width, height, paneColor1, paneColor2, TileMode.CLAMP);
            case SlopedAlt:
                return new LinearGradient(width, 0, 0, height, paneColor1, paneColor2, TileMode.CLAMP);
            case Horizontal:
                return new LinearGradient(0, 0, width, 0, paneColor1, paneColor2, TileMode.CLAMP);
            case Vertical:
                return new LinearGradient(0, 0, 0, height, paneColor1, paneColor2, TileMode.CLAMP);
        }
        return null;
    }

    /**
     * 
     * @param width
     * @param height
     * @return
     */
    public LinearGradient getFieldGradient(float width, float height) {
        switch (fieldGradientDirection) {
            case Sloped:
                return new LinearGradient(0, 0, width, height, fieldColor1, fieldColor2, TileMode.CLAMP);
            case SlopedAlt:
                return new LinearGradient(width, 0, 0, height, fieldColor1, fieldColor2, TileMode.CLAMP);
            case Horizontal:
                return new LinearGradient(0, 0, width, 0, fieldColor1, fieldColor2, TileMode.CLAMP);
            case Vertical:
                return new LinearGradient(0, 0, 0, height, fieldColor1, fieldColor2, TileMode.CLAMP);
        }
        return null;
    }

   
    /**
     * 
     * @param g
     * @param width
     * @param height
     * @param margin
     */
    public void draw(Canvas g, Paint p, float width, float height, Margin margin) {
        int startColor = getPaneColor1();
        int endColor = getPaneColor2();
        Style savedStyle = p.getStyle();

        if ((startColor != 0) && (endColor != 0)) {
            if (startColor != endColor) {
                p.setShader(getPaneGradient(width, height));
            } else {
                p.setShader(null);
                p.setColor(startColor);
            }
            p.setStyle(Style.FILL_AND_STROKE);
            if ((!transparentPane) && (fieldColor1 == 0) && (fieldColor2 == 0)) {
                g.drawRect(0, 0, width, height, p); //fill
            } else {
                g.drawRect(0, 0, width, margin.getTop(), p);
                g.drawRect(0, height - margin.getBottom(), width, 2*height - margin.getBottom(), p);
                g.drawRect(0, margin.getTop(), margin.getLeft(), height + margin.getTop() - margin.getBottom(), p);
                g.drawRect(width - margin.getRight(), margin.getTop(), 2*width - margin.getRight(), height + margin.getTop() - margin.getBottom(), p);
            }
        }

        startColor = getFieldColor1();
        endColor = getFieldColor2();

        if ((startColor != 0) && (endColor != 0)) {
            if (startColor != endColor) {
                p.setShader(getFieldGradient(width, height));
            } else {
                p.setShader(null);
                p.setColor(startColor);
            }
            p.setStyle(Style.FILL_AND_STROKE);
            g.drawRect(margin.getLeft(), margin.getTop(),
                       width - margin.getRight(),  height - margin.getBottom(), p); //fill
        }
        p.setShader(null);
        p.setStyle(savedStyle);
      }
    
}
