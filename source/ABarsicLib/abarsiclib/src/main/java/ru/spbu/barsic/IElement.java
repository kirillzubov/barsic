package ru.spbu.barsic;

import ru.spbu.abarsic.IModel;

public interface IElement {

    void setName(String value);

    void setLeftPos(int value); //renamed to avoid conflict with the "setLeft" method of the View class

    void setTopPos(int value); //renamed to avoid conflict with the "setTop" method of the View class

    int width();

    void width(int value);

    int height();

    void height(int value);

    void setHint(String value);

    void setIsEnabled(boolean value);

    void setIsVisible(boolean value);

    void setAlign(String value);

    void setOnHelp(final Runnable eventHandler);

    void bindWithCommonObjects(IModel model);

}