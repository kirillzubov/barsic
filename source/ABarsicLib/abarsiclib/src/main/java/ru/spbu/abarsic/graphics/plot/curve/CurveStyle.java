/*
 * CurveStyle.java
 */

package ru.spbu.abarsic.graphics.plot.curve;

import android.graphics.Color;

/**
 *
 * @author Max
 */

/**
 * Porting to Android performed by Martynyuk
 */

public class CurveStyle {
    
    /**
     * 
     */
    public CurveStyle()
    {        
        
    }
    
    /**
     * 
     * @param pointstyle
     * @param pointsize
     * @param pointcolor
     * @param linestyle
     * @param linewidth
     * @param linecolor
     * @param showPoints
     * @param showLines
     */
    public CurveStyle(
            PointStyle pointstyle,
            double pointsize,
            int pointcolor,
            LineStyle linestyle,
            double linewidth,
            int linecolor,
            boolean showPoints,
            boolean showLines)
    {
        this.pointStyle=pointstyle;
        this.pointsSize=pointsize;
        this.pointsColor=pointcolor;

        this.lineStyle=linestyle;
        this.LineWidth=linewidth;
        this.LineColor=linecolor;
        this.ShowLines=showLines;
        this.ShowPoints=showPoints;
    }
    
    
    /**
     * 
     */
    public enum PointStyle {

        /**
         * 
         */
        CIRCLE,
        /**
         * 
         */
        RECTANGLE,
        /**
         * 
         */
        CROSS,
        /**
         * 
         */
        DIAGONALCROSS
    }

    /**
     * 
     */
    public enum LineStyle {

        /**
         * 
         */
        SOLID,
        /**
         * 
         */
        DASH,
        /**
         * 
         */
        DASHDOT,
        /**
         * 
         */
        DOT
    }

    /**
     * Style of the line
     */
    private LineStyle lineStyle = LineStyle.SOLID;

    /**
     * 
     * @return
     */
    public LineStyle getLineStyle() {
        return this.lineStyle;
    }

    /**
     * 
     * @param value
     */
    public void setLineStyle(LineStyle value) {
        this.lineStyle = value;
    }
    /**
     * Width of the line
     */
    private double LineWidth = 1.0;

    /**
     * 
     * @return
     */
    public double getLineWidth() {
        return this.LineWidth;
    }

    /**
     * 
     * @param value
     */
    public void setLineWidth(double value) {
        this.LineWidth = value;
    }
    /**
     * Color of the line
     */
    private int LineColor = Color.BLACK;

    /**
     * 
     * @return
     */
    public int getLineColor() {
        return this.LineColor;
    }

    /**
     * 
     * @param value
     */
    public void setLineColor(int value) {
        if (value != 0)
            this.LineColor = value;
    }
    /**
     * Style of the points
     */
    private PointStyle pointStyle = PointStyle.CIRCLE;

    /**
     * 
     * @return
     */
    public PointStyle getPointStyle() {
        return this.pointStyle;
    }

    /**
     * 
     * @param value
     */
    public void setPointStyle(PointStyle value) {
        this.pointStyle = value;
    }
    /**
     * Color of the points
     */
    private int pointsColor = Color.RED;

    /**
     * 
     * @return
     */
    public int getPointsColor() {
        return this.pointsColor;
    }

    /**
     * 
     * @param value
     */
    public void setPointsColor(int value) {
        if (value != 0)
            this.pointsColor = value;
    }
    /**
     * Size of the points
     */
    private double pointsSize = 3.0;

    /**
     * 
     * @return
     */
    public double getPointsSize() {
        return this.pointsSize;
    }

    /**
     * 
     * @param value
     */
    public void setPointsSize(double value) {
        this.pointsSize = value;
    }
    /**
     * Enables lines visualisation
     */
    private boolean ShowLines = true;

    /**
     * 
     * @return
     */
    public boolean getShowLines() {
        return this.ShowLines;
    }

    /**
     * 
     * @param value
     */
    public void setShowLines(boolean value) {
        this.ShowLines = value;
    }
    /**
     * Enables points visualisation
     */
    private boolean ShowPoints = true;

    /**
     * 
     * @return
     */
    public boolean getShowPoints() {
        return this.ShowPoints;
    }

    /**
     * 
     * @param value
     */
    public void setShowPoints(boolean value) {
        this.ShowPoints = value;
    }

}
