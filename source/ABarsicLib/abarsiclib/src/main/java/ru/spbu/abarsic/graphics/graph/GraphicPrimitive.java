package ru.spbu.abarsic.graphics.graph;

/**
 * Created by Мартынюк on 07.12.2014.
 */

import android.graphics.Color;
import android.graphics.Paint;

import static java.lang.Math.abs;
import static java.lang.Math.max;

public abstract class GraphicPrimitive extends GraphicObject {

    int fillColor = Color.BLACK;
    int strokeColor = Color.BLACK;
    double strokeWidth = 0.0;

    protected GraphicPrimitive(double xCenter, double yCenter, int fillColor, int strokeColor, double strokeWidth) {
        super(xCenter, yCenter);
        this.fillColor = fillColor;
        this.strokeColor = strokeColor;
        this.strokeWidth = strokeWidth;
    }

    protected void setFillPaint(Paint fillPaint) {
        fillPaint.setColor(fillColor);
    }

    protected void setStrokePaint(Paint strokePaint, double ax) {
        strokePaint.setColor(strokeColor);
        strokePaint.setStrokeWidth(max((float)(abs(ax)*strokeWidth), 1.0f));
    }

}
