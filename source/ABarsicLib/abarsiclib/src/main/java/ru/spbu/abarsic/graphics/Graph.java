package ru.spbu.abarsic.graphics;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;

import java.util.ArrayList;
import java.util.Arrays;

import static ru.spbu.abarsic.graphics.CartTransform.*;
import static ru.spbu.abarsic.graphics.ColorUtils.formatColorAsInt;
import static ru.spbu.abarsic.graphics.ColorUtils.formatColorAsString;
import static java.lang.Math.abs;
import static java.lang.Math.max;

import ru.spbu.barsic.IBrush;
import ru.spbu.barsic.ICanvasManager;
import ru.spbu.barsic.IFont;
import ru.spbu.barsic.IGroup;
import ru.spbu.barsic.IPen;
import ru.spbu.barsic.IGraph;
import ru.spbu.abarsic.graphics.graph.Arc;
import ru.spbu.abarsic.graphics.graph.Chord;
import ru.spbu.abarsic.graphics.graph.Pie;
import ru.spbu.abarsic.graphics.graph.Circle;
import ru.spbu.abarsic.graphics.graph.Ellipse;
import ru.spbu.abarsic.graphics.graph.GraphicObject;
import ru.spbu.abarsic.graphics.graph.Line;
import ru.spbu.abarsic.graphics.graph.PolyLine;
import ru.spbu.abarsic.graphics.graph.Polygon;
import ru.spbu.abarsic.graphics.graph.Rectangle;
import ru.spbu.abarsic.graphics.graph.RoundRectangle;
import ru.spbu.abarsic.graphics.graph.Text;

/**
 * Created by Мартынюк on 08.12.2014.
 */
public final class Graph implements IGraph {

    ArrayList<GraphicObject> mGraphicObjects = new ArrayList<GraphicObject>(256);
    ArrayList<Group> mGroups = new ArrayList<Group>(32);

    Subwindow mSubwindowView;
    double[] mCoeffs;

    public Brush brush;
    public Pen pen;
    public Font font;
    public CanvasManager canvas;

    private Canvas mCanvas;
    private Paint mFillPaint, mStrokePaint;
    private Paint mBrushPaint, mPenPaint;
    private RectF mRect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);
    private Path mPath = new Path();

    int mContext = CONTEXT_OBJECTS;

    static int CONTEXT_OBJECTS = 0;
    static int CONTEXT_CANVAS = 1;

    Graph() {
        super();
        brush = new Brush(this);
        pen = new Pen(this);
        font = new Font(this);
        canvas = new CanvasManager();

        mBrushPaint = brush.mPaint;
        mPenPaint = pen.mPaint;
        mFillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFillPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mStrokePaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    public void update() {
        mSubwindowView.mUpdating = true;
        mSubwindowView.update();
    }

    void drawTo(Canvas c) {
        if (mSubwindowView == null) return;
        double[] coeffs = Arrays.copyOf(mSubwindowView.mCoeffs, 4);
        for (int i = 0; i < mGraphicObjects.size(); i++) {
            mGraphicObjects.get(i).draw(c, mFillPaint, mStrokePaint, coeffs);
        }
    }

    public Group group(String name) {
        Group g = findGroup(name);
        if (g == null) {
            g = new Group(name, mGroups.size());
            mGroups.add(g);
        }
        return g;
    }

    @Override
    public IGroup objects(int minIndex, int maxIndex) {
        return null;
    }

    public Group objects(int index) {
        Group g;
        if ((index < mGraphicObjects.size()) && (index > 0)) {
            g = new Group("group"+String.valueOf(mGroups.size()+1), -1);
            g.mMinIndex = mGraphicObjects.size()-1;
            g.mMaxIndex = mGraphicObjects.size()-1;
            return g;
        } else {
            return null;
        }
    }

    @Override
    public IBrush brush() {
        return brush;
    }

    @Override
    public IPen pen() {
        return pen;
    }

    @Override
    public IFont font() {
        return font;
    }

    @Override
    public ICanvasManager canvas() {
        return canvas;
    }

    @Override
    public void bounds(double xLeft, double xRight, double yBottom, double yTop) {
        mSubwindowView.mXMin = xLeft;
        mSubwindowView.mXMax = xRight;
        mSubwindowView.mYMin = yBottom;
        mSubwindowView.mYMax = yTop;
        calculateCoeffs(xLeft, xRight, yBottom, yTop,
                mSubwindowView.mWidth, mSubwindowView.mHeight, mSubwindowView.mCoeffs);
        mCoeffs = Arrays.copyOf(mSubwindowView.mCoeffs, 4);
    }

    @Override
    public void arc(double xCenter, double yCenter, double width, double height, double startAngle, double endAngle) {
        if (mContext == CONTEXT_OBJECTS) {
            mGraphicObjects.add(new Arc(xCenter, yCenter, width, height, startAngle, endAngle, brush.colorInt(), pen.colorInt(), pen.width()));
        } else if ((mContext == CONTEXT_CANVAS) && ((mCanvas = mSubwindowView.lockCanvas()) != null)) {
            mRect.set((float)(mCoeffs[AX]*(xCenter-width*0.5)+mCoeffs[BX]), (float)(mCoeffs[AY]*(yCenter+height*0.5)+mCoeffs[BY]),
                      (float)(mCoeffs[AX]*(xCenter+width*0.5)+mCoeffs[BX]), (float)(mCoeffs[AY]*(yCenter-height*0.5)+mCoeffs[BY]));
            mCanvas.drawArc(mRect, (float)(startAngle), (float)(endAngle-startAngle), false, mPenPaint);
        }
    }

    @Override
    public void chord(double xCenter, double yCenter, double width, double height, double startAngle, double endAngle) {
        if (mContext == CONTEXT_OBJECTS) {
            mGraphicObjects.add(new Chord(xCenter, yCenter, width, height, startAngle, endAngle, brush.colorInt(), pen.colorInt(), pen.width()));
        } else if ((mContext == CONTEXT_CANVAS) && ((mCanvas = mSubwindowView.lockCanvas()) != null)) {
            mRect.set((float)(mCoeffs[AX]*(xCenter-width*0.5)+mCoeffs[BX]), (float)(mCoeffs[AY]*(yCenter+height*0.5)+mCoeffs[BY]),
                      (float)(mCoeffs[AX]*(xCenter+width*0.5)+mCoeffs[BX]), (float)(mCoeffs[AY]*(yCenter-height*0.5)+mCoeffs[BY]));
            mCanvas.drawArc(mRect, (float)(startAngle), (float)(endAngle-startAngle), false, mBrushPaint);
            mCanvas.drawArc(mRect, (float)(startAngle), (float)(endAngle-startAngle), false, mPenPaint);
        }
    }

    @Override
    public void circle(double xCenter, double yCenter, double radius) {
        if (mContext == CONTEXT_OBJECTS) {
            mGraphicObjects.add(new Circle(xCenter, yCenter, radius, brush.colorInt(), pen.colorInt(), pen.width()));
        } else if ((mContext == CONTEXT_CANVAS) && ((mCanvas = mSubwindowView.lockCanvas()) != null)) {
            mRect.set((float)(mCoeffs[AX]*(xCenter-radius)+mCoeffs[BX]), (float)(mCoeffs[AY]*(yCenter+radius)+mCoeffs[BY]),
                      (float)(mCoeffs[AX]*(xCenter+radius)+mCoeffs[BX]), (float)(mCoeffs[AY]*(yCenter-radius)+mCoeffs[BY]));
            mCanvas.drawOval(mRect, mBrushPaint);
            mCanvas.drawOval(mRect, mPenPaint);
        }
    }

    @Override
    public void ellipse(double xCenter, double yCenter, double width, double height) {
        if (mContext == CONTEXT_OBJECTS) {
            mGraphicObjects.add(new Ellipse(xCenter, yCenter, width, height, brush.colorInt(), pen.colorInt(), pen.width()));
        } else if ((mContext == CONTEXT_CANVAS) && ((mCanvas = mSubwindowView.lockCanvas()) != null)) {
            mRect.set((float)(mCoeffs[AX]*(xCenter-width*0.5)+mCoeffs[BX]), (float)(mCoeffs[AY]*(yCenter+height*0.5)+mCoeffs[BY]),
                      (float)(mCoeffs[AX]*(xCenter+width*0.5)+mCoeffs[BX]), (float)(mCoeffs[AY]*(yCenter-height*0.5)+mCoeffs[BY]));
            mCanvas.drawOval(mRect, mBrushPaint);
            mCanvas.drawOval(mRect, mPenPaint);
        }
    }

    @Override
    public void line(double x1, double y1, double x2, double y2) {
        if (mContext == CONTEXT_OBJECTS) {
            mGraphicObjects.add(new Line(x1, y1, x2, y2, brush.colorInt(), pen.colorInt(), pen.width()));
        } else if ((mContext == CONTEXT_CANVAS) && ((mCanvas = mSubwindowView.lockCanvas()) != null)) {
            mCanvas.drawLine((float)(mCoeffs[AX]*x1+mCoeffs[BX]), (float)(mCoeffs[AY]*y1+mCoeffs[BY]),
                             (float)(mCoeffs[AX]*x2+mCoeffs[BX]), (float)(mCoeffs[AY]*y2+mCoeffs[BY]), mPenPaint);
        }
    }

    @Override
    public void picture(String file, double xLeft, double yTop, double xRight, double yBottom) {
        if (mContext == CONTEXT_OBJECTS) {

        } else if ((mContext == CONTEXT_CANVAS) && ((mCanvas = mSubwindowView.lockCanvas()) != null)) {

        }
    }

    @Override
    public void pie(double xCenter, double yCenter, double width, double height, double startAngle, double endAngle) {
        if (mContext == CONTEXT_OBJECTS) {
            mGraphicObjects.add(new Pie(xCenter, yCenter, width, height, startAngle, endAngle, brush.colorInt(), pen.colorInt(), pen.width()));
        } else if ((mContext == CONTEXT_CANVAS) && ((mCanvas = mSubwindowView.lockCanvas()) != null)) {
            mRect.set((float)(mCoeffs[AX]*(xCenter-width*0.5)+mCoeffs[BX]), (float)(mCoeffs[AY]*(yCenter+height*0.5)+mCoeffs[BY]),
                      (float)(mCoeffs[AX]*(xCenter+width*0.5)+mCoeffs[BX]), (float)(mCoeffs[AY]*(yCenter-height*0.5)+mCoeffs[BY]));
            mCanvas.drawArc(mRect, (float)(startAngle), (float)(endAngle-startAngle), true, mBrushPaint);
            mCanvas.drawArc(mRect, (float)(startAngle), (float)(endAngle-startAngle), true, mPenPaint);
        }
    }

    @Override
    public void rectangle(double xCenter, double yCenter, double width, double height) {
        if (mContext == CONTEXT_OBJECTS) {
            mGraphicObjects.add(new Rectangle(xCenter, yCenter, width, height, brush.colorInt(), pen.colorInt(), pen.width()));
        } else if ((mContext == CONTEXT_CANVAS) && ((mCanvas = mSubwindowView.lockCanvas()) != null)) {
            mRect.set(
                (float)(mCoeffs[AX]*(xCenter-width*0.5)+mCoeffs[BX]), (float)(mCoeffs[AY]*(yCenter+height*0.5)+mCoeffs[BY]),
                (float)(mCoeffs[AX]*(xCenter+width*0.5)+mCoeffs[BX]), (float)(mCoeffs[AY]*(yCenter-height*0.5)+mCoeffs[BY]));
            mCanvas.drawRect(mRect, mBrushPaint);
            mCanvas.drawRect(mRect, mPenPaint);
        }
    }

    @Override
    public void roundRectangle(double xCenter, double yCenter, double width, double height, double radiusX, double radiusY) {
        if (mContext == CONTEXT_OBJECTS) {
            mGraphicObjects.add(new RoundRectangle(xCenter, yCenter, width, height, radiusX, radiusY, brush.colorInt(), pen.colorInt(), pen.width()));
        } else if ((mContext == CONTEXT_CANVAS) && ((mCanvas = mSubwindowView.lockCanvas()) != null)) {
            mRect.set((float) (mCoeffs[AX] * (xCenter - width * 0.5) + mCoeffs[BX]), (float) (mCoeffs[AY] * (yCenter + height * 0.5) + mCoeffs[BY]),
                    (float) (mCoeffs[AX] * (xCenter + width * 0.5) + mCoeffs[BX]), (float) (mCoeffs[AY] * (yCenter - height * 0.5) + mCoeffs[BY]));
            mCanvas.drawRoundRect(mRect, (float)(mCoeffs[AX]*radiusX), (float)(-mCoeffs[AY]*radiusY), mBrushPaint);
            mCanvas.drawRoundRect(mRect, (float) (mCoeffs[AX] * radiusX), (float) (-mCoeffs[AY] * radiusY), mPenPaint);
        }
    }

    @Override
    public void text(String s, double xLeft, double yTop) {
        if (mContext == CONTEXT_OBJECTS) {
            mGraphicObjects.add(new Text(s, xLeft, yTop, brush.colorInt(), pen.colorInt(), pen.width()));
        } else if ((mContext == CONTEXT_CANVAS) && ((mCanvas = mSubwindowView.lockCanvas()) != null)) {
            float xs = (float)(mCoeffs[AX]*xLeft + mCoeffs[BX]);
            float ys = (float)(mCoeffs[AY]*yTop + mCoeffs[BY]) + mBrushPaint.getTextSize();
            mCanvas.drawText(s, xs, ys, mBrushPaint);
            mCanvas.drawText(s, xs, ys, mPenPaint);
        }
    }

    @Override
    public void polygon(double[] xyPoints) {
        if (mContext == CONTEXT_OBJECTS) {
            mGraphicObjects.add(new Polygon(xyPoints, brush.colorInt(), pen.colorInt(), pen.width()));
        } else if ((mContext == CONTEXT_CANVAS) && ((mCanvas = mSubwindowView.lockCanvas()) != null)) {
            mPath.rewind();
            mPath.moveTo((float) (mCoeffs[AX] * xyPoints[0] + mCoeffs[BX]), (float) (mCoeffs[AY] * xyPoints[1] + mCoeffs[BY]));
            for (int i=2; i<xyPoints.length-1; i+=2) {
                mPath.lineTo((float)(mCoeffs[AX]*xyPoints[i]+mCoeffs[BX]), (float)(mCoeffs[AY]*xyPoints[i+1]+mCoeffs[BY]));
            }
            mPath.close();

            mCanvas.drawPath(mPath, mBrushPaint);
            mCanvas.drawPath(mPath, mPenPaint);
        }
    }

    @Override
    public void polyLine(double[] xyPoints) {
        if (mContext == CONTEXT_OBJECTS) {
            mGraphicObjects.add(new PolyLine(xyPoints, brush.colorInt(), pen.colorInt(), pen.width()));
        } else if ((mContext == CONTEXT_CANVAS) && ((mCanvas = mSubwindowView.lockCanvas()) != null)) {
            mPath.rewind();
            mPath.moveTo((float)(mCoeffs[AX]*xyPoints[0]+mCoeffs[BX]), (float)(mCoeffs[AY]*xyPoints[1]+mCoeffs[BY]));
            for (int i=2; i<xyPoints.length-1; i+=2) {
                mPath.lineTo((float)(mCoeffs[AX]*xyPoints[i]+mCoeffs[BX]), (float)(mCoeffs[AY]*xyPoints[i+1]+mCoeffs[BY]));
            }

            mCanvas.drawPath(mPath, mPenPaint);
        }
    }

    /**
     * Created by Мартынюк on 09.02.2015.
     */
    public final class Pen implements IPen {

        Graph mGraph;
        Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        double width = 0.0;

        public Pen(Graph graph) {
            super();
            mGraph = graph;
            mPaint.setStyle(Paint.Style.STROKE);
        }

        @Override
        public String color() { //'#RRGGBB' format
            return formatColorAsString(mPaint.getColor());
        }

        int colorInt() {
            return mPaint.getColor();
        }

        @Override
        public void color(String value) { //'#RRGGBB' format
            mPaint.setColor(formatColorAsInt(value));
        }

        void colorInt(int value) {
            mPaint.setColor(value);
        }

        @Override
        public double width() {
            return width;
        }

        @Override
        public void width(double value) {
            if (value < 0.0) {
                throw new IllegalArgumentException("Width must be non-negative");
            }
            float screenWidth = (float)(abs(mGraph.mSubwindowView.mCoeffs[AX])*value);
            mPaint.setStrokeWidth(max(screenWidth, 1.0f));
            width = value;
        }

    }

    /**
     * Created by Мартынюк on 09.02.2015.
     */
    public final class Brush implements IBrush {

        Graph mGraph;
        Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        public Brush(Graph graph) {
            super();
            mGraph = graph;
            mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        }

        @Override
        public String color() { //'#RRGGBB' format
            return formatColorAsString(mPaint.getColor());
        }

        int colorInt() {
            return mPaint.getColor();
        }

        @Override
        public void color(String value) { //'#RRGGBB' format
            mPaint.setColor(formatColorAsInt(value));
        }

        void colorInt(int value) {
            mPaint.setColor(value);
        }

        @Override
        public String style() {
            switch (mPaint.getStyle()) {
                case STROKE:
                    return "transparent";
                case FILL: case FILL_AND_STROKE: default:
                    return "solid";
            }
        }

        @Override
        public void style(String style) {
            if (style.equals("transparent")) {
                mPaint.setStyle(Paint.Style.STROKE);
            } else if (style.equals("solid")) {
                mPaint.setStyle(Paint.Style.FILL);
            } else {
                mPaint.setStyle(Paint.Style.FILL);
            }
        }

    }

    /**
     * Created by Мартынюк on 09.02.2015.
     */
    final class Font implements IFont { //TODO complete the class 'Font'

        Graph mGraph;

        //protected Typeface font;
        //float size;

        Font(Graph graph) {
            super();
            mGraph = graph;
            //font = Typeface.create(face, Typeface.NORMAL);
            //this.size = (float)size;
        }

        @Override
        public String color() {
            return null;
        }

        @Override
        public void color(String value) {

        }

        @Override
        public String face() {
            return "";
        }

        @Override
        public void face(String value) {

        }

        @Override
        public double height() {
            return 0;
        }

        @Override
        public void height(double value) {

        }

        @Override
        public boolean isBold() {
            return false;
        }

        @Override
        public void bold(boolean value) {

        }

        @Override
        public boolean isItalic() {
            return false;
        }

        @Override
        public void italic(boolean value) {

        }

        @Override
        public boolean isStrikeout() {
            return false;
        }

        @Override
        public void strikeout(boolean value) {

        }

        @Override
        public boolean isUnderline() {
            return false;
        }

        @Override
        public void underline(boolean value) {

        }
    }

    public final class CanvasManager implements ICanvasManager {

        @Override
        public void clear() {
            mGraphicObjects.clear();
        }
    }

    private Group findGroup(String name) {
        Group group;
        for (int i=0; i<mGroups.size(); i++) {
            group = mGroups.get(i);
            if (group.mName.equals(name))
                return group;
        }
        return null;
    }

    public final class Group implements IGroup {

        int mGroupIndex = -1;

        String mName;
        int mMinIndex = -1;
        int mMaxIndex = -1;

        Group(String name, int groupIndex) {
            super();
            mName = name;
            mGroupIndex = groupIndex;
        }

        @Override
        public void output() {
            if (mMinIndex == -1)
                mMinIndex = mGraphicObjects.size();
        }

        @Override
        public void endOutput() {
            if (mMaxIndex == -1)
                mMaxIndex = mGraphicObjects.size()-1;
        }

        @Override
        public int minIndex() {
            return mMinIndex;
        }

        @Override
        public int maxIndex() {
            return mMaxIndex;
        }

        @Override
        public int count() {
            return mMaxIndex - mMinIndex + 1;
        }

        @Override
        public void delete() {
            mGroups.remove(mGroupIndex);
            mGroups.trimToSize();
        }

        @Override
        public boolean hasIndex() {
            return mGroupIndex != -1;
        }

        @Override
        public void moveBy(double deltaX, double deltaY) {
            if ((mMinIndex != -1) && (mMaxIndex != -1))
            for (int i=mMinIndex; i<=mMaxIndex; i++) {
                mGraphicObjects.get(i).moveBy(deltaX, deltaY);
            }
        }

    }
}
