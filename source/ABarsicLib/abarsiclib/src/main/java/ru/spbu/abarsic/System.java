package ru.spbu.abarsic;

import ru.spbu.barsic.ISystem;
import ru.spbu.barsic.ITimer;

/**
 * Created by Мартынюк on 23.04.2016.
 */
public final class System implements ISystem {

    public Timer timer;

    System() {
        super();
        timer = new Timer();
    }

    @Override
    public void beep() {
        
    }

    @Override
    public String currentFolder() {
        return null;
    }

    @Override
    public String currentWinFolder() {
        return null;
    }

    @Override
    public String getDate() {
        return null;
    }

    @Override
    public int getDateAsNumber() {
        return 0;
    }

    @Override
    public String[] getDriveNames() {
        return new String[0];
    }

    @Override
    public String getTime() {
        return null;
    }

    @Override
    public int getTimeAsNumber() {
        return 0;
    }

    @Override
    public long memoryUsed() {
        return 0;
    }

    @Override
    public Timer timer() {
        return null;
    }

    public final class Timer implements ITimer {

        long mStartTime;
        long mTimeout = -1;

        Timer() {
            super();
            reset();
        }

        @Override
        public void clearTimeOut() {
            mTimeout = -1;
        }

        @Override
        public void reset() {
            mStartTime = java.lang.System.currentTimeMillis();
        }

        @Override
        public void setTimeOut(double timeOut) {
            if (timeOut >= 0.0) {
                mTimeout = Math.round(timeOut * 1000.0);
            }
        }

        @Override
        public double time() {
            return (java.lang.System.currentTimeMillis() - mStartTime) * 0.001;
        }

        public long timeMillis() {
            return java.lang.System.currentTimeMillis() - mStartTime;
        }

    }

}
