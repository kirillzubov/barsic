package ru.spbu.barsicmodel;

import android.view.Gravity;
import android.widget.AbsoluteLayout;
import android.widget.LinearLayout;

import java.util.Map;

import ru.spbu.abarsic.Button;
import ru.spbu.abarsic.Model;
import ru.spbu.abarsic.TextEdit;
import ru.spbu.abarsic.graphics.Subwindow;

import static java.lang.Math.*;

/**
 * Created by Мартынюк on 12.10.2015.
 */
public final class ModelCar extends Model {

    Subwindow subwin1;
    Subwindow subwin2;
    Button button1;
    Button button2;
    TextEdit textEdit1;

    private static final long FRAME_DURATION = 40L;

    double time;
    int iFrame = 0;
    double xCar = 0.0;
    double vCar = 10.0;
    public double[] pars = new double[100];

    @Override
    protected void createComponents() {

        mRootLayout = new AbsoluteLayout(getActivity());

        //car
        subwin1 = new Subwindow(getActivity().getBaseContext(), null);
        subwin1.setWidth(410);
        subwin1.setHeight(300);
        subwin1.setX(50);
        subwin1.setY(20);
        mRootLayout.addView(subwin1);
        addElement(subwin1);

        //plot
        subwin2 = new Subwindow(getActivity().getBaseContext(), null);
        subwin2.setWidth(300);
        subwin2.setHeight(300);
        subwin2.setX(50);
        subwin2.setY(305);
        mRootLayout.addView(subwin2);
        addElement(subwin2);

        AbsoluteLayout buttonLayout = new AbsoluteLayout(getActivity());
        mRootLayout.addView(buttonLayout);

        button1 = new Button(getActivity().getBaseContext(), null);
        buttonLayout.addView(button1);
        button1.setOnClick(run);
        button1.setTextSize(20);
        button1.setText("Пуск");
        button1.width(AbsoluteLayout.LayoutParams.MATCH_PARENT);
        button1.height(AbsoluteLayout.LayoutParams.MATCH_PARENT);
        button1.setX(0);
        button1.setY(601);
        addElement(button1);

        button2 = new Button(getActivity().getBaseContext(), null);
        buttonLayout.addView(button2);
        button2.setOnClick(reset);
        button1.setTextSize(20);
        button2.setTextSize(7);
        button2.setText("В начало");
        button2.width(AbsoluteLayout.LayoutParams.MATCH_PARENT);
        button2.height(AbsoluteLayout.LayoutParams.MATCH_PARENT);
        button2.setX(200);
        button2.setY(701);
        addElement(button2);

        AbsoluteLayout textEditLayout = new AbsoluteLayout(getActivity());
        mRootLayout.addView(textEditLayout);

        textEdit1 = new TextEdit(getActivity().getBaseContext(), null);
        buttonLayout.addView(textEdit1);
        textEdit1.setTextSize(20);
        textEdit1.setTextSize(7);
        textEdit1.setIsVisible(true);
        textEdit1.setHint("HintText");
        textEdit1.width(AbsoluteLayout.LayoutParams.MATCH_PARENT);
        textEdit1.height(AbsoluteLayout.LayoutParams.MATCH_PARENT);
        textEdit1.setX(300);
        textEdit1.setY(601);
        addElement(textEdit1);

    }

    @Override
    protected void initializeComponents() {
        subwin1.output(); {
            graph.bounds(-5.0, 55.0, -30.0 * (double) subwin1.height() / subwin1.width(), 30.0 * (double) subwin1.height() / subwin1.width());
            drawBackground();
            drawRail();
            drawScale();
            drawCar();
            graph.update();
        } subwin1.endOutput();

        subwin2.output(); {
            plot.bounds(0, 5, 0, 50);
            plot.axes.x.title("t, с");
            plot.axes.y.title("x, м");
            subwindow.update();
        } subwin2.endOutput();
    }

    @Override
    public void paramVariableSet() {

        int i = 0;
        for( Map.Entry<String, Double> entry : variables.entrySet() ) {
            pars[i] = entry.getValue();
            i++;
        }
        //button2.setText(String.format("par1 = "+pars[0]));

    }

    void drawCar() {
        subwindow.context("objects");

        //car
        graph.brush.color("#00FF00");
        graph.pen.color("#000000");
        graph.polygon(new double[]{xCar - 2.5, 1.0, xCar + 2.5, 1.0, xCar + 2.5, 3.5, xCar + 1.5, 3.5,
                xCar + 1.5, 2.5, xCar + 1.0, 2.0, xCar - 1.0, 2.0, xCar - 1.5, 2.5, xCar - 1.5, 3.5, xCar - 2.5, 3.5});

        //car wheels
        graph.brush.color("#808080");
        graph.circle(xCar - 1.5, 0.5, 0.5);
        graph.circle(xCar + 1.5, 0.5, 0.5);

        //dye
        graph.brush.color("#FF0000");
        graph.pen.color("#FF0000");
        graph.polygon(new double[]{xCar + 1.5, 3.5, xCar + 1.5, 2.5, xCar + 1.0, 2.0, xCar - 1.0, 2.0, xCar - 1.5, 2.5, xCar - 1.5, 3.5});

        //arrow
        graph.brush.color("#FF0000");
        graph.pen.color("#FF0000");
        graph.polygon(new double[]{xCar + 0.1, 2, xCar - 0.1, 2, xCar - 0.1, 0.5, xCar + 0.2, 0.5, xCar + 0., 0.0, xCar - 0.2, 0.5, xCar + 0.1, 0.5});

    }

    void drawRail() {
        subwindow.context("canvas");

        //rail
        graph.brush.color("#C0C0C0");
        graph.pen.color("#C0C0C0");
        graph.rectangle(25, -2.5, 56, 5.0);
        graph.rectangle(-3.5, -1.25, 2, 7.5);
        graph.rectangle(53.75, -1.25, 2, 7.5);
        graph.pen.color("#000000");
        graph.line(-2.5, 0.0, 52.75, 0.0);
        graph.line(-2.5, 0.0, -2.5, 2.5);
        graph.line(-2.5, 2.5, -4.5, 2.5);
        graph.line(-4.5, 2.5, -4.5, -5);
        graph.line(-4.5, -5, 54.75, -5);
        graph.line(54.75, -5, 54.75, 2.5);
        graph.line(54.75, 2.5, 52.75, 2.5);
        graph.line(52.75, 2.5, 52.75, 0);

    }

    void drawBackground() {
        subwindow.context("canvas");

        //Back ground
        graph.brush.color("#00FFFF");
        graph.pen.color("#00FFFF");
        graph.rectangle(25.0, 0.0, 150.0, 50.0);
    }

    void drawScale() {
        subwindow.context("canvas");

        //Small scale
        graph.pen.color("#000000");
        float i;
        for (i=0; i<=50; i= (float) (i+0.5)) {
            graph.line(i, 0, i, -1);
        }

        //Medium scale
        float k;
        for (k=5; k<=50; k+=10) {
            graph.line(k, 0, k, -1.5);
        }

        //Large scale
        float j;
        for (j=0; j<=50; j+=10) {
            graph.line(j, 0, j, -2);
        }
    }

    boolean running = false;
    boolean paused = true;
    long frameStartTime;

    Runnable run = new Runnable() {

        @Override
        public void run() {
            if (!running) {
                running = true;
                paused = false;
                iFrame = 0;
                long sleepTime;
                button1.setText("Пауза");

                while ((xCar < 50.0) && (running)) {
                    if (!paused) {
                        frameStartTime = system.timer.timeMillis();
                        time = (double) (iFrame) * FRAME_DURATION / 1000.0;
                        xCar = vCar * time;

                        subwin1.output(); {
                            subwindow.clear();
                            drawBackground();
                            drawRail();
                            drawScale();
                            drawCar();
                            graph.update();
                        } subwin1.endOutput();

                        subwin2.output(); {
                            plot.point(time, xCar);
                            subwindow.update();
                        } subwin2.endOutput();

                        iFrame++;
                        sleepTime = frameStartTime - system.timer.timeMillis() + FRAME_DURATION;
                        application.sleep(sleepTime);
                    } else {
                        application.sleep(FRAME_DURATION);
                    }
                }

                iFrame = 0;
                running = false;
                paused = true;
                button1.setText("Пуск");

            } else {
                if (!paused) {
                    paused = true;
                    button1.setText("Пуск");
                } else {
                    paused = false;
                    button1.setText("Пауза");
                }
            }
        }
    };

    Runnable reset = new Runnable() {
        @Override
        public void run() {
            running = false;

            subwin1.output(); {
                xCar = 0.0;
                subwindow.clear();
                drawBackground();
                drawRail();
                drawScale();
                drawCar();
                graph.update();
            } subwin1.endOutput();

            subwin2.output(); {
                subwindow.clear();
                subwindow.update();
            } subwin2.endOutput();

            button1.setText("Пуск");
        }
    };

}
