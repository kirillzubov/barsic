package ru.spbu.barsicmodel;

import android.view.Gravity;
import android.widget.LinearLayout;

import ru.spbu.abarsic.Button;
import ru.spbu.abarsic.Model;
import ru.spbu.abarsic.graphics.Subwindow;

import static java.lang.Math.*;

/**
 * Created by Мартынюк on 24.03.2016.
 */
public final class ModelPendulum extends Model {

    Subwindow subwin1;
    Subwindow subwin2;
    Button button1;
    Button button2;

    private static final long FRAME_DURATION = 40L;

    double time = 0.0;
    int iFrame = 0;
    double g = 9.81;
    double L = 50.0;
    double phi = 0.0;
    double omega = sqrt(g/L);

    @Override
    protected void createComponents() {
        mRootLayout = new LinearLayout(getActivity());
        ((LinearLayout)mRootLayout).setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        ((LinearLayout) mRootLayout).setLayoutParams(layoutParams);

        //pendulum
        subwin1 = new Subwindow(getActivity().getBaseContext(), null);
        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.gravity = Gravity.TOP;
        layoutParams.weight = 3;
        subwin1.setLayoutParams(layoutParams);
        mRootLayout.addView(subwin1);
        addElement(subwin1);

        //plot
        subwin2 = new Subwindow(getActivity().getBaseContext(), null);
        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.gravity = Gravity.TOP;
        layoutParams.weight = 2;
        subwin2.setLayoutParams(layoutParams);
        mRootLayout.addView(subwin2);
        addElement(subwin2);

        LinearLayout buttonLayout = new LinearLayout(getActivity());
        buttonLayout.setOrientation(LinearLayout.HORIZONTAL);
        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.TOP;
        layoutParams.weight = 0;
        mRootLayout.addView(buttonLayout);

        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.NO_GRAVITY;
        layoutParams.weight = 1;
        layoutParams.setMargins(5, 5, 5, 5);

        button1 = new Button(getActivity().getBaseContext(), null);
        button1.setLayoutParams(layoutParams);
        buttonLayout.addView(button1);
        button1.setOnClick(run);
        button1.setText("Пуск");
        addElement(button1);

        button2 = new Button(getActivity().getBaseContext(), null);
        button2.setLayoutParams(layoutParams);
        buttonLayout.addView(button2);
        button2.setOnClick(reset);
        button2.setText("В начало");
        addElement(button2);
    }

    @Override
    protected void initializeComponents() {
        subwin1.output(); {
            graph.bounds(-35.0 * subwin1.width() / subwin1.height(), 35.0 * subwin1.width() / subwin1.height(), -10.0, 60.0);
            drawCeiling();
            drawPendulum();
            graph.update();
        } subwin1.endOutput();

        subwin2.output(); {
            plot.bounds(0, 50.0, -PI / 4.0, PI / 4.0);
        } subwin2.endOutput();
    }

    @Override
    public void paramVariableSet() {

    }

    void drawCeiling() {
        graph.brush.color("#808080");
        graph.pen.color("#808080");
        graph.rectangle(0.0, 75.0, 500.0, 50.0);
        graph.pen.color("#000000");
        graph.line(-250.0, L, 250.0, L);
    }

    void drawPendulum() {
        graph.pen.color("#000000");
        graph.line(0.0, L, L * sin(phi), L * (1.0 - cos(phi)));
        graph.brush.color("#0000FF");
        graph.pen.color("#0000FF");
        graph.circle(L * sin(phi), L * (1.0 - cos(phi)), 2.5);
    }

    boolean running = false;
    boolean paused = true;
    long frameStartTime;
    long sleepTime;

    Runnable run = new Runnable() {
        @Override
        public void run() {
            if (!running) {
                running = true;
                paused = false;
                iFrame = 0;
                button1.setText("Пауза");

                while ((time < 50.0) && (running)) {
                    if (!paused) {
                        frameStartTime = system.timer.timeMillis();
                        time = iFrame * FRAME_DURATION / 1000.0;
                        phi = PI / 6.0 * sin(omega * time);

                        subwin1.output(); {
                            subwindow.clear();
                            drawCeiling();
                            drawPendulum();
                            graph.update();
                        } subwin1.endOutput();

                        subwin2.output(); {
                            plot.point(time, phi);
                            subwindow.update();
                        } subwin2.endOutput();

                        iFrame++;
                        sleepTime = frameStartTime - system.timer.timeMillis() + FRAME_DURATION;
                        application.sleep(sleepTime);
                    } else {
                        application.sleep(FRAME_DURATION);
                    }
                }

                iFrame = 0;
                running = false;
                paused = true;
                button1.setText("Пуск");

            } else {
                if (!paused) {
                    paused = true;
                    button1.setText("Пуск");
                } else {
                    paused = false;
                    button1.setText("Пауза");
                }
            }
        }
    };

    Runnable reset = new Runnable() {
        @Override
        public void run() {
            running = false;

            subwin1.output(); {
                phi = 0.0;
                subwindow.clear();
                drawCeiling();
                drawPendulum();
                graph.update();
            } subwin1.endOutput();

            subwin2.output(); {
                subwindow.clear();
                subwindow.update();
            } subwin2.endOutput();

            button1.setText("Пуск");
        }
    };

}
