package ru.spbu.barsicmodel;

/**
 * Created by Мартынюк on 03.10.2015.
 */
public class GraphicActivity /*extends Activity*/ {

    /*private static final long FRAME_DURATION = 40L;

    Plot.ABarsicGraph aBGraph;
    Plot.ABarsicGraph aBPlot;
    Button cmdRun;
    Button cmdReset;

    double time;
    int iFrame = 0;
    double xCar = 0.0;
    double vCar = 5.0;
    Animator animator;

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_graphic);

        aBGraph = (Plot.ABarsicGraph)findViewById(R.id.aBGraph);
        aBPlot = (Plot.ABarsicGraph)findViewById(R.id.aBPlot);
        cmdRun = (Button)findViewById(R.id.cmdRun);
        cmdReset = (Button)findViewById(R.id.cmdReset);

        if (state != null) {

            iFrame = state.getInt("iFrame");
            xCar = state.getDouble("xCar");
            vCar = state.getDouble("vCar");

            //aBPlot.cleanCurves();
            //aBPlot.points.addNewPointTo(time, xCar, 0);
            //aBPlot.plot();
            //aBPlot.update();

            //example of using drawing methods of the BarsicGraph component is in
            // the onWindowFocusChanged method

        } else {
            //aBPlot.plot();
        }

        aBPlot.needScale = false;
        aBPlot.cleanCurves();
        aBPlot.getAxisX().setTitle("t, с");
        aBPlot.getAxisY().setTitle("x, м");
        aBPlot.addCurve("x(t)", new double[]{time}, new double[]{xCar});
        aBPlot.plot();
        aBPlot.update();

        cmdRun.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if ((animator == null) || (!animator.running)) {
                    animator = new Animator();
                    animator.start();
                }

                if (animator.paused) {
                    animator.paused = false;
                    cmdRun.setText("Пауза");
                } else {
                    animator.paused = true;
                    cmdRun.setText("Пуск");
                }
            }

        });

        cmdReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (animator.running) {
                    animator.running = false;
                }
                iFrame = 0;
                time = 0.0;
                xCar = 0.0;

                aBGraph.clear();
                drawGround();
                drawCar();
                aBGraph.update();

                aBPlot.cleanCurves();
                aBPlot.needScale = false;
                aBPlot.addCurve("x(t)", new double[]{time}, new double[]{xCar});
                aBPlot.dataArea.setRect(0.0, 0.0, 10.0, 50.0);
                aBPlot.plot();
                aBPlot.update();

                cmdRun.setText("Пуск");
            }
        });

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            //example of using drawing methods of the BarsicGraph component
            aBGraph.setBounds(-5.0, 55.0, -5.0, 5.0);
            drawGround();
            drawCar();
            aBGraph.update();

            aBPlot.dataArea.setRect(0.0, 0.0, 10.0, 50.0);
            aBPlot.plot();
            aBPlot.update();
        }
    }

    void drawCar() {
        //car
        aBGraph.brush().setColorInt(Color.GREEN);
        aBGraph.pen().setColorInt(Color.BLACK);
        aBGraph.polygon(new double[]{xCar-2.5, 1.0, xCar+2.5, 1.0, xCar+2.5, 3.5, xCar+1.5, 3.5,
                xCar+1.5, 2.5, xCar+1.0, 2.0, xCar-1.0, 2.0, xCar-1.5, 2.5, xCar-1.5, 3.5, xCar-2.5, 3.5});

        //car wheels
        aBGraph.brush().setColorInt(Color.GRAY);
        aBGraph.circle(xCar-1.5, 0.5, 0.5);
        aBGraph.circle(xCar+1.5, 0.5, 0.5);
    }

    void drawGround() {
        //ground
        aBGraph.brush().setColorInt(Color.LTGRAY);
        aBGraph.pen().setColorInt(Color.LTGRAY);
        aBGraph.rectangle(25.0, -2.5, 60.0, 5.0);
        aBGraph.pen().setColorInt(Color.BLACK);
        aBGraph.line(-5.0, 0.0, 55.0, 0.0);
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);
        state.putDouble("iFrame", iFrame);
        state.putDouble("xCar", xCar);
        state.putDouble("vCar", vCar);
    }

    private final class Animator extends Thread {

        boolean running = false;
        boolean paused = true;

        long sleepTime;

        @Override
        public void run() {
            running = true;
            paused = false;

            aBGraph.cleanCurves();
            aBPlot.needScale = false;
            aBPlot.dataArea.setRect(0.0, 0.0, 10.0, 50.0);
            iFrame = 0;

            while ((xCar <= 50.0) && (running)) {
                if (!paused) {
                    sleepTime = System.currentTimeMillis();

                    time = (double) (iFrame) * FRAME_DURATION / 1000.0;
                    xCar = vCar * time;

                    aBGraph.clear();
                    drawGround();
                    drawCar();
                    aBGraph.update();

                    aBPlot.points.addNewPointTo(time, xCar, 0, iFrame);
                    aBPlot.plot();
                    aBPlot.update();
                    iFrame++;

                    sleepTime = FRAME_DURATION - (System.currentTimeMillis() - sleepTime);
                    try {
                        if (sleepTime > 0) sleep(sleepTime);
                    } catch (InterruptedException e) {

                    }
                }
            }

            iFrame = 0;
            running = false;
            paused = true;

        }
    }
    */
}