package demo.ribbon;

import test.svg.transcoded.*;
import jbarsicgraph.curve.CurveStyle.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import jbarsicgraph.JBarsicGraphPlotControl;

import org.jvnet.flamingo.common.*;
import org.jvnet.flamingo.common.JCommandButton.CommandButtonKind;

import org.jvnet.flamingo.ribbon.*;

public class ControlTestRibbon extends JRibbonFrame {

    private JBarsicGraphPlotControl jBarsicGraphControl;
    
    // <editor-fold defaultstate="collapsed" desc="ribbon tasks">    
    private RibbonTask PlotTask;
    private RibbonTask AppearanceTask;
    private RibbonTask SeriesTask;
    private RibbonTask AxesTask;
    private RibbonTask LegendTask;
    private RibbonTask OtherPlotTypesTask;
    private RibbonTask WindowTask;
    // </editor-fold>

    private static void showMsgInfo(String theMessage) {
        javax.swing.JOptionPane.showMessageDialog((java.awt.Component) null, theMessage, "info", javax.swing.JOptionPane.INFORMATION_MESSAGE);
    }

    protected RibbonApplicationMenu getMenu() {
        RibbonApplicationMenuEntryPrimary amEntryNew = new RibbonApplicationMenuEntryPrimary(
                new document_new(), "New", new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("on new()");
            }
        }, CommandButtonKind.ACTION_ONLY);
        amEntryNew.setActionKeyTip("N");

        RibbonApplicationMenuEntryPrimary amEntrySave = new RibbonApplicationMenuEntryPrimary(
                new document_save(), "Save", new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("on save()");
            }
        }, CommandButtonKind.ACTION_ONLY);

        RibbonApplicationMenuEntryPrimary amEntryPrint = new RibbonApplicationMenuEntryPrimary(
                new document_print(), "Print", new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("On Print");
            }
        }, CommandButtonKind.ACTION_ONLY);

        RibbonApplicationMenu applicationMenu = new RibbonApplicationMenu();
        applicationMenu.addMenuEntry(amEntryNew);
        applicationMenu.addMenuEntry(amEntrySave);
        applicationMenu.addMenuEntry(amEntryPrint);

        applicationMenu.setDefaultCallback(new RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback() {

            @Override
            public void menuEntryActivated(JPanel targetPanel) {
                targetPanel.removeAll();
                JCommandButtonPanel openHistoryPanel = new JCommandButtonPanel(
                        CommandButtonDisplayState.MEDIUM);
                String groupName = "Default Documents";
                openHistoryPanel.addButtonGroup(groupName);
                openHistoryPanel.setMaxButtonColumns(1);
                targetPanel.setLayout(new BorderLayout());
                targetPanel.add(openHistoryPanel, BorderLayout.CENTER);
            }
        });

        return applicationMenu;
    }
    

    public ControlTestRibbon() {

        jBarsicGraphControl = new jbarsicgraph.JBarsicGraphPlotControl();

       /* advPlotControl1.getAxisX().setLineColor(Color.white);
        advPlotControl1.getAxisY().setLineColor(Color.white);
        advPlotControl1.getAxisX().setForeground(Color.white);
        advPlotControl1.getAxisY().setForeground(Color.white);
        advPlotControl1.getAxisX().setTitleForeground(Color.white);
        advPlotControl1.getAxisY().setTitleForeground(Color.white);
*/

        jBarsicGraphControl.getPane().setPaneGradientDirection(jbarsicgraph.Pane.GradientDirection.Vertical);
        jBarsicGraphControl.getPane().setPaneColor1(new Color(200,219,238));
        jBarsicGraphControl.getPane().setPaneColor2(Color.white);

        /*advPlotControl1.getLegend().setColor1(Color.white);
        advPlotControl1.getLegend().setColor2(new Color(200,219,238));*/


        PlotTask = new demo.ribbon.tasks.PlotTask(this, jBarsicGraphControl).getTask();
        AppearanceTask = new demo.ribbon.tasks.AppearanceTask(this, jBarsicGraphControl).getTask();
        SeriesTask = new demo.ribbon.tasks.SeriesTask(this, jBarsicGraphControl).getTask();
        AxesTask = new demo.ribbon.tasks.AxesTask(this, jBarsicGraphControl).getTask();
        LegendTask = new demo.ribbon.tasks.LegendTask(this, jBarsicGraphControl).getTask();
        OtherPlotTypesTask = new demo.ribbon.tasks.OtherPlotTypesTask(this, jBarsicGraphControl).getTask();
        WindowTask = new demo.ribbon.tasks.WindowTask(this).getTask();

        this.getRibbon().addTask(PlotTask);
        this.getRibbon().addTask(SeriesTask);
        this.getRibbon().addTask(AxesTask);
        this.getRibbon().addTask(AppearanceTask);
        this.getRibbon().addTask(LegendTask);
        this.getRibbon().addTask(OtherPlotTypesTask);
        this.getRibbon().addTask(WindowTask);

        //help button
        this.getRibbon().configureHelp(new help_browser(),
                new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        //TODO add help support
                        String help = "Sine sin(x)\r\nCosine cos(x)\r\nTangent tan(x)\r\nArc Sine asin(x)\r\nArc Cosine acos(x)    \r\nArc Tangent atan(x)\r\nArc Tangent (with 2 parameters) atan2(y, x)      \r\nHyperbolic Sine sinh(x)\r\nHyperbolic Cosine cosh(x)    \r\nHyperbolic Tangent tanh(x)    \r\nInverse Hyperbolic Sine asinh(x)    \r\nInverse Hyperbolic Cosine acosh(x)    \r\nInverse Hyperbolic Tangent atanh(x)  \r\nNatural Logarithm ln(x)\r\nLogarithm base 10 log(x)  \r\nExponential exp(x)    \r\nAbsolute Value / Magnitude abs(x)\r\nRandom number (between 0 and 1) rand()  \r\nModulus mod(x,y) = x % y\r\nSquare Root sqrt(x)\r\nSum sum(x,y,z)\r\nIf if(cond,trueval,falseval)\r\nStr str(x) \r\nBinomial coeficients binom(n,i)\r\n";
                        showMsgInfo(help);
                    }
                });

        this.getRibbon().setApplicationMenu(this.getMenu());

        this.setSize(640, 640);
        this.setPreferredSize(new Dimension(640, 640));

        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setTitle("JBarsicGraph 1.0 Demo by Maxim A. Maximov");

        this.add(jBarsicGraphControl);

        this.pack();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {

                try {
                    UIManager.setLookAndFeel(new org.pushingpixels.substance.api.skin.SubstanceOfficeBlue2007LookAndFeel());
                } catch (Exception ex) {
                    System.out.println("Exception:" + ex.getMessage());
                }

                new ControlTestRibbon().setVisible(true);
            }
        });
    }
}
