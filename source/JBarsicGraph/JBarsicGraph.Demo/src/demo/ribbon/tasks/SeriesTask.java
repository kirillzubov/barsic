/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package demo.ribbon.tasks;


import jbarsicgraph.curve.CurveStyle;
import javax.swing.event.ChangeEvent;

import test.svg.transcoded.*;
import jbarsicgraph.curve.CurveStyle.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.ChangeListener;

import org.jvnet.flamingo.ribbon.*;

/**
 *
 * @author Max
 */
public class SeriesTask {

    public SeriesTask(JFrame _frame,jbarsicgraph.JBarsicGraphPlotControl _advPlotControl1)
    {
        frame=_frame;
        this.advPlotControl1=_advPlotControl1;

        advPlotControl1.addSeriesChangedListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UpdateSeries();
            }
        });

    }


    private jbarsicgraph.JBarsicGraphPlotControl advPlotControl1;

    public RibbonTask getTask()
    {

       return new RibbonTask("Series", this.getSelectSeriesBand(), this.getPointsStyleBand(), this.getLineStyleBand(),this.getOtherSettingsBand());
    }

    boolean isControlsSetting = false;


    JFrame frame;

    //Point Style band
    private javax.swing.JComboBox cboPointsType;
    private javax.swing.JComboBox cboPointsColor;
    private javax.swing.JSlider sldrLineSize;
    //Line Style band
    private javax.swing.JComboBox cboLineType;
    private javax.swing.JComboBox cboLineColor;
    private javax.swing.JSlider sldrPointsSize;

    /*Other Plot Types task*/
    //Stored Function band
    private javax.swing.JComboBox cboFunction;
    private javax.swing.JTextField txtNPoints;
    private javax.swing.JTextField txtXMax;
    private javax.swing.JTextField txtXMin;

    private javax.swing.JComboBox cboSelectSeries;

    private javax.swing.JCheckBox cbShowPoints;
    private javax.swing.JCheckBox cbShowLines;



    Color colors[] = {Color.WHITE, Color.BLACK, Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.BLACK};
    Object sColors[] = new Object[]{"White","Black", "Red", "Green", "Blue", "Yellow", "Custom..."};



    public void UpdateSeries() {
        //System.out.println("UpdateSeries()");
        String[] titles = advPlotControl1.getSeriesTitles();

        int len = titles.length;

        cboSelectSeries.removeAllItems();
        if (len == 0) {
            cboSelectSeries.addItem(new KeyValue(-1, "<Empty>"));
        }

        for (int i = 0; i < len; i++) {
            cboSelectSeries.addItem(new KeyValue(i, titles[i]));
        }
    }

    protected JRibbonBand getSelectSeriesBand() {

        cboSelectSeries = new javax.swing.JComboBox();       
        cboSelectSeries.addItem(new KeyValue(-1, "<Empty>"));
        
        JRibbonBand scaleBand = new JRibbonBand("Current curve", new document_new());

        cboSelectSeries.addActionListener(new ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setStyleControls();
            }
        });

        scaleBand.addRibbonComponent(new JRibbonComponent(
                new format_justify_left(), "curve", cboSelectSeries));


        JButton btnDeleteCurve = new JButton("Delete curve");

        btnDeleteCurve.setPreferredSize(new Dimension(110, 23));

        btnDeleteCurve.addActionListener(new ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteCurveActionPerformed(evt);
            }
        });

        JButton btnClearAllCurves = new JButton("Clear all");

        btnClearAllCurves.setPreferredSize(new Dimension(110, 23));




        btnClearAllCurves.addActionListener(new ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearAllCurvesActionPerformed(evt);
            }
        });

        scaleBand.addRibbonComponent(new JRibbonComponent(
                null, "", btnDeleteCurve));

        scaleBand.addRibbonComponent(new JRibbonComponent(
                null, "", btnClearAllCurves));


        return scaleBand;
    }

    private void btnDeleteCurveActionPerformed(java.awt.event.ActionEvent e) {

        int keyId = ((KeyValue) cboSelectSeries.getSelectedItem()).getKey();
        if (keyId != -1) {
            advPlotControl1.getSeries().remove(keyId);
        } else {
            System.out.println("is empty value");
        }


        UpdateSeries();

        advPlotControl1.repaint();
    }

    private void btnClearAllCurvesActionPerformed(java.awt.event.ActionEvent e) {

        advPlotControl1.getSeries().clear();

        UpdateSeries();

        advPlotControl1.repaint();
    }


     protected JRibbonBand getPointsStyleBand() {
        JRibbonBand applicationsBand = new JRibbonBand("Point Style", new applications_other());

        cboPointsType = new JComboBox(new Object[]{"Circle", "Rectangle"});
        cboPointsType.setPreferredSize(new Dimension(100, cboPointsType.getPreferredSize().height));

        cboPointsType.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                JComboBox cbo = (JComboBox) evt.getSource();

                switch (cbo.getSelectedIndex()) {
                    case 0:
                        // pointsStyle = PointStyle.CIRCLE;
                        style.setPointStyle(PointStyle.Circle);
                        break;
                    case 1:
                        //pointsStyle = PointStyle.RECTANGLE;
                        style.setPointStyle(PointStyle.Rectangle);
                        break;
                }

                ApplyStyle();
            }
        });

        applicationsBand.addRibbonComponent(new JRibbonComponent(new applications_games(),
                "Style", cboPointsType));


        cboPointsColor = new JComboBox(sColors);
        cboPointsColor.setPreferredSize(new Dimension(100, cboPointsColor.getPreferredSize().height));

        cboPointsColor.setSelectedIndex(1);

        cboPointsColor.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboPointsColorActionPerformed(evt);
            }
        });

        applicationsBand.addRibbonComponent(new JRibbonComponent(new applications_games(),
                "Color", cboPointsColor));


        sldrPointsSize = new JSlider(10, 50, 20);
        sldrPointsSize.setPreferredSize(new Dimension(100, sldrPointsSize.getPreferredSize().height));

        sldrPointsSize.addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {
                sldrPointSizeActionPerformed(e);
            }
        });


        JRibbonComponent internet = new JRibbonComponent(
                new edit_find(), "Size", sldrPointsSize);

        applicationsBand.addRibbonComponent(internet);

        return applicationsBand;
    }

    public void sldrPointSizeActionPerformed(ChangeEvent e) {
        double pointsSize = 3 * 0.050 * sldrPointsSize.getValue(); //sldr.getValue();

        this.style.setPointsSize(pointsSize);
        ApplyStyle();
    }

    public void cboPointsTypeActionPerformed(ChangeEvent e) {
    }

    public void cboPointsColorActionPerformed(ActionEvent evt) {
        JComboBox cbo = (JComboBox) evt.getSource();

        int index = cbo.getSelectedIndex();

        if (index != colors.length - 1) {
            this.style.setPointsColor(colors[cbo.getSelectedIndex()]);
        } else {
            if (isControlsSetting) {
                return;
            }
            this.style.setPointsColor(com.bric.swing.ColorPicker.showDialog(frame, this.style.getPointsColor()));
        }

        ApplyStyle();
    }


    /**
     * Appearance|Line Style
     *
     * Style cbo
     * Color cbo
     * Size slider
     */
    protected JRibbonBand getLineStyleBand() {
        JRibbonBand applicationsBand = new JRibbonBand("Line Style", new applications_other());

        cboLineType = new JComboBox(new Object[]{"Solid", "Dash", "Dot", "DashDot"});

        cboLineType.setPreferredSize(new Dimension(100, cboLineType.getPreferredSize().height));

        cboLineType.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {



                cboLineTypeActionPerformed(evt);
            }
        });



        applicationsBand.addRibbonComponent(new JRibbonComponent(new applications_games(),
                "Style", cboLineType));


        cboLineColor = new JComboBox(sColors);
        cboLineColor.setPreferredSize(new Dimension(100, cboLineColor.getPreferredSize().height));

        cboLineColor.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboLineColorActionPerformed(evt);
                //System.err.println("dsaads");
            }
        });


        applicationsBand.addRibbonComponent(new JRibbonComponent(new applications_games(),
                "Color", cboLineColor));


        sldrLineSize = new JSlider(10, 50, 20);
        sldrLineSize.setPreferredSize(new Dimension(100, sldrLineSize.getPreferredSize().height));

        sldrLineSize.addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {
                sldrLineWidthActionPerformed(e);
            }
        });

        applicationsBand.addRibbonComponent(new JRibbonComponent(
                new edit_find(), "Width", sldrLineSize));

        return applicationsBand;
    }

    public void cboLineTypeActionPerformed(ActionEvent e) {
        switch (cboLineType.getSelectedIndex()) {
            case 0:
                style.setLineStyle(LineStyle.Solid);
                break;
            case 1:
                style.setLineStyle(LineStyle.Dash);
                break;
            case 2:
                style.setLineStyle(LineStyle.Dot);
                break;
            case 3:
                style.setLineStyle(LineStyle.DashDot);
                break;
            default:
        }

        ApplyStyle();

    }

    public void cboLineColorActionPerformed(ActionEvent evt) {
        int index = cboLineColor.getSelectedIndex();

        if (index != colors.length - 1) {
            this.style.setLineColor(colors[index]);
        } else {
            if (!isControlsSetting) {
                this.style.setLineColor(com.bric.swing.ColorPicker.showDialog(frame, this.style.getLineColor()));
            }
        }

        ApplyStyle();
    }

    public void sldrLineWidthActionPerformed(ChangeEvent e) {
        style.setLineWidth(0.075 * sldrLineSize.getValue());
        ApplyStyle();
    }


    private jbarsicgraph.curve.CurveStyle style = new jbarsicgraph.curve.CurveStyle(
            PointStyle.Circle,
            3.0,
            Color.GREEN,
            LineStyle.Solid,
            1.0,
            Color.BLACK,
            true,
            true);

    public void ApplyStyle() {

        int seriesId = ((KeyValue) cboSelectSeries.getSelectedItem()).getKey();
        if (seriesId == -1) {
            return;
        }

        //System.out.println("applyStyle to "+seriesId);


        jbarsicgraph.curve.CurveStyle currStyle = new CurveStyle();

        currStyle.setLineColor(style.getLineColor());
        currStyle.setPointsColor(style.getPointsColor());
        currStyle.setPointStyle(style.getPointStyle());
        currStyle.setLineStyle(style.getLineStyle());
        currStyle.setLineWidth(style.getLineWidth());
        currStyle.setPointsSize(style.getPointsSize());
        currStyle.setShowPoints(style.getShowPoints());
        currStyle.setShowLines(style.getShowLines());

        //System.out.println("seriesId="+seriesId);
        advPlotControl1.getSeries(seriesId).setStyle(currStyle);
        currStyle = null;

        advPlotControl1.repaint();
    }



    public void setStyleControls() {

        isControlsSetting = true;

        if (cboSelectSeries.getItemCount() == 0) {
            return;
        }
        int seriesId = ((KeyValue) cboSelectSeries.getSelectedItem()).getKey();
        if (seriesId == -1) {
            return;
        }

        //System.out.println("SELECTED_ITEM=" + cboSelectSeries.getSelectedItem().toString());
        jbarsicgraph.curve.CurveStyle currStyle = advPlotControl1.getSeries(seriesId).getStyle();

        switch (currStyle.getLineStyle()) {
            case Solid:
                cboLineType.setSelectedIndex(0);
                break;
            case Dash:
                cboLineType.setSelectedIndex(1);
                break;
            case Dot:
                cboLineType.setSelectedIndex(2);
                break;
            case DashDot:
                cboLineType.setSelectedIndex(3);
                break;
            default:
        }

        switch (currStyle.getPointStyle()) {
            case Circle:
                cboPointsType.setSelectedIndex(0);
                break;
            case Rectangle:
                cboPointsType.setSelectedIndex(1);
                break;    
            default:
        }
        java.awt.Color pointsColor = currStyle.getPointsColor();

        boolean isCustom = true;

        for (int i = 0; i < colors.length; i++) {
            if (pointsColor == colors[i]) {
                cboPointsColor.setSelectedIndex(i);
                isCustom = false;
                break;
            }
        }

        if (isCustom) {
            cboPointsColor.setSelectedIndex(colors.length - 1);
            this.style.setPointsColor(pointsColor);
        }

        java.awt.Color lineColor = currStyle.getLineColor();
        isCustom = true;

        for (int i = 0; i < colors.length; i++) {
            if (lineColor == colors[i]) {
                cboLineColor.setSelectedIndex(i);
                isCustom = false;
                break;
            }
        }

        if (isCustom) {
            cboLineColor.setSelectedIndex(colors.length - 1);
            this.style.setLineColor(lineColor);
        }


        cbShowPoints.setSelected(currStyle.getShowPoints());
        this.style.setShowPoints(currStyle.getShowPoints());

        cbShowLines.setSelected(currStyle.getShowLines());
        this.style.setShowLines(currStyle.getShowLines());



        isControlsSetting = false;

        sldrPointsSize.setValue(((int) (currStyle.getPointsSize() / (3 * 0.050))));

        sldrLineSize.setValue(((int) (currStyle.getLineWidth() / (0.075))));//0.050

        advPlotControl1.getSeries(seriesId).setStyle(currStyle);
        
        advPlotControl1.repaint();
    }

   
    protected JRibbonBand getOtherSettingsBand() {
        JRibbonBand band = new JRibbonBand("Other Options",
                new format_justify_left(), null);

        cbShowPoints = new JCheckBox("Show Points");
        cbShowPoints.setSelected(true);
        cbShowPoints.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.out.println("actionPerformed");
                if(isControlsSetting)return;
                JCheckBox cb = (JCheckBox) e.getSource();
                if (cb.getSelectedObjects() == null) {
                    style.setShowPoints(false);
                    System.out.println("showPoints=false");
                } else {
                    style.setShowPoints(true);
                    System.out.println("showPoints=true");
                }
                ApplyStyle();
            }
        });
        
        cbShowLines = new JCheckBox("Show Lines");
        cbShowLines.setSelected(true);
        cbShowLines.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(isControlsSetting)return;
                JCheckBox cb = (JCheckBox) e.getSource();
                if (cb.getSelectedObjects() == null) {
                    style.setShowLines(false);
                } else {
                    style.setShowLines(true);
                }
                ApplyStyle();
            }
        });


        
        band.addRibbonComponent(new JRibbonComponent(cbShowPoints));
        band.addRibbonComponent(new JRibbonComponent(cbShowLines));

        return band;
    }

}
