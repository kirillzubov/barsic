/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package demo.ribbon.tasks;


import test.svg.transcoded.*;
import jbarsicgraph.curve.CurveStyle.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import org.jvnet.flamingo.ribbon.*;

/**
 *
 * @author Max
 */
public class WindowTask {

    public WindowTask(JFrame _frame)
    {
        this.frame=_frame;
    }

    public RibbonTask getTask()
    {
       return new RibbonTask("Window", this.getWindowBand());
    }


    JFrame frame;

    private JRibbonBand getWindowBand() {

        JRibbonBand actionBand = new JRibbonBand("WindowStyle", new document_new());

        LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();


        final JComboBox jcb = new JComboBox(lafs);
        for (LookAndFeelInfo lafi : lafs) {
            if (UIManager.getLookAndFeel().getName().equals(lafi.getName())) {
                jcb.setSelectedItem(lafi);
                break;
            }
        }

        jcb.addItemListener(new ItemListener() {

            public void itemStateChanged(ItemEvent e) {
                SwingUtilities.invokeLater(new Runnable() {

                    public void run() {
                        boolean wasDecoratedByOS = !frame.isUndecorated();
                        try {
                            LookAndFeelInfo selected = (LookAndFeelInfo) jcb.getSelectedItem();
                            UIManager.setLookAndFeel(selected.getClassName());
                            SwingUtilities.updateComponentTreeUI(frame);
                        } catch (Exception exc) {
                            exc.printStackTrace();
                        }
                        boolean canBeDecoratedByLAF = UIManager.getLookAndFeel().getSupportsWindowDecorations();
                        if (canBeDecoratedByLAF == wasDecoratedByOS) {
                            boolean wasVisible = frame.isVisible();

                            frame.setVisible(false);
                            frame.dispose();
                            if (!canBeDecoratedByLAF) {
                                frame.setUndecorated(false);
                                frame.getRootPane().setWindowDecorationStyle(
                                        JRootPane.NONE);

                            } else {
                                frame.setUndecorated(true);
                                frame.getRootPane().setWindowDecorationStyle(
                                        JRootPane.FRAME);
                            }
                            frame.setVisible(wasVisible);
                            wasDecoratedByOS = !frame.isUndecorated();
                       }
                    }
                });
            }
        });
        jcb.setRenderer(new DefaultListCellRenderer() {

            @Override
            public Component getListCellRendererComponent(JList list,
                    Object value, int index, boolean isSelected,
                    boolean cellHasFocus) {
                return super.getListCellRendererComponent(list,
                        ((LookAndFeelInfo) value).getName(), index, isSelected,
                        cellHasFocus);
            }
        });

        actionBand.addRibbonComponent(new JRibbonComponent(null, "Look And Feel Type:", jcb));

        return actionBand;
    }

}
