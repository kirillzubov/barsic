/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package demo.ribbon.tasks;

import test.svg.transcoded.*;
import jbarsicgraph.curve.CurveStyle.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import org.jvnet.flamingo.common.*;

import org.jvnet.flamingo.ribbon.*;
import org.jvnet.flamingo.ribbon.resize.*;

/**
 *
 * @author Max
 */
public class OtherPlotTypesTask {

    public OtherPlotTypesTask(JFrame _frame,jbarsicgraph.JBarsicGraphPlotControl _advPlotControl1)
    {
        frame=_frame;
        this.advPlotControl1=_advPlotControl1;

    }



    private jbarsicgraph.JBarsicGraphPlotControl advPlotControl1;

    public RibbonTask getTask()
    {
       return new RibbonTask("Other Plot Types", this.getCustomPlotBand(), this.getPlotBand());
    }


    JFrame frame;

    /*Other Plot Types task*/
    //Stored Function band
    private javax.swing.JComboBox cboFunction;
    private javax.swing.JTextField txtNPoints;
    private javax.swing.JTextField txtXMax;
    private javax.swing.JTextField txtXMin;
    /*Other Plot Types task*/
    //custom plot item
    private javax.swing.JTextField txtXValues;
    private javax.swing.JTextField txtYValues;


    private static void showException(String theMessage) {
        javax.swing.JOptionPane.showMessageDialog((java.awt.Component) null, theMessage, "alert", javax.swing.JOptionPane.ERROR_MESSAGE);
    }


 /**
     * Plot|Plot
     *
     * XMin txt     |function|btn
     * XMax txt     |cbo     |Plot
     * Npoints txt  |        |
     */
    protected JRibbonBand getPlotBand() {

        txtXMin = new javax.swing.JTextField();
        txtXMax = new javax.swing.JTextField();
        txtNPoints = new javax.swing.JTextField();

        txtXMin.setText("-10.0");
        txtXMax.setText("10.0");
        txtNPoints.setText("200");


        txtXMin.setPreferredSize(new Dimension(100, 23));
        txtXMax.setPreferredSize(new Dimension(100, 23));
        txtNPoints.setPreferredSize(new Dimension(100, 23));

        cboFunction = new JComboBox();
        cboFunction.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"sin(x)/x", "x*x", "x*sin(x)"}));
        cboFunction.setPreferredSize(new Dimension(100, 21));

        JRibbonBand plotBand = new JRibbonBand("Stored Function", new document_new());


        plotBand.startGroup();

        plotBand.addRibbonComponent(new JRibbonComponent(
                new format_justify_left(), "XMin:", txtXMin));

        plotBand.addRibbonComponent(new JRibbonComponent(
                new format_justify_right(), "XMax:", txtXMax));

        plotBand.addRibbonComponent(new JRibbonComponent(
                new format_justify_right(), "NPoints:", txtNPoints));

        plotBand.startGroup("Function");
        plotBand.addRibbonComponent(new JRibbonComponent(null, "", cboFunction));

        plotBand.startGroup();

        JCommandButton btnPlot = new JCommandButton("Plot",
                new edit_paste());

        btnPlot.addActionListener(new ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlotActionPerformed(evt);
            }
        });

        plotBand.addCommandButton(btnPlot, RibbonElementPriority.TOP);

        List<RibbonBandResizePolicy> resizePolicies = new ArrayList<RibbonBandResizePolicy>();
        resizePolicies.add(new CoreRibbonResizePolicies.Mirror(plotBand.getControlPanel()));
        resizePolicies.add(new CoreRibbonResizePolicies.Mid2Low(plotBand.getControlPanel()));
        resizePolicies.add(new IconRibbonBandResizePolicy(plotBand.getControlPanel()));
        plotBand.setResizePolicies(resizePolicies);

        return plotBand;
    }

     private void btnPlotActionPerformed(java.awt.event.ActionEvent evt) {

        int NPoints = 200;
        try {
            NPoints = Integer.parseInt(txtNPoints.getText());
        } catch (Exception ex) {
            showException("Entered data is invalid");
            return;
        }

        switch (cboFunction.getSelectedIndex()) {
            case 0:
                advPlotControl1.setTestDataSinDivX(Utils.parseDouble(txtXMin.getText()), Utils.parseDouble(txtXMax.getText()), NPoints);
                break;
            case 1:
                advPlotControl1.setTestDataSqrX(Utils.parseDouble(txtXMin.getText()), Utils.parseDouble(txtXMax.getText()), NPoints);
                break;
            case 2:
                advPlotControl1.setTestDataSinMulX(Utils.parseDouble(txtXMin.getText()), Utils.parseDouble(txtXMax.getText()), NPoints);
                break;
        }

        advPlotControl1.repaint();

    }


        /**
     * Plot|Other Plot Types
     *
     * X txt    |btn
     * Y txt    |Plot
     */
    protected JRibbonBand getCustomPlotBand() {

        txtXValues = new javax.swing.JTextField("10 20 30 40 50");
        txtYValues = new javax.swing.JTextField("20 60 80 90 150");

        txtXValues.setPreferredSize(new Dimension(100, 23));
        txtYValues.setPreferredSize(new Dimension(100, 23));

        JRibbonBand plotBand = new JRibbonBand("Custom Points", new document_new());


        plotBand.startGroup();

        plotBand.addRibbonComponent(new JRibbonComponent(
                new format_justify_left(), "X:", txtXValues));

        plotBand.addRibbonComponent(new JRibbonComponent(
                new format_justify_right(), "Y:", txtYValues));


        plotBand.startGroup();

        JCommandButton btnPlot = new JCommandButton("Plot",
                new edit_paste());

        btnPlot.addActionListener(new ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCustomPlotActionPerformed(evt);
            }
        });

        plotBand.addCommandButton(btnPlot, RibbonElementPriority.TOP);

        List<RibbonBandResizePolicy> resizePolicies = new ArrayList<RibbonBandResizePolicy>();
        resizePolicies.add(new CoreRibbonResizePolicies.Mirror(plotBand.getControlPanel()));
        resizePolicies.add(new CoreRibbonResizePolicies.Mid2Low(plotBand.getControlPanel()));
        resizePolicies.add(new IconRibbonBandResizePolicy(plotBand.getControlPanel()));
        plotBand.setResizePolicies(resizePolicies);

        return plotBand;
    }

    private void btnCustomPlotActionPerformed(java.awt.event.ActionEvent e) {

        java.util.Scanner s = new Scanner(txtXValues.getText());

        java.util.LinkedList<Double> x = new java.util.LinkedList<Double>();

        java.util.Locale locale = java.util.Locale.getDefault();
        java.util.Locale.setDefault(Locale.US);
        while (s.hasNextDouble()) {
            x.add(s.nextDouble());
        }


        s.close();
        s = new Scanner(txtYValues.getText());

        java.util.LinkedList<Double> y = new java.util.LinkedList<Double>();
        while (s.hasNextDouble()) {
            y.add(s.nextDouble());
        }

        s.close();

        if (x.size() != y.size()) {
            showException("Entered data is invalid");
            return;
        }

        advPlotControl1.addCurve(x, y);

        java.util.Locale.setDefault(locale);

        advPlotControl1.repaint();
    }




}
