/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package demo.ribbon.tasks;

/**
 *
 * @author Max
 */
import java.util.Locale;

public class Utils {

    public static double parseDouble(String s) {
        java.util.Locale current = java.util.Locale.getDefault();


        try {
            return Double.parseDouble(s);
        } catch (Exception ex) {
        }

        try {
            java.util.Locale.setDefault(Locale.US);
            double val = Double.parseDouble(s);
            java.util.Locale.setDefault(current);
            return val;
        } catch (Exception ex) {
            java.util.Locale.setDefault(current);
        }

        return 0;
    }


}
