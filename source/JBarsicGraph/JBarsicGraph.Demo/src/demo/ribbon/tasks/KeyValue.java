/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package demo.ribbon.tasks;


/**
 *
 * @author Max
 */
public class KeyValue {

    private int key;
    private String value;

    KeyValue () {
    }

    KeyValue (int key, String value) {
        this.key=key;
        this.value=value;
    }

    public int getKey() {
        return this.key;
    }
    public void setKey(int key) {
        this.key=key;
    }

    public String getValue() {
        return this.value;
    }
    public void setValue(String value) {
        this.value=value;
    }

    @Override
    public String toString() {
        return this.value;
    }

}
