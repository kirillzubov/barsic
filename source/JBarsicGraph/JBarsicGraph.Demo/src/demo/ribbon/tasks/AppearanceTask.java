/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package demo.ribbon.tasks;



import test.svg.transcoded.*;
import jbarsicgraph.curve.CurveStyle.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import org.jvnet.flamingo.ribbon.*;
/**
 *
 * @author Max
 */
public class AppearanceTask {

    public AppearanceTask(JFrame _frame,jbarsicgraph.JBarsicGraphPlotControl _advPlotControl1)
    {
        frame=_frame;
        this.advPlotControl1=_advPlotControl1;

    }


    private jbarsicgraph.JBarsicGraphPlotControl advPlotControl1;

    boolean isControlsSetting = false;

    public RibbonTask getTask()
    {
       return new RibbonTask("Appearance", this.getRenderingBand(), this.getControlStyleBand());
    }


    JFrame frame;

    private javax.swing.JTextField txtXTitle;
    private javax.swing.JTextField txtYTitle;


    Color colors[] = {Color.WHITE, Color.BLACK, Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.BLACK};
    Object sColors[] = new Object[]{"White","Black", "Red", "Green", "Blue", "Yellow", "Custom..."};

  
    protected JRibbonBand getControlStyleBand() {

        JRibbonBand scaleBand = new JRibbonBand("Control Style", new document_new());

        scaleBand.startGroup("Pane:");

        JComboBox cboPaneColor1 = new JComboBox(sColors);
        cboPaneColor1.setPreferredSize(new Dimension(80, cboPaneColor1.getPreferredSize().height));

        cboPaneColor1.setSelectedIndex(1);

        cboPaneColor1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {                
                cboPaneColor1ActionPerformed(evt);
            }
        });

        JComboBox cboPaneColor2 = new JComboBox(sColors);
        cboPaneColor2.setPreferredSize(new Dimension(80, cboPaneColor1.getPreferredSize().height));

        cboPaneColor2.setSelectedIndex(1);

        cboPaneColor2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboPaneColor2ActionPerformed(evt);
            }
        });


        JComboBox cboFieldColor1 = new JComboBox(sColors);
        cboFieldColor1.setPreferredSize(new Dimension(80, cboPaneColor1.getPreferredSize().height));

        cboFieldColor1.setSelectedIndex(1);

        cboFieldColor1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboFieldColor1ActionPerformed(evt);
            }
        });

        JComboBox cboFieldColor2 = new JComboBox(sColors);
        cboFieldColor2.setPreferredSize(new Dimension(80, cboPaneColor1.getPreferredSize().height));

        cboFieldColor2.setSelectedIndex(1);

        cboFieldColor2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboFiled2Color1ActionPerformed(evt);
            }
        });

        scaleBand.addRibbonComponent(new JRibbonComponent(new applications_games(),
                "Color1", cboPaneColor1));

        scaleBand.addRibbonComponent(new JRibbonComponent(new applications_games(),
                "Color2", cboPaneColor2));

        scaleBand.startGroup("Field:");


        scaleBand.addRibbonComponent(new JRibbonComponent(new applications_games(),
                "", cboFieldColor1));

        scaleBand.addRibbonComponent(new JRibbonComponent(new applications_games(),
                "", cboFieldColor2));

        return scaleBand;
    }

    public void cboPaneColor1ActionPerformed(ActionEvent evt) {
        JComboBox cbo = (JComboBox) evt.getSource();

        int index = cbo.getSelectedIndex();

        if (index != colors.length - 1) {
            advPlotControl1.getPane().setPaneColor1(colors[cbo.getSelectedIndex()]);
        } else {
            if (isControlsSetting) {
                return;
            }
            advPlotControl1.getPane().setPaneColor1(com.bric.swing.ColorPicker.showDialog(frame, Color.orange));
        }

        advPlotControl1.repaint();
    }

    public void cboPaneColor2ActionPerformed(ActionEvent evt) {
        JComboBox cbo = (JComboBox) evt.getSource();

        int index = cbo.getSelectedIndex();

        if (index != colors.length - 1) {
            advPlotControl1.getPane().setPaneColor2(colors[cbo.getSelectedIndex()]);
        } else {
            if (isControlsSetting) {
                return;
            }
            advPlotControl1.getPane().setPaneColor2(com.bric.swing.ColorPicker.showDialog(frame, Color.orange));
        }

        advPlotControl1.repaint();
    }

    public void cboFieldColor1ActionPerformed(ActionEvent evt) {
        JComboBox cbo = (JComboBox) evt.getSource();

        int index = cbo.getSelectedIndex();

        if (index != colors.length - 1) {
            advPlotControl1.getPane().setFieldColor1(colors[cbo.getSelectedIndex()]);
        } else {
            if (isControlsSetting) {
                return;
            }
            advPlotControl1.getPane().setFieldColor1(com.bric.swing.ColorPicker.showDialog(frame, Color.orange));
        }

        advPlotControl1.repaint();
    }

    public void cboFiled2Color1ActionPerformed(ActionEvent evt) {
        JComboBox cbo = (JComboBox) evt.getSource();

        int index = cbo.getSelectedIndex();

        if (index != colors.length - 1) {
            advPlotControl1.getPane().setFieldColor2(colors[cbo.getSelectedIndex()]);
        } else {
            if (isControlsSetting) {
                return;
            }
            advPlotControl1.getPane() .setFieldColor2(com.bric.swing.ColorPicker.showDialog(frame, Color.orange));
        }

        advPlotControl1.repaint();
    }


      /**
     * Appearance|Rendering
     *
     * cb Antialiasing
     */
    protected JRibbonBand getRenderingBand() {
        JRibbonBand showHideBand = new JRibbonBand("Rendering",
                new format_justify_left(), null);

        JCheckBox cbAntiAlias = new JCheckBox("Antialiasing");
        cbAntiAlias.setSelected(true);
        cbAntiAlias.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JCheckBox cb = (JCheckBox) e.getSource();
                if (cb.getSelectedObjects() == null) {
                    advPlotControl1.setAntialising(false);
                } else {
                     advPlotControl1.setAntialising(true);
                }
                advPlotControl1.repaint();
            }
        });
        JRibbonComponent rulerWrapper = new JRibbonComponent(cbAntiAlias);
        showHideBand.addRibbonComponent(rulerWrapper);

        return showHideBand;
    }

}
