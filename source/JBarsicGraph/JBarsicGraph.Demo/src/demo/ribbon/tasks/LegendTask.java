/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package demo.ribbon.tasks;


import jbarsicgraph.axis.AxisValue.AlignH;
import jbarsicgraph.axis.AxisValue.AlignW;

import test.svg.transcoded.*;
import jbarsicgraph.curve.CurveStyle.*;

import jbarsicgraph.legend.Legend.AlignX;
import jbarsicgraph.legend.Legend.AlignY;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import org.jvnet.flamingo.ribbon.*;

/**
 *
 * @author Max
 */
public class LegendTask {

    public LegendTask(JFrame _frame,jbarsicgraph.JBarsicGraphPlotControl _advPlotControl1)
    {
        frame=_frame;
        this.advPlotControl1=_advPlotControl1;

    }


    private jbarsicgraph.JBarsicGraphPlotControl advPlotControl1;

    boolean isControlsSetting = false;

    public RibbonTask getTask()
    {
       return new RibbonTask("Legend", this.getAlignmentBand());
    }


    JFrame frame;

    private javax.swing.JTextField txtXTitle;
    private javax.swing.JTextField txtYTitle;


    Color colors[] = {Color.WHITE, Color.BLACK, Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.BLACK};
    Object sColors[] = new Object[]{"White","Black", "Red", "Green", "Blue", "Yellow", "Custom..."};

  
    protected JRibbonBand getControlStyleBand() {

        JRibbonBand scaleBand = new JRibbonBand("Control Style", new document_new());

        scaleBand.startGroup("Pane:");

        JComboBox cboPaneColor1 = new JComboBox(sColors);
        cboPaneColor1.setPreferredSize(new Dimension(80, cboPaneColor1.getPreferredSize().height));

        cboPaneColor1.setSelectedIndex(1);

        cboPaneColor1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {                
                cboPaneColor1ActionPerformed(evt);
            }
        });

        JComboBox cboPaneColor2 = new JComboBox(sColors);
        cboPaneColor2.setPreferredSize(new Dimension(80, cboPaneColor1.getPreferredSize().height));

        cboPaneColor2.setSelectedIndex(1);

        cboPaneColor2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboPaneColor2ActionPerformed(evt);
            }
        });


        JComboBox cboFieldColor1 = new JComboBox(sColors);
        cboFieldColor1.setPreferredSize(new Dimension(80, cboPaneColor1.getPreferredSize().height));

        cboFieldColor1.setSelectedIndex(1);

        cboFieldColor1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboFieldColor1ActionPerformed(evt);
            }
        });

        JComboBox cboFieldColor2 = new JComboBox(sColors);
        cboFieldColor2.setPreferredSize(new Dimension(80, cboPaneColor1.getPreferredSize().height));

        cboFieldColor2.setSelectedIndex(1);

        cboFieldColor2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboFiled2Color1ActionPerformed(evt);
            }
        });

        scaleBand.addRibbonComponent(new JRibbonComponent(new applications_games(),
                "Color1", cboPaneColor1));

        scaleBand.addRibbonComponent(new JRibbonComponent(new applications_games(),
                "Color2", cboPaneColor2));

        scaleBand.startGroup("Field:");


        scaleBand.addRibbonComponent(new JRibbonComponent(new applications_games(),
                "", cboFieldColor1));

        scaleBand.addRibbonComponent(new JRibbonComponent(new applications_games(),
                "", cboFieldColor2));

        return scaleBand;
    }

    public void cboPaneColor1ActionPerformed(ActionEvent evt) {
        JComboBox cbo = (JComboBox) evt.getSource();

        int index = cbo.getSelectedIndex();

        if (index != colors.length - 1) {
            advPlotControl1.getPane().setPaneColor1(colors[cbo.getSelectedIndex()]);
        } else {
            if (isControlsSetting) {
                return;
            }
            advPlotControl1.getPane().setPaneColor1(com.bric.swing.ColorPicker.showDialog(frame, Color.orange));
        }

        advPlotControl1.repaint();
    }

    public void cboPaneColor2ActionPerformed(ActionEvent evt) {
        JComboBox cbo = (JComboBox) evt.getSource();

        int index = cbo.getSelectedIndex();

        if (index != colors.length - 1) {
            advPlotControl1.getPane().setPaneColor2(colors[cbo.getSelectedIndex()]);
        } else {
            if (isControlsSetting) {
                return;
            }
            advPlotControl1.getPane().setPaneColor2(com.bric.swing.ColorPicker.showDialog(frame, Color.orange));
        }

        advPlotControl1.repaint();
    }

    public void cboFieldColor1ActionPerformed(ActionEvent evt) {
        JComboBox cbo = (JComboBox) evt.getSource();

        int index = cbo.getSelectedIndex();

        if (index != colors.length - 1) {
            advPlotControl1.getPane().setFieldColor1(colors[cbo.getSelectedIndex()]);
        } else {
            if (isControlsSetting) {
                return;
            }
            advPlotControl1.getPane().setFieldColor1(com.bric.swing.ColorPicker.showDialog(frame, Color.orange));
        }

        advPlotControl1.repaint();
    }

    public void cboFiled2Color1ActionPerformed(ActionEvent evt) {
        JComboBox cbo = (JComboBox) evt.getSource();

        int index = cbo.getSelectedIndex();

        if (index != colors.length - 1) {
            advPlotControl1.getPane().setFieldColor2(colors[cbo.getSelectedIndex()]);
        } else {
            if (isControlsSetting) {
                return;
            }
            advPlotControl1.getPane() .setFieldColor2(com.bric.swing.ColorPicker.showDialog(frame, Color.orange));
        }

        advPlotControl1.repaint();
    }


      /**
     * Appearance|Rendering
     *
     * cb Antialiasing
     */
    protected JRibbonBand getAlignmentBand() {
        JRibbonBand band = new JRibbonBand("Alignment",
                new format_justify_left(), null);

        JCheckBox cbAntiAlias = new JCheckBox("Show Legend");
        cbAntiAlias.setSelected(true);
        cbAntiAlias.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JCheckBox cb = (JCheckBox) e.getSource();
                if (cb.getSelectedObjects() == null) {
                    advPlotControl1.getLegend().setVisible(false);
                } else {
                    advPlotControl1.getLegend().setVisible(true);
                }
                advPlotControl1.repaint();
            }
        });
        
        String[] sAlignX={"Left","Center","Right"};
        String[] sAlignY={"Bottom","Center","Top"};

        JComboBox cboAlignX = new JComboBox(sAlignX);

        cboAlignX.setPreferredSize(new Dimension(80, 23));
        band.addRibbonComponent(new JRibbonComponent(new format_justify_left(),
                "alignX", cboAlignX));
        cboAlignX.setSelectedIndex(2);

        cboAlignX.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                int index = ((JComboBox) evt.getSource()).getSelectedIndex();

                switch (index) {
                    case 0:
                        advPlotControl1.setLegendAlignmentX(AlignX.Left);
                        break;
                    case 1:
                        advPlotControl1.setLegendAlignmentX(AlignX.Center);
                        break;
                    case 2:
                        advPlotControl1.setLegendAlignmentX(AlignX.Right);
                        break;
                }
                advPlotControl1.repaint();
            }
        });

        JComboBox cboAlignY = new JComboBox(sAlignY);

        cboAlignY.setPreferredSize(new Dimension(80, 23));
        band.addRibbonComponent(new JRibbonComponent(new format_justify_left(),
                "alignY", cboAlignY));
        cboAlignY.setSelectedIndex(2);

        cboAlignY.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                int index = ((JComboBox) evt.getSource()).getSelectedIndex();

                switch (index) {
                    case 0:
                        advPlotControl1.setLegendAlignmentY(AlignY.Bottom);
                        break;
                    case 1:
                        advPlotControl1.setLegendAlignmentY(AlignY.Center);
                        break;
                    case 2:
                        advPlotControl1.setLegendAlignmentY(AlignY.Top);
                        break;
                }
                advPlotControl1.repaint();
            }
        });

        JRibbonComponent rulerWrapper = new JRibbonComponent(cbAntiAlias);
        band.addRibbonComponent(rulerWrapper);

        return band;
    }




}
