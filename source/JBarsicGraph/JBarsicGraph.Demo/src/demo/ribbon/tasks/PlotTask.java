/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package demo.ribbon.tasks;

import test.svg.transcoded.*;
import jbarsicgraph.curve.CurveStyle.*;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import org.jvnet.flamingo.common.*;

import org.jvnet.flamingo.ribbon.*;
import org.jvnet.flamingo.ribbon.resize.*;

/**
 *
 * @author Max
 */
public class PlotTask {

    public PlotTask(JFrame _frame,jbarsicgraph.JBarsicGraphPlotControl _advPlotControl1)
    {
        frame=_frame;
        this.advPlotControl1=_advPlotControl1;

    }


    private jbarsicgraph.JBarsicGraphPlotControl advPlotControl1;

    public RibbonTask getTask()
    {
       return new RibbonTask("Plot", this.getFuncPlotBand());
    }


    JFrame frame;


    private javax.swing.JTextField txtFunction_func;
    private javax.swing.JTextField txtNPoints_func;
    private javax.swing.JTextField txtXMax_func;
    private javax.swing.JTextField txtXMin_func;

    private JRibbonBand getWindowBand() {

        JRibbonBand actionBand = new JRibbonBand("WindowStyle", new document_new());

        LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();


        final JComboBox jcb = new JComboBox(lafs);
        for (LookAndFeelInfo lafi : lafs) {
            if (UIManager.getLookAndFeel().getName().equals(lafi.getName())) {
                jcb.setSelectedItem(lafi);
                break;
            }
        }

        jcb.addItemListener(new ItemListener() {

            public void itemStateChanged(ItemEvent e) {
                SwingUtilities.invokeLater(new Runnable() {

                    public void run() {
                        boolean wasDecoratedByOS = !frame.isUndecorated();
                        try {
                            LookAndFeelInfo selected = (LookAndFeelInfo) jcb.getSelectedItem();
                            UIManager.setLookAndFeel(selected.getClassName());
                            SwingUtilities.updateComponentTreeUI(frame);
                        } catch (Exception exc) {
                            exc.printStackTrace();
                        }
                        boolean canBeDecoratedByLAF = UIManager.getLookAndFeel().getSupportsWindowDecorations();
                        if (canBeDecoratedByLAF == wasDecoratedByOS) {
                            boolean wasVisible = frame.isVisible();

                            frame.setVisible(false);
                            frame.dispose();
                            if (!canBeDecoratedByLAF) {
                                frame.setUndecorated(false);
                                frame.getRootPane().setWindowDecorationStyle(
                                        JRootPane.NONE);

                            } else {
                                frame.setUndecorated(true);
                                frame.getRootPane().setWindowDecorationStyle(
                                        JRootPane.FRAME);
                            }
                            frame.setVisible(wasVisible);
                            wasDecoratedByOS = !frame.isUndecorated();
                       }
                    }
                });
            }
        });
        jcb.setRenderer(new DefaultListCellRenderer() {

            @Override
            public Component getListCellRendererComponent(JList list,
                    Object value, int index, boolean isSelected,
                    boolean cellHasFocus) {
                return super.getListCellRendererComponent(list,
                        ((LookAndFeelInfo) value).getName(), index, isSelected,
                        cellHasFocus);
            }
        });

        actionBand.addRibbonComponent(new JRibbonComponent(null, "Look And Feel Type:", jcb));

        return actionBand;
    }


    protected JRibbonBand getFuncPlotBand() {

        txtXMin_func = new javax.swing.JTextField();
        txtXMax_func = new javax.swing.JTextField();
        txtNPoints_func = new javax.swing.JTextField();
        txtFunction_func = new javax.swing.JTextField();

        txtXMin_func.setText("-10.0");
        txtXMax_func.setText("10.0");
        txtNPoints_func.setText("200");
        txtFunction_func.setText("sin(x)/x");

        txtXMin_func.setPreferredSize(new Dimension(100, 23));
        txtXMax_func.setPreferredSize(new Dimension(100, 23));
        txtNPoints_func.setPreferredSize(new Dimension(100, 23));
        txtFunction_func.setPreferredSize(new Dimension(100, 50));

        JRibbonBand plotBand = new JRibbonBand("Custom Function", new document_new());


        plotBand.startGroup();

        plotBand.addRibbonComponent(new JRibbonComponent(
                new format_justify_left(), "XMin:", txtXMin_func));

        plotBand.addRibbonComponent(new JRibbonComponent(
                new format_justify_right(), "XMax:", txtXMax_func));

        plotBand.addRibbonComponent(new JRibbonComponent(
                new format_justify_right(), "NPoints:", txtNPoints_func));

        plotBand.startGroup("Function");
        plotBand.addRibbonComponent(new JRibbonComponent(null, "", txtFunction_func));

        plotBand.startGroup();

        JCommandButton btnPlot = new JCommandButton("Plot",
                new edit_paste());

        btnPlot.addActionListener(new ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFuncPlotActionPerformed(evt);
            }
        });

        plotBand.addCommandButton(btnPlot, RibbonElementPriority.TOP);

        List<RibbonBandResizePolicy> resizePolicies = new ArrayList<RibbonBandResizePolicy>();
        resizePolicies.add(new CoreRibbonResizePolicies.Mirror(plotBand.getControlPanel()));
        resizePolicies.add(new CoreRibbonResizePolicies.Mid2Low(plotBand.getControlPanel()));
        resizePolicies.add(new IconRibbonBandResizePolicy(plotBand.getControlPanel()));
        plotBand.setResizePolicies(resizePolicies);

        return plotBand;
    }

    private void btnFuncPlotActionPerformed(java.awt.event.ActionEvent e) {
        int NPoints = 200;
        try {
            NPoints = Integer.parseInt(txtNPoints_func.getText());
        } catch (Exception ex) {
            //showException("Entered data is invalid");
            return;
        }

        long time = System.currentTimeMillis();
        //advPlotControl1.addCurve(txtFunction_func.getText(), txtXMin_func.getText(),txtXMax_func.getText(), NPoints);


        double xmin=Utils.parseDouble(txtXMin_func.getText());
        double xmax=Utils.parseDouble(txtXMax_func.getText());
        String function=txtFunction_func.getText();

        double[] x=new double[NPoints];
        double[] y=new double[NPoints];

        final double EPSILON=1E-100;

        org.lsmp.djep.xjep.XJep j = new org.lsmp.djep.xjep.XJep();

        j.addStandardConstants();
        j.addStandardFunctions();
        j.addComplex();
        j.setAllowUndeclared(true);
        j.setAllowAssignment(true);


        double delta = (xmin != xmax)&& (NPoints>1) ? ((xmax - xmin) * 1.0 / (NPoints-1)) : 0;

        try {
            org.nfunk.jep.Node node1 = j.preprocess(j.parse("x=" + new Double(xmin).toString()));
            org.nfunk.jep.Node node2 = j.preprocess(j.parse(function));


            int NPoints_=NPoints-1;
            for (int i = 0; i < NPoints; i++) {
                double _x=xmin+i*delta;
                j.setVarValue("x", new Double(_x));
                double _y=(Double) j.evaluate(node2);
                if(Double.isInfinite(_y)||Double.isNaN(_y))
                {
                    //System.out.println("Infinite or NaN==================");
                    j.setVarValue("x", new Double(_x+EPSILON));
                    _y=(Double) j.evaluate(node2);
                    if(Double.isInfinite(_y)||Double.isNaN(_y))
                        System.out.println("x="+_x+" :Infinite or NaN==================");
                }

                x[i]=_x;
                y[i]=_y;
            }
            x[NPoints-1]=xmax;

        } catch (Exception ex) {
             showMsgError("Entered Data is invalid("+ex.getMessage()+")");
        }

        advPlotControl1.addCurve(function,x, y);
        x=null;
        y=null;


        //advPlotControl1.addCurve(txtFunction_func.getText(), Utils.parseDouble("-10.0"), Utils.parseDouble("10.0"), NPoints);
        System.out.println("all the time of setting data=" + (System.currentTimeMillis() - time));

        //ApplyStyle();
    }

     private static void showMsgError(String theMessage) {
        javax.swing.JOptionPane.showMessageDialog((java.awt.Component) null, theMessage, "error", javax.swing.JOptionPane.ERROR_MESSAGE);
    }

}
