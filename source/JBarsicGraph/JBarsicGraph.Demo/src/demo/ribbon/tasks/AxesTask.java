/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package demo.ribbon.tasks;

import javax.swing.event.ChangeEvent;

import test.svg.transcoded.*;
import jbarsicgraph.curve.CurveStyle.*;
import jbarsicgraph.axis.Axis.*;
import jbarsicgraph.axis.SciAxis.*;


import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

import javax.swing.*;

import javax.swing.event.ChangeListener;
import jbarsicgraph.axis.SciAxis;

import org.jvnet.flamingo.ribbon.*;
import org.jvnet.flamingo.ribbon.resize.*;

/**
 *
 * @author Max
 */
public class AxesTask {

    public AxesTask(JFrame _frame,jbarsicgraph.JBarsicGraphPlotControl _advPlotControl1)
    {
        frame=_frame;
        this.advPlotControl1=_advPlotControl1;

    }


    private jbarsicgraph.JBarsicGraphPlotControl advPlotControl1;

    boolean isControlsSetting = false;

    public RibbonTask getTask()
    {
       return new RibbonTask("Axes", this.getScaleBand(), this.getTitleBand(),this.getUnitsBand(),this.getGridlinesBand());
    }


    JFrame frame;

    private javax.swing.JTextField txtXTitle;
    private javax.swing.JTextField txtYTitle;

    private javax.swing.JTextField txtXUnits;
    private javax.swing.JTextField txtYUnits;


    Color colors[] = {Color.WHITE, Color.BLACK, Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW, Color.BLACK};
    Object sColors[] = new Object[]{"White","Black", "Red", "Green", "Blue", "Yellow", "Custom..."};


       

   protected JRibbonBand getScaleBand() {

        JSpinner spnTenPowerX = new JSpinner(new SpinnerNumberModel(3, 1, 100, 1));

        spnTenPowerX.addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {
                JSpinner spn = (JSpinner) e.getSource();

                int val = (Integer) spn.getValue();
                ((SciAxis)advPlotControl1.getAxisX()).setMaxOrder(val);

                advPlotControl1.repaint();
            }
        });

        JSpinner spnTenPowerY = new JSpinner(new SpinnerNumberModel(3, 1, 100, 1));

        spnTenPowerY.addChangeListener(new ChangeListener() {

            public void stateChanged(ChangeEvent e) {
                JSpinner spn = (JSpinner) e.getSource();

                int val = (Integer) spn.getValue();

                ((SciAxis)advPlotControl1.getAxisY()).setMaxOrder(val);

                advPlotControl1.repaint();
            }
        });

        JRibbonBand scaleBand = new JRibbonBand("Scale Order", new document_new());

        JCheckBox cbEnabled = new JCheckBox("Enabled");
        cbEnabled.setSelected(true);
        cbEnabled.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JCheckBox cb = (JCheckBox) e.getSource();
                if (cb.getSelectedObjects() == null) {
                    ((SciAxis)advPlotControl1.getAxisX()).setAutoOrderEnabled(false);
                    ((SciAxis)advPlotControl1.getAxisY()).setAutoOrderEnabled(false);
                } else {
                    ((SciAxis)advPlotControl1.getAxisX()).setAutoOrderEnabled(true);
                    ((SciAxis)advPlotControl1.getAxisY()).setAutoOrderEnabled(true);
                }
                advPlotControl1.repaint();
            }
        });

        scaleBand.addRibbonComponent(new JRibbonComponent(
                new format_justify_left(), "By X:", spnTenPowerX));

        scaleBand.addRibbonComponent(new JRibbonComponent(
                new format_justify_left(), "By Y:", spnTenPowerY));

        scaleBand.addRibbonComponent(new JRibbonComponent(
                null, "", cbEnabled));

        List<RibbonBandResizePolicy> resizePolicies = new ArrayList<RibbonBandResizePolicy>();
        resizePolicies.add(new CoreRibbonResizePolicies.Mirror(scaleBand.getControlPanel()));
        resizePolicies.add(new CoreRibbonResizePolicies.Mid2Low(scaleBand.getControlPanel()));
        resizePolicies.add(new IconRibbonBandResizePolicy(scaleBand.getControlPanel()));
        scaleBand.setResizePolicies(resizePolicies);

        return scaleBand;
    }

    protected JRibbonBand getTitleBand() {
        JRibbonBand band = new JRibbonBand("Titles", new document_new());

        txtXTitle = new javax.swing.JTextField();
        txtYTitle = new javax.swing.JTextField();

        txtXTitle.setText("x-coord");
        txtYTitle.setText("y-coord");

        txtXTitle.setPreferredSize(new Dimension(70, 23));
        txtYTitle.setPreferredSize(new Dimension(70, 23));

        JButton btnApply = new JButton("Apply");


        btnApply.addActionListener(new ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                advPlotControl1.getAxisX().setTitle(txtXTitle.getText());
                advPlotControl1.getAxisY().setTitle(txtYTitle.getText());
                advPlotControl1.repaint();


            }
        });

        band.addRibbonComponent(new JRibbonComponent(
                new format_justify_left(), "X:", txtXTitle));

        band.addRibbonComponent(new JRibbonComponent(
                new format_justify_left(), "Y:", txtYTitle));

        band.addRibbonComponent(new JRibbonComponent(
                null, "", btnApply));

        return band;
    }

    protected JRibbonBand getUnitsBand() {
        JRibbonBand band = new JRibbonBand("Units", new document_new());

        txtXUnits = new javax.swing.JTextField();
        txtYUnits = new javax.swing.JTextField();

        txtXUnits.setText("units");
        txtYUnits.setText("units");

        txtXUnits.setPreferredSize(new Dimension(70, 23));
        txtYUnits.setPreferredSize(new Dimension(70, 23));

        JButton btnApply = new JButton("Apply");


        btnApply.addActionListener(new ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {
                advPlotControl1.getAxisX().setUnits(txtXUnits.getText());
                advPlotControl1.getAxisY().setUnits(txtYUnits.getText());
                advPlotControl1.repaint();
            }
        });

        band.addRibbonComponent(new JRibbonComponent(
                new format_justify_left(), "X:", txtXUnits));

        band.addRibbonComponent(new JRibbonComponent(
                new format_justify_left(), "Y:", txtYUnits));

        band.addRibbonComponent(new JRibbonComponent(
                null, "", btnApply));

        return band;
    }   

    protected JRibbonBand getGridlinesBand() {
        JRibbonBand band = new JRibbonBand("Gridlines Options",
                new format_justify_left(), null);

        JCheckBox cbShowGridX = new JCheckBox("Gridlines by X");
        cbShowGridX.setSelected(true);

        cbShowGridX.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(isControlsSetting)return;
                JCheckBox cb = (JCheckBox) e.getSource();
                if (cb.getSelectedObjects() == null) {
                    advPlotControl1.getAxisX().setShowGridLines(false);
                } else {
                    advPlotControl1.getAxisX().setShowGridLines(true);
                }
                advPlotControl1.repaint();
            }
        });

        JCheckBox cbShowGridY = new JCheckBox("Gridlines by Y");
        cbShowGridY.setSelected(true);

        cbShowGridY.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(isControlsSetting)return;
                JCheckBox cb = (JCheckBox) e.getSource();
                if (cb.getSelectedObjects() == null) {
                    advPlotControl1.getAxisY().setShowGridLines(false);
                } else {
                    advPlotControl1.getAxisY().setShowGridLines(true);
                }
                advPlotControl1.repaint();
            }
        });



        band.addRibbonComponent(new JRibbonComponent(cbShowGridX));
        band.addRibbonComponent(new JRibbonComponent(cbShowGridY));

        return band;
    }
}
