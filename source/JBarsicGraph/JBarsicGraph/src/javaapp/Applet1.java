package javaapp;

/**
 *
 *Applet1 
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Panel;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import jbarsicgraph.DefFunction;
import jbarsicgraph.JBarsicGraph;
import jbarsicgraph.points.PointStyle;

/**
 * %
 * @author Анита
 */
public class Applet1 extends JApplet {

    @Override
    public String getAppletInfo() {
        return "Name: JLabApplet\r\n"
                + "Author: Anita Vasilyeva\r\n"
                + "Created with NetBeans Version 7.0.1";
    }
    private int width = 480, height = 560;
  
    Panel fredPanel = new Panel();
    JButton plotButton = new JButton("plot");
    JButton stopButton = new JButton("stop");
    JButton cleanButton = new JButton("clean");
    JLabel xminlabel = new JLabel("xmin");
    JTextField xminEdit = new JTextField("-10", 10);
    JLabel xmaxlabel = new JLabel("xmax");
    JTextField xmaxEdit = new JTextField("10", 10);
    JLabel funlabel = new JLabel("f(x)=");
    JTextField funField = new JTextField("sin(x)", 40);
    private DefFunction defFun;
    private JBarsicGraph barsG;
    PointStyle pointstyle;

//   инициализация апплета
    @Override
    public void init() {
        super.init();

        setSize(width, height);
        setLayout(null);
        initJBarsG(width / 2 - 10,height );
        barsG.setBounds(0, 0, width, height-80);
        add(barsG);
     
        fredPanel.add(funlabel);
        fredPanel.add(funField);
        fredPanel.add(xminlabel);
        fredPanel.add(xminEdit);
        fredPanel.add(xmaxlabel);
        fredPanel.add(xmaxEdit);
        fredPanel.add(plotButton);
        fredPanel.add(cleanButton);
        fredPanel.setBounds(0, height-80, width, 80);
        add(fredPanel);
        plotButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                barsG.needScale=false;
                String s = funField.getText();
                double xmin = Double.parseDouble(xminEdit.getText());
                double xmax = Double.parseDouble(xmaxEdit.getText());
//                barsG.dataArea=new Rectangle2D.Double(xmin,-10,xmax-xmin,60);
                int n = (int) ((xmax - xmin) / 0.05);
                double[] x = new double[n];
                double[] y = new double[n];
                x[0] = xmin;
                for (int i = 0; i < n; i++) {
                    x[i] = x[0] + 0.05 * i;
                    defFun = new DefFunction(s, x[i], 'x');
                    defFun.defFunction();
                    y[i] = defFun.fun;
                      pointstyle=new PointStyle(PointStyle.PointShape.RECTANGLE, 3.0, Color.BLUE);
                      barsG.points.addNewPointTo(x[i],y[i],i,pointstyle);
//                      barsG.repaint();

                }

//                barsG.addCurve(x, y);
                barsG.repaint();


            }
        });


        cleanButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                barsG.cleanCurves();
                barsG.repaint();
            }
        });


    }
    
    private void initJBarsG(double w,double h) {
        barsG = new JBarsicGraph();
        barsG.setPreferredSize(new Dimension((int) w, (int) h));
        barsG.getAxisX().setTitle("x");
        barsG.getAxisX().setUnits("");
        barsG.getAxisY().setTitle("f(x)");
        barsG.getAxisY().setUnits("");
        barsG.setVisible(true);
    }

    /**
     * Отрисовка апплета, делегирование её сцене.
     * @param g Холст апплета.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

    }

    @Override
    public void start() {
//        timer.start();
    }

    @Override
    public void stop() {
//        timer.stop();
    }

    @Override
    public void destroy() {
        fredPanel = null;
        barsG = null;
    }
}
