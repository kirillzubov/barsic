/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapp;

/**
 *
 * @author Анита
 */
import java.applet.Applet;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import javax.swing.*;
import jbarsicgraph.DefFunction;
import jbarsicgraph.JBarsicGraph;

/**
 *
 * @author Анита
 */
public class AppLog extends Applet {

    @Override
    public String getAppletInfo() {
        return "Name: JLabApplet\r\n"
                + "Author: Anita Vasilyeva\r\n"
                + "Created with NetBeans Version 7.0.1";
    }
    private int width = 900, height = 480;
//    Panel fredPanel = new Panel();
    JButton plotButton = new JButton("plot");
    JButton stopButton = new JButton("stop");
    JButton clearButton = new JButton("clean");
    JLabel xminlabel = new JLabel("xmin");
    JTextField xminEdit = new JTextField("1", 10);
    JLabel xmaxlabel = new JLabel("xmax");
    JTextField xmaxEdit = new JTextField("10", 10);
    JLabel fun1label = new JLabel("y(x)=");
    JTextField fun1Field = new JTextField("exp(x)", 37);
    JRadioButton radXLog = new JRadioButton("логарифм координаты х        ");
    JRadioButton radYLog = new JRadioButton("логарифмифм координаты y");
    JRadioButton radXYLog = new JRadioButton("логарифмы координат х и у  ");
    JRadioButton rad = new JRadioButton("по умолчанию,  у(х)             ");
    Panel bPanel1 = new Panel();//панель с текст полями
    Panel bPanel2 = new Panel();//панель с переключателями 
    Panel bPanel3 = new Panel();//панель с  кнопками
    private DefFunction defFun;
    private JBarsicGraph barsG;
    int n;
    private double[] x = new double[n];
    private double[] y = new double[n];
    ButtonGroup bGroup = new ButtonGroup();

//   инициализация апплета
    @Override
    public void init() {
        super.init();

        setSize(width, height);
//        режим размещения: 1 строка, 2 столбца
        setLayout(null);

        barsG = new JBarsicGraph();
        initJBarsG(480, 480);
        barsG.setBounds(0, 0, (int) (width / 2), height);
        add(barsG);
        bPanel1.setBounds(width / 2, 0, width / 2, 70);
        bPanel1.add(fun1label);
        bPanel1.add(fun1Field);
        bPanel1.add(xminlabel);
        bPanel1.add(xminEdit);
        bPanel1.add(xmaxlabel);
        bPanel1.add(xmaxEdit);
        add(bPanel1);

        bPanel2.setBounds(width / 2, 80, width / 4, 200);
        bGroup.add(radXLog);
        bGroup.add(radYLog);
        bGroup.add(radXYLog);
        bGroup.add(rad);
        bPanel2.add(radXLog);
        bPanel2.add(radYLog);
        bPanel2.add(radXYLog);
        bPanel2.add(rad);
        rad.setSelected(true);
        add(bPanel2);

        bPanel3.setBounds(3*width/4, 80,width / 4 ,  200);
        bPanel3.add(plotButton);
        bPanel3.add(clearButton);
        add(bPanel3);




        plotButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                barsG.needScale = false;
                barsG.dataArea = new Rectangle2D.Double(1, -3, 10, 10);
                final String s1 = fun1Field.getText();
                double xmin = Double.parseDouble(xminEdit.getText());
                double xmax = Double.parseDouble(xmaxEdit.getText());
                n = (int) ((xmax - xmin) / 0.05);
                x = new double[n];
                y = new double[n];
                x[0] = xmin;
                if (radXLog.isSelected()) {

                    for (int i = 0; i < n; i++) {
                        x[i] = Math.log(xmin + 0.05 * i);
                        defFun = new DefFunction(s1, x[i], 'x');
                        defFun.defFunction();
                        y[i] = defFun.fun;
                        barsG.points.addNewPointTo(x[i], y[i], i);
                    }

                }

                if (radYLog.isSelected()) {
                    for (int i = 0; i < n; i++) {
                        x[i] = x[0] + 0.05 * i;
                        defFun = new DefFunction(s1, x[i], 'x');
                        defFun.defFunction();
                        y[i] = Math.log(defFun.fun);
                        barsG.points.addNewPointTo(x[i], y[i], i);
                        barsG.repaint();
                    }

                }

                if (radXYLog.isSelected()) {
                    for (int i = 0; i < n; i++) {
                        x[i] = Math.log(xmin + 0.05 * i);
                        defFun = new DefFunction(s1, x[i], 'x');
                        defFun.defFunction();
                        y[i] = Math.log(defFun.fun);
                        barsG.points.addNewPointTo(x[i], y[i], i);
                        barsG.repaint();
                    }

                }
                if (rad.isSelected()) {
                    for (int i = 0; i < n; i++) {
                        x[i] = x[0] + 0.05 * i;
                        defFun = new DefFunction(s1, x[i], 'x');
                        defFun.defFunction();
                        y[i] = defFun.fun;
                        barsG.points.addNewPointTo(x[i], y[i], i);
                        barsG.repaint();
                    }

                }

            }
        });


        stopButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ev) {
            }
        });

        clearButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                barsG.cleanCurves();
                barsG.repaint();
            }
        });

    }

    private void initJBarsG(double w, double h) {
        barsG = new JBarsicGraph();
        barsG.setPreferredSize(new Dimension((int) w, (int) h));
        barsG.getAxisX().setTitle("x");
        barsG.getAxisX().setUnits("");
        barsG.getAxisY().setTitle("f(x)");
        barsG.getAxisY().setUnits("");
        barsG.setVisible(true);
    }

    /**
     * Отрисовка апплета, делегирование её сцене.
     * @param g Холст апплета.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

    }

    @Override
    public void start() {
//        timer.start();
    }

    @Override
    public void stop() {
//        timer.stop();
    }

    @Override
    public void destroy() {
        bPanel1 = null;
        bPanel2 = null;
        bPanel3 = null;
        barsG = null;
    }
}
