/*
 * Applet for display parametric curves
 * 
 */
package javaapp;
/**
 *
 * @author Анита
 */
import java.applet.Applet;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import jbarsicgraph.DefFunction;
import jbarsicgraph.JBarsicGraph;

/**
 *
 * @author Анита
 */
public class AppParam extends Applet {

    @Override
    public String getAppletInfo() {
        return "Name: JLabApplet\r\n"
                + "Author: Anita Vasilyeva\r\n"
                + "Created with NetBeans Version 7.0.1";
    }
    // Сцена, где все происходит
    private int width = 480, height = 580;
    Panel fredPanel = new Panel();
    JButton plotButton = new JButton("plot");
    JButton stopButton = new JButton("stop");
    JButton cleanButton = new JButton("clean");
    JLabel tminlabel = new JLabel("tmin");
    JTextField tminEdit = new JTextField("-10", 10);
    JLabel tmaxlabel = new JLabel("tmax");
    JTextField tmaxEdit = new JTextField("10", 10);
    JLabel fun1label = new JLabel("x(t)=");
    JTextField fun1Field = new JTextField("sin(t)", 40);
    JLabel fun2label = new JLabel("y(t)=");
    JTextField fun2Field = new JTextField("cos(t)", 40);
    private DefFunction defFun, defFun1;
    private JBarsicGraph barsG;

//   инициализация апплета
    @Override
    public void init() {
        super.init();

        setSize(width, height);
        setLayout(null);
        initJBarsG(width / 2 - 10, height - 90);
        barsG.setBounds(0, 0, width, height-90);
        add(barsG);
        fredPanel.add(fun1label);
        fredPanel.add(fun1Field);
        fredPanel.add(fun2label);
        fredPanel.add(fun2Field);
        fredPanel.add(tminlabel);
        fredPanel.add(tminEdit);
        fredPanel.add(tmaxlabel);
        fredPanel.add(tmaxEdit);
        fredPanel.add(plotButton);
        fredPanel.add(cleanButton);
        fredPanel.setBounds(0,height-90, width, 90);
        add(fredPanel);
        plotButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                 barsG.needScale=false;
                 barsG.dataArea=new Rectangle2D.Double(-2,-2,4,4);
                String s1 = fun1Field.getText();
                String s2 = fun2Field.getText();
                double tmin = Double.parseDouble(tminEdit.getText());
                double tmax = Double.parseDouble(tmaxEdit.getText());
                int n = (int) ((tmax - tmin) / 0.05);
                double[] t = new double[n];
                double[] x = new double[n];
                double[] y = new double[n];
                t[0] = tmin;

                for (int i = 0; i < n; i++) {
                    t[i] = t[0] + 0.05 * i;
                    defFun = new DefFunction(s1, t[i], 't');
                    defFun.defFunction();
                    x[i] = defFun.fun;
                    defFun1 = new DefFunction(s2, t[i], 't');
                    defFun1.defFunction();
                    y[i] = defFun1.fun;
                      barsG.points.addNewPointTo(x[i],y[i],0,i);
                      barsG.repaint();

                }
//                barsG.addCurve(x, y);
//                barsG.repaint();


            }
        });


        stopButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ev) {
            }
        });

        cleanButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                defFun = null;
                defFun1 = null;
                barsG.cleanCurves();
                barsG.repaint();
            }
        });


    }
    /**
     * initialisation graphics component
     * @param w
     * @param h 
     */
     private void initJBarsG(double w,double h) {
        barsG = new JBarsicGraph();
        barsG.setPreferredSize(new Dimension((int) w, (int) h));
        barsG.getAxisX().setTitle("x");
        barsG.getAxisX().setUnits("");
        barsG.getAxisY().setTitle("f(x)");
        barsG.getAxisY().setUnits("");
        barsG.setVisible(true);
    }

    /**
     * Отрисовка апплета, делегирование её сцене.
     * @param g Холст апплета.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

    }


    @Override
    public void start() {
//        timer.start();
    }

    @Override
    public void stop() {
//        timer.stop();
    }

    @Override
    public void destroy() {
        fredPanel = null;
        barsG = null;
    }
}
