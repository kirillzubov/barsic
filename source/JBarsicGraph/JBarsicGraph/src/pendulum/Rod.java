package pendulum;

import java.awt.Color;
import java.awt.Graphics;
import static java.lang.Math.*;
import jbarsicgraph.JBarsicGraph;
import jbarsicgraph.points.PointStyle;

/**
 *Класс невесомого стержня
 * @author Anita
 */
public final class Rod extends MaterialObject {

    /**
     * объект класса штатива
     */
    protected Pendulum pendulum;
    private int i = 0;
    private double t = 0;
    private int n = 400;
    double xmax;
    /**
     * скорость тела по fi
     */
    protected double vfi = 0;
    private final float dt = 0.1f;//время итерации
    private JBarsicGraph barsG;
    private double x0;
    private double y0;
    private int blue, green;
    /**
     * угол отклонения
     */
    protected double fi;
    protected double fi0=0;
    /**
     * происходит ли движение
     */
    protected boolean runbool = false;

    /**
     * 
     */
    public Rod() {
        this.height = 75;
        this.width = 6;
        this.x = 350;
        this.y = 216;
        this.fi = 0;
    }

    /**
     * 
     * @param l-длина 
     * @param pendulum- объект класса Pendulum
     * @param fi-угол отклонения
     * @param barsG- компонент графики  
     */
    public Rod(double l, double fi, Pendulum pendulum, JBarsicGraph barsG) {
        this.pendulum = pendulum;
        this.height = (int) l;//длина стержня
        this.fi = fi;
        this.width = 5 * pendulum.width / 96;//ширина стержня
        this.barsG = barsG;
//        координаты точки подвеса стержня
        this.x = (int) (pendulum.x + pendulum.width * 141 / 80 - pendulum.l * Math.sin(30 * Math.PI / 180) / 128);
        this.y = (int) (pendulum.y - 4 * pendulum.height / 5 - 29 * pendulum.l / 32 + pendulum.width * Math.tan(15 * Math.PI / 180) / 8
                + 1.2 * pendulum.width * Math.tan(15 * Math.PI / 180)
                + pendulum.l * Math.pow(Math.cos(15 * Math.PI / 180), 2) / 64);
    }

    /**
     * 
     * @param g-графический контекст
     */
    @Override
    public void paintSelf(Graphics g) {

        this.x0 = this.height * sin(fi * PI / 180);
        this.y0 = this.height * cos(fi / 180 * PI);
        double xc = this.x + 4 * this.x0;
        double yc = this.y + 4 * this.y0;
        double ml = 5 * pendulum.width / 36;
        int[] xrod = new int[4];
        int[] yrod = new int[4];
        xrod[0] = (int) (this.x - ml * sin(fi * PI / 180) - this.width * cos(fi * PI / 180) / 2);
        yrod[0] = (int) (this.y - ml * cos(fi * PI / 180) + this.width * sin(fi * PI / 180) / 2);
        xrod[1] = (int) (this.x - ml * sin(fi * PI / 180) + this.width * cos(fi * PI / 180) / 2);
        yrod[1] = (int) (this.y - ml * cos(fi * PI / 180) - this.width * sin(fi * PI / 180) / 2);
        xrod[2] = (int) (xc + this.width * cos(fi * PI / 180) / 2);
        yrod[2] = (int) (yc - this.width * sin(fi * PI / 180) / 2);
        xrod[3] = (int) (xc - this.width * cos(fi * PI / 180) / 2);
        yrod[3] = (int) (yc + this.width * sin(fi * PI / 180) / 2);
        g.setColor(new Color(139, 139, 131));
        g.fillPolygon(xrod, yrod, 4);
        g.setColor(new Color(54, 54, 54));
        g.drawPolygon(xrod, yrod, 4);
        g.setColor(Color.LIGHT_GRAY);
        g.drawLine(this.x, this.y, (int) xc, (int) yc);
        int[] xscrew = new int[4];
        int[] yscrew = new int[4];
        xscrew[0] = (int) (this.x - 5 * ml / 14 * sin(fi * PI / 180) - this.width * cos(fi * PI / 180));
        yscrew[0] = (int) (this.y - 5 * ml / 14 * cos(fi * PI / 180) + this.width * sin(fi * PI / 180));
        xscrew[1] = (int) (this.x - 5 * ml / 14 * sin(fi * PI / 180) + this.width * cos(fi * PI / 180));
        yscrew[1] = (int) (this.y - 5 * ml / 14 * cos(fi * PI / 180) - this.width * sin(fi * PI / 180));
        xscrew[2] = (int) (this.x + 5 * ml / 14 * sin(fi * PI / 180) + this.width * cos(fi * PI / 180));
        yscrew[2] = (int) (this.y + 5 * ml / 14 * cos(fi * PI / 180) - this.width * sin(fi * PI / 180));
        xscrew[3] = (int) (this.x + 5 * ml / 14 * sin(fi * PI / 180) - this.width * cos(fi * PI / 180));
        yscrew[3] = (int) (this.y + 5 * ml / 14 * cos(fi * PI / 180) + this.width * sin(fi * PI / 180));
        g.setColor(new Color(54, 54, 54));
        g.fillPolygon(xscrew, yscrew, 4);
    }

    /**
     * запуск анимации
     */
    protected void startAnimation() {
        this.x0 = this.height * sin(fi * PI / 180);
        this.y0 = this.height * cos(fi / 180 * PI);
        runbool = true;
    }

    /**
     * остановка анимации
     */
    protected void stopAnimation() {
        t = 0;
        i = 0;
        runbool = false;
    }

    /**
     * построение кривых после остановки маятника
     */
    protected void plotGraph() {
        int n = 600;
        double[] time = new double[n];
        double[] xAxis = new double[n];
        double[] yAxis = new double[n];
        if (i < n) {
            t = t + dt;
            time[i] = t;
            xAxis[i] = x0;
            yAxis[i] = y0;
            t = t + dt;
            i++;
        } else {
            barsG.addCurve("X(t)", time, xAxis);
            barsG.repaint();
            stopAnimation();
        }
    }

    /**
     * построение кривых при добавлении каждой точки
     * построение кривых во время движения маятника
     */
    protected void plotGraph1() {

        PointStyle pointstyle = null;
        if (i < n) {
            t = t + dt;
            calcColor(i);
            pointstyle = new PointStyle(PointStyle.PointShape.RECTANGLE, 3.0, new Color(0, green, blue));
            barsG.points.addNewPointTo(t, this.x0, 0, i, pointstyle);
//            barsG.points.addNewPointTo(t, this.x0,0,i);
            barsG.points.addNewPointTo(t, this.y0, 1, i);

            barsG.repaint();
            i++;
        } else {
            stopAnimation();
        }
    }

    /**
     * вычисляет угол отклонения на каждом шаге методом Эйлера
     */
    protected void calcNewFi() {
        double g = 1000;//ускорение свободного падения в (см/с^2)
        double w = sqrt(g / this.height);
        double afi = -w * w * sin(fi * PI / 180);
        vfi = vfi + afi * dt;
        fi = fi + vfi * dt;
        fi0=fi;
    }

    private void calcColor(int i) {
        if (i == 0) {
            if (abs(fi) <= 90) {
                xmax = abs(this.x0);
            }
            if (abs(fi) > 90) {
                xmax = height;
            }
            blue = (int) round(120 + 110 * this.x0 / xmax);
            green = (int) round(120 - 110 * this.x0 / xmax);

        } else {
            if (this.x0 < barsG.points.Y.get(0).get(i-1)) {
                blue = (int) round(120 + 110 * this.x0 / xmax);
                green = (int) round(120 - 110 * this.x0 / xmax);
            }
            if (this.x0 >= barsG.points.Y.get(0).get(i-1)) {
                blue = (int) round(120 + 110 * this.x0 / xmax);
                green = (int) round(120 - 110 * this.x0 / xmax);
            }

        }
    }
}
