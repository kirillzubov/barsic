package pendulum;

import java.awt.Graphics;

/**
 * Класс материального тела. 
 * @author Anita
 */
public abstract class MaterialObject {

    /**
     * x-координата 
     */
    /**
     * y- кордината
     */
    protected int x, y;
    // Позиция по умолчанию         
    /**
     * 
     */
    /**
     * 
     */
    protected int defaultX, defaultY;
    /**
     *ширина материального объекта
     */
    protected int width;
    /**
     * высота материального объекта
     */
    protected int height;

    /**
     * конструктор материального объекта по умолчанию
     */
    public MaterialObject() {
    }

    /**
     * Конструктор материального объекта. Такой объект видим и имеет размеры.
     * @param x Х координата
     * @param y У координата
     * @param width Длина
     * @param height Ширина
    
     */
    public MaterialObject(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * Метод отрисовки материального объекта. 
     * @param graphics 
     */
    public final void paint(Graphics graphics) {
        paintSelf(graphics);
    }

    /**
     * 
     * @param g
     */
    protected abstract void paintSelf(Graphics g);
}