package pendulum;

/**
 *Класс сцены
 * @author Anita
 */
import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import javax.swing.JPanel;

/**
 * 
 * @author Анита
 */
public class Scene {

    /**
     * width-ширина сцены
     * height-высота сцены
     */
    protected int width = 640, height = 900;
    private List objects = new LinkedList();

    /**
     *конструктор по умолчанию
     */
    public Scene() {
    }

    /**
     * @param width Ширина
     * @param height Высота
     */
    public Scene(int width, int height) {
        this.width = width;
        this.height = height;
//        this.setDoubleBuffered(true);
    }

    /**
     * Добавление объекта на сцену
     * @param object 
     */
    public void addObject(MaterialObject object) {
        objects.add(object);
    }

    /**
     * Удаление объекта со сцены
     * @param object 
     */
    public void removeObject(MaterialObject object) {
        objects.remove(object);
    }

    /**
     * Отрисовка сцены - очистка фона и перерисовка всех объектов
     */
    
    public void paint(Graphics g) {
        g.clearRect(0, 0, width, height);
        //определим цвет сцены
        Color sceneColor = new Color(187, 255, 255);
        g.setColor(sceneColor);
        g.fillRect(0, 0, width, height);
        //цвет стола
        Color tableColor = new Color(238, 173, 15);
        //        координаты стола
        int[] xtable = new int[6];
        int[] ytable = new int[6];
        double w1 = width / 20;
        double h1 = height / 3;
        double h2 = h1 / 20;
        double w2 = width - w1;
        xtable[0] = 0;
        ytable[0] = height;
        xtable[1] = 0;
        ytable[1] = (int) (height - h2);
        xtable[2] = (int) w1;
        ytable[2] = (int) (height - h1);
        xtable[3] = (int) w2;
        ytable[3] = (int) (height - h1);
        xtable[4] = width;
        ytable[4] = (int) (height - h2);
        xtable[5] = width;
        ytable[5] = height;
        g.setColor(tableColor);
        g.fillPolygon(xtable, ytable, 6);//стол
        g.setColor(new Color(205, 133, 63));
        g.fillRect(xtable[1], ytable[1], width, height - ytable[1]);//полоска
        g.setColor(Color.orange);
        g.drawLine(xtable[1], height, width, height);

        ListIterator li = objects.listIterator();
        // Нужен последний элемент
        while (li.hasNext()) {
            MaterialObject obj = (MaterialObject) (li.next());
            obj.paintSelf(g);
        }
    }

    /**
     * запуск анимации
     */
    public void start() {
        ListIterator li = objects.listIterator();
        while (li.hasNext()) {
            if (li.next() instanceof Rod) {
                li.previous();
                Rod r = (Rod) li.next();
                r.startAnimation();
            }
        }
    }

    /**
     * остановка анимации
     */
    public void stop() {
        ListIterator li = objects.listIterator();
        while (li.hasNext()) {
            if (li.next() instanceof Rod) {
                li.previous();
                Rod r = (Rod) li.next();
                r.stopAnimation();
            }
        }
    }
}
