package pendulum;

import java.awt.Color;
import java.awt.Graphics;

/**
 *класс штатива
 *@author Anita
 */
public class Pendulum extends MaterialObject {

    Scene scene;
    /**
     * длина стержня
     */
    protected int l;

    /**
     * Конструктор штатива  по умолчанию
     */
    public Pendulum() {
        this.y = 825;
        this.x = 128;
        this.width = 128;
        this.l = 600;
        this.height = 150;
    }

    /**
     * Конструктор штатива 
     * @param scene 
     */
    public Pendulum(Scene scene) {
        this.scene = scene;
        this.y = 11 * this.scene.height / 12;
        this.x = this.scene.width / 5;
        this.width = this.scene.width / 5;
        this.l = 2 * this.scene.height / 3;
        this.height = scene.height / 6;



    }

    /**
     * переопределенный метод отрисовки 
     * @param g
     */
    @Override
    public void paintSelf(Graphics g) {
        //  отрисовка подставки
        int[] xpendulum = new int[10];
        int[] ypendulum = new int[10];
        xpendulum[0] = (int) x;
        ypendulum[0] = (int) y;
        ypendulum[1] = (int) (y - height);
        double lstend = scene.width / 34;
        xpendulum[1] = (int) (x + lstend);
        xpendulum[2] = (int) (x + width - lstend);
        ypendulum[2] = (int) (y - height);
        xpendulum[3] = (int) (x + width);
        ypendulum[3] = (int) y;
        xpendulum[4] = (int) (x + width);
        ypendulum[4] = (int) (y + width / 10);
        xpendulum[5] = (int) (xpendulum[4] - lstend);
        ypendulum[5] = ypendulum[4];
        xpendulum[6] = xpendulum[5];
        ypendulum[6] = (int) (ypendulum[5] - width / 20);
        xpendulum[7] = xpendulum[1];
        ypendulum[7] = ypendulum[6];
        xpendulum[8] = xpendulum[1];
        ypendulum[8] = ypendulum[5];
        xpendulum[9] = xpendulum[0];
        ypendulum[9] = (int) ypendulum[8];
        Color pend1Color = new Color(54, 54, 54);//grey 21
        g.setColor(pend1Color);
        g.fillPolygon(xpendulum, ypendulum, 10);
        g.setColor(Color.BLACK);//для черных линий
        g.drawPolyline(xpendulum, ypendulum, 6);
        g.drawLine(xpendulum[6], ypendulum[6], xpendulum[7], ypendulum[7]);
        g.drawLine(xpendulum[8], ypendulum[8], xpendulum[9], ypendulum[9]);
        Color pend2Color = new Color(156, 156, 156);//grey 61 блики
        g.setColor(pend2Color);
        g.fillRect(xpendulum[0], ypendulum[0], width, 1);
        g.drawLine(xpendulum[5], ypendulum[5], xpendulum[6], ypendulum[6]);
        g.drawLine(xpendulum[9], ypendulum[9], xpendulum[0], ypendulum[0]);
        g.drawLine(xpendulum[7], ypendulum[7], xpendulum[8], ypendulum[8]);
        //рисование стойки
        int xrack[] = new int[4];
        int yrack[] = new int[4];
        xrack[0] = (int) (x + width / 2 - width / 16);
        yrack[0] = (int) (y - 4 * height / 5);
        xrack[1] = (int) (x + width / 2 - width / 16);
        yrack[1] = (int) (y - 4 * height / 5 - l);
        xrack[2] = (int) (x + width / 2 + width / 16);
        yrack[2] = (int) (y - 4 * height / 5 - l);
        xrack[3] = (int) (x + width / 2 + width / 16);
        yrack[3] = (int) (y - 4 * height / 5);
        g.setColor(Color.BLACK);
        //основание стойки
        g.fillOval((int) (xrack[0] - width / 32), (int) (yrack[0] + height / 20), (int) (3 * width / 16), (int) (height / 10));
        g.fillOval((int) (xrack[0] - width / 32), yrack[0] - height / 20, (int) (3 * width / 16), (int) (height / 10));
        g.fillRect((int) (xrack[0] - width / 32), yrack[0], (int) (3 * width / 16), (int) (height / 10));
        g.drawLine((int) (xrack[0] - width / 32), (int) (yrack[0] + height / 10), (int) (xrack[0] - width / 32), yrack[0]);
        g.drawLine((int) (xrack[3] + width / 32), (int) (yrack[3] + height / 10), (int) (xrack[3] + width / 32), yrack[3]);
        //сама стойка
        g.setColor(pend2Color);
        g.fillPolygon(xrack, yrack, 4);
        g.fillOval(xrack[0], (int) (yrack[1] - l / 60), (int) (width / 8), (int) (l / 30));
        g.fillOval(xrack[0], (int) (yrack[0] - l / 60), (int) (width / 8), (int) (l / 30));
        //блики на стойке
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect((int) (xrack[1] + width / 32), yrack[1], (int) (width / 32), l);
        g.setColor(pend1Color);
        g.drawArc(xrack[0], (int) (yrack[1] - l / 60), (int) (width / 8), (int) (l / 30), 0, 180);
        g.drawRect(xrack[1], yrack[1], (int) (width / 128), l);
        g.drawRect((int) (xrack[2] - width / 64), yrack[2], (int) (width / 64), l);
        //держатель-левая часть
        int[] xleft = new int[4];
        int[] yleft = new int[4];
        xleft[0] = (int) (xrack[1]);
        yleft[0] = (int) (yrack[1] + l / 8);
        xleft[1] = (int) (xrack[1]);
        yleft[1] = (int) (yleft[0] - l / 32);
        xleft[2] = (int) (xleft[1] - width * Math.cos(15 * Math.PI / 180) / 2);
        yleft[2] = (int) (yleft[1] - width * Math.sin(15 * Math.PI / 180) / 2);
        xleft[3] = (int) (xleft[2] - l * Math.cos(15 * Math.PI / 180) * Math.sin(15 * Math.PI / 180) / 32);
        yleft[3] = (int) (yleft[2] + l * Math.pow((Math.cos(15 * Math.PI / 180)), 2) / 32);
        g.setColor(pend2Color);
        g.fillPolygon(xleft, yleft, 4);


        //держатель-правая  часть
        int[] xright = new int[4];
        int[] yright = new int[4];
        xright[0] = (int) (xleft[0] + width / 8);
        yright[0] = (int) (yleft[0] + width * Math.tan(15 * Math.PI / 180) / 8);
        xright[1] = xright[0];
        yright[1] = (int) (yright[0] - l / 32);
        xright[2] = (int) (xright[1] + 1.2 * width);
        yright[2] = (int) (yright[1] + 1.2 * width * Math.tan(15 * Math.PI / 180));
        xright[3] = (int) (xright[2] - l * Math.cos(15 * Math.PI / 180) * Math.sin(15 * Math.PI / 180) / 32);
        yright[3] = (int) (yright[2] + l * Math.pow((Math.cos(15 * Math.PI / 180)), 2) / 32);
        g.setColor(pend2Color);
        g.fillPolygon(xright, yright, 4);
        //округление концов держателя
        int xc2, yc2, r2;
        int xc1 = (int) ((xleft[2] + xleft[3]) / 2);
        int yc1 = (int) ((yleft[2] + yleft[3]) / 2);
        int r1 = (int) (Math.sqrt(Math.pow(xleft[3] - xleft[2], 2) + Math.pow(yleft[3] - yleft[2], 2)) / 2);
        int xl1 = xc1 - r1;
        int yl1 = yc1 - r1;
        g.setColor(pend2Color);
        g.fillOval(xl1, yl1, 2 * r1, 2 * r1);//округление левого  конца
        xc2 = (int) ((xright[2] + xright[3]) / 2);
        yc2 = (int) ((yright[2] + yright[3]) / 2);
        r2 = (int) (Math.sqrt(Math.pow(xright[3] - xright[2], 2) + Math.pow(yright[3] - yright[2], 2)) / 2);
        int xl2 = xc2 - r2;
        int yl2 = yc2 - r2;
        g.fillOval(xl2, yl2, 2 * r2, 2 * r2);//округление правого  конца

        // блики и тени на левой части
//       верхняя полоска
        g.setColor(pend1Color);
        g.drawLine(xleft[1], yleft[1], xleft[2], yleft[2]);
//        нижняя полоска
        g.drawLine(xleft[3], yleft[3], xleft[0], yleft[0]);
        //светлая полоска
        double xd = xleft[2];
        double yd = yleft[2] + 2 * l / 96;
        double h2 = (yd - yleft[2]) * Math.cos(15 * Math.PI / 180);
        double hk = h2 * Math.sin(15 * Math.PI / 180);
        double k2 = h2 * Math.cos(15 * Math.PI / 180);
        int xh = (int) (xd - hk);
        int yh = (int) (yleft[2] + k2);
        int xm = xleft[0];
        int ym = (int) (yleft[1] + (yd - yleft[2]));
        g.setColor(new Color(190, 190, 190));
        g.drawLine(xh, yh, xm, ym);
        // блики и тени на правой части
        g.setColor(pend1Color);
        g.drawLine(xright[1], yright[1], xright[2], yright[2]);
        g.drawLine(xright[3], yright[3], xright[0], yright[0]);
//        светлая полоска
        int xm_ = xright[0];
        double ym_ = ym + width * Math.tan(15 * Math.PI / 180) / 8;
//        int ym_=yright[0] = (int) (yright[3] - (yright[0]-xm_));
        int xh_ = xright[3];
//        int yh_=(int)(ym_+(xh_-xm_)*Math.tan(15*Math.PI/180));
        int yh_ = (int) (yright[3] - (yright[0] - ym_));
        g.setColor(Color.LIGHT_GRAY);
        g.drawLine(xh_, yh_, xm_, (int) ym_);
//   крепеж
        g.setColor(pend1Color);
        g.fillRect(xleft[1], yleft[1] - l / 256, width / 32, 5 * l / 128);
        g.fillRect(xright[1] - width / 24, yright[1] - l / 256, width / 16, 5 * l / 128);
        g.setColor(Color.black);
        g.fillOval((int) (xright[0] - width / 16), (int) (yright[0] - l / 64 - l / 96), (int) (l / 48), (int) (l / 48));
        g.setColor(Color.LIGHT_GRAY);
        int[] xFixing = new int[4];
        int[] yFixing = new int[4];
        xFixing[0] = (int) (xright[0] - width / 16 - l / 96);
        yFixing[0] = (int) (yright[0] - l / 64 - l / 48);
        xFixing[1] = (int) (xright[0] - width / 16 - l / 48);
        yFixing[1] = (int) (yright[0] - l / 64 - l / 96);
        xFixing[2] = (int) (xright[0] - width / 16 + l / 48);
        yFixing[2] = (int) (yright[0] - l / 64 + l / 96);
        xFixing[3] = (int) (xright[0] - width / 16 + l / 96);
        yFixing[3] = (int) (yright[0] - l / 64 + l / 48);
        g.fillPolygon(xFixing, yFixing, 4);
        g.setColor(Color.BLACK);
        g.drawPolygon(xFixing, yFixing, 4);


    }
}
