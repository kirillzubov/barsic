package pendulum;

/**
 *
 * @author Anita
 */
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import java.awt.Panel;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import jbarsicgraph.JBarsicGraph;

/**
 * %
 * @author Анита
 */
public class JLabApplet extends JApplet {

    @Override
    public String getAppletInfo() {
        return "Name: JLabApplet\r\n"
                + "Author: Anita Vasilyeva\r\n"
                + "Created with NetBeans Version 7.0.1";
    }
    // Сцена, где все происходит
    private Scene scene = null;
    // Размеры
    private int width = 1280, height = 960;
    // Промежуток между кадрами анимации (задает частоту анимации)
    private final int TIMER_DELAY = 100;
    private Pendulum pendulum = null;
    private Rod rod = null;
    private Body body = null;
    Panel bPanel1 = new Panel();//панель с кнопками и переключателями под сценой
    Panel bPanel2 = new Panel();//панель с переключателями под компонентом графики
    Panel bPanel3 = new Panel();//панель с кнопками и текстовыми полями под компонентом графики
    JButton startButton = new JButton("старт");
    JButton stopButton = new JButton("стоп");
//    Button resetButton = new Button("сброс");
    JButton cleanButton = new JButton("очистить");
    JButton updateButton = new JButton("обновить");
    JLabel length1 = new JLabel("длина");
    JTextField lengthEdit = new JTextField("75", 5);
    JLabel length2 = new JLabel("см  ");
    JLabel angle1 = new JLabel("угол");
    JTextField angleEdit = new JTextField("45", 5);
    JLabel angle2 = new JLabel("градус(ов)");
    JRadioButton radScale1 = new JRadioButton("автомасштабирование");
    JRadioButton radScale2 = new JRadioButton("без масштабирования");
    ButtonGroup bGroup = new ButtonGroup();
    JLabel xMinLabel1 = new JLabel("tMin  ");
    JTextField xMinEdit = new JTextField("0", 7);
    JLabel xMinLabel2 = new JLabel(" c ");
    JLabel xMaxLabel1 = new JLabel("  tMax  ");
    JTextField xMaxEdit = new JTextField("40", 7);
    JLabel xMaxLabel2 = new JLabel(" с ");
    JLabel yMinLabel1 = new JLabel("xyMin");
    JTextField yMinEdit = new JTextField("-55", 7);
    JLabel yMinLabel2 = new JLabel("см");
    JLabel yMaxLabel1 = new JLabel("xyMax");
    JTextField yMaxEdit = new JTextField("80", 7);
    JLabel yMaxLabel2 = new JLabel("см");
    private JBarsicGraph barsG;
    /**
     * угол отклонения стержня rod
     */
    public double fi;
    /**
     * длина стержня rod
     */
    public double l = 75;
    /**
     * начальный угол отклонения стержня 
     */
    public double fi0;
    private Image buffer;
    /* Создаём анонимный внутренний класс для каждого ActionListener */
    Timer timer = new Timer(TIMER_DELAY, new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            paint(getGraphics());
            if (rod.runbool) {
                rod.calcNewFi();
                rod.plotGraph1();
            }

        }
    });

//   инициализация апплета
    @Override
    public void init() {
        super.init();
        setSize(width, height);
        buffer = new BufferedImage(width / 2, (int) (15 * height / 16), BufferedImage.TYPE_INT_RGB);
        setLayout(null);
        /*сцена для процесса*/
        scene = new Scene((int) (width / 2), (int) (15 * height / 16));
        /*создаем наши объекты*/
        pendulum = new Pendulum(scene);
        initJBarsG((int) (width / 2), (int) (15 * height / 16));
        rod = new Rod(l, fi, pendulum, barsG);
        body = new Body(rod, pendulum);
        //добавляем объекты на сцену
        scene.addObject(pendulum);
        scene.addObject(rod);
        scene.addObject(body);
//        scene.setBounds(0, 0, (int) (width / 2), (int) (15 * height / 16));
//        add(scene);
        barsG.setBounds((int) (width / 2), 0, (int) (width / 2), (int) (15 * height / 16));
        add(barsG);
        bPanel1.setBackground(new Color(238, 118, 33));
        bPanel1.setBounds(0, (int) (15 * height / 16), (int) (width / 2), (int) (height / 16));
        bPanel1.add(length1);
        bPanel1.add(lengthEdit);
        bPanel1.add(length2);
        bPanel1.add(angle1);
        bPanel1.add(angleEdit);
        bPanel1.add(angle2);
        bPanel1.add(updateButton);
        bPanel1.add(startButton);
        bPanel1.add(stopButton);
        bPanel1.add(cleanButton);
        add(bPanel1);

        bPanel2.setBackground(new Color(238, 118, 33));
        bPanel2.setBounds(width / 2 + 10, 15 * height / 16, width / 4, height / 16);
        radScale1.setSelected(true);
        bPanel2.add(radScale1);
        bPanel2.add(radScale2);
        bGroup.add(radScale1);
        bGroup.add(radScale2);
        add(bPanel2);

        bPanel3.setBackground(new Color(238, 118, 33));
        bPanel3.setBounds(3 * width / 4 + 10, 15 * height / 16, width / 4 - 10, height / 16);
        bPanel3.add(xMinLabel1);
        bPanel3.add(xMinEdit);
        bPanel3.add(xMinLabel2);
        bPanel3.add(xMaxLabel1);
        bPanel3.add(xMaxEdit);
        bPanel3.add(xMaxLabel2);
        bPanel3.add(yMinLabel1);
        bPanel3.add(yMinEdit);
        bPanel3.add(yMinLabel2);
        bPanel3.add(yMaxLabel1);
        bPanel3.add(yMaxEdit);
        bPanel3.add(yMaxLabel2);
        add(bPanel3);

        startButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                timer.start();
                scene.start();
            }
        });

        stopButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ev) {
                scene.removeObject(rod);
                scene.removeObject(body);
                rod = new Rod(l, fi, pendulum, barsG);
                body = new Body(rod, pendulum);
                scene.addObject(rod);
                scene.addObject(body);
                timer.stop();
                scene.stop();
            }
        });

        updateButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                timer.start();
                fi = Double.parseDouble(angleEdit.getText());
                fi0 = fi;
                l = Double.parseDouble(lengthEdit.getText());
                scene.removeObject(rod);
                scene.removeObject(body);
                rod = new Rod(l, fi, pendulum, barsG);
                body = new Body(rod, pendulum);
                scene.addObject(rod);
                scene.addObject(body);
                getGraphics();
                if (radScale1.isSelected()) {
                    barsG.needScale = true;

                }
                if (radScale2.isSelected()) {
                    barsG.needScale = false;
                    double xMin = Double.parseDouble(xMinEdit.getText());
                    double yMin = Double.parseDouble(yMinEdit.getText());
                    double xMax = Double.parseDouble(xMaxEdit.getText());
                    double yMax = Double.parseDouble(yMaxEdit.getText());
                    barsG.dataArea = new Rectangle2D.Double(xMin, yMin, xMax - xMin, yMax - yMin);
                    barsG.repaint();

                }
            }
        });
        cleanButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                barsG.cleanCurves();
                barsG.repaint();
            }
        });


    }

    private void initJBarsG(double w, double h) {
        barsG = new JBarsicGraph();
        barsG.setPreferredSize(new Dimension((int) w, (int) h));
        barsG.getAxisX().setTitle("t");
        barsG.getAxisX().setUnits("c");
        barsG.getAxisY().setTitle("X,Y");
        barsG.getAxisY().setUnits("см");
        barsG.setVisible(true);
    }

    /**
     * Отрисовка апплета, делегирование её на сцене.
     * @param g Холст апплета.
     */
    @Override
    public void paint(Graphics g) {
//        super.paint(g);
//        scene.paint(g);
        scene.paint(buffer.getGraphics());
        g.drawImage(buffer, 0, 0, this);
        barsG.repaint();
    }

//    запуск анимации
    @Override
    public void start() {
        timer.start();
    }
//    остановка анимации

    @Override
    public void stop() {
        timer.stop();
    }

//    метод, выполняющийся при уничтожении апплета
    @Override
    public void destroy() {
        scene = null;
        bPanel1 = null;
        bPanel2 = null;
        barsG = null;
    }
}
