package pendulum;

import java.awt.Color;
import java.awt.Graphics;
import static java.lang.Math.*;

/**
 * Класс материального тела
 * @author Anita
 */
public final class Body extends MaterialObject {

    /**
     * стержень
     */
    protected Rod rod;
    /**
     * штатив
     */
    protected Pendulum pendulum;

    /**
     * конструктор тела по умолчанию
     */
    public Body() {
        this.width = 34;
        this.height = 11;

    }

    /**
     * конструктор тела
     * @param rod
     * @param pendulum
     */
    public Body(Rod rod, Pendulum pendulum) {
        this.rod = rod;
        this.pendulum = pendulum;
        this.width = 5 * pendulum.width / 18;
        this.height = 5 * pendulum.width / 54;

    }

    ;
     @Override
    public void paintSelf(Graphics g) {
        this.x = (int) (rod.x + 4 * rod.height * sin(rod.fi * 3.14 / 180));
        this.y = (int) (rod.y + 4 * rod.height * cos(rod.fi * 3.14 / 180));
        int[] xbody = new int[4];
        int[] ybody = new int[4];
        xbody[0] = (int) (this.x - this.width / 2 * cos(rod.fi * PI / 180));
        ybody[0] = (int) (this.y + this.width / 2 * sin(rod.fi * PI / 180));
        xbody[1] = (int) (this.x + this.width / 2 * cos(rod.fi * PI / 180));
        ybody[1] = (int) (this.y - this.width / 2 * sin(rod.fi * PI / 180));
        xbody[2] = (int) (this.x + this.width / 2 * sin(rod.fi * PI / 180) + this.width / 2 * cos(rod.fi * PI / 180));
        ybody[2] = (int) (this.y + this.width / 2 * cos(rod.fi * PI / 180) - this.width / 2 * sin(rod.fi * PI / 180));
        xbody[3] = (int) (this.x + this.width / 2 * sin(rod.fi * PI / 180) - this.width / 2 * cos(rod.fi * PI / 180));
        ybody[3] = (int) (this.y + this.width / 2 * cos(rod.fi * PI / 180) + this.width / 2 * sin(rod.fi * PI / 180));
        g.setColor(new Color(0, 205, 0));
        g.fillPolygon(xbody, ybody, 4);

        // xbodyright,xbodyleft,ybodyright,ybodyleft - точки пересечения стержня и грузика         
        int xbodyright = (int) (this.x + rod.width / 2 * cos(rod.fi * PI / 180));
        int xbodyleft = (int) (this.x - rod.width / 2 * cos(rod.fi * PI / 180));
        int ybodyleft = (int) (this.y + rod.width / 2 * sin(rod.fi * PI / 180));
        int ybodyright = (int) (this.y - rod.width / 2 * sin(rod.fi * PI / 180));
        g.setColor(new Color(0, 139, 0));
        g.drawLine(xbody[0], ybody[0], xbodyleft, ybodyleft);
        g.drawLine(xbody[1], ybody[1], xbodyright, ybodyright);
        g.drawLine(xbody[0], ybody[0], xbody[3], ybody[3]);
        g.drawLine(xbody[1], ybody[1], xbody[2], ybody[2]);
        g.drawLine(xbody[2], ybody[2], xbody[3], ybody[3]);
    }
}
