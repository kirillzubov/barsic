/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jbarsicgraph;

/**
 *
 * @author Max
 */

import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D;

/**
 * 
 * @author Анита
 */
public class GeomUtils {
    /// <summary>
        /// Checks is the point p in the rect
        /// </summary>
    /**
     * 
     * @param p
     * @param rect
     * @return
     */
    public static boolean isInBox(Point2D p, Rectangle2D rect)
        {
            return (p.getX() >= rect.getMinX()) && (p.getX() <= rect.getMaxX()) && (p.getY() >= rect.getMinY()) && (p.getY() <= rect.getMaxY());
        }

        /// <summary>
        /// Returns rectangle which center is the center point
        /// </summary>
        /**
         * 
         * @param center
         * @param rect
         * @return
         */
        public static Rectangle2D.Double MoveRectOnCenter(Point2D center, Rectangle2D rect)
        {
            return new Rectangle2D.Double(center.getX() - rect.getWidth() * 0.5, center.getY() - rect.getHeight() * 0.5, rect.getWidth(), rect.getHeight());
        }

}
