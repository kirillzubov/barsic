/*
 * SciAxis.java
 */
package jbarsicgraph.axis;

import jbarsicgraph.*;
import java.awt.FontMetrics;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D;
import java.awt.geom.Line2D;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.AttributedString;
import java.awt.font.TextAttribute;

/**
 *
 * Scientific axis class
 */
public class SciAxis extends Axis {

    /**
     * Enumeration of the type of axis
     */
    public enum AxisType{

        /**
         * Axis is horisontal
         */
        Horizontal,
        /**
         * 
         */
        HorizontalAlt,
        /**
         * 
         */
        Vertical,
        /**
         * 
         */
        VerticalAlt,
        /**
         * 
         */
        Custom
    }

        /**
     * power of ten for carrying out
     *
     * if maxOrder=3
     * 500->500 currentOrder=0
     * 1500->150.0 currentOrder=1
     * 15000000->150.0 currentOrder=5
     */
    protected int maxOrder = 3;
    /**
     * 
     */
    public enum TenPowerPosition{
        /**
         * 
         */
        onTitle,
        /**
         * 
         */
        outOfTitle
    }
    
    /**
     * 
     */
    protected TenPowerPosition tenPowerPosition=TenPowerPosition.onTitle; 
    /**
     * Enables AutoOrdering
     * @see       #maxOrder
     */
    protected boolean autoOrderEnabled = true;
    AxisType axisType = AxisType.Horizontal;
    /**
     * 
     */
    public SciAxis(){}

    /**
     * 
     * @param type
     */
    public SciAxis(AxisType type)
    {
        setAxisType(type);
    }
    

    /**
     * 
     * @return
     */
    public int getMaxOrder() {
        return this.maxOrder;
    }

    /**
     * 
     * @param value
     */
    public void setMaxOrder(int value) {
        this.maxOrder=value;
    }
      
    
    /**
     * 
     * @param value
     */
    public void setTenPowerPosition(TenPowerPosition value)
     {
         tenPowerPosition=value;
     }     
     
    /**
     * 
     * @return
     */
    public TenPowerPosition getTenPowerPosition()
     {
         return tenPowerPosition;
     }
   

    /**
     * 
     * @return
     */
    public boolean getAutoOrderEnabled() {
        return this.autoOrderEnabled;
    }

    /**
     * 
     * @param value
     */
    public void setAutoOrderEnabled(boolean value) {
        this.autoOrderEnabled=value;
    }

    /**
     * 
     * @param type
     */
    public void setAxisType(AxisType type)
    {
        this.axisType=type;
        switch(axisType)
        {
            case Horizontal:
                angle=0;
                digitsOrientation=AxisValue.Orientation.Horizontal;
                alignW=AxisValue.AlignW.Center;
                alignH=AxisValue.AlignH.Bottom;               
                break;
            case Vertical:
                angle=0.5*Math.PI;
                digitsOrientation=AxisValue.Orientation.VerticalAlt;
                alignW=AxisValue.AlignW.Left;
                alignH=AxisValue.AlignH.Center;
                this.reverse=true;
                break;
           case HorizontalAlt:
                angle=Math.PI;
                digitsOrientation=AxisValue.Orientation.HorizontalAlt;
                alignW=AxisValue.AlignW.Center;
                alignH=AxisValue.AlignH.Top;
                this.reverse=true;
                break;
            case VerticalAlt:
                angle=1.5*Math.PI;
                digitsOrientation=AxisValue.Orientation.Vertical;
                alignW=AxisValue.AlignW.Right;
                alignH=AxisValue.AlignH.Center;
                //this.reverse=true;
                break;
        }
    }

   
    /**
     * 
     * @param p
     */
    public void setTopLeft(Point2D p)
    {
        double a_x=0;
        double a_y=0;
        double b_x=width*Math.cos(angle);
        double b_y=width*Math.sin(angle);
        double c_x=width*Math.cos(angle)-height*Math.sin(angle);
        double c_y=width*Math.sin(angle)+height*Math.cos(angle);
        double d_x=-height*Math.sin(angle);
        double d_y=height*Math.cos(angle);


        if((angle<0)||(angle>2*Math.PI))
            this.angle=angle-Math.floor(angle/(2*Math.PI))*2*Math.PI;

        if((angle>=0)&&(angle<=0.5*Math.PI))
        {
            pos=new Point2D.Double(p.getX()-d_x,p.getY()-a_y);
            return;
        }

        if((angle>=0.5*Math.PI)&&(angle<=Math.PI))
        {
            pos=new Point2D.Double(p.getX()-c_x,p.getY()-d_y);
            return;
        }

        if((angle>=Math.PI)&&(angle<=1.5*Math.PI))
        {
            pos=new Point2D.Double(p.getX()-b_x,p.getY()-c_y);
            return;
        }

        if((angle>=1.5*Math.PI)&&(angle<=2.0*Math.PI))
        {
            pos=new Point2D.Double(p.getX()-a_x,p.getY()-b_y);
            return;
        }
       
        pos=new Point2D.Double(pos.getX()-a_x,pos.getY()-b_y);

    }

    /**
     * 
     * @return
     */
    public Rectangle2D getBorders()
    {
        double a_x=0;
        double a_y=0;
        double b_x=width*Math.cos(angle);
        double b_y=width*Math.sin(angle);
        double c_x=width*Math.cos(angle)-height*Math.sin(angle);
        double c_y=width*Math.sin(angle)+height*Math.cos(angle);
        double d_x=-height*Math.sin(angle);
        double d_y=height*Math.cos(angle);


        if((angle<0)||(angle>2*Math.PI))
            this.angle=angle-Math.floor(angle/(2*Math.PI))*2*Math.PI;

        System.out.println("angle="+angle);
        if((angle>=0)&&(angle<=0.5*Math.PI))
        {
            return new Rectangle2D.Double(pos.getX()+d_x,pos.getY()+a_y,b_x-d_x,c_y-a_y);
        }

        if((angle>=0.5*Math.PI)&&(angle<=Math.PI))
        {
            return new Rectangle2D.Double(pos.getX()+c_x,pos.getY()+d_y,a_x-c_x,b_y-d_y);
        }

        if((angle>=Math.PI)&&(angle<=1.5*Math.PI))
        {
            return new Rectangle2D.Double(pos.getX()+b_x,pos.getY()+c_y,d_x-b_x,a_y-c_y);
        }

        if((angle>=1.5*Math.PI)&&(angle<=2.0*Math.PI))
        {
            return new Rectangle2D.Double(pos.getX()+a_x,pos.getY()+b_y,c_x-a_x,d_y-b_y);
        }
        return new Rectangle2D.Double(pos.getX()+a_x,pos.getY()+b_y,c_x-a_x,d_y-b_y);
    }


     private int currentOrder = 0;


    /// <summary>
    /// sets transformations for convenient using
    /// </summary>
    /// <param name="g">Graphics context</param>
    private void setTransforms(Graphics2D g) {
        g.translate(pos.getX(), pos.getY());
        g.rotate(angle);
        //g.Clip = new Region(new Rectangle(0, 0, (int)_width, (int)_height));
        }


    private AxisValue getDefaultAxisValue()
    {
        AxisValue val=new AxisValue();

        val.setOrientation(digitsOrientation);
        val.alignW=this.alignW;
        val.alignH=this.alignH;
        val.setfont(this.font);
        val.setForeground(this.foreground);
        
        return val;
    }

    /**
     * Draws scale with values
     * @param g Graphics context
     * @param dataSegment data segment
     * @param largeSteps set og large graduation marks
     * @param reverse enables reverse order of graduation marks
     */
    private void drawScale(Graphics2D g, Segment dataSegment, BigDecimal[] largeSteps, boolean reverse) {

        //System.out.println("DrawScale()");
        //System.out.println( new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date()));

        if ((largeSteps == null)) {
            System.err.println("largeSteps is null");
            return;
        }
        //saving old transform
        AffineTransform save = g.getTransform();

        Segment renderSegment = new Segment(margin.getLeft(), this.getWidth() - margin.getRight() );
        renderSegment.setTop(0.0);

        //applying the style
        int isUp = 0, isDown = 0;

        final int BIG_MARKS_SIZE = 10;
        final int SMALL_MARKS_SIZE = 4;

        switch (marksType) {
            case UpDown:
                isUp = 1;
                isDown = 1;
                break;
            case UpOnly:
                isUp = 1;
                isDown = 0;
                break;
            case DownOnly:
                isUp = 0;
                isDown = 1;
                break;
        }

        marksType = MarksType.DownOnly;

        AxisValue val=getDefaultAxisValue();

        if ((largeSteps.length > 0) && (dataSegment.getWidth() != 0)) {

            double scaleFactor = renderSegment.getWidth() / dataSegment.getWidth();
            int NLargeMarks = largeSteps.length;

            double delta = largeSteps[1].subtract(largeSteps[0]).doubleValue();

            double smallStep = (largeSteps.length > 1) ? (scaleFactor * java.lang.Math.abs(delta) / (smallMarksCount + 1)) : (2.0 * scaleFactor * dataSegment.getWidth());

            int rightIndex = 0;
            int leftindex = NLargeMarks - 1;
            int cullIndex = -1;

            for (int i = NLargeMarks - 1; i >= 0; i--) {
                double x = scaleFactor * (largeSteps[i].doubleValue() - dataSegment.getLeft());

                if (x >= 0 && x <= renderSegment.getWidth()) {
                    rightIndex = i;
                    break;
                }
            }

            for (int i = 0; i < NLargeMarks; i++) {
                double x = scaleFactor * (largeSteps[i].doubleValue() - dataSegment.getLeft());
                if (x >= 0 && x <= renderSegment.getWidth()) {
                    leftindex = i;
                    break;
                }
            }
           
            g.draw(new Line2D.Double(renderSegment.getLeft(), renderSegment.getTop(), renderSegment.getRight(), renderSegment.getTop()));

            if (!reverse) {
                double leftlimit = -margin.getLeft();
                double rightlimit = renderSegment.getWidth()+margin.getRight();

                g.translate(renderSegment.getLeft(), renderSegment.getTop());

                g.setColor(Color.BLACK);

                for (int i = 0; i < NLargeMarks; i++) {
                    double x = scaleFactor * (largeSteps[i].doubleValue() - dataSegment.getLeft());

                    if (x >= 0 && x <= renderSegment.getWidth()) {
                        g.draw(new Line2D.Double(x, BIG_MARKS_SIZE * isDown, x, -BIG_MARKS_SIZE * isUp));
                       
                        if (i != cullIndex) {
                            if(this.showGridLines)
                            {
                                g.setColor(this.gridLinesColor);
                                g.draw(new Line2D.Double(x,0,x,-this.gridLinesLength));
                            }
                            val.setValue(largeSteps[i].toString());
                            val.setOrientation(this.digitsOrientation);
                            val.draw(g, new Point2D.Double(x, BIG_MARKS_SIZE + 5), leftlimit, rightlimit, reverse);
                        }
                    }

                    for (int j = 1; j <= smallMarksCount; j++) {
                        double x_small = x + j * smallStep;
                        if (x_small >= 0 && x_small <= renderSegment.getWidth()) {
                            g.draw(new Line2D.Double(x_small, -SMALL_MARKS_SIZE * isUp, x_small, SMALL_MARKS_SIZE * isDown));
                        }
                    }
                }
            } else {
                g.translate(renderSegment.getRight(), renderSegment.getTop());               

                double leftlimit = margin.getRight();
                double rightlimit = -renderSegment.getWidth() - margin.getLeft() ;

                for (int i = 0; i < NLargeMarks; i++) {
                    double x = scaleFactor * (largeSteps[i].doubleValue() - dataSegment.getLeft());

                    if (x >= 0 && x <= renderSegment.getWidth()) {
                        g.draw(new Line2D.Double(-x, BIG_MARKS_SIZE * isDown, -x, -BIG_MARKS_SIZE * isUp));
                        if (i != cullIndex) {
                            if(this.showGridLines)
                            {
                                g.setColor(this.gridLinesColor);
                                g.draw(new Line2D.Double(-x,0,-x,-this.gridLinesLength));
                            }
                            val.setValue(largeSteps[i].toString());
                            val.setOrientation(this.digitsOrientation);
                            val.draw(g, new Point2D.Double(-x, 15), leftlimit, rightlimit, reverse);
                        }                        
                    }

                    for (int j = 1; j <= smallMarksCount; j++) {
                        double x_small = x + j * smallStep;
                        if (x_small >= 0 && x_small <= renderSegment.getWidth()) {
                            g.draw(new Line2D.Double(-x_small, -SMALL_MARKS_SIZE * isUp, -x_small, SMALL_MARKS_SIZE * isDown));
                        }
                    }
                }
            }
        }else System.err.println("no data to render");

        java.awt.Font saveFont = g.getFont();

        g.setFont(saveFont);

        g.setTransform(save);
    }

  
    private Segment getDataSegmentAfterScaling(Segment dataSegment) {
        Segment ans = dataSegment;

        BigDecimal left = new BigDecimal(dataSegment.getLeft());
        BigDecimal right = new BigDecimal(dataSegment.getRight());

        int leftOrder = left.precision() - left.scale() - 1;
        int rightOrder = right.precision() - right.scale() - 1;
               currentOrder = 0;
        if (leftOrder >= maxOrder || rightOrder >= maxOrder) {
            currentOrder = (int) Math.round(Math.max(leftOrder, rightOrder)) - maxOrder + 1;

            double mult = Math.pow(10, -currentOrder);
            ans.setLeft(dataSegment.getLeft() * mult);
            ans.setRight(dataSegment.getRight() * mult);
        }else
        {
            if(leftOrder <= -maxOrder && rightOrder <= -maxOrder)
            {
                currentOrder = (int) Math.round(Math.max(leftOrder, rightOrder)) ;
                double mult = Math.pow(10, -currentOrder);

                ans.setLeft(dataSegment.getLeft() * mult);
                ans.setRight(dataSegment.getRight() * mult);

            }

        }

         return ans;

    }

    /**
     * Main paint event. Draws the axis
     * @param g Graphics context
     */
    @Override
    public void draw(Graphics2D g) {
        
        if (autoOrderEnabled) 
            dataSegment=this.getDataSegmentAfterScaling(dataSegment);

        //System.out.println("Draw()");
        draw(g, dataSegment);
    }
    /**
     *  is it necessary to hide middle value
     */
    private boolean shouldCullMiddle = false;

    /**
     * (for debugging)
     * draws axis bound rectangle(0,0,w,h)
     * @param g Graphics context
     */
    private void drawBorders(Graphics2D g) {
        Stroke savedStroke = g.getStroke();
        Color savedColor = g.getColor();

        float dash[] = {1.0f};
        g.setColor(Color.DARK_GRAY);
        g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));
        g.drawRect(0, 0, (int) this.getWidth(), (int) getHeight());
        g.setStroke(savedStroke);
        g.setColor(savedColor);

    }

    /**
     * Main paint event. Draws the axis
     * @param g Graphics context
     * @param dataSegment data segment
     */
    public void draw(Graphics2D g, Segment dataSegment) {
        AffineTransform save = g.getTransform();
        setTransforms(g);
        //drawBorders(g);

        Segment renderSegment = new Segment(margin.getLeft(), this.getWidth() - margin.getRight());
        renderSegment.setTop(10.0);
        BigDecimal[] large = getLargeStepsSet(g, dataSegment, renderSegment);
        drawScale(g, dataSegment, large, reverse);

        large=null;

        if(autoOrderEnabled)drawPow10(g);

        drawTitle(g);
        g.setTransform(save);
    }

    /**
     * 
     * @param g
     */
    public void drawTitle(Graphics2D g)
    {
 
        AxisValue val=new AxisValue(this.title);
        val.setForeground(this.titleForeground);


        String sTitle=(!title.equals(""))?title+", ":"";
        String sTenPower = ((tenPowerPosition==tenPowerPosition.onTitle)&&this.autoOrderEnabled && this.currentOrder != 0) ? "10" + currentOrder : "";

        String sUnits=this.units;

        String s=sTitle+sTenPower+sUnits;

        AttributedString as=new AttributedString(s);
        
        as.addAttribute(TextAttribute.SIZE, titleFont.getSize());
        as.addAttribute(TextAttribute.FAMILY, titleFont.getFamily());

        if(!sTenPower.equals(""))
          as.addAttribute(java.awt.font.TextAttribute.SUPERSCRIPT, java.awt.font.TextAttribute.SUPERSCRIPT_SUPER, sTitle.length()+2, sTitle.length()+sTenPower.length());

             

        switch(axisType)
        {
            case Horizontal:
                val.alignH=AxisValue.AlignH.Top;
                val.alignW=AxisValue.AlignW.Center;
                //val.Draw(g, new Point2D.Double(0.5*this.getWidth(),this.getHeight()-5));
                val.draw(g, as, new Point2D.Double(0.5*this.getWidth(),this.getHeight()-5));
                break;
            case Vertical:
                val.setOrientation(digitsOrientation.HorizontalAlt);
                val.alignH=AxisValue.AlignH.Bottom;
                val.alignW=AxisValue.AlignW.Center;
                val.draw(g, as, new Point2D.Double(0.5*this.getWidth(),this.getHeight()-5));
                break;
            case VerticalAlt:
                val.alignH=AxisValue.AlignH.Top;
                val.alignW=AxisValue.AlignW.Center;
                val.draw(g, as, new Point2D.Double(0.5*this.getWidth(),this.getHeight()-5));
                break;
            case HorizontalAlt:
                val.setOrientation(digitsOrientation.HorizontalAlt);
                val.alignH=AxisValue.AlignH.Bottom;
                val.alignW=AxisValue.AlignW.Center;
                val.draw(g, as, new Point2D.Double(0.5*this.getWidth(),this.getHeight()-5));
                break;

        }

        g.setColor(Color.black);
    }
    
    private void drawPow10(Graphics2D g)
    {        
        if(currentOrder==0||tenPowerPosition!=tenPowerPosition.outOfTitle)return;

        switch(axisType)
        {
            case Horizontal:
                drawPow10(g, new Point2D.Double(this.getWidth()-5,this.getHeight()-5), currentOrder, AxisValue.AlignW.Left, AxisValue.AlignH.Top,digitsOrientation.Horizontal);
                break;
            case Vertical:
                drawPow10(g, new Point2D.Double(this.getWidth()+5,this.getHeight()-5), currentOrder, AxisValue.AlignW.Right, AxisValue.AlignH.Top,digitsOrientation.HorizontalAlt);
                break;
            case VerticalAlt:
                drawPow10(g, new Point2D.Double(-5,this.getHeight()-5), currentOrder, AxisValue.AlignW.Left, AxisValue.AlignH.Top,digitsOrientation.Horizontal);
                break;
            case HorizontalAlt:
                drawPow10(g, new Point2D.Double(5,this.getHeight()-5), currentOrder, AxisValue.AlignW.Right, AxisValue.AlignH.Top,digitsOrientation.HorizontalAlt);
                break;
        }        
        
    }

      private void drawPow10(Graphics2D g, Point2D pt, int power, AxisValue.AlignW alignW, AxisValue.AlignH alignH,AxisValue.Orientation orientation) {
        AxisValue val10=new AxisValue("10");
        val10.setfont(new Font(Font.MONOSPACED, Font.BOLD, 12));
        AxisValue valPower=new AxisValue(new Integer(power).toString());
        valPower.setfont(new Font(Font.MONOSPACED, Font.BOLD, 10));

        Rectangle2D rect10=val10.getBounds(g);
        Rectangle2D rectPower=valPower.getBounds(g);
        
        double x10=0;
        double xPower=0;
        double y10=0;
        double yPower=0;

        val10.setOrientation(orientation);
        valPower.setOrientation(orientation);

        switch (orientation) {
              case Horizontal:
                  switch (alignW) {
                      case Left:
                          val10.alignW = alignW.Left;
                          valPower.alignW = alignW.Left;
                          xPower = pt.getX();
                          x10 = xPower - rectPower.getWidth();
                          break;
                      case Center:
                          val10.alignW = alignW.Right;
                          valPower.alignW = alignW.Left;
                          double w = rect10.getWidth() + rectPower.getWidth();
                          x10 = pt.getX() - 0.5 * w;
                          xPower = x10 + w;
                          break;
                      case Right:
                          val10.alignW = alignW.Right;
                          valPower.alignW = alignW.Right;
                          x10 = pt.getX();
                          xPower = x10 + rect10.getWidth();
                          break;
                  }

                  switch (alignH) {
                      case Top: case Baseline:
                          val10.alignH = alignH.Top;
                          valPower.alignH = alignH.Top;
                          y10 = pt.getY();
                          yPower = y10 - rect10.getHeight();
                          break;
                      case Center:
                          double h = rect10.getHeight() + rectPower.getHeight();
                          val10.alignH = alignH.Top;
                          valPower.alignH = alignH.Bottom;
                          yPower = pt.getY() - h * 0.5;
                          y10 = yPower + h;
                          break;                      
                      case Bottom:
                          val10.alignH = alignH.Bottom;
                          valPower.alignH = alignH.Bottom;
                          yPower = pt.getY();
                          y10 = pt.getY() + rectPower.getHeight();
                          break;
                  }
                  break;

                  case HorizontalAlt:

                  switch (alignW) {
                      case Left:
                          val10.alignW = alignW.Left;
                          valPower.alignW = alignW.Left;
                          x10=pt.getX();
                          xPower = pt.getX() - rect10.getWidth();
                          break;
                      case Center:
                          val10.alignW = alignW.Left;
                          valPower.alignW = alignW.Right;
                          double w = rect10.getWidth() + rectPower.getWidth();
                          x10 = pt.getX() + 0.5 * w;
                          xPower = pt.getX() - 0.5*w;
                          break;
                      case Right:
                          val10.alignW = alignW.Right;
                          valPower.alignW = alignW.Right;
                          xPower = pt.getX();
                          x10 = xPower+rectPower.getWidth();
                          break;
                  }

                  switch (alignH) {
                      case Bottom:
                          val10.alignH = alignH.Top;
                          valPower.alignH = alignH.Top;
                          y10 = pt.getY();
                          yPower = y10 + rect10.getHeight();
                          break;
                      case Center:
                          double h = rect10.getHeight() + rectPower.getHeight();
                          val10.alignH = alignH.Top;
                          valPower.alignH = alignH.Bottom;
                          yPower = pt.getY()+0.5*h ;
                          y10=yPower-h;
                          break;                          
                      case Baseline: case Top:
                          val10.alignH = alignH.Bottom;
                          valPower.alignH = alignH.Bottom;
                          yPower = pt.getY();
                          y10 = pt.getY() - rectPower.getHeight();
                          break;
                  }
                  break;
          }
        
        val10.draw(g, new Point2D.Double(x10,y10));
        valPower.draw(g, new Point2D.Double(xPower,yPower));
    }


    /**
     *
     * @param dataSegment data segment of data area
     * @param largeStepSize large maks step length<
     * @return large marks step set
     */
    private BigDecimal[] getLargeStepsSet(Segment dataSegment, BigDecimal largeStepSize) {
        BigDecimal first;
        if (dataSegment.getLeft() >= 0) {
            BigDecimal n=new BigDecimal(java.lang.Math.floor(dataSegment.getLeft() / largeStepSize.doubleValue()));
            first = largeStepSize.multiply(n.subtract(BigDecimal.ONE)); 
            } else {
            
            BigDecimal n=new BigDecimal(java.lang.Math.floor(java.lang.Math.abs(dataSegment.getLeft() / largeStepSize.doubleValue())));

            first = largeStepSize.multiply(n.negate().subtract(BigDecimal.ONE));
        }

        if (first.doubleValue() > dataSegment.getRight()) {
            return null;
        }

        if (java.lang.Math.floor((dataSegment.getRight() - first.doubleValue()) / largeStepSize.doubleValue()) < 500f) {
            int NSteps = 3 + (int) java.lang.Math.floor((dataSegment.getRight() - first.doubleValue()) / largeStepSize.doubleValue());//+2 for safety

            BigDecimal[] answer = new BigDecimal[NSteps];

            
            for (int i = 0; i < NSteps; i++) {
                answer[i] = first.add(largeStepSize.multiply(new BigDecimal(i)));
            }
            return answer;
        }

        return null;
    }

    /**
     *
     * @param g Graphics context
     * @param renderSegment render segment
     * @param dataSegment data segment
     * @param MinRenderLargeMarkStep minimal length of the step
     * @return step period of the large marks
     */
    private BigDecimal calcLargeStepsSize(Graphics2D g, Segment renderSegment, Segment dataSegment, double MinRenderLargeMarkStep) {
        int[] Mantissas = {1, 2, 5};

        shouldCullMiddle = false;

        double approxTickStep = (MinRenderLargeMarkStep / renderSegment.getWidth()) * dataSegment.getWidth();

        double exponent = java.lang.Math.floor(java.lang.Math.log10(approxTickStep));
        double mantissa = java.lang.Math.pow(10.0, java.lang.Math.log10(approxTickStep) - exponent);


        // approximate the value using mantisses set
        int mantissaIndex = Mantissas.length - 1;
        for (int i = 1; i < Mantissas.length; ++i) {
            if (mantissa < Mantissas[i]) {
                mantissaIndex = i - 1;
                break;
            }
        }


        double exponent_start = exponent;
        // then choose next largest spacing.
        mantissaIndex += 1;
        if (mantissaIndex == Mantissas.length) {
            mantissaIndex = 0;
            exponent += 1.0;
        }

        exponent = exponent_start + java.lang.Math.round(exponent - exponent_start);

        // now make sure that the returned value is such that at least two
        // large tick marks will be displayed.
        double dataStep = java.lang.Math.pow(10.0, exponent) * Mantissas[mantissaIndex];
        double renderStep = ((dataStep / dataSegment.getWidth()) * renderSegment.getWidth());
        int exp = (int) java.lang.Math.round(exponent);

        BigDecimal ans = new BigDecimal("10");

        ans = (exp >= 0) ? ans.pow(exp) : new BigDecimal(BigInteger.ONE).divide(ans.pow(-exp));

        return ans.multiply(new BigDecimal(Mantissas[mantissaIndex]));
    }
    double largestValue = 0;

    /**
     *
     * @param g Graphics context
     * @param values set of values for check
     * @return maximum render size of the biggest label
     */
    double getLargestValue(Graphics2D g, BigDecimal[] values) {

        long time = System.currentTimeMillis();
        if (values == null) {
            return 100.0;
        }

        double max = 0f;
        int max_scale = -1;

        FontMetrics fm = g.getFontMetrics(font);

        int size_zero = (int) java.lang.Math.ceil(fm.stringWidth("0")) + 1;

        if ((this.digitsOrientation == AxisValue.Orientation.Horizontal)||(this.digitsOrientation == AxisValue.Orientation.HorizontalAlt)) {

            /*
            for (int i = 0; i < values.length; i++) {
            if (values[i].scale() - max_scale > 0) {
            max_scale = values[i].scale();
            System.out.println("max_scale="+max_scale);
            }
            }*/
            max_scale = values[0].scale();
            for (int i = 0; i < values.length; i++) {
                if (values[i].scale() != max_scale) {
                    JBarsicGraph.showException("AAAAAAAAAAAAAAAA");
                }
            }
           
            for (int i = 0; i < values.length; i++) {
                    max = java.lang.Math.max(fm.stringWidth(values[i].toString()), max);

            }
        } else {
            max = fm.getHeight();
        }
      
        return max + 2.0;
    }

    private void printValues(BigDecimal[] values, String label) {
        System.out.print(label + "[");

        for (int i = 0; i < values.length; i++) {
            System.out.print(values[i] + " ");
        }

        System.out.println("]");
    }

    /**
     *
     * @param g Graphics context
     * @param dataSegment our data area
     * @param renderSegment render segment
     * @return large marks set of positions in two passes
     */
    private BigDecimal[] getLargeStepsSet(Graphics2D g, Segment dataSegment, Segment renderSegment) {
        BigDecimal largeStep = calcLargeStepsSize(g, renderSegment, dataSegment, 30f);

        BigDecimal[] large = getLargeStepsSet(dataSegment, largeStep);

        if(large==null)
        {
            System.err.println("LARGESTEPS IS NULL;largeStep="+largeStep);
        }

        double largestValue = java.lang.Math.round(getLargestValue(g, large));//largest lavue by rendering size

        largeStep = calcLargeStepsSize(g, renderSegment, dataSegment, largestValue + 15.0);
        

        if( (this.digitsOrientation == AxisValue.Orientation.Horizontal)||(this.digitsOrientation == AxisValue.Orientation.HorizontalAlt)) {

            large = getLargeStepsSet(dataSegment, largeStep);

            double m = java.lang.Math.round(getLargestValue(g, large));//largest lavue by rendering size

            int count = 0;
            while (m > largestValue) {
                //printValues(large, "AFTER");

                largestValue = java.lang.Math.ceil(java.lang.Math.max(m, largestValue)) + 1;
                largeStep = calcLargeStepsSize(g, renderSegment, dataSegment, largestValue + 15.0);
                large = getLargeStepsSet(dataSegment, largeStep);
                m = java.lang.Math.round(getLargestValue(g, large));//largest lavue by rendering size

                if (count > 10) {
                    break;
                }
                count++;
            }
        }
        
        return large;
    }
}
