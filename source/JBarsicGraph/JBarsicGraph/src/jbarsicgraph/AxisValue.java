/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jbarsicgraph;

import java.awt.Color;

import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.*;
import java.awt.font.*;

import java.awt.Font;
import java.awt.font.TextLayout;
import java.awt.Graphics2D;
import java.text.AttributedString;

/**
 * 
 * @author Max
 */
public class AxisValue {
    
    /**
     * 
     */
    public AxisValue() {
    }

    /**
     * 
     * @param value
     * @param deg
     */
    public AxisValue(float value, int deg) {
        this.value = String.format("{0:F" + deg + "}", value);
    }

    /**
     * 
     * @param value
     */
    public AxisValue(String value) {
        this.value = value;
    }

    /**
     * 
     */
    public enum Orientation {

        /**
         * 
         */
        Horizontal,
        /**
         * 
         */
        Vertical,
        /**
         * 
         */
        VerticalAlt,
        /**
         * 
         */
        HorizontalAlt
    }

    /**
     * 
     */
    public enum AlignW {

        /**
         * 
         */
        Left,
        /**
         * 
         */
        Center,
        /**
         * 
         */
        Right
    }

    /**
     * 
     */
    public enum AlignH {

        /**
         * 
         */
        Top,
        /**
         * 
         */
        Center,
        /**
         * 
         */
        Bottom,
        /**
         * 
         */
        Baseline
    }

    /**
     * 
     */
    public AlignW alignW = AlignW.Center;
    /**
     * 
     */
    public AlignH alignH = AlignH.Bottom;
    //
    private Orientation orientation = Orientation.Horizontal;

    /**
     * Returns the orientation (angle) of the value
     * @return the orientation (angle) of the value
     */
    public Orientation getOrientation() {
        return this.orientation;
    }

    /**
     * Sets the orientation (angle) of the value. The default value is Horizontal
     * @param value the orientation of the value
     */
    public void setOrientation(Orientation value) {
        this.orientation = value;
    }

    private String value = "";

    /**
     * Returns the value that we want to display
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the value that we want to display
     * @param value 
     */
    public void setValue(String value) {
        this.value = value;
    }
    
    private Font font = new Font("Monospaced", Font.BOLD, 8 );

    /**
     * Returns the font of the value
     * @return the font of the value
     */
    public Font getfont() {
        return this.font;
    }

    /**
     * Sets the font of the value
     * @param value the font of the value
     */
    public void setfont(Font value) {
        this.font = value;
    }

    private Color foreground = Color.BLACK;

    /**
     * Returns the font color of the value
     * @return the font color of the value
     */
    public Color getForeground() {
        return this.foreground;
    }

    /**
     * Sets the font color of the value
     * @param value the font color of the value
     */
    public void setForeground(Color value) {
        this.foreground = value;
    }

    /**
     * Returns the render width of the value shape
     * @param g Graphics context
     * @return the width of the value shape
     */
    public double getWidth(Graphics2D g) {
        GlyphVector glyphVector = font.createGlyphVector(g.getFontRenderContext(), this.value);
        return glyphVector.getVisualBounds().getBounds2D().getWidth();
    }

    /**
     * Returns the render height of the value shape
     * @param g Graphics context
     * @return the height of the value shape
     */
    public double getHeight(Graphics2D g) {
        GlyphVector glyphVector = font.createGlyphVector(g.getFontRenderContext(), this.value);
        return glyphVector.getVisualBounds().getBounds2D().getHeight();
    }

    /** Returns the render bound rectangle of the value shape
     * @param g Graphics context
     * @return the bound of the value shape
     */
    public Rectangle2D getBounds(Graphics2D g)
    {
        GlyphVector glyphVector = font.createGlyphVector(g.getFontRenderContext(), this.value);
        return glyphVector.getVisualBounds().getBounds2D();
    }

   /**
    * Draws the value
    * @param g Graphics context
    * @param pos position of the value(also you can set the alignment using alignH and alighW properties)
    */
     public void draw(Graphics2D g, Point2D pos) {
        g.setFont(font);
        g.setColor(this.foreground);

        String val=this.value;

        double splitX = 0f;
        double splitY = 0f;

        GlyphVector glyphVector = font.createGlyphVector(g.getFontRenderContext(), val);
        Rectangle2D rect = glyphVector.getVisualBounds().getBounds2D();

        switch (orientation) {
            case Horizontal:

                switch (alignW) {
                    case Right:
                        splitX = -rect.getMinX();
                        break;
                    case Center:
                        splitX = -rect.getMinX() -rect.getWidth() * 0.5;
                        break;
                    case Left:
                        splitX = -rect.getMinX()-rect.getWidth();
                        break;
                }
                switch (alignH) {
                    case Top:
                        splitY = -rect.getMaxY();
                        break;
                    case Center:
                        splitY = -rect.getMaxY()+rect.getHeight()*0.5;
                        break;
                    case Bottom:
                        splitY = -rect.getMinY();
                        break;
                    case Baseline:
                        splitY=0;
                        break;
                }


                g.drawString(val, (float) (pos.getX() + splitX), (float) (pos.getY() + splitY));
                break;
            case Vertical:


                switch (alignW) {
                    case Left:
                        splitY=-rect.getMaxX();
                        break;
                    case Center:
                        splitY=-rect.getMaxX()+rect.getWidth()*0.5;
                        break;
                    case Right:
                        splitY=-rect.getMinX();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitX=-rect.getMaxY();
                        break;
                    case Center:
                        splitX=-rect.getMaxY()+rect.getHeight()*0.5;
                        break;
                    case Bottom:
                        splitX=-rect.getMinY();
                        break;
                    case Baseline:
                        splitX=0;
                        break;
                }

                double _90deg = java.lang.Math.PI / 2;
                g.rotate(_90deg);

                g.drawString(val, (float) (pos.getY() + splitY), (float) (-pos.getX() + splitX));
                
                g.rotate(-_90deg);
                break;
            case VerticalAlt:

                switch (alignW) {
                    case Left:
                        splitY=rect.getMaxX();
                        break;
                    case Center:
                        splitY=rect.getMaxX()-rect.getWidth()*0.5;
                        break;
                    case Right:
                        splitY=rect.getMinX();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitX = rect.getMaxY();
                        break;
                    case Center:
                        splitX = rect.getMaxY() - rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitX = rect.getMinY();
                        break;
                    case Baseline:
                        splitX=0;
                        break;
                }

               

                double __90deg = java.lang.Math.PI / 2;

                
                g.rotate(-__90deg);
               
                g.drawString(val, (float) (-pos.getY() - splitY), (float) (pos.getX() - splitX));
                
                g.rotate(__90deg);
                break;

            case HorizontalAlt:

                switch (alignW) {
                    case Left:
                        splitX = -rect.getMinX();
                        break;
                    case Center:
                        splitX = -rect.getMinX() -rect.getWidth() * 0.5;
                        break;
                    case Right:
                        splitX = -rect.getMinX()-rect.getWidth();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitY = -rect.getMaxY();
                        break;
                    case Center:
                        splitY = -rect.getMaxY()+rect.getHeight()*0.5;
                        break;
                    case Bottom:
                        splitY = -rect.getMinY();
                        break;
                    case Baseline:
                        splitY=0;
                        break;
                }


                double _180deg = java.lang.Math.PI;
                
                g.rotate(_180deg);
                
                g.drawString(val, (float) (-pos.getX() + splitX), (float) (-pos.getY() + splitY));
               
                g.rotate(-_180deg);
                break;
        }
    }

     /**
      * Draws the value. Note that all the data about string value and font stores in the as argument
      * @param g Graphics context
      * @param as the value
      * @param pos pos position of the value(also you can set the alignment using alignH and alighW properties)
      */
      public void draw(Graphics2D g, AttributedString as, Point2D pos) {
        //g.setFont(font);
        //all the data about font stores in AttributedString attributes
        g.setColor(this.foreground);

        //Font font = style.getFont();


        String val=this.value;

        double splitX = 0f;
        double splitY = 0f;

        //GlyphVector glyphVector = font.createGlyphVector(g.getFontRenderContext(), val);

        TextLayout t = new TextLayout(as.getIterator(), g.getFontRenderContext());

        Rectangle2D rect = t.getBounds().getBounds2D();

        switch (orientation) {
            case Horizontal:

                switch (alignW) {
                    case Right:
                        splitX = -rect.getMinX();
                        break;
                    case Center:
                        splitX = -rect.getMinX() -rect.getWidth() * 0.5;
                        break;
                    case Left:
                        splitX = -rect.getMinX()-rect.getWidth();
                        break;
                }
                switch (alignH) {
                    case Top:
                        splitY = -rect.getMaxY();
                        break;
                    case Center:
                        splitY = -rect.getMaxY()+rect.getHeight()*0.5;
                        break;
                    case Bottom:
                        splitY = -rect.getMinY();
                        break;
                    case Baseline:
                        splitY=0;
                        break;
                }

                t.draw(g, (float) (pos.getX() + splitX), (float) (pos.getY() + splitY));

                break;
            case Vertical:


                switch (alignW) {
                    case Left:
                        splitY=-rect.getMaxX();
                        break;
                    case Center:
                        splitY=-rect.getMaxX()+rect.getWidth()*0.5;
                        break;
                    case Right:
                        splitY=-rect.getMinX();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitX=-rect.getMaxY();
                        break;
                    case Center:
                        splitX=-rect.getMaxY()+rect.getHeight()*0.5;
                        break;
                    case Bottom:
                        splitX=-rect.getMinY();
                        break;
                    case Baseline:
                        splitX=0;
                        break;
                }

                double _90deg = java.lang.Math.PI / 2;
                g.rotate(_90deg);

                //g.drawString(val, (float) (pos.getY() + splitY), (float) (-pos.getX() + splitX));
                t.draw(g, (float) (pos.getY() + splitY), (float) (-pos.getX() + splitX));

                g.rotate(-_90deg);
                break;
            case VerticalAlt:

                switch (alignW) {
                    case Left:
                        splitY=rect.getMaxX();
                        break;
                    case Center:
                        splitY=rect.getMaxX()-rect.getWidth()*0.5;
                        break;
                    case Right:
                        splitY=rect.getMinX();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitX = rect.getMaxY();
                        break;
                    case Center:
                        splitX = rect.getMaxY() - rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitX = rect.getMinY();
                        break;
                    case Baseline:
                        splitX=0;
                        break;
                }



                double __90deg = java.lang.Math.PI / 2;


                g.rotate(-__90deg);

                t.draw(g,(float) (-pos.getY() - splitY), (float) (pos.getX() - splitX));

                g.rotate(__90deg);
                break;

            case HorizontalAlt:

                switch (alignW) {
                    case Left:
                        splitX = -rect.getMinX();
                        break;
                    case Center:
                        splitX = -rect.getMinX() -rect.getWidth() * 0.5;
                        break;
                    case Right:
                        splitX = -rect.getMinX()-rect.getWidth();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitY = -rect.getMaxY();
                        break;
                    case Center:
                        splitY = -rect.getMaxY()+rect.getHeight()*0.5;
                        break;
                    case Bottom:
                        splitY = -rect.getMinY();
                        break;
                    case Baseline:
                        splitY=0;
                        break;
                }


                double _180deg = java.lang.Math.PI;

                g.rotate(_180deg);

                t.draw(g, (float) (-pos.getX() + splitX), (float) (-pos.getY() + splitY));

                g.rotate(-_180deg);
                break;
        }
    }

     /**
     * Draws the value if his bounds by x greater than leftlimit and less than rightlimit. The method is convenient for drawing value on axis
     * @param g Graphics context
     * @param pos position of the value(also you can set the alignment using alignH and alighW properties)
     * @param leftlimit the left limit(see the description)
     * @param rightlimit the rightlimit(see the description)
     * @param reverse leftlimit and rightlimit changes
     */
    public void draw(Graphics2D g, Point2D pos, double leftlimit, double rightlimit, boolean reverse) {
        g.setFont(font);
        g.setColor(this.foreground);

        String val=this.value;

        double splitX = 0f;
        double splitY = 0f;
        
        GlyphVector glyphVector = font.createGlyphVector(g.getFontRenderContext(), val);
        Rectangle2D rect = glyphVector.getVisualBounds().getBounds2D();

        switch (orientation) {
            case Horizontal:

                switch (alignW) {
                    case Right:
                        splitX = -rect.getMinX();
                        break;
                    case Center:
                        splitX = -rect.getMinX() -rect.getWidth() * 0.5;
                        break;
                    case Left:
                        splitX = -rect.getMinX()-rect.getWidth();
                        break;
                }
                switch (alignH) {
                    case Top:
                        splitY = -rect.getMaxY();
                        break;
                    case Center:
                        splitY = -rect.getMaxY()+rect.getHeight()*0.5;
                        break;
                    case Bottom:
                        splitY = -rect.getMinY();
                        break;
                    case Baseline:
                        splitY=0;
                        break;
                }
               
                
                if (!reverse) {
                    if (pos.getX()+splitX+rect.getMinX()< leftlimit) {
                        return;
                    }
                    if (pos.getX()+splitX+rect.getMaxX() > rightlimit) {
                        return;
                    }
                } else {
                    System.out.println("green="+(pos.getX()+splitX+rect.getMinX())+";rightlimit="+rightlimit);
                    if (rightlimit > pos.getX()+splitX+rect.getMinX()) {
                        return;
                    }
                    if (leftlimit < pos.getX()+splitX+rect.getMaxX()) {
                        return;
                    }
                }

                /*g.setColor(Color.green);
                g.draw(new Line2D.Double(pos.getX()+splitX+rect.getMinX(),0,pos.getX()+splitX+rect.getMinX(),100));
                g.setColor(Color.YELLOW);
                g.draw(new Line2D.Double(pos.getX()+splitX+rect.getMinX()+rect.getWidth(),0,pos.getX()+splitX+rect.getMinX()+rect.getWidth(),50));
                g.setColor(Color.DARK_GRAY);
                g.draw(new Line2D.Double(pos.getX()+splitX+rect.getMaxX(),50,pos.getX()+splitX+rect.getMinX()+rect.getWidth(),100));
                g.setColor(Color.MAGENTA);
                g.draw(new Line2D.Double(leftlimit,0,leftlimit,100));
                g.setColor(Color.red);
                g.draw(new Line2D.Double(rightlimit,0,rightlimit,100));                
*/
                g.drawString(val, (float) (pos.getX() + splitX), (float) (pos.getY() + splitY));
/*
                g.setColor(Color.blue);
                g.draw(new Line2D.Double(0,pos.getY(),1000,pos.getY()));

                g.setColor(Color.BLACK);
                g.draw(new Rectangle2D.Double(pos.getX()+splitX+rect.getMinX(),pos.getY()+splitY+rect.getMinY(),rect.getWidth(),rect.getHeight()));
                */

                break;
            case Vertical:


                switch (alignW) {
                    case Left:
                        splitY=-rect.getMaxX();
                        break;
                    case Center:
                        splitY=-rect.getMaxX()+rect.getWidth()*0.5;
                        break;
                    case Right:
                        splitY=-rect.getMinX();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitX=-rect.getMaxY();
                        break;
                    case Center:
                        splitX=-rect.getMaxY()+rect.getHeight()*0.5;
                        break;
                    case Bottom:
                        splitX=-rect.getMinY();
                        break;
                    case Baseline:
                        splitX=0;
                        break;
                }

    
                if (!reverse) {
                   if (-pos.getX() + splitX+rect.getMaxY() > -leftlimit) {
                        return;
                    }
                    if (-pos.getX() + splitX+rect.getMinY() < -rightlimit) {
                        return;
                    }
                } else {
                     System.out.println("x="+(-pos.getX() + splitX+rect.getMinY())+" "+rightlimit+" "+value);
                    
                    if (-pos.getX() + splitX+rect.getMinY() < -leftlimit) {
                        return;
                    }

                    if (-pos.getX() + splitX+rect.getMaxY() > -rightlimit) {
                        return;
                    }
                }

/*
                g.setColor(Color.blue);
                g.draw(new Line2D.Double(rightlimit,0,rightlimit,100));*/

                /*g.setColor(Color.red);
                g.draw(new Line2D.Double(leftlimit,0,leftlimit,100));*/

                double _90deg = java.lang.Math.PI / 2;
                g.rotate(_90deg);               
                
                /*g.setColor(Color.GREEN);
                g.draw(new Line2D.Double(0,-pos.getX() + splitX+rect.getMinY(),100,-pos.getX() + splitX+rect.getMinY()));
                
                g.setColor(Color.BLUE);
                g.draw(new Line2D.Double(0,-pos.getX() + splitX+rect.getMaxY(),100,-pos.getX() + splitX+rect.getMaxY()));
                */
                g.drawString(val, (float) (pos.getY() + splitY), (float) (-pos.getX() + splitX));
                /*g.setColor(Color.RED);
                g.draw(new Rectangle2D.Double(pos.getY() + splitY+rect.getMinX(),-pos.getX() + splitX+rect.getMinY(),rect.getWidth(),rect.getHeight()));
                g.setColor(Color.BLACK);*/
                g.rotate(-_90deg);
                break;
            case VerticalAlt:
                                
                switch (alignW) {
                    case Left:
                        splitY=rect.getMaxX();
                        break;
                    case Center:
                        splitY=rect.getMaxX()-rect.getWidth()*0.5;
                        break;
                    case Right:
                        splitY=rect.getMinX();
                        break;
                }
                
                switch (alignH) {
                    case Top:
                        splitX = rect.getMaxY();
                        break;
                    case Center:
                        splitX = rect.getMaxY() - rect.getHeight() * 0.5;
                        break;
                    case Bottom:
                        splitX = rect.getMinY();
                        break;
                    case Baseline:
                        splitX=0;
                        break;
                }
               
                if (!reverse) {
                    if (pos.getX() - splitX+rect.getMaxY() > rightlimit) {
                        return;
                    }
                    if (pos.getX() - splitX+rect.getMinY() < leftlimit) {
                        return;
                    }
                } else {
                    if (pos.getX() - splitX+rect.getMinY() < rightlimit) {
                        return;
                    }
                    if (pos.getX() - splitX+rect.getMaxY() > leftlimit) {
                        return;
                    }
                }

                double __90deg = java.lang.Math.PI / 2;

                /*g.setColor(Color.red);
                g.draw(new Line2D.Double(leftlimit,0,leftlimit,100));
                 */

                /*
                g.setColor(Color.red);
                g.draw(new Line2D.Double(rightlimit,0,rightlimit,100));
                g.setColor(Color.magenta);
                g.draw(new Line2D.Double(leftlimit,0,leftlimit,100));                
                */
                g.rotate(-__90deg);
                /*
                g.setColor(Color.GREEN);
                g.draw(new Line2D.Double(-100,pos.getX() - splitX+rect.getMinY(),0,pos.getX() - splitX+rect.getMinY()));
                g.setColor(Color.BLUE);
                g.draw(new Line2D.Double(-100,pos.getX() - splitX+rect.getMaxY(),0,pos.getX() - splitX+rect.getMaxY()));
                */
                g.drawString(val, (float) (-pos.getY() - splitY), (float) (pos.getX() - splitX));
                /*g.setColor(Color.RED);
                g.draw(new Rectangle2D.Double(-pos.getY() - splitY+rect.getMinX(),pos.getX() - splitX+rect.getMinY(),rect.getWidth(),rect.getHeight()));
                g.setColor(Color.BLACK);
                 */
                g.rotate(__90deg);
                break;

            case HorizontalAlt:
               
                switch (alignW) {
                    case Left:
                        splitX = -rect.getMinX();
                        break;
                    case Center:
                        splitX = -rect.getMinX() -rect.getWidth() * 0.5;
                        break;
                    case Right:
                        splitX = -rect.getMinX()-rect.getWidth();
                        break;
                }

                switch (alignH) {
                    case Top:
                        splitY = -rect.getMaxY();
                        break;
                    case Center:
                        splitY = -rect.getMaxY()+rect.getHeight()*0.5;
                        break;
                    case Bottom:
                        splitY = -rect.getMinY();
                        break;
                    case Baseline:
                        splitY=0;
                        break;
                }


                if (!reverse) {
                    if (-pos.getX()+splitX+rect.getMaxX() > -leftlimit) {
                        return;
                    }
                    if (-pos.getX()+splitX+rect.getMinX() < -rightlimit) {
                        return;
                    }
                } else {
                    if (-rightlimit < -pos.getX()+splitX+rect.getMaxX()) {
                        return;
                    }
                    if (-leftlimit >-pos.getX()+splitX+rect.getMinX()) {
                        return;
                    }
                }


                double _180deg = java.lang.Math.PI;

                /*g.setColor(Color.red);
                g.draw(new Line2D.Double(rightlimit,0,rightlimit,100));
                g.setColor(Color.magenta);
                g.draw(new Line2D.Double(leftlimit,0,leftlimit,100));
                */
                /*g.setColor(Color.blue);
                g.draw(new Line2D.Double(-1000,pos.getY(),1000,pos.getY()));*/

                g.rotate(_180deg);
                /*
                g.setColor(Color.green);
                g.draw(new Line2D.Double(-pos.getX()+splitX+rect.getMinX(),0,-pos.getX()+splitX+rect.getMinX(),-100));

                g.setColor(Color.BLUE);
                g.draw(new Line2D.Double(-pos.getX()+splitX+rect.getMaxX(),0,-pos.getX()+splitX+rect.getMinX()+rect.getWidth(),-100));
                */
                g.drawString(val, (float) (-pos.getX() + splitX), (float) (-pos.getY() + splitY));
                /*g.setColor(Color.RED);
                g.draw(new Rectangle2D.Double(-pos.getX()+splitX+rect.getMinX(),-pos.getY()+splitY+rect.getMinY(),rect.getWidth(),rect.getHeight()));
                g.setColor(Color.BLACK);*/
                g.rotate(-_180deg);
                break;
        }
    }

}
