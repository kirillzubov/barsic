package jbarsicgraph;

import jbarsicgraph.axis.SciAxis;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;

import java.awt.Dimension;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D;
import java.awt.geom.Line2D;
import jbarsicgraph.axis.*;

import java.awt.Rectangle;
import java.awt.RenderingHints;

/**
 * Plot Control class
 */
public class AxisControl extends JComponent{

    /**
     * 
     */
    public AxisControl() {
       InitializeComponent();

    }

 
    /**
     * 
     */
    public SciAxis axisX;


    /**
     * Margin of the plot field borders on the display that we want to display
     */
    private Margin margin = new Margin(75, 20, 25, 45);

    /**
     * 
     * @param m
     */
    public void setMargin(Margin m) {
        renderArea = new Rectangle2D.Double(margin.getLeft(), margin.getTop(),
                getWidth() - margin.getLeft() - margin.getRight(), getHeight() - margin.getTop() - margin.getBottom());
    }

    /**
     * 
     * @return
     */
    public Margin getMargin() {
        return margin;
    }
    /**
     * default field of the values that we want to display
     */
    public final static Rectangle2D DEFAULT_DATA_AREA = new Rectangle2D.Double(-10, -10, 20, 20);
    /**
     * 
     */
    public static final double MAX_SCALE_FACTOR = 5E7;
    /**
     * 
     */
    public static final double MIN_SCALE_FACTOR = 1E-7;
    /**
     * field of the values that we want to display
     */
    private Rectangle2D DataArea = new Rectangle2D.Double(-10, -10, 20, 20);
    /**
     * field on the display that we want to display
     */
    private Rectangle2D renderArea = new Rectangle2D.Double(-10, -10, 20, 20);

    private void InitializeComponent() {

        axisX = new SciAxis();

    }

    //debug
    /**
     * 
     * @param theMessage
     */
    public static void showException(String theMessage) {
        javax.swing.JOptionPane.showMessageDialog((java.awt.Component) null, theMessage, "alert", javax.swing.JOptionPane.ERROR_MESSAGE);
    }

 
    public void paintComponent(Graphics gc) {

     long time = System.currentTimeMillis();
        //System.out.println("timer started");
        super.paintComponent(gc);

        Graphics2D g = (Graphics2D) gc;       


        renderArea = new Rectangle2D.Double(margin.getLeft(), margin.getTop(),
                getWidth() - margin.getLeft() - margin.getRight(), getHeight() - margin.getTop() - margin.getBottom());


        double x=400;
        double y=400;

        axisX.setWidth(350);
        axisX.setHeight(50);
        axisX.setTopLeft(new Point2D.Double(x,y));
        axisX.setdataSegment(new Segment(DataArea.getMinX(), DataArea.getMaxX()));
       
        //System.out.println("time_0=" + (System.currentTimeMillis() - time));

        axisX.draw(g);

        g.setColor(Color.blue);
        g.draw(new Line2D.Double(x, 0, x, 1000));
        g.draw(new Line2D.Double(0, y, 1000, y));

        g.setColor(Color.red);
        g.draw(axisX.getBorders());
        //System.out.println("time_2=" + (System.currentTimeMillis() - time));

    }

    public Dimension getMinimumSize() {
        return new Dimension(350, 250);
    }

    /**
     * 
     * @return
     */
    public Dimension getPrefferedSize() {
        return new Dimension(350, 250);
    }
}
