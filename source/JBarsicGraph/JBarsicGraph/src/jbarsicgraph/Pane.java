/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jbarsicgraph;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author Max
 */
public class Pane {

    /**
     * 
     */
    public enum GradientDirection {

        /**
         * 
         */
        Horizontal,
        /**
         * 
         */
        Vertical,
        /**
         * 
         */
        Sloped,
        /**
         * 
         */
        SlopedAlt
    }
    GradientDirection paneGradientDirection = GradientDirection.Sloped;
    GradientDirection fieldGradientDirection = GradientDirection.Sloped;
    private Color paneColor1 = Color.white;
    private Color paneColor2 = Color.white;
    private Color fieldColor1 = Color.white;
    private Color fieldColor2 = Color.white;
     /**
     * boolean value indicates addition of pane
     */ 
    
    private boolean transparentPane = false;

    /**
     * 
     */
    public Pane() {
        /*this.setPaneGradientDirection(Pane.GradientDirection.Vertical);
        this.setPaneColor1(new Color(200,219,238));
        this.setPaneColor2(Color.white);*/
    }

    /**
     * 
     * @param fieldColor1
     * @param fieldColor2
     * @param paneColor1
     * @param paneColor2
     */
    public Pane(Color fieldColor1, Color fieldColor2, Color paneColor1, Color paneColor2) {
        this.fieldColor1 = fieldColor1;
        this.fieldColor2 = fieldColor2;
        this.paneColor1 = paneColor1;
        this.paneColor2 = paneColor2;
    }

    /**
     * 
     * @param value
     */
    public void setPaneGradientDirection(GradientDirection value) {
        this.paneGradientDirection = value;
    }

    /**
     * 
     * @return
     */
    public GradientDirection getPaneGradientDirection() {
        return paneGradientDirection;
    }

    /**
     * 
     * @param value
     */
    public void setFieldGradientDirection(GradientDirection value) {
        this.fieldGradientDirection = value;
    }

    /**
     * 
     * @return
     */
    public GradientDirection getFieldGradientDirection() {
        return fieldGradientDirection;
    }

    /**
     * 
     * @return
     */
    public Color getPaneColor1() {
        return paneColor1;
    }

    /**
     * 
     * @param value
     */
    public void setPaneColor1(Color value) {
        this.paneColor1 = value;
    }

    /**
     * 
     * @return
     */
    public Color getPaneColor2() {
        return paneColor2;
    }

    /**
     * 
     * @param value
     */
    public void setPaneColor2(Color value) {
        this.paneColor2 = value;
    }

    /**
     * 
     * @param color
     */
    public void setPaneColor(Color color) {
        this.paneColor1 = color;
        this.paneColor2 = color;
    }

    /**
     * 
     * @param color1
     * @param color2
     */
    public void setPaneColor(Color color1, Color color2) {
        this.paneColor1 = color1;
        this.paneColor2 = color2;
    }

    /**
     * 
     * @return
     */
    public Color getFieldColor1() {
        return fieldColor1;
    }

    /**
     * 
     * @param value
     */
    public void setFieldColor1(Color value) {
        this.fieldColor1 = value;
    }

    /**
     * 
     * @return
     */
    public Color getFieldColor2() {
        return fieldColor2;
    }

    /**
     * 
     * @param value
     */
    public void setFieldColor2(Color value) {
        this.fieldColor2 = value;
    }

    /**
     * 
     * @param color
     */
    public void setFieldColor(Color color) {
        this.fieldColor1 = color;
        this.fieldColor2 = color;
    }

    /**
     * 
     * @param color1
     * @param color2
     */
    public void setFieldColor(Color color1, Color color2) {
        this.fieldColor1 = color1;
        this.fieldColor2 = color2;
    }

    /**
     * 
     * @param isTransparentPane
     */
    public void setTransparentField(boolean isTransparentPane) {
        fieldColor1 = null;
        fieldColor2 = null;
        this.transparentPane = isTransparentPane;
    }

    /**
     * делает оси прозрачными
     */
    public void setTransparentPane() {
        paneColor1 = null;
        paneColor2 = null;
    }

    /**
     * делает всю панель прозрачной
     */
    public void setTransparent() {
        fieldColor1 = null;
        fieldColor2 = null;
        paneColor1 = null;
        paneColor2 = null;
    }

    /**
     * 
     * @param width
     * @param height
     * @return
     */
    public java.awt.GradientPaint getPaneGradient(int width, int height) {
        switch (paneGradientDirection) {
            case Sloped:
                return new java.awt.GradientPaint(0, 0, paneColor1, width, height, paneColor2);
            case SlopedAlt:
                return new java.awt.GradientPaint(width, 0, paneColor1, 0, height, paneColor2);
            case Horizontal:
                return new java.awt.GradientPaint(0, 0, paneColor1, width, 0, paneColor2);
            case Vertical:
                return new java.awt.GradientPaint(0, 0, paneColor1, 0, height, paneColor2);
        }
        return null;
    }

    /**
     * 
     * @param width
     * @param height
     * @return
     */
    public java.awt.GradientPaint getFieldGradient(int width, int height) {
        switch (fieldGradientDirection) {
            case Sloped:
                return new java.awt.GradientPaint(0, 0, fieldColor1, width, height, fieldColor2);
            case SlopedAlt:
                return new java.awt.GradientPaint(width, 0, fieldColor1, 0, height, fieldColor2);
            case Horizontal:
                return new java.awt.GradientPaint(0, 0, fieldColor1, width, 0, fieldColor2);
            case Vertical:
                return new java.awt.GradientPaint(0, 0, fieldColor1, 0, height, fieldColor2);
        }
        return null;
    }

   
    /**
     * 
     * @param g
     * @param width
     * @param height
     * @param margin
     */
    public void draw(Graphics2D g, int width, int height, Margin margin) {
        Color startColor = getPaneColor1();
        Color endColor = getPaneColor2();

        if ((startColor != null) && (endColor != null)) {
            if (startColor != endColor) {
                g.setPaint(getPaneGradient(width, height));
            } else {
                g.setPaint(null);
                g.setColor(startColor);
            }
            if ((!transparentPane) && (fieldColor1 == null) && (fieldColor2 == null)) {
                g.fillRect(0, 0, width, height);
            } else {
                g.fillRect(0, 0, width, margin.getTop());
                g.fillRect(0, height - margin.getBottom(), width, margin.getBottom());
                g.fillRect(0, margin.getTop(), margin.getLeft(), height - margin.getBottom() - margin.getTop());
                g.fillRect(width - margin.getRight(), margin.getTop(), margin.getRight(), height - margin.getBottom() - margin.getTop());
            }
        }

        startColor = getFieldColor1();
        endColor = getFieldColor2();

        if ((startColor != null) && (endColor != null)) {
            if (startColor != endColor) {
                g.setPaint(getFieldGradient(width, height));
            } else {
                g.setPaint(null);
                g.setColor(startColor);
            }
            g.fillRect(margin.getLeft(), margin.getTop(), width - margin.getLeft() - margin.getRight(),
                    height - margin.getTop() - margin.getBottom());
        }
        g.setPaint(null);
      }
}
