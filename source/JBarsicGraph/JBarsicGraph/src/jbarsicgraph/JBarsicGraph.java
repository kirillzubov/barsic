package jbarsicgraph;

import jbarsicgraph.points.Point;
import jbarsicgraph.points.PointListAr;
import jbarsicgraph.axis.SciAxis;
import jbarsicgraph.curve.Curve;
import jbarsicgraph.legend.Legend;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import java.awt.Dimension;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import javax.swing.event.*;
import jbarsicgraph.points.PointStyle;

/**
 *  class of graphics component JBarsicGraph
 */
public class JBarsicGraph extends JComponent implements MouseWheelListener, MouseListener, MouseMotionListener, KeyListener {

    /**
     * boolean value indicates need of scale
     */
    public boolean needScale = true;
    /**
     * 
     */
    public PointListAr points = new PointListAr();

    /**
     * constructor of JBarsicGraph by default
     * @author Anita
     */
    public JBarsicGraph() {
        series = new ArrayList<Curve>();
        //setTestDataSinDivX(-10.0, 10.0, 200);
        initializeComponent();

        this.addMouseWheelListener(this);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.addKeyListener(this);
    }

    /**
     * 
     * constructor of JBarsicGraph
     * @param title 
     * @param x x-coordinate values
     * @param y y-coordinate values
     * @author Anita
     */
    public JBarsicGraph(String title, double x[], double y[]) {
        series = new ArrayList<Curve>();
        addCurve(title, x, y);
        initializeComponent();

        this.addMouseWheelListener(this);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    /**
     * constructor of JBarsicGraph
     * @param x x-coordinate values
     * @param y y-coordinate values
     */
    public JBarsicGraph(double x[], double y[]) {
        series = new ArrayList<Curve>();
        addCurve(x, y);
        initializeComponent();

        this.addMouseWheelListener(this);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    /**
     * constructor of JBarsicGraph
     * @param x x-coordinate values
     * @param y y-coordinate values
     */
    public JBarsicGraph(java.util.LinkedList<Double> x, java.util.LinkedList<Double> y) {
        addCurve(x, y);
        initializeComponent();

        this.addMouseWheelListener(this);
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    private void initializeComponent() {

        axisX = new SciAxis(SciAxis.AxisType.Horizontal);
        axisX.setUnits("cm");
        axisX.setTitle("x-coord");
        axisY = new SciAxis(SciAxis.AxisType.Vertical);
        axisY.setTitle("y-coord");
        axisY.setUnits("eV");

    }

    @Override
    public void paintComponent(Graphics gc) {

        super.paintComponent(gc);
        Graphics2D g = (Graphics2D) gc;


        pane.draw(g, this.getWidth(), this.getHeight(), this.margin);

        if (antiAliasEnabled) {
            g.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING, java.awt.RenderingHints.VALUE_ANTIALIAS_ON);
        } else {
            g.setRenderingHint(java.awt.RenderingHints.KEY_ANTIALIASING, java.awt.RenderingHints.VALUE_ANTIALIAS_OFF);
        }

        if ((points.needCalcDataArea) && (needScale == true)) {
            dataArea = calcDataArea(dataArea);
        }
        drawLegend(g);
        if (isSelecting) {
            drawSelectionRect(g, selectOffset, clickOffset);
        }
        g.setColor(Color.black);

        axisX.setGridLinesLength(this.getHeight() - margin.getTop() - margin.getBottom());
        axisX.setMargin(new Margin(15, 15));
        axisX.setWidth(this.getWidth() - margin.getLeft() - margin.getRight()
                + axisX.getMargin().getLeft() + axisX.getMargin().getRight());
        axisX.setHeight(margin.getBottom());
        axisX.setTopLeft(new Point2D.Double(margin.getLeft() - axisX.getMargin().getLeft(),
                this.getHeight() - margin.getBottom()));
        axisX.setdataSegment(new Segment(dataArea.getMinX(), dataArea.getMaxX()));
        axisX.draw(g);
        axisY.setGridLinesLength(this.getWidth() - margin.getLeft() - margin.getRight());
        axisY.setMargin(new Margin(0, 0));
        axisY.setWidth(this.getHeight() - margin.getTop() - margin.getBottom()
                + axisY.getMargin().getLeft() + axisY.getMargin().getRight());
        axisY.setHeight(margin.getLeft());
        axisY.setTopLeft(new Point2D.Double(0, margin.getTop() - axisY.getMargin().getLeft()));
        axisY.setdataSegment(new Segment(dataArea.getMinY(), dataArea.getMaxY()));
        axisY.draw(g);
        renderArea = new Rectangle2D.Double(margin.getLeft(), margin.getTop(),
                getWidth() - margin.getLeft() - margin.getRight(),
                getHeight() - margin.getTop() - margin.getBottom());
        g.drawRect((int) renderArea.getMinX(), (int) renderArea.getMinY(),
                (int) renderArea.getWidth(), (int) renderArea.getHeight());
        if (points.X.size() != 0) {
            addPartsOfCurves(g);
        }
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(350, 250);
    }

    /**
     * 
     * @return prefered size
     */
    public Dimension getPrefferedSize() {
        return new Dimension(350, 250);
    }

    /**
     * Sets (x,y)-values that we want to display
     *
     * @param title 
     * @param x x-coordinate values
     * @param y y-coordinate values
     */
    public void addCurve(String title, double[] x, double[] y) {
        Curve curve = new Curve();
        curve.points = new PointListAr(x, y);
        curve.setTitle(title);
        series.add(curve);
        dataArea = inscribeToRectangle();
        seriesChangedPerformed();
        curve = null;
    }

    /**
     * add curve without legend name
     * @param x
     * @param y
     */
    public void addCurve(double[] x, double[] y) {
        addCurve("not set", x, y);
    }

    /**
     * Sets (x,y)-values that we want to display
     *
     * @param title 
     * @param x x-coordinate values
     * @param y y-coordinate values
     */
    public void addCurve(String title, java.util.LinkedList<Double> x, java.util.LinkedList<Double> y) {
        Curve curve = new Curve();
        curve.points = new PointListAr(x, y);
        curve.points.sort();
        curve.setTitle(title);
        series.add(curve);
        dataArea = inscribeToRectangle();
        seriesChangedPerformed();
        curve = null;
    }

    /**
     * 
     * @param x
     * @param y
     */
    public void addCurve(java.util.LinkedList<Double> x, java.util.LinkedList<Double> y) {
        addCurve("not set", x, y);
    }

    /**
     * add new parts of curves
     * @param g
     * @auther Anita
     */
    public void addPartsOfCurves(Graphics2D g) {
        g.setClip((int) renderArea.getMinX(), (int) renderArea.getMinY(),
                (int) renderArea.getWidth(), (int) renderArea.getHeight());
        Point2D scaleFactor;
        scaleFactor = calcScaleFactors(dataArea, renderArea);
        AffineTransform save = g.getTransform();
        double dx = renderArea.getMinX() - dataArea.getMinX() * scaleFactor.getX();
        double dy = renderArea.getMaxY() + dataArea.getMinY() * scaleFactor.getY();
        AffineTransform s = new AffineTransform(1, 0, 0, -1, dx, dy);
        g.translate(s.getTranslateX(), s.getTranslateY());

        series.clear();
        for (int i = 0; i < points.numCurve + 1; i++) {
            Curve curve = new Curve();
            curve.points = new PointListAr(points.X.get(i), points.Y.get(i));
            series.add(curve);
            seriesChangedPerformed();

            int len = points.ps.get(i).size();
            curve.points.pointStyle = new PointStyle[len];
            for (int m = 0; m < len; m++) {
                curve.points.pointStyle[m] = points.ps.get(i).get(m);
            }
            curve.points.numN = points.numN;
//            curve.points.partsVisible = true;

//            curve.points.numN = points.Xi[i].length - 1;
            curve.drawNewPartsOfCurve(g, scaleFactor);
        }
        g.setTransform(save);

    }

//    calcs optimase dataArea from all dataArea of points
    private Rectangle2D calcDataArea(Rectangle2D dataArea) {
        Rectangle2D[] dataAr = new Rectangle2D[points.numCurve + 1];
        double dmaxX = dataArea.getMaxX();
        double dminX = dataArea.getMinX();
        double dmaxY = dataArea.getMaxY();
        double dminY = dataArea.getMinY();
        for (int i = 0; i < points.numCurve + 1; i++) {
            dataAr[i] = dataArea;
        }

        for (int i = 0; i < points.numCurve + 1; i++) {
            Curve curve = new Curve();
            curve.points = new PointListAr(points.X.get(i), points.Y.get(i));
            curve.points.numN = points.X.get(i).size() - 1;
            for (int j = 0; j < curve.points.numN; j++) {
                double sx = curve.points.x[j];
                double sy = curve.points.y[j];
                if (curve.points.x[j] <= dataAr[i].getMinX()) {
                    double dAminX = dataAr[i].getMinX() - dataAr[i].getWidth() / 2 - 3.0;
                    double dAmaxX = dataAr[i].getMaxX();
                    double dAminY = dataAr[i].getMinY();
                    double dAmaxY = dataAr[i].getMaxY();
                    dataAr[i] = new Rectangle2D.Double(dAminX, dAminY, dAmaxX - dAminX, dAmaxY - dAminY);
                }
                if (curve.points.x[j] >= dataAr[i].getMaxX()) {
                    double dAminX = dataAr[i].getMinX();
                    double dAmaxX = dataAr[i].getMaxX() + dataAr[i].getWidth() / 2 + 3.0;
                    double dAminY = dataAr[i].getMinY();
                    double dAmaxY = dataAr[i].getMaxY();
                    dataAr[i] = new Rectangle2D.Double(dAminX, dAminY, dAmaxX - dAminX, dAmaxY - dAminY);

                }
                if (curve.points.y[j] <= dataAr[i].getMinY()) {
                    double dAminX = dataAr[i].getMinX();
                    double dAmaxX = dataAr[i].getMaxX();
                    double dAminY = dataAr[i].getMinY() - dataAr[i].getHeight() / 2 - 3.0;
                    double dAmaxY = dataAr[i].getMaxY();
                    dataAr[i] = new Rectangle2D.Double(dAminX, dAminY, dAmaxX - dAminX, dAmaxY - dAminY);

                }
                if (curve.points.y[j] >= dataAr[i].getMaxY()) {
                    double dAminX = dataAr[i].getMinX();
                    double dAmaxX = dataAr[i].getMaxX();
                    double dAminY = dataAr[i].getMinY();
                    double dAmaxY = dataAr[i].getMaxY() + dataAr[i].getHeight() / 2 + 3.0;
                    dataAr[i] = new Rectangle2D.Double(dAminX, dAminY, dAmaxX - dAminX, dAmaxY - dAminY);

                }

            }
            if (dataAr[i].getMaxX() > dmaxX) {
                dmaxX = dataAr[i].getMaxX();
            }
            if (dataAr[i].getMinX() < dminX) {
                dminX = dataAr[i].getMinX();
            }
            if (dataAr[i].getMaxY() > dmaxY) {
                dmaxY = dataAr[i].getMaxY();
            }
            if (dataAr[i].getMinY() < dminY) {
                dminY = dataAr[i].getMinY();
            }

        }
        dataArea = new Rectangle2D.Double(dminX, dminY, dmaxX - dminX, dmaxY - dminY);
        return dataArea;
    }

    /**
     * cleans arrays of points
     */
    private void cleanPoints() {
        points.numCurve = 0;
        points.numN = 0;
        points.needCalcDataArea = false;
        points.X.clear();
        points.Y.clear();
        points.ps.clear();
    }

    /**
     * cleans series of curves and arrays of points
     */
    public void cleanCurves() {
        series.clear();
        this.cleanPoints();
    }

    /**
     * 
     * @param dataArea
     * @param renderArea
     * @return
     */
    protected Point2D calcScaleFactors(Rectangle2D dataArea, Rectangle2D renderArea) {
        double scaleFactorX = (dataArea.getWidth() != 0) ? (renderArea.getWidth() / dataArea.getWidth()) : 1000000000;//double.MaxValue - 10f;
        double scaleFactorY = (dataArea.getHeight() != 0) ? (renderArea.getHeight() / dataArea.getHeight()) : 1000000000;
        return new Point2D.Double(scaleFactorX, scaleFactorY);
    }
    /**
     * 
     */
    public ArrayList<Curve> series;

    /**
     * 
     * @return
     */
    public ArrayList<Curve> getSeries() {
        return series;
    }

    /**
     * 
     * @param index
     * @return
     */
    public Curve getSeries(int index) {
        return series.get(index);
    }
    private EventListenerList listenerList = new EventListenerList();

    /**
     * 
     * @param l
     */
    public void addSeriesChangedListener(java.awt.event.ActionListener l) {
        listenerList.add(java.awt.event.ActionListener.class, l);
    }

    /**
     * 
     * @param l
     */
    public void removeSeriesChangedListener(java.awt.event.ActionListener l) {
        listenerList.remove(java.awt.event.ActionListener.class, l);
    }

    /**
     * 
     */
    public void seriesChangedPerformed() {
        Object[] listeners = listenerList.getListenerList();

        for (int i = 0; i < listeners.length; i++) {
            if (listeners[i] instanceof java.awt.event.ActionListener) {
                ((java.awt.event.ActionListener) listeners[i]).actionPerformed(null);
            }
        }
    }
    /**
     * margin of the plot area on the pane
     */
    private Margin margin = new Margin(75, 20, 20, 60);//new Margin(75, 20, 25, 45);

    /**
     * 
     * @param m
     */
    public void setMargin(Margin m) {
        renderArea = new Rectangle2D.Double(margin.getLeft(), margin.getTop(),
                getWidth() - margin.getLeft() - margin.getRight(), getHeight() - margin.getTop() - margin.getBottom());
    }

    /**
     * 
     * @return
     */
    public Margin getMargin() {
        return margin;
    }
    private boolean antiAliasEnabled = true;

    /**
     * 
     * @param value
     */
    public void setAntialising(boolean value) {
        antiAliasEnabled = value;
    }
    /**
     * default field of the values that we want to display
     */
    public final static Rectangle2D DEFAULT_DATA_AREA = new Rectangle2D.Double(-10, -10, 20, 20);
    /**
     * field of the values that we want to display
     */
    public Rectangle2D dataArea = new Rectangle2D.Double(-10, -10, 20, 20);
    /**
     * field on the display that we want to display
     */
    private Rectangle2D renderArea = new Rectangle2D.Double(-10, -10, 20, 20);
    private SciAxis axisX;

    /**
     * 
     * @return
     */
    public SciAxis getAxisX() {
        return axisX;
    }
    private SciAxis axisY;

    /**
     * 
     * @return
     */
    public SciAxis getAxisY() {
        return axisY;
    }

    /**
     * 
     * @param theMessage
     */
    public static void showException(String theMessage) {
        javax.swing.JOptionPane.showMessageDialog((java.awt.Component) null, theMessage, "alert", javax.swing.JOptionPane.ERROR_MESSAGE);
    }

    /**
     * 
     * @param XMin
     * @param XMax
     * @param NPoints
     * @return
     */
    public boolean validateTestData(double XMin, double XMax, int NPoints) {
        if (NPoints < 1 || (XMin >= XMax)) {
            showException("Entered data is invalid");
            return false;
        }
        return true;
    }

    //(debug)
    /**
     * 
     * @param XMin
     * @param XMax
     * @param NPoints
     */
    public void setTestDataSinDivX(double XMin, double XMax, int NPoints) {

        if (!validateTestData(XMin, XMax, NPoints)) {
            return;
        }

        Point[] points = new Point[NPoints];

        double delta = 0;

        if (NPoints > 1) {
            delta = (XMax - XMin) / (NPoints - 1);
        }

        double curr_x = XMin;

        double[] x = new double[NPoints];
        double[] y = new double[NPoints];


        for (int i = 0; i <= NPoints - 1; i++) {
            x[i] = curr_x;
            y[i] = java.lang.Math.sin(curr_x) / curr_x;
            curr_x += delta;
        }

        addCurve("sin(x)/x", x, y);

        dataArea = inscribeToRectangle();
    }

    //(debug)
    /**
     * 
     * @param XMin
     * @param XMax
     * @param NPoints
     */
    public void setTestDataSqrX(double XMin, double XMax, int NPoints) {

        if (!validateTestData(XMin, XMax, NPoints)) {
            return;
        }

        Point[] points = new Point[NPoints];

        double delta = 0;

        if (NPoints > 1) {
            delta = (XMax - XMin) / (NPoints - 1);
        }

        double curr_x = XMin;

        double[] x = new double[NPoints];
        double[] y = new double[NPoints];

        for (int i = 0; i <= NPoints - 1; i++) {
            x[i] = curr_x;
            y[i] = curr_x * curr_x;
            curr_x += delta;
        }

        addCurve("x*x", x, y);

        dataArea = inscribeToRectangle();

    }

    //(debug)
    /**
     * 
     * @param XMin
     * @param XMax
     * @param NPoints
     */
    public void setTestDataSinMulX(double XMin, double XMax, int NPoints) {

        if (!validateTestData(XMin, XMax, NPoints)) {
            return;
        }

        Point[] points = new Point[NPoints];

        double delta = 0;

        if (NPoints > 1) {
            delta = (XMax - XMin) / (NPoints - 1);
        }

        double curr_x = XMin;

        double[] x = new double[NPoints];
        double[] y = new double[NPoints];


        for (int i = 0; i <= NPoints - 1; i++) {
            x[i] = curr_x;
            y[i] = java.lang.Math.sin(curr_x) * curr_x;
            curr_x += delta;
        }

        addCurve("x*sin(x)", x, y);

        dataArea = inscribeToRectangle();
    }

    /**
     * 
     * @return minimal data area rhat contain all data points
     */
    public Rectangle2D inscribeToRectangle() {
        int size = (series != null) ? series.size() : 0;

        if (size == 0) {
            return DEFAULT_DATA_AREA;
        }

        Rectangle2D ans = series.get(0).inscribeToRectangle();

        for (int i = 0; i < size; i++) {
            Rectangle2D buf = series.get(i).inscribeToRectangle();
            ans = ans.createUnion(buf);
        }

        return ans;
    }

    /**
     * Sets values that we want to display for the custom function
     *
     * @param expression string presentation of the function
     * @param xmin left limit of x-coordinate
     * @param xmax left limit of x-coordinate
     * @param NPoints number of the points
     */
    /* public void addCurve(String function, double xmin, double xmax, int NPoints) {
    
    if (!validateTestData(xmin, xmax, NPoints)) {
    return;
    }
    
    //curve.Points = new PointListAr(function, xmin, xmax, NPoints);
    
    Curve c=new Curve();
    c.Points= new PointListAr(function, xmin, xmax, NPoints);
    c.setTitle(function);
    series.add(c);
    seriesChangedPerformed();
    
    c=null;
    dataArea=InscribeToRectangle();
    }*/
    /**
     * (debug)
     * Draw the borders of the whole control
     * @param g Graphics context
     */
    private void drawBorders(Graphics2D g) {
        Stroke save = g.getStroke();

        float dash[] = {2.0f};
        g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));
        g.drawRect(1, 1, getWidth() - 2, getHeight() - 2);
        g.setStroke(save);
    }
    /**
     * is it needs to cancel previous zoom on backward selection
     * (Monahov's order)
     */
    private final boolean CancelZoom = true;

    /**
     *
     * @param p1 first selection point
     * @param p2 second selection point
     */
    private void onSelectionEnd(Point p1, Point p2) {
        System.out.println("onSelectionEnded");


        if ((p2.getX() >= p1.getX()) && CancelZoom) {
            //initiates original size (if the option CancleZoom is enabled)
            dataArea = inscribeToRectangle();
            this.repaint();
            return;
        }

        int x1, y1, x2, y2;

        if (p1.getX() <= p2.getX()) {
            x1 = (int) p1.getX();
            x2 = (int) p2.getX();
        } else {
            x2 = (int) p1.getX();
            x1 = (int) p2.getX();
        }

        if (p1.getY() <= p2.getY()) {
            y1 = (int) p1.getY();
            y2 = (int) p2.getY();
        } else {
            y2 = (int) p1.getY();
            y1 = (int) p2.getY();
        }

        x1 = (int) java.lang.Math.max(renderArea.getMinX(), x1);
        y1 = (int) java.lang.Math.max(renderArea.getMinY(), y1);

        x2 = (int) java.lang.Math.min(renderArea.getMaxX(), x2);
        y2 = (int) java.lang.Math.min(renderArea.getMaxY(), y2);

        zoomIn(new Rectangle2D.Double(x1, y1, x2 - x1, y2 - y1));
    }

    /**
     * 
     * @return
     */
    public String[] getSeriesTitles() {
        int size = series.size();
        String[] titles = new String[size];

        for (int i = 0; i < size; i++) {
            titles[i] = series.get(i).getTitle();
        }

        return titles;
    }
    private Pane pane = new Pane();

    /**
     * 
     * @return
     */
    public Pane getPane() {
        return pane;
    }
    private Legend legend = new Legend();

    /**
     * 
     * @return
     */
    public Legend getLegend() {
        return legend;
    }
    AxisValue.AlignW alignLegendX = AxisValue.AlignW.Right;

    /**
     * 
     * @param value
     */
    public void setLegendAlignmentX(AxisValue.AlignW value) {
        this.alignLegendX = value;
    }
    AxisValue.AlignH alignLegendY = AxisValue.AlignH.Top;

    /**
     * 
     * @param value
     */
    public void setLegendAlignmentY(AxisValue.AlignH value) {
        this.alignLegendY = value;
    }

    /**
     * 
     * @param g
     */
    public void drawLegend(Graphics2D g) {
        int size = series.size();
        double legendX = 0;
        double legendY = 0;
        double gap = 5;

        switch (alignLegendX) {
            case Left:
                legendX = margin.getLeft() + gap;
                legend.setAlignW(AxisValue.AlignW.Right);
                break;
            case Center:
                legendX = margin.getLeft() + (this.getWidth() - margin.getLeft() - margin.getRight()) * 0.5;
                legend.setAlignW(AxisValue.AlignW.Center);
                break;
            case Right:
                legendX = this.getWidth() - margin.getRight() - gap;
                legend.setAlignW(AxisValue.AlignW.Left);
                break;
        }

        switch (alignLegendY) {
            case Bottom:
                legendY = this.getHeight() - margin.getBottom() - gap;
                legend.setAlignH(AxisValue.AlignH.Top);
                break;
            case Center:
                legendY = margin.getTop() + (this.getHeight() - margin.getTop() - margin.getBottom()) * 0.5;
                legend.setAlignH(AxisValue.AlignH.Center);
                break;
            case Top:
                legendY = margin.getTop() + gap;
                legend.setAlignH(AxisValue.AlignH.Bottom);
                break;
        }

        legend.setPos(new Point2D.Double(legendX, legendY));

        //if(legend.getItemsCount()!=size)
        //{
        legend.clear();
        for (int i = 0; i < size; i++) {
            legend.addCurve(series.get(i).getStyle(), series.get(i).getTitle());
        }
        // }
//
//        if (size != 0) {
//            legend.draw(g);
//        }

    }

    /**
     * Moves plot on dx,dy
     * @param delta translation vector
     */
    private void dragPlot(Point2D delta) {
        points.needCalcDataArea = false;
        double w = dataArea.getWidth();
        double h = dataArea.getHeight();

        double scaleFactorX = (dataArea.getWidth() != 0) ? (renderArea.getWidth() / dataArea.getWidth()) : 1000000000;//double.MaxValue - 10f;
//        scaleFactorX = java.lang.Math.max(scaleFactorX, MIN_SCALE_FACTOR);
        //       scaleFactorX = java.lang.Math.min(scaleFactorX, MAX_SCALE_FACTOR);

        double scaleFactorY = (dataArea.getHeight() != 0) ? (renderArea.getHeight() / dataArea.getHeight()) : 1000000000;


        //System.err.println(String.format("s=(%f,%f)", scaleFactorX,scaleFactorY));
        //       scaleFactorY = java.lang.Math.max(scaleFactorY, MIN_SCALE_FACTOR);
        //       scaleFactorY = java.lang.Math.min(scaleFactorY, MAX_SCALE_FACTOR);

        dataArea.setRect(dataArea.getMinX() - (delta.getX()) / scaleFactorX, dataArea.getMinY() + (delta.getY()) / scaleFactorY, w, h);
    }

    /**
     * Zooms plot
     * @param center mouse position-the center of zoom
     * @param scale scale factor
     */
    public void zoomIn(Point2D center, double scale) {
        points.needCalcDataArea = false;
        double nx = (renderArea.getWidth() != 0) ? ((center.getX() - renderArea.getMinX()) / renderArea.getWidth()) : 0.5;
        double ny = (renderArea.getHeight() != 0) ? ((center.getY() - renderArea.getMinY()) / renderArea.getHeight()) : 0.5;

        double scaleFactorX = (dataArea.getWidth() != 0) ? (renderArea.getWidth() / dataArea.getWidth()) : 1000000000;//double.MaxValue - 10f;
//        scaleFactorX = java.lang.Math.max(scaleFactorX, MIN_SCALE_FACTOR);
//        scaleFactorX = java.lang.Math.min(scaleFactorX, MAX_SCALE_FACTOR);


        double scaleFactorY = (dataArea.getHeight() != 0) ? (renderArea.getHeight() / dataArea.getHeight()) : 1000000000;
//        scaleFactorY = java.lang.Math.max(scaleFactorY, MIN_SCALE_FACTOR);
        //       scaleFactorY = java.lang.Math.min(scaleFactorY, MAX_SCALE_FACTOR);
        //disable scaling to bypass overflow exception
/*        if ((scaleFactorX >= MAX_SCALE_FACTOR || scaleFactorY >= MAX_SCALE_FACTOR) && scale < 1) {
        return;
        }
        if ((scaleFactorX <= MIN_SCALE_FACTOR || scaleFactorY <= MIN_SCALE_FACTOR) && scale > 1) {
        return;
        }*/

        double dx = renderArea.getMinX() - dataArea.getMinX() * scaleFactorX;
        double dy = renderArea.getMaxY() + dataArea.getMinY() * scaleFactorY;


        double mouse_center_x = (center.getX() - dx) / scaleFactorX;
        double mouse_center_y = -(center.getY() - dy) / scaleFactorY;

        dataArea.setRect(mouse_center_x - nx * dataArea.getWidth() * scale, mouse_center_y - (1 - ny) * dataArea.getHeight() * scale, dataArea.getWidth() * scale, dataArea.getHeight() * scale);
    }

    /**
     * Zooms the plot
     * @param newRenderArea new render area
     */
    public void zoomIn(Rectangle2D newRenderArea) {
        points.needCalcDataArea = false;
        System.out.println("zoomIn()");
        double nx = (newRenderArea.getMinX() - renderArea.getMinX()) / renderArea.getWidth();
        double ny = (newRenderArea.getMinY() - renderArea.getMinY()) / renderArea.getHeight();

        double sx = (newRenderArea.getWidth() / renderArea.getWidth());
        double sy = (newRenderArea.getHeight() / renderArea.getHeight());

        double w = dataArea.getWidth();
        double h = dataArea.getHeight();

        dataArea.setRect(dataArea.getMinX() + w * nx, dataArea.getMinY() + h * (1 - (ny + sy)), sx * w, h * sy);
    }

    /**
     * 
     * @param scale
     */
    public void zoomIn(double scale) {
        points.needCalcDataArea = false;
        double deltaX = dataArea.getWidth() * (1 - scale) * 0.5;
        double deltaY = dataArea.getHeight() * (1 - scale) * 0.5;

        double newWidth = dataArea.getWidth() * scale;
        double newHeight = dataArea.getHeight() * scale;

        dataArea.setRect(dataArea.getMinX() + deltaX, dataArea.getMinY() + deltaY, newWidth, newHeight);

    }

    /**
     *
     * @param p mouse cursor point on the control
     * @return is the point lies on vertical axis
     */
    private boolean isOnVerticalAxis(Point2D p) {
        if (p.getX() >= 0 && p.getX() <= margin.getLeft() && p.getY() >= 0 && p.getY() <= this.getHeight()) {
            return true;
        }

        return false;
    }

    /**
     *
     * @param p mouse cursor point on the control
     * @return is the point lies on horizontal axis
     */
    private boolean isOnHorizontalAxis(Point2D p) {
        if (p.getX() >= margin.getLeft() && p.getX() <= this.getWidth() && p.getY() >= (this.getHeight() - margin.getBottom()) && p.getY() <= this.getHeight()) {
            return true;
        }

        return false;
    }

    /**
     *
     * @param p mouse cursor point on the control
     * @return is the point lies on plot area
     */
    private boolean isOnPlot(Point2D p) {
        if (p.getX() >= margin.getLeft() && p.getX() <= (this.getWidth() - margin.getRight()) && p.getY() >= margin.getTop() && p.getY() <= (this.getHeight() - margin.getBottom())) {
            return true;
        }

        return false;
    }

    /**
     * updates cursor type that respects cursor position and key modifiers
     * @param e Mouse event
     */
    private void updateCursor(MouseEvent e) {
        //System.out.println("updateCursor");

        Point2D currPoint = new Point2D.Double(e.getX(), e.getY());

        if ((e.getModifiersEx() & MouseEvent.ALT_DOWN_MASK) != 0) {
            if (isOnPlot(currPoint)) {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                return;
            }
        }


        if ((e.getModifiersEx() & MouseEvent.CTRL_DOWN_MASK) != 0) {
            if (isOnPlot(currPoint)) {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
                return;
            }
        }

        if (isOnVerticalAxis(currPoint) || isDraggingY) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));//HAND_CURSOR
            return;
        }

        if (isOnHorizontalAxis(currPoint) || isDraggingX) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
            return;
        }

        this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

    }

    private void updateCursor(Point2D p, int modifiers) {
        //System.out.println("updateCursor");

        if (p == null) {
            return;
        }

        Point2D currPoint = new Point2D.Double(p.getX(), p.getY());

        if ((modifiers & MouseEvent.ALT_DOWN_MASK) != 0) {
            if (isOnPlot(currPoint)) {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                return;
            }
        }

        if ((modifiers & MouseEvent.CTRL_DOWN_MASK) != 0) {
            if (isOnPlot(currPoint)) {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
                return;
            }
        }

        if (isOnVerticalAxis(currPoint)) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
            return;
        }

        if (isOnHorizontalAxis(currPoint)) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
            return;
        }

        this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

    }
    private Point clickOffset = new Point(0, 0);//initial selection rect point
    private Point selectOffset = new Point(0, 0);//travelling selection rect point
    private boolean isSelecting = false;//is the selection mode enabled
    private boolean isDraggingX = false;//is the plot dragging by x axis using hand tool on axis
    private boolean isDraggingY = false;//is the plot dragging by y axis using hand tool on axis

    /**
     * Enables or disables selection mode
     * @param bSelection will be the selection mode enabled?
     */
    private void enableSelectionMode(boolean bSelection) {
        if (bSelection) {
            isSelecting = true;
            isDraggingX = false;
            isDraggingY = false;
        } else {
            selectOffset = clickOffset;
            isSelecting = false;
        }
    }

    /**
     * Draws the selection rect using paint(non-xor) mode
     * @param g Graphics context
     * @param p1 a selection point
     * @param p2 a selection point
     */
    private void drawSelectionRect(Graphics2D g, Point p1, Point p2) {
        Stroke save = g.getStroke();

        float dash[] = {2.0f};
        g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));

        int x1, y1, x2, y2;

        if (p1.getX() <= p2.getX()) {
            x1 = (int) p1.getX();
            x2 = (int) p2.getX();
        } else {
            x2 = (int) p1.getX();
            x1 = (int) p2.getX();
        }

        if (p1.getY() <= p2.getY()) {
            y1 = (int) p1.getY();
            y2 = (int) p2.getY();
        } else {
            y2 = (int) p1.getY();
            y1 = (int) p2.getY();
        }

        x1 = (int) java.lang.Math.max(renderArea.getMinX(), x1);
        y1 = (int) java.lang.Math.max(renderArea.getMinY(), y1);

        x2 = (int) java.lang.Math.min(renderArea.getMaxX(), x2);
        y2 = (int) java.lang.Math.min(renderArea.getMaxY(), y2);

        g.setColor(Color.darkGray);

        g.drawRect(x1, y1, x2 - x1, y2 - y1);
        g.setStroke(save);
    }

    /**
     * Draws the selection rect using xor mode. it's extremely slow on latest JDKs
     * @param g Graphics context
     * @param p1 a selection point
     * @param p2 a selection point
     */
    private void drawSelectionRect_xor(Graphics2D g, Point p1, Point p2) {

        Stroke save = g.getStroke();

        float dash[] = {2.0f};
        g.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));

        int x1, y1, x2, y2;

        if (p1.getX() <= p2.getX()) {
            x1 = (int) p1.getX();
            x2 = (int) p2.getX();
        } else {
            x2 = (int) p1.getX();
            x1 = (int) p2.getX();
        }

        if (p1.getY() <= p2.getY()) {
            y1 = (int) p1.getY();
            y2 = (int) p2.getY();
        } else {
            y2 = (int) p1.getY();
            y1 = (int) p2.getY();
        }

        x1 = (int) java.lang.Math.max(renderArea.getMinX(), x1);
        y1 = (int) java.lang.Math.max(renderArea.getMinY(), y1);

        x2 = (int) java.lang.Math.min(renderArea.getMaxX(), x2);
        y2 = (int) java.lang.Math.min(renderArea.getMaxY(), y2);

        g.setColor(Color.BLACK);
        g.setXORMode(Color.WHITE);

        g.drawRect(x1, y1, x2 - x1, y2 - y1);

        g.setPaintMode();

        g.setStroke(save);
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
        points.needCalcDataArea = false;
        if (e.getModifiers() == InputEvent.CTRL_MASK) {
            if (GeomUtils.isInBox(e.getPoint(), this.renderArea)) {
                double scale = java.lang.Math.pow(0.9, -e.getWheelRotation());
                this.zoomIn(e.getPoint(), scale);
                this.repaint();
            }
        }
    }

    public void mouseClicked(MouseEvent e) {
        points.needCalcDataArea = false;
//        points.partsVisible = false;
        updateCursor(e);
    }

    public void mousePressed(MouseEvent e) {
        points.needCalcDataArea = false;
        clickOffset = new Point(e.getX(), e.getY());
        selectOffset = new Point(e.getX(), e.getY());

        if ((!isOnPlot(new Point2D.Double(e.getX(), e.getY()))) && (!isDraggingX) && (!isDraggingY)) {
            enableSelectionMode(false);
        }

        updateCursor(e);
        //System.out.println("mousePressed");
        //g_save=(Graphics2D)this.getGraphics();
    }

    public void mouseReleased(MouseEvent e) {
        points.needCalcDataArea = false;
        if (isSelecting && java.lang.Math.abs(clickOffset.getX() - selectOffset.getX()) >= 5 && java.lang.Math.abs(clickOffset.getY() - selectOffset.getY()) >= 5) {
            onSelectionEnd(clickOffset, selectOffset);
        }

        enableSelectionMode(false);
        isDraggingX = false;
        isDraggingY = false;

        repaint();
        //System.out.println("mouseReleased");

        updateCursor(e);
    }

    public void mouseEntered(MouseEvent e) {
        updateCursor(e);
    }

    public void mouseExited(MouseEvent e) {
        // System.out.println("mouseExited");
    }

    public void mouseMoved(MouseEvent e) {
        points.needCalcDataArea = false;
        this.requestFocus();
        updateCursor(e);
    }

    public void mouseDragged(MouseEvent e) {
//        points.partsVisible = false;
        points.needCalcDataArea = false;
        Point2D currPoint = new Point2D.Double(e.getX(), e.getY());

        if (((e.getModifiersEx() & MouseEvent.ALT_DOWN_MASK) == 0) && isOnPlot(currPoint) && (!isDraggingX) && (!isDraggingY)) {
            enableSelectionMode(true);
        }

        if ((e.getModifiersEx() & MouseEvent.ALT_DOWN_MASK) != 0) {
            if (isOnPlot(currPoint)) {
                dragPlot(new Point2D.Double(e.getX() - clickOffset.getX(), e.getY() - clickOffset.getY()));
            }
            enableSelectionMode(false);
        }

        if (!isSelecting) {
            if (isOnVerticalAxis(currPoint) || isDraggingY) {
                dragPlot(new Point2D.Double(0, e.getY() - clickOffset.getY()));
                enableSelectionMode(false);
                isDraggingY = true;
            }

            if (isOnHorizontalAxis(currPoint) || isDraggingX) {
                dragPlot(new Point2D.Double(e.getX() - clickOffset.getX(), 0));
                enableSelectionMode(false);
                isDraggingX = true;
            }
        }

        clickOffset = new Point(e.getX(), e.getY());


        updateCursor(e);
        //repaint();
        //long time=System.currentTimeMillis();

        if (isSelecting) {
            //repaint();
            long time = System.currentTimeMillis();
            //if(g_save==null)g_save=(Graphics2D)this.getGraphics();
            /*drawSelectionRect_xor(g_save,selectOffset,clickOffset);
            System.out.println("time=" + (System.currentTimeMillis() - time));*/
            repaint();
        } else {
            repaint();
        }
    }

    public void keyPressed(KeyEvent e) {
        updateCursor(this.getMousePosition(), e.getModifiersEx());
    }

    public void keyReleased(KeyEvent e) {
        //this.getM
        updateCursor(this.getMousePosition(), e.getModifiersEx());
    }

    public void keyTyped(KeyEvent e) {
    }
}
