
package jbarsicgraph.curve;

import java.awt.Color;
import jbarsicgraph.points.PointStyle;
import jbarsicgraph.points.PointStyle.PointShape;

/**
 * style class of the curve 
 * @author Max
 * changed by Anita
 */
public class CurveStyle {
    /**
     * Style of the line
     */
    private LineStyle lineStyle = LineStyle.SOLID;
     /**
     * Width of the line
     */
    private double LineWidth = 1.0;
    /**
     * Color of the line
     */
    private Color LineColor = Color.BLACK;
     /**
     * Enables lines visualisation
     */
    private boolean ShowLines = true;
    
    private PointStyle pointStyle = new PointStyle();
    
    /**
     * Style of the points
     */
   
    private PointShape pointsShape =pointStyle.getPointShape();
    /**
     * Size of the points
     */
    private double pointsSize = pointStyle.getPointSize();
    /**
     * Color of the points
     */
    private Color pointsColor =pointStyle.getPointColor();
    /**
     * Enables points visualisation
     */
     private boolean ShowPoints = pointStyle.getShowPoint();
    /**
     * constructor by default
     */
    public CurveStyle()
    {        
        
    }
    
    /**
     * constructor
     * @param pointstyle
     * @param pointsize
     * @param pointcolor
     * @param linestyle
     * @param linewidth
     * @param linecolor
     * @param showPoints
     * @param showLines
     */
    public CurveStyle(
            PointStyle pointstyle,
            double pointsize,
            Color pointcolor,
            LineStyle linestyle,
            double linewidth,
            Color linecolor,
            boolean showPoints,
            boolean showLines)
    {
        this.pointStyle=pointstyle;
        this.pointsSize=pointsize;
        this.pointsColor=pointcolor;

        this.lineStyle=linestyle;
        this.LineWidth=linewidth;
        this.LineColor=linecolor;
        this.ShowLines=showLines;
        this.ShowPoints=showPoints;
    }
    
    
    
   
    /**
     * 
     */
    public enum LineStyle {

        /**
         * 
         */
        SOLID,
        /**
         * 
         */
        DASH,
        /**
         * 
         */
        DASHDOT,
        /**
         * 
         */
        DOT
    }

    

    /**
     * 
     * @return
     */
    public LineStyle getLineStyle() {
        return this.lineStyle;
    }

    /**
     * 
     * @param value
     */
    public void setLineStyle(LineStyle value) {
        this.lineStyle = value;
    }
   

    /**
     * 
     * @return
     */
    public double getLineWidth() {
        return this.LineWidth;
    }

    /**
     * 
     * @param value
     */
    public void setLineWidth(double value) {
        this.LineWidth = value;
    }
    

    /**
     * 
     * @return
     */
    public Color getLineColor() {
        return this.LineColor;
    }

    /**
     * 
     * @param value
     */
    public void setLineColor(Color value) {
        if(value!=null)
            this.LineColor = value;
    }
     /**
     * 
     * @return
     */
    public boolean getShowLines() {
        return this.ShowLines;
    }

    /**
     * 
     * @param value
     */
    public void setShowLines(boolean value) {
        this.ShowLines = value;
    }
    
    /**
     * gets shape of points 
     * @return
     */
    public PointShape getPointsShape() {
        return this.pointsShape;
    }

    /**
     * sets shape of points 
     * @param value
     */
    public void setPointsShape (PointShape value) {
        this.pointsShape = value;
    }
    
    /**
     * gets color of points 
     * @return
     */
    public Color getPointsColor() {
        return this.pointsColor;
    }

    /**
     * sets color of points 
     * @param value
     */
    public void setPointsColor(Color value) {
        if(value!=null)
            this.pointsColor = value;
    }
    

    /**
     * gets size of points
     * @return
     */
    public double getPointsSize() {
        return this.pointsSize;
    }

    /**
     * sets size of points
     * @param value
     */
    public void setPointsSize(double value) {
        this.pointsSize = value;
    }
      

    /**
     * 
     * @return
     */
    public boolean getShowPoints() {
        return this.ShowPoints;
    }

    /**
     * 
     * @param value
     */
    public void setShowPoints(boolean value) {
        this.ShowPoints = value;
    }

}
