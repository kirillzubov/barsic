/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jbarsicgraph.curve;

/**
 *
 * @author Max
 * @changed by Anita
 */
import jbarsicgraph.points.PointListAr;
import jbarsicgraph.*;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.*;
import java.util.Locale;

/**
 * Curve class
 */
public class Curve {

    /**
     * Points set of the curve
     */
    public PointListAr points;
    private String title;
    private CurveStyle style = new CurveStyle();
//    public PointStyle[] pointStyle = new PointStyle[0];
//    PointStyle pointstyle=new PointStyle();

    /**
     * Sets the style of the curve
     * @param value 
     */
    public void setStyle(CurveStyle value) {
        style = value;
    }

    /**
     * Returns the style of the curve
     * @return the style of the curve
     */
    public CurveStyle getStyle() {
        return style;
    }

    /**
     * 
     */
    public Curve() {
        this.points = new PointListAr();
    }

    /**
     * 
     * @param x
     * @param y
     */
    public Curve(double[] x, double[] y) {
        this.points = new PointListAr(x, y);
    }

    /**
     * Returns the alias of the curve
     * @return the alias of the curve
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the alias of the curve
     * @param title 
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Calculates scale factors(stretch multiplies) by x and y
     * @param dataArea
     * @param renderArea
     * @return x-coordinate of the returned Point2D instance:scaleFaxcor by x, y-coordinate:scaleFaxcor by x
     */
    private Point2D calcScaleFactors(Rectangle2D dataArea, Rectangle2D renderArea) {
        double scaleFactorX = (dataArea.getWidth() != 0) ? (renderArea.getWidth() / dataArea.getWidth()) : 1000000000;//double.MaxValue - 10f;
        double scaleFactorY = (dataArea.getHeight() != 0) ? (renderArea.getHeight() / dataArea.getHeight()) : 1000000000;
        return new Point2D.Double(scaleFactorX, scaleFactorY);
    }

    /**
     * Main Paint event
     *
     * @param g Graphics context
     * @param dataArea area of the values that we want to display
     * @param renderArea visualisation area       
     */
    public void draw(Graphics2D g, Rectangle2D dataArea, Rectangle2D renderArea) {
        AffineTransform save = g.getTransform();
        Point2D scaleFactor = calcScaleFactors(dataArea, renderArea);
        g.setClip((int) renderArea.getMinX(), (int) renderArea.getMinY(), (int) renderArea.getWidth(), (int) renderArea.getHeight());
        double dx = renderArea.getMinX() - dataArea.getMinX() * scaleFactor.getX();
        double dy = renderArea.getMaxY() + dataArea.getMinY() * scaleFactor.getY();
        long time = System.currentTimeMillis();
        if (style.getShowLines()) {
            drawLines_(g, dataArea, scaleFactor.getX(), scaleFactor.getY(), new AffineTransform(1, 0, 0, -1, dx, dy));// new Matrix(1,0,0,1,dx, dy));
        }
        g.setTransform(save);
        g.translate(dx, dy);
        if (style.getShowPoints()) {
            drawPoints(g, dataArea, scaleFactor.getX(), scaleFactor.getY());
        }
        g.setTransform(save);
        g.setClip(null);
    }

    /**
     * Draws the lines of the curve
     * @param g Graphics context
     * @param dataArea area of the values
     * @param scaleFactorX stretch multiplier by x
     * @param scaleFactorY stretch multiplier by y
     * @param translateTransform affine translate transform to origin of the plot
     */
    private void drawLines(Graphics2D g, Rectangle2D dataArea, double scaleFactorX, double scaleFactorY, AffineTransform translateTransform) {
        AffineTransform save = g.getTransform();
        g.translate(translateTransform.getTranslateX(), translateTransform.getTranslateY());
        g.scale(1, -1);
        int countOfCurvesPoints = this.points.getSize();
        Color saveColor = g.getColor();
        g.setColor(this.style.getLineColor());
        java.awt.Stroke saveStroke = g.getStroke();

        switch (style.getLineStyle()) {
            case SOLID:
                g.setStroke(new BasicStroke((float) style.getLineWidth()));
                break;
            case DASH:
                float[] dash = {10.0f};
                g.setStroke(new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));
                break;
            case DASHDOT:
                float[] dashdot = {10.0f, 9.0f, 2.0f, 9.0f};
                g.setStroke(new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dashdot, 0.0f));
                break;
            case DOT:
                float[] dot = {2.0f};
                g.setStroke(new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dot, 0.0f));
                break;
        }

        if (countOfCurvesPoints > 1) {
            for (int i = 0; i < countOfCurvesPoints - 1; i++) {
                g.draw(new Line2D.Double(this.points.x[i] * scaleFactorX,
                        this.points.y[i] * scaleFactorY,
                        this.points.x[i + 1] * scaleFactorX,
                        this.points.y[i + 1] * scaleFactorY));

            }
        }

        g.setColor(saveColor);
        g.setStroke(saveStroke);
        g.setTransform(save);
    }

    /**
     * Draws the lines of the curve
     * @param g Graphics context
     * @param dataArea area of the values
     * @param scaleFactorX stretch multiplier by x
     * @param scaleFactorY stretch multiplier by y
     * @param translateTransform affine translate transform to origin of the plot
     */
    private void drawLines_(Graphics2D g, Rectangle2D dataArea, double scaleFactorX, double scaleFactorY, AffineTransform translateTransform) {

        AffineTransform save = g.getTransform();

        g.translate(translateTransform.getTranslateX(), translateTransform.getTranslateY());

        g.scale(1, -1);

        int countOfCurvesPoints = this.points.getSize();

        Color saveColor = g.getColor();
        g.setColor(style.getLineColor());

        java.awt.Stroke saveStroke = g.getStroke();

        switch (style.getLineStyle()) {
            case SOLID:
                g.setStroke(new BasicStroke((float) style.getLineWidth()));
                break;
            case DASH:
                float[] dash = {10.0f};
                g.setStroke(new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));
                break;
            case DASHDOT:
                float[] dashdot = {10.0f, 9.0f, 2.0f, 9.0f};
                g.setStroke(new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dashdot, 0.0f));
                break;
            case DOT:
                float[] dot = {2.0f};
                g.setStroke(new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dot, 0.0f));
                break;
        }

        if (countOfCurvesPoints > 1) {
            for (int i = 0; i < countOfCurvesPoints - 1; i++) {
                g.draw(new Line2D.Double(
                        points.x[i] * scaleFactorX,
                        points.y[i] * scaleFactorY,
                        points.x[i + 1] * scaleFactorX,
                        points.y[i + 1] * scaleFactorY));
            }
        }

        g.setColor(saveColor);
        g.setStroke(saveStroke);
        g.setTransform(save);
    }

    /**
     * Draws the points of the curve
     * @param g Graphics context
     * @param dataArea area of the values
     * @param scaleFactorX stretch multiplier by x
     * @param scaleFactorY stretch multiplier by y
     */
    private void drawPoints(Graphics2D g, Rectangle2D dataArea, double scaleFactorX, double scaleFactorY) {
        //TODO:there is a bug during scaling of the curve
        int N_points = points.getSize();
        Color save = g.getColor();
        g.scale(scaleFactorX, -scaleFactorY);

        if (N_points > 0) {
            double sizex = style.getPointsSize() / scaleFactorX;
            double sizey = style.getPointsSize() / scaleFactorY;
            double width = 2 * sizex;
            double height = 2 * sizey;
            g.setColor(style.getPointsColor());
            switch (style.getPointsShape()) {
                case CIRCLE:
                    for (int i = 0; i < N_points; i++) // if (inBox[i])
                    {
                        g.fill(new Ellipse2D.Double(
                                points.x[i] - sizex,
                                points.y[i] - sizey,
                                width, height));
                    }
                    break;

                case RECTANGLE:
                    for (int i = 0; i < N_points; i++) // if (inBox[i])
                    {
                        g.fill(new Rectangle2D.Double(
                                points.x[i] - sizex,
                                points.y[i] - sizey,
                                width, height));
                    }
                    break;
            }
        }
        g.setColor(save);

    }

    /**
     * draws the points of the curve
     * @param g Graphics context
     * @param dataArea area of the values
     * @param scaleFactorX stretch multiplier by x
     * @param scaleFactorY stretch multiplier by y
     */
    private void drawPoints_(Graphics2D g, Rectangle2D dataArea, double scaleFactorX, double scaleFactorY) {
        java.util.Locale.setDefault(Locale.US);
        int N_points = points.getSize();
        Color save = g.getColor();
        if (N_points > 0) {
            double sizex = style.getPointsSize();
            double sizey = style.getPointsSize();
            double width = 2 * sizex;
            double height = 2 * sizey;
            for (int i = 0; i < 100; i++) {
                g.fill(new Ellipse2D.Double(5 * i, -5 * i, width, height));
            }
            g.setColor(style.getPointsColor());
            switch (style.getPointsShape()) {
                case CIRCLE:
                    for (int i = 0; i < N_points; i++) // if (inBox[i])
                    {
                        double x = (int) (points.x[i] * scaleFactorX - sizex);
                        double y = (int) (points.y[i] * (-scaleFactorY) - sizey);

                        System.out.print(String.format("(%f,%f,%f,%f)", x, y, sizex, sizey));

                        g.fill(new Ellipse2D.Double(x, y, width, height));
                    }
                    break;

                case RECTANGLE:
                    for (int i = 0; i < N_points; i++) // if (inBox[i])
                    {
                        g.fill(new Rectangle2D.Double(
                                points.x[i] * scaleFactorX - sizex,
                                points.y[i] * (-scaleFactorY) - sizey,
                                width, height));
                    }
                    break;
            }
        }
        System.out.println();
        System.out.println("----------");
        g.setColor(save);
    }

    /**
     *
     * @return manimal data area rhat contain all data points
     */
    public Rectangle2D inscribeToRectangle() {
        if (points.getSize() > 1) {
            double _ymax = -Double.MAX_VALUE + 10, _ymin = Double.MAX_VALUE - 10;
            double _xmax = 0;
            double _xmin = 0;
            int len = points.getSize();

            for (int i = 0; i < len; i++) {
                double y = points.y[i];
                if (y > _ymax) {
                    _ymax = y;
                }
                if (y < _ymin) {
                    _ymin = y;
                }
            }

            for (int i = 0; i < len; i++) {
                double x = points.x[i];
                if (x > _xmax) {
                    _xmax = x;
                }
                if (x < _xmin) {
                    _xmin = x;
                }
            }


            return new Rectangle2D.Double(_xmin - Math.abs(_xmin / 10), _ymin - Math.abs(_ymin / 10), _xmax - _xmin + 2 * Math.abs(_xmin / 10), _ymax - _ymin + 2 * Math.abs(_ymin / 10));
        }

        if (points.getSize() == 1) {
            return new Rectangle2D.Double(
                    points.x[0] - (JBarsicGraph.DEFAULT_DATA_AREA.getWidth() / 2),
                    points.y[0] - (JBarsicGraph.DEFAULT_DATA_AREA.getHeight() / 2),
                    JBarsicGraph.DEFAULT_DATA_AREA.getWidth(),
                    JBarsicGraph.DEFAULT_DATA_AREA.getHeight());
        }

        return JBarsicGraph.DEFAULT_DATA_AREA;
    }

    /**
     * 
     * @param g 
     * @param scaleFactor
     * @author Anita
     */
    public void drawNewPartsOfCurve(Graphics2D g, Point2D scaleFactor) {

        if (style.getShowLines()) {
            drawPartsOfLine(g, scaleFactor);
        }
        if (style.getShowPoints()) {
            drawNewPoints(g, scaleFactor);
        }
    }

    /**
     * 
     * @param g
     * @param scaleFactor  
     * @author Anita
     */
    public void drawNewPoints(Graphics2D g, Point2D scaleFactor) {
        AffineTransform saveTr = g.getTransform();
        g.scale(1, -1);
        Color save = g.getColor();
        g.setColor(style.getLineColor());
        if (points.numN == 0) {

            double sizex = this.points.pointStyle[0].getPointSize();
            double sizey = this.points.pointStyle[0].getPointSize();
            double width = 2 * sizex;
            double height = 2 * sizey;
            g.setColor(this.points.pointStyle[0].getPointColor());

            switch (this.points.pointStyle[0].getPointShape()) {

                case CIRCLE:
                    g.fill(new Ellipse2D.Double(
                            this.points.x[0] * scaleFactor.getX() - sizex,
                            this.points.y[0] * scaleFactor.getY() - sizey,
                            width, height));
                    break;
                case RECTANGLE:
                    g.fill(new Rectangle2D.Double(
                            this.points.x[0] * scaleFactor.getX() - sizex,
                            this.points.y[0] * scaleFactor.getY() - sizey,
                            width, height));
                    break;
            }
        }

        if (points.numN > 0) {
            for (int i = 0; i < points.numN + 1; i++) {
                double sizex = this.points.pointStyle[i].getPointSize();
                double sizey = this.points.pointStyle[i].getPointSize();
                double width = 2 * sizex;
                double height = 2 * sizey;
                g.setColor(this.points.pointStyle[i].getPointColor());
                switch (this.points.pointStyle[i].getPointShape()) {
                    case CIRCLE:
                        g.fill(new Ellipse2D.Double(
                                points.x[i] * scaleFactor.getX() - sizex,
                                points.y[i] * scaleFactor.getY() - sizey,
                                width, height));
                        break;
                    case RECTANGLE:
                        g.fill(new Rectangle2D.Double(
                                points.x[i] * scaleFactor.getX() - sizex,
                                points.y[i] * scaleFactor.getY() - sizey,
                                width, height));
                        break;

                }

            }
            g.setTransform(saveTr);
            g.setColor(save);
        }
    }

    /**
     * draws lines from last points to new points
     * @param g
     * @param scaleFactor 
     * @author Anita
     **/
    public void drawPartsOfLine(Graphics2D g, Point2D scaleFactor) {
        AffineTransform saveTr = g.getTransform();
        g.scale(1, -1);
        Color saveColor = g.getColor();
        g.setColor(style.getLineColor());
        java.awt.Stroke saveStroke = g.getStroke();

        switch (style.getLineStyle()) {
            case SOLID:
                g.setStroke(new BasicStroke((float) style.getLineWidth()));
                break;
            case DASH:
                float[] dash = {10.0f};
                g.setStroke(new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));
                break;
            case DASHDOT:
                float[] dashdot = {10.0f, 9.0f, 2.0f, 9.0f};
                g.setStroke(new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dashdot, 0.0f));
                break;
            case DOT:
                float[] dot = {2.0f};
                g.setStroke(new BasicStroke((float) style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dot, 0.0f));
                break;
        }


        if (this.points.numN == 0) {

            double x1 = this.points.x[0] * scaleFactor.getX();
            double y1 = this.points.y[0] * scaleFactor.getY();
            double x2 = this.points.x[0] * scaleFactor.getX();
            double y2 = this.points.y[0] * scaleFactor.getY();

            g.draw(new Line2D.Double(x1, y1, x2, y2));

        } else {
            for (int j = 0; j < this.points.numN; j++) {
                double x1 = this.points.x[j] * scaleFactor.getX();
                double y1 = this.points.y[j] * scaleFactor.getY();
                double x2 = this.points.x[j + 1] * scaleFactor.getX();
                double y2 = this.points.y[j + 1] * scaleFactor.getY();
                g.draw(new Line2D.Double(x1, y1, x2, y2));
            }
        }

        g.setTransform(saveTr);
        g.setColor(saveColor);
        g.setStroke(saveStroke);
    }
}
