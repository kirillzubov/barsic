/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jbarsicgraph.points;

/**
 * Point class
 */
public class Point {
    /**
     * constructor of point
     */
    public Point() {
        this.x = 0;
        this.y = 0;
    }

    /**
     * constructor of point
     * @param x
     * @param y
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * constructor of point
     * @param x
     * @param y
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    private double x;
    private double y;

    /**
     * 
     * @return x
     */
    public double getX() {
        return this.x;
    }

    /**
     * 
     * @return y
     */
    public double getY() {
        return this.y;
    }

    
}
