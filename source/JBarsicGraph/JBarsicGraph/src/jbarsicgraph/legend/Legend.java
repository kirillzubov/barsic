/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jbarsicgraph.legend;

import jbarsicgraph.AxisValue;
import jbarsicgraph.Margin;
import jbarsicgraph.curve.CurveStyle;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Ellipse2D;
import java.awt.*;
import java.awt.geom.AffineTransform;
import jbarsicgraph.points.PointStyle;

/**
 *
 * @author Max
 */
public class Legend {

    private Margin padding = new Margin(10, 5, 10, 5);

    /**
     * sets 
     * @param padding
     * (Anita's correction)
     */
    public void setPadding(Margin padding) {
        this.padding = padding;
    }

    /**
     * 
     * @return
     */
    public Margin getPadding() {
        return padding;
    }

    private Point2D pos;

    /**
     * 
     * @return
     */
    public Point2D getPos() {
        return pos;
    }

    /**
     * 
     * @param pos
     * (Anita's correction)
     */
    public void setPos(Point2D pos) {
        this.pos = pos;
    }

    /**
     * 
     */
    protected AxisValue.AlignW alignW = AxisValue.AlignW.Right;

    /**
     * 
     * @return
     */
    public AxisValue.AlignW getAlignW() {
        return alignW;
    }

    /**
     * 
     * @param alignW
     * (Anita's correction)
     */
    public void setAlignW(AxisValue.AlignW alignW) {
        this.alignW = alignW;
    }

    /**
     * 
     */
    protected AxisValue.AlignH alignH = AxisValue.AlignH.Bottom;

    /**
     * 
     * @return
     */
    public AxisValue.AlignH getAlignH() {
        return alignH;
    }

    /**
     * 
     * @param alignH 
     */
    public void setAlignH(AxisValue.AlignH alignH) {
        this.alignH = alignH;
    }

    /**
     * width of picrogram of line with point
     */
    private double pictogramWidth=40;

    /**
     * 
     * @param pictogramWidth 
     */
    public void setPictogramWidth(double pictogramWidth) {
        this.pictogramWidth = pictogramWidth;
    }

    private Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 12);

    /**
     * 
     * @return
     */
    public Font getfont() {
        return this.font;
    }

    /**
     * sets font
     * @param font
     * (Anita's correction)
     */
    public void setFont(Font font) {
        this.font = font;
    }
    /*
     * gradient color1
     */
    private Color color1 = Color.white;

    /**
     * 
     * @return
     */
    public Color getColor1() {
        return color1;
    }

    /**
     * 
     * @param color1
     * (Anita's correction)
     */
    public void setColor1(Color color1) {
        this.color1 = color1;
    }

    /*
     * gradient color2
     */
    private Color color2 = new Color(200, 200, 230);

    /**
     * 
     * @return
     */
    public Color getColor2() {
        return color2;
    }

    /**
     * 
     * @param color2
     * (Anita's correction)
     */
    public void setColor2(Color color2) {
        this.color2 = color2;
    }

    /**
     * sets identical color to color1 and color2
     * @param color
     */
    public void setColor(Color color) {
        this.color1 = color;
        this.color2 = color;
    }

    /**
     * sets different color to color1 and color2
     * @param color1
     * @param color2
     */
    public void setColor(Color color1, Color color2) {
        this.color1 = color1;
        this.color2 = color2;
    }

    /**
     * 
     */
    public enum GradientDirection {

        /**
         * 
         */
        Horizontal,
        /**
         * 
         */
        Vertical,
        /**
         * 
         */
        Sloped,
        /**
         * 
         */
        SlopedAlt
    }

    GradientDirection gradientDirection = GradientDirection.Sloped;

    /**
     * sets gradient direction
     * @param gradientDirection
     * (Anita's correction)
     */
    public void setGradientDirection(GradientDirection gradientDirection) {
        this.gradientDirection = gradientDirection;
    }

    /**
     *returns Gradient Direction
     * @return
     */
    public GradientDirection getGradientDirection() {
        return gradientDirection;
    }

    private java.awt.GradientPaint getGradient(double width, double height) {
        switch (gradientDirection) {
            case Sloped:
                return new java.awt.GradientPaint(0, 0, color1, (float)width, (float)height, color2);
            case SlopedAlt:
                return new java.awt.GradientPaint((float)width, 0, color1, 0, (float)height, color2);
            case Horizontal:
                return new java.awt.GradientPaint(0, 0, color1, (float)width, 0, color2);
            case Vertical:
                return new java.awt.GradientPaint(0, 0, color1, 0, (float)height, color2);
        }
        return null;
    }

    java.util.ArrayList<LegendItem> items = new java.util.ArrayList<LegendItem>();

    /**
     * 
     * @return
     */
    public int getItemsCount()
    {
        return items.size();
    }

    /**
     * 
     */
    public void clear() {
        items.clear();
    }

    /**
     * 
     * @param curve
     * @param title
     */
    public void addCurve(CurveStyle curve, String title) {
        items.add(new LegendItem(curve, title));
    }

    /**
     * @param g
     * @return rectangle that have width with
     */
    private Rectangle2D getTextBounds(Graphics2D g) {
        double xMax = 0;
        double yMax = 0;

        int size = items.size();

        for (int i = 0; i < size; i++) {
            LegendItem item = items.get(i);
            //GlyphVector glyphVector = font.createGlyphVector(g.getFontRenderContext(), item.getTitle());
            Rectangle2D bounds = font.createGlyphVector(g.getFontRenderContext(), item.getTitle()).getVisualBounds().getBounds2D();
            //max=java.lang.Math.max(glyphVector.getVisualBounds().getBounds2D().getWidth(),max);
            xMax = java.lang.Math.max(bounds.getWidth(), xMax);
            yMax = java.lang.Math.max(bounds.getHeight(), yMax);
        }

        return new Rectangle2D.Double(0, 0, xMax, yMax);
    }

    private void setTestData() {
        clear();
        CurveStyle style = new CurveStyle();
        style.setLineColor(Color.red);
        //style.setLineWidth(5.0);
        //style.setPointsSize(7.0);
        style.setLineStyle(CurveStyle.LineStyle.SOLID);
        style.setPointsColor(Color.blue);
        addCurve(style, "sin(x)/x");
        style = new CurveStyle();
        style.setLineColor(Color.blue);
        style.setLineWidth(2.0);
        style.setPointsShape(PointStyle.PointShape.RECTANGLE);
        style.setPointsSize(5.0);
        addCurve(style, "exp(x)");
        /*style=new CurveStyle();
        style.setLineColor(Color.GRAY);
        style.setLineStyle(CurveStyle.LineStyle.DASHDOT);
        style.setLineWidth(1.0);
        style.setPointStyle(CurveStyle.PointStyle.RECTANGLE);
        addCurve(style, "testing");
        //addCurve(new CurveStyle(), "very looooooooooong title");
        //addCurve(new CurveStyle(), "very looooooooooong title");
        style=new CurveStyle();
        style.setLineColor(Color.cyan);
        style.setPointsSize(4.0);
        style.setLineWidth(1.5);
        style.setPointsColor(Color.BLACK);
        addCurve(style, "item 1");
        addCurve(new CurveStyle(), "item 2");*/
    }

    private boolean transparentPane=false;
    /**
     * 
     * @param isTransparentPane
     */
    public void setTransparentField(boolean isTransparentPane) {
        color1 = null;
        color2 = null;
        this.transparentPane=isTransparentPane;
    }

    private boolean visible=true;

    /**
     * 
     * @param visible
     * (Anita's correction)
     */
    public void setVisible(boolean visible) {
        visible=visible;
    }

    private void drawDiagram(Graphics2D g, CurveStyle Style, double width, double x, double y) {
        Stroke save = g.getStroke();

        g.setColor(Style.getLineColor());

        switch (Style.getLineStyle()) {
            case SOLID:
                g.setStroke(new BasicStroke((float) Style.getLineWidth()));
                g.draw(new Line2D.Double(x + 0.5 * Style.getLineWidth(), y, x + width - 0.5 * Style.getLineWidth(), y));
                break;
            case DASH:
                float dash[] = {10.0f};
                g.setStroke(new BasicStroke((float) Style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f));
                g.draw(new Line2D.Double(x, y, x + width, y));
                break;
            case DASHDOT:
                float dashdot[] = {10.0f, 9.0f, 2.0f, 9.0f};
                g.setStroke(new BasicStroke((float) Style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dashdot, 0.0f));
                g.draw(new Line2D.Double(x, y, x + width, y));
                break;
            case DOT:
                float dot[] = {2.0f};
                g.setStroke(new BasicStroke((float) Style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dot, 0.0f));
                g.draw(new Line2D.Double(x, y, x + width, y));
                break;
        }


        //g.draw(new Line2D.Double(x+0.5*Style.getLineWidth(),y,x+width-0.5*Style.getLineWidth(),y));
        //g.draw(new Line2D.Double(x,y,x+width,y));

        g.setColor(Style.getPointsColor());
        double size = Style.getPointsSize();

        switch (Style.getPointsShape()) {
            case CIRCLE:
                g.fill(new Ellipse2D.Double(
                        x + 0.5 * width - size,
                        y - size,
                        2 * size, 2 * size));

                break;

            case RECTANGLE:
                g.fill(new Rectangle2D.Double(
                        x + 0.5 * width - size,
                        y - size,
                        2 * size, 2 * size));
                break;
        }


        g.setStroke(save);
    }

    private void fillPane(Graphics2D g,double width,double height)
    {
        if ((color1 != null) && (color2 != null)) {
            if (color1 != color2) {
                g.setPaint(getGradient(width, height));
            } else {
                g.setPaint(null);
                g.setColor(color1);
            }
            if (!transparentPane)
                g.fill(new Rectangle2D.Double(0, 0, width, height));
        }

        g.setPaint(null);

        g.setColor(Color.BLACK);

        g.draw(new Rectangle2D.Double(0, 0, width, height));        
    }
    
    /**
     * 
     * @param g
     */
    public void draw(Graphics2D g) {
        if(!visible)return;
        AffineTransform saveTransform = g.getTransform();

        g.setFont(font);

        //setTestData();
        int itemsCount = items.size();

        double xGap = 5;
        double yGap = 5;
       
        Rectangle2D textBounds = getTextBounds(g);

        double yStep = 2 * yGap + textBounds.getHeight();

        double width = pictogramWidth + xGap + textBounds.getWidth() + padding.getLeft() + padding.getRight();
        double height = padding.getTop() + padding.getBottom() + yStep * itemsCount;


        double splitX = 0;
        double splitY = 0;

        switch (alignW) {
            case Right:
                splitX = 0;
                break;
            case Center:
                splitX = -0.5 * width;
                break;
            case Left:
                splitX = -width;
                break;
        }


        switch (alignH) {
            case Bottom:
                splitY = 0;
                break;
            case Center:
                splitY = -0.5 * height;
                break;
            case Top:
                splitY = -height;
                break;
        }

        g.translate(pos.getX() + splitX, pos.getY() + splitY);


        Color startColor = Color.WHITE;
        Color endColor = new Color(200, 200, 230);

        //if ((startColor != null) && (endColor != null)) {
        //if (startColor != endColor) {
        // g.setPaint(new java.awt.GradientPaint(0, 0, startColor, width, height, endColor));
            /*} else {
        g.setPaint(null);
        g.setColor(startColor);
        }*/
        // g.fillRect(margin.getLeft(), margin.getTop(), width - margin.getLeft() - margin.getRight(),
        //        height - margin.getTop() - margin.getBottom());
        //
        //}
        // g.setPaint(null);

        //g.setColor(Color.yellow);
        //g.setColor(Color.WHITE);


        /*.setPaint(new java.awt.GradientPaint(0, 0, startColor, (float) width, (float) height, endColor));
        g.fill(new Rectangle2D.Double(0, 0, width, height));
        g.setPaint(null);


        g.setColor(Color.black);
        g.draw(new Rectangle2D.Double(0, 0, width, height));*/

        fillPane(g, width, height);
        AxisValue val = new AxisValue();
        val.alignH = alignH.Center;
        val.alignW = alignW.Center;
        val.setfont(font);

        for (int i = 0; i < itemsCount; i++) {
            g.setColor(Color.BLACK);
            double x = padding.getLeft() + pictogramWidth + 0.5 * textBounds.getWidth() + xGap;
            double y = padding.getTop() + (i + 0.5) * yStep;

            val.setValue(items.get(i).getTitle());
            val.draw(g, new Point2D.Double(x, y));

            /*g.setColor(Color.magenta);
            g.draw(new Rectangle2D.Double(padding.getLeft(),padding.getTop()+(i)*yStep,pictogramWidth,yStep));
            g.setColor(Color.black);
            g.draw(new Line2D.Double(0, padding.getTop()+(i+1)*yStep, width, padding.getTop()+(i+1)*yStep));
            g.setColor(Color.red);*/
            // g.draw(new Line2D.Double(0, padding.getTop()+(i+0.5)*yStep, width, padding.getTop()+(i+0.5)*yStep));

            /*g.setColor(Color.BLUE);
            g.draw(new Line2D.Double(padding.getLeft()+pictogramWidth+xGap,padding.getTop(),padding.getLeft()+pictogramWidth+xGap,height-padding.getBottom()));
             */

            drawDiagram(g, items.get(i).getCurveStyle(), pictogramWidth, padding.getLeft(), padding.getTop() + (i + 0.5) * yStep);
        }

        g.setTransform(saveTransform);

    }
}
