package barsic.app;

import barsic.Bootstrapper;
import barsic.app.modules.*;
import barsic.core.IBarsicModule;

public final class ModuleLoader {
    private static void registerModules() {
        Bootstrapper.init(new LibModule(), new SwingUIModule());
    }

    public static void main(String[] args) {
        registerModules();
        IBarsicModule module = new MainModule();
        module.onProgramStart();
    }
}