package barsic.app;

import barsic.*;
import barsic.math.*;
import barsic.standartClass.*;
import barsic.ui.*;
import barsic.ui.swing.form.Form;

import java.util.Arrays;

public class MainModule implements barsic.core.IBarsicModule {
    private static MainModule instance;

    public MainModule() {
        instance = this;
    }

    public static MainModule getInstance() {
        return instance;
    }

    double f2 = Bootstrapper.resolve(IStdLib.class).eval(("4." + "3"));
    double estr = Bootstrapper.resolve(IStdLib.class).eval("2");
    double z = Bootstrapper.resolve(IBarsicMath.class).min(2, 3);
    String s = "5";
    double mah = 1;
    double minX = 2 - mah;
    String stri = Bootstrapper.resolve(IStdLib.class).str(Bootstrapper.resolve(IStdLib.class).eval("1"));
    String polindrom = stri + " ono " + "1.0";
    double minXx = Bootstrapper.resolve(IBarsicMath.class).min(1, Bootstrapper.resolve(IStdLib.class).eval("2"));
    double num = Bootstrapper.resolve(IStdLib.class).eval("10");
    double xtest = 5;

    public void ifTest() {
        double var1 = Bootstrapper.resolve(IBarsicMath.class).random(1, 10);
        double var2 = Bootstrapper.resolve(IBarsicMath.class).random(1, 10);
        if ((var1 > var2)) {
            test1("1", "2", 3);

        } else {
            test2("1", 2);

        }

    }

    double xcv = 1;

    public void testIfIn() {
        double lenRail = Bootstrapper.resolve(IBarsicMath.class).min(Bootstrapper.resolve(IBarsicMath.class).random(), xcv);
        if (Arrays.asList(new Integer[]{1, 2, 3}).contains((int) lenRail)) {
            double dr = 100;

        }
        if (lenRail >= 5 && lenRail <= 10) {
            double dr = 10;

        }
        if (lenRail >= 50 && lenRail <= 100) {
            double dr = 1;

        }

    }

    public void testMultyArr() {
        IArray<Integer> doubleArray = new BArray(Integer.class, 101, 5);
        IArray<Boolean> tripleArray = new BArray(Boolean.class, 101, 5, 6);
        Integer da = doubleArray.getDArray()[(int) 1][(int) 2];
        Boolean ta = tripleArray.getTArray()[(int) 1][(int) 2][(int) 3];
        doubleArray.getDArray()[(int) 1][(int) 2] = 1;
        tripleArray.getTArray()[(int) 1][(int) 2][(int) 3] = true;

    }

    public void testCase1() {
        boolean isTrOn = true;
        double sign = 1;
        Object a;
        if (isTrOn) {
            a = sign;
        } else {
            a = 0;
        }
    }

    public void testCase2() {
        Object maxt;
        if ((CarV0 > 0)) {
            if (isTrOn) {
                if ((CarV0 >= 0)) {
                    maxt = CarV0;
                } else {
                    maxt = CarV0;
                }
            } else {
                maxt = lenRail;
            }
        }
        if ((CarV0 < 0)) {
            if (isTrOn) {
                if ((CarV0 >= 0)) {
                    maxt = CarV0;
                } else {
                    maxt = CarV0;
                }
            } else {
                maxt = CarV0;
            }
        } else {
            maxt = 10;
        }
    }

    double Y = 1;
    int i = 4;

    public void looptest() {
        double length = 1;
        for (int i = 0; i <= length; i++) {
            x = 2 * x - 1;
        }
        while ((Y < 10)) {
            Y = 1 + Y;
            i = 1 + i;
        }

    }

    public void test2(String t, double e) {
        double y = 1;
        double x = 2;

        test1("1", "2", (y - x));
    }

    public void iftest2() {
        IArray<Integer> ColorsX = new BArray(Integer.class, 7);
        boolean isDrawLine = false;
        boolean isDrawLine2 = true;
        if ((x < 1)) {
            if (isDrawLine) {
                double plot = 1;

            }
            if (isDrawLine2) {
                double plot = 2;

            } else {
                double plot = 3;

            }
            if (isDrawLine) {
                double plot = 4;

            } else {
                double plot = 5;

            }
            String r = "points";

        }

    }

    IArray<String> xt = new BArray(String.class, 7);
    String str = " to do";

    public void test1(String t, String s, double e) {
        double y = 1;
        double x = 2;
        xt.getSArray()[(int) 1] = "I try";

        String ittdi = str + xt.getSArray()[(int) 1] + t;
        double index = 1 + x1 + e - 4;
    }

    public void Calc() {
        test1("it", "2", 3);

    }

    IArray<Integer> ColorsX = new BArray(Integer.class, 7);
    IArray<Integer> ColorsV = new BArray(Integer.class, 9);
    IArray<String> StrLine = new BArray(String.class, 11);
    IArray<Boolean> markScheme = new BArray(Boolean.class, 10);
    double x1 = 1;
    double x2 = 2;

    public void testArra() {
        ColorsX.getSArray()[(int) 10] = ColorsX.getSArray()[(int) (ColorsX.getSArray()[(int) 3] - 1)];
        ColorsX.getSArray()[(int) ((6 / 3) + 1)] = ColorsX.getSArray()[(int) 5];
        ColorsX.getSArray()[(int) 2] = 1;
        markScheme.getSArray()[(int) 0] = true;
        String s = "d";
        StrLine.getSArray()[(int) 2] = "line2";
        String line = StrLine.getSArray()[(int) 2];
        String mark = StrLine.getSArray()[(int) 1];
        Boolean isBoo = markScheme.getSArray()[(int) 9];
        double a = 1;
        ColorsX.getSArray()[(int) 5] = ColorsX.getSArray()[(int) a];
        Integer color1 = ColorsV.getSArray()[(int) 1];
        String strcolor2 = Bootstrapper.resolve(IStdLib.class).str(ColorsV.getSArray()[(int) 2]);
        Integer ff = ColorsV.getSArray()[(int) (color1 + (3 * 6))];

    }

    String strA = "qwerty+foo";
    double b = 1;
    double c = 3 + b;
    double d = 3 / c;
    String a = strA;
    double f1 = Bootstrapper.resolve(IStdLib.class).eval("4.0");
    double smotry = 3;
    String e = Bootstrapper.resolve(IStdLib.class).str(((4 + 7) + 2));
    double x = Bootstrapper.resolve(IBarsicMath.class).max(4, 3);

    public void testdexer() {
        ColorsX.getSArray()[(int) 1] = 0xD0000;
        ColorsX.getSArray()[(int) 2] = 0x0000f;
        ColorsX.getSArray()[(int) 3] = 0xE000E;
        ColorsX.getSArray()[(int) 4] = 0xE1710;
        ColorsX.getSArray()[(int) 5] = 0xE0E0E;
        ColorsX.getSArray()[(int) 6] = 0x00E00;
        ColorsV.getSArray()[(int) 1] = 0x70000;
        ColorsV.getSArray()[(int) 2] = 0x00008;
        ColorsV.getSArray()[(int) 3] = 0x80008;
        ColorsV.getSArray()[(int) 4] = 0xaa550;
        ColorsV.getSArray()[(int) 5] = 0x80808;
        ColorsV.getSArray()[(int) 6] = 0x00800;

    }

    public void onProgramStart() {
        form1 = MainForm.createform1();
        initForm();
        form1.showForm();

    }

    int iW = 1000;

    public void form1_button1_onClick() {
        double myvar = 4;
        form1.setWidth(iW);
        form1.setTitle(strA);

    }

    public void exit() {
        form1.showForm();

    }

    double dropsShift = 0;
    double ColN = 1;
    boolean isColorSet = false;
    String s_path = "-------------------";
    double topBound = 20;
    double bottomBound = -10;
    double leftBound = -60;
    double rightBound = 60;
    double N = 20;
    double railForm = 0;
    double dr = 10;
    double dropsN = 0;
    String defaultParam = "";
    String defFolder = "";
    String s_calc = "0";
    double rDrop = 0;
    double zoom = 2;
    double lenRail = 100;
    double CarX0 = 0;
    double CarX = CarX0;
    double prevCarX = CarX;
    double CarV0 = 0;
    double CarV = CarV0;
    double prevCarV = CarV;
    double t0 = 0;
    double t = t0;
    double prevt = t;
    double tcV = 0;
    double maxt = 20;
    boolean maxtDefine = false;
    double tDelay = 1;
    boolean isDrawEqu = true;
    boolean equHasXt = true;
    boolean equHasVt = true;
    double DT = 1 * lenRail / N;
    boolean isThickPoint = true;
    boolean isDrawLine = true;
    boolean isThickLine = false;
    boolean isDrawXt = true;
    boolean isDrawVt = false;
    boolean showGraph = false;
    boolean showDrop = true;
    double kFtr = 0;
    boolean isTrOn = true;
    double g = 9;

    public void testInvocation() {
        String x = ColorsX.getLength();
        String y = button.getName();
        form1.button.text = "abc";
        String startT = system.getTimer().getTime();
        plot.axes.gridMode = GridMode.getROUGH();
        form1.clear();
        form1.refresh();
        form1.close();
        String defFolder = application.getPath().extractPath();

    }

    IButton button;
    ITextField textField1;
    ITextLabel textLabel2;
    IForm form1;

    void initForm() {
        button = (IButton) form1.getElementById("button");
        textField1 = (ITextField) form1.getElementById("textField1");
        textLabel2 = (ITextLabel) form1.getElementById("textLabel2");
    }

}



