package barsic.app;

import barsic.*;
import barsic.math.*;
import barsic.standartClass.*;
import barsic.ui.*;
import barsic.ui.swing.form.Form;

import java.util.Arrays;
public class MainForm{

    public static MainModule getModule()
    {
        return MainModule.getInstance();
    }
double version = 1185;
String stringVar = "value";
boolean boolVar = true;
    
    public static IForm createform1(){
        return new MainForm.form1();
}

    
public static class form1 extends Form {        
       form1() {
            createComponents();
        }
        protected void createComponents() {
IForm form1 = this;
form1.setTitle("Components task");
form1.setLeft(221);
form1.setTop(135);
form1.setClientWidth(355);
form1.setClientHeight(261);
form1.setHint("");
form1.setFont("Courier New,15,false,false,false,false,FontScript.getCYRILLIC(),clWindowText");
form1.setIsBorderCloseIcon(true);
form1.setIsResizeable(false);
form1.setIsEnabledOnCalc(false);

IButton button = Bootstrapper.resolve(IButton.class);
button.setOnClick(new Runnable() {@Override public void run() { getModule().form1_button1_onClick(); }   });
button.setIsDefaultOK(false);
button.setIsEnabledOnCalc(false);
button.setIsVisible(true);
button.setIsParentFont(true);
button.setTopPos(204);
button.setLeftPos(207);
button.setHint("another tooltip here");
button.setIsEnabled(true);
button.setName("button");
button.setWidth(128);
button.setText("Submit11");
button.setHeight(27);
form1.addElement(button);
ITextField textField1 = Bootstrapper.resolve(ITextField.class);
textField1.setAssociatedVariable("s");
textField1.setIsEnabledOnCalc(false);
textField1.setOnInput(new Runnable() {@Override public void run() { getModule().form1_button1_onClick(); }   });
textField1.setIsParentBackColor(false);
textField1.setIsVisible(true);
textField1.setIsParentFont(true);
textField1.setIsReadOnly(false);
textField1.setTopPos(204);
textField1.setLeftPos(207);
textField1.setIsSourceCode(true);
textField1.setHint("");
textField1.setIsEnabled(true);
textField1.setName("textField1");
textField1.setWidth(128);
textField1.setOnHelp(new Runnable() {@Override public void run() { getModule().form1_button1_onClick(); }   });
textField1.setText("Любой текст");
textField1.setHeight(31);
form1.addElement(textField1);
ITextLabel textLabel2 = Bootstrapper.resolve(ITextLabel.class);
textLabel2.setOnClick(new Runnable() {@Override public void run() { getModule().form1_button1_onClick(); }   });
textLabel2.setTextAlign(right);
textLabel2.setIsEnabledOnCalc(false);
textLabel2.setIsParentBackColor(true);
textLabel2.setAssociatedExpression("");
textLabel2.setIsVisible(true);
textLabel2.setIsParentFont(true);
textLabel2.setTopPos(69);
textLabel2.setLeftPos(32);
textLabel2.setHint("");
textLabel2.setIsEnabled(true);
textLabel2.setName("textLabel2");
textLabel2.setWidth(64);
textLabel2.setOnHelp(new Runnable() {@Override public void run() { getModule().form1_button1_onClick(); }   });
textLabel2.setText("subS=");
textLabel2.setHeight(23);
form1.addElement(textLabel2);

}
}

}



