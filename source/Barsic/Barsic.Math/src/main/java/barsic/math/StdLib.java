package barsic.math;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kirill Zubov on 28.03.2015.
 */

public class StdLib implements IStdLib {
    @Override
    public String str(double x) {
        return String.valueOf(x);
//        switch (x.length) {
//            case 0:
//                return "";
//            case 1:
//                return Double.toString(x[0]);
//            default:
//                StringBuilder sb = new StringBuilder();
//                for (int i = 0; i < x.length - 1; i++) {
//                    sb.append(x[0] + ",");
//                }
//                if (x.length > 0) {
//                    sb.append(x[x.length - 1]);
//                }
//                return sb.toString();
    }
    @Override
    public double eval(String x) {
            return Double.valueOf(x);
    }


    public String getName() {
        return "IStdLib";
    }

    public Class getNameClass() {
        return this.getClass();
    }

    @Override
    public List<String> getExports() {
        List<String> export = new ArrayList<String>();
        export.add("str");
        export.add("eval");
        return export;
    }
}
