package barsic.math;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Maxim Maximov, 2012
 * 2xmax@mail.ru
 * cph.maxim.maximov
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */
public class BarsicMathTests {
    private static double EPSILON = 0.000001;
    private IBarsicMath math;

    @Before
    public void setUp() {
        math = new BarsicMath();
    }

    @Test
    public void cotTests() {
        assertEquals(0, math.cot(Math.PI * 0.5), EPSILON);
        assertEquals(math.infinity(), math.cot(0), EPSILON);
        assertEquals(-math.infinity(), math.cot(Math.PI), EPSILON);
    }

    @Test
    public void lnTests() {
        assertEquals(1, math.ln(Math.E), EPSILON);
        assertEquals(2, math.ln(Math.E * Math.E), EPSILON);
        assertTrue(Double.isNaN(math.ln(-1)));
    }

    @Test
    public void absTests() {
        assertEquals(1000, math.abs(-1000), EPSILON);
        assertEquals(0, math.abs(0), EPSILON);
        assertEquals(math.infinity(), math.abs(-math.infinity()), EPSILON);
    }

    @Test
    public void truncTests() {
        assertEquals(78, math.trunc(77.7777, 0), EPSILON);
        assertEquals(77.8, math.trunc(77.7777, 1), EPSILON);
        assertEquals(77.78, math.trunc(77.7777, 2), EPSILON);
        assertEquals(-77.78, math.trunc(-77.7777, 2), EPSILON);
        assertEquals(0, math.trunc(0, 0), EPSILON);
    }

    @Test
    public void fracTests() {
        assertEquals(0.1, math.frac(1.1), EPSILON);
        assertEquals(0.9, math.frac(-1.1), EPSILON);
    }

    @Test
    public void minTests() {
        assertEquals(7, math.min(7, 8), EPSILON);
        assertEquals(-math.infinity(), math.min(-math.infinity(), 7, 8), EPSILON);
        assertEquals(7, math.min(7, 8, math.infinity()), EPSILON);
    }

    @Test
    public void maxTests() {
        assertEquals(8, math.max(7, 8), EPSILON);
        assertEquals(8, math.max(-math.infinity(), 7, 8), EPSILON);
        assertEquals(math.infinity(), math.max(7, 8, math.infinity()), EPSILON);
    }

    @Test
    public void powTests() {
        assertEquals(256, math.power(2, 8), EPSILON);
        assertEquals(1.414213562373095, math.power(2, 0.5), EPSILON);
        assertEquals(0, math.power(0, math.infinity()), EPSILON);
        assertEquals(0, math.power(0.5, math.infinity()), EPSILON);
        assertEquals(1, math.power(1, math.infinity()), EPSILON);
        assertEquals(math.infinity(), math.power(2, math.infinity()), EPSILON);
    }

    @Test
    public void expTests() {
        assertEquals(0, math.exp(-math.infinity()), EPSILON);
        assertEquals(math.infinity(), math.exp(math.infinity()), EPSILON);
    }
}
