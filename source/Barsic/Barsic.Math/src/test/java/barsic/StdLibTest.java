package barsic;

import barsic.math.IStdLib;
import barsic.math.StdLib;
import junit.framework.TestCase;

public class StdLibTest extends TestCase {

    private IStdLib lib = new StdLib();

    public void testStr() throws Exception {
        assertEquals("4.0,1.0", lib.str(4));
    }

    public void testSingleValue() throws Exception {
        assertEquals("42.0", lib.str(42));
    }
}