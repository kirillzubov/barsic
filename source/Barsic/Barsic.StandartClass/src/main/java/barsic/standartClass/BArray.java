package barsic.standartClass;

import java.lang.reflect.Array;

/**
 * Created by Kirill Zubov on 2018.
 */

public class BArray<T> implements IArray {
    private int length;
    public T[] array;

    // private final Class<? extends T> cls;
    public BArray(Class<? extends T> cls, int... size) {
        length = size[0];
        array = (T[]) Array.newInstance(cls, size);
    }

    public int getLength() {
        return length;
    }

    public T[] getSArray() {
        return (T[])array;
    }

    public T[][] getDArray() {
        return (T[][]) array;
    }

    public T[][][] getTArray() {
        return (T[][][]) array;
    }
    public Object getOArray() {
        return  array;
    }
}
