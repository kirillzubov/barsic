package barsic.ui.swing.form;

import barsic.ui.IButton;
import barsic.ui.IElement;
import barsic.ui.IForm;
import barsic.ui.ISubwindow;

import javax.swing.*;
import java.awt.*;

import static barsic.ui.swing.form.utils.ColorUtil.parse;

public class Form extends JFrame implements IForm {

    public Form() {
        super();
        //GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        //int formSize = 320 * gd.getDisplayMode().getHeight() / 768;
        //this.setSize(formSize, formSize);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        //todo:remove hardcode
        this.setPreferredSize(new Dimension(640, 640));
        //todo:create custom layout
        this.getContentPane().setLayout(null);
    }

    @Override
    public void addElement(IElement element) {
        this.getContentPane().add((Component) element);
    }

    @Override
    public void showForm() {
        this.setVisible(true);

    }
    @Override
    public void setLeftPos(int value) {
        this.setLeft(value);
    }

    @Override
    public void setTopPos(int value) {
        this.setTop(value);
    }

    @Override
    public void setWidth(int value) {
        this.setSize(value, getSize().height);
    }

    @Override
    public void setHeight(int value) {
        this.setSize(getSize().width, value);
    }

    @Override
    public void setAlign(String value) {

    }

    @Override
    public void setOnHelp(Runnable eventHandler) {

    }

    @Override
    public void setLeft(int value) {
        Rectangle rect = this.getBounds();
        rect.setLocation(value, new Double(rect.getY()).intValue());
        this.setBounds(rect);
    }

    @Override
    public void setTop(int value) {
        Rectangle rect = this.getBounds();
        rect.setLocation(new Double(rect.getX()).intValue(), value);
        this.setBounds(rect);
    }

    @Override
    public void setClientWidth(int value) {
        setWidth(value);
    }


    @Override
    public void setHint(String value) {

    }

    @Override
    public void setIsEnabled(boolean value) {

    }

    @Override
    public void setIsVisible(boolean value) {

    }

    @Override
    public void setClientHeight(int value) {
        setHeight(value);
    }

    @Override
    public void setBackColor(String value) {
        this.setBackground(parse(value));
    }

    //@Override
    public void setAssociatedArray(String value) {

    }

    @Override
    public void setIsResizeable(Boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setFont(String value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsBorderCloseIcon(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public IElement getElementById(String id) {
        Component[] components = this.getContentPane().getComponents();
        for (Component component : components) {
            if (component.getName()==id){
                return (IElement) component;
            }
//            if (component instanceof ISubwindow) {
//                return (ISubwindow) component;
//            }
//            if (component instanceof IButton) {
//                return (IButton) component;
//            }
        }
        return null;
    }

    public void setOnShow(String s) {

    }
    @Override
    public void refresh(){
    }
    @Override
    public void close(){
        System.exit(0);
    }
    @Override
    public void quit(){
        System.exit(0);
    }
    public void clear(){

    }
}
