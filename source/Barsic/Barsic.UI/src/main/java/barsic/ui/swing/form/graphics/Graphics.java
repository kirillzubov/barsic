package barsic.ui.swing.form.graphics;

import barsic.ui.IBrush;
import barsic.ui.IFont;
import barsic.ui.IGraph;
import barsic.ui.IPen;

public class Graphics implements IGraph {
    @Override
    public IBrush brush() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public IPen pen() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public IFont font() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double[] bounds() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setBounds(double xLeft, double xRight, double yBottom, double yTop) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void arc(double xCenter, double yCenter, double width, double height, double startAngle, double endAngle) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void chord(double xCenter, double yCenter, double width, double height, double startAngle, double endAngle) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void circle(double xCenter, double yCenter, double radius) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void ellipse(double xCenter, double yCenter, double width, double height) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void line(double x1, double y1, double x2, double y2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void picture(String file, double xLeft, double yTop, double xRight, double yBottom) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void pie(double xCenter, double yCenter, double width, double height, double startAngle, double endAngle) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void polygon(double[] xyPoints) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void polyLine(double[] xyPoints) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void rectangle(double xCenter, double yCenter, double width, double height) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void roundRectangle(double xCenter, double yCenter, double width, double height, double radiusX, double radiusY) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void text(String s, double xLeft, double yTop) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void update() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
