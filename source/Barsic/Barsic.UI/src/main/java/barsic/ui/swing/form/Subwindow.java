package barsic.ui.swing.form;

import barsic.ui.IPlot;
import barsic.ui.ISubwindow;

import javax.swing.*;
import java.awt.*;

public class Subwindow extends JPanel implements ISubwindow {
    private final IPlot plot;

    public Subwindow() {
        this.setSize(new Dimension(600, 600));
        plot = new Plot();
        this.add((Component) plot);
    }

    //@Override
    public void output() {

    }

    //@Override
    public void endOutput() {

    }

    @Override
    public void setClipping(String value) {

    }

    @Override
    public double xMin() {
        return 0;
    }

    @Override
    public double xMax() {
        return 0;
    }

    @Override
    public double yMin() {
        return 0;
    }

    @Override
    public double yMax() {
        return 0;
    }

    @Override
    public int pixels(char coordinate, double value) {
        return 0;
    }

    @Override
    public double x(int pixels) {
        return 0;
    }

    @Override
    public double y(int pixels) {
        return 0;
    }

    @Override
    public void setContext(String value) {

    }

    @Override
    public void setIsParentBackColor(boolean value) {

    }

    //@Override
    public void setLeft(int value) {
        Rectangle rect = this.getBounds();
        rect.setLocation(value, new Double(rect.getY()).intValue());
        this.setBounds(rect);
    }

    //@Override
    public void setTop(int value) {
        Rectangle rect = this.getBounds();
        rect.setLocation(new Double(rect.getX()).intValue(), value);
        this.setBounds(rect);
    }

    public void setLeftPos(int value) {

    }

    public void setTopPos(int value) {

    }

    //@Override
    public void setWidth(int value) {
        //plot.setWidth(value);
        this.setSize(value, getSize().height);
    }

    //@Override
    public void setHint(String value) {

    }

    //@Override
    public void setIsEnabled(boolean value) {

    }

    //@Override
    public void setIsVisible(boolean value) {

    }

    //@Override
    public void setHeight(int value) {
        //plot.setHeight(value);
        this.setSize(getSize().width, value);
    }

    //@Override
    public void setAlign(String value) {

    }

    public void setOnHelp(Runnable eventHandler) {

    }

    //@Override
    public void setBackColor(String value) {


    }

    //@Override
    public void setOnMouseDown(String value) {

    }

    //@Override
    public void setOnDrag(String value) {

    }

    //@Override
    public void setOnDragDrop(String value) {

    }

    //@Override
    public void setOnMouseOver(String value) {

    }

    //@Override
    public void setOnMouseScale(String value) {

    }

    @Override
    public void clear() {

    }

    @Override
    public void update() {

    }

    //@Override
    public void setAssociatedArray(String value) {

    }
}
