package barsic.ui.swing.form;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//quick implementation of function parser/
//todo:include it in grammar
public class FunctionParser {
    public static ParsingResult parse(String expr) {
        Pattern pattern = Pattern.compile("(.*)[,]([-]?\\d+)\\.\\.(\\d+)$");
        Matcher matcher = pattern.matcher(expr);
        String function = expr;
        int min = -10;
        int max = 10;
        if (matcher.matches()) {
            function = matcher.group(1);
            min = Integer.parseInt(matcher.group(2));
            max = Integer.parseInt(matcher.group(3));
        }
        return new ParsingResult(function, min, max);
    }

    public static class ParsingResult {
        private final double from;
        private final double to;
        private final String function;

        ParsingResult(String function, double from, double to) {
            this.from = from;
            this.to = to;
            this.function = function;
        }

        public double getFrom() {
            return from;
        }

        public double getTo() {
            return to;
        }

        public String getFunction() {
            return function;
        }
    }
}
