package barsic.ui.swing.form;

import barsic.ui.IButton;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static barsic.ui.swing.form.utils.ColorUtil.parse;

/**
 * Maxim Maximov, 2012
 * 2xmax@mail.ru
 * cph.maxim.maximov
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */
public class Button extends JButton implements IButton {
    public String text;

    @Override
    public void setWidth(int value) {
        this.setSize(value, getSize().height);
    }

    @Override
    public void setHint(String value) {
        this.setToolTipText(value);
    }

    @Override
    public void setTextSize(int value) {

    }

    @Override
    public void setIsEnabled(boolean value) {
        this.setEnabled(value);
    }

    @Override
    public void setIsVisible(boolean value) {
        this.setVisible(value);
    }

    @Override
    public void setHeight(int value) {
        this.setSize(getSize().width, value);
    }

    @Override
    public void setAlign(String value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    //@Override
    public void setBackColor(String value) {
        this.setBackground(parse(value));
    }

    //@Override
    public void setAssociatedArray(String value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setOnHelp(Runnable eventHandler) {

    }

    @Override
    public void setIsDefaultOK(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsParentFont(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setLeftPos(int value) {
        Rectangle rect = this.getBounds();
        rect.setLocation(value, new Double(rect.getY()).intValue());
        this.setBounds(rect);
    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setFont(String value) {

    }

    @Override
    public void setTopPos(int value) {
        Rectangle rect = this.getBounds();
        rect.setLocation(new Double(rect.getX()).intValue(), value);
        this.setBounds(rect);
    }

    @Override
    public void setOnClick(final Runnable eventHandler) {
        this.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                eventHandler.run();
            }
        });
    }

    @Override
    public String getTitle() {
        return this.getText();
    }

    @Override
    public void setTitle(String value) {
        this.setText(value);
    }

    @Override
    public void reload() {

    }
    @Override
    public String getName(){
        return super.getName();
    }
}
