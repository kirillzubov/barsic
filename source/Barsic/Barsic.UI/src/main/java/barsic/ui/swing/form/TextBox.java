package barsic.ui.swing.form;

import barsic.ui.ITextField;

import javax.swing.*;
import java.awt.*;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ru
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public class TextBox extends JTextField implements ITextField {
    @Override
    public void setWidth(int value) {
        this.setSize(value, this.getSize().height);
    }

    @Override
    public void setHint(String value) {
        this.setToolTipText(value);
    }

    @Override
    public void setTextSize(int value) {

    }

    @Override
    public void setIsSourceCode(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsParentBackColor(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsEnabled(boolean value) {
        this.setEnabled(value);
    }

    @Override
    public void setIsVisible(boolean value) {
        this.setVisible(value);
    }

    @Override
    public void setHeight(int value) {
        this.setSize(getSize().width, value);
    }

    @Override
    public void setAlign(String value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setOnHelp(Runnable eventHandler) {

    }

    @Override
    public void setIsReadOnly(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsParentFont(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setAssociatedVariable(String s) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    //@Override
    public void setBackColor(String value) {

    }

    //@Override
    public void setAssociatedArray(String value){
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setLeftPos(int value) {
        Rectangle rect = this.getBounds();
        rect.setLocation(value, new Double(rect.getY()).intValue());
        this.setBounds(rect);
    }

    @Override
    public void setTopPos(int value) {
        Rectangle rect = this.getBounds();
        rect.setLocation(new Double(rect.getX()).intValue(), value);
        this.setBounds(rect);
    }

    @Override
    public String getText() {
        return super.getText();
    }
}
