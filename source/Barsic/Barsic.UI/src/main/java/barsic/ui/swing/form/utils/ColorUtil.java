package barsic.ui.swing.form.utils;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class ColorUtil {
    private static final Map<String, Color> namedColorsMap;
    private static final Color DEFAULT_COLOR = Color.BLACK;

    static {
        namedColorsMap = new HashMap<String, Color>();
        namedColorsMap.put("clBtnFace", Color.gray);
        namedColorsMap.put("clWindowText", Color.black);
    }

    public static Color parse(String s) {
        if (s != null) {
            if (s.startsWith("#")) {
                try {
                    return parseHexColor(s);
                } catch (Exception ignored) {
                }
            }
            if (namedColorsMap.containsKey(s)) {
                return namedColorsMap.get(s);
            }
        }
        return DEFAULT_COLOR;
    }

    private static Color parseHexColor(String hex) {
        return Color.decode("0x" + hex.substring(1));
    }

}
