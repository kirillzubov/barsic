package barsic.ui.swing.form;

import barsic.ui.ITextLabel;

import javax.swing.*;
import java.awt.*;

import static barsic.ui.swing.form.utils.ColorUtil.parse;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ru
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public class TextLabel extends JLabel implements ITextLabel {
    @Override
    public void setIsParentBackColor(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsEnabled(boolean value) {
        this.setEnabled(value);
    }

    @Override
    public void setIsVisible(boolean value) {
        this.setVisible(value);
    }

    @Override
    public void setHeight(int value) {
        this.setSize(this.getSize().width, value);
    }

    @Override
    public void setAlign(String value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setOnHelp(Runnable eventHandler) {

    }

    @Override
    public void setIsReadOnly(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsParentFont(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setAssociatedVariable(String value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    //@Override
    public void setBackColor(String value) {
        this.setBackground(parse(value));
    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setHint(String value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setLeftPos(int value) {
        Rectangle rect = this.getBounds();
        rect.setLocation(value, new Double(rect.getY()).intValue());
        this.setBounds(rect);
    }

    @Override
    public void setTopPos(int value) {
        Rectangle rect = this.getBounds();
        rect.setLocation(new Double(rect.getX()).intValue(), value);
        this.setBounds(rect);
    }

    @Override
    public void setTextAlign(String value) {

    }

    public void setTextAlign(int value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setWidth(int value) {
        this.setSize(value, this.getSize().height);
    }

    //@Override
    public void setAssociatedExpression(String value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setFont(String value) {

    }

    @Override
    public String getText() {
        return super.getText();
    }
}
