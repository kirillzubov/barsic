package barsic.ui.swing.form;

import barsic.ui.ITextArea;

import javax.swing.*;
import java.awt.*;

import static barsic.ui.swing.form.utils.ColorUtil.parse;

public class TextArea extends JTextArea implements ITextArea {
    @Override
    public void setName(String value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setLeftPos(int value) {
        Rectangle rect = this.getBounds();
        rect.setLocation(value, new Double(rect.getY()).intValue());
        this.setBounds(rect);
    }

    @Override
    public void setTopPos(int value) {
        Rectangle rect = this.getBounds();
        rect.setLocation(new Double(rect.getX()).intValue(), value);
        this.setBounds(rect);
    }

    @Override
    public void setWidth(int value) {
        this.setSize(value, getSize().height);
    }

    @Override
    public void setHint(String value) {
        this.setToolTipText(value);
    }

    @Override
    public void setIsEnabled(boolean value) {
        this.setEnabled(value);
    }

    @Override
    public void setIsVisible(boolean value) {
        this.setVisible(value);
    }

    @Override
    public void setHeight(int value) {
        this.setSize(getSize().width, value);
    }

    @Override
    public void setAlign(String value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    //@Override
    public void setBackColor(String value) {
        this.setBackground(parse(value));
    }

    @Override
    public void setOnHelp(Runnable eventHandler) {
    }

    //@Override
    public void setAssociatedArray(String value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    // @Override
    // public void setText(String s) {
    //   //To change body of implemented methods use File | Settings | File Templates.
    // }

    @Override
    public void setIsParentBackColor(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsScrollBars(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsSourceCode(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsWordWrap(boolean b) {
        this.setWrapStyleWord(b);
    }

    @Override
    public void setIsReadOnly(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsParentFont(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setAssociatedVariable(String value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
