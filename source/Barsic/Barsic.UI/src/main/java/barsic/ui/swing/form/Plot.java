package barsic.ui.swing.form;

import barsic.ui.IPlot;
import jbarsicgraph.DefFunction;
import jbarsicgraph.JBarsicGraph;
import jbarsicgraph.points.PointStyle;

import java.awt.*;

@SuppressWarnings("WeakerAccess")
public class Plot extends JBarsicGraph implements IPlot {
    public Plot() {
        this.setPreferredSize(new Dimension(600, 600));
        this.getAxisX().setTitle("x");
        this.getAxisX().setUnits("units");
        this.getAxisY().setTitle("f(x)");
        this.getAxisY().setUnits("units");
        this.setVisible(true);
    }

    @Override
    public void setTitle(String value) {

    }

    @Override
    public void setFunction(String expr) {
        FunctionParser.ParsingResult parsingResult = FunctionParser.parse(expr);
        double xmin = Math.min(parsingResult.getFrom(), parsingResult.getTo()) + 0.0001;
        double xmax = Math.max(parsingResult.getFrom(), parsingResult.getTo()) - 0.0001;
        final int nPoints = 110;
        double step = (xmax - xmin) / (nPoints - 1);
        PointStyle pointstyle = new PointStyle(PointStyle.PointShape.RECTANGLE, 3.0, Color.BLUE);
        for (int i = 0; i < nPoints; i++) {
            double x = xmin + step * i;
            DefFunction defFun = new DefFunction(parsingResult.getFunction(), x, 'x');
            defFun.defFunction();
            double y = defFun.fun;
            points.addNewPointTo(x, y, i, pointstyle);
        }
        this.repaint();
    }

    //@Override
    public void setLeft(int value) {
    }

    //@Override
    public void setTop(int value) {
    }

    //@Override
    public void setWidth(int value) {
        this.setPreferredSize(new Dimension(value, this.getPreferredSize().height));
        this.repaint();
    }

    //@Override
    public void setHint(String value) {

    }

    //@Override
    public void setIsEnabled(boolean value) {

    }

    //@Override
    public void setIsVisible(boolean value) {

    }

    //@Override
    public void setHeight(int value) {
        this.setPreferredSize(new Dimension(this.getPreferredSize().width, value));
        this.repaint();
    }

    //@Override
    public void setAlign(String value) {

    }

    //@Override
    public void setBackColor(String value) {

    }

    //@Override
    public void setAssociatedArray(String value) {

    }
}
