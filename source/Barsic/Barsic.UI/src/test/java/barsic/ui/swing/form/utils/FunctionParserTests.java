package barsic.ui.swing.form.utils;

import barsic.ui.swing.form.FunctionParser;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FunctionParserTests {

    @Test
    public void testColor() {
        FunctionParser.ParsingResult parsingResult = FunctionParser.parse("sin(x)");
        assertEquals("sin(x)", parsingResult.getFunction());

        parsingResult = FunctionParser.parse("cos(x)");
        assertEquals("cos(x)", parsingResult.getFunction());


        parsingResult = FunctionParser.parse("ln(x),0..10");
        assertEquals("ln(x)", parsingResult.getFunction());
        assertEquals(0, parsingResult.getFrom(), Config.EPS);
        assertEquals(10, parsingResult.getTo(), Config.EPS);

        parsingResult = FunctionParser.parse("sin(x),-100500..9001");
        assertEquals("sin(x)", parsingResult.getFunction());
        assertEquals(-100500, parsingResult.getFrom(), Config.EPS);
        assertEquals(9001, parsingResult.getTo(), Config.EPS);


    }
}
