package barsic.ui.swing.form.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.awt.*;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ColorUtilTests {

    private String input;
    private Color expected;

    public ColorUtilTests(String input, Color expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection testCases() {
        return Arrays.asList(new Object[][]{
                //trivial or invalid values
                {null, Color.BLACK},
                {"fdldfsklsdf", Color.BLACK},
                {"#fdldfsklsdf", Color.BLACK},
                {"#", Color.BLACK},
                //standard values
                {"#000000", Color.BLACK},
                {"#ff0000", Color.red},
                {"#00ff00", Color.green},
                {"#0000ff", Color.blue},
                {"#ffc800", Color.ORANGE},
                {"#FFC800", Color.ORANGE},
        });
    }

    @Test
    public void testColor() {
        assertEquals(expected, barsic.ui.swing.form.utils.ColorUtil.parse(input));
    }
}
