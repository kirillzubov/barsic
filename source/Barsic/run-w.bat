@echo off
rem usage: 
rem run-w.bat Forms                                                 (it will output into the Barsic project-related app folder)
rem run-w.bat Android ..\BarsicGraph\app\src\main\java\barsic\app   (it will output into the BarsicGraph Android project)
rem run-w.bat C:\somedir\project.bpr C:\somedir\out                 (absolute path)

pushd "%~dp0"
if "%1%" equ "" (
    set ProjectName=Forms
) else (
    set ProjectName=%1%
)

if exist "%ProjectName%" (
	set ProjectPath=%ProjectName%
) else (
	set ProjectPath=Barsic.App\Samples\%ProjectName%\project.bpr
)

if not exist %ProjectPath% (
		goto end
)

if "%2%" equ "" (
    set OutputDir=Barsic.App\Samples\%ProjectName%\app
) else (
    set OutputDir=%2%
)

mkdir "%OutputDir%"

java -cp "Barsic.App/target/barsic.app-compiler-1.0-SNAPSHOT.jar;Barsic.App/target/barsic.app-1.0-SNAPSHOT.jar" barsic.JavaTranslator "%ProjectPath%" "%OutputDir%"
popd

:end