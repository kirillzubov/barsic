package barsic.math;

import barsic.utils.NotImplementedException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Maxim Maximov, 2012
 * 2xmax@mail.ru
 * cph.maxim.maximov
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */
@SuppressWarnings("UnusedDeclaration")
public class BarsicMath implements IBarsicMath {
    public static final double LN10 = 2.302585092994046;
    public static final double LN2 = 0.6931471805599453;
    public static final double LOG2E = 1.4426950408889634;
    public static final double LOG10E = 0.4342944819032518;
    public static final double SQRT1_2 = 0.7071067811865476;
    public static final double SQRT2 = 1.4142135623730951;
    @SuppressWarnings("WeakerAccess")
    public static double EPSILON = 0.000001;

    @Override
    public Double sin(Double arg) {
        return Math.sin(arg);
    }

    @Override
    public double pi() {
        return Math.PI;
    }

    @Override
    public double infinity() {
        return Double.POSITIVE_INFINITY;//MAX_VALUE;
    }

    @Override
    public double sin(double x) {
        return Math.sin(x);
    }

    @Override
    public double cos(double x) {
        return (x == Double.POSITIVE_INFINITY
                || x == Double.NEGATIVE_INFINITY)
                ? Double.NaN : Math.cos(x);
    }

    @Override
    public double tan(double x) {
        return Math.tan(x);
    }

    @Override
    public double cot(double x) {
        double sin = sin(x);
        double cos = cos(x);

        //todo: sin < eps
        if (sin < EPSILON) {
            return infinity() * sign(cos);
        }

        return cos / sin;
    }

    @Override
    public double ln(double x) {
        return Math.log(x);
    }

    @Override
    public double log10(double x) {
        return Math.log10(x);
    }

    @Override
    public double square(double x) {
        return x * x;
    }

    @Override
    public double sqrt(double x) {
        return Math.sqrt(x);
    }

    @Override
    public double atan(double x) {
        return Math.atan(x);
    }

    @Override
    public double asin(double x) {
        return Math.asin(x);
    }

    @Override
    public double acos(double x) {
        return Math.acos(x);
    }

    @Override
    public double acot(double x) {
        if (x == 0) {
            return Math.PI / 2;
        }
        return atan(1 / x);
    }

    @Override
    public double sh(double x) {
        return Math.sinh(x);
    }

    @Override
    public double ch(double x) {
        return Math.cosh(x);
    }

    @Override
    public double abs(double x) {
        // abs(-0.0) should be 0.0, but -0.0 < 0.0 == false
        return (x == 0.0) ? 0.0 : (x < 0.0) ? -x : x;
        //return Math.abs(x);
    }

    @Override
    public long round(double x) {
        return Math.round(x);
    }

    @Override
    public double floor(double x) {
        return Math.floor(x);
    }

    @Override
    public double ceil(double x) {
        return Math.ceil(x);
    }

    @Override
    public double trunc(double d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(d);
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public double frac(double x) {
        return x - floor(x);
    }

    @Override
    public double sign(Double x) {
        if (x > 0) {
            return 1;
        }

        if (x == 0) {
            return 0;
        }

        return -1;
    }

    @Override
    public double min(double a, double b) {
        return Math.min(a, b);
    }

    @Override
    public double min(double... args) throws IllegalArgumentException {
        if (args.length == 0) {
            throw new IllegalArgumentException();
        }
        double ret = args[0];
        for (int i = 1; i < args.length; i++) {
            ret = Math.min(ret, args[i]);
        }
        return ret;
    }

    @Override
    public double max(double a, double b) {
        return Math.max(a, b);
    }

    @Override
    public double max(double... args) throws IllegalArgumentException {
        if (args.length == 0) {
            throw new IllegalArgumentException();
        }
        double ret = args[0];
        for (int i = 1; i < args.length; i++) {
            ret = Math.max(ret, args[i]);
        }
        return ret;
    }

    @Override
    public double erf(double x) {
        //didn't test yet
        //http://en.wikipedia.org/wiki/Error_function
        //http://upload.wikimedia.org/math/f/2/9/f2996e9360b30d094ebb806879797c48.png
        int nTailor = 10;
        double sum = 0;
        double sqrx = x * x;
        for (int n = 0; n < nTailor; n++) {
            double prod = 1;
            for (int k = 0; k < n; k++) {
                prod *= -sqrx / (1.0 * k);
            }
            sum += prod * x / 1.0 * (2 * n + 1);
        }
        return 2 * Math.sqrt(Math.PI) * sum;
    }

    @Override
    public double erfcx(double args) throws NotImplementedException {
        //didn't test yet
        //http://en.wikipedia.org/wiki/Error_function
        throw new NotImplementedException();
    }

    @Override
    public double random(double min, double max) throws IllegalArgumentException {
        if (max < min) {
            throw new IllegalArgumentException("min < max");
        }
        return min + random() * (max - min);
    }

    public double random() {
        return Math.random();
    }

    @Override
    public double power(double value, double degree) {
        double result;
        if (degree != degree) {
            // y is NaN, result is always NaN
            result = degree;
        } else if (degree == 0) {
            // Java's pow(NaN, 0) = NaN; we need 1
            result = 1.0;
        } else if (value == 0) {
            // Many differences from Java's Math.pow
            if (1 / value > 0) {
                result = (degree > 0) ? 0 : Double.POSITIVE_INFINITY;
            } else {
                // x is -0, need to check if y is an odd integer
                long y_long = (long) degree;
                if (y_long == degree && (y_long & 0x1) != 0) {
                    result = (degree > 0) ? -0.0 : Double.NEGATIVE_INFINITY;
                } else {
                    result = (degree > 0) ? 0.0 : Double.POSITIVE_INFINITY;
                }
            }
        } else {
            result = Math.pow(value, degree);
            if (result != result) {
                // Check for broken Java implementations that gives NaN
                // when they should return something else
                if (degree == Double.POSITIVE_INFINITY) {
                    if (value < -1.0 || 1.0 < value) {
                        result = Double.POSITIVE_INFINITY;
                    } else if (-1.0 < value && value < 1.0) {
                        result = 0;
                    } else if (value == 1) {
                        result = 1;
                    }
                } else if (degree == Double.NEGATIVE_INFINITY) {
                    if (value < -1.0 || 1.0 < value) {
                        result = 0;
                    } else if (-1.0 < value && value < 1.0) {
                        result = Double.POSITIVE_INFINITY;
                    }
                } else if (value == Double.POSITIVE_INFINITY) {
                    result = (degree > 0) ? Double.POSITIVE_INFINITY : 0.0;
                } else if (value == Double.NEGATIVE_INFINITY) {
                    long y_long = (long) degree;
                    if (y_long == degree && (y_long & 0x1) != 0) {
                        // y is odd integer
                        result = (degree > 0) ? Double.NEGATIVE_INFINITY : -0.0;
                    } else {
                        result = (degree > 0) ? Double.POSITIVE_INFINITY : 0.0;
                    }
                }
            }
        }
        return result;
    }

    @Override
    public double exp(double degree) {
        return (degree == Double.POSITIVE_INFINITY) ? degree
                : (degree == Double.NEGATIVE_INFINITY) ? 0.0
                : Math.exp(degree);
    }

    public String getName(){
        return "IBarsicMath";
    }

    public  Class getNameClass(){
        return this.getClass();
    }

        @Override
    public List<String> getExports() {
        List<String> exports = new ArrayList<String>();
        exports.add("sin");
        exports.add("pi");
        exports.add("infinity");
        exports.add("cos");
        exports.add("ln");
        exports.add("log10");
        exports.add("square");
        exports.add("sqrt");
        exports.add("atan");
        exports.add("asin");
        exports.add("acos");
        exports.add("sh");
        exports.add("ch");
        exports.add("round");
        exports.add("floor");
        exports.add("ceil");
        exports.add("trunc");
        exports.add("frac");
        exports.add("sign");
        exports.add("erf");
        exports.add("erfcx");
        exports.add("power");
        exports.add("exp");
        exports.add("tan");
        exports.add("cot");
        exports.add("max");
        exports.add("min");
        exports.add("random");
        return exports;
    }
}

