package barsic.math;

import barsic.core.IBarsicLib;
import barsic.utils.NotImplementedException;

public interface IBarsicMath extends IBarsicLib {

    Double sin(Double arg);

    double pi();

    double infinity();

    double sin(double x);

    double cos(double x);

    double tan(double x);

    double cot(double x);

    double ln(double x);

    double log10(double x);

    double square(double x);

    double sqrt(double x);

    double atan(double x);

    double asin(double x);

    double acos(double x);

    double acot(double x);

    double sh(double x);

    double ch(double x);

    double abs(double x);

    long round(double x);

    double floor(double x);

    double ceil(double x);

    double trunc(double d, int decimalPlace);

    double frac(double x);

    double sign(Double x);

    double min(double a, double b);

    double min(double... args);

    double max(double a, double b);

    double max(double... args);

    double erf(double args);

    double erfcx(double args) throws NotImplementedException;

    double random(double min, double max);

    double random();

    double power(double value, double degree);

    double exp(double degree);
}
