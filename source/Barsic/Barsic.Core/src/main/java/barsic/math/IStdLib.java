package barsic.math;

import barsic.core.IBarsicLib;

/**
 * Created by root on 28.03.2015.
 */
public interface IStdLib extends IBarsicLib {
    String str(double x);
    double eval (String x);
}
