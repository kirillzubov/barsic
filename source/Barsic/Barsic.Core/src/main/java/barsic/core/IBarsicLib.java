package barsic.core;

import java.util.List;

/**
 * Created by root on 28.03.2015.
 */
public interface IBarsicLib {
    List<String> getExports();
    String getName();
    Class getNameClass();
}
