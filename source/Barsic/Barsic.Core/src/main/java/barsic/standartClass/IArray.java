package barsic.standartClass;

public interface IArray<T> {
    int getLength();

    T[] getSArray();

    T[][] getDArray();

    T[][][] getTArray();

    Object getOArray();

}

