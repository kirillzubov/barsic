package barsic.standartClass;

public interface ISystem {
    void reset();
    String getTime();
}
