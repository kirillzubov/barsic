package barsic.ui;

public interface ITextLabel extends IElement {
    void setText(String value);

    void setIsParentBackColor(boolean value);

    void setIsReadOnly(boolean value);

    void setIsParentFont(boolean value);

    void setAssociatedVariable(String value);

    void setIsEnabledOnCalc(boolean value);

    //todo:leave only single one
    void setTextAlign(int value);

    void setTextAlign(String value);

    void setAssociatedExpression(String value);

    void setFont(String value);
    public String getText();
}
