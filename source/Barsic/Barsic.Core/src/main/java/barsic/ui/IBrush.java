package barsic.ui;

public interface IBrush {
    
    String color();
    
    void setColor(String value);
    
    String style();
    
    void setStyle(String style);
    
}
