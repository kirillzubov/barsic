package barsic.ui;

public interface IPanel extends IElement {
    //todo:WTF? angle?
    void setBevelInner(String lowered);

    void setIsParentFont(boolean value);

    void setBevelOuter(String raised);

    void setIsParentBackColor(boolean value);

    void addElement(IPanel panel2);
}