package barsic.ui;

public interface ICheckBox extends IElement {
    void setText(String value);

    void setIsParentBackColor(boolean value);

    void setIsChecked(boolean value);

    void setIsParentFont(boolean value);

    void setAssociatedVariable(String value);

    void setIsEnabledOnCalc(boolean value);
}
