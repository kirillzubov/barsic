package barsic.ui;

public interface IForm extends IElement {

    void addElement(IElement element);

    void showForm();

    void setTitle(String value);

    void setWidth(int value);

    void setHeight(int value);

    void setLeft(int value);

    void setTop(int value);

    void setClientWidth(int value);

    void setHint(String value);

    void setClientHeight(int value);

    void setBackColor(String value);

    void setIsResizeable(Boolean value);

    void setFont(String value);

    void setIsBorderCloseIcon(boolean value);

    void setIsEnabledOnCalc(boolean value);

    IElement getElementById(String id);

    void setOnShow(String s);

    void refresh();

    void close();
    void quit();

    void clear();
}
