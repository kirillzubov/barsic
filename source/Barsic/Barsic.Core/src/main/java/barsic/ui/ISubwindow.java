package barsic.ui;

public interface ISubwindow extends IElement {

    double xMin();

    double xMax();

    double yMin();

    double yMax();

    int pixels(char coordinate, double value);

    double x(int pixels);

    double y(int pixels);

    void setClipping(String value);

    void setContext(String value);

    void setIsParentBackColor(boolean value);

    //Bitmap getBitmap();

    //IMouse mouse();

    void clear();

    void update();

}
