package barsic.ui;

public interface IFont {
    
    /*
    "name"
    "script"
    */
    
    String color();
    
    void setColor(String value);
    
    String face();
    
    void setFace(String value);
    
    double height();
    
    void setHeight(double value);
    
    boolean isBold();
    
    void setBold(boolean value);
    
    boolean isItalic();
    
    void setItalic(boolean value);
    
    boolean isStrikeout();
    
    void setStrikeout(boolean value);
    
    boolean isUnderline();
    
    void setUnderline(boolean value);
    
}
