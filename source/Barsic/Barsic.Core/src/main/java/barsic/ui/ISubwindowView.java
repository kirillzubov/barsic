package barsic.ui;

/**
 * Created by Мартынюк on 20.09.2015.
 */
public interface ISubwindowView extends IElement {

    void output();

    void endOutput();

    void setIsParentBackColor(boolean value);

    void setBackColor(String value);

    void setOnMouseDown(final Runnable eventHandler);

    void setOnDrag(final Runnable eventHandler);

    void setOnDragDrop(final Runnable eventHandler);

    void setOnMouseOver(final Runnable eventHandler);

    void setOnMouseScale(final Runnable eventHandler);

    /*
    "copyToMaximizedForm"
    "getBitmap"
    "getHandle"
    "pixelHeight"
    "pixelLeft"
    "pixelTop"
    "pixelWidth"
    */

    //todo: to IElement
    int getWidth();
    int getHeight();
}
