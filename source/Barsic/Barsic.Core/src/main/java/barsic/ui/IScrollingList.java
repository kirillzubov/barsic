package barsic.ui;

//todo:listbox?
public interface IScrollingList extends IElement {
    void setIsParentBackColor(boolean value);

    void setIsEnabledOnCalc(boolean value);

    void setAssociatedVariable(String value);

    void setIsParentFont(boolean value);
}