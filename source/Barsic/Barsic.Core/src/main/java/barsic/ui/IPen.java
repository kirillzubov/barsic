package barsic.ui;

public interface IPen {
    
    String color();
    
    void setColor(String value);
    
    double width();
    
    void setWidth(double value);
    
}
