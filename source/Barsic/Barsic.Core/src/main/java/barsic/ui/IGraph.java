package barsic.ui;

public interface IGraph {
    
    /*
    "arrow"
    "canvas.clear"
    "floodFill"
    "group"
    "objects"
    */
    
    IBrush brush();

    IPen pen();
    
    IFont font();
    
    double[] bounds();
    
    void setBounds(double xLeft, double xRight, double yBottom, double yTop);
    
    void arc(double xCenter, double yCenter, double width, double height,
             double startAngle, double endAngle);
    
    void chord(double xCenter, double yCenter, double width, double height,
               double startAngle, double endAngle);
    
    void circle(double xCenter, double yCenter, double radius);
    
    void ellipse(double xCenter, double yCenter, double width, double height);
    
    void line(double x1, double y1, double x2, double y2);
    
    void picture(String file, double xLeft, double yTop, double xRight, double yBottom);
    
    void pie(double xCenter, double yCenter, double width, double height,
             double startAngle, double endAngle);
    
    void polygon(double[] xyPoints);
    
    void polyLine(double[] xyPoints);
    
    void rectangle(double xCenter, double yCenter, double width, double height);
    
    void roundRectangle(double xCenter, double yCenter, double width, double height,
                        double radiusX, double radiusY);
    
    void text(String s, double xLeft, double yTop);
    
    void update();
    
}
