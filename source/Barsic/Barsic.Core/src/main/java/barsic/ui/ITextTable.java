package barsic.ui;

public interface ITextTable extends IElement {
    void setBgColorsArray(String value);

    void setCol0Width(int value);

    void setIsParentBackColor(boolean value);

    void setColDefaultWidth(int value);

    void setColorsArray(String value);

    void setIsHighlightCell(boolean value);

    void setIsReadOnly(boolean value);

    void setIsParentFont(boolean value);

    void setIsFixedRow(boolean value);

    void setColCaptions(String value);

    void setColCount(int value);

    void setIsFixedColumn(boolean value);

    void setRowCaptions(String value);
}