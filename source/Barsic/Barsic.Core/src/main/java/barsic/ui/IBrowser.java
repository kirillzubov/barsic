package barsic.ui;

/**
 * Created by Faze on 16.04.2015.
 */
public interface IBrowser extends IElement {
    void setUrl(String value);

    void reload();

    void executeJavascript(String code);
    //должен интерпретировать JS в консоли (например, broswer.executeJavascript("alert('hello');")
    // можешь еще что-нибудь придумать
}
