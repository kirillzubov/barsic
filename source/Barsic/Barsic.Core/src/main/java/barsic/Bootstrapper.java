package barsic;

import barsic.core.IBarsicLib;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

import java.util.ArrayList;
import java.util.List;

public class Bootstrapper {
    private static Injector injector;
    private static List<IBarsicLib> modules = new ArrayList<IBarsicLib>();

    public static void init(Module... modules) {
        Bootstrapper.injector = Guice.createInjector(modules);
    }

    public static <T> T resolve(java.lang.Class<T> tClass) {
        return injector.getInstance(tClass);
    }

    public static void registerModule(IBarsicLib lib){
        modules.add(lib);
    }

    public static IBarsicLib getModuleByExport(String exportName){
        for(IBarsicLib lib : modules){
            if(lib.getExports().contains(exportName)){
                return lib;
            }
        }
        return null;
    }
}
