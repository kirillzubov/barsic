#!/bin/sh

# clean
find . -name "target" -print0 | xargs -0 rm -rf
mvn clean

# build
mvn install