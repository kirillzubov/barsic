@echo off

IF [%M2_HOME%]==[] (
	echo installing maven using chocolatey package manager
	rem @powershell -NoProfile -ExecutionPolicy Bypass -Command "iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))" && SET PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin
	rem cinst -y mvn
)

pushd %~dp0
call "%M2_HOME%\bin\mvn" clean
call "%M2_HOME%\bin\mvn" install
popd
