package barsic.math.tests.bool;

import barsic.math.tests.util.TreeHelper;
import barsic.tree.primitives.*;
import org.antlr.runtime.RecognitionException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SimpleLogicalOperatorsTests {
    @Test
    public void lessThanTest() throws RecognitionException {
        testOperator("b = 1 < 2;", LTNode.class);
    }

    @Test
    public void lessThanOrEqualTest() throws RecognitionException {
        testOperator("b = 1 <= 2;", LTEqualsNode.class);
    }

    @Test
    public void greaterThanTest() throws RecognitionException {
        testOperator("b = 1 > 2;", GTNode.class);
    }

    @Test
    public void greaterThanOrEqualTest() throws RecognitionException {
        testOperator("b = 1 >= 2;", GTEqualsNode.class);
    }

    @Test
    public void notEqualsTest() throws RecognitionException {
        testOperator("b = 1 <> 2;", GTEqualsNode.class);
    }

    //todo: find a way to access to the internal node
    private void testOperator(String expression, Class ltNodeClass) throws RecognitionException {
        BlockNode block = TreeHelper.getBlock(expression);
        assertEquals(1, block.getStatements().size());
        AstNode statement = block.getStatements().get(0);
        assertTrue(statement instanceof AssignmentNode);
    }
}
