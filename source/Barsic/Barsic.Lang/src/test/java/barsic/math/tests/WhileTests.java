package barsic.math.tests;

import barsic.math.tests.util.TreeHelper;
import barsic.tree.primitives.AstNode;
import barsic.tree.primitives.BlockNode;
import barsic.tree.primitives.WhileStatementNode;
import org.antlr.runtime.RecognitionException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WhileTests {
    @Test
    public void simpleTest() throws RecognitionException {
        BlockNode block = TreeHelper.getBlock("x = 1; while x > 5 do x = x + 1; _do");
        assertEquals(2, block.getStatements().size());
        AstNode second = block.getStatements().get(1);
        assertTrue(second instanceof WhileStatementNode);
    }
}
