package barsic.math.tests.util;

import barsic.parser.BarsicLexer;
import barsic.parser.BarsicParser;
import barsic.parser.BarsicTreeWalker;
import barsic.tree.primitives.AstNode;
import barsic.tree.primitives.BlockNode;
import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;

import static org.junit.Assert.assertTrue;

public class TreeHelper {
    public static AstNode walk(String source) throws RecognitionException {
        ANTLRStringStream stream = new ANTLRStringStream(source);
        BarsicLexer lex = new BarsicLexer(stream);
        CommonTokenStream ts = new CommonTokenStream(lex);
        BarsicParser parser = new BarsicParser(ts);
        // Abstract Syntax Tree
        CommonTree ast = (CommonTree) parser.parse().getTree();

        // node stream from AST
        CommonTreeNodeStream nodeStream = new CommonTreeNodeStream(ast);

        // pass the reference to the Map of functions to the tree walker
        BarsicTreeWalker walker = new BarsicTreeWalker(nodeStream,
                parser.functions);

        // make transform of AST and search
        AstNode ret = walker.walk();
        return ret;
    }

    public static BlockNode getBlock(String source) throws RecognitionException {
        AstNode root = TreeHelper.walk(source);
        assertTrue(root instanceof BlockNode);
        return (BlockNode) root;
    }
}
