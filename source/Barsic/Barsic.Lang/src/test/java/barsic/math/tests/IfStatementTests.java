package barsic.math.tests;

import barsic.math.tests.util.TreeHelper;
import barsic.tree.primitives.AstNode;
import barsic.tree.primitives.BlockNode;
import barsic.tree.primitives.IfNode;
import org.antlr.runtime.RecognitionException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IfStatementTests {
    @Test
    public void simpleTest() throws RecognitionException {
        BlockNode block = TreeHelper.getBlock("ax = 3; if ax > 2 => ax=4; _if");
        assertEquals(2, block.getStatements().size());
        AstNode second = block.getStatements().get(1);
        assertTrue(second instanceof IfNode);
    }
}
