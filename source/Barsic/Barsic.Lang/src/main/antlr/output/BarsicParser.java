// $ANTLR 3.4 C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g 2018-05-29 20:22:11

  package barsic.parser;
  import barsic.Scope;
  import barsic.Function;
  import barsic.tree.BarsicValue;
  import barsic.tree.functions.PrintlnNode;
  import barsic.tree.form.FormNode;
  import barsic.tree.primitives.*;
  import java.util.Map;
  import java.util.HashMap;
  import java.util.Arrays;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class BarsicParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ARRAY", "ARRAYCALL", "ASSIGNMENT", "ASSIGNMENT_LIST", "And", "Array", "BELONGS", "BLOCK", "Begin", "Bool", "Button", "CASE", "CASE1", "CASE2", "CASEASSIGMENT", "CASEEXP", "CASEEXPELSE", "CBrace", "CLOSE", "CTOR", "Case", "Close", "CloseIdentifier", "Comment", "Def", "Digit", "Do", "EXP", "EXPARRAY", "EXP_LIST", "Else", "End", "EndCase", "EndDo", "EndIf", "FUNCTIONCALL", "FUNC_CALL", "FUNC_DEF", "FUNC_EXP", "File", "For", "Form", "FormType", "Hence", "IDENTIFIER", "IDENTIFIERLIST_AND_TYPE", "IDENTIFIER_LIST", "IDFORMLIST", "ID_LIST", "IF1", "IF2", "Identifier", "If", "Import", "In", "Infinity", "Int", "LIST", "LOOKUP", "NAMECLASS", "NEGATE", "NUMLIST", "New", "Null", "Number", "OBrace", "Of", "Or", "PROCEDURE", "PROCEDUREPARAMETER", "Panel", "Println", "Procedure", "RETURN", "Return", "SIZE", "SIZES", "STATEMENTS", "Space", "Step", "String", "Subwindow", "TERNARY", "TYPE1", "TYPE2", "TYPE3", "TYPE4", "TYPECAST2LIST", "TYPECASTEXP", "TYPECASTID", "TextField", "TextLabel", "To", "Type", "UNARY_MIN", "Variables", "While", "'!'", "'!='", "'%'", "'&&'", "'('", "')'", "'*'", "'+'", "','", "'-'", "'.'", "'/'", "':'", "';'", "'<'", "'<='", "'='", "'=='", "'>'", "'>='", "'?'", "'['", "']'", "'^'", "'_output'", "'output'", "'|'", "'||'"
    };

    public static final int EOF=-1;
    public static final int T__101=101;
    public static final int T__102=102;
    public static final int T__103=103;
    public static final int T__104=104;
    public static final int T__105=105;
    public static final int T__106=106;
    public static final int T__107=107;
    public static final int T__108=108;
    public static final int T__109=109;
    public static final int T__110=110;
    public static final int T__111=111;
    public static final int T__112=112;
    public static final int T__113=113;
    public static final int T__114=114;
    public static final int T__115=115;
    public static final int T__116=116;
    public static final int T__117=117;
    public static final int T__118=118;
    public static final int T__119=119;
    public static final int T__120=120;
    public static final int T__121=121;
    public static final int T__122=122;
    public static final int T__123=123;
    public static final int T__124=124;
    public static final int T__125=125;
    public static final int T__126=126;
    public static final int T__127=127;
    public static final int T__128=128;
    public static final int ARRAY=4;
    public static final int ARRAYCALL=5;
    public static final int ASSIGNMENT=6;
    public static final int ASSIGNMENT_LIST=7;
    public static final int And=8;
    public static final int Array=9;
    public static final int BELONGS=10;
    public static final int BLOCK=11;
    public static final int Begin=12;
    public static final int Bool=13;
    public static final int Button=14;
    public static final int CASE=15;
    public static final int CASE1=16;
    public static final int CASE2=17;
    public static final int CASEASSIGMENT=18;
    public static final int CASEEXP=19;
    public static final int CASEEXPELSE=20;
    public static final int CBrace=21;
    public static final int CLOSE=22;
    public static final int CTOR=23;
    public static final int Case=24;
    public static final int Close=25;
    public static final int CloseIdentifier=26;
    public static final int Comment=27;
    public static final int Def=28;
    public static final int Digit=29;
    public static final int Do=30;
    public static final int EXP=31;
    public static final int EXPARRAY=32;
    public static final int EXP_LIST=33;
    public static final int Else=34;
    public static final int End=35;
    public static final int EndCase=36;
    public static final int EndDo=37;
    public static final int EndIf=38;
    public static final int FUNCTIONCALL=39;
    public static final int FUNC_CALL=40;
    public static final int FUNC_DEF=41;
    public static final int FUNC_EXP=42;
    public static final int File=43;
    public static final int For=44;
    public static final int Form=45;
    public static final int FormType=46;
    public static final int Hence=47;
    public static final int IDENTIFIER=48;
    public static final int IDENTIFIERLIST_AND_TYPE=49;
    public static final int IDENTIFIER_LIST=50;
    public static final int IDFORMLIST=51;
    public static final int ID_LIST=52;
    public static final int IF1=53;
    public static final int IF2=54;
    public static final int Identifier=55;
    public static final int If=56;
    public static final int Import=57;
    public static final int In=58;
    public static final int Infinity=59;
    public static final int Int=60;
    public static final int LIST=61;
    public static final int LOOKUP=62;
    public static final int NAMECLASS=63;
    public static final int NEGATE=64;
    public static final int NUMLIST=65;
    public static final int New=66;
    public static final int Null=67;
    public static final int Number=68;
    public static final int OBrace=69;
    public static final int Of=70;
    public static final int Or=71;
    public static final int PROCEDURE=72;
    public static final int PROCEDUREPARAMETER=73;
    public static final int Panel=74;
    public static final int Println=75;
    public static final int Procedure=76;
    public static final int RETURN=77;
    public static final int Return=78;
    public static final int SIZE=79;
    public static final int SIZES=80;
    public static final int STATEMENTS=81;
    public static final int Space=82;
    public static final int Step=83;
    public static final int String=84;
    public static final int Subwindow=85;
    public static final int TERNARY=86;
    public static final int TYPE1=87;
    public static final int TYPE2=88;
    public static final int TYPE3=89;
    public static final int TYPE4=90;
    public static final int TYPECAST2LIST=91;
    public static final int TYPECASTEXP=92;
    public static final int TYPECASTID=93;
    public static final int TextField=94;
    public static final int TextLabel=95;
    public static final int To=96;
    public static final int Type=97;
    public static final int UNARY_MIN=98;
    public static final int Variables=99;
    public static final int While=100;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public BarsicParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public BarsicParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return BarsicParser.tokenNames; }
    public String getGrammarFileName() { return "C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g"; }


      public Map<String, Function> functions = new HashMap<String, Function>();


    public static class parse_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "parse"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:79:1: parse : block EOF -> block ;
    public final BarsicParser.parse_return parse() throws RecognitionException {
        BarsicParser.parse_return retval = new BarsicParser.parse_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token EOF2=null;
        BarsicParser.block_return block1 =null;


        Object EOF2_tree=null;
        RewriteRuleTokenStream stream_EOF=new RewriteRuleTokenStream(adaptor,"token EOF");
        RewriteRuleSubtreeStream stream_block=new RewriteRuleSubtreeStream(adaptor,"rule block");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:80:3: ( block EOF -> block )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:80:6: block EOF
            {
            pushFollow(FOLLOW_block_in_parse300);
            block1=block();

            state._fsp--;

            stream_block.add(block1.getTree());

            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_parse302);  
            stream_EOF.add(EOF2);


            // AST REWRITE
            // elements: block
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 80:16: -> block
            {
                adaptor.addChild(root_0, stream_block.nextTree());

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "parse"


    public static class block_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "block"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:84:1: block : ( statement )* ( Return expression ';' )? -> ^( BLOCK ^( STATEMENTS ( statement )* ) ^( RETURN ( expression )? ) ) ;
    public final BarsicParser.block_return block() throws RecognitionException {
        BarsicParser.block_return retval = new BarsicParser.block_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Return4=null;
        Token char_literal6=null;
        BarsicParser.statement_return statement3 =null;

        BarsicParser.expression_return expression5 =null;


        Object Return4_tree=null;
        Object char_literal6_tree=null;
        RewriteRuleTokenStream stream_Return=new RewriteRuleTokenStream(adaptor,"token Return");
        RewriteRuleTokenStream stream_114=new RewriteRuleTokenStream(adaptor,"token 114");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_statement=new RewriteRuleSubtreeStream(adaptor,"rule statement");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:85:3: ( ( statement )* ( Return expression ';' )? -> ^( BLOCK ^( STATEMENTS ( statement )* ) ^( RETURN ( expression )? ) ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:85:6: ( statement )* ( Return expression ';' )?
            {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:85:6: ( statement )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==Array||LA1_0==Button||(LA1_0 >= File && LA1_0 <= Form)||(LA1_0 >= Identifier && LA1_0 <= If)||LA1_0==Panel||LA1_0==Procedure||LA1_0==Subwindow||(LA1_0 >= TextField && LA1_0 <= TextLabel)||LA1_0==While) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:85:7: statement
            	    {
            	    pushFollow(FOLLOW_statement_in_block323);
            	    statement3=statement();

            	    state._fsp--;

            	    stream_statement.add(statement3.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:85:19: ( Return expression ';' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==Return) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:85:20: Return expression ';'
                    {
                    Return4=(Token)match(input,Return,FOLLOW_Return_in_block328);  
                    stream_Return.add(Return4);


                    pushFollow(FOLLOW_expression_in_block330);
                    expression5=expression();

                    state._fsp--;

                    stream_expression.add(expression5.getTree());

                    char_literal6=(Token)match(input,114,FOLLOW_114_in_block332);  
                    stream_114.add(char_literal6);


                    }
                    break;

            }


            // AST REWRITE
            // elements: expression, statement
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 86:6: -> ^( BLOCK ^( STATEMENTS ( statement )* ) ^( RETURN ( expression )? ) )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:86:9: ^( BLOCK ^( STATEMENTS ( statement )* ) ^( RETURN ( expression )? ) )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(BLOCK, "BLOCK")
                , root_1);

                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:86:17: ^( STATEMENTS ( statement )* )
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(STATEMENTS, "STATEMENTS")
                , root_2);

                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:86:30: ( statement )*
                while ( stream_statement.hasNext() ) {
                    adaptor.addChild(root_2, stream_statement.nextTree());

                }
                stream_statement.reset();

                adaptor.addChild(root_1, root_2);
                }

                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:86:42: ^( RETURN ( expression )? )
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(RETURN, "RETURN")
                , root_2);

                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:86:51: ( expression )?
                if ( stream_expression.hasNext() ) {
                    adaptor.addChild(root_2, stream_expression.nextTree());

                }
                stream_expression.reset();

                adaptor.addChild(root_1, root_2);
                }

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "block"


    public static class statement_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "statement"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:89:1: statement : ( assignment ';' -> assignment | objectInvocation ';' -> objectInvocation | functionCall ';' -> functionCall | ifStatement | forStatement | whileStatement | caseAssigment | procedure ';' -> procedure );
    public final BarsicParser.statement_return statement() throws RecognitionException {
        BarsicParser.statement_return retval = new BarsicParser.statement_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal8=null;
        Token char_literal10=null;
        Token char_literal12=null;
        Token char_literal18=null;
        BarsicParser.assignment_return assignment7 =null;

        BarsicParser.objectInvocation_return objectInvocation9 =null;

        BarsicParser.functionCall_return functionCall11 =null;

        BarsicParser.ifStatement_return ifStatement13 =null;

        BarsicParser.forStatement_return forStatement14 =null;

        BarsicParser.whileStatement_return whileStatement15 =null;

        BarsicParser.caseAssigment_return caseAssigment16 =null;

        BarsicParser.procedure_return procedure17 =null;


        Object char_literal8_tree=null;
        Object char_literal10_tree=null;
        Object char_literal12_tree=null;
        Object char_literal18_tree=null;
        RewriteRuleTokenStream stream_114=new RewriteRuleTokenStream(adaptor,"token 114");
        RewriteRuleSubtreeStream stream_objectInvocation=new RewriteRuleSubtreeStream(adaptor,"rule objectInvocation");
        RewriteRuleSubtreeStream stream_assignment=new RewriteRuleSubtreeStream(adaptor,"rule assignment");
        RewriteRuleSubtreeStream stream_functionCall=new RewriteRuleSubtreeStream(adaptor,"rule functionCall");
        RewriteRuleSubtreeStream stream_procedure=new RewriteRuleSubtreeStream(adaptor,"rule procedure");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:90:3: ( assignment ';' -> assignment | objectInvocation ';' -> objectInvocation | functionCall ';' -> functionCall | ifStatement | forStatement | whileStatement | caseAssigment | procedure ';' -> procedure )
            int alt3=8;
            alt3 = dfa3.predict(input);
            switch (alt3) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:90:6: assignment ';'
                    {
                    pushFollow(FOLLOW_assignment_in_statement373);
                    assignment7=assignment();

                    state._fsp--;

                    stream_assignment.add(assignment7.getTree());

                    char_literal8=(Token)match(input,114,FOLLOW_114_in_statement375);  
                    stream_114.add(char_literal8);


                    // AST REWRITE
                    // elements: assignment
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 90:23: -> assignment
                    {
                        adaptor.addChild(root_0, stream_assignment.nextTree());

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:91:6: objectInvocation ';'
                    {
                    pushFollow(FOLLOW_objectInvocation_in_statement388);
                    objectInvocation9=objectInvocation();

                    state._fsp--;

                    stream_objectInvocation.add(objectInvocation9.getTree());

                    char_literal10=(Token)match(input,114,FOLLOW_114_in_statement390);  
                    stream_114.add(char_literal10);


                    // AST REWRITE
                    // elements: objectInvocation
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 91:27: -> objectInvocation
                    {
                        adaptor.addChild(root_0, stream_objectInvocation.nextTree());

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:92:6: functionCall ';'
                    {
                    pushFollow(FOLLOW_functionCall_in_statement401);
                    functionCall11=functionCall();

                    state._fsp--;

                    stream_functionCall.add(functionCall11.getTree());

                    char_literal12=(Token)match(input,114,FOLLOW_114_in_statement403);  
                    stream_114.add(char_literal12);


                    // AST REWRITE
                    // elements: functionCall
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 92:23: -> functionCall
                    {
                        adaptor.addChild(root_0, stream_functionCall.nextTree());

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 4 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:93:6: ifStatement
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_ifStatement_in_statement414);
                    ifStatement13=ifStatement();

                    state._fsp--;

                    adaptor.addChild(root_0, ifStatement13.getTree());

                    }
                    break;
                case 5 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:94:6: forStatement
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_forStatement_in_statement421);
                    forStatement14=forStatement();

                    state._fsp--;

                    adaptor.addChild(root_0, forStatement14.getTree());

                    }
                    break;
                case 6 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:95:6: whileStatement
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_whileStatement_in_statement428);
                    whileStatement15=whileStatement();

                    state._fsp--;

                    adaptor.addChild(root_0, whileStatement15.getTree());

                    }
                    break;
                case 7 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:96:6: caseAssigment
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_caseAssigment_in_statement435);
                    caseAssigment16=caseAssigment();

                    state._fsp--;

                    adaptor.addChild(root_0, caseAssigment16.getTree());

                    }
                    break;
                case 8 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:97:6: procedure ';'
                    {
                    pushFollow(FOLLOW_procedure_in_statement442);
                    procedure17=procedure();

                    state._fsp--;

                    stream_procedure.add(procedure17.getTree());

                    char_literal18=(Token)match(input,114,FOLLOW_114_in_statement444);  
                    stream_114.add(char_literal18);


                    // AST REWRITE
                    // elements: procedure
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 97:19: -> procedure
                    {
                        adaptor.addChild(root_0, stream_procedure.nextTree());

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "statement"


    public static class functionCall_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "functionCall"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:100:1: functionCall : Identifier '(' ( exprList )? ')' -> ^( FUNCTIONCALL Identifier ( exprList )? ) ;
    public final BarsicParser.functionCall_return functionCall() throws RecognitionException {
        BarsicParser.functionCall_return retval = new BarsicParser.functionCall_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Identifier19=null;
        Token char_literal20=null;
        Token char_literal22=null;
        BarsicParser.exprList_return exprList21 =null;


        Object Identifier19_tree=null;
        Object char_literal20_tree=null;
        Object char_literal22_tree=null;
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
        RewriteRuleTokenStream stream_105=new RewriteRuleTokenStream(adaptor,"token 105");
        RewriteRuleTokenStream stream_106=new RewriteRuleTokenStream(adaptor,"token 106");
        RewriteRuleSubtreeStream stream_exprList=new RewriteRuleSubtreeStream(adaptor,"rule exprList");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:101:3: ( Identifier '(' ( exprList )? ')' -> ^( FUNCTIONCALL Identifier ( exprList )? ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:101:6: Identifier '(' ( exprList )? ')'
            {
            Identifier19=(Token)match(input,Identifier,FOLLOW_Identifier_in_functionCall461);  
            stream_Identifier.add(Identifier19);


            char_literal20=(Token)match(input,105,FOLLOW_105_in_functionCall463);  
            stream_105.add(char_literal20);


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:101:21: ( exprList )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==Array||(LA4_0 >= Bool && LA4_0 <= Button)||LA4_0==File||LA4_0==Form||LA4_0==Identifier||(LA4_0 >= New && LA4_0 <= OBrace)||LA4_0==Panel||(LA4_0 >= String && LA4_0 <= Subwindow)||(LA4_0 >= TextField && LA4_0 <= TextLabel)||LA4_0==101||LA4_0==110) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:101:21: exprList
                    {
                    pushFollow(FOLLOW_exprList_in_functionCall465);
                    exprList21=exprList();

                    state._fsp--;

                    stream_exprList.add(exprList21.getTree());

                    }
                    break;

            }


            char_literal22=(Token)match(input,106,FOLLOW_106_in_functionCall468);  
            stream_106.add(char_literal22);


            // AST REWRITE
            // elements: exprList, Identifier
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 101:36: -> ^( FUNCTIONCALL Identifier ( exprList )? )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:101:39: ^( FUNCTIONCALL Identifier ( exprList )? )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(FUNCTIONCALL, "FUNCTIONCALL")
                , root_1);

                adaptor.addChild(root_1, 
                stream_Identifier.nextNode()
                );

                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:101:65: ( exprList )?
                if ( stream_exprList.hasNext() ) {
                    adaptor.addChild(root_1, stream_exprList.nextTree());

                }
                stream_exprList.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "functionCall"


    public static class procedure_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "procedure"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:104:1: procedure : Procedure Identifier '(' ( procedureParameter )? ')' ( Import identifiertList )? ( Variables paramList )? ( Begin | OBrace ) block ( End | CBrace ) -> ^( PROCEDURE Identifier ( procedureParameter )? ( identifiertList )? ( paramList )? block ) ;
    public final BarsicParser.procedure_return procedure() throws RecognitionException {
        BarsicParser.procedure_return retval = new BarsicParser.procedure_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Procedure23=null;
        Token Identifier24=null;
        Token char_literal25=null;
        Token char_literal27=null;
        Token Import28=null;
        Token Variables30=null;
        Token Begin32=null;
        Token OBrace33=null;
        Token End35=null;
        Token CBrace36=null;
        BarsicParser.procedureParameter_return procedureParameter26 =null;

        BarsicParser.identifiertList_return identifiertList29 =null;

        BarsicParser.paramList_return paramList31 =null;

        BarsicParser.block_return block34 =null;


        Object Procedure23_tree=null;
        Object Identifier24_tree=null;
        Object char_literal25_tree=null;
        Object char_literal27_tree=null;
        Object Import28_tree=null;
        Object Variables30_tree=null;
        Object Begin32_tree=null;
        Object OBrace33_tree=null;
        Object End35_tree=null;
        Object CBrace36_tree=null;
        RewriteRuleTokenStream stream_Variables=new RewriteRuleTokenStream(adaptor,"token Variables");
        RewriteRuleTokenStream stream_OBrace=new RewriteRuleTokenStream(adaptor,"token OBrace");
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
        RewriteRuleTokenStream stream_Import=new RewriteRuleTokenStream(adaptor,"token Import");
        RewriteRuleTokenStream stream_105=new RewriteRuleTokenStream(adaptor,"token 105");
        RewriteRuleTokenStream stream_106=new RewriteRuleTokenStream(adaptor,"token 106");
        RewriteRuleTokenStream stream_Begin=new RewriteRuleTokenStream(adaptor,"token Begin");
        RewriteRuleTokenStream stream_Procedure=new RewriteRuleTokenStream(adaptor,"token Procedure");
        RewriteRuleTokenStream stream_End=new RewriteRuleTokenStream(adaptor,"token End");
        RewriteRuleTokenStream stream_CBrace=new RewriteRuleTokenStream(adaptor,"token CBrace");
        RewriteRuleSubtreeStream stream_procedureParameter=new RewriteRuleSubtreeStream(adaptor,"rule procedureParameter");
        RewriteRuleSubtreeStream stream_identifiertList=new RewriteRuleSubtreeStream(adaptor,"rule identifiertList");
        RewriteRuleSubtreeStream stream_paramList=new RewriteRuleSubtreeStream(adaptor,"rule paramList");
        RewriteRuleSubtreeStream stream_block=new RewriteRuleSubtreeStream(adaptor,"rule block");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:105:3: ( Procedure Identifier '(' ( procedureParameter )? ')' ( Import identifiertList )? ( Variables paramList )? ( Begin | OBrace ) block ( End | CBrace ) -> ^( PROCEDURE Identifier ( procedureParameter )? ( identifiertList )? ( paramList )? block ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:105:5: Procedure Identifier '(' ( procedureParameter )? ')' ( Import identifiertList )? ( Variables paramList )? ( Begin | OBrace ) block ( End | CBrace )
            {
            Procedure23=(Token)match(input,Procedure,FOLLOW_Procedure_in_procedure493);  
            stream_Procedure.add(Procedure23);


            Identifier24=(Token)match(input,Identifier,FOLLOW_Identifier_in_procedure496);  
            stream_Identifier.add(Identifier24);


            char_literal25=(Token)match(input,105,FOLLOW_105_in_procedure498);  
            stream_105.add(char_literal25);


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:105:31: ( procedureParameter )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==Identifier) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:105:31: procedureParameter
                    {
                    pushFollow(FOLLOW_procedureParameter_in_procedure500);
                    procedureParameter26=procedureParameter();

                    state._fsp--;

                    stream_procedureParameter.add(procedureParameter26.getTree());

                    }
                    break;

            }


            char_literal27=(Token)match(input,106,FOLLOW_106_in_procedure503);  
            stream_106.add(char_literal27);


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:106:3: ( Import identifiertList )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==Import) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:106:4: Import identifiertList
                    {
                    Import28=(Token)match(input,Import,FOLLOW_Import_in_procedure508);  
                    stream_Import.add(Import28);


                    pushFollow(FOLLOW_identifiertList_in_procedure513);
                    identifiertList29=identifiertList();

                    state._fsp--;

                    stream_identifiertList.add(identifiertList29.getTree());

                    }
                    break;

            }


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:108:2: ( Variables paramList )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==Variables) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:108:3: Variables paramList
                    {
                    Variables30=(Token)match(input,Variables,FOLLOW_Variables_in_procedure519);  
                    stream_Variables.add(Variables30);


                    pushFollow(FOLLOW_paramList_in_procedure523);
                    paramList31=paramList();

                    state._fsp--;

                    stream_paramList.add(paramList31.getTree());

                    }
                    break;

            }


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:110:3: ( Begin | OBrace )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==Begin) ) {
                alt8=1;
            }
            else if ( (LA8_0==OBrace) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;

            }
            switch (alt8) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:110:4: Begin
                    {
                    Begin32=(Token)match(input,Begin,FOLLOW_Begin_in_procedure530);  
                    stream_Begin.add(Begin32);


                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:110:12: OBrace
                    {
                    OBrace33=(Token)match(input,OBrace,FOLLOW_OBrace_in_procedure534);  
                    stream_OBrace.add(OBrace33);


                    }
                    break;

            }


            pushFollow(FOLLOW_block_in_procedure540);
            block34=block();

            state._fsp--;

            stream_block.add(block34.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:112:3: ( End | CBrace )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==End) ) {
                alt9=1;
            }
            else if ( (LA9_0==CBrace) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;

            }
            switch (alt9) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:112:4: End
                    {
                    End35=(Token)match(input,End,FOLLOW_End_in_procedure545);  
                    stream_End.add(End35);


                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:112:10: CBrace
                    {
                    CBrace36=(Token)match(input,CBrace,FOLLOW_CBrace_in_procedure549);  
                    stream_CBrace.add(CBrace36);


                    }
                    break;

            }


            // AST REWRITE
            // elements: procedureParameter, paramList, block, identifiertList, Identifier
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 112:20: -> ^( PROCEDURE Identifier ( procedureParameter )? ( identifiertList )? ( paramList )? block )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:112:23: ^( PROCEDURE Identifier ( procedureParameter )? ( identifiertList )? ( paramList )? block )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(PROCEDURE, "PROCEDURE")
                , root_1);

                adaptor.addChild(root_1, 
                stream_Identifier.nextNode()
                );

                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:112:46: ( procedureParameter )?
                if ( stream_procedureParameter.hasNext() ) {
                    adaptor.addChild(root_1, stream_procedureParameter.nextTree());

                }
                stream_procedureParameter.reset();

                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:112:66: ( identifiertList )?
                if ( stream_identifiertList.hasNext() ) {
                    adaptor.addChild(root_1, stream_identifiertList.nextTree());

                }
                stream_identifiertList.reset();

                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:112:83: ( paramList )?
                if ( stream_paramList.hasNext() ) {
                    adaptor.addChild(root_1, stream_paramList.nextTree());

                }
                stream_paramList.reset();

                adaptor.addChild(root_1, stream_block.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "procedure"


    public static class procedureParameter_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "procedureParameter"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:115:1: procedureParameter : identifiertList_and_type ( ',' identifiertList_and_type )* -> ^( PROCEDUREPARAMETER ( identifiertList_and_type )+ ) ;
    public final BarsicParser.procedureParameter_return procedureParameter() throws RecognitionException {
        BarsicParser.procedureParameter_return retval = new BarsicParser.procedureParameter_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal38=null;
        BarsicParser.identifiertList_and_type_return identifiertList_and_type37 =null;

        BarsicParser.identifiertList_and_type_return identifiertList_and_type39 =null;


        Object char_literal38_tree=null;
        RewriteRuleTokenStream stream_109=new RewriteRuleTokenStream(adaptor,"token 109");
        RewriteRuleSubtreeStream stream_identifiertList_and_type=new RewriteRuleSubtreeStream(adaptor,"rule identifiertList_and_type");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:116:3: ( identifiertList_and_type ( ',' identifiertList_and_type )* -> ^( PROCEDUREPARAMETER ( identifiertList_and_type )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:116:5: identifiertList_and_type ( ',' identifiertList_and_type )*
            {
            pushFollow(FOLLOW_identifiertList_and_type_in_procedureParameter588);
            identifiertList_and_type37=identifiertList_and_type();

            state._fsp--;

            stream_identifiertList_and_type.add(identifiertList_and_type37.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:116:30: ( ',' identifiertList_and_type )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==109) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:116:31: ',' identifiertList_and_type
            	    {
            	    char_literal38=(Token)match(input,109,FOLLOW_109_in_procedureParameter591);  
            	    stream_109.add(char_literal38);


            	    pushFollow(FOLLOW_identifiertList_and_type_in_procedureParameter593);
            	    identifiertList_and_type39=identifiertList_and_type();

            	    state._fsp--;

            	    stream_identifiertList_and_type.add(identifiertList_and_type39.getTree());

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            // AST REWRITE
            // elements: identifiertList_and_type
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 116:62: -> ^( PROCEDUREPARAMETER ( identifiertList_and_type )+ )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:116:65: ^( PROCEDUREPARAMETER ( identifiertList_and_type )+ )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(PROCEDUREPARAMETER, "PROCEDUREPARAMETER")
                , root_1);

                if ( !(stream_identifiertList_and_type.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_identifiertList_and_type.hasNext() ) {
                    adaptor.addChild(root_1, stream_identifiertList_and_type.nextTree());

                }
                stream_identifiertList_and_type.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "procedureParameter"


    public static class identifiertList_and_type_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "identifiertList_and_type"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:119:1: identifiertList_and_type : ( identifiertList ':' Type ) -> ^( IDENTIFIERLIST_AND_TYPE identifiertList Type ) ;
    public final BarsicParser.identifiertList_and_type_return identifiertList_and_type() throws RecognitionException {
        BarsicParser.identifiertList_and_type_return retval = new BarsicParser.identifiertList_and_type_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal41=null;
        Token Type42=null;
        BarsicParser.identifiertList_return identifiertList40 =null;


        Object char_literal41_tree=null;
        Object Type42_tree=null;
        RewriteRuleTokenStream stream_Type=new RewriteRuleTokenStream(adaptor,"token Type");
        RewriteRuleTokenStream stream_113=new RewriteRuleTokenStream(adaptor,"token 113");
        RewriteRuleSubtreeStream stream_identifiertList=new RewriteRuleSubtreeStream(adaptor,"rule identifiertList");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:120:3: ( ( identifiertList ':' Type ) -> ^( IDENTIFIERLIST_AND_TYPE identifiertList Type ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:120:5: ( identifiertList ':' Type )
            {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:120:5: ( identifiertList ':' Type )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:120:6: identifiertList ':' Type
            {
            pushFollow(FOLLOW_identifiertList_in_identifiertList_and_type618);
            identifiertList40=identifiertList();

            state._fsp--;

            stream_identifiertList.add(identifiertList40.getTree());

            char_literal41=(Token)match(input,113,FOLLOW_113_in_identifiertList_and_type620);  
            stream_113.add(char_literal41);


            Type42=(Token)match(input,Type,FOLLOW_Type_in_identifiertList_and_type622);  
            stream_Type.add(Type42);


            }


            // AST REWRITE
            // elements: Type, identifiertList
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 120:32: -> ^( IDENTIFIERLIST_AND_TYPE identifiertList Type )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:120:35: ^( IDENTIFIERLIST_AND_TYPE identifiertList Type )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(IDENTIFIERLIST_AND_TYPE, "IDENTIFIERLIST_AND_TYPE")
                , root_1);

                adaptor.addChild(root_1, stream_identifiertList.nextTree());

                adaptor.addChild(root_1, 
                stream_Type.nextNode()
                );

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "identifiertList_and_type"


    public static class identifier_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "identifier"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:123:1: identifier : Identifier -> ^( IDENTIFIER Identifier ) ;
    public final BarsicParser.identifier_return identifier() throws RecognitionException {
        BarsicParser.identifier_return retval = new BarsicParser.identifier_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Identifier43=null;

        Object Identifier43_tree=null;
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:124:2: ( Identifier -> ^( IDENTIFIER Identifier ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:124:4: Identifier
            {
            Identifier43=(Token)match(input,Identifier,FOLLOW_Identifier_in_identifier647);  
            stream_Identifier.add(Identifier43);


            // AST REWRITE
            // elements: Identifier
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 124:15: -> ^( IDENTIFIER Identifier )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:124:18: ^( IDENTIFIER Identifier )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(IDENTIFIER, "IDENTIFIER")
                , root_1);

                adaptor.addChild(root_1, 
                stream_Identifier.nextNode()
                );

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "identifier"


    public static class identifiertList_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "identifiertList"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:127:1: identifiertList : Identifier ( ',' Identifier )* -> ^( IDENTIFIER_LIST ( Identifier )+ ) ;
    public final BarsicParser.identifiertList_return identifiertList() throws RecognitionException {
        BarsicParser.identifiertList_return retval = new BarsicParser.identifiertList_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Identifier44=null;
        Token char_literal45=null;
        Token Identifier46=null;

        Object Identifier44_tree=null;
        Object char_literal45_tree=null;
        Object Identifier46_tree=null;
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
        RewriteRuleTokenStream stream_109=new RewriteRuleTokenStream(adaptor,"token 109");

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:128:2: ( Identifier ( ',' Identifier )* -> ^( IDENTIFIER_LIST ( Identifier )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:128:4: Identifier ( ',' Identifier )*
            {
            Identifier44=(Token)match(input,Identifier,FOLLOW_Identifier_in_identifiertList668);  
            stream_Identifier.add(Identifier44);


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:128:16: ( ',' Identifier )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==109) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:128:17: ',' Identifier
            	    {
            	    char_literal45=(Token)match(input,109,FOLLOW_109_in_identifiertList672);  
            	    stream_109.add(char_literal45);


            	    Identifier46=(Token)match(input,Identifier,FOLLOW_Identifier_in_identifiertList673);  
            	    stream_Identifier.add(Identifier46);


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            // AST REWRITE
            // elements: Identifier
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 128:33: -> ^( IDENTIFIER_LIST ( Identifier )+ )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:128:36: ^( IDENTIFIER_LIST ( Identifier )+ )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(IDENTIFIER_LIST, "IDENTIFIER_LIST")
                , root_1);

                if ( !(stream_Identifier.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_Identifier.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_Identifier.nextNode()
                    );

                }
                stream_Identifier.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "identifiertList"


    public static class assignment_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "assignment"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:130:1: assignment : ( identifier paramList CloseIdentifier -> ^( ASSIGNMENT identifier paramList ) | lookuparr '=' expression -> ^( ASSIGNMENT lookuparr expression ) );
    public final BarsicParser.assignment_return assignment() throws RecognitionException {
        BarsicParser.assignment_return retval = new BarsicParser.assignment_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token CloseIdentifier49=null;
        Token char_literal51=null;
        BarsicParser.identifier_return identifier47 =null;

        BarsicParser.paramList_return paramList48 =null;

        BarsicParser.lookuparr_return lookuparr50 =null;

        BarsicParser.expression_return expression52 =null;


        Object CloseIdentifier49_tree=null;
        Object char_literal51_tree=null;
        RewriteRuleTokenStream stream_CloseIdentifier=new RewriteRuleTokenStream(adaptor,"token CloseIdentifier");
        RewriteRuleTokenStream stream_117=new RewriteRuleTokenStream(adaptor,"token 117");
        RewriteRuleSubtreeStream stream_identifier=new RewriteRuleSubtreeStream(adaptor,"rule identifier");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_lookuparr=new RewriteRuleSubtreeStream(adaptor,"rule lookuparr");
        RewriteRuleSubtreeStream stream_paramList=new RewriteRuleSubtreeStream(adaptor,"rule paramList");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:131:3: ( identifier paramList CloseIdentifier -> ^( ASSIGNMENT identifier paramList ) | lookuparr '=' expression -> ^( ASSIGNMENT lookuparr expression ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==Identifier) ) {
                int LA12_1 = input.LA(2);

                if ( (LA12_1==111||LA12_1==117||LA12_1==122) ) {
                    alt12=2;
                }
                else if ( (LA12_1==Array||LA12_1==Button||LA12_1==File||LA12_1==Form||LA12_1==Identifier||LA12_1==Panel||LA12_1==Subwindow||(LA12_1 >= TextField && LA12_1 <= TextLabel)) ) {
                    alt12=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 12, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA12_0==Array||LA12_0==Button||LA12_0==File||LA12_0==Form||LA12_0==Panel||LA12_0==Subwindow||(LA12_0 >= TextField && LA12_0 <= TextLabel)) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;

            }
            switch (alt12) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:131:6: identifier paramList CloseIdentifier
                    {
                    pushFollow(FOLLOW_identifier_in_assignment696);
                    identifier47=identifier();

                    state._fsp--;

                    stream_identifier.add(identifier47.getTree());

                    pushFollow(FOLLOW_paramList_in_assignment698);
                    paramList48=paramList();

                    state._fsp--;

                    stream_paramList.add(paramList48.getTree());

                    CloseIdentifier49=(Token)match(input,CloseIdentifier,FOLLOW_CloseIdentifier_in_assignment700);  
                    stream_CloseIdentifier.add(CloseIdentifier49);


                    // AST REWRITE
                    // elements: identifier, paramList
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 131:45: -> ^( ASSIGNMENT identifier paramList )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:131:48: ^( ASSIGNMENT identifier paramList )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(ASSIGNMENT, "ASSIGNMENT")
                        , root_1);

                        adaptor.addChild(root_1, stream_identifier.nextTree());

                        adaptor.addChild(root_1, stream_paramList.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:132:6: lookuparr '=' expression
                    {
                    pushFollow(FOLLOW_lookuparr_in_assignment720);
                    lookuparr50=lookuparr();

                    state._fsp--;

                    stream_lookuparr.add(lookuparr50.getTree());

                    char_literal51=(Token)match(input,117,FOLLOW_117_in_assignment722);  
                    stream_117.add(char_literal51);


                    pushFollow(FOLLOW_expression_in_assignment724);
                    expression52=expression();

                    state._fsp--;

                    stream_expression.add(expression52.getTree());

                    // AST REWRITE
                    // elements: lookuparr, expression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 132:31: -> ^( ASSIGNMENT lookuparr expression )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:132:34: ^( ASSIGNMENT lookuparr expression )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(ASSIGNMENT, "ASSIGNMENT")
                        , root_1);

                        adaptor.addChild(root_1, stream_lookuparr.nextTree());

                        adaptor.addChild(root_1, stream_expression.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "assignment"


    public static class paramList_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "paramList"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:135:1: paramList : ( assignment )+ ( ',' assignment )* -> ^( ASSIGNMENT_LIST ( assignment )+ ) ;
    public final BarsicParser.paramList_return paramList() throws RecognitionException {
        BarsicParser.paramList_return retval = new BarsicParser.paramList_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal54=null;
        BarsicParser.assignment_return assignment53 =null;

        BarsicParser.assignment_return assignment55 =null;


        Object char_literal54_tree=null;
        RewriteRuleTokenStream stream_109=new RewriteRuleTokenStream(adaptor,"token 109");
        RewriteRuleSubtreeStream stream_assignment=new RewriteRuleSubtreeStream(adaptor,"rule assignment");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:136:3: ( ( assignment )+ ( ',' assignment )* -> ^( ASSIGNMENT_LIST ( assignment )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:136:6: ( assignment )+ ( ',' assignment )*
            {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:136:6: ( assignment )+
            int cnt13=0;
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==Array||LA13_0==Button||LA13_0==File||LA13_0==Form||LA13_0==Identifier||LA13_0==Panel||LA13_0==Subwindow||(LA13_0 >= TextField && LA13_0 <= TextLabel)) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:136:6: assignment
            	    {
            	    pushFollow(FOLLOW_assignment_in_paramList748);
            	    assignment53=assignment();

            	    state._fsp--;

            	    stream_assignment.add(assignment53.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt13 >= 1 ) break loop13;
                        EarlyExitException eee =
                            new EarlyExitException(13, input);
                        throw eee;
                }
                cnt13++;
            } while (true);


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:136:18: ( ',' assignment )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==109) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:136:19: ',' assignment
            	    {
            	    char_literal54=(Token)match(input,109,FOLLOW_109_in_paramList752);  
            	    stream_109.add(char_literal54);


            	    pushFollow(FOLLOW_assignment_in_paramList754);
            	    assignment55=assignment();

            	    state._fsp--;

            	    stream_assignment.add(assignment55.getTree());

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);


            // AST REWRITE
            // elements: assignment
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 136:36: -> ^( ASSIGNMENT_LIST ( assignment )+ )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:136:39: ^( ASSIGNMENT_LIST ( assignment )+ )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(ASSIGNMENT_LIST, "ASSIGNMENT_LIST")
                , root_1);

                if ( !(stream_assignment.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_assignment.hasNext() ) {
                    adaptor.addChild(root_1, stream_assignment.nextTree());

                }
                stream_assignment.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "paramList"


    public static class ifStatement_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "ifStatement"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:139:1: ifStatement : ( ifStat ( elseStat )? EndIf ';' -> ^( IF1 ifStat ( elseStat )? ) | If ( ifsStat )+ ( elseStat )? EndIf ';' -> ^( IF2 ( ifsStat )+ ( elseStat )? ) );
    public final BarsicParser.ifStatement_return ifStatement() throws RecognitionException {
        BarsicParser.ifStatement_return retval = new BarsicParser.ifStatement_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token EndIf58=null;
        Token char_literal59=null;
        Token If60=null;
        Token EndIf63=null;
        Token char_literal64=null;
        BarsicParser.ifStat_return ifStat56 =null;

        BarsicParser.elseStat_return elseStat57 =null;

        BarsicParser.ifsStat_return ifsStat61 =null;

        BarsicParser.elseStat_return elseStat62 =null;


        Object EndIf58_tree=null;
        Object char_literal59_tree=null;
        Object If60_tree=null;
        Object EndIf63_tree=null;
        Object char_literal64_tree=null;
        RewriteRuleTokenStream stream_114=new RewriteRuleTokenStream(adaptor,"token 114");
        RewriteRuleTokenStream stream_EndIf=new RewriteRuleTokenStream(adaptor,"token EndIf");
        RewriteRuleTokenStream stream_If=new RewriteRuleTokenStream(adaptor,"token If");
        RewriteRuleSubtreeStream stream_ifStat=new RewriteRuleSubtreeStream(adaptor,"rule ifStat");
        RewriteRuleSubtreeStream stream_ifsStat=new RewriteRuleSubtreeStream(adaptor,"rule ifsStat");
        RewriteRuleSubtreeStream stream_elseStat=new RewriteRuleSubtreeStream(adaptor,"rule elseStat");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:140:3: ( ifStat ( elseStat )? EndIf ';' -> ^( IF1 ifStat ( elseStat )? ) | If ( ifsStat )+ ( elseStat )? EndIf ';' -> ^( IF2 ( ifsStat )+ ( elseStat )? ) )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==If) ) {
                int LA18_1 = input.LA(2);

                if ( (LA18_1==Array||(LA18_1 >= Bool && LA18_1 <= Button)||LA18_1==File||LA18_1==Form||LA18_1==Identifier||(LA18_1 >= New && LA18_1 <= OBrace)||LA18_1==Panel||(LA18_1 >= String && LA18_1 <= Subwindow)||(LA18_1 >= TextField && LA18_1 <= TextLabel)||LA18_1==101||LA18_1==110) ) {
                    alt18=1;
                }
                else if ( (LA18_1==127) ) {
                    alt18=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 18, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;

            }
            switch (alt18) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:140:5: ifStat ( elseStat )? EndIf ';'
                    {
                    pushFollow(FOLLOW_ifStat_in_ifStatement778);
                    ifStat56=ifStat();

                    state._fsp--;

                    stream_ifStat.add(ifStat56.getTree());

                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:140:12: ( elseStat )?
                    int alt15=2;
                    int LA15_0 = input.LA(1);

                    if ( (LA15_0==Else) ) {
                        alt15=1;
                    }
                    switch (alt15) {
                        case 1 :
                            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:140:12: elseStat
                            {
                            pushFollow(FOLLOW_elseStat_in_ifStatement780);
                            elseStat57=elseStat();

                            state._fsp--;

                            stream_elseStat.add(elseStat57.getTree());

                            }
                            break;

                    }


                    EndIf58=(Token)match(input,EndIf,FOLLOW_EndIf_in_ifStatement784);  
                    stream_EndIf.add(EndIf58);


                    char_literal59=(Token)match(input,114,FOLLOW_114_in_ifStatement786);  
                    stream_114.add(char_literal59);


                    // AST REWRITE
                    // elements: elseStat, ifStat
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 140:33: -> ^( IF1 ifStat ( elseStat )? )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:140:36: ^( IF1 ifStat ( elseStat )? )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(IF1, "IF1")
                        , root_1);

                        adaptor.addChild(root_1, stream_ifStat.nextTree());

                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:140:50: ( elseStat )?
                        if ( stream_elseStat.hasNext() ) {
                            adaptor.addChild(root_1, stream_elseStat.nextTree());

                        }
                        stream_elseStat.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:141:5: If ( ifsStat )+ ( elseStat )? EndIf ';'
                    {
                    If60=(Token)match(input,If,FOLLOW_If_in_ifStatement804);  
                    stream_If.add(If60);


                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:141:9: ( ifsStat )+
                    int cnt16=0;
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==127) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:141:9: ifsStat
                    	    {
                    	    pushFollow(FOLLOW_ifsStat_in_ifStatement807);
                    	    ifsStat61=ifsStat();

                    	    state._fsp--;

                    	    stream_ifsStat.add(ifsStat61.getTree());

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt16 >= 1 ) break loop16;
                                EarlyExitException eee =
                                    new EarlyExitException(16, input);
                                throw eee;
                        }
                        cnt16++;
                    } while (true);


                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:141:18: ( elseStat )?
                    int alt17=2;
                    int LA17_0 = input.LA(1);

                    if ( (LA17_0==Else) ) {
                        alt17=1;
                    }
                    switch (alt17) {
                        case 1 :
                            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:141:18: elseStat
                            {
                            pushFollow(FOLLOW_elseStat_in_ifStatement810);
                            elseStat62=elseStat();

                            state._fsp--;

                            stream_elseStat.add(elseStat62.getTree());

                            }
                            break;

                    }


                    EndIf63=(Token)match(input,EndIf,FOLLOW_EndIf_in_ifStatement814);  
                    stream_EndIf.add(EndIf63);


                    char_literal64=(Token)match(input,114,FOLLOW_114_in_ifStatement816);  
                    stream_114.add(char_literal64);


                    // AST REWRITE
                    // elements: elseStat, ifsStat
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 141:39: -> ^( IF2 ( ifsStat )+ ( elseStat )? )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:141:42: ^( IF2 ( ifsStat )+ ( elseStat )? )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(IF2, "IF2")
                        , root_1);

                        if ( !(stream_ifsStat.hasNext()) ) {
                            throw new RewriteEarlyExitException();
                        }
                        while ( stream_ifsStat.hasNext() ) {
                            adaptor.addChild(root_1, stream_ifsStat.nextTree());

                        }
                        stream_ifsStat.reset();

                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:141:58: ( elseStat )?
                        if ( stream_elseStat.hasNext() ) {
                            adaptor.addChild(root_1, stream_elseStat.nextTree());

                        }
                        stream_elseStat.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "ifStatement"


    public static class ifStat_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "ifStat"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:144:1: ifStat : If ifexpression Hence block -> ^( EXP ifexpression block ) ;
    public final BarsicParser.ifStat_return ifStat() throws RecognitionException {
        BarsicParser.ifStat_return retval = new BarsicParser.ifStat_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token If65=null;
        Token Hence67=null;
        BarsicParser.ifexpression_return ifexpression66 =null;

        BarsicParser.block_return block68 =null;


        Object If65_tree=null;
        Object Hence67_tree=null;
        RewriteRuleTokenStream stream_Hence=new RewriteRuleTokenStream(adaptor,"token Hence");
        RewriteRuleTokenStream stream_If=new RewriteRuleTokenStream(adaptor,"token If");
        RewriteRuleSubtreeStream stream_ifexpression=new RewriteRuleSubtreeStream(adaptor,"rule ifexpression");
        RewriteRuleSubtreeStream stream_block=new RewriteRuleSubtreeStream(adaptor,"rule block");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:145:3: ( If ifexpression Hence block -> ^( EXP ifexpression block ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:145:5: If ifexpression Hence block
            {
            If65=(Token)match(input,If,FOLLOW_If_in_ifStat842);  
            stream_If.add(If65);


            pushFollow(FOLLOW_ifexpression_in_ifStat844);
            ifexpression66=ifexpression();

            state._fsp--;

            stream_ifexpression.add(ifexpression66.getTree());

            Hence67=(Token)match(input,Hence,FOLLOW_Hence_in_ifStat846);  
            stream_Hence.add(Hence67);


            pushFollow(FOLLOW_block_in_ifStat848);
            block68=block();

            state._fsp--;

            stream_block.add(block68.getTree());

            // AST REWRITE
            // elements: ifexpression, block
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 145:33: -> ^( EXP ifexpression block )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:145:36: ^( EXP ifexpression block )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(EXP, "EXP")
                , root_1);

                adaptor.addChild(root_1, stream_ifexpression.nextTree());

                adaptor.addChild(root_1, stream_block.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "ifStat"


    public static class ifsStat_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "ifsStat"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:148:1: ifsStat : '|' ifexpression Hence block -> ^( EXP ifexpression block ) ;
    public final BarsicParser.ifsStat_return ifsStat() throws RecognitionException {
        BarsicParser.ifsStat_return retval = new BarsicParser.ifsStat_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal69=null;
        Token Hence71=null;
        BarsicParser.ifexpression_return ifexpression70 =null;

        BarsicParser.block_return block72 =null;


        Object char_literal69_tree=null;
        Object Hence71_tree=null;
        RewriteRuleTokenStream stream_Hence=new RewriteRuleTokenStream(adaptor,"token Hence");
        RewriteRuleTokenStream stream_127=new RewriteRuleTokenStream(adaptor,"token 127");
        RewriteRuleSubtreeStream stream_ifexpression=new RewriteRuleSubtreeStream(adaptor,"rule ifexpression");
        RewriteRuleSubtreeStream stream_block=new RewriteRuleSubtreeStream(adaptor,"rule block");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:149:3: ( '|' ifexpression Hence block -> ^( EXP ifexpression block ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:149:5: '|' ifexpression Hence block
            {
            char_literal69=(Token)match(input,127,FOLLOW_127_in_ifsStat871);  
            stream_127.add(char_literal69);


            pushFollow(FOLLOW_ifexpression_in_ifsStat873);
            ifexpression70=ifexpression();

            state._fsp--;

            stream_ifexpression.add(ifexpression70.getTree());

            Hence71=(Token)match(input,Hence,FOLLOW_Hence_in_ifsStat875);  
            stream_Hence.add(Hence71);


            pushFollow(FOLLOW_block_in_ifsStat877);
            block72=block();

            state._fsp--;

            stream_block.add(block72.getTree());

            // AST REWRITE
            // elements: block, ifexpression
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 149:34: -> ^( EXP ifexpression block )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:149:37: ^( EXP ifexpression block )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(EXP, "EXP")
                , root_1);

                adaptor.addChild(root_1, stream_ifexpression.nextTree());

                adaptor.addChild(root_1, stream_block.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "ifsStat"


    public static class elseStat_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "elseStat"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:152:1: elseStat : Else block -> ^( EXP block ) ;
    public final BarsicParser.elseStat_return elseStat() throws RecognitionException {
        BarsicParser.elseStat_return retval = new BarsicParser.elseStat_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Else73=null;
        BarsicParser.block_return block74 =null;


        Object Else73_tree=null;
        RewriteRuleTokenStream stream_Else=new RewriteRuleTokenStream(adaptor,"token Else");
        RewriteRuleSubtreeStream stream_block=new RewriteRuleSubtreeStream(adaptor,"rule block");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:153:3: ( Else block -> ^( EXP block ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:153:6: Else block
            {
            Else73=(Token)match(input,Else,FOLLOW_Else_in_elseStat901);  
            stream_Else.add(Else73);


            pushFollow(FOLLOW_block_in_elseStat903);
            block74=block();

            state._fsp--;

            stream_block.add(block74.getTree());

            // AST REWRITE
            // elements: block
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 153:17: -> ^( EXP block )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:153:20: ^( EXP block )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(EXP, "EXP")
                , root_1);

                adaptor.addChild(root_1, stream_block.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "elseStat"


    public static class ifexpression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "ifexpression"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:156:1: ifexpression : ( belongs | expression );
    public final BarsicParser.ifexpression_return ifexpression() throws RecognitionException {
        BarsicParser.ifexpression_return retval = new BarsicParser.ifexpression_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        BarsicParser.belongs_return belongs75 =null;

        BarsicParser.expression_return expression76 =null;



        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:157:3: ( belongs | expression )
            int alt19=2;
            alt19 = dfa19.predict(input);
            switch (alt19) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:157:5: belongs
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_belongs_in_ifexpression924);
                    belongs75=belongs();

                    state._fsp--;

                    adaptor.addChild(root_0, belongs75.getTree());

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:158:5: expression
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_expression_in_ifexpression930);
                    expression76=expression();

                    state._fsp--;

                    adaptor.addChild(root_0, expression76.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "ifexpression"


    public static class belongs_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "belongs"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:161:1: belongs : lookup In '[' sizeBelongs ']' -> ^( BELONGS lookup sizeBelongs ) ;
    public final BarsicParser.belongs_return belongs() throws RecognitionException {
        BarsicParser.belongs_return retval = new BarsicParser.belongs_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token In78=null;
        Token char_literal79=null;
        Token char_literal81=null;
        BarsicParser.lookup_return lookup77 =null;

        BarsicParser.sizeBelongs_return sizeBelongs80 =null;


        Object In78_tree=null;
        Object char_literal79_tree=null;
        Object char_literal81_tree=null;
        RewriteRuleTokenStream stream_122=new RewriteRuleTokenStream(adaptor,"token 122");
        RewriteRuleTokenStream stream_123=new RewriteRuleTokenStream(adaptor,"token 123");
        RewriteRuleTokenStream stream_In=new RewriteRuleTokenStream(adaptor,"token In");
        RewriteRuleSubtreeStream stream_lookup=new RewriteRuleSubtreeStream(adaptor,"rule lookup");
        RewriteRuleSubtreeStream stream_sizeBelongs=new RewriteRuleSubtreeStream(adaptor,"rule sizeBelongs");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:162:3: ( lookup In '[' sizeBelongs ']' -> ^( BELONGS lookup sizeBelongs ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:162:5: lookup In '[' sizeBelongs ']'
            {
            pushFollow(FOLLOW_lookup_in_belongs943);
            lookup77=lookup();

            state._fsp--;

            stream_lookup.add(lookup77.getTree());

            In78=(Token)match(input,In,FOLLOW_In_in_belongs945);  
            stream_In.add(In78);


            char_literal79=(Token)match(input,122,FOLLOW_122_in_belongs947);  
            stream_122.add(char_literal79);


            pushFollow(FOLLOW_sizeBelongs_in_belongs950);
            sizeBelongs80=sizeBelongs();

            state._fsp--;

            stream_sizeBelongs.add(sizeBelongs80.getTree());

            char_literal81=(Token)match(input,123,FOLLOW_123_in_belongs952);  
            stream_123.add(char_literal81);


            // AST REWRITE
            // elements: sizeBelongs, lookup
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 162:36: -> ^( BELONGS lookup sizeBelongs )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:162:40: ^( BELONGS lookup sizeBelongs )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(BELONGS, "BELONGS")
                , root_1);

                adaptor.addChild(root_1, stream_lookup.nextTree());

                adaptor.addChild(root_1, stream_sizeBelongs.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "belongs"


    public static class sizeBelongs_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "sizeBelongs"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:165:1: sizeBelongs : ( size | numList );
    public final BarsicParser.sizeBelongs_return sizeBelongs() throws RecognitionException {
        BarsicParser.sizeBelongs_return retval = new BarsicParser.sizeBelongs_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        BarsicParser.size_return size82 =null;

        BarsicParser.numList_return numList83 =null;



        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:166:3: ( size | numList )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==Number) ) {
                int LA20_1 = input.LA(2);

                if ( (LA20_1==To) ) {
                    alt20=1;
                }
                else if ( (LA20_1==109||LA20_1==123) ) {
                    alt20=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 20, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;

            }
            switch (alt20) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:166:6: size
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_size_in_sizeBelongs977);
                    size82=size();

                    state._fsp--;

                    adaptor.addChild(root_0, size82.getTree());

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:167:6: numList
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_numList_in_sizeBelongs984);
                    numList83=numList();

                    state._fsp--;

                    adaptor.addChild(root_0, numList83.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "sizeBelongs"


    public static class size_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "size"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:170:1: size : Number To Number -> ^( SIZE Number Number ) ;
    public final BarsicParser.size_return size() throws RecognitionException {
        BarsicParser.size_return retval = new BarsicParser.size_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Number84=null;
        Token To85=null;
        Token Number86=null;

        Object Number84_tree=null;
        Object To85_tree=null;
        Object Number86_tree=null;
        RewriteRuleTokenStream stream_Number=new RewriteRuleTokenStream(adaptor,"token Number");
        RewriteRuleTokenStream stream_To=new RewriteRuleTokenStream(adaptor,"token To");

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:171:2: ( Number To Number -> ^( SIZE Number Number ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:171:4: Number To Number
            {
            Number84=(Token)match(input,Number,FOLLOW_Number_in_size996);  
            stream_Number.add(Number84);


            To85=(Token)match(input,To,FOLLOW_To_in_size998);  
            stream_To.add(To85);


            Number86=(Token)match(input,Number,FOLLOW_Number_in_size1000);  
            stream_Number.add(Number86);


            // AST REWRITE
            // elements: Number, Number
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 171:21: -> ^( SIZE Number Number )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:171:24: ^( SIZE Number Number )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(SIZE, "SIZE")
                , root_1);

                adaptor.addChild(root_1, 
                stream_Number.nextNode()
                );

                adaptor.addChild(root_1, 
                stream_Number.nextNode()
                );

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "size"


    public static class numList_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "numList"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:174:1: numList : Number ( ',' Number )* -> ^( NUMLIST ( Number )+ ) ;
    public final BarsicParser.numList_return numList() throws RecognitionException {
        BarsicParser.numList_return retval = new BarsicParser.numList_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Number87=null;
        Token char_literal88=null;
        Token Number89=null;

        Object Number87_tree=null;
        Object char_literal88_tree=null;
        Object Number89_tree=null;
        RewriteRuleTokenStream stream_Number=new RewriteRuleTokenStream(adaptor,"token Number");
        RewriteRuleTokenStream stream_109=new RewriteRuleTokenStream(adaptor,"token 109");

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:175:2: ( Number ( ',' Number )* -> ^( NUMLIST ( Number )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:175:4: Number ( ',' Number )*
            {
            Number87=(Token)match(input,Number,FOLLOW_Number_in_numList1021);  
            stream_Number.add(Number87);


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:175:11: ( ',' Number )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==109) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:175:12: ',' Number
            	    {
            	    char_literal88=(Token)match(input,109,FOLLOW_109_in_numList1024);  
            	    stream_109.add(char_literal88);


            	    Number89=(Token)match(input,Number,FOLLOW_Number_in_numList1026);  
            	    stream_Number.add(Number89);


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            // AST REWRITE
            // elements: Number
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 175:25: -> ^( NUMLIST ( Number )+ )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:175:28: ^( NUMLIST ( Number )+ )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(NUMLIST, "NUMLIST")
                , root_1);

                if ( !(stream_Number.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_Number.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_Number.nextNode()
                    );

                }
                stream_Number.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "numList"


    public static class caseAssigment_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "caseAssigment"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:178:1: caseAssigment : lookup '=' caseStatement ';' -> ^( CASEASSIGMENT lookup caseStatement ) ;
    public final BarsicParser.caseAssigment_return caseAssigment() throws RecognitionException {
        BarsicParser.caseAssigment_return retval = new BarsicParser.caseAssigment_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal91=null;
        Token char_literal93=null;
        BarsicParser.lookup_return lookup90 =null;

        BarsicParser.caseStatement_return caseStatement92 =null;


        Object char_literal91_tree=null;
        Object char_literal93_tree=null;
        RewriteRuleTokenStream stream_114=new RewriteRuleTokenStream(adaptor,"token 114");
        RewriteRuleTokenStream stream_117=new RewriteRuleTokenStream(adaptor,"token 117");
        RewriteRuleSubtreeStream stream_lookup=new RewriteRuleSubtreeStream(adaptor,"rule lookup");
        RewriteRuleSubtreeStream stream_caseStatement=new RewriteRuleSubtreeStream(adaptor,"rule caseStatement");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:179:4: ( lookup '=' caseStatement ';' -> ^( CASEASSIGMENT lookup caseStatement ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:179:6: lookup '=' caseStatement ';'
            {
            pushFollow(FOLLOW_lookup_in_caseAssigment1050);
            lookup90=lookup();

            state._fsp--;

            stream_lookup.add(lookup90.getTree());

            char_literal91=(Token)match(input,117,FOLLOW_117_in_caseAssigment1052);  
            stream_117.add(char_literal91);


            pushFollow(FOLLOW_caseStatement_in_caseAssigment1054);
            caseStatement92=caseStatement();

            state._fsp--;

            stream_caseStatement.add(caseStatement92.getTree());

            char_literal93=(Token)match(input,114,FOLLOW_114_in_caseAssigment1056);  
            stream_114.add(char_literal93);


            // AST REWRITE
            // elements: lookup, caseStatement
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 179:35: -> ^( CASEASSIGMENT lookup caseStatement )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:179:38: ^( CASEASSIGMENT lookup caseStatement )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(CASEASSIGMENT, "CASEASSIGMENT")
                , root_1);

                adaptor.addChild(root_1, stream_lookup.nextTree());

                adaptor.addChild(root_1, stream_caseStatement.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "caseAssigment"


    public static class caseStatement_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "caseStatement"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:182:1: caseStatement : ( caseStat elsecaseStat EndCase -> ^( CASE1 caseStat elsecaseStat ) | Case ( casesStat )+ elsecaseStat EndCase -> ^( CASE2 ( casesStat )+ elsecaseStat ) );
    public final BarsicParser.caseStatement_return caseStatement() throws RecognitionException {
        BarsicParser.caseStatement_return retval = new BarsicParser.caseStatement_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token EndCase96=null;
        Token Case97=null;
        Token EndCase100=null;
        BarsicParser.caseStat_return caseStat94 =null;

        BarsicParser.elsecaseStat_return elsecaseStat95 =null;

        BarsicParser.casesStat_return casesStat98 =null;

        BarsicParser.elsecaseStat_return elsecaseStat99 =null;


        Object EndCase96_tree=null;
        Object Case97_tree=null;
        Object EndCase100_tree=null;
        RewriteRuleTokenStream stream_EndCase=new RewriteRuleTokenStream(adaptor,"token EndCase");
        RewriteRuleTokenStream stream_Case=new RewriteRuleTokenStream(adaptor,"token Case");
        RewriteRuleSubtreeStream stream_caseStat=new RewriteRuleSubtreeStream(adaptor,"rule caseStat");
        RewriteRuleSubtreeStream stream_elsecaseStat=new RewriteRuleSubtreeStream(adaptor,"rule elsecaseStat");
        RewriteRuleSubtreeStream stream_casesStat=new RewriteRuleSubtreeStream(adaptor,"rule casesStat");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:183:3: ( caseStat elsecaseStat EndCase -> ^( CASE1 caseStat elsecaseStat ) | Case ( casesStat )+ elsecaseStat EndCase -> ^( CASE2 ( casesStat )+ elsecaseStat ) )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==Case) ) {
                int LA23_1 = input.LA(2);

                if ( (LA23_1==Array||(LA23_1 >= Bool && LA23_1 <= Button)||LA23_1==File||LA23_1==Form||LA23_1==Identifier||(LA23_1 >= New && LA23_1 <= OBrace)||LA23_1==Panel||(LA23_1 >= String && LA23_1 <= Subwindow)||(LA23_1 >= TextField && LA23_1 <= TextLabel)||LA23_1==101||LA23_1==110) ) {
                    alt23=1;
                }
                else if ( (LA23_1==127) ) {
                    alt23=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 23, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;

            }
            switch (alt23) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:183:5: caseStat elsecaseStat EndCase
                    {
                    pushFollow(FOLLOW_caseStat_in_caseStatement1082);
                    caseStat94=caseStat();

                    state._fsp--;

                    stream_caseStat.add(caseStat94.getTree());

                    pushFollow(FOLLOW_elsecaseStat_in_caseStatement1084);
                    elsecaseStat95=elsecaseStat();

                    state._fsp--;

                    stream_elsecaseStat.add(elsecaseStat95.getTree());

                    EndCase96=(Token)match(input,EndCase,FOLLOW_EndCase_in_caseStatement1087);  
                    stream_EndCase.add(EndCase96);


                    // AST REWRITE
                    // elements: caseStat, elsecaseStat
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 183:36: -> ^( CASE1 caseStat elsecaseStat )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:183:39: ^( CASE1 caseStat elsecaseStat )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(CASE1, "CASE1")
                        , root_1);

                        adaptor.addChild(root_1, stream_caseStat.nextTree());

                        adaptor.addChild(root_1, stream_elsecaseStat.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:184:5: Case ( casesStat )+ elsecaseStat EndCase
                    {
                    Case97=(Token)match(input,Case,FOLLOW_Case_in_caseStatement1104);  
                    stream_Case.add(Case97);


                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:184:11: ( casesStat )+
                    int cnt22=0;
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( (LA22_0==127) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:184:11: casesStat
                    	    {
                    	    pushFollow(FOLLOW_casesStat_in_caseStatement1107);
                    	    casesStat98=casesStat();

                    	    state._fsp--;

                    	    stream_casesStat.add(casesStat98.getTree());

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt22 >= 1 ) break loop22;
                                EarlyExitException eee =
                                    new EarlyExitException(22, input);
                                throw eee;
                        }
                        cnt22++;
                    } while (true);


                    pushFollow(FOLLOW_elsecaseStat_in_caseStatement1110);
                    elsecaseStat99=elsecaseStat();

                    state._fsp--;

                    stream_elsecaseStat.add(elsecaseStat99.getTree());

                    EndCase100=(Token)match(input,EndCase,FOLLOW_EndCase_in_caseStatement1113);  
                    stream_EndCase.add(EndCase100);


                    // AST REWRITE
                    // elements: casesStat, elsecaseStat
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 184:45: -> ^( CASE2 ( casesStat )+ elsecaseStat )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:184:48: ^( CASE2 ( casesStat )+ elsecaseStat )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(CASE2, "CASE2")
                        , root_1);

                        if ( !(stream_casesStat.hasNext()) ) {
                            throw new RewriteEarlyExitException();
                        }
                        while ( stream_casesStat.hasNext() ) {
                            adaptor.addChild(root_1, stream_casesStat.nextTree());

                        }
                        stream_casesStat.reset();

                        adaptor.addChild(root_1, stream_elsecaseStat.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "caseStatement"


    public static class caseStat_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "caseStat"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:187:1: caseStat : Case expression Hence caseStatOrExpr -> ^( CASEEXP expression caseStatOrExpr ) ;
    public final BarsicParser.caseStat_return caseStat() throws RecognitionException {
        BarsicParser.caseStat_return retval = new BarsicParser.caseStat_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Case101=null;
        Token Hence103=null;
        BarsicParser.expression_return expression102 =null;

        BarsicParser.caseStatOrExpr_return caseStatOrExpr104 =null;


        Object Case101_tree=null;
        Object Hence103_tree=null;
        RewriteRuleTokenStream stream_Hence=new RewriteRuleTokenStream(adaptor,"token Hence");
        RewriteRuleTokenStream stream_Case=new RewriteRuleTokenStream(adaptor,"token Case");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_caseStatOrExpr=new RewriteRuleSubtreeStream(adaptor,"rule caseStatOrExpr");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:188:3: ( Case expression Hence caseStatOrExpr -> ^( CASEEXP expression caseStatOrExpr ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:188:5: Case expression Hence caseStatOrExpr
            {
            Case101=(Token)match(input,Case,FOLLOW_Case_in_caseStat1139);  
            stream_Case.add(Case101);


            pushFollow(FOLLOW_expression_in_caseStat1141);
            expression102=expression();

            state._fsp--;

            stream_expression.add(expression102.getTree());

            Hence103=(Token)match(input,Hence,FOLLOW_Hence_in_caseStat1143);  
            stream_Hence.add(Hence103);


            pushFollow(FOLLOW_caseStatOrExpr_in_caseStat1145);
            caseStatOrExpr104=caseStatOrExpr();

            state._fsp--;

            stream_caseStatOrExpr.add(caseStatOrExpr104.getTree());

            // AST REWRITE
            // elements: expression, caseStatOrExpr
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 188:42: -> ^( CASEEXP expression caseStatOrExpr )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:188:45: ^( CASEEXP expression caseStatOrExpr )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(CASEEXP, "CASEEXP")
                , root_1);

                adaptor.addChild(root_1, stream_expression.nextTree());

                adaptor.addChild(root_1, stream_caseStatOrExpr.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "caseStat"


    public static class elsecaseStat_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "elsecaseStat"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:191:1: elsecaseStat : Else expression -> ^( CASEEXPELSE expression ) ;
    public final BarsicParser.elsecaseStat_return elsecaseStat() throws RecognitionException {
        BarsicParser.elsecaseStat_return retval = new BarsicParser.elsecaseStat_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Else105=null;
        BarsicParser.expression_return expression106 =null;


        Object Else105_tree=null;
        RewriteRuleTokenStream stream_Else=new RewriteRuleTokenStream(adaptor,"token Else");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:192:3: ( Else expression -> ^( CASEEXPELSE expression ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:192:6: Else expression
            {
            Else105=(Token)match(input,Else,FOLLOW_Else_in_elsecaseStat1169);  
            stream_Else.add(Else105);


            pushFollow(FOLLOW_expression_in_elsecaseStat1171);
            expression106=expression();

            state._fsp--;

            stream_expression.add(expression106.getTree());

            // AST REWRITE
            // elements: expression
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 192:22: -> ^( CASEEXPELSE expression )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:192:25: ^( CASEEXPELSE expression )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(CASEEXPELSE, "CASEEXPELSE")
                , root_1);

                adaptor.addChild(root_1, stream_expression.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "elsecaseStat"


    public static class casesStat_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "casesStat"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:195:1: casesStat : '|' expression Hence caseStatOrExpr -> ^( CASEEXP expression caseStatOrExpr ) ;
    public final BarsicParser.casesStat_return casesStat() throws RecognitionException {
        BarsicParser.casesStat_return retval = new BarsicParser.casesStat_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal107=null;
        Token Hence109=null;
        BarsicParser.expression_return expression108 =null;

        BarsicParser.caseStatOrExpr_return caseStatOrExpr110 =null;


        Object char_literal107_tree=null;
        Object Hence109_tree=null;
        RewriteRuleTokenStream stream_Hence=new RewriteRuleTokenStream(adaptor,"token Hence");
        RewriteRuleTokenStream stream_127=new RewriteRuleTokenStream(adaptor,"token 127");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_caseStatOrExpr=new RewriteRuleSubtreeStream(adaptor,"rule caseStatOrExpr");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:196:3: ( '|' expression Hence caseStatOrExpr -> ^( CASEEXP expression caseStatOrExpr ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:196:5: '|' expression Hence caseStatOrExpr
            {
            char_literal107=(Token)match(input,127,FOLLOW_127_in_casesStat1192);  
            stream_127.add(char_literal107);


            pushFollow(FOLLOW_expression_in_casesStat1194);
            expression108=expression();

            state._fsp--;

            stream_expression.add(expression108.getTree());

            Hence109=(Token)match(input,Hence,FOLLOW_Hence_in_casesStat1196);  
            stream_Hence.add(Hence109);


            pushFollow(FOLLOW_caseStatOrExpr_in_casesStat1198);
            caseStatOrExpr110=caseStatOrExpr();

            state._fsp--;

            stream_caseStatOrExpr.add(caseStatOrExpr110.getTree());

            // AST REWRITE
            // elements: expression, caseStatOrExpr
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 196:41: -> ^( CASEEXP expression caseStatOrExpr )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:196:44: ^( CASEEXP expression caseStatOrExpr )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(CASEEXP, "CASEEXP")
                , root_1);

                adaptor.addChild(root_1, stream_expression.nextTree());

                adaptor.addChild(root_1, stream_caseStatOrExpr.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "casesStat"


    public static class caseStatOrExpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "caseStatOrExpr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:199:1: caseStatOrExpr : ( caseStatement | expression );
    public final BarsicParser.caseStatOrExpr_return caseStatOrExpr() throws RecognitionException {
        BarsicParser.caseStatOrExpr_return retval = new BarsicParser.caseStatOrExpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        BarsicParser.caseStatement_return caseStatement111 =null;

        BarsicParser.expression_return expression112 =null;



        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:200:3: ( caseStatement | expression )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==Case) ) {
                alt24=1;
            }
            else if ( (LA24_0==Array||(LA24_0 >= Bool && LA24_0 <= Button)||LA24_0==File||LA24_0==Form||LA24_0==Identifier||(LA24_0 >= New && LA24_0 <= OBrace)||LA24_0==Panel||(LA24_0 >= String && LA24_0 <= Subwindow)||(LA24_0 >= TextField && LA24_0 <= TextLabel)||LA24_0==101||LA24_0==110) ) {
                alt24=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;

            }
            switch (alt24) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:200:6: caseStatement
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_caseStatement_in_caseStatOrExpr1222);
                    caseStatement111=caseStatement();

                    state._fsp--;

                    adaptor.addChild(root_0, caseStatement111.getTree());

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:201:6: expression
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_expression_in_caseStatOrExpr1229);
                    expression112=expression();

                    state._fsp--;

                    adaptor.addChild(root_0, expression112.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "caseStatOrExpr"


    public static class forStatement_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "forStatement"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:204:1: forStatement : For Identifier '=' expression To expression ( ',' Step '=' expression )? Do block EndDo ';' -> ^( For Identifier expression expression ( expression )? block ) ;
    public final BarsicParser.forStatement_return forStatement() throws RecognitionException {
        BarsicParser.forStatement_return retval = new BarsicParser.forStatement_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token For113=null;
        Token Identifier114=null;
        Token char_literal115=null;
        Token To117=null;
        Token char_literal119=null;
        Token Step120=null;
        Token char_literal121=null;
        Token Do123=null;
        Token EndDo125=null;
        Token char_literal126=null;
        BarsicParser.expression_return expression116 =null;

        BarsicParser.expression_return expression118 =null;

        BarsicParser.expression_return expression122 =null;

        BarsicParser.block_return block124 =null;


        Object For113_tree=null;
        Object Identifier114_tree=null;
        Object char_literal115_tree=null;
        Object To117_tree=null;
        Object char_literal119_tree=null;
        Object Step120_tree=null;
        Object char_literal121_tree=null;
        Object Do123_tree=null;
        Object EndDo125_tree=null;
        Object char_literal126_tree=null;
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
        RewriteRuleTokenStream stream_114=new RewriteRuleTokenStream(adaptor,"token 114");
        RewriteRuleTokenStream stream_117=new RewriteRuleTokenStream(adaptor,"token 117");
        RewriteRuleTokenStream stream_For=new RewriteRuleTokenStream(adaptor,"token For");
        RewriteRuleTokenStream stream_EndDo=new RewriteRuleTokenStream(adaptor,"token EndDo");
        RewriteRuleTokenStream stream_109=new RewriteRuleTokenStream(adaptor,"token 109");
        RewriteRuleTokenStream stream_To=new RewriteRuleTokenStream(adaptor,"token To");
        RewriteRuleTokenStream stream_Step=new RewriteRuleTokenStream(adaptor,"token Step");
        RewriteRuleTokenStream stream_Do=new RewriteRuleTokenStream(adaptor,"token Do");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_block=new RewriteRuleSubtreeStream(adaptor,"rule block");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:205:3: ( For Identifier '=' expression To expression ( ',' Step '=' expression )? Do block EndDo ';' -> ^( For Identifier expression expression ( expression )? block ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:205:6: For Identifier '=' expression To expression ( ',' Step '=' expression )? Do block EndDo ';'
            {
            For113=(Token)match(input,For,FOLLOW_For_in_forStatement1243);  
            stream_For.add(For113);


            Identifier114=(Token)match(input,Identifier,FOLLOW_Identifier_in_forStatement1245);  
            stream_Identifier.add(Identifier114);


            char_literal115=(Token)match(input,117,FOLLOW_117_in_forStatement1247);  
            stream_117.add(char_literal115);


            pushFollow(FOLLOW_expression_in_forStatement1249);
            expression116=expression();

            state._fsp--;

            stream_expression.add(expression116.getTree());

            To117=(Token)match(input,To,FOLLOW_To_in_forStatement1251);  
            stream_To.add(To117);


            pushFollow(FOLLOW_expression_in_forStatement1253);
            expression118=expression();

            state._fsp--;

            stream_expression.add(expression118.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:205:50: ( ',' Step '=' expression )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==109) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:205:51: ',' Step '=' expression
                    {
                    char_literal119=(Token)match(input,109,FOLLOW_109_in_forStatement1256);  
                    stream_109.add(char_literal119);


                    Step120=(Token)match(input,Step,FOLLOW_Step_in_forStatement1258);  
                    stream_Step.add(Step120);


                    char_literal121=(Token)match(input,117,FOLLOW_117_in_forStatement1260);  
                    stream_117.add(char_literal121);


                    pushFollow(FOLLOW_expression_in_forStatement1262);
                    expression122=expression();

                    state._fsp--;

                    stream_expression.add(expression122.getTree());

                    }
                    break;

            }


            Do123=(Token)match(input,Do,FOLLOW_Do_in_forStatement1266);  
            stream_Do.add(Do123);


            pushFollow(FOLLOW_block_in_forStatement1268);
            block124=block();

            state._fsp--;

            stream_block.add(block124.getTree());

            EndDo125=(Token)match(input,EndDo,FOLLOW_EndDo_in_forStatement1270);  
            stream_EndDo.add(EndDo125);


            char_literal126=(Token)match(input,114,FOLLOW_114_in_forStatement1271);  
            stream_114.add(char_literal126);


            // AST REWRITE
            // elements: expression, expression, Identifier, expression, For, block
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 206:6: -> ^( For Identifier expression expression ( expression )? block )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:206:9: ^( For Identifier expression expression ( expression )? block )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                stream_For.nextNode()
                , root_1);

                adaptor.addChild(root_1, 
                stream_Identifier.nextNode()
                );

                adaptor.addChild(root_1, stream_expression.nextTree());

                adaptor.addChild(root_1, stream_expression.nextTree());

                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:206:48: ( expression )?
                if ( stream_expression.hasNext() ) {
                    adaptor.addChild(root_1, stream_expression.nextTree());

                }
                stream_expression.reset();

                adaptor.addChild(root_1, stream_block.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "forStatement"


    public static class whileStatement_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "whileStatement"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:209:1: whileStatement : While expression Do block EndDo ';' -> ^( While expression block ) ;
    public final BarsicParser.whileStatement_return whileStatement() throws RecognitionException {
        BarsicParser.whileStatement_return retval = new BarsicParser.whileStatement_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token While127=null;
        Token Do129=null;
        Token EndDo131=null;
        Token char_literal132=null;
        BarsicParser.expression_return expression128 =null;

        BarsicParser.block_return block130 =null;


        Object While127_tree=null;
        Object Do129_tree=null;
        Object EndDo131_tree=null;
        Object char_literal132_tree=null;
        RewriteRuleTokenStream stream_114=new RewriteRuleTokenStream(adaptor,"token 114");
        RewriteRuleTokenStream stream_EndDo=new RewriteRuleTokenStream(adaptor,"token EndDo");
        RewriteRuleTokenStream stream_While=new RewriteRuleTokenStream(adaptor,"token While");
        RewriteRuleTokenStream stream_Do=new RewriteRuleTokenStream(adaptor,"token Do");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_block=new RewriteRuleSubtreeStream(adaptor,"rule block");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:210:3: ( While expression Do block EndDo ';' -> ^( While expression block ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:210:6: While expression Do block EndDo ';'
            {
            While127=(Token)match(input,While,FOLLOW_While_in_whileStatement1307);  
            stream_While.add(While127);


            pushFollow(FOLLOW_expression_in_whileStatement1309);
            expression128=expression();

            state._fsp--;

            stream_expression.add(expression128.getTree());

            Do129=(Token)match(input,Do,FOLLOW_Do_in_whileStatement1311);  
            stream_Do.add(Do129);


            pushFollow(FOLLOW_block_in_whileStatement1313);
            block130=block();

            state._fsp--;

            stream_block.add(block130.getTree());

            EndDo131=(Token)match(input,EndDo,FOLLOW_EndDo_in_whileStatement1315);  
            stream_EndDo.add(EndDo131);


            char_literal132=(Token)match(input,114,FOLLOW_114_in_whileStatement1317);  
            stream_114.add(char_literal132);


            // AST REWRITE
            // elements: While, expression, block
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 210:43: -> ^( While expression block )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:210:46: ^( While expression block )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                stream_While.nextNode()
                , root_1);

                adaptor.addChild(root_1, stream_expression.nextTree());

                adaptor.addChild(root_1, stream_block.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "whileStatement"


    public static class expression_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expression"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:214:1: expression : ( condExpr | New 'Form' paramList CloseIdentifier -> ^( CTOR 'Form' paramList ) | OBrace block CBrace -> ^( FUNC_DEF block ) | array -> ^( EXPARRAY array ) );
    public final BarsicParser.expression_return expression() throws RecognitionException {
        BarsicParser.expression_return retval = new BarsicParser.expression_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token New134=null;
        Token string_literal135=null;
        Token CloseIdentifier137=null;
        Token OBrace138=null;
        Token CBrace140=null;
        BarsicParser.condExpr_return condExpr133 =null;

        BarsicParser.paramList_return paramList136 =null;

        BarsicParser.block_return block139 =null;

        BarsicParser.array_return array141 =null;


        Object New134_tree=null;
        Object string_literal135_tree=null;
        Object CloseIdentifier137_tree=null;
        Object OBrace138_tree=null;
        Object CBrace140_tree=null;
        RewriteRuleTokenStream stream_New=new RewriteRuleTokenStream(adaptor,"token New");
        RewriteRuleTokenStream stream_CloseIdentifier=new RewriteRuleTokenStream(adaptor,"token CloseIdentifier");
        RewriteRuleTokenStream stream_OBrace=new RewriteRuleTokenStream(adaptor,"token OBrace");
        RewriteRuleTokenStream stream_Form=new RewriteRuleTokenStream(adaptor,"token Form");
        RewriteRuleTokenStream stream_CBrace=new RewriteRuleTokenStream(adaptor,"token CBrace");
        RewriteRuleSubtreeStream stream_array=new RewriteRuleSubtreeStream(adaptor,"rule array");
        RewriteRuleSubtreeStream stream_paramList=new RewriteRuleSubtreeStream(adaptor,"rule paramList");
        RewriteRuleSubtreeStream stream_block=new RewriteRuleSubtreeStream(adaptor,"rule block");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:215:3: ( condExpr | New 'Form' paramList CloseIdentifier -> ^( CTOR 'Form' paramList ) | OBrace block CBrace -> ^( FUNC_DEF block ) | array -> ^( EXPARRAY array ) )
            int alt26=4;
            switch ( input.LA(1) ) {
            case Array:
            case Bool:
            case Button:
            case File:
            case Form:
            case Identifier:
            case Null:
            case Number:
            case Panel:
            case String:
            case Subwindow:
            case TextField:
            case TextLabel:
            case 101:
            case 110:
                {
                alt26=1;
                }
                break;
            case New:
                {
                int LA26_2 = input.LA(2);

                if ( (LA26_2==Form) ) {
                    alt26=2;
                }
                else if ( (LA26_2==Array) ) {
                    alt26=4;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 26, 2, input);

                    throw nvae;

                }
                }
                break;
            case OBrace:
                {
                alt26=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;

            }

            switch (alt26) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:215:5: condExpr
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_condExpr_in_expression1343);
                    condExpr133=condExpr();

                    state._fsp--;

                    adaptor.addChild(root_0, condExpr133.getTree());

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:216:5: New 'Form' paramList CloseIdentifier
                    {
                    New134=(Token)match(input,New,FOLLOW_New_in_expression1349);  
                    stream_New.add(New134);


                    string_literal135=(Token)match(input,Form,FOLLOW_Form_in_expression1351);  
                    stream_Form.add(string_literal135);


                    pushFollow(FOLLOW_paramList_in_expression1353);
                    paramList136=paramList();

                    state._fsp--;

                    stream_paramList.add(paramList136.getTree());

                    CloseIdentifier137=(Token)match(input,CloseIdentifier,FOLLOW_CloseIdentifier_in_expression1355);  
                    stream_CloseIdentifier.add(CloseIdentifier137);


                    // AST REWRITE
                    // elements: paramList, Form
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 216:42: -> ^( CTOR 'Form' paramList )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:216:45: ^( CTOR 'Form' paramList )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(CTOR, "CTOR")
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_Form.nextNode()
                        );

                        adaptor.addChild(root_1, stream_paramList.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:217:5: OBrace block CBrace
                    {
                    OBrace138=(Token)match(input,OBrace,FOLLOW_OBrace_in_expression1371);  
                    stream_OBrace.add(OBrace138);


                    pushFollow(FOLLOW_block_in_expression1373);
                    block139=block();

                    state._fsp--;

                    stream_block.add(block139.getTree());

                    CBrace140=(Token)match(input,CBrace,FOLLOW_CBrace_in_expression1375);  
                    stream_CBrace.add(CBrace140);


                    // AST REWRITE
                    // elements: block
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 217:25: -> ^( FUNC_DEF block )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:217:28: ^( FUNC_DEF block )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(FUNC_DEF, "FUNC_DEF")
                        , root_1);

                        adaptor.addChild(root_1, stream_block.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 4 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:218:5: array
                    {
                    pushFollow(FOLLOW_array_in_expression1389);
                    array141=array();

                    state._fsp--;

                    stream_array.add(array141.getTree());

                    // AST REWRITE
                    // elements: array
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 218:11: -> ^( EXPARRAY array )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:218:14: ^( EXPARRAY array )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(EXPARRAY, "EXPARRAY")
                        , root_1);

                        adaptor.addChild(root_1, stream_array.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expression"


    public static class exprList_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "exprList"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:221:1: exprList : expression ( ',' expression )* -> ^( EXP_LIST ( expression )+ ) ;
    public final BarsicParser.exprList_return exprList() throws RecognitionException {
        BarsicParser.exprList_return retval = new BarsicParser.exprList_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal143=null;
        BarsicParser.expression_return expression142 =null;

        BarsicParser.expression_return expression144 =null;


        Object char_literal143_tree=null;
        RewriteRuleTokenStream stream_109=new RewriteRuleTokenStream(adaptor,"token 109");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:222:3: ( expression ( ',' expression )* -> ^( EXP_LIST ( expression )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:222:6: expression ( ',' expression )*
            {
            pushFollow(FOLLOW_expression_in_exprList1411);
            expression142=expression();

            state._fsp--;

            stream_expression.add(expression142.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:222:17: ( ',' expression )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==109) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:222:18: ',' expression
            	    {
            	    char_literal143=(Token)match(input,109,FOLLOW_109_in_exprList1414);  
            	    stream_109.add(char_literal143);


            	    pushFollow(FOLLOW_expression_in_exprList1416);
            	    expression144=expression();

            	    state._fsp--;

            	    stream_expression.add(expression144.getTree());

            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);


            // AST REWRITE
            // elements: expression
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 222:35: -> ^( EXP_LIST ( expression )+ )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:222:38: ^( EXP_LIST ( expression )+ )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(EXP_LIST, "EXP_LIST")
                , root_1);

                if ( !(stream_expression.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_expression.hasNext() ) {
                    adaptor.addChild(root_1, stream_expression.nextTree());

                }
                stream_expression.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "exprList"


    public static class condExpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "condExpr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:225:1: condExpr : ( orExpr -> orExpr ) ( '?' a= expression ':' b= expression -> ^( TERNARY orExpr $a $b) )? ;
    public final BarsicParser.condExpr_return condExpr() throws RecognitionException {
        BarsicParser.condExpr_return retval = new BarsicParser.condExpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal146=null;
        Token char_literal147=null;
        BarsicParser.expression_return a =null;

        BarsicParser.expression_return b =null;

        BarsicParser.orExpr_return orExpr145 =null;


        Object char_literal146_tree=null;
        Object char_literal147_tree=null;
        RewriteRuleTokenStream stream_121=new RewriteRuleTokenStream(adaptor,"token 121");
        RewriteRuleTokenStream stream_113=new RewriteRuleTokenStream(adaptor,"token 113");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_orExpr=new RewriteRuleSubtreeStream(adaptor,"rule orExpr");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:226:3: ( ( orExpr -> orExpr ) ( '?' a= expression ':' b= expression -> ^( TERNARY orExpr $a $b) )? )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:226:6: ( orExpr -> orExpr ) ( '?' a= expression ':' b= expression -> ^( TERNARY orExpr $a $b) )?
            {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:226:6: ( orExpr -> orExpr )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:226:7: orExpr
            {
            pushFollow(FOLLOW_orExpr_in_condExpr1442);
            orExpr145=orExpr();

            state._fsp--;

            stream_orExpr.add(orExpr145.getTree());

            // AST REWRITE
            // elements: orExpr
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 226:14: -> orExpr
            {
                adaptor.addChild(root_0, stream_orExpr.nextTree());

            }


            retval.tree = root_0;

            }


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:227:6: ( '?' a= expression ':' b= expression -> ^( TERNARY orExpr $a $b) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==121) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:227:8: '?' a= expression ':' b= expression
                    {
                    char_literal146=(Token)match(input,121,FOLLOW_121_in_condExpr1456);  
                    stream_121.add(char_literal146);


                    pushFollow(FOLLOW_expression_in_condExpr1460);
                    a=expression();

                    state._fsp--;

                    stream_expression.add(a.getTree());

                    char_literal147=(Token)match(input,113,FOLLOW_113_in_condExpr1462);  
                    stream_113.add(char_literal147);


                    pushFollow(FOLLOW_expression_in_condExpr1466);
                    b=expression();

                    state._fsp--;

                    stream_expression.add(b.getTree());

                    // AST REWRITE
                    // elements: orExpr, a, b
                    // token labels: 
                    // rule labels: a, b, retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_b=new RewriteRuleSubtreeStream(adaptor,"rule b",b!=null?b.tree:null);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 227:42: -> ^( TERNARY orExpr $a $b)
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:227:45: ^( TERNARY orExpr $a $b)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(TERNARY, "TERNARY")
                        , root_1);

                        adaptor.addChild(root_1, stream_orExpr.nextTree());

                        adaptor.addChild(root_1, stream_a.nextTree());

                        adaptor.addChild(root_1, stream_b.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "condExpr"


    public static class orExpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "orExpr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:230:1: orExpr : andExpr ( '||' ^ andExpr )* ;
    public final BarsicParser.orExpr_return orExpr() throws RecognitionException {
        BarsicParser.orExpr_return retval = new BarsicParser.orExpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token string_literal149=null;
        BarsicParser.andExpr_return andExpr148 =null;

        BarsicParser.andExpr_return andExpr150 =null;


        Object string_literal149_tree=null;

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:231:3: ( andExpr ( '||' ^ andExpr )* )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:231:6: andExpr ( '||' ^ andExpr )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_andExpr_in_orExpr1496);
            andExpr148=andExpr();

            state._fsp--;

            adaptor.addChild(root_0, andExpr148.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:231:14: ( '||' ^ andExpr )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==128) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:231:15: '||' ^ andExpr
            	    {
            	    string_literal149=(Token)match(input,128,FOLLOW_128_in_orExpr1499); 
            	    string_literal149_tree = 
            	    (Object)adaptor.create(string_literal149)
            	    ;
            	    root_0 = (Object)adaptor.becomeRoot(string_literal149_tree, root_0);


            	    pushFollow(FOLLOW_andExpr_in_orExpr1502);
            	    andExpr150=andExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, andExpr150.getTree());

            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "orExpr"


    public static class andExpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "andExpr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:234:1: andExpr : equExpr ( '&&' ^ equExpr )* ;
    public final BarsicParser.andExpr_return andExpr() throws RecognitionException {
        BarsicParser.andExpr_return retval = new BarsicParser.andExpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token string_literal152=null;
        BarsicParser.equExpr_return equExpr151 =null;

        BarsicParser.equExpr_return equExpr153 =null;


        Object string_literal152_tree=null;

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:235:3: ( equExpr ( '&&' ^ equExpr )* )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:235:6: equExpr ( '&&' ^ equExpr )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_equExpr_in_andExpr1518);
            equExpr151=equExpr();

            state._fsp--;

            adaptor.addChild(root_0, equExpr151.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:235:14: ( '&&' ^ equExpr )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==104) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:235:15: '&&' ^ equExpr
            	    {
            	    string_literal152=(Token)match(input,104,FOLLOW_104_in_andExpr1521); 
            	    string_literal152_tree = 
            	    (Object)adaptor.create(string_literal152)
            	    ;
            	    root_0 = (Object)adaptor.becomeRoot(string_literal152_tree, root_0);


            	    pushFollow(FOLLOW_equExpr_in_andExpr1524);
            	    equExpr153=equExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, equExpr153.getTree());

            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "andExpr"


    public static class equExpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "equExpr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:238:1: equExpr : relExpr ( ( '==' | '!=' ) ^ relExpr )* ;
    public final BarsicParser.equExpr_return equExpr() throws RecognitionException {
        BarsicParser.equExpr_return retval = new BarsicParser.equExpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token set155=null;
        BarsicParser.relExpr_return relExpr154 =null;

        BarsicParser.relExpr_return relExpr156 =null;


        Object set155_tree=null;

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:239:3: ( relExpr ( ( '==' | '!=' ) ^ relExpr )* )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:239:6: relExpr ( ( '==' | '!=' ) ^ relExpr )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_relExpr_in_equExpr1540);
            relExpr154=relExpr();

            state._fsp--;

            adaptor.addChild(root_0, relExpr154.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:239:14: ( ( '==' | '!=' ) ^ relExpr )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==102||LA31_0==118) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:239:15: ( '==' | '!=' ) ^ relExpr
            	    {
            	    set155=(Token)input.LT(1);

            	    set155=(Token)input.LT(1);

            	    if ( input.LA(1)==102||input.LA(1)==118 ) {
            	        input.consume();
            	        root_0 = (Object)adaptor.becomeRoot(
            	        (Object)adaptor.create(set155)
            	        , root_0);
            	        state.errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }


            	    pushFollow(FOLLOW_relExpr_in_equExpr1552);
            	    relExpr156=relExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, relExpr156.getTree());

            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "equExpr"


    public static class relExpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "relExpr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:242:1: relExpr : addExpr ( ( '>=' | '<=' | '>' | '<' ) ^ addExpr )* ;
    public final BarsicParser.relExpr_return relExpr() throws RecognitionException {
        BarsicParser.relExpr_return retval = new BarsicParser.relExpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token set158=null;
        BarsicParser.addExpr_return addExpr157 =null;

        BarsicParser.addExpr_return addExpr159 =null;


        Object set158_tree=null;

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:243:3: ( addExpr ( ( '>=' | '<=' | '>' | '<' ) ^ addExpr )* )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:243:6: addExpr ( ( '>=' | '<=' | '>' | '<' ) ^ addExpr )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_addExpr_in_relExpr1568);
            addExpr157=addExpr();

            state._fsp--;

            adaptor.addChild(root_0, addExpr157.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:243:14: ( ( '>=' | '<=' | '>' | '<' ) ^ addExpr )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( ((LA32_0 >= 115 && LA32_0 <= 116)||(LA32_0 >= 119 && LA32_0 <= 120)) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:243:15: ( '>=' | '<=' | '>' | '<' ) ^ addExpr
            	    {
            	    set158=(Token)input.LT(1);

            	    set158=(Token)input.LT(1);

            	    if ( (input.LA(1) >= 115 && input.LA(1) <= 116)||(input.LA(1) >= 119 && input.LA(1) <= 120) ) {
            	        input.consume();
            	        root_0 = (Object)adaptor.becomeRoot(
            	        (Object)adaptor.create(set158)
            	        , root_0);
            	        state.errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }


            	    pushFollow(FOLLOW_addExpr_in_relExpr1588);
            	    addExpr159=addExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, addExpr159.getTree());

            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "relExpr"


    public static class addExpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "addExpr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:246:1: addExpr : mulExpr ( ( '+' | '-' ) ^ mulExpr )* ;
    public final BarsicParser.addExpr_return addExpr() throws RecognitionException {
        BarsicParser.addExpr_return retval = new BarsicParser.addExpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token set161=null;
        BarsicParser.mulExpr_return mulExpr160 =null;

        BarsicParser.mulExpr_return mulExpr162 =null;


        Object set161_tree=null;

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:247:3: ( mulExpr ( ( '+' | '-' ) ^ mulExpr )* )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:247:6: mulExpr ( ( '+' | '-' ) ^ mulExpr )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_mulExpr_in_addExpr1604);
            mulExpr160=mulExpr();

            state._fsp--;

            adaptor.addChild(root_0, mulExpr160.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:247:14: ( ( '+' | '-' ) ^ mulExpr )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==108||LA33_0==110) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:247:15: ( '+' | '-' ) ^ mulExpr
            	    {
            	    set161=(Token)input.LT(1);

            	    set161=(Token)input.LT(1);

            	    if ( input.LA(1)==108||input.LA(1)==110 ) {
            	        input.consume();
            	        root_0 = (Object)adaptor.becomeRoot(
            	        (Object)adaptor.create(set161)
            	        , root_0);
            	        state.errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }


            	    pushFollow(FOLLOW_mulExpr_in_addExpr1616);
            	    mulExpr162=mulExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, mulExpr162.getTree());

            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "addExpr"


    public static class mulExpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "mulExpr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:250:1: mulExpr : powExpr ( ( '*' | '/' | '%' ) ^ powExpr )* ;
    public final BarsicParser.mulExpr_return mulExpr() throws RecognitionException {
        BarsicParser.mulExpr_return retval = new BarsicParser.mulExpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token set164=null;
        BarsicParser.powExpr_return powExpr163 =null;

        BarsicParser.powExpr_return powExpr165 =null;


        Object set164_tree=null;

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:251:3: ( powExpr ( ( '*' | '/' | '%' ) ^ powExpr )* )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:251:6: powExpr ( ( '*' | '/' | '%' ) ^ powExpr )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_powExpr_in_mulExpr1632);
            powExpr163=powExpr();

            state._fsp--;

            adaptor.addChild(root_0, powExpr163.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:251:14: ( ( '*' | '/' | '%' ) ^ powExpr )*
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( (LA34_0==103||LA34_0==107||LA34_0==112) ) {
                    alt34=1;
                }


                switch (alt34) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:251:15: ( '*' | '/' | '%' ) ^ powExpr
            	    {
            	    set164=(Token)input.LT(1);

            	    set164=(Token)input.LT(1);

            	    if ( input.LA(1)==103||input.LA(1)==107||input.LA(1)==112 ) {
            	        input.consume();
            	        root_0 = (Object)adaptor.becomeRoot(
            	        (Object)adaptor.create(set164)
            	        , root_0);
            	        state.errorRecovery=false;
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        throw mse;
            	    }


            	    pushFollow(FOLLOW_powExpr_in_mulExpr1648);
            	    powExpr165=powExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, powExpr165.getTree());

            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "mulExpr"


    public static class powExpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "powExpr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:254:1: powExpr : unaryExpr ( '^' unaryExpr )* ;
    public final BarsicParser.powExpr_return powExpr() throws RecognitionException {
        BarsicParser.powExpr_return retval = new BarsicParser.powExpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal167=null;
        BarsicParser.unaryExpr_return unaryExpr166 =null;

        BarsicParser.unaryExpr_return unaryExpr168 =null;


        Object char_literal167_tree=null;

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:255:3: ( unaryExpr ( '^' unaryExpr )* )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:255:6: unaryExpr ( '^' unaryExpr )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_unaryExpr_in_powExpr1664);
            unaryExpr166=unaryExpr();

            state._fsp--;

            adaptor.addChild(root_0, unaryExpr166.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:255:16: ( '^' unaryExpr )*
            loop35:
            do {
                int alt35=2;
                int LA35_0 = input.LA(1);

                if ( (LA35_0==124) ) {
                    alt35=1;
                }


                switch (alt35) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:255:17: '^' unaryExpr
            	    {
            	    char_literal167=(Token)match(input,124,FOLLOW_124_in_powExpr1667); 
            	    char_literal167_tree = 
            	    (Object)adaptor.create(char_literal167)
            	    ;
            	    adaptor.addChild(root_0, char_literal167_tree);


            	    pushFollow(FOLLOW_unaryExpr_in_powExpr1669);
            	    unaryExpr168=unaryExpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, unaryExpr168.getTree());

            	    }
            	    break;

            	default :
            	    break loop35;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "powExpr"


    public static class unaryExpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "unaryExpr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:258:1: unaryExpr : ( '-' atom -> ^( UNARY_MIN atom ) | '!' atom -> ^( NEGATE atom ) | atom | identifier | arrayCall | functionCall -> ^( EXP functionCall ) | nameClass | objectInvocation2 );
    public final BarsicParser.unaryExpr_return unaryExpr() throws RecognitionException {
        BarsicParser.unaryExpr_return retval = new BarsicParser.unaryExpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal169=null;
        Token char_literal171=null;
        BarsicParser.atom_return atom170 =null;

        BarsicParser.atom_return atom172 =null;

        BarsicParser.atom_return atom173 =null;

        BarsicParser.identifier_return identifier174 =null;

        BarsicParser.arrayCall_return arrayCall175 =null;

        BarsicParser.functionCall_return functionCall176 =null;

        BarsicParser.nameClass_return nameClass177 =null;

        BarsicParser.objectInvocation2_return objectInvocation2178 =null;


        Object char_literal169_tree=null;
        Object char_literal171_tree=null;
        RewriteRuleTokenStream stream_110=new RewriteRuleTokenStream(adaptor,"token 110");
        RewriteRuleTokenStream stream_101=new RewriteRuleTokenStream(adaptor,"token 101");
        RewriteRuleSubtreeStream stream_functionCall=new RewriteRuleSubtreeStream(adaptor,"rule functionCall");
        RewriteRuleSubtreeStream stream_atom=new RewriteRuleSubtreeStream(adaptor,"rule atom");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:259:3: ( '-' atom -> ^( UNARY_MIN atom ) | '!' atom -> ^( NEGATE atom ) | atom | identifier | arrayCall | functionCall -> ^( EXP functionCall ) | nameClass | objectInvocation2 )
            int alt36=8;
            switch ( input.LA(1) ) {
            case 110:
                {
                alt36=1;
                }
                break;
            case 101:
                {
                alt36=2;
                }
                break;
            case Bool:
            case Null:
            case Number:
            case String:
                {
                alt36=3;
                }
                break;
            case Identifier:
                {
                switch ( input.LA(2) ) {
                case 122:
                    {
                    alt36=5;
                    }
                    break;
                case 105:
                    {
                    alt36=6;
                    }
                    break;
                case Array:
                case Begin:
                case Button:
                case CloseIdentifier:
                case Do:
                case Else:
                case EndCase:
                case File:
                case Form:
                case Hence:
                case Identifier:
                case OBrace:
                case Panel:
                case Subwindow:
                case TextField:
                case TextLabel:
                case To:
                case 102:
                case 103:
                case 104:
                case 106:
                case 107:
                case 108:
                case 109:
                case 110:
                case 112:
                case 113:
                case 114:
                case 115:
                case 116:
                case 118:
                case 119:
                case 120:
                case 121:
                case 123:
                case 124:
                case 127:
                case 128:
                    {
                    alt36=4;
                    }
                    break;
                case 111:
                    {
                    alt36=8;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 4, input);

                    throw nvae;

                }

                }
                break;
            case Array:
                {
                int LA36_5 = input.LA(2);

                if ( (LA36_5==Array||LA36_5==Begin||LA36_5==Button||LA36_5==CloseIdentifier||LA36_5==Do||LA36_5==Else||LA36_5==EndCase||LA36_5==File||LA36_5==Form||LA36_5==Hence||LA36_5==Identifier||LA36_5==OBrace||LA36_5==Panel||LA36_5==Subwindow||(LA36_5 >= TextField && LA36_5 <= To)||(LA36_5 >= 102 && LA36_5 <= 104)||(LA36_5 >= 106 && LA36_5 <= 110)||(LA36_5 >= 112 && LA36_5 <= 116)||(LA36_5 >= 118 && LA36_5 <= 121)||(LA36_5 >= 123 && LA36_5 <= 124)||(LA36_5 >= 127 && LA36_5 <= 128)) ) {
                    alt36=7;
                }
                else if ( (LA36_5==105) ) {
                    alt36=8;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 5, input);

                    throw nvae;

                }
                }
                break;
            case Form:
                {
                int LA36_6 = input.LA(2);

                if ( (LA36_6==Array||LA36_6==Begin||LA36_6==Button||LA36_6==CloseIdentifier||LA36_6==Do||LA36_6==Else||LA36_6==EndCase||LA36_6==File||LA36_6==Form||LA36_6==Hence||LA36_6==Identifier||LA36_6==OBrace||LA36_6==Panel||LA36_6==Subwindow||(LA36_6 >= TextField && LA36_6 <= To)||(LA36_6 >= 102 && LA36_6 <= 104)||(LA36_6 >= 106 && LA36_6 <= 110)||(LA36_6 >= 112 && LA36_6 <= 116)||(LA36_6 >= 118 && LA36_6 <= 121)||(LA36_6 >= 123 && LA36_6 <= 124)||(LA36_6 >= 127 && LA36_6 <= 128)) ) {
                    alt36=7;
                }
                else if ( (LA36_6==105) ) {
                    alt36=8;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 6, input);

                    throw nvae;

                }
                }
                break;
            case Subwindow:
                {
                int LA36_7 = input.LA(2);

                if ( (LA36_7==Array||LA36_7==Begin||LA36_7==Button||LA36_7==CloseIdentifier||LA36_7==Do||LA36_7==Else||LA36_7==EndCase||LA36_7==File||LA36_7==Form||LA36_7==Hence||LA36_7==Identifier||LA36_7==OBrace||LA36_7==Panel||LA36_7==Subwindow||(LA36_7 >= TextField && LA36_7 <= To)||(LA36_7 >= 102 && LA36_7 <= 104)||(LA36_7 >= 106 && LA36_7 <= 110)||(LA36_7 >= 112 && LA36_7 <= 116)||(LA36_7 >= 118 && LA36_7 <= 121)||(LA36_7 >= 123 && LA36_7 <= 124)||(LA36_7 >= 127 && LA36_7 <= 128)) ) {
                    alt36=7;
                }
                else if ( (LA36_7==105) ) {
                    alt36=8;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 7, input);

                    throw nvae;

                }
                }
                break;
            case Button:
                {
                int LA36_8 = input.LA(2);

                if ( (LA36_8==Array||LA36_8==Begin||LA36_8==Button||LA36_8==CloseIdentifier||LA36_8==Do||LA36_8==Else||LA36_8==EndCase||LA36_8==File||LA36_8==Form||LA36_8==Hence||LA36_8==Identifier||LA36_8==OBrace||LA36_8==Panel||LA36_8==Subwindow||(LA36_8 >= TextField && LA36_8 <= To)||(LA36_8 >= 102 && LA36_8 <= 104)||(LA36_8 >= 106 && LA36_8 <= 110)||(LA36_8 >= 112 && LA36_8 <= 116)||(LA36_8 >= 118 && LA36_8 <= 121)||(LA36_8 >= 123 && LA36_8 <= 124)||(LA36_8 >= 127 && LA36_8 <= 128)) ) {
                    alt36=7;
                }
                else if ( (LA36_8==105) ) {
                    alt36=8;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 8, input);

                    throw nvae;

                }
                }
                break;
            case File:
                {
                int LA36_9 = input.LA(2);

                if ( (LA36_9==Array||LA36_9==Begin||LA36_9==Button||LA36_9==CloseIdentifier||LA36_9==Do||LA36_9==Else||LA36_9==EndCase||LA36_9==File||LA36_9==Form||LA36_9==Hence||LA36_9==Identifier||LA36_9==OBrace||LA36_9==Panel||LA36_9==Subwindow||(LA36_9 >= TextField && LA36_9 <= To)||(LA36_9 >= 102 && LA36_9 <= 104)||(LA36_9 >= 106 && LA36_9 <= 110)||(LA36_9 >= 112 && LA36_9 <= 116)||(LA36_9 >= 118 && LA36_9 <= 121)||(LA36_9 >= 123 && LA36_9 <= 124)||(LA36_9 >= 127 && LA36_9 <= 128)) ) {
                    alt36=7;
                }
                else if ( (LA36_9==105) ) {
                    alt36=8;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 9, input);

                    throw nvae;

                }
                }
                break;
            case Panel:
                {
                int LA36_10 = input.LA(2);

                if ( (LA36_10==Array||LA36_10==Begin||LA36_10==Button||LA36_10==CloseIdentifier||LA36_10==Do||LA36_10==Else||LA36_10==EndCase||LA36_10==File||LA36_10==Form||LA36_10==Hence||LA36_10==Identifier||LA36_10==OBrace||LA36_10==Panel||LA36_10==Subwindow||(LA36_10 >= TextField && LA36_10 <= To)||(LA36_10 >= 102 && LA36_10 <= 104)||(LA36_10 >= 106 && LA36_10 <= 110)||(LA36_10 >= 112 && LA36_10 <= 116)||(LA36_10 >= 118 && LA36_10 <= 121)||(LA36_10 >= 123 && LA36_10 <= 124)||(LA36_10 >= 127 && LA36_10 <= 128)) ) {
                    alt36=7;
                }
                else if ( (LA36_10==105) ) {
                    alt36=8;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 10, input);

                    throw nvae;

                }
                }
                break;
            case TextLabel:
                {
                int LA36_11 = input.LA(2);

                if ( (LA36_11==Array||LA36_11==Begin||LA36_11==Button||LA36_11==CloseIdentifier||LA36_11==Do||LA36_11==Else||LA36_11==EndCase||LA36_11==File||LA36_11==Form||LA36_11==Hence||LA36_11==Identifier||LA36_11==OBrace||LA36_11==Panel||LA36_11==Subwindow||(LA36_11 >= TextField && LA36_11 <= To)||(LA36_11 >= 102 && LA36_11 <= 104)||(LA36_11 >= 106 && LA36_11 <= 110)||(LA36_11 >= 112 && LA36_11 <= 116)||(LA36_11 >= 118 && LA36_11 <= 121)||(LA36_11 >= 123 && LA36_11 <= 124)||(LA36_11 >= 127 && LA36_11 <= 128)) ) {
                    alt36=7;
                }
                else if ( (LA36_11==105) ) {
                    alt36=8;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 11, input);

                    throw nvae;

                }
                }
                break;
            case TextField:
                {
                int LA36_12 = input.LA(2);

                if ( (LA36_12==Array||LA36_12==Begin||LA36_12==Button||LA36_12==CloseIdentifier||LA36_12==Do||LA36_12==Else||LA36_12==EndCase||LA36_12==File||LA36_12==Form||LA36_12==Hence||LA36_12==Identifier||LA36_12==OBrace||LA36_12==Panel||LA36_12==Subwindow||(LA36_12 >= TextField && LA36_12 <= To)||(LA36_12 >= 102 && LA36_12 <= 104)||(LA36_12 >= 106 && LA36_12 <= 110)||(LA36_12 >= 112 && LA36_12 <= 116)||(LA36_12 >= 118 && LA36_12 <= 121)||(LA36_12 >= 123 && LA36_12 <= 124)||(LA36_12 >= 127 && LA36_12 <= 128)) ) {
                    alt36=7;
                }
                else if ( (LA36_12==105) ) {
                    alt36=8;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 36, 12, input);

                    throw nvae;

                }
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;

            }

            switch (alt36) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:259:6: '-' atom
                    {
                    char_literal169=(Token)match(input,110,FOLLOW_110_in_unaryExpr1685);  
                    stream_110.add(char_literal169);


                    pushFollow(FOLLOW_atom_in_unaryExpr1687);
                    atom170=atom();

                    state._fsp--;

                    stream_atom.add(atom170.getTree());

                    // AST REWRITE
                    // elements: atom
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 259:15: -> ^( UNARY_MIN atom )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:259:18: ^( UNARY_MIN atom )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(UNARY_MIN, "UNARY_MIN")
                        , root_1);

                        adaptor.addChild(root_1, stream_atom.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:260:6: '!' atom
                    {
                    char_literal171=(Token)match(input,101,FOLLOW_101_in_unaryExpr1702);  
                    stream_101.add(char_literal171);


                    pushFollow(FOLLOW_atom_in_unaryExpr1704);
                    atom172=atom();

                    state._fsp--;

                    stream_atom.add(atom172.getTree());

                    // AST REWRITE
                    // elements: atom
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 260:15: -> ^( NEGATE atom )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:260:18: ^( NEGATE atom )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(NEGATE, "NEGATE")
                        , root_1);

                        adaptor.addChild(root_1, stream_atom.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:261:6: atom
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_atom_in_unaryExpr1719);
                    atom173=atom();

                    state._fsp--;

                    adaptor.addChild(root_0, atom173.getTree());

                    }
                    break;
                case 4 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:262:6: identifier
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_identifier_in_unaryExpr1726);
                    identifier174=identifier();

                    state._fsp--;

                    adaptor.addChild(root_0, identifier174.getTree());

                    }
                    break;
                case 5 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:263:6: arrayCall
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_arrayCall_in_unaryExpr1734);
                    arrayCall175=arrayCall();

                    state._fsp--;

                    adaptor.addChild(root_0, arrayCall175.getTree());

                    }
                    break;
                case 6 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:264:6: functionCall
                    {
                    pushFollow(FOLLOW_functionCall_in_unaryExpr1741);
                    functionCall176=functionCall();

                    state._fsp--;

                    stream_functionCall.add(functionCall176.getTree());

                    // AST REWRITE
                    // elements: functionCall
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 264:19: -> ^( EXP functionCall )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:264:22: ^( EXP functionCall )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(EXP, "EXP")
                        , root_1);

                        adaptor.addChild(root_1, stream_functionCall.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 7 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:265:6: nameClass
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_nameClass_in_unaryExpr1756);
                    nameClass177=nameClass();

                    state._fsp--;

                    adaptor.addChild(root_0, nameClass177.getTree());

                    }
                    break;
                case 8 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:266:6: objectInvocation2
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_objectInvocation2_in_unaryExpr1764);
                    objectInvocation2178=objectInvocation2();

                    state._fsp--;

                    adaptor.addChild(root_0, objectInvocation2178.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "unaryExpr"


    public static class atom_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "atom"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:269:1: atom : ( Number | Bool | Null | String );
    public final BarsicParser.atom_return atom() throws RecognitionException {
        BarsicParser.atom_return retval = new BarsicParser.atom_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token set179=null;

        Object set179_tree=null;

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:270:3: ( Number | Bool | Null | String )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:
            {
            root_0 = (Object)adaptor.nil();


            set179=(Token)input.LT(1);

            if ( input.LA(1)==Bool||(input.LA(1) >= Null && input.LA(1) <= Number)||input.LA(1)==String ) {
                input.consume();
                adaptor.addChild(root_0, 
                (Object)adaptor.create(set179)
                );
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "atom"


    public static class idList_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "idList"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:276:1: idList : Identifier ( '.' Identifier )* -> ^( ID_LIST ( Identifier )+ ) ;
    public final BarsicParser.idList_return idList() throws RecognitionException {
        BarsicParser.idList_return retval = new BarsicParser.idList_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Identifier180=null;
        Token char_literal181=null;
        Token Identifier182=null;

        Object Identifier180_tree=null;
        Object char_literal181_tree=null;
        Object Identifier182_tree=null;
        RewriteRuleTokenStream stream_111=new RewriteRuleTokenStream(adaptor,"token 111");
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:277:3: ( Identifier ( '.' Identifier )* -> ^( ID_LIST ( Identifier )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:277:6: Identifier ( '.' Identifier )*
            {
            Identifier180=(Token)match(input,Identifier,FOLLOW_Identifier_in_idList1813);  
            stream_Identifier.add(Identifier180);


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:277:17: ( '.' Identifier )*
            loop37:
            do {
                int alt37=2;
                int LA37_0 = input.LA(1);

                if ( (LA37_0==111) ) {
                    alt37=1;
                }


                switch (alt37) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:277:18: '.' Identifier
            	    {
            	    char_literal181=(Token)match(input,111,FOLLOW_111_in_idList1816);  
            	    stream_111.add(char_literal181);


            	    Identifier182=(Token)match(input,Identifier,FOLLOW_Identifier_in_idList1818);  
            	    stream_Identifier.add(Identifier182);


            	    }
            	    break;

            	default :
            	    break loop37;
                }
            } while (true);


            // AST REWRITE
            // elements: Identifier
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 277:35: -> ^( ID_LIST ( Identifier )+ )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:277:38: ^( ID_LIST ( Identifier )+ )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(ID_LIST, "ID_LIST")
                , root_1);

                if ( !(stream_Identifier.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_Identifier.hasNext() ) {
                    adaptor.addChild(root_1, 
                    stream_Identifier.nextNode()
                    );

                }
                stream_Identifier.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "idList"


    public static class idFormList_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "idFormList"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:280:1: idFormList : idForm ( '.' idForm )* -> ^( ID_LIST ( idForm )+ ) ;
    public final BarsicParser.idFormList_return idFormList() throws RecognitionException {
        BarsicParser.idFormList_return retval = new BarsicParser.idFormList_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal184=null;
        BarsicParser.idForm_return idForm183 =null;

        BarsicParser.idForm_return idForm185 =null;


        Object char_literal184_tree=null;
        RewriteRuleTokenStream stream_111=new RewriteRuleTokenStream(adaptor,"token 111");
        RewriteRuleSubtreeStream stream_idForm=new RewriteRuleSubtreeStream(adaptor,"rule idForm");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:281:2: ( idForm ( '.' idForm )* -> ^( ID_LIST ( idForm )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:281:5: idForm ( '.' idForm )*
            {
            pushFollow(FOLLOW_idForm_in_idFormList1844);
            idForm183=idForm();

            state._fsp--;

            stream_idForm.add(idForm183.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:281:12: ( '.' idForm )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( (LA38_0==111) ) {
                    alt38=1;
                }


                switch (alt38) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:281:13: '.' idForm
            	    {
            	    char_literal184=(Token)match(input,111,FOLLOW_111_in_idFormList1847);  
            	    stream_111.add(char_literal184);


            	    pushFollow(FOLLOW_idForm_in_idFormList1849);
            	    idForm185=idForm();

            	    state._fsp--;

            	    stream_idForm.add(idForm185.getTree());

            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);


            // AST REWRITE
            // elements: idForm
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 281:26: -> ^( ID_LIST ( idForm )+ )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:281:29: ^( ID_LIST ( idForm )+ )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(ID_LIST, "ID_LIST")
                , root_1);

                if ( !(stream_idForm.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_idForm.hasNext() ) {
                    adaptor.addChild(root_1, stream_idForm.nextTree());

                }
                stream_idForm.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "idFormList"


    public static class idForm_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "idForm"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:284:1: idForm : ( Identifier -> Identifier | nameClass '(' Identifier ')' -> Identifier );
    public final BarsicParser.idForm_return idForm() throws RecognitionException {
        BarsicParser.idForm_return retval = new BarsicParser.idForm_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Identifier186=null;
        Token char_literal188=null;
        Token Identifier189=null;
        Token char_literal190=null;
        BarsicParser.nameClass_return nameClass187 =null;


        Object Identifier186_tree=null;
        Object char_literal188_tree=null;
        Object Identifier189_tree=null;
        Object char_literal190_tree=null;
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
        RewriteRuleTokenStream stream_105=new RewriteRuleTokenStream(adaptor,"token 105");
        RewriteRuleTokenStream stream_106=new RewriteRuleTokenStream(adaptor,"token 106");
        RewriteRuleSubtreeStream stream_nameClass=new RewriteRuleSubtreeStream(adaptor,"rule nameClass");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:285:3: ( Identifier -> Identifier | nameClass '(' Identifier ')' -> Identifier )
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==Identifier) ) {
                alt39=1;
            }
            else if ( (LA39_0==Array||LA39_0==Button||LA39_0==File||LA39_0==Form||LA39_0==Panel||LA39_0==Subwindow||(LA39_0 >= TextField && LA39_0 <= TextLabel)) ) {
                alt39=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 39, 0, input);

                throw nvae;

            }
            switch (alt39) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:285:6: Identifier
                    {
                    Identifier186=(Token)match(input,Identifier,FOLLOW_Identifier_in_idForm1881);  
                    stream_Identifier.add(Identifier186);


                    // AST REWRITE
                    // elements: Identifier
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 285:18: -> Identifier
                    {
                        adaptor.addChild(root_0, 
                        stream_Identifier.nextNode()
                        );

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:286:6: nameClass '(' Identifier ')'
                    {
                    pushFollow(FOLLOW_nameClass_in_idForm1893);
                    nameClass187=nameClass();

                    state._fsp--;

                    stream_nameClass.add(nameClass187.getTree());

                    char_literal188=(Token)match(input,105,FOLLOW_105_in_idForm1895);  
                    stream_105.add(char_literal188);


                    Identifier189=(Token)match(input,Identifier,FOLLOW_Identifier_in_idForm1896);  
                    stream_Identifier.add(Identifier189);


                    char_literal190=(Token)match(input,106,FOLLOW_106_in_idForm1897);  
                    stream_106.add(char_literal190);


                    // AST REWRITE
                    // elements: Identifier
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 286:33: -> Identifier
                    {
                        adaptor.addChild(root_0, 
                        stream_Identifier.nextNode()
                        );

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "idForm"


    public static class formlookup_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "formlookup"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:289:1: formlookup : idFormList -> ^( LOOKUP idFormList ) ;
    public final BarsicParser.formlookup_return formlookup() throws RecognitionException {
        BarsicParser.formlookup_return retval = new BarsicParser.formlookup_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        BarsicParser.idFormList_return idFormList191 =null;


        RewriteRuleSubtreeStream stream_idFormList=new RewriteRuleSubtreeStream(adaptor,"rule idFormList");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:290:3: ( idFormList -> ^( LOOKUP idFormList ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:290:6: idFormList
            {
            pushFollow(FOLLOW_idFormList_in_formlookup1918);
            idFormList191=idFormList();

            state._fsp--;

            stream_idFormList.add(idFormList191.getTree());

            // AST REWRITE
            // elements: idFormList
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 290:19: -> ^( LOOKUP idFormList )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:290:22: ^( LOOKUP idFormList )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(LOOKUP, "LOOKUP")
                , root_1);

                adaptor.addChild(root_1, stream_idFormList.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "formlookup"


    public static class lookup_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "lookup"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:293:1: lookup : idList -> ^( LOOKUP idList ) ;
    public final BarsicParser.lookup_return lookup() throws RecognitionException {
        BarsicParser.lookup_return retval = new BarsicParser.lookup_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        BarsicParser.idList_return idList192 =null;


        RewriteRuleSubtreeStream stream_idList=new RewriteRuleSubtreeStream(adaptor,"rule idList");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:294:3: ( idList -> ^( LOOKUP idList ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:294:6: idList
            {
            pushFollow(FOLLOW_idList_in_lookup1944);
            idList192=idList();

            state._fsp--;

            stream_idList.add(idList192.getTree());

            // AST REWRITE
            // elements: idList
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 294:15: -> ^( LOOKUP idList )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:294:18: ^( LOOKUP idList )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(LOOKUP, "LOOKUP")
                , root_1);

                adaptor.addChild(root_1, stream_idList.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "lookup"


    public static class lookuparr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "lookuparr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:297:1: lookuparr : ( formlookup | arrayCall );
    public final BarsicParser.lookuparr_return lookuparr() throws RecognitionException {
        BarsicParser.lookuparr_return retval = new BarsicParser.lookuparr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        BarsicParser.formlookup_return formlookup193 =null;

        BarsicParser.arrayCall_return arrayCall194 =null;



        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:298:3: ( formlookup | arrayCall )
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==Identifier) ) {
                int LA40_1 = input.LA(2);

                if ( (LA40_1==122) ) {
                    alt40=2;
                }
                else if ( (LA40_1==111||LA40_1==117) ) {
                    alt40=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 40, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA40_0==Array||LA40_0==Button||LA40_0==File||LA40_0==Form||LA40_0==Panel||LA40_0==Subwindow||(LA40_0 >= TextField && LA40_0 <= TextLabel)) ) {
                alt40=1;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 40, 0, input);

                throw nvae;

            }
            switch (alt40) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:298:5: formlookup
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_formlookup_in_lookuparr1970);
                    formlookup193=formlookup();

                    state._fsp--;

                    adaptor.addChild(root_0, formlookup193.getTree());

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:299:5: arrayCall
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_arrayCall_in_lookuparr1976);
                    arrayCall194=arrayCall();

                    state._fsp--;

                    adaptor.addChild(root_0, arrayCall194.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "lookuparr"


    public static class objectInvocation_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "objectInvocation"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:307:1: objectInvocation : a= typeCast1 ( '.' typeCast1 )* ( '.' objectInvocationMethod ) -> ^( FUNC_CALL FormType $a objectInvocationMethod ) ;
    public final BarsicParser.objectInvocation_return objectInvocation() throws RecognitionException {
        BarsicParser.objectInvocation_return retval = new BarsicParser.objectInvocation_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal195=null;
        Token char_literal197=null;
        BarsicParser.typeCast1_return a =null;

        BarsicParser.typeCast1_return typeCast1196 =null;

        BarsicParser.objectInvocationMethod_return objectInvocationMethod198 =null;


        Object char_literal195_tree=null;
        Object char_literal197_tree=null;
        RewriteRuleTokenStream stream_111=new RewriteRuleTokenStream(adaptor,"token 111");
        RewriteRuleSubtreeStream stream_objectInvocationMethod=new RewriteRuleSubtreeStream(adaptor,"rule objectInvocationMethod");
        RewriteRuleSubtreeStream stream_typeCast1=new RewriteRuleSubtreeStream(adaptor,"rule typeCast1");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:308:3: (a= typeCast1 ( '.' typeCast1 )* ( '.' objectInvocationMethod ) -> ^( FUNC_CALL FormType $a objectInvocationMethod ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:308:5: a= typeCast1 ( '.' typeCast1 )* ( '.' objectInvocationMethod )
            {
            pushFollow(FOLLOW_typeCast1_in_objectInvocation1997);
            a=typeCast1();

            state._fsp--;

            stream_typeCast1.add(a.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:308:18: ( '.' typeCast1 )*
            loop41:
            do {
                int alt41=2;
                int LA41_0 = input.LA(1);

                if ( (LA41_0==111) ) {
                    int LA41_1 = input.LA(2);

                    if ( (LA41_1==Identifier) ) {
                        int LA41_2 = input.LA(3);

                        if ( (LA41_2==111) ) {
                            alt41=1;
                        }


                    }
                    else if ( (LA41_1==Array||LA41_1==Button||LA41_1==File||LA41_1==Form||LA41_1==Panel||LA41_1==Subwindow||(LA41_1 >= TextField && LA41_1 <= TextLabel)) ) {
                        alt41=1;
                    }


                }


                switch (alt41) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:308:19: '.' typeCast1
            	    {
            	    char_literal195=(Token)match(input,111,FOLLOW_111_in_objectInvocation2001);  
            	    stream_111.add(char_literal195);


            	    pushFollow(FOLLOW_typeCast1_in_objectInvocation2003);
            	    typeCast1196=typeCast1();

            	    state._fsp--;

            	    stream_typeCast1.add(typeCast1196.getTree());

            	    }
            	    break;

            	default :
            	    break loop41;
                }
            } while (true);


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:308:35: ( '.' objectInvocationMethod )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:308:36: '.' objectInvocationMethod
            {
            char_literal197=(Token)match(input,111,FOLLOW_111_in_objectInvocation2008);  
            stream_111.add(char_literal197);


            pushFollow(FOLLOW_objectInvocationMethod_in_objectInvocation2010);
            objectInvocationMethod198=objectInvocationMethod();

            state._fsp--;

            stream_objectInvocationMethod.add(objectInvocationMethod198.getTree());

            }


            // AST REWRITE
            // elements: a, objectInvocationMethod
            // token labels: 
            // rule labels: a, retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 308:65: -> ^( FUNC_CALL FormType $a objectInvocationMethod )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:308:68: ^( FUNC_CALL FormType $a objectInvocationMethod )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(FUNC_CALL, "FUNC_CALL")
                , root_1);

                adaptor.addChild(root_1, 
                (Object)adaptor.create(FormType, "FormType")
                );

                adaptor.addChild(root_1, stream_a.nextTree());

                adaptor.addChild(root_1, stream_objectInvocationMethod.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "objectInvocation"


    public static class objectInvocationMethod_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "objectInvocationMethod"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:311:1: objectInvocationMethod : ( Identifier paramList CloseIdentifier -> ^( FUNC_CALL FormType Identifier paramList ) | 'output' block '_output' -> ^( FUNC_CALL FormType block ) | Identifier '(' ( exprList )? ')' -> ^( FUNC_CALL FormType Identifier ( exprList )? ) );
    public final BarsicParser.objectInvocationMethod_return objectInvocationMethod() throws RecognitionException {
        BarsicParser.objectInvocationMethod_return retval = new BarsicParser.objectInvocationMethod_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Identifier199=null;
        Token CloseIdentifier201=null;
        Token string_literal202=null;
        Token string_literal204=null;
        Token Identifier205=null;
        Token char_literal206=null;
        Token char_literal208=null;
        BarsicParser.paramList_return paramList200 =null;

        BarsicParser.block_return block203 =null;

        BarsicParser.exprList_return exprList207 =null;


        Object Identifier199_tree=null;
        Object CloseIdentifier201_tree=null;
        Object string_literal202_tree=null;
        Object string_literal204_tree=null;
        Object Identifier205_tree=null;
        Object char_literal206_tree=null;
        Object char_literal208_tree=null;
        RewriteRuleTokenStream stream_CloseIdentifier=new RewriteRuleTokenStream(adaptor,"token CloseIdentifier");
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
        RewriteRuleTokenStream stream_125=new RewriteRuleTokenStream(adaptor,"token 125");
        RewriteRuleTokenStream stream_126=new RewriteRuleTokenStream(adaptor,"token 126");
        RewriteRuleTokenStream stream_105=new RewriteRuleTokenStream(adaptor,"token 105");
        RewriteRuleTokenStream stream_106=new RewriteRuleTokenStream(adaptor,"token 106");
        RewriteRuleSubtreeStream stream_paramList=new RewriteRuleSubtreeStream(adaptor,"rule paramList");
        RewriteRuleSubtreeStream stream_exprList=new RewriteRuleSubtreeStream(adaptor,"rule exprList");
        RewriteRuleSubtreeStream stream_block=new RewriteRuleSubtreeStream(adaptor,"rule block");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:312:2: ( Identifier paramList CloseIdentifier -> ^( FUNC_CALL FormType Identifier paramList ) | 'output' block '_output' -> ^( FUNC_CALL FormType block ) | Identifier '(' ( exprList )? ')' -> ^( FUNC_CALL FormType Identifier ( exprList )? ) )
            int alt43=3;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==Identifier) ) {
                int LA43_1 = input.LA(2);

                if ( (LA43_1==105) ) {
                    alt43=3;
                }
                else if ( (LA43_1==Array||LA43_1==Button||LA43_1==File||LA43_1==Form||LA43_1==Identifier||LA43_1==Panel||LA43_1==Subwindow||(LA43_1 >= TextField && LA43_1 <= TextLabel)) ) {
                    alt43=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 43, 1, input);

                    throw nvae;

                }
            }
            else if ( (LA43_0==126) ) {
                alt43=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;

            }
            switch (alt43) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:312:5: Identifier paramList CloseIdentifier
                    {
                    Identifier199=(Token)match(input,Identifier,FOLLOW_Identifier_in_objectInvocationMethod2040);  
                    stream_Identifier.add(Identifier199);


                    pushFollow(FOLLOW_paramList_in_objectInvocationMethod2042);
                    paramList200=paramList();

                    state._fsp--;

                    stream_paramList.add(paramList200.getTree());

                    CloseIdentifier201=(Token)match(input,CloseIdentifier,FOLLOW_CloseIdentifier_in_objectInvocationMethod2044);  
                    stream_CloseIdentifier.add(CloseIdentifier201);


                    // AST REWRITE
                    // elements: paramList, Identifier
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 312:42: -> ^( FUNC_CALL FormType Identifier paramList )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:312:45: ^( FUNC_CALL FormType Identifier paramList )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(FUNC_CALL, "FUNC_CALL")
                        , root_1);

                        adaptor.addChild(root_1, 
                        (Object)adaptor.create(FormType, "FormType")
                        );

                        adaptor.addChild(root_1, 
                        stream_Identifier.nextNode()
                        );

                        adaptor.addChild(root_1, stream_paramList.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:313:4: 'output' block '_output'
                    {
                    string_literal202=(Token)match(input,126,FOLLOW_126_in_objectInvocationMethod2061);  
                    stream_126.add(string_literal202);


                    pushFollow(FOLLOW_block_in_objectInvocationMethod2063);
                    block203=block();

                    state._fsp--;

                    stream_block.add(block203.getTree());

                    string_literal204=(Token)match(input,125,FOLLOW_125_in_objectInvocationMethod2065);  
                    stream_125.add(string_literal204);


                    // AST REWRITE
                    // elements: block
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 313:29: -> ^( FUNC_CALL FormType block )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:313:32: ^( FUNC_CALL FormType block )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(FUNC_CALL, "FUNC_CALL")
                        , root_1);

                        adaptor.addChild(root_1, 
                        (Object)adaptor.create(FormType, "FormType")
                        );

                        adaptor.addChild(root_1, stream_block.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:314:5: Identifier '(' ( exprList )? ')'
                    {
                    Identifier205=(Token)match(input,Identifier,FOLLOW_Identifier_in_objectInvocationMethod2081);  
                    stream_Identifier.add(Identifier205);


                    char_literal206=(Token)match(input,105,FOLLOW_105_in_objectInvocationMethod2083);  
                    stream_105.add(char_literal206);


                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:314:20: ( exprList )?
                    int alt42=2;
                    int LA42_0 = input.LA(1);

                    if ( (LA42_0==Array||(LA42_0 >= Bool && LA42_0 <= Button)||LA42_0==File||LA42_0==Form||LA42_0==Identifier||(LA42_0 >= New && LA42_0 <= OBrace)||LA42_0==Panel||(LA42_0 >= String && LA42_0 <= Subwindow)||(LA42_0 >= TextField && LA42_0 <= TextLabel)||LA42_0==101||LA42_0==110) ) {
                        alt42=1;
                    }
                    switch (alt42) {
                        case 1 :
                            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:314:20: exprList
                            {
                            pushFollow(FOLLOW_exprList_in_objectInvocationMethod2085);
                            exprList207=exprList();

                            state._fsp--;

                            stream_exprList.add(exprList207.getTree());

                            }
                            break;

                    }


                    char_literal208=(Token)match(input,106,FOLLOW_106_in_objectInvocationMethod2088);  
                    stream_106.add(char_literal208);


                    // AST REWRITE
                    // elements: exprList, Identifier
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 314:34: -> ^( FUNC_CALL FormType Identifier ( exprList )? )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:314:37: ^( FUNC_CALL FormType Identifier ( exprList )? )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(FUNC_CALL, "FUNC_CALL")
                        , root_1);

                        adaptor.addChild(root_1, 
                        (Object)adaptor.create(FormType, "FormType")
                        );

                        adaptor.addChild(root_1, 
                        stream_Identifier.nextNode()
                        );

                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:314:69: ( exprList )?
                        if ( stream_exprList.hasNext() ) {
                            adaptor.addChild(root_1, stream_exprList.nextTree());

                        }
                        stream_exprList.reset();

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "objectInvocationMethod"


    public static class typeCast1_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "typeCast1"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:317:1: typeCast1 : ( nameClass '(' expression ')' -> ^( TYPECASTEXP expression ) | identifier -> ^( TYPECASTID identifier ) );
    public final BarsicParser.typeCast1_return typeCast1() throws RecognitionException {
        BarsicParser.typeCast1_return retval = new BarsicParser.typeCast1_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal210=null;
        Token char_literal212=null;
        BarsicParser.nameClass_return nameClass209 =null;

        BarsicParser.expression_return expression211 =null;

        BarsicParser.identifier_return identifier213 =null;


        Object char_literal210_tree=null;
        Object char_literal212_tree=null;
        RewriteRuleTokenStream stream_105=new RewriteRuleTokenStream(adaptor,"token 105");
        RewriteRuleTokenStream stream_106=new RewriteRuleTokenStream(adaptor,"token 106");
        RewriteRuleSubtreeStream stream_nameClass=new RewriteRuleSubtreeStream(adaptor,"rule nameClass");
        RewriteRuleSubtreeStream stream_identifier=new RewriteRuleSubtreeStream(adaptor,"rule identifier");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:318:3: ( nameClass '(' expression ')' -> ^( TYPECASTEXP expression ) | identifier -> ^( TYPECASTID identifier ) )
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==Array||LA44_0==Button||LA44_0==File||LA44_0==Form||LA44_0==Panel||LA44_0==Subwindow||(LA44_0 >= TextField && LA44_0 <= TextLabel)) ) {
                alt44=1;
            }
            else if ( (LA44_0==Identifier) ) {
                alt44=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 44, 0, input);

                throw nvae;

            }
            switch (alt44) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:318:6: nameClass '(' expression ')'
                    {
                    pushFollow(FOLLOW_nameClass_in_typeCast12117);
                    nameClass209=nameClass();

                    state._fsp--;

                    stream_nameClass.add(nameClass209.getTree());

                    char_literal210=(Token)match(input,105,FOLLOW_105_in_typeCast12119);  
                    stream_105.add(char_literal210);


                    pushFollow(FOLLOW_expression_in_typeCast12121);
                    expression211=expression();

                    state._fsp--;

                    stream_expression.add(expression211.getTree());

                    char_literal212=(Token)match(input,106,FOLLOW_106_in_typeCast12123);  
                    stream_106.add(char_literal212);


                    // AST REWRITE
                    // elements: expression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 318:36: -> ^( TYPECASTEXP expression )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:318:39: ^( TYPECASTEXP expression )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(TYPECASTEXP, "TYPECASTEXP")
                        , root_1);

                        adaptor.addChild(root_1, stream_expression.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:319:6: identifier
                    {
                    pushFollow(FOLLOW_identifier_in_typeCast12139);
                    identifier213=identifier();

                    state._fsp--;

                    stream_identifier.add(identifier213.getTree());

                    // AST REWRITE
                    // elements: identifier
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 319:22: -> ^( TYPECASTID identifier )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:319:25: ^( TYPECASTID identifier )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(TYPECASTID, "TYPECASTID")
                        , root_1);

                        adaptor.addChild(root_1, stream_identifier.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "typeCast1"


    public static class typeCast2_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "typeCast2"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:322:1: typeCast2 : ( nameClass '(' expression ')' -> ^( TYPE1 expression ) | functionCall -> ^( TYPE2 functionCall ) | identifier -> ^( TYPE3 identifier ) );
    public final BarsicParser.typeCast2_return typeCast2() throws RecognitionException {
        BarsicParser.typeCast2_return retval = new BarsicParser.typeCast2_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal215=null;
        Token char_literal217=null;
        BarsicParser.nameClass_return nameClass214 =null;

        BarsicParser.expression_return expression216 =null;

        BarsicParser.functionCall_return functionCall218 =null;

        BarsicParser.identifier_return identifier219 =null;


        Object char_literal215_tree=null;
        Object char_literal217_tree=null;
        RewriteRuleTokenStream stream_105=new RewriteRuleTokenStream(adaptor,"token 105");
        RewriteRuleTokenStream stream_106=new RewriteRuleTokenStream(adaptor,"token 106");
        RewriteRuleSubtreeStream stream_nameClass=new RewriteRuleSubtreeStream(adaptor,"rule nameClass");
        RewriteRuleSubtreeStream stream_identifier=new RewriteRuleSubtreeStream(adaptor,"rule identifier");
        RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
        RewriteRuleSubtreeStream stream_functionCall=new RewriteRuleSubtreeStream(adaptor,"rule functionCall");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:323:2: ( nameClass '(' expression ')' -> ^( TYPE1 expression ) | functionCall -> ^( TYPE2 functionCall ) | identifier -> ^( TYPE3 identifier ) )
            int alt45=3;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==Array||LA45_0==Button||LA45_0==File||LA45_0==Form||LA45_0==Panel||LA45_0==Subwindow||(LA45_0 >= TextField && LA45_0 <= TextLabel)) ) {
                alt45=1;
            }
            else if ( (LA45_0==Identifier) ) {
                int LA45_2 = input.LA(2);

                if ( (LA45_2==105) ) {
                    alt45=2;
                }
                else if ( (LA45_2==Array||LA45_2==Begin||LA45_2==Button||LA45_2==CloseIdentifier||LA45_2==Do||LA45_2==Else||LA45_2==EndCase||LA45_2==File||LA45_2==Form||LA45_2==Hence||LA45_2==Identifier||LA45_2==OBrace||LA45_2==Panel||LA45_2==Subwindow||(LA45_2 >= TextField && LA45_2 <= To)||(LA45_2 >= 102 && LA45_2 <= 104)||(LA45_2 >= 106 && LA45_2 <= 116)||(LA45_2 >= 118 && LA45_2 <= 121)||(LA45_2 >= 123 && LA45_2 <= 124)||(LA45_2 >= 127 && LA45_2 <= 128)) ) {
                    alt45=3;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 45, 2, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 45, 0, input);

                throw nvae;

            }
            switch (alt45) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:323:5: nameClass '(' expression ')'
                    {
                    pushFollow(FOLLOW_nameClass_in_typeCast22170);
                    nameClass214=nameClass();

                    state._fsp--;

                    stream_nameClass.add(nameClass214.getTree());

                    char_literal215=(Token)match(input,105,FOLLOW_105_in_typeCast22172);  
                    stream_105.add(char_literal215);


                    pushFollow(FOLLOW_expression_in_typeCast22174);
                    expression216=expression();

                    state._fsp--;

                    stream_expression.add(expression216.getTree());

                    char_literal217=(Token)match(input,106,FOLLOW_106_in_typeCast22176);  
                    stream_106.add(char_literal217);


                    // AST REWRITE
                    // elements: expression
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 323:34: -> ^( TYPE1 expression )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:323:37: ^( TYPE1 expression )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(TYPE1, "TYPE1")
                        , root_1);

                        adaptor.addChild(root_1, stream_expression.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:324:5: functionCall
                    {
                    pushFollow(FOLLOW_functionCall_in_typeCast22190);
                    functionCall218=functionCall();

                    state._fsp--;

                    stream_functionCall.add(functionCall218.getTree());

                    // AST REWRITE
                    // elements: functionCall
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 324:18: -> ^( TYPE2 functionCall )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:324:21: ^( TYPE2 functionCall )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(TYPE2, "TYPE2")
                        , root_1);

                        adaptor.addChild(root_1, stream_functionCall.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:325:5: identifier
                    {
                    pushFollow(FOLLOW_identifier_in_typeCast22205);
                    identifier219=identifier();

                    state._fsp--;

                    stream_identifier.add(identifier219.getTree());

                    // AST REWRITE
                    // elements: identifier
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 325:15: -> ^( TYPE3 identifier )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:325:18: ^( TYPE3 identifier )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(TYPE3, "TYPE3")
                        , root_1);

                        adaptor.addChild(root_1, stream_identifier.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "typeCast2"


    public static class typeCast2List_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "typeCast2List"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:329:2: typeCast2List : ( '.' typeCast2 )+ -> ^( TYPECAST2LIST ( typeCast2 )+ ) ;
    public final BarsicParser.typeCast2List_return typeCast2List() throws RecognitionException {
        BarsicParser.typeCast2List_return retval = new BarsicParser.typeCast2List_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal220=null;
        BarsicParser.typeCast2_return typeCast2221 =null;


        Object char_literal220_tree=null;
        RewriteRuleTokenStream stream_111=new RewriteRuleTokenStream(adaptor,"token 111");
        RewriteRuleSubtreeStream stream_typeCast2=new RewriteRuleSubtreeStream(adaptor,"rule typeCast2");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:330:3: ( ( '.' typeCast2 )+ -> ^( TYPECAST2LIST ( typeCast2 )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:330:5: ( '.' typeCast2 )+
            {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:330:5: ( '.' typeCast2 )+
            int cnt46=0;
            loop46:
            do {
                int alt46=2;
                int LA46_0 = input.LA(1);

                if ( (LA46_0==111) ) {
                    alt46=1;
                }


                switch (alt46) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:330:6: '.' typeCast2
            	    {
            	    char_literal220=(Token)match(input,111,FOLLOW_111_in_typeCast2List2231);  
            	    stream_111.add(char_literal220);


            	    pushFollow(FOLLOW_typeCast2_in_typeCast2List2233);
            	    typeCast2221=typeCast2();

            	    state._fsp--;

            	    stream_typeCast2.add(typeCast2221.getTree());

            	    }
            	    break;

            	default :
            	    if ( cnt46 >= 1 ) break loop46;
                        EarlyExitException eee =
                            new EarlyExitException(46, input);
                        throw eee;
                }
                cnt46++;
            } while (true);


            // AST REWRITE
            // elements: typeCast2
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 330:22: -> ^( TYPECAST2LIST ( typeCast2 )+ )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:330:24: ^( TYPECAST2LIST ( typeCast2 )+ )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(TYPECAST2LIST, "TYPECAST2LIST")
                , root_1);

                if ( !(stream_typeCast2.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_typeCast2.hasNext() ) {
                    adaptor.addChild(root_1, stream_typeCast2.nextTree());

                }
                stream_typeCast2.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "typeCast2List"


    public static class objectInvocation2_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "objectInvocation2"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:333:1: objectInvocation2 : typeCast1 typeCast2List -> ^( FUNC_EXP typeCast1 typeCast2List ) ;
    public final BarsicParser.objectInvocation2_return objectInvocation2() throws RecognitionException {
        BarsicParser.objectInvocation2_return retval = new BarsicParser.objectInvocation2_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        BarsicParser.typeCast1_return typeCast1222 =null;

        BarsicParser.typeCast2List_return typeCast2List223 =null;


        RewriteRuleSubtreeStream stream_typeCast1=new RewriteRuleSubtreeStream(adaptor,"rule typeCast1");
        RewriteRuleSubtreeStream stream_typeCast2List=new RewriteRuleSubtreeStream(adaptor,"rule typeCast2List");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:334:3: ( typeCast1 typeCast2List -> ^( FUNC_EXP typeCast1 typeCast2List ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:334:6: typeCast1 typeCast2List
            {
            pushFollow(FOLLOW_typeCast1_in_objectInvocation22258);
            typeCast1222=typeCast1();

            state._fsp--;

            stream_typeCast1.add(typeCast1222.getTree());

            pushFollow(FOLLOW_typeCast2List_in_objectInvocation22260);
            typeCast2List223=typeCast2List();

            state._fsp--;

            stream_typeCast2List.add(typeCast2List223.getTree());

            // AST REWRITE
            // elements: typeCast2List, typeCast1
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 334:32: -> ^( FUNC_EXP typeCast1 typeCast2List )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:334:35: ^( FUNC_EXP typeCast1 typeCast2List )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(FUNC_EXP, "FUNC_EXP")
                , root_1);

                adaptor.addChild(root_1, stream_typeCast1.nextTree());

                adaptor.addChild(root_1, stream_typeCast2List.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "objectInvocation2"


    public static class arrayCall_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "arrayCall"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:342:1: arrayCall : Identifier '[' exprList ']' -> ^( ARRAYCALL Identifier exprList ) ;
    public final BarsicParser.arrayCall_return arrayCall() throws RecognitionException {
        BarsicParser.arrayCall_return retval = new BarsicParser.arrayCall_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Identifier224=null;
        Token char_literal225=null;
        Token char_literal227=null;
        BarsicParser.exprList_return exprList226 =null;


        Object Identifier224_tree=null;
        Object char_literal225_tree=null;
        Object char_literal227_tree=null;
        RewriteRuleTokenStream stream_122=new RewriteRuleTokenStream(adaptor,"token 122");
        RewriteRuleTokenStream stream_123=new RewriteRuleTokenStream(adaptor,"token 123");
        RewriteRuleTokenStream stream_Identifier=new RewriteRuleTokenStream(adaptor,"token Identifier");
        RewriteRuleSubtreeStream stream_exprList=new RewriteRuleSubtreeStream(adaptor,"rule exprList");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:343:2: ( Identifier '[' exprList ']' -> ^( ARRAYCALL Identifier exprList ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:343:4: Identifier '[' exprList ']'
            {
            Identifier224=(Token)match(input,Identifier,FOLLOW_Identifier_in_arrayCall2296);  
            stream_Identifier.add(Identifier224);


            char_literal225=(Token)match(input,122,FOLLOW_122_in_arrayCall2298);  
            stream_122.add(char_literal225);


            pushFollow(FOLLOW_exprList_in_arrayCall2299);
            exprList226=exprList();

            state._fsp--;

            stream_exprList.add(exprList226.getTree());

            char_literal227=(Token)match(input,123,FOLLOW_123_in_arrayCall2300);  
            stream_123.add(char_literal227);


            // AST REWRITE
            // elements: exprList, Identifier
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 343:30: -> ^( ARRAYCALL Identifier exprList )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:343:33: ^( ARRAYCALL Identifier exprList )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(ARRAYCALL, "ARRAYCALL")
                , root_1);

                adaptor.addChild(root_1, 
                stream_Identifier.nextNode()
                );

                adaptor.addChild(root_1, stream_exprList.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "arrayCall"


    public static class array_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "array"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:346:1: array : New 'Array' '[' sizes ']' Of Type -> ^( ARRAY sizes Type ) ;
    public final BarsicParser.array_return array() throws RecognitionException {
        BarsicParser.array_return retval = new BarsicParser.array_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token New228=null;
        Token string_literal229=null;
        Token char_literal230=null;
        Token char_literal232=null;
        Token Of233=null;
        Token Type234=null;
        BarsicParser.sizes_return sizes231 =null;


        Object New228_tree=null;
        Object string_literal229_tree=null;
        Object char_literal230_tree=null;
        Object char_literal232_tree=null;
        Object Of233_tree=null;
        Object Type234_tree=null;
        RewriteRuleTokenStream stream_Array=new RewriteRuleTokenStream(adaptor,"token Array");
        RewriteRuleTokenStream stream_New=new RewriteRuleTokenStream(adaptor,"token New");
        RewriteRuleTokenStream stream_122=new RewriteRuleTokenStream(adaptor,"token 122");
        RewriteRuleTokenStream stream_123=new RewriteRuleTokenStream(adaptor,"token 123");
        RewriteRuleTokenStream stream_Type=new RewriteRuleTokenStream(adaptor,"token Type");
        RewriteRuleTokenStream stream_Of=new RewriteRuleTokenStream(adaptor,"token Of");
        RewriteRuleSubtreeStream stream_sizes=new RewriteRuleSubtreeStream(adaptor,"rule sizes");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:347:2: ( New 'Array' '[' sizes ']' Of Type -> ^( ARRAY sizes Type ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:347:4: New 'Array' '[' sizes ']' Of Type
            {
            New228=(Token)match(input,New,FOLLOW_New_in_array2321);  
            stream_New.add(New228);


            string_literal229=(Token)match(input,Array,FOLLOW_Array_in_array2323);  
            stream_Array.add(string_literal229);


            char_literal230=(Token)match(input,122,FOLLOW_122_in_array2325);  
            stream_122.add(char_literal230);


            pushFollow(FOLLOW_sizes_in_array2326);
            sizes231=sizes();

            state._fsp--;

            stream_sizes.add(sizes231.getTree());

            char_literal232=(Token)match(input,123,FOLLOW_123_in_array2327);  
            stream_123.add(char_literal232);


            Of233=(Token)match(input,Of,FOLLOW_Of_in_array2329);  
            stream_Of.add(Of233);


            Type234=(Token)match(input,Type,FOLLOW_Type_in_array2331);  
            stream_Type.add(Type234);


            // AST REWRITE
            // elements: Type, sizes
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 347:36: -> ^( ARRAY sizes Type )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:347:39: ^( ARRAY sizes Type )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(ARRAY, "ARRAY")
                , root_1);

                adaptor.addChild(root_1, stream_sizes.nextTree());

                adaptor.addChild(root_1, 
                stream_Type.nextNode()
                );

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "array"


    public static class sizes_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "sizes"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:350:1: sizes : size ( ',' size )* -> ^( SIZES ( size )+ ) ;
    public final BarsicParser.sizes_return sizes() throws RecognitionException {
        BarsicParser.sizes_return retval = new BarsicParser.sizes_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal236=null;
        BarsicParser.size_return size235 =null;

        BarsicParser.size_return size237 =null;


        Object char_literal236_tree=null;
        RewriteRuleTokenStream stream_109=new RewriteRuleTokenStream(adaptor,"token 109");
        RewriteRuleSubtreeStream stream_size=new RewriteRuleSubtreeStream(adaptor,"rule size");
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:351:2: ( size ( ',' size )* -> ^( SIZES ( size )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:351:4: size ( ',' size )*
            {
            pushFollow(FOLLOW_size_in_sizes2352);
            size235=size();

            state._fsp--;

            stream_size.add(size235.getTree());

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:351:9: ( ',' size )*
            loop47:
            do {
                int alt47=2;
                int LA47_0 = input.LA(1);

                if ( (LA47_0==109) ) {
                    alt47=1;
                }


                switch (alt47) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:351:10: ',' size
            	    {
            	    char_literal236=(Token)match(input,109,FOLLOW_109_in_sizes2355);  
            	    stream_109.add(char_literal236);


            	    pushFollow(FOLLOW_size_in_sizes2357);
            	    size237=size();

            	    state._fsp--;

            	    stream_size.add(size237.getTree());

            	    }
            	    break;

            	default :
            	    break loop47;
                }
            } while (true);


            // AST REWRITE
            // elements: size
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 351:21: -> ^( SIZES ( size )+ )
            {
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:351:24: ^( SIZES ( size )+ )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(
                (Object)adaptor.create(SIZES, "SIZES")
                , root_1);

                if ( !(stream_size.hasNext()) ) {
                    throw new RewriteEarlyExitException();
                }
                while ( stream_size.hasNext() ) {
                    adaptor.addChild(root_1, stream_size.nextTree());

                }
                stream_size.reset();

                adaptor.addChild(root_0, root_1);
                }

            }


            retval.tree = root_0;

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "sizes"


    public static class nameClass_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "nameClass"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:354:1: nameClass : ( Array -> ^( NAMECLASS Array ) | Form -> ^( NAMECLASS Form ) | Subwindow -> ^( NAMECLASS Subwindow ) | Button -> ^( NAMECLASS Button ) | File -> ^( NAMECLASS File ) | Panel -> ^( NAMECLASS Panel ) | TextLabel -> ^( NAMECLASS TextLabel ) | TextField -> ^( NAMECLASS TextField ) );
    public final BarsicParser.nameClass_return nameClass() throws RecognitionException {
        BarsicParser.nameClass_return retval = new BarsicParser.nameClass_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token Array238=null;
        Token Form239=null;
        Token Subwindow240=null;
        Token Button241=null;
        Token File242=null;
        Token Panel243=null;
        Token TextLabel244=null;
        Token TextField245=null;

        Object Array238_tree=null;
        Object Form239_tree=null;
        Object Subwindow240_tree=null;
        Object Button241_tree=null;
        Object File242_tree=null;
        Object Panel243_tree=null;
        Object TextLabel244_tree=null;
        Object TextField245_tree=null;
        RewriteRuleTokenStream stream_Array=new RewriteRuleTokenStream(adaptor,"token Array");
        RewriteRuleTokenStream stream_Panel=new RewriteRuleTokenStream(adaptor,"token Panel");
        RewriteRuleTokenStream stream_TextLabel=new RewriteRuleTokenStream(adaptor,"token TextLabel");
        RewriteRuleTokenStream stream_Form=new RewriteRuleTokenStream(adaptor,"token Form");
        RewriteRuleTokenStream stream_Button=new RewriteRuleTokenStream(adaptor,"token Button");
        RewriteRuleTokenStream stream_Subwindow=new RewriteRuleTokenStream(adaptor,"token Subwindow");
        RewriteRuleTokenStream stream_TextField=new RewriteRuleTokenStream(adaptor,"token TextField");
        RewriteRuleTokenStream stream_File=new RewriteRuleTokenStream(adaptor,"token File");

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:355:2: ( Array -> ^( NAMECLASS Array ) | Form -> ^( NAMECLASS Form ) | Subwindow -> ^( NAMECLASS Subwindow ) | Button -> ^( NAMECLASS Button ) | File -> ^( NAMECLASS File ) | Panel -> ^( NAMECLASS Panel ) | TextLabel -> ^( NAMECLASS TextLabel ) | TextField -> ^( NAMECLASS TextField ) )
            int alt48=8;
            switch ( input.LA(1) ) {
            case Array:
                {
                alt48=1;
                }
                break;
            case Form:
                {
                alt48=2;
                }
                break;
            case Subwindow:
                {
                alt48=3;
                }
                break;
            case Button:
                {
                alt48=4;
                }
                break;
            case File:
                {
                alt48=5;
                }
                break;
            case Panel:
                {
                alt48=6;
                }
                break;
            case TextLabel:
                {
                alt48=7;
                }
                break;
            case TextField:
                {
                alt48=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 48, 0, input);

                throw nvae;

            }

            switch (alt48) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:355:4: Array
                    {
                    Array238=(Token)match(input,Array,FOLLOW_Array_in_nameClass2381);  
                    stream_Array.add(Array238);


                    // AST REWRITE
                    // elements: Array
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 355:11: -> ^( NAMECLASS Array )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:355:14: ^( NAMECLASS Array )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(NAMECLASS, "NAMECLASS")
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_Array.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:356:4: Form
                    {
                    Form239=(Token)match(input,Form,FOLLOW_Form_in_nameClass2395);  
                    stream_Form.add(Form239);


                    // AST REWRITE
                    // elements: Form
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 356:10: -> ^( NAMECLASS Form )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:356:13: ^( NAMECLASS Form )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(NAMECLASS, "NAMECLASS")
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_Form.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 3 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:357:4: Subwindow
                    {
                    Subwindow240=(Token)match(input,Subwindow,FOLLOW_Subwindow_in_nameClass2409);  
                    stream_Subwindow.add(Subwindow240);


                    // AST REWRITE
                    // elements: Subwindow
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 357:15: -> ^( NAMECLASS Subwindow )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:357:20: ^( NAMECLASS Subwindow )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(NAMECLASS, "NAMECLASS")
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_Subwindow.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 4 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:358:4: Button
                    {
                    Button241=(Token)match(input,Button,FOLLOW_Button_in_nameClass2425);  
                    stream_Button.add(Button241);


                    // AST REWRITE
                    // elements: Button
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 358:13: -> ^( NAMECLASS Button )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:358:20: ^( NAMECLASS Button )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(NAMECLASS, "NAMECLASS")
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_Button.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 5 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:359:4: File
                    {
                    File242=(Token)match(input,File,FOLLOW_File_in_nameClass2446);  
                    stream_File.add(File242);


                    // AST REWRITE
                    // elements: File
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 359:9: -> ^( NAMECLASS File )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:359:16: ^( NAMECLASS File )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(NAMECLASS, "NAMECLASS")
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_File.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 6 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:360:4: Panel
                    {
                    Panel243=(Token)match(input,Panel,FOLLOW_Panel_in_nameClass2465);  
                    stream_Panel.add(Panel243);


                    // AST REWRITE
                    // elements: Panel
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 360:10: -> ^( NAMECLASS Panel )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:360:17: ^( NAMECLASS Panel )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(NAMECLASS, "NAMECLASS")
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_Panel.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 7 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:361:4: TextLabel
                    {
                    TextLabel244=(Token)match(input,TextLabel,FOLLOW_TextLabel_in_nameClass2484);  
                    stream_TextLabel.add(TextLabel244);


                    // AST REWRITE
                    // elements: TextLabel
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 361:14: -> ^( NAMECLASS TextLabel )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:361:17: ^( NAMECLASS TextLabel )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(NAMECLASS, "NAMECLASS")
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_TextLabel.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;
                case 8 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:362:4: TextField
                    {
                    TextField245=(Token)match(input,TextField,FOLLOW_TextField_in_nameClass2497);  
                    stream_TextField.add(TextField245);


                    // AST REWRITE
                    // elements: TextField
                    // token labels: 
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 362:14: -> ^( NAMECLASS TextField )
                    {
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:362:17: ^( NAMECLASS TextField )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(
                        (Object)adaptor.create(NAMECLASS, "NAMECLASS")
                        , root_1);

                        adaptor.addChild(root_1, 
                        stream_TextField.nextNode()
                        );

                        adaptor.addChild(root_0, root_1);
                        }

                    }


                    retval.tree = root_0;

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "nameClass"

    // Delegated rules


    protected DFA3 dfa3 = new DFA3(this);
    protected DFA19 dfa19 = new DFA19(this);
    static final String DFA3_eotS =
        "\45\uffff";
    static final String DFA3_eofS =
        "\45\uffff";
    static final String DFA3_minS =
        "\2\11\10\151\6\uffff\4\11\10\151\2\uffff\1\146\1\11\1\157\1\146"+
        "\1\11\1\157\1\11";
    static final String DFA3_maxS =
        "\1\144\1\172\10\151\6\uffff\1\176\2\156\1\165\10\151\2\uffff\1\u0080"+
        "\1\156\1\165\1\u0080\1\176\2\165";
    static final String DFA3_acceptS =
        "\12\uffff\1\4\1\5\1\6\1\10\1\1\1\3\14\uffff\1\2\1\7\7\uffff";
    static final String DFA3_specialS =
        "\45\uffff}>";
    static final String[] DFA3_transitionS = {
            "\1\2\4\uffff\1\5\34\uffff\1\6\1\13\1\3\11\uffff\1\1\1\12\21"+
            "\uffff\1\7\1\uffff\1\15\10\uffff\1\4\10\uffff\1\11\1\10\4\uffff"+
            "\1\14",
            "\1\16\4\uffff\1\16\34\uffff\1\16\1\uffff\1\16\11\uffff\1\16"+
            "\22\uffff\1\16\12\uffff\1\16\10\uffff\2\16\11\uffff\1\17\5\uffff"+
            "\1\20\5\uffff\1\21\4\uffff\1\16",
            "\1\22",
            "\1\22",
            "\1\22",
            "\1\22",
            "\1\22",
            "\1\22",
            "\1\22",
            "\1\22",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\24\4\uffff\1\27\34\uffff\1\30\1\uffff\1\25\11\uffff\1\23"+
            "\22\uffff\1\31\12\uffff\1\26\10\uffff\1\33\1\32\36\uffff\1\34",
            "\1\16\3\uffff\2\16\11\uffff\1\35\22\uffff\1\16\1\uffff\1\16"+
            "\11\uffff\1\16\12\uffff\4\16\4\uffff\1\16\11\uffff\2\16\10\uffff"+
            "\2\16\5\uffff\1\16\10\uffff\1\16",
            "\1\34\3\uffff\2\34\34\uffff\1\34\1\uffff\1\34\11\uffff\1\36"+
            "\12\uffff\4\34\4\uffff\1\34\11\uffff\2\34\10\uffff\2\34\5\uffff"+
            "\1\34\10\uffff\1\34",
            "\1\34\4\uffff\1\34\34\uffff\1\34\1\uffff\1\34\11\uffff\1\34"+
            "\22\uffff\1\34\12\uffff\1\34\10\uffff\2\34\11\uffff\1\34\5\uffff"+
            "\1\20\5\uffff\1\21",
            "\1\37",
            "\1\37",
            "\1\37",
            "\1\37",
            "\1\37",
            "\1\37",
            "\1\37",
            "\1\37",
            "",
            "",
            "\4\34\1\40\2\34\1\uffff\3\34\2\uffff\2\34\1\uffff\5\34\1\uffff"+
            "\1\34\3\uffff\1\34",
            "\1\34\3\uffff\2\34\34\uffff\1\34\1\uffff\1\34\11\uffff\1\41"+
            "\12\uffff\4\34\4\uffff\1\34\11\uffff\2\34\10\uffff\2\34\5\uffff"+
            "\1\34\10\uffff\1\34",
            "\1\42\5\uffff\1\16",
            "\4\34\1\43\2\34\1\uffff\3\34\2\uffff\2\34\1\uffff\5\34\1\uffff"+
            "\1\34\3\uffff\1\34",
            "\1\24\4\uffff\1\27\34\uffff\1\30\1\uffff\1\25\11\uffff\1\44"+
            "\22\uffff\1\31\12\uffff\1\26\10\uffff\1\33\1\32\36\uffff\1\34",
            "\1\42\5\uffff\1\16",
            "\1\34\4\uffff\1\34\34\uffff\1\34\1\uffff\1\34\11\uffff\1\34"+
            "\22\uffff\1\34\12\uffff\1\34\10\uffff\2\34\11\uffff\1\34\5\uffff"+
            "\1\42\5\uffff\1\16"
    };

    static final short[] DFA3_eot = DFA.unpackEncodedString(DFA3_eotS);
    static final short[] DFA3_eof = DFA.unpackEncodedString(DFA3_eofS);
    static final char[] DFA3_min = DFA.unpackEncodedStringToUnsignedChars(DFA3_minS);
    static final char[] DFA3_max = DFA.unpackEncodedStringToUnsignedChars(DFA3_maxS);
    static final short[] DFA3_accept = DFA.unpackEncodedString(DFA3_acceptS);
    static final short[] DFA3_special = DFA.unpackEncodedString(DFA3_specialS);
    static final short[][] DFA3_transition;

    static {
        int numStates = DFA3_transitionS.length;
        DFA3_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA3_transition[i] = DFA.unpackEncodedString(DFA3_transitionS[i]);
        }
    }

    class DFA3 extends DFA {

        public DFA3(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 3;
            this.eot = DFA3_eot;
            this.eof = DFA3_eof;
            this.min = DFA3_min;
            this.max = DFA3_max;
            this.accept = DFA3_accept;
            this.special = DFA3_special;
            this.transition = DFA3_transition;
        }
        public String getDescription() {
            return "89:1: statement : ( assignment ';' -> assignment | objectInvocation ';' -> objectInvocation | functionCall ';' -> functionCall | ifStatement | forStatement | whileStatement | caseAssigment | procedure ';' -> procedure );";
        }
    }
    static final String DFA19_eotS =
        "\6\uffff";
    static final String DFA19_eofS =
        "\6\uffff";
    static final String DFA19_minS =
        "\1\11\1\57\1\uffff\1\11\1\uffff\1\57";
    static final String DFA19_maxS =
        "\1\156\1\u0080\1\uffff\1\137\1\uffff\1\u0080";
    static final String DFA19_acceptS =
        "\2\uffff\1\2\1\uffff\1\1\1\uffff";
    static final String DFA19_specialS =
        "\6\uffff}>";
    static final String[] DFA19_transitionS = {
            "\1\2\3\uffff\2\2\34\uffff\1\2\1\uffff\1\2\11\uffff\1\1\12\uffff"+
            "\4\2\4\uffff\1\2\11\uffff\2\2\10\uffff\2\2\5\uffff\1\2\10\uffff"+
            "\1\2",
            "\1\2\12\uffff\1\4\53\uffff\4\2\1\uffff\2\2\1\uffff\1\2\1\3"+
            "\1\2\2\uffff\2\2\1\uffff\5\2\1\uffff\1\2\3\uffff\1\2",
            "",
            "\1\2\4\uffff\1\2\34\uffff\1\2\1\uffff\1\2\11\uffff\1\5\22\uffff"+
            "\1\2\12\uffff\1\2\10\uffff\2\2",
            "",
            "\1\2\12\uffff\1\4\53\uffff\4\2\1\uffff\2\2\1\uffff\1\2\1\3"+
            "\1\2\2\uffff\2\2\1\uffff\4\2\2\uffff\1\2\3\uffff\1\2"
    };

    static final short[] DFA19_eot = DFA.unpackEncodedString(DFA19_eotS);
    static final short[] DFA19_eof = DFA.unpackEncodedString(DFA19_eofS);
    static final char[] DFA19_min = DFA.unpackEncodedStringToUnsignedChars(DFA19_minS);
    static final char[] DFA19_max = DFA.unpackEncodedStringToUnsignedChars(DFA19_maxS);
    static final short[] DFA19_accept = DFA.unpackEncodedString(DFA19_acceptS);
    static final short[] DFA19_special = DFA.unpackEncodedString(DFA19_specialS);
    static final short[][] DFA19_transition;

    static {
        int numStates = DFA19_transitionS.length;
        DFA19_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA19_transition[i] = DFA.unpackEncodedString(DFA19_transitionS[i]);
        }
    }

    class DFA19 extends DFA {

        public DFA19(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 19;
            this.eot = DFA19_eot;
            this.eof = DFA19_eof;
            this.min = DFA19_min;
            this.max = DFA19_max;
            this.accept = DFA19_accept;
            this.special = DFA19_special;
            this.transition = DFA19_transition;
        }
        public String getDescription() {
            return "156:1: ifexpression : ( belongs | expression );";
        }
    }
 

    public static final BitSet FOLLOW_block_in_parse300 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_parse302 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_statement_in_block323 = new BitSet(new long[]{0x0180380000004202L,0x00000010C0205400L});
    public static final BitSet FOLLOW_Return_in_block328 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_block330 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});
    public static final BitSet FOLLOW_114_in_block332 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_assignment_in_statement373 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});
    public static final BitSet FOLLOW_114_in_statement375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_objectInvocation_in_statement388 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});
    public static final BitSet FOLLOW_114_in_statement390 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_functionCall_in_statement401 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});
    public static final BitSet FOLLOW_114_in_statement403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ifStatement_in_statement414 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_forStatement_in_statement421 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_whileStatement_in_statement428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_caseAssigment_in_statement435 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_procedure_in_statement442 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});
    public static final BitSet FOLLOW_114_in_statement444 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Identifier_in_functionCall461 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
    public static final BitSet FOLLOW_105_in_functionCall463 = new BitSet(new long[]{0x0080280000006200L,0x00004420C030043CL});
    public static final BitSet FOLLOW_exprList_in_functionCall465 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
    public static final BitSet FOLLOW_106_in_functionCall468 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Procedure_in_procedure493 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_Identifier_in_procedure496 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
    public static final BitSet FOLLOW_105_in_procedure498 = new BitSet(new long[]{0x0080000000000000L,0x0000040000000000L});
    public static final BitSet FOLLOW_procedureParameter_in_procedure500 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
    public static final BitSet FOLLOW_106_in_procedure503 = new BitSet(new long[]{0x0200000000001000L,0x0000000800000020L});
    public static final BitSet FOLLOW_Import_in_procedure508 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_identifiertList_in_procedure513 = new BitSet(new long[]{0x0000000000001000L,0x0000000800000020L});
    public static final BitSet FOLLOW_Variables_in_procedure519 = new BitSet(new long[]{0x0080280000004200L,0x00000000C0200400L});
    public static final BitSet FOLLOW_paramList_in_procedure523 = new BitSet(new long[]{0x0000000000001000L,0x0000000000000020L});
    public static final BitSet FOLLOW_Begin_in_procedure530 = new BitSet(new long[]{0x0180380800204200L,0x00000010C0205400L});
    public static final BitSet FOLLOW_OBrace_in_procedure534 = new BitSet(new long[]{0x0180380800204200L,0x00000010C0205400L});
    public static final BitSet FOLLOW_block_in_procedure540 = new BitSet(new long[]{0x0000000800200000L});
    public static final BitSet FOLLOW_End_in_procedure545 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CBrace_in_procedure549 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_identifiertList_and_type_in_procedureParameter588 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_109_in_procedureParameter591 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_identifiertList_and_type_in_procedureParameter593 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_identifiertList_in_identifiertList_and_type618 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_identifiertList_and_type620 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
    public static final BitSet FOLLOW_Type_in_identifiertList_and_type622 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Identifier_in_identifier647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Identifier_in_identifiertList668 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_109_in_identifiertList672 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_Identifier_in_identifiertList673 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_identifier_in_assignment696 = new BitSet(new long[]{0x0080280000004200L,0x00000000C0200400L});
    public static final BitSet FOLLOW_paramList_in_assignment698 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_CloseIdentifier_in_assignment700 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_lookuparr_in_assignment720 = new BitSet(new long[]{0x0000000000000000L,0x0020000000000000L});
    public static final BitSet FOLLOW_117_in_assignment722 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_assignment724 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_assignment_in_paramList748 = new BitSet(new long[]{0x0080280000004202L,0x00002000C0200400L});
    public static final BitSet FOLLOW_109_in_paramList752 = new BitSet(new long[]{0x0080280000004200L,0x00000000C0200400L});
    public static final BitSet FOLLOW_assignment_in_paramList754 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_ifStat_in_ifStatement778 = new BitSet(new long[]{0x0000004400000000L});
    public static final BitSet FOLLOW_elseStat_in_ifStatement780 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_EndIf_in_ifStatement784 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});
    public static final BitSet FOLLOW_114_in_ifStatement786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_If_in_ifStatement804 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_ifsStat_in_ifStatement807 = new BitSet(new long[]{0x0000004400000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_elseStat_in_ifStatement810 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_EndIf_in_ifStatement814 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});
    public static final BitSet FOLLOW_114_in_ifStatement816 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_If_in_ifStat842 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_ifexpression_in_ifStat844 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_Hence_in_ifStat846 = new BitSet(new long[]{0x0180380000004200L,0x00000010C0205400L});
    public static final BitSet FOLLOW_block_in_ifStat848 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_127_in_ifsStat871 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_ifexpression_in_ifsStat873 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_Hence_in_ifsStat875 = new BitSet(new long[]{0x0180380000004200L,0x00000010C0205400L});
    public static final BitSet FOLLOW_block_in_ifsStat877 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Else_in_elseStat901 = new BitSet(new long[]{0x0180380000004200L,0x00000010C0205400L});
    public static final BitSet FOLLOW_block_in_elseStat903 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_belongs_in_ifexpression924 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_ifexpression930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_lookup_in_belongs943 = new BitSet(new long[]{0x0400000000000000L});
    public static final BitSet FOLLOW_In_in_belongs945 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_122_in_belongs947 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_sizeBelongs_in_belongs950 = new BitSet(new long[]{0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_123_in_belongs952 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_size_in_sizeBelongs977 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numList_in_sizeBelongs984 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Number_in_size996 = new BitSet(new long[]{0x0000000000000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_To_in_size998 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_Number_in_size1000 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Number_in_numList1021 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_109_in_numList1024 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_Number_in_numList1026 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_lookup_in_caseAssigment1050 = new BitSet(new long[]{0x0000000000000000L,0x0020000000000000L});
    public static final BitSet FOLLOW_117_in_caseAssigment1052 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_caseStatement_in_caseAssigment1054 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});
    public static final BitSet FOLLOW_114_in_caseAssigment1056 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_caseStat_in_caseStatement1082 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_elsecaseStat_in_caseStatement1084 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_EndCase_in_caseStatement1087 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Case_in_caseStatement1104 = new BitSet(new long[]{0x0000000000000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_casesStat_in_caseStatement1107 = new BitSet(new long[]{0x0000000400000000L,0x8000000000000000L});
    public static final BitSet FOLLOW_elsecaseStat_in_caseStatement1110 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_EndCase_in_caseStatement1113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Case_in_caseStat1139 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_caseStat1141 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_Hence_in_caseStat1143 = new BitSet(new long[]{0x0080280001006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_caseStatOrExpr_in_caseStat1145 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Else_in_elsecaseStat1169 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_elsecaseStat1171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_127_in_casesStat1192 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_casesStat1194 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_Hence_in_casesStat1196 = new BitSet(new long[]{0x0080280001006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_caseStatOrExpr_in_casesStat1198 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_caseStatement_in_caseStatOrExpr1222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_caseStatOrExpr1229 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_For_in_forStatement1243 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_Identifier_in_forStatement1245 = new BitSet(new long[]{0x0000000000000000L,0x0020000000000000L});
    public static final BitSet FOLLOW_117_in_forStatement1247 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_forStatement1249 = new BitSet(new long[]{0x0000000000000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_To_in_forStatement1251 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_forStatement1253 = new BitSet(new long[]{0x0000000040000000L,0x0000200000000000L});
    public static final BitSet FOLLOW_109_in_forStatement1256 = new BitSet(new long[]{0x0000000000000000L,0x0000000000080000L});
    public static final BitSet FOLLOW_Step_in_forStatement1258 = new BitSet(new long[]{0x0000000000000000L,0x0020000000000000L});
    public static final BitSet FOLLOW_117_in_forStatement1260 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_forStatement1262 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_Do_in_forStatement1266 = new BitSet(new long[]{0x0180382000004200L,0x00000010C0205400L});
    public static final BitSet FOLLOW_block_in_forStatement1268 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_EndDo_in_forStatement1270 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});
    public static final BitSet FOLLOW_114_in_forStatement1271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_While_in_whileStatement1307 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_whileStatement1309 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_Do_in_whileStatement1311 = new BitSet(new long[]{0x0180382000004200L,0x00000010C0205400L});
    public static final BitSet FOLLOW_block_in_whileStatement1313 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_EndDo_in_whileStatement1315 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});
    public static final BitSet FOLLOW_114_in_whileStatement1317 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_condExpr_in_expression1343 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_New_in_expression1349 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_Form_in_expression1351 = new BitSet(new long[]{0x0080280000004200L,0x00000000C0200400L});
    public static final BitSet FOLLOW_paramList_in_expression1353 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_CloseIdentifier_in_expression1355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_OBrace_in_expression1371 = new BitSet(new long[]{0x0180380000204200L,0x00000010C0205400L});
    public static final BitSet FOLLOW_block_in_expression1373 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_CBrace_in_expression1375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_array_in_expression1389 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_exprList1411 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_109_in_exprList1414 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_exprList1416 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_orExpr_in_condExpr1442 = new BitSet(new long[]{0x0000000000000002L,0x0200000000000000L});
    public static final BitSet FOLLOW_121_in_condExpr1456 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_condExpr1460 = new BitSet(new long[]{0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_113_in_condExpr1462 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_condExpr1466 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_andExpr_in_orExpr1496 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_128_in_orExpr1499 = new BitSet(new long[]{0x0080280000006200L,0x00004020C0300418L});
    public static final BitSet FOLLOW_andExpr_in_orExpr1502 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_equExpr_in_andExpr1518 = new BitSet(new long[]{0x0000000000000002L,0x0000010000000000L});
    public static final BitSet FOLLOW_104_in_andExpr1521 = new BitSet(new long[]{0x0080280000006200L,0x00004020C0300418L});
    public static final BitSet FOLLOW_equExpr_in_andExpr1524 = new BitSet(new long[]{0x0000000000000002L,0x0000010000000000L});
    public static final BitSet FOLLOW_relExpr_in_equExpr1540 = new BitSet(new long[]{0x0000000000000002L,0x0040004000000000L});
    public static final BitSet FOLLOW_set_in_equExpr1543 = new BitSet(new long[]{0x0080280000006200L,0x00004020C0300418L});
    public static final BitSet FOLLOW_relExpr_in_equExpr1552 = new BitSet(new long[]{0x0000000000000002L,0x0040004000000000L});
    public static final BitSet FOLLOW_addExpr_in_relExpr1568 = new BitSet(new long[]{0x0000000000000002L,0x0198000000000000L});
    public static final BitSet FOLLOW_set_in_relExpr1571 = new BitSet(new long[]{0x0080280000006200L,0x00004020C0300418L});
    public static final BitSet FOLLOW_addExpr_in_relExpr1588 = new BitSet(new long[]{0x0000000000000002L,0x0198000000000000L});
    public static final BitSet FOLLOW_mulExpr_in_addExpr1604 = new BitSet(new long[]{0x0000000000000002L,0x0000500000000000L});
    public static final BitSet FOLLOW_set_in_addExpr1607 = new BitSet(new long[]{0x0080280000006200L,0x00004020C0300418L});
    public static final BitSet FOLLOW_mulExpr_in_addExpr1616 = new BitSet(new long[]{0x0000000000000002L,0x0000500000000000L});
    public static final BitSet FOLLOW_powExpr_in_mulExpr1632 = new BitSet(new long[]{0x0000000000000002L,0x0001088000000000L});
    public static final BitSet FOLLOW_set_in_mulExpr1635 = new BitSet(new long[]{0x0080280000006200L,0x00004020C0300418L});
    public static final BitSet FOLLOW_powExpr_in_mulExpr1648 = new BitSet(new long[]{0x0000000000000002L,0x0001088000000000L});
    public static final BitSet FOLLOW_unaryExpr_in_powExpr1664 = new BitSet(new long[]{0x0000000000000002L,0x1000000000000000L});
    public static final BitSet FOLLOW_124_in_powExpr1667 = new BitSet(new long[]{0x0080280000006200L,0x00004020C0300418L});
    public static final BitSet FOLLOW_unaryExpr_in_powExpr1669 = new BitSet(new long[]{0x0000000000000002L,0x1000000000000000L});
    public static final BitSet FOLLOW_110_in_unaryExpr1685 = new BitSet(new long[]{0x0000000000002000L,0x0000000000100018L});
    public static final BitSet FOLLOW_atom_in_unaryExpr1687 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_101_in_unaryExpr1702 = new BitSet(new long[]{0x0000000000002000L,0x0000000000100018L});
    public static final BitSet FOLLOW_atom_in_unaryExpr1704 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_atom_in_unaryExpr1719 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_identifier_in_unaryExpr1726 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_arrayCall_in_unaryExpr1734 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_functionCall_in_unaryExpr1741 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_nameClass_in_unaryExpr1756 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_objectInvocation2_in_unaryExpr1764 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Identifier_in_idList1813 = new BitSet(new long[]{0x0000000000000002L,0x0000800000000000L});
    public static final BitSet FOLLOW_111_in_idList1816 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_Identifier_in_idList1818 = new BitSet(new long[]{0x0000000000000002L,0x0000800000000000L});
    public static final BitSet FOLLOW_idForm_in_idFormList1844 = new BitSet(new long[]{0x0000000000000002L,0x0000800000000000L});
    public static final BitSet FOLLOW_111_in_idFormList1847 = new BitSet(new long[]{0x0080280000004200L,0x00000000C0200400L});
    public static final BitSet FOLLOW_idForm_in_idFormList1849 = new BitSet(new long[]{0x0000000000000002L,0x0000800000000000L});
    public static final BitSet FOLLOW_Identifier_in_idForm1881 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_nameClass_in_idForm1893 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
    public static final BitSet FOLLOW_105_in_idForm1895 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_Identifier_in_idForm1896 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
    public static final BitSet FOLLOW_106_in_idForm1897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_idFormList_in_formlookup1918 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_idList_in_lookup1944 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_formlookup_in_lookuparr1970 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_arrayCall_in_lookuparr1976 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_typeCast1_in_objectInvocation1997 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
    public static final BitSet FOLLOW_111_in_objectInvocation2001 = new BitSet(new long[]{0x0080280000004200L,0x00000000C0200400L});
    public static final BitSet FOLLOW_typeCast1_in_objectInvocation2003 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
    public static final BitSet FOLLOW_111_in_objectInvocation2008 = new BitSet(new long[]{0x0080000000000000L,0x4000000000000000L});
    public static final BitSet FOLLOW_objectInvocationMethod_in_objectInvocation2010 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Identifier_in_objectInvocationMethod2040 = new BitSet(new long[]{0x0080280000004200L,0x00000000C0200400L});
    public static final BitSet FOLLOW_paramList_in_objectInvocationMethod2042 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_CloseIdentifier_in_objectInvocationMethod2044 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_126_in_objectInvocationMethod2061 = new BitSet(new long[]{0x0180380000004200L,0x20000010C0205400L});
    public static final BitSet FOLLOW_block_in_objectInvocationMethod2063 = new BitSet(new long[]{0x0000000000000000L,0x2000000000000000L});
    public static final BitSet FOLLOW_125_in_objectInvocationMethod2065 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Identifier_in_objectInvocationMethod2081 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
    public static final BitSet FOLLOW_105_in_objectInvocationMethod2083 = new BitSet(new long[]{0x0080280000006200L,0x00004420C030043CL});
    public static final BitSet FOLLOW_exprList_in_objectInvocationMethod2085 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
    public static final BitSet FOLLOW_106_in_objectInvocationMethod2088 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_nameClass_in_typeCast12117 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
    public static final BitSet FOLLOW_105_in_typeCast12119 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_typeCast12121 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
    public static final BitSet FOLLOW_106_in_typeCast12123 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_identifier_in_typeCast12139 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_nameClass_in_typeCast22170 = new BitSet(new long[]{0x0000000000000000L,0x0000020000000000L});
    public static final BitSet FOLLOW_105_in_typeCast22172 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_expression_in_typeCast22174 = new BitSet(new long[]{0x0000000000000000L,0x0000040000000000L});
    public static final BitSet FOLLOW_106_in_typeCast22176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_functionCall_in_typeCast22190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_identifier_in_typeCast22205 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_111_in_typeCast2List2231 = new BitSet(new long[]{0x0080280000004200L,0x00000000C0200400L});
    public static final BitSet FOLLOW_typeCast2_in_typeCast2List2233 = new BitSet(new long[]{0x0000000000000002L,0x0000800000000000L});
    public static final BitSet FOLLOW_typeCast1_in_objectInvocation22258 = new BitSet(new long[]{0x0000000000000000L,0x0000800000000000L});
    public static final BitSet FOLLOW_typeCast2List_in_objectInvocation22260 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Identifier_in_arrayCall2296 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_122_in_arrayCall2298 = new BitSet(new long[]{0x0080280000006200L,0x00004020C030043CL});
    public static final BitSet FOLLOW_exprList_in_arrayCall2299 = new BitSet(new long[]{0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_123_in_arrayCall2300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_New_in_array2321 = new BitSet(new long[]{0x0000000000000200L});
    public static final BitSet FOLLOW_Array_in_array2323 = new BitSet(new long[]{0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_122_in_array2325 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_sizes_in_array2326 = new BitSet(new long[]{0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_123_in_array2327 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_Of_in_array2329 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
    public static final BitSet FOLLOW_Type_in_array2331 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_size_in_sizes2352 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_109_in_sizes2355 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_size_in_sizes2357 = new BitSet(new long[]{0x0000000000000002L,0x0000200000000000L});
    public static final BitSet FOLLOW_Array_in_nameClass2381 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Form_in_nameClass2395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Subwindow_in_nameClass2409 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Button_in_nameClass2425 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_File_in_nameClass2446 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Panel_in_nameClass2465 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TextLabel_in_nameClass2484 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TextField_in_nameClass2497 = new BitSet(new long[]{0x0000000000000002L});

}