tree grammar BarsicTreeWalker;

options {
  tokenVocab=Barsic;
  ASTLabelType=CommonTree;
}

@header {
  package barsic.parser;
  import barsic.Scope;
  import barsic.Function;
  import barsic.tree.form.*;
  import barsic.tree.BarsicValue;
  import barsic.tree.functions.*;
  import barsic.tree.primitives.*;
  import java.util.Map;
  import java.util.HashMap;
  import java.util.Arrays;
}

@members {
  public Map<String, Function> functions = null;
  Scope currentScope = null;
  
  public BarsicTreeWalker(CommonTreeNodeStream nodes, Map<String, Function> fns) {
    this(nodes, null, fns);
  }
  
  public BarsicTreeWalker(CommonTreeNodeStream nds, Scope sc, Map<String, Function> fns) {
    super(nds);
    currentScope = sc;
    functions = fns;
  }
}

walk returns [AstNode node]
  :  block {node = $block.node;}
  ;

block returns [AstNode node]
@init {
  BlockNode bn = new BlockNode();
  node = bn;
  Scope local = new Scope(currentScope);
  currentScope = local;
}
@after {
  currentScope = currentScope.parent();
}
  : ^(BLOCK ^(STATEMENTS (statement {bn.addStatement($statement.node);})*) ^(RETURN (expression {bn.addReturn($expression.node);})?))
  ;

statement returns [AstNode node]
  :  assignment     {node = $assignment.node;}
  |  objectInvocation   {node = $objectInvocation.node;}
  |  ifStatement    {node = $ifStatement.node;}
  |  forStatement   {node = $forStatement.node;}
  |  whileStatement {node = $whileStatement.node;}
  |  procedure      {node = $procedure.node;}
  |  functionCall   {node = $functionCall.node;}
  |  caseAssigment  {node = $caseAssigment.node;}
  ;
  
functionCall returns [AstNode node]
  :	^(FUNCTIONCALL Identifier exprList? )  { node = new FunctionCallNode($Identifier.text, $exprList.e, currentScope); }
  ;
	
procedure returns [AstNode node]
  :	^(PROCEDURE Identifier procedureParameter?   identifiertList?  paramList?  block )  { node = new ProcedureFuncNode($Identifier.text, $procedureParameter.e, $identifiertList.e, $paramList.e, $block.node); }
  ;

procedureParameter returns [java.util.List<IdentifiertList_and_typeNode> e]
  @init  {e = new java.util.ArrayList<IdentifiertList_and_typeNode>();}
  :	^(PROCEDUREPARAMETER (identifiertList_and_type {e.add((IdentifiertList_and_typeNode)$identifiertList_and_type.node);})+)
  ;

identifiertList_and_type  returns[AstNode node]
  :	^(IDENTIFIERLIST_AND_TYPE identifiertList Type){node = new IdentifiertList_and_typeNode ($identifiertList.e , $Type.text);}
  ;

identifiertList returns[java.util.List<String> e]
  @init  {e = new java.util.ArrayList<String>();}
  :	^(IDENTIFIER_LIST (Identifier {e.add($Identifier.text);})+)
  ;

assignment returns [AstNode node]
  :  ^(ASSIGNMENT lookuparr expression) {node = new AssignmentNode($lookuparr.node, $expression.node, null, currentScope);}
  |  ^(ASSIGNMENT identifier paramList) {node = new AssignmentNode($identifier.node, null, $paramList.e, currentScope);}
  ;
  
typeCast1 returns [AstNode node]
 :  ^(TYPECASTEXP expression) {node =  $expression.node;}
 |  ^(TYPECASTID  identifier) {node = $identifier.node;}
 ;
 
 typeCast2 returns [AstNode node]
 :  ^(TYPE1 expression){node =  $expression.node;}
 |  ^(TYPE2 functionCall){node = $functionCall.node;}
 |  ^(TYPE3  identifier) {node = $identifier.node;}
// |  ^( ){}
 ;
 
 typeCast2List returns [java.util.List<AstNode> e]
  @init  {e = new java.util.ArrayList<AstNode>();}
  :  ^(TYPECAST2LIST (typeCast2 {e.add($typeCast2.node);})+)
  ;

 objectInvocation2 returns [AstNode node]
  :  ^(FUNC_EXP typeCast1 typeCast2List ){node = new ObjectInvocation2Node($typeCast1.node , $typeCast2List.e); }
  ;
 
objectInvocationMethod returns [ObjectInvocationBase node]
  : ^(FUNC_CALL FormType Identifier exprList?) {node = new FunctionCallArguments($Identifier.text, $exprList.e);}
  | ^(FUNC_CALL FormType Identifier paramList)  {node = new ParameterListFuncCallArgument($Identifier.text, $paramList.e);}
  | ^(FUNC_CALL FormType block)  {node = new BlockFuncCallArgument("output", $block.node);}
  ;

objectInvocation returns [FormCallNode node]
  :  ^(FUNC_CALL FormType typeCast1 objectInvocationMethod)  { node = new FormCallNode($objectInvocationMethod.node, $typeCast1.node);}
  ;

ifStatement returns [AstNode node]
@init  {IfNode ifNode = new IfNode();}
@after {node = ifNode;}
  :  ^(IF1 ifStat[ifNode] elseStat[ifNode]?)
  |  ^(IF2 ifsStat[ifNode]+ elseStat[ifNode]?)
  ;

ifStat[IfNode parent]
  :  ^(EXP ifexpression block) {parent.addIf("if", $ifexpression.node, $block.node, currentScope);}
  ;

ifsStat[IfNode parent]
 :  ^(EXP  ifexpression block) {parent.addChoices("if", $ifexpression.node, $block.node, currentScope);}
 ;

elseStat[IfNode parent]
  :  ^(EXP block) {parent.addElse("else", $block.node, currentScope);}
  ;

ifexpression returns [AstNode node]
  :  belongs {node = $belongs.node;}
  |  expression {node = $expression.node;}
  ;

belongs returns [AstNode node]
  :  ^(BELONGS (lookup sizeBelongs {node = new BelongsNode($lookup.node, $sizeBelongs.e, currentScope);}))
  ;

sizeBelongs returns [java.util.List<String> e]
  :  size { e = Arrays.asList($size.text.split("\\.."));}
  |  numList {e = $numList.e;}
  ;

size returns [String text]
@init { java.lang.StringBuilder sb = new java.lang.StringBuilder(); }
@after {text = sb.toString();}
 :  ^(SIZE (a=Number b=Number  {sb = sb.append($a).append("..").append(b);}))
 ;

numList returns [java.util.List<String> e]
 @init {e = new java.util.ArrayList<String>();}
 :  ^(NUMLIST (Number {e.add($Number.text);})+)
 ;

caseAssigment returns [AstNode node]
  : ^(CASEASSIGMENT lookup caseStatement) {node = new CaseAssigmentNode($lookup.node, $caseStatement.node, currentScope);}
  ;

caseStatement returns [AstNode node]
@init  {CaseNode caseNode = new CaseNode();}
@after {node = caseNode;}
  :  ^(CASE1 caseStat[caseNode] elsecaseStat[caseNode])
  |  ^(CASE2 casesStat[caseNode]+ elsecaseStat[caseNode])
  ;

caseStat [CaseNode node]
  :  ^(CASEEXP expression caseStatOrExpr) {node.addCase("if", $expression.node, $caseStatOrExpr.node);}
  ;

casesStat[CaseNode node]
  :  ^(CASEEXP  expression caseStatOrExpr) {node.addChoiceCase("if", $expression.node, $caseStatOrExpr.node);}
  ;

elsecaseStat  [CaseNode node]
  :  ^(CASEEXPELSE expression) {node.addDefault("else", $expression.node);}
  ;

caseStatOrExpr returns [AstNode node]
  :  caseStatement {node = $caseStatement.node;}
  |  expression    {node = $expression.node;}
  ;

forStatement returns [AstNode node]
  :  ^(For Identifier a=expression b=expression s=expression? block) {node = new ForStatementNode($Identifier.text, $a.node, $b.node, $s.node, $block.node, currentScope);}
  ;

whileStatement returns [AstNode node]
  :  ^(While expression block) {node = new WhileStatementNode($expression.node, $block.node);}
  ;

idList returns [String text]
@init { java.lang.StringBuilder sb = new java.lang.StringBuilder(); }
@after {text = sb.toString();}
  :  ^(ID_LIST (Identifier { sb = sb.length() > 0 ? sb.append("." + $Identifier.text) : sb.append($Identifier.text);})+)
  ;

exprList returns [java.util.List<AstNode> e]
@init  {e = new java.util.ArrayList<AstNode>();}
  :  ^(EXP_LIST (expression {e.add($expression.node);})+)
  ;

paramList returns [java.util.List<AssignmentNode> e]
@init  {e = new java.util.ArrayList<AssignmentNode>();}
  :  ^(ASSIGNMENT_LIST (assignment {e.add((AssignmentNode)$assignment.node);})+)
  ;

array returns [AstNode node]
  :  ^(ARRAY sizes Type )  { node = new ArrayNode($sizes.e, $Type.text); }
  ;

sizes returns[java.util.List<String> e]
  @init  {e = new java.util.ArrayList<String>();}
  :  ^(SIZES (size {e.add($size.text);})+)
  ;

lookuparr returns [AstNode node]
  :  formlookup  {node =  $formlookup.node;}
  |  arraycall {node = $arraycall.node;}
  ;

arraycall returns [AstNode node]
  :   ^(ARRAYCALL Identifier exprList)  { node = new ArrayCallNode($Identifier.text, $exprList.e, currentScope); }
  ;
  
expression returns [AstNode node]
  :  ^(TERNARY a=expression b=expression c=expression) {node = new TernaryNode($a.node, $b.node, $c.node);}
  |  ^(In a=expression b=expression)                   {node = new InNode($a.node, $b.node);}
  |  ^('||' a=expression b=expression)                 {node = new OrNode($a.node, $b.node);}
  |  ^('&&' a=expression b=expression)                 {node = new AndNode($a.node, $b.node);}
  |  ^('==' a=expression b=expression)                 {node = new EqualsNode($a.node, $b.node);}
  |  ^('!=' a=expression b=expression)                 {node = new NotEqualsNode($a.node, $b.node);}
  |  ^('>=' a=expression b=expression)                 {node = new GTEqualsNode($a.node, $b.node);}
  |  ^('<=' a=expression b=expression)                 {node = new LTEqualsNode($a.node, $b.node);}
  |  ^('>' a=expression b=expression)                  {node = new GTNode($a.node, $b.node);}
  |  ^('<' a=expression b=expression)                  {node = new LTNode($a.node, $b.node);}
  |  ^('+' a=expression b=expression)                  {node = new AddNode($a.node, $b.node);}
  |  ^('-' a=expression b=expression)                  {node = new SubNode($a.node, $b.node);}
  |  ^('*' a=expression b=expression)                  {node = new MulNode($a.node, $b.node);}
  |  ^('/' a=expression b=expression)                  {node = new DivNode($a.node, $b.node);}
  |  ^('%' a=expression b=expression)                  {node = new ModNode($a.node, $b.node);}
  |  ^('^' a=expression b=expression)                  {node = new PowNode($a.node, $b.node);}
  |  ^(UNARY_MIN a=expression)                         {node = new UnaryMinusNode($a.node);}
  |  ^(NEGATE a=expression)                            {node = new NegateNode($a.node);}
  |  Number                                            {node = new AtomNode(Double.parseDouble($Number.text));}
  |  Bool                                              {node = new AtomNode(Boolean.parseBoolean($Bool.text));}
  |  Null                                              {node = new AtomNode(null);}
  |  String                                            {node = new AtomNode($String.text);}
  |  arraycall		       		       	       {node = $arraycall.node;}
  |  identifier			       		       {node = $identifier.node;}	
  |  nameClass       		       			{node = $nameClass.node;}	
  |  objectInvocation2					{node = $objectInvocation2.node;}
  |  ^(CTOR 'Form' paramList)                          {node = new FormNode($paramList.e);}
  |  ^(FUNC_DEF block)                                 {node = $block.node;}
  |  ^(EXP functionCall) 			       {node = $functionCall.node;}
  |  ^(EXPARRAY array) 			       	       {node = $array.node;}
  ;

list returns [AstNode node]
  :  ^(LIST exprList?) {node = new ListNode($exprList.e);}
  ;

lookup returns [AstNode node]
  :   ^(LOOKUP i=idList) {node = new IdentifierNode($i.text, currentScope); }
  ;
  
formlookup returns [AstNode node]
  :   ^(LOOKUP i=idList) {node = new IdentifierNode($i.text, currentScope); }
  ; 
 
identifier returns [AstNode node]
  : ^(IDENTIFIER Identifier) {node = new IdentifierNode($Identifier.text, currentScope);}	
  ; 

nameClass returns [AstNode node]
  : ^(NAMECLASS Array) {node = new IdentifierNode($Array.text, currentScope);}
  | ^(NAMECLASS Form ) {node = new IdentifierNode($Form.text, currentScope);}	
  | ^(NAMECLASS Subwindow) {node = new IdentifierNode($Subwindow.text, currentScope);}	
  | ^(NAMECLASS Button) {node = new IdentifierNode($Button.text, currentScope);}	
  | ^(NAMECLASS File) {node = new IdentifierNode($File.text, currentScope);}	
  | ^(NAMECLASS Panel) {node = new IdentifierNode($Panel.text, currentScope);}	
  | ^(NAMECLASS TextLabel) {node = new IdentifierNode($TextLabel.text, currentScope);}	
  | ^(NAMECLASS TextField) {node = new IdentifierNode($TextField.text, currentScope);}	
  ;   
  
/*  
idFormList returns [String text]
@init { java.lang.StringBuilder sb = new java.lang.StringBuilder(); }
@after {text = sb.toString();}
  :  ^(IDFORMLIST (Identifier { sb = sb.length() > 0 ? sb.append("." + $idForm.text) : sb.append($idForm.text);})+)
 ;
 */  

  
