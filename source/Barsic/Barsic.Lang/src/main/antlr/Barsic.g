grammar Barsic;

options {
  output=AST;
}

tokens {
  BLOCK;
  RETURN;
  STATEMENTS;
  ASSIGNMENT;
  ASSIGNMENT_LIST;  
  FUNC_CALL;
  FUNC_DEF;
  FUNC_EXP;
  CTOR;
  EXP;
  EXP_LIST;
  ID_LIST;
  IF1;
  IF2;
  CASE;
  TERNARY;
  UNARY_MIN;
  NEGATE;
  LIST;
  LOOKUP;
  CLOSE;
  PROCEDURE;
  FUNCTIONCALL;
  ARRAY;
  EXPARRAY;
  ARRAYCALL;
  PROCEDUREPARAMETER;
  IDENTIFIER_LIST;
  IDENTIFIERLIST_AND_TYPE;
  CASE1;
  CASE2;
  CASEEXP;
  CASEEXPELSE;
  CASEASSIGMENT;
  SIZE;
  SIZES;
  BELONGS;
  NUMLIST;
  IDENTIFIER;
  IDFORMLIST;
  TYPECASTEXP;
  TYPECASTID;
  NAMECLASS;
  TYPE1;
  TYPE2;
  TYPE3;
  TYPE4;
  TYPECAST2LIST; 
}

@parser::header {
  package barsic.parser;
  import barsic.Scope;
  import barsic.Function;
  import barsic.tree.BarsicValue;
  import barsic.tree.functions.PrintlnNode;
  import barsic.tree.form.FormNode;
  import barsic.tree.primitives.*;
  import java.util.Map;
  import java.util.HashMap;
  import java.util.Arrays;
}

@lexer::header {
  package barsic.parser;
}

@parser::members {
  public Map<String, Function> functions = new HashMap<String, Function>();
}

parse
  :  block EOF -> block
  ;

/* Basic elements */
block
  :  (statement)* (Return expression ';')?
     -> ^(BLOCK ^(STATEMENTS statement*) ^(RETURN expression?))
  ;

statement
  :  assignment ';'   -> assignment
  |  objectInvocation ';' -> objectInvocation
  |  functionCall ';' -> functionCall
  |  ifStatement
  |  forStatement
  |  whileStatement
  |  caseAssigment
  |  procedure ';'-> procedure
  ;

functionCall
  :  Identifier '(' exprList? ')'  -> ^(FUNCTIONCALL Identifier exprList?)
  ;

procedure
  : Procedure  Identifier '(' procedureParameter? ')'
  (Import
   identifiertList)?
 (Variables
  paramList)?
  (Begin | OBrace)
   block
  (End | CBrace)   -> ^(PROCEDURE Identifier procedureParameter? identifiertList? paramList? block )   //procedureParameter?  identifiertList?
  ;

procedureParameter
  : identifiertList_and_type (',' identifiertList_and_type)* -> ^(PROCEDUREPARAMETER identifiertList_and_type+)
  ;

identifiertList_and_type
  :	(identifiertList ':' Type) -> ^(IDENTIFIERLIST_AND_TYPE identifiertList Type)
  ;
  
identifier
 : Identifier -> ^(IDENTIFIER Identifier)
 ;	
	
identifiertList
 : Identifier 	(','Identifier)* -> ^(IDENTIFIER_LIST Identifier+)
 ;
assignment
  :  identifier paramList CloseIdentifier   -> ^(ASSIGNMENT identifier paramList) //Identifier
  |  lookuparr '=' expression -> ^(ASSIGNMENT lookuparr expression)
  ;

paramList
  :  assignment+ (',' assignment)* -> ^(ASSIGNMENT_LIST assignment+)
  ;

ifStatement
  : ifStat elseStat?  EndIf ';' -> ^(IF1 ifStat  elseStat?)
  | If  ifsStat+ elseStat?  EndIf ';' -> ^(IF2 ifsStat+  elseStat?)
  ;

ifStat
  : If ifexpression Hence block -> ^(EXP ifexpression block)
  ;

ifsStat
  : '|' ifexpression Hence block -> ^(EXP ifexpression block)
  ;

elseStat
  :  Else block -> ^(EXP block)
  ;

ifexpression
  : belongs
  | expression
  ;

belongs
  : lookup In '['  sizeBelongs ']' ->  ^(BELONGS lookup sizeBelongs)
  ;

sizeBelongs
  :  size
  |  numList
  ;

size
 : Number To Number -> ^(SIZE Number Number)
 ;

numList
 : Number (',' Number)* -> ^(NUMLIST Number+)
 ;

caseAssigment
   : lookup '=' caseStatement ';' -> ^(CASEASSIGMENT  lookup  caseStatement)
   ;

caseStatement
  : caseStat elsecaseStat  EndCase -> ^(CASE1 caseStat  elsecaseStat)
  | Case  casesStat+ elsecaseStat  EndCase  -> ^(CASE2 casesStat+  elsecaseStat)
  ;

caseStat
  : Case expression Hence caseStatOrExpr -> ^(CASEEXP expression caseStatOrExpr)
  ;

elsecaseStat
  :  Else expression -> ^(CASEEXPELSE expression)
  ;

casesStat
  : '|' expression Hence caseStatOrExpr -> ^(CASEEXP expression caseStatOrExpr)
  ;

caseStatOrExpr
  :  caseStatement
  |  expression
  ;

forStatement
  :  For Identifier '=' expression To expression (',' Step '=' expression)? Do block EndDo';'
     -> ^(For Identifier expression expression expression? block)
  ;

whileStatement
  :  While expression Do block EndDo ';'  -> ^(While expression block)
  ;

/* Primitive  elements */
expression
  : condExpr
  | New 'Form' paramList CloseIdentifier -> ^(CTOR 'Form' paramList)
  | OBrace block CBrace -> ^(FUNC_DEF block)
  | array -> ^(EXPARRAY array)
  ;

exprList
  :  expression (',' expression)* -> ^(EXP_LIST expression+)
  ;

condExpr
  :  (orExpr -> orExpr)
     ( '?' a=expression ':' b=expression -> ^(TERNARY orExpr $a $b))?
  ;

orExpr
  :  andExpr ('||'^ andExpr)*
  ;

andExpr
  :  equExpr ('&&'^ equExpr)*
  ;

equExpr
  :  relExpr (('==' | '!=')^ relExpr)*
  ;

relExpr
  :  addExpr (('>=' | '<=' | '>' | '<')^ addExpr)*
  ;

addExpr
  :  mulExpr (('+' | '-')^ mulExpr)*
  ;

mulExpr
  :  powExpr (('*' | '/' | '%')^ powExpr)*
  ;

powExpr
  :  unaryExpr ('^' unaryExpr)*
  ;

unaryExpr
  :  '-' atom -> ^(UNARY_MIN atom)
  |  '!' atom -> ^(NEGATE atom)
  |  atom
  |  identifier 
  |  arrayCall
  |  functionCall -> ^(EXP functionCall)
  |  nameClass 
  |  objectInvocation2
  ;

atom
  :  Number
  |  Bool
  |  Null
  |  String
  ;

idList
  :  Identifier ('.' Identifier)* -> ^(ID_LIST Identifier+)
  ;
  
idFormList
 :  idForm ('.' idForm)* -> ^(ID_LIST idForm+)
 ;			  
  
idForm	
  :  Identifier  -> Identifier
  |  nameClass '('Identifier')' ->  Identifier
  ;
  
formlookup
  :  idFormList   -> ^(LOOKUP idFormList)
  ;  

lookup
  :  idList   -> ^(LOOKUP idList)
  ;
  	
lookuparr
  : formlookup
  | arrayCall
  ;

//typeCast
 //:  nameClass '(' expression ')' -> expression
//  | Identifier '.' Identifier '(' expression ')' -> expression
//  ;

objectInvocation
  : a=typeCast1  ('.' typeCast1)* ('.' objectInvocationMethod)  -> ^(FUNC_CALL FormType $a objectInvocationMethod)  //typeCast1+ ('.' objectInvocationMethod)+
  ;

objectInvocationMethod
 :  Identifier paramList CloseIdentifier -> ^(FUNC_CALL FormType Identifier paramList)
 | 'output' block '_output' -> ^(FUNC_CALL FormType block)
 |  Identifier '(' exprList? ')' -> ^(FUNC_CALL FormType Identifier exprList?)
 ; 
  
typeCast1
  :  nameClass '(' expression ')'  -> ^(TYPECASTEXP expression)
  |  identifier 		   -> ^(TYPECASTID  identifier)
  ;  
  
typeCast2
 :  nameClass '(' expression ')' -> ^(TYPE1 expression)
 |  functionCall -> ^(TYPE2  functionCall)
 |  identifier-> ^(TYPE3  identifier)
  //|  arrayCall
 ;
 
 typeCast2List
  : ('.' typeCast2)+ ->^(TYPECAST2LIST typeCast2+)
  ;
 
objectInvocation2
  :  typeCast1 typeCast2List   -> ^(FUNC_EXP typeCast1 typeCast2List )
  ;

 //rmBy	
 // :  ReplaceBy '(' block ')'
 // |  MoveBy '(' assignment  ',' assignment')'
 // ;	
  
arrayCall
 : Identifier '['exprList']' -> ^(ARRAYCALL Identifier exprList)
 ;

array
 : New 'Array' '['sizes']' Of Type -> ^(ARRAY sizes Type)
 ;

sizes
 : size (',' size)* -> ^(SIZES size+)
 ;
 
nameClass 
 : Array 	-> ^(NAMECLASS Array)
 | Form 	-> ^(NAMECLASS Form)
 | Subwindow  ->   ^(NAMECLASS Subwindow)
 | Button   -> 	   ^(NAMECLASS Button ) 
 | File -> 	   ^(NAMECLASS File ) 
 | Panel -> 	   ^(NAMECLASS Panel ) 
 | TextLabel -> ^(NAMECLASS TextLabel)
 | TextField -> ^(NAMECLASS TextField)
 ;  
 
Type	 : 'Number'| 'Bool'| 'String';

Array 	 : 'Array';
Form     : 'Form';
Subwindow :'Subwindow' ;
Button : 'Button' ;
File 	:	'File';
Panel	:	'Panel';
TextLabel :	'TextLabel';	
TextField :     'TextField'; 	 

Of	 : 'of';
Import	 : 'import';
Variables   :'variables';

Procedure: 'procedure';
Begin	:  'begin';


Infinity : 'infinity';
New      : 'new';


FormType : 'FormType';
Println  : 'println';

Def      : 'def';

Return   : 'return';
For      : 'for';
While    : 'while';

Step	: 'step';

To       : '..';
Do       : 'do';
EndDo    : '_do';

End      : 'end';
In       : 'in';
Null     : 'null';
Hence    : '=>';

If       : 'if';
Else     : 'else';
EndIf    : '_if';

Case     : 'case';
EndCase  :  '_case';

OBrace   : '{';
CBrace   : '}';
Close	: '_';

Bool  :  'true' | 'false';

And 	:	'and';
Or 	:	'or';

Number
  :  Int ('.' Digit*)?
  ;

Identifier
  : ('a'..'z' | 'A'..'Z' |  '#' ) ('a'..'z' | 'A'..'Z' | '_' | Digit)*
  ;

CloseIdentifier
  : (Close) Identifier
  ;

String
@after {
  setText(getText().substring(1, getText().length()-1).replaceAll("\\\\(.)", "$1"));
}
  :  '"'  (~('"' | '\\')  | '\\' ('\\' | '"'))* '"'
  |  '\'' (~('\'' | '\\') | '\\' ('\\' | '\''))* '\''
  ;

Comment
  :  '//' ~('\r' | '\n')* {skip();}
  |  '/*' .* '*/'         {skip();}
  ;

Space
  :  (' ' | '\t' | '\r' | '\n' | '\u000C') {skip();}
  ;

fragment Int
  :  '1'..'9' Digit*
  |  '0'
  ;

fragment Digit
  :  '0'..'9'
  ;