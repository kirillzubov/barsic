package barsic;

import java.io.File;

public class FileUtils {
    public static String getNameWithoutExtension(File file) {
        String name = file.getName();
        int i = name.lastIndexOf('.');
        return name.substring(0, i);
    }

    public static String getExtension(File file) {
        String name = file.getName();
        int i = name.lastIndexOf('.');
        return name.substring(i + 1);
    }
}
