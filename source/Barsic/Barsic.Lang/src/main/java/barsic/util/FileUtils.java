package barsic.util;

import java.io.*;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ru
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public class FileUtils {
    public static String getFileNameWithoutExt(File file) {
        return file.getName().replaceFirst("[.][^.]+$", "");
    }

    public static String readAllText(File file) throws IOException {
        InputStream in = new FileInputStream(file);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();
        while (line != null) {
            sb.append(line);
            sb.append(System.lineSeparator());
            line = br.readLine();
        }
        in.close();
        return sb.toString();
    }
}
