// $ANTLR 3.4 C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g 2018-05-29 20:22:17

  package barsic.parser;
  import barsic.Scope;
  import barsic.Function;
  import barsic.tree.form.*;
  import barsic.tree.BarsicValue;
  import barsic.tree.functions.*;
  import barsic.tree.primitives.*;
  import java.util.Map;
  import java.util.HashMap;
  import java.util.Arrays;


import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class BarsicTreeWalker extends TreeParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ARRAY", "ARRAYCALL", "ASSIGNMENT", "ASSIGNMENT_LIST", "And", "Array", "BELONGS", "BLOCK", "Begin", "Bool", "Button", "CASE", "CASE1", "CASE2", "CASEASSIGMENT", "CASEEXP", "CASEEXPELSE", "CBrace", "CLOSE", "CTOR", "Case", "Close", "CloseIdentifier", "Comment", "Def", "Digit", "Do", "EXP", "EXPARRAY", "EXP_LIST", "Else", "End", "EndCase", "EndDo", "EndIf", "FUNCTIONCALL", "FUNC_CALL", "FUNC_DEF", "FUNC_EXP", "File", "For", "Form", "FormType", "Hence", "IDENTIFIER", "IDENTIFIERLIST_AND_TYPE", "IDENTIFIER_LIST", "IDFORMLIST", "ID_LIST", "IF1", "IF2", "Identifier", "If", "Import", "In", "Infinity", "Int", "LIST", "LOOKUP", "NAMECLASS", "NEGATE", "NUMLIST", "New", "Null", "Number", "OBrace", "Of", "Or", "PROCEDURE", "PROCEDUREPARAMETER", "Panel", "Println", "Procedure", "RETURN", "Return", "SIZE", "SIZES", "STATEMENTS", "Space", "Step", "String", "Subwindow", "TERNARY", "TYPE1", "TYPE2", "TYPE3", "TYPE4", "TYPECAST2LIST", "TYPECASTEXP", "TYPECASTID", "TextField", "TextLabel", "To", "Type", "UNARY_MIN", "Variables", "While", "'!'", "'!='", "'%'", "'&&'", "'('", "')'", "'*'", "'+'", "','", "'-'", "'.'", "'/'", "':'", "';'", "'<'", "'<='", "'='", "'=='", "'>'", "'>='", "'?'", "'['", "']'", "'^'", "'_output'", "'output'", "'|'", "'||'"
    };

    public static final int EOF=-1;
    public static final int T__101=101;
    public static final int T__102=102;
    public static final int T__103=103;
    public static final int T__104=104;
    public static final int T__105=105;
    public static final int T__106=106;
    public static final int T__107=107;
    public static final int T__108=108;
    public static final int T__109=109;
    public static final int T__110=110;
    public static final int T__111=111;
    public static final int T__112=112;
    public static final int T__113=113;
    public static final int T__114=114;
    public static final int T__115=115;
    public static final int T__116=116;
    public static final int T__117=117;
    public static final int T__118=118;
    public static final int T__119=119;
    public static final int T__120=120;
    public static final int T__121=121;
    public static final int T__122=122;
    public static final int T__123=123;
    public static final int T__124=124;
    public static final int T__125=125;
    public static final int T__126=126;
    public static final int T__127=127;
    public static final int T__128=128;
    public static final int ARRAY=4;
    public static final int ARRAYCALL=5;
    public static final int ASSIGNMENT=6;
    public static final int ASSIGNMENT_LIST=7;
    public static final int And=8;
    public static final int Array=9;
    public static final int BELONGS=10;
    public static final int BLOCK=11;
    public static final int Begin=12;
    public static final int Bool=13;
    public static final int Button=14;
    public static final int CASE=15;
    public static final int CASE1=16;
    public static final int CASE2=17;
    public static final int CASEASSIGMENT=18;
    public static final int CASEEXP=19;
    public static final int CASEEXPELSE=20;
    public static final int CBrace=21;
    public static final int CLOSE=22;
    public static final int CTOR=23;
    public static final int Case=24;
    public static final int Close=25;
    public static final int CloseIdentifier=26;
    public static final int Comment=27;
    public static final int Def=28;
    public static final int Digit=29;
    public static final int Do=30;
    public static final int EXP=31;
    public static final int EXPARRAY=32;
    public static final int EXP_LIST=33;
    public static final int Else=34;
    public static final int End=35;
    public static final int EndCase=36;
    public static final int EndDo=37;
    public static final int EndIf=38;
    public static final int FUNCTIONCALL=39;
    public static final int FUNC_CALL=40;
    public static final int FUNC_DEF=41;
    public static final int FUNC_EXP=42;
    public static final int File=43;
    public static final int For=44;
    public static final int Form=45;
    public static final int FormType=46;
    public static final int Hence=47;
    public static final int IDENTIFIER=48;
    public static final int IDENTIFIERLIST_AND_TYPE=49;
    public static final int IDENTIFIER_LIST=50;
    public static final int IDFORMLIST=51;
    public static final int ID_LIST=52;
    public static final int IF1=53;
    public static final int IF2=54;
    public static final int Identifier=55;
    public static final int If=56;
    public static final int Import=57;
    public static final int In=58;
    public static final int Infinity=59;
    public static final int Int=60;
    public static final int LIST=61;
    public static final int LOOKUP=62;
    public static final int NAMECLASS=63;
    public static final int NEGATE=64;
    public static final int NUMLIST=65;
    public static final int New=66;
    public static final int Null=67;
    public static final int Number=68;
    public static final int OBrace=69;
    public static final int Of=70;
    public static final int Or=71;
    public static final int PROCEDURE=72;
    public static final int PROCEDUREPARAMETER=73;
    public static final int Panel=74;
    public static final int Println=75;
    public static final int Procedure=76;
    public static final int RETURN=77;
    public static final int Return=78;
    public static final int SIZE=79;
    public static final int SIZES=80;
    public static final int STATEMENTS=81;
    public static final int Space=82;
    public static final int Step=83;
    public static final int String=84;
    public static final int Subwindow=85;
    public static final int TERNARY=86;
    public static final int TYPE1=87;
    public static final int TYPE2=88;
    public static final int TYPE3=89;
    public static final int TYPE4=90;
    public static final int TYPECAST2LIST=91;
    public static final int TYPECASTEXP=92;
    public static final int TYPECASTID=93;
    public static final int TextField=94;
    public static final int TextLabel=95;
    public static final int To=96;
    public static final int Type=97;
    public static final int UNARY_MIN=98;
    public static final int Variables=99;
    public static final int While=100;

    // delegates
    public TreeParser[] getDelegates() {
        return new TreeParser[] {};
    }

    // delegators


    public BarsicTreeWalker(TreeNodeStream input) {
        this(input, new RecognizerSharedState());
    }
    public BarsicTreeWalker(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    public String[] getTokenNames() { return BarsicTreeWalker.tokenNames; }
    public String getGrammarFileName() { return "C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g"; }


      public Map<String, Function> functions = null;
      Scope currentScope = null;
      
      public BarsicTreeWalker(CommonTreeNodeStream nodes, Map<String, Function> fns) {
        this(nodes, null, fns);
      }
      
      public BarsicTreeWalker(CommonTreeNodeStream nds, Scope sc, Map<String, Function> fns) {
        super(nds);
        currentScope = sc;
        functions = fns;
      }



    // $ANTLR start "walk"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:36:1: walk returns [AstNode node] : block ;
    public final AstNode walk() throws RecognitionException {
        AstNode node = null;


        AstNode block1 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:37:3: ( block )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:37:6: block
            {
            pushFollow(FOLLOW_block_in_walk50);
            block1=block();

            state._fsp--;


            node = block1;

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "walk"



    // $ANTLR start "block"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:40:1: block returns [AstNode node] : ^( BLOCK ^( STATEMENTS ( statement )* ) ^( RETURN ( expression )? ) ) ;
    public final AstNode block() throws RecognitionException {
        AstNode node = null;


        AstNode statement2 =null;

        AstNode expression3 =null;



          BlockNode bn = new BlockNode();
          node = bn;
          Scope local = new Scope(currentScope);
          currentScope = local;

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:50:3: ( ^( BLOCK ^( STATEMENTS ( statement )* ) ^( RETURN ( expression )? ) ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:50:5: ^( BLOCK ^( STATEMENTS ( statement )* ) ^( RETURN ( expression )? ) )
            {
            match(input,BLOCK,FOLLOW_BLOCK_in_block80); 

            match(input, Token.DOWN, null); 
            match(input,STATEMENTS,FOLLOW_STATEMENTS_in_block83); 

            if ( input.LA(1)==Token.DOWN ) {
                match(input, Token.DOWN, null); 
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:50:26: ( statement )*
                loop1:
                do {
                    int alt1=2;
                    int LA1_0 = input.LA(1);

                    if ( (LA1_0==ASSIGNMENT||LA1_0==CASEASSIGMENT||(LA1_0 >= FUNCTIONCALL && LA1_0 <= FUNC_CALL)||LA1_0==For||(LA1_0 >= IF1 && LA1_0 <= IF2)||LA1_0==PROCEDURE||LA1_0==While) ) {
                        alt1=1;
                    }


                    switch (alt1) {
                	case 1 :
                	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:50:27: statement
                	    {
                	    pushFollow(FOLLOW_statement_in_block86);
                	    statement2=statement();

                	    state._fsp--;


                	    bn.addStatement(statement2);

                	    }
                	    break;

                	default :
                	    break loop1;
                    }
                } while (true);


                match(input, Token.UP, null); 
            }


            match(input,RETURN,FOLLOW_RETURN_in_block94); 

            if ( input.LA(1)==Token.DOWN ) {
                match(input, Token.DOWN, null); 
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:50:85: ( expression )?
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==ARRAYCALL||LA2_0==Bool||LA2_0==CTOR||(LA2_0 >= EXP && LA2_0 <= EXPARRAY)||(LA2_0 >= FUNC_DEF && LA2_0 <= FUNC_EXP)||LA2_0==IDENTIFIER||LA2_0==In||(LA2_0 >= NAMECLASS && LA2_0 <= NEGATE)||(LA2_0 >= Null && LA2_0 <= Number)||LA2_0==String||LA2_0==TERNARY||LA2_0==UNARY_MIN||(LA2_0 >= 102 && LA2_0 <= 104)||(LA2_0 >= 107 && LA2_0 <= 108)||LA2_0==110||LA2_0==112||(LA2_0 >= 115 && LA2_0 <= 116)||(LA2_0 >= 118 && LA2_0 <= 120)||LA2_0==124||LA2_0==128) ) {
                    alt2=1;
                }
                switch (alt2) {
                    case 1 :
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:50:86: expression
                        {
                        pushFollow(FOLLOW_expression_in_block97);
                        expression3=expression();

                        state._fsp--;


                        bn.addReturn(expression3);

                        }
                        break;

                }


                match(input, Token.UP, null); 
            }


            match(input, Token.UP, null); 


            }


              currentScope = currentScope.parent();

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "block"



    // $ANTLR start "statement"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:53:1: statement returns [AstNode node] : ( assignment | objectInvocation | ifStatement | forStatement | whileStatement | procedure | functionCall | caseAssigment );
    public final AstNode statement() throws RecognitionException {
        AstNode node = null;


        AstNode assignment4 =null;

        FormCallNode objectInvocation5 =null;

        AstNode ifStatement6 =null;

        AstNode forStatement7 =null;

        AstNode whileStatement8 =null;

        AstNode procedure9 =null;

        AstNode functionCall10 =null;

        AstNode caseAssigment11 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:54:3: ( assignment | objectInvocation | ifStatement | forStatement | whileStatement | procedure | functionCall | caseAssigment )
            int alt3=8;
            switch ( input.LA(1) ) {
            case ASSIGNMENT:
                {
                alt3=1;
                }
                break;
            case FUNC_CALL:
                {
                alt3=2;
                }
                break;
            case IF1:
            case IF2:
                {
                alt3=3;
                }
                break;
            case For:
                {
                alt3=4;
                }
                break;
            case While:
                {
                alt3=5;
                }
                break;
            case PROCEDURE:
                {
                alt3=6;
                }
                break;
            case FUNCTIONCALL:
                {
                alt3=7;
                }
                break;
            case CASEASSIGMENT:
                {
                alt3=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }

            switch (alt3) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:54:6: assignment
                    {
                    pushFollow(FOLLOW_assignment_in_statement121);
                    assignment4=assignment();

                    state._fsp--;


                    node = assignment4;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:55:6: objectInvocation
                    {
                    pushFollow(FOLLOW_objectInvocation_in_statement134);
                    objectInvocation5=objectInvocation();

                    state._fsp--;


                    node = objectInvocation5;

                    }
                    break;
                case 3 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:56:6: ifStatement
                    {
                    pushFollow(FOLLOW_ifStatement_in_statement145);
                    ifStatement6=ifStatement();

                    state._fsp--;


                    node = ifStatement6;

                    }
                    break;
                case 4 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:57:6: forStatement
                    {
                    pushFollow(FOLLOW_forStatement_in_statement157);
                    forStatement7=forStatement();

                    state._fsp--;


                    node = forStatement7;

                    }
                    break;
                case 5 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:58:6: whileStatement
                    {
                    pushFollow(FOLLOW_whileStatement_in_statement168);
                    whileStatement8=whileStatement();

                    state._fsp--;


                    node = whileStatement8;

                    }
                    break;
                case 6 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:59:6: procedure
                    {
                    pushFollow(FOLLOW_procedure_in_statement177);
                    procedure9=procedure();

                    state._fsp--;


                    node = procedure9;

                    }
                    break;
                case 7 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:60:6: functionCall
                    {
                    pushFollow(FOLLOW_functionCall_in_statement191);
                    functionCall10=functionCall();

                    state._fsp--;


                    node = functionCall10;

                    }
                    break;
                case 8 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:61:6: caseAssigment
                    {
                    pushFollow(FOLLOW_caseAssigment_in_statement202);
                    caseAssigment11=caseAssigment();

                    state._fsp--;


                    node = caseAssigment11;

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "statement"



    // $ANTLR start "functionCall"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:64:1: functionCall returns [AstNode node] : ^( FUNCTIONCALL Identifier ( exprList )? ) ;
    public final AstNode functionCall() throws RecognitionException {
        AstNode node = null;


        CommonTree Identifier12=null;
        java.util.List<AstNode> exprList13 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:65:3: ( ^( FUNCTIONCALL Identifier ( exprList )? ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:65:5: ^( FUNCTIONCALL Identifier ( exprList )? )
            {
            match(input,FUNCTIONCALL,FOLLOW_FUNCTIONCALL_in_functionCall225); 

            match(input, Token.DOWN, null); 
            Identifier12=(CommonTree)match(input,Identifier,FOLLOW_Identifier_in_functionCall227); 

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:65:31: ( exprList )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==EXP_LIST) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:65:31: exprList
                    {
                    pushFollow(FOLLOW_exprList_in_functionCall229);
                    exprList13=exprList();

                    state._fsp--;


                    }
                    break;

            }


            match(input, Token.UP, null); 


             node = new FunctionCallNode((Identifier12!=null?Identifier12.getText():null), exprList13, currentScope); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "functionCall"



    // $ANTLR start "procedure"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:68:1: procedure returns [AstNode node] : ^( PROCEDURE Identifier ( procedureParameter )? ( identifiertList )? ( paramList )? block ) ;
    public final AstNode procedure() throws RecognitionException {
        AstNode node = null;


        CommonTree Identifier14=null;
        java.util.List<IdentifiertList_and_typeNode> procedureParameter15 =null;

        java.util.List<String> identifiertList16 =null;

        java.util.List<AssignmentNode> paramList17 =null;

        AstNode block18 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:69:3: ( ^( PROCEDURE Identifier ( procedureParameter )? ( identifiertList )? ( paramList )? block ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:69:5: ^( PROCEDURE Identifier ( procedureParameter )? ( identifiertList )? ( paramList )? block )
            {
            match(input,PROCEDURE,FOLLOW_PROCEDURE_in_procedure254); 

            match(input, Token.DOWN, null); 
            Identifier14=(CommonTree)match(input,Identifier,FOLLOW_Identifier_in_procedure256); 

            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:69:28: ( procedureParameter )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==PROCEDUREPARAMETER) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:69:28: procedureParameter
                    {
                    pushFollow(FOLLOW_procedureParameter_in_procedure258);
                    procedureParameter15=procedureParameter();

                    state._fsp--;


                    }
                    break;

            }


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:69:50: ( identifiertList )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==IDENTIFIER_LIST) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:69:50: identifiertList
                    {
                    pushFollow(FOLLOW_identifiertList_in_procedure263);
                    identifiertList16=identifiertList();

                    state._fsp--;


                    }
                    break;

            }


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:69:68: ( paramList )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==ASSIGNMENT_LIST) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:69:68: paramList
                    {
                    pushFollow(FOLLOW_paramList_in_procedure267);
                    paramList17=paramList();

                    state._fsp--;


                    }
                    break;

            }


            pushFollow(FOLLOW_block_in_procedure271);
            block18=block();

            state._fsp--;


            match(input, Token.UP, null); 


             node = new ProcedureFuncNode((Identifier14!=null?Identifier14.getText():null), procedureParameter15, identifiertList16, paramList17, block18); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "procedure"



    // $ANTLR start "procedureParameter"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:72:1: procedureParameter returns [java.util.List<IdentifiertList_and_typeNode> e] : ^( PROCEDUREPARAMETER ( identifiertList_and_type )+ ) ;
    public final java.util.List<IdentifiertList_and_typeNode> procedureParameter() throws RecognitionException {
        java.util.List<IdentifiertList_and_typeNode> e = null;


        AstNode identifiertList_and_type19 =null;


        e = new java.util.ArrayList<IdentifiertList_and_typeNode>();
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:74:3: ( ^( PROCEDUREPARAMETER ( identifiertList_and_type )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:74:5: ^( PROCEDUREPARAMETER ( identifiertList_and_type )+ )
            {
            match(input,PROCEDUREPARAMETER,FOLLOW_PROCEDUREPARAMETER_in_procedureParameter302); 

            match(input, Token.DOWN, null); 
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:74:26: ( identifiertList_and_type )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==IDENTIFIERLIST_AND_TYPE) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:74:27: identifiertList_and_type
            	    {
            	    pushFollow(FOLLOW_identifiertList_and_type_in_procedureParameter305);
            	    identifiertList_and_type19=identifiertList_and_type();

            	    state._fsp--;


            	    e.add((IdentifiertList_and_typeNode)identifiertList_and_type19);

            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);


            match(input, Token.UP, null); 


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return e;
    }
    // $ANTLR end "procedureParameter"



    // $ANTLR start "identifiertList_and_type"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:77:1: identifiertList_and_type returns [AstNode node] : ^( IDENTIFIERLIST_AND_TYPE identifiertList Type ) ;
    public final AstNode identifiertList_and_type() throws RecognitionException {
        AstNode node = null;


        CommonTree Type21=null;
        java.util.List<String> identifiertList20 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:78:3: ( ^( IDENTIFIERLIST_AND_TYPE identifiertList Type ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:78:5: ^( IDENTIFIERLIST_AND_TYPE identifiertList Type )
            {
            match(input,IDENTIFIERLIST_AND_TYPE,FOLLOW_IDENTIFIERLIST_AND_TYPE_in_identifiertList_and_type328); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_identifiertList_in_identifiertList_and_type330);
            identifiertList20=identifiertList();

            state._fsp--;


            Type21=(CommonTree)match(input,Type,FOLLOW_Type_in_identifiertList_and_type332); 

            match(input, Token.UP, null); 


            node = new IdentifiertList_and_typeNode (identifiertList20 , (Type21!=null?Type21.getText():null));

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "identifiertList_and_type"



    // $ANTLR start "identifiertList"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:81:1: identifiertList returns [java.util.List<String> e] : ^( IDENTIFIER_LIST ( Identifier )+ ) ;
    public final java.util.List<String> identifiertList() throws RecognitionException {
        java.util.List<String> e = null;


        CommonTree Identifier22=null;

        e = new java.util.ArrayList<String>();
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:83:3: ( ^( IDENTIFIER_LIST ( Identifier )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:83:5: ^( IDENTIFIER_LIST ( Identifier )+ )
            {
            match(input,IDENTIFIER_LIST,FOLLOW_IDENTIFIER_LIST_in_identifiertList359); 

            match(input, Token.DOWN, null); 
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:83:23: ( Identifier )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==Identifier) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:83:24: Identifier
            	    {
            	    Identifier22=(CommonTree)match(input,Identifier,FOLLOW_Identifier_in_identifiertList362); 

            	    e.add((Identifier22!=null?Identifier22.getText():null));

            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            match(input, Token.UP, null); 


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return e;
    }
    // $ANTLR end "identifiertList"



    // $ANTLR start "assignment"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:86:1: assignment returns [AstNode node] : ( ^( ASSIGNMENT lookuparr expression ) | ^( ASSIGNMENT identifier paramList ) );
    public final AstNode assignment() throws RecognitionException {
        AstNode node = null;


        AstNode lookuparr23 =null;

        AstNode expression24 =null;

        AstNode identifier25 =null;

        java.util.List<AssignmentNode> paramList26 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:87:3: ( ^( ASSIGNMENT lookuparr expression ) | ^( ASSIGNMENT identifier paramList ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==ASSIGNMENT) ) {
                int LA10_1 = input.LA(2);

                if ( (LA10_1==DOWN) ) {
                    int LA10_2 = input.LA(3);

                    if ( (LA10_2==ARRAYCALL||LA10_2==LOOKUP) ) {
                        alt10=1;
                    }
                    else if ( (LA10_2==IDENTIFIER) ) {
                        alt10=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 10, 2, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;

            }
            switch (alt10) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:87:6: ^( ASSIGNMENT lookuparr expression )
                    {
                    match(input,ASSIGNMENT,FOLLOW_ASSIGNMENT_in_assignment386); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_lookuparr_in_assignment388);
                    lookuparr23=lookuparr();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_assignment390);
                    expression24=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new AssignmentNode(lookuparr23, expression24, null, currentScope);

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:88:6: ^( ASSIGNMENT identifier paramList )
                    {
                    match(input,ASSIGNMENT,FOLLOW_ASSIGNMENT_in_assignment401); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_identifier_in_assignment403);
                    identifier25=identifier();

                    state._fsp--;


                    pushFollow(FOLLOW_paramList_in_assignment405);
                    paramList26=paramList();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new AssignmentNode(identifier25, null, paramList26, currentScope);

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "assignment"



    // $ANTLR start "typeCast1"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:91:1: typeCast1 returns [AstNode node] : ( ^( TYPECASTEXP expression ) | ^( TYPECASTID identifier ) );
    public final AstNode typeCast1() throws RecognitionException {
        AstNode node = null;


        AstNode expression27 =null;

        AstNode identifier28 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:92:2: ( ^( TYPECASTEXP expression ) | ^( TYPECASTID identifier ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==TYPECASTEXP) ) {
                alt11=1;
            }
            else if ( (LA11_0==TYPECASTID) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }
            switch (alt11) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:92:5: ^( TYPECASTEXP expression )
                    {
                    match(input,TYPECASTEXP,FOLLOW_TYPECASTEXP_in_typeCast1428); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_typeCast1430);
                    expression27=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node =  expression27;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:93:5: ^( TYPECASTID identifier )
                    {
                    match(input,TYPECASTID,FOLLOW_TYPECASTID_in_typeCast1440); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_identifier_in_typeCast1443);
                    identifier28=identifier();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = identifier28;

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "typeCast1"



    // $ANTLR start "typeCast2"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:96:2: typeCast2 returns [AstNode node] : ( ^( TYPE1 expression ) | ^( TYPE2 functionCall ) | ^( TYPE3 identifier ) );
    public final AstNode typeCast2() throws RecognitionException {
        AstNode node = null;


        AstNode expression29 =null;

        AstNode functionCall30 =null;

        AstNode identifier31 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:97:2: ( ^( TYPE1 expression ) | ^( TYPE2 functionCall ) | ^( TYPE3 identifier ) )
            int alt12=3;
            switch ( input.LA(1) ) {
            case TYPE1:
                {
                alt12=1;
                }
                break;
            case TYPE2:
                {
                alt12=2;
                }
                break;
            case TYPE3:
                {
                alt12=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;

            }

            switch (alt12) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:97:5: ^( TYPE1 expression )
                    {
                    match(input,TYPE1,FOLLOW_TYPE1_in_typeCast2465); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_typeCast2467);
                    expression29=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node =  expression29;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:98:5: ^( TYPE2 functionCall )
                    {
                    match(input,TYPE2,FOLLOW_TYPE2_in_typeCast2476); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_functionCall_in_typeCast2478);
                    functionCall30=functionCall();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = functionCall30;

                    }
                    break;
                case 3 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:99:5: ^( TYPE3 identifier )
                    {
                    match(input,TYPE3,FOLLOW_TYPE3_in_typeCast2487); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_identifier_in_typeCast2490);
                    identifier31=identifier();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = identifier31;

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "typeCast2"



    // $ANTLR start "typeCast2List"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:103:2: typeCast2List returns [java.util.List<AstNode> e] : ^( TYPECAST2LIST ( typeCast2 )+ ) ;
    public final java.util.List<AstNode> typeCast2List() throws RecognitionException {
        java.util.List<AstNode> e = null;


        AstNode typeCast232 =null;


        e = new java.util.ArrayList<AstNode>();
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:105:3: ( ^( TYPECAST2LIST ( typeCast2 )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:105:6: ^( TYPECAST2LIST ( typeCast2 )+ )
            {
            match(input,TYPECAST2LIST,FOLLOW_TYPECAST2LIST_in_typeCast2List522); 

            match(input, Token.DOWN, null); 
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:105:22: ( typeCast2 )+
            int cnt13=0;
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0 >= TYPE1 && LA13_0 <= TYPE3)) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:105:23: typeCast2
            	    {
            	    pushFollow(FOLLOW_typeCast2_in_typeCast2List525);
            	    typeCast232=typeCast2();

            	    state._fsp--;


            	    e.add(typeCast232);

            	    }
            	    break;

            	default :
            	    if ( cnt13 >= 1 ) break loop13;
                        EarlyExitException eee =
                            new EarlyExitException(13, input);
                        throw eee;
                }
                cnt13++;
            } while (true);


            match(input, Token.UP, null); 


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return e;
    }
    // $ANTLR end "typeCast2List"



    // $ANTLR start "objectInvocation2"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:108:2: objectInvocation2 returns [AstNode node] : ^( FUNC_EXP typeCast1 typeCast2List ) ;
    public final AstNode objectInvocation2() throws RecognitionException {
        AstNode node = null;


        AstNode typeCast133 =null;

        java.util.List<AstNode> typeCast2List34 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:109:3: ( ^( FUNC_EXP typeCast1 typeCast2List ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:109:6: ^( FUNC_EXP typeCast1 typeCast2List )
            {
            match(input,FUNC_EXP,FOLLOW_FUNC_EXP_in_objectInvocation2550); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_typeCast1_in_objectInvocation2552);
            typeCast133=typeCast1();

            state._fsp--;


            pushFollow(FOLLOW_typeCast2List_in_objectInvocation2554);
            typeCast2List34=typeCast2List();

            state._fsp--;


            match(input, Token.UP, null); 


            node = new ObjectInvocation2Node(typeCast133 , typeCast2List34); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "objectInvocation2"



    // $ANTLR start "objectInvocationMethod"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:112:1: objectInvocationMethod returns [ObjectInvocationBase node] : ( ^( FUNC_CALL FormType Identifier ( exprList )? ) | ^( FUNC_CALL FormType Identifier paramList ) | ^( FUNC_CALL FormType block ) );
    public final ObjectInvocationBase objectInvocationMethod() throws RecognitionException {
        ObjectInvocationBase node = null;


        CommonTree Identifier35=null;
        CommonTree Identifier37=null;
        java.util.List<AstNode> exprList36 =null;

        java.util.List<AssignmentNode> paramList38 =null;

        AstNode block39 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:113:3: ( ^( FUNC_CALL FormType Identifier ( exprList )? ) | ^( FUNC_CALL FormType Identifier paramList ) | ^( FUNC_CALL FormType block ) )
            int alt15=3;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==FUNC_CALL) ) {
                int LA15_1 = input.LA(2);

                if ( (LA15_1==DOWN) ) {
                    int LA15_2 = input.LA(3);

                    if ( (LA15_2==FormType) ) {
                        int LA15_3 = input.LA(4);

                        if ( (LA15_3==Identifier) ) {
                            int LA15_4 = input.LA(5);

                            if ( (LA15_4==UP||LA15_4==EXP_LIST) ) {
                                alt15=1;
                            }
                            else if ( (LA15_4==ASSIGNMENT_LIST) ) {
                                alt15=2;
                            }
                            else {
                                NoViableAltException nvae =
                                    new NoViableAltException("", 15, 4, input);

                                throw nvae;

                            }
                        }
                        else if ( (LA15_3==BLOCK) ) {
                            alt15=3;
                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 15, 3, input);

                            throw nvae;

                        }
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 15, 2, input);

                        throw nvae;

                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 15, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;

            }
            switch (alt15) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:113:5: ^( FUNC_CALL FormType Identifier ( exprList )? )
                    {
                    match(input,FUNC_CALL,FOLLOW_FUNC_CALL_in_objectInvocationMethod576); 

                    match(input, Token.DOWN, null); 
                    match(input,FormType,FOLLOW_FormType_in_objectInvocationMethod578); 

                    Identifier35=(CommonTree)match(input,Identifier,FOLLOW_Identifier_in_objectInvocationMethod580); 

                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:113:37: ( exprList )?
                    int alt14=2;
                    int LA14_0 = input.LA(1);

                    if ( (LA14_0==EXP_LIST) ) {
                        alt14=1;
                    }
                    switch (alt14) {
                        case 1 :
                            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:113:37: exprList
                            {
                            pushFollow(FOLLOW_exprList_in_objectInvocationMethod582);
                            exprList36=exprList();

                            state._fsp--;


                            }
                            break;

                    }


                    match(input, Token.UP, null); 


                    node = new FunctionCallArguments((Identifier35!=null?Identifier35.getText():null), exprList36);

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:114:5: ^( FUNC_CALL FormType Identifier paramList )
                    {
                    match(input,FUNC_CALL,FOLLOW_FUNC_CALL_in_objectInvocationMethod593); 

                    match(input, Token.DOWN, null); 
                    match(input,FormType,FOLLOW_FormType_in_objectInvocationMethod595); 

                    Identifier37=(CommonTree)match(input,Identifier,FOLLOW_Identifier_in_objectInvocationMethod597); 

                    pushFollow(FOLLOW_paramList_in_objectInvocationMethod599);
                    paramList38=paramList();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new ParameterListFuncCallArgument((Identifier37!=null?Identifier37.getText():null), paramList38);

                    }
                    break;
                case 3 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:115:5: ^( FUNC_CALL FormType block )
                    {
                    match(input,FUNC_CALL,FOLLOW_FUNC_CALL_in_objectInvocationMethod610); 

                    match(input, Token.DOWN, null); 
                    match(input,FormType,FOLLOW_FormType_in_objectInvocationMethod612); 

                    pushFollow(FOLLOW_block_in_objectInvocationMethod614);
                    block39=block();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new BlockFuncCallArgument("output", block39);

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "objectInvocationMethod"



    // $ANTLR start "objectInvocation"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:118:1: objectInvocation returns [FormCallNode node] : ^( FUNC_CALL FormType typeCast1 objectInvocationMethod ) ;
    public final FormCallNode objectInvocation() throws RecognitionException {
        FormCallNode node = null;


        ObjectInvocationBase objectInvocationMethod40 =null;

        AstNode typeCast141 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:119:3: ( ^( FUNC_CALL FormType typeCast1 objectInvocationMethod ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:119:6: ^( FUNC_CALL FormType typeCast1 objectInvocationMethod )
            {
            match(input,FUNC_CALL,FOLLOW_FUNC_CALL_in_objectInvocation637); 

            match(input, Token.DOWN, null); 
            match(input,FormType,FOLLOW_FormType_in_objectInvocation639); 

            pushFollow(FOLLOW_typeCast1_in_objectInvocation641);
            typeCast141=typeCast1();

            state._fsp--;


            pushFollow(FOLLOW_objectInvocationMethod_in_objectInvocation643);
            objectInvocationMethod40=objectInvocationMethod();

            state._fsp--;


            match(input, Token.UP, null); 


             node = new FormCallNode(objectInvocationMethod40, typeCast141);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "objectInvocation"



    // $ANTLR start "ifStatement"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:122:1: ifStatement returns [AstNode node] : ( ^( IF1 ifStat[ifNode] ( elseStat[ifNode] )? ) | ^( IF2 ( ifsStat[ifNode] )+ ( elseStat[ifNode] )? ) );
    public final AstNode ifStatement() throws RecognitionException {
        AstNode node = null;


        IfNode ifNode = new IfNode();
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:125:3: ( ^( IF1 ifStat[ifNode] ( elseStat[ifNode] )? ) | ^( IF2 ( ifsStat[ifNode] )+ ( elseStat[ifNode] )? ) )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==IF1) ) {
                alt19=1;
            }
            else if ( (LA19_0==IF2) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;

            }
            switch (alt19) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:125:6: ^( IF1 ifStat[ifNode] ( elseStat[ifNode] )? )
                    {
                    match(input,IF1,FOLLOW_IF1_in_ifStatement677); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_ifStat_in_ifStatement679);
                    ifStat(ifNode);

                    state._fsp--;


                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:125:27: ( elseStat[ifNode] )?
                    int alt16=2;
                    int LA16_0 = input.LA(1);

                    if ( (LA16_0==EXP) ) {
                        alt16=1;
                    }
                    switch (alt16) {
                        case 1 :
                            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:125:27: elseStat[ifNode]
                            {
                            pushFollow(FOLLOW_elseStat_in_ifStatement682);
                            elseStat(ifNode);

                            state._fsp--;


                            }
                            break;

                    }


                    match(input, Token.UP, null); 


                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:126:6: ^( IF2 ( ifsStat[ifNode] )+ ( elseStat[ifNode] )? )
                    {
                    match(input,IF2,FOLLOW_IF2_in_ifStatement693); 

                    match(input, Token.DOWN, null); 
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:126:12: ( ifsStat[ifNode] )+
                    int cnt17=0;
                    loop17:
                    do {
                        int alt17=2;
                        int LA17_0 = input.LA(1);

                        if ( (LA17_0==EXP) ) {
                            int LA17_1 = input.LA(2);

                            if ( (LA17_1==DOWN) ) {
                                int LA17_3 = input.LA(3);

                                if ( (LA17_3==ARRAYCALL||LA17_3==BELONGS||LA17_3==Bool||LA17_3==CTOR||(LA17_3 >= EXP && LA17_3 <= EXPARRAY)||(LA17_3 >= FUNC_DEF && LA17_3 <= FUNC_EXP)||LA17_3==IDENTIFIER||LA17_3==In||(LA17_3 >= NAMECLASS && LA17_3 <= NEGATE)||(LA17_3 >= Null && LA17_3 <= Number)||LA17_3==String||LA17_3==TERNARY||LA17_3==UNARY_MIN||(LA17_3 >= 102 && LA17_3 <= 104)||(LA17_3 >= 107 && LA17_3 <= 108)||LA17_3==110||LA17_3==112||(LA17_3 >= 115 && LA17_3 <= 116)||(LA17_3 >= 118 && LA17_3 <= 120)||LA17_3==124||LA17_3==128) ) {
                                    alt17=1;
                                }


                            }


                        }


                        switch (alt17) {
                    	case 1 :
                    	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:126:12: ifsStat[ifNode]
                    	    {
                    	    pushFollow(FOLLOW_ifsStat_in_ifStatement695);
                    	    ifsStat(ifNode);

                    	    state._fsp--;


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt17 >= 1 ) break loop17;
                                EarlyExitException eee =
                                    new EarlyExitException(17, input);
                                throw eee;
                        }
                        cnt17++;
                    } while (true);


                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:126:29: ( elseStat[ifNode] )?
                    int alt18=2;
                    int LA18_0 = input.LA(1);

                    if ( (LA18_0==EXP) ) {
                        alt18=1;
                    }
                    switch (alt18) {
                        case 1 :
                            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:126:29: elseStat[ifNode]
                            {
                            pushFollow(FOLLOW_elseStat_in_ifStatement699);
                            elseStat(ifNode);

                            state._fsp--;


                            }
                            break;

                    }


                    match(input, Token.UP, null); 


                    }
                    break;

            }
            node = ifNode;
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "ifStatement"



    // $ANTLR start "ifStat"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:129:1: ifStat[IfNode parent] : ^( EXP ifexpression block ) ;
    public final void ifStat(IfNode parent) throws RecognitionException {
        AstNode ifexpression42 =null;

        AstNode block43 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:130:3: ( ^( EXP ifexpression block ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:130:6: ^( EXP ifexpression block )
            {
            match(input,EXP,FOLLOW_EXP_in_ifStat718); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_ifexpression_in_ifStat720);
            ifexpression42=ifexpression();

            state._fsp--;


            pushFollow(FOLLOW_block_in_ifStat722);
            block43=block();

            state._fsp--;


            match(input, Token.UP, null); 


            parent.addIf("if", ifexpression42, block43, currentScope);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "ifStat"



    // $ANTLR start "ifsStat"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:133:1: ifsStat[IfNode parent] : ^( EXP ifexpression block ) ;
    public final void ifsStat(IfNode parent) throws RecognitionException {
        AstNode ifexpression44 =null;

        AstNode block45 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:134:2: ( ^( EXP ifexpression block ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:134:5: ^( EXP ifexpression block )
            {
            match(input,EXP,FOLLOW_EXP_in_ifsStat740); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_ifexpression_in_ifsStat743);
            ifexpression44=ifexpression();

            state._fsp--;


            pushFollow(FOLLOW_block_in_ifsStat745);
            block45=block();

            state._fsp--;


            match(input, Token.UP, null); 


            parent.addChoices("if", ifexpression44, block45, currentScope);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "ifsStat"



    // $ANTLR start "elseStat"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:137:1: elseStat[IfNode parent] : ^( EXP block ) ;
    public final void elseStat(IfNode parent) throws RecognitionException {
        AstNode block46 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:138:3: ( ^( EXP block ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:138:6: ^( EXP block )
            {
            match(input,EXP,FOLLOW_EXP_in_elseStat763); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_block_in_elseStat765);
            block46=block();

            state._fsp--;


            match(input, Token.UP, null); 


            parent.addElse("else", block46, currentScope);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "elseStat"



    // $ANTLR start "ifexpression"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:141:1: ifexpression returns [AstNode node] : ( belongs | expression );
    public final AstNode ifexpression() throws RecognitionException {
        AstNode node = null;


        AstNode belongs47 =null;

        AstNode expression48 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:142:3: ( belongs | expression )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==BELONGS) ) {
                alt20=1;
            }
            else if ( (LA20_0==ARRAYCALL||LA20_0==Bool||LA20_0==CTOR||(LA20_0 >= EXP && LA20_0 <= EXPARRAY)||(LA20_0 >= FUNC_DEF && LA20_0 <= FUNC_EXP)||LA20_0==IDENTIFIER||LA20_0==In||(LA20_0 >= NAMECLASS && LA20_0 <= NEGATE)||(LA20_0 >= Null && LA20_0 <= Number)||LA20_0==String||LA20_0==TERNARY||LA20_0==UNARY_MIN||(LA20_0 >= 102 && LA20_0 <= 104)||(LA20_0 >= 107 && LA20_0 <= 108)||LA20_0==110||LA20_0==112||(LA20_0 >= 115 && LA20_0 <= 116)||(LA20_0 >= 118 && LA20_0 <= 120)||LA20_0==124||LA20_0==128) ) {
                alt20=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;

            }
            switch (alt20) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:142:6: belongs
                    {
                    pushFollow(FOLLOW_belongs_in_ifexpression786);
                    belongs47=belongs();

                    state._fsp--;


                    node = belongs47;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:143:6: expression
                    {
                    pushFollow(FOLLOW_expression_in_ifexpression795);
                    expression48=expression();

                    state._fsp--;


                    node = expression48;

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "ifexpression"



    // $ANTLR start "belongs"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:146:1: belongs returns [AstNode node] : ^( BELONGS ( lookup sizeBelongs ) ) ;
    public final AstNode belongs() throws RecognitionException {
        AstNode node = null;


        AstNode lookup49 =null;

        java.util.List<String> sizeBelongs50 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:147:3: ( ^( BELONGS ( lookup sizeBelongs ) ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:147:6: ^( BELONGS ( lookup sizeBelongs ) )
            {
            match(input,BELONGS,FOLLOW_BELONGS_in_belongs816); 

            match(input, Token.DOWN, null); 
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:147:16: ( lookup sizeBelongs )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:147:17: lookup sizeBelongs
            {
            pushFollow(FOLLOW_lookup_in_belongs819);
            lookup49=lookup();

            state._fsp--;


            pushFollow(FOLLOW_sizeBelongs_in_belongs821);
            sizeBelongs50=sizeBelongs();

            state._fsp--;


            node = new BelongsNode(lookup49, sizeBelongs50, currentScope);

            }


            match(input, Token.UP, null); 


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "belongs"



    // $ANTLR start "sizeBelongs"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:150:1: sizeBelongs returns [java.util.List<String> e] : ( size | numList );
    public final java.util.List<String> sizeBelongs() throws RecognitionException {
        java.util.List<String> e = null;


        String size51 =null;

        java.util.List<String> numList52 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:151:3: ( size | numList )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==SIZE) ) {
                alt21=1;
            }
            else if ( (LA21_0==NUMLIST) ) {
                alt21=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;

            }
            switch (alt21) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:151:6: size
                    {
                    pushFollow(FOLLOW_size_in_sizeBelongs843);
                    size51=size();

                    state._fsp--;


                     e = Arrays.asList(size51.split("\\.."));

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:152:6: numList
                    {
                    pushFollow(FOLLOW_numList_in_sizeBelongs852);
                    numList52=numList();

                    state._fsp--;


                    e = numList52;

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return e;
    }
    // $ANTLR end "sizeBelongs"



    // $ANTLR start "size"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:155:1: size returns [String text] : ^( SIZE (a= Number b= Number ) ) ;
    public final String size() throws RecognitionException {
        String text = null;


        CommonTree a=null;
        CommonTree b=null;

         java.lang.StringBuilder sb = new java.lang.StringBuilder(); 
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:158:2: ( ^( SIZE (a= Number b= Number ) ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:158:5: ^( SIZE (a= Number b= Number ) )
            {
            match(input,SIZE,FOLLOW_SIZE_in_size882); 

            match(input, Token.DOWN, null); 
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:158:12: (a= Number b= Number )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:158:13: a= Number b= Number
            {
            a=(CommonTree)match(input,Number,FOLLOW_Number_in_size887); 

            b=(CommonTree)match(input,Number,FOLLOW_Number_in_size891); 

            sb = sb.append(a).append("..").append(b);

            }


            match(input, Token.UP, null); 


            }

            text = sb.toString();
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return text;
    }
    // $ANTLR end "size"



    // $ANTLR start "numList"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:161:1: numList returns [java.util.List<String> e] : ^( NUMLIST ( Number )+ ) ;
    public final java.util.List<String> numList() throws RecognitionException {
        java.util.List<String> e = null;


        CommonTree Number53=null;

        e = new java.util.ArrayList<String>();
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:163:2: ( ^( NUMLIST ( Number )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:163:5: ^( NUMLIST ( Number )+ )
            {
            match(input,NUMLIST,FOLLOW_NUMLIST_in_numList919); 

            match(input, Token.DOWN, null); 
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:163:15: ( Number )+
            int cnt22=0;
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==Number) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:163:16: Number
            	    {
            	    Number53=(CommonTree)match(input,Number,FOLLOW_Number_in_numList922); 

            	    e.add((Number53!=null?Number53.getText():null));

            	    }
            	    break;

            	default :
            	    if ( cnt22 >= 1 ) break loop22;
                        EarlyExitException eee =
                            new EarlyExitException(22, input);
                        throw eee;
                }
                cnt22++;
            } while (true);


            match(input, Token.UP, null); 


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return e;
    }
    // $ANTLR end "numList"



    // $ANTLR start "caseAssigment"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:166:1: caseAssigment returns [AstNode node] : ^( CASEASSIGMENT lookup caseStatement ) ;
    public final AstNode caseAssigment() throws RecognitionException {
        AstNode node = null;


        AstNode lookup54 =null;

        AstNode caseStatement55 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:167:3: ( ^( CASEASSIGMENT lookup caseStatement ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:167:5: ^( CASEASSIGMENT lookup caseStatement )
            {
            match(input,CASEASSIGMENT,FOLLOW_CASEASSIGMENT_in_caseAssigment944); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_lookup_in_caseAssigment946);
            lookup54=lookup();

            state._fsp--;


            pushFollow(FOLLOW_caseStatement_in_caseAssigment948);
            caseStatement55=caseStatement();

            state._fsp--;


            match(input, Token.UP, null); 


            node = new CaseAssigmentNode(lookup54, caseStatement55, currentScope);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "caseAssigment"



    // $ANTLR start "caseStatement"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:170:1: caseStatement returns [AstNode node] : ( ^( CASE1 caseStat[caseNode] elsecaseStat[caseNode] ) | ^( CASE2 ( casesStat[caseNode] )+ elsecaseStat[caseNode] ) );
    public final AstNode caseStatement() throws RecognitionException {
        AstNode node = null;


        CaseNode caseNode = new CaseNode();
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:173:3: ( ^( CASE1 caseStat[caseNode] elsecaseStat[caseNode] ) | ^( CASE2 ( casesStat[caseNode] )+ elsecaseStat[caseNode] ) )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==CASE1) ) {
                alt24=1;
            }
            else if ( (LA24_0==CASE2) ) {
                alt24=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;

            }
            switch (alt24) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:173:6: ^( CASE1 caseStat[caseNode] elsecaseStat[caseNode] )
                    {
                    match(input,CASE1,FOLLOW_CASE1_in_caseStatement981); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_caseStat_in_caseStatement983);
                    caseStat(caseNode);

                    state._fsp--;


                    pushFollow(FOLLOW_elsecaseStat_in_caseStatement986);
                    elsecaseStat(caseNode);

                    state._fsp--;


                    match(input, Token.UP, null); 


                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:174:6: ^( CASE2 ( casesStat[caseNode] )+ elsecaseStat[caseNode] )
                    {
                    match(input,CASE2,FOLLOW_CASE2_in_caseStatement996); 

                    match(input, Token.DOWN, null); 
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:174:14: ( casesStat[caseNode] )+
                    int cnt23=0;
                    loop23:
                    do {
                        int alt23=2;
                        int LA23_0 = input.LA(1);

                        if ( (LA23_0==CASEEXP) ) {
                            alt23=1;
                        }


                        switch (alt23) {
                    	case 1 :
                    	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:174:14: casesStat[caseNode]
                    	    {
                    	    pushFollow(FOLLOW_casesStat_in_caseStatement998);
                    	    casesStat(caseNode);

                    	    state._fsp--;


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt23 >= 1 ) break loop23;
                                EarlyExitException eee =
                                    new EarlyExitException(23, input);
                                throw eee;
                        }
                        cnt23++;
                    } while (true);


                    pushFollow(FOLLOW_elsecaseStat_in_caseStatement1002);
                    elsecaseStat(caseNode);

                    state._fsp--;


                    match(input, Token.UP, null); 


                    }
                    break;

            }
            node = caseNode;
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "caseStatement"



    // $ANTLR start "caseStat"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:177:1: caseStat[CaseNode node] : ^( CASEEXP expression caseStatOrExpr ) ;
    public final void caseStat(CaseNode node) throws RecognitionException {
        AstNode expression56 =null;

        AstNode caseStatOrExpr57 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:178:3: ( ^( CASEEXP expression caseStatOrExpr ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:178:6: ^( CASEEXP expression caseStatOrExpr )
            {
            match(input,CASEEXP,FOLLOW_CASEEXP_in_caseStat1021); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_expression_in_caseStat1023);
            expression56=expression();

            state._fsp--;


            pushFollow(FOLLOW_caseStatOrExpr_in_caseStat1025);
            caseStatOrExpr57=caseStatOrExpr();

            state._fsp--;


            match(input, Token.UP, null); 


            node.addCase("if", expression56, caseStatOrExpr57);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "caseStat"



    // $ANTLR start "casesStat"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:181:1: casesStat[CaseNode node] : ^( CASEEXP expression caseStatOrExpr ) ;
    public final void casesStat(CaseNode node) throws RecognitionException {
        AstNode expression58 =null;

        AstNode caseStatOrExpr59 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:182:3: ( ^( CASEEXP expression caseStatOrExpr ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:182:6: ^( CASEEXP expression caseStatOrExpr )
            {
            match(input,CASEEXP,FOLLOW_CASEEXP_in_casesStat1044); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_expression_in_casesStat1047);
            expression58=expression();

            state._fsp--;


            pushFollow(FOLLOW_caseStatOrExpr_in_casesStat1049);
            caseStatOrExpr59=caseStatOrExpr();

            state._fsp--;


            match(input, Token.UP, null); 


            node.addChoiceCase("if", expression58, caseStatOrExpr59);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "casesStat"



    // $ANTLR start "elsecaseStat"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:185:1: elsecaseStat[CaseNode node] : ^( CASEEXPELSE expression ) ;
    public final void elsecaseStat(CaseNode node) throws RecognitionException {
        AstNode expression60 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:186:3: ( ^( CASEEXPELSE expression ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:186:6: ^( CASEEXPELSE expression )
            {
            match(input,CASEEXPELSE,FOLLOW_CASEEXPELSE_in_elsecaseStat1070); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_expression_in_elsecaseStat1072);
            expression60=expression();

            state._fsp--;


            match(input, Token.UP, null); 


            node.addDefault("else", expression60);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return ;
    }
    // $ANTLR end "elsecaseStat"



    // $ANTLR start "caseStatOrExpr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:189:1: caseStatOrExpr returns [AstNode node] : ( caseStatement | expression );
    public final AstNode caseStatOrExpr() throws RecognitionException {
        AstNode node = null;


        AstNode caseStatement61 =null;

        AstNode expression62 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:190:3: ( caseStatement | expression )
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( ((LA25_0 >= CASE1 && LA25_0 <= CASE2)) ) {
                alt25=1;
            }
            else if ( (LA25_0==ARRAYCALL||LA25_0==Bool||LA25_0==CTOR||(LA25_0 >= EXP && LA25_0 <= EXPARRAY)||(LA25_0 >= FUNC_DEF && LA25_0 <= FUNC_EXP)||LA25_0==IDENTIFIER||LA25_0==In||(LA25_0 >= NAMECLASS && LA25_0 <= NEGATE)||(LA25_0 >= Null && LA25_0 <= Number)||LA25_0==String||LA25_0==TERNARY||LA25_0==UNARY_MIN||(LA25_0 >= 102 && LA25_0 <= 104)||(LA25_0 >= 107 && LA25_0 <= 108)||LA25_0==110||LA25_0==112||(LA25_0 >= 115 && LA25_0 <= 116)||(LA25_0 >= 118 && LA25_0 <= 120)||LA25_0==124||LA25_0==128) ) {
                alt25=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;

            }
            switch (alt25) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:190:6: caseStatement
                    {
                    pushFollow(FOLLOW_caseStatement_in_caseStatOrExpr1093);
                    caseStatement61=caseStatement();

                    state._fsp--;


                    node = caseStatement61;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:191:6: expression
                    {
                    pushFollow(FOLLOW_expression_in_caseStatOrExpr1102);
                    expression62=expression();

                    state._fsp--;


                    node = expression62;

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "caseStatOrExpr"



    // $ANTLR start "forStatement"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:194:1: forStatement returns [AstNode node] : ^( For Identifier a= expression b= expression (s= expression )? block ) ;
    public final AstNode forStatement() throws RecognitionException {
        AstNode node = null;


        CommonTree Identifier63=null;
        AstNode a =null;

        AstNode b =null;

        AstNode s =null;

        AstNode block64 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:195:3: ( ^( For Identifier a= expression b= expression (s= expression )? block ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:195:6: ^( For Identifier a= expression b= expression (s= expression )? block )
            {
            match(input,For,FOLLOW_For_in_forStatement1126); 

            match(input, Token.DOWN, null); 
            Identifier63=(CommonTree)match(input,Identifier,FOLLOW_Identifier_in_forStatement1128); 

            pushFollow(FOLLOW_expression_in_forStatement1132);
            a=expression();

            state._fsp--;


            pushFollow(FOLLOW_expression_in_forStatement1136);
            b=expression();

            state._fsp--;


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:195:50: (s= expression )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==ARRAYCALL||LA26_0==Bool||LA26_0==CTOR||(LA26_0 >= EXP && LA26_0 <= EXPARRAY)||(LA26_0 >= FUNC_DEF && LA26_0 <= FUNC_EXP)||LA26_0==IDENTIFIER||LA26_0==In||(LA26_0 >= NAMECLASS && LA26_0 <= NEGATE)||(LA26_0 >= Null && LA26_0 <= Number)||LA26_0==String||LA26_0==TERNARY||LA26_0==UNARY_MIN||(LA26_0 >= 102 && LA26_0 <= 104)||(LA26_0 >= 107 && LA26_0 <= 108)||LA26_0==110||LA26_0==112||(LA26_0 >= 115 && LA26_0 <= 116)||(LA26_0 >= 118 && LA26_0 <= 120)||LA26_0==124||LA26_0==128) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:195:50: s= expression
                    {
                    pushFollow(FOLLOW_expression_in_forStatement1140);
                    s=expression();

                    state._fsp--;


                    }
                    break;

            }


            pushFollow(FOLLOW_block_in_forStatement1143);
            block64=block();

            state._fsp--;


            match(input, Token.UP, null); 


            node = new ForStatementNode((Identifier63!=null?Identifier63.getText():null), a, b, s, block64, currentScope);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "forStatement"



    // $ANTLR start "whileStatement"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:198:1: whileStatement returns [AstNode node] : ^( While expression block ) ;
    public final AstNode whileStatement() throws RecognitionException {
        AstNode node = null;


        AstNode expression65 =null;

        AstNode block66 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:199:3: ( ^( While expression block ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:199:6: ^( While expression block )
            {
            match(input,While,FOLLOW_While_in_whileStatement1165); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_expression_in_whileStatement1167);
            expression65=expression();

            state._fsp--;


            pushFollow(FOLLOW_block_in_whileStatement1169);
            block66=block();

            state._fsp--;


            match(input, Token.UP, null); 


            node = new WhileStatementNode(expression65, block66);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "whileStatement"



    // $ANTLR start "idList"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:202:1: idList returns [String text] : ^( ID_LIST ( Identifier )+ ) ;
    public final String idList() throws RecognitionException {
        String text = null;


        CommonTree Identifier67=null;

         java.lang.StringBuilder sb = new java.lang.StringBuilder(); 
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:205:3: ( ^( ID_LIST ( Identifier )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:205:6: ^( ID_LIST ( Identifier )+ )
            {
            match(input,ID_LIST,FOLLOW_ID_LIST_in_idList1201); 

            match(input, Token.DOWN, null); 
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:205:16: ( Identifier )+
            int cnt27=0;
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==Identifier) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:205:17: Identifier
            	    {
            	    Identifier67=(CommonTree)match(input,Identifier,FOLLOW_Identifier_in_idList1204); 

            	     sb = sb.length() > 0 ? sb.append("." + (Identifier67!=null?Identifier67.getText():null)) : sb.append((Identifier67!=null?Identifier67.getText():null));

            	    }
            	    break;

            	default :
            	    if ( cnt27 >= 1 ) break loop27;
                        EarlyExitException eee =
                            new EarlyExitException(27, input);
                        throw eee;
                }
                cnt27++;
            } while (true);


            match(input, Token.UP, null); 


            }

            text = sb.toString();
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return text;
    }
    // $ANTLR end "idList"



    // $ANTLR start "exprList"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:208:1: exprList returns [java.util.List<AstNode> e] : ^( EXP_LIST ( expression )+ ) ;
    public final java.util.List<AstNode> exprList() throws RecognitionException {
        java.util.List<AstNode> e = null;


        AstNode expression68 =null;


        e = new java.util.ArrayList<AstNode>();
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:210:3: ( ^( EXP_LIST ( expression )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:210:6: ^( EXP_LIST ( expression )+ )
            {
            match(input,EXP_LIST,FOLLOW_EXP_LIST_in_exprList1234); 

            match(input, Token.DOWN, null); 
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:210:17: ( expression )+
            int cnt28=0;
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==ARRAYCALL||LA28_0==Bool||LA28_0==CTOR||(LA28_0 >= EXP && LA28_0 <= EXPARRAY)||(LA28_0 >= FUNC_DEF && LA28_0 <= FUNC_EXP)||LA28_0==IDENTIFIER||LA28_0==In||(LA28_0 >= NAMECLASS && LA28_0 <= NEGATE)||(LA28_0 >= Null && LA28_0 <= Number)||LA28_0==String||LA28_0==TERNARY||LA28_0==UNARY_MIN||(LA28_0 >= 102 && LA28_0 <= 104)||(LA28_0 >= 107 && LA28_0 <= 108)||LA28_0==110||LA28_0==112||(LA28_0 >= 115 && LA28_0 <= 116)||(LA28_0 >= 118 && LA28_0 <= 120)||LA28_0==124||LA28_0==128) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:210:18: expression
            	    {
            	    pushFollow(FOLLOW_expression_in_exprList1237);
            	    expression68=expression();

            	    state._fsp--;


            	    e.add(expression68);

            	    }
            	    break;

            	default :
            	    if ( cnt28 >= 1 ) break loop28;
                        EarlyExitException eee =
                            new EarlyExitException(28, input);
                        throw eee;
                }
                cnt28++;
            } while (true);


            match(input, Token.UP, null); 


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return e;
    }
    // $ANTLR end "exprList"



    // $ANTLR start "paramList"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:213:1: paramList returns [java.util.List<AssignmentNode> e] : ^( ASSIGNMENT_LIST ( assignment )+ ) ;
    public final java.util.List<AssignmentNode> paramList() throws RecognitionException {
        java.util.List<AssignmentNode> e = null;


        AstNode assignment69 =null;


        e = new java.util.ArrayList<AssignmentNode>();
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:215:3: ( ^( ASSIGNMENT_LIST ( assignment )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:215:6: ^( ASSIGNMENT_LIST ( assignment )+ )
            {
            match(input,ASSIGNMENT_LIST,FOLLOW_ASSIGNMENT_LIST_in_paramList1267); 

            match(input, Token.DOWN, null); 
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:215:24: ( assignment )+
            int cnt29=0;
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==ASSIGNMENT) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:215:25: assignment
            	    {
            	    pushFollow(FOLLOW_assignment_in_paramList1270);
            	    assignment69=assignment();

            	    state._fsp--;


            	    e.add((AssignmentNode)assignment69);

            	    }
            	    break;

            	default :
            	    if ( cnt29 >= 1 ) break loop29;
                        EarlyExitException eee =
                            new EarlyExitException(29, input);
                        throw eee;
                }
                cnt29++;
            } while (true);


            match(input, Token.UP, null); 


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return e;
    }
    // $ANTLR end "paramList"



    // $ANTLR start "array"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:218:1: array returns [AstNode node] : ^( ARRAY sizes Type ) ;
    public final AstNode array() throws RecognitionException {
        AstNode node = null;


        CommonTree Type71=null;
        java.util.List<String> sizes70 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:219:3: ( ^( ARRAY sizes Type ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:219:6: ^( ARRAY sizes Type )
            {
            match(input,ARRAY,FOLLOW_ARRAY_in_array1294); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_sizes_in_array1296);
            sizes70=sizes();

            state._fsp--;


            Type71=(CommonTree)match(input,Type,FOLLOW_Type_in_array1298); 

            match(input, Token.UP, null); 


             node = new ArrayNode(sizes70, (Type71!=null?Type71.getText():null)); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "array"



    // $ANTLR start "sizes"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:222:1: sizes returns [java.util.List<String> e] : ^( SIZES ( size )+ ) ;
    public final java.util.List<String> sizes() throws RecognitionException {
        java.util.List<String> e = null;


        String size72 =null;


        e = new java.util.ArrayList<String>();
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:224:3: ( ^( SIZES ( size )+ ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:224:6: ^( SIZES ( size )+ )
            {
            match(input,SIZES,FOLLOW_SIZES_in_sizes1329); 

            match(input, Token.DOWN, null); 
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:224:14: ( size )+
            int cnt30=0;
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==SIZE) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:224:15: size
            	    {
            	    pushFollow(FOLLOW_size_in_sizes1332);
            	    size72=size();

            	    state._fsp--;


            	    e.add(size72);

            	    }
            	    break;

            	default :
            	    if ( cnt30 >= 1 ) break loop30;
                        EarlyExitException eee =
                            new EarlyExitException(30, input);
                        throw eee;
                }
                cnt30++;
            } while (true);


            match(input, Token.UP, null); 


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return e;
    }
    // $ANTLR end "sizes"



    // $ANTLR start "lookuparr"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:227:1: lookuparr returns [AstNode node] : ( formlookup | arraycall );
    public final AstNode lookuparr() throws RecognitionException {
        AstNode node = null;


        AstNode formlookup73 =null;

        AstNode arraycall74 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:228:3: ( formlookup | arraycall )
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==LOOKUP) ) {
                alt31=1;
            }
            else if ( (LA31_0==ARRAYCALL) ) {
                alt31=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 31, 0, input);

                throw nvae;

            }
            switch (alt31) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:228:6: formlookup
                    {
                    pushFollow(FOLLOW_formlookup_in_lookuparr1355);
                    formlookup73=formlookup();

                    state._fsp--;


                    node =  formlookup73;

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:229:6: arraycall
                    {
                    pushFollow(FOLLOW_arraycall_in_lookuparr1365);
                    arraycall74=arraycall();

                    state._fsp--;


                    node = arraycall74;

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "lookuparr"



    // $ANTLR start "arraycall"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:232:1: arraycall returns [AstNode node] : ^( ARRAYCALL Identifier exprList ) ;
    public final AstNode arraycall() throws RecognitionException {
        AstNode node = null;


        CommonTree Identifier75=null;
        java.util.List<AstNode> exprList76 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:233:3: ( ^( ARRAYCALL Identifier exprList ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:233:7: ^( ARRAYCALL Identifier exprList )
            {
            match(input,ARRAYCALL,FOLLOW_ARRAYCALL_in_arraycall1387); 

            match(input, Token.DOWN, null); 
            Identifier75=(CommonTree)match(input,Identifier,FOLLOW_Identifier_in_arraycall1389); 

            pushFollow(FOLLOW_exprList_in_arraycall1391);
            exprList76=exprList();

            state._fsp--;


            match(input, Token.UP, null); 


             node = new ArrayCallNode((Identifier75!=null?Identifier75.getText():null), exprList76, currentScope); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "arraycall"



    // $ANTLR start "expression"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:236:1: expression returns [AstNode node] : ( ^( TERNARY a= expression b= expression c= expression ) | ^( In a= expression b= expression ) | ^( '||' a= expression b= expression ) | ^( '&&' a= expression b= expression ) | ^( '==' a= expression b= expression ) | ^( '!=' a= expression b= expression ) | ^( '>=' a= expression b= expression ) | ^( '<=' a= expression b= expression ) | ^( '>' a= expression b= expression ) | ^( '<' a= expression b= expression ) | ^( '+' a= expression b= expression ) | ^( '-' a= expression b= expression ) | ^( '*' a= expression b= expression ) | ^( '/' a= expression b= expression ) | ^( '%' a= expression b= expression ) | ^( '^' a= expression b= expression ) | ^( UNARY_MIN a= expression ) | ^( NEGATE a= expression ) | Number | Bool | Null | String | arraycall | identifier | nameClass | objectInvocation2 | ^( CTOR 'Form' paramList ) | ^( FUNC_DEF block ) | ^( EXP functionCall ) | ^( EXPARRAY array ) );
    public final AstNode expression() throws RecognitionException {
        AstNode node = null;


        CommonTree Number77=null;
        CommonTree Bool78=null;
        CommonTree String79=null;
        AstNode a =null;

        AstNode b =null;

        AstNode c =null;

        AstNode arraycall80 =null;

        AstNode identifier81 =null;

        AstNode nameClass82 =null;

        AstNode objectInvocation283 =null;

        java.util.List<AssignmentNode> paramList84 =null;

        AstNode block85 =null;

        AstNode functionCall86 =null;

        AstNode array87 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:237:3: ( ^( TERNARY a= expression b= expression c= expression ) | ^( In a= expression b= expression ) | ^( '||' a= expression b= expression ) | ^( '&&' a= expression b= expression ) | ^( '==' a= expression b= expression ) | ^( '!=' a= expression b= expression ) | ^( '>=' a= expression b= expression ) | ^( '<=' a= expression b= expression ) | ^( '>' a= expression b= expression ) | ^( '<' a= expression b= expression ) | ^( '+' a= expression b= expression ) | ^( '-' a= expression b= expression ) | ^( '*' a= expression b= expression ) | ^( '/' a= expression b= expression ) | ^( '%' a= expression b= expression ) | ^( '^' a= expression b= expression ) | ^( UNARY_MIN a= expression ) | ^( NEGATE a= expression ) | Number | Bool | Null | String | arraycall | identifier | nameClass | objectInvocation2 | ^( CTOR 'Form' paramList ) | ^( FUNC_DEF block ) | ^( EXP functionCall ) | ^( EXPARRAY array ) )
            int alt32=30;
            switch ( input.LA(1) ) {
            case TERNARY:
                {
                alt32=1;
                }
                break;
            case In:
                {
                alt32=2;
                }
                break;
            case 128:
                {
                alt32=3;
                }
                break;
            case 104:
                {
                alt32=4;
                }
                break;
            case 118:
                {
                alt32=5;
                }
                break;
            case 102:
                {
                alt32=6;
                }
                break;
            case 120:
                {
                alt32=7;
                }
                break;
            case 116:
                {
                alt32=8;
                }
                break;
            case 119:
                {
                alt32=9;
                }
                break;
            case 115:
                {
                alt32=10;
                }
                break;
            case 108:
                {
                alt32=11;
                }
                break;
            case 110:
                {
                alt32=12;
                }
                break;
            case 107:
                {
                alt32=13;
                }
                break;
            case 112:
                {
                alt32=14;
                }
                break;
            case 103:
                {
                alt32=15;
                }
                break;
            case 124:
                {
                alt32=16;
                }
                break;
            case UNARY_MIN:
                {
                alt32=17;
                }
                break;
            case NEGATE:
                {
                alt32=18;
                }
                break;
            case Number:
                {
                alt32=19;
                }
                break;
            case Bool:
                {
                alt32=20;
                }
                break;
            case Null:
                {
                alt32=21;
                }
                break;
            case String:
                {
                alt32=22;
                }
                break;
            case ARRAYCALL:
                {
                alt32=23;
                }
                break;
            case IDENTIFIER:
                {
                alt32=24;
                }
                break;
            case NAMECLASS:
                {
                alt32=25;
                }
                break;
            case FUNC_EXP:
                {
                alt32=26;
                }
                break;
            case CTOR:
                {
                alt32=27;
                }
                break;
            case FUNC_DEF:
                {
                alt32=28;
                }
                break;
            case EXP:
                {
                alt32=29;
                }
                break;
            case EXPARRAY:
                {
                alt32=30;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;

            }

            switch (alt32) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:237:6: ^( TERNARY a= expression b= expression c= expression )
                    {
                    match(input,TERNARY,FOLLOW_TERNARY_in_expression1416); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1420);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1424);
                    b=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1428);
                    c=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new TernaryNode(a, b, c);

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:238:6: ^( In a= expression b= expression )
                    {
                    match(input,In,FOLLOW_In_in_expression1439); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1443);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1447);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new InNode(a, b);

                    }
                    break;
                case 3 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:239:6: ^( '||' a= expression b= expression )
                    {
                    match(input,128,FOLLOW_128_in_expression1476); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1480);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1484);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new OrNode(a, b);

                    }
                    break;
                case 4 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:240:6: ^( '&&' a= expression b= expression )
                    {
                    match(input,104,FOLLOW_104_in_expression1511); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1515);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1519);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new AndNode(a, b);

                    }
                    break;
                case 5 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:241:6: ^( '==' a= expression b= expression )
                    {
                    match(input,118,FOLLOW_118_in_expression1546); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1550);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1554);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new EqualsNode(a, b);

                    }
                    break;
                case 6 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:242:6: ^( '!=' a= expression b= expression )
                    {
                    match(input,102,FOLLOW_102_in_expression1581); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1585);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1589);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new NotEqualsNode(a, b);

                    }
                    break;
                case 7 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:243:6: ^( '>=' a= expression b= expression )
                    {
                    match(input,120,FOLLOW_120_in_expression1616); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1620);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1624);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new GTEqualsNode(a, b);

                    }
                    break;
                case 8 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:244:6: ^( '<=' a= expression b= expression )
                    {
                    match(input,116,FOLLOW_116_in_expression1651); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1655);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1659);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new LTEqualsNode(a, b);

                    }
                    break;
                case 9 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:245:6: ^( '>' a= expression b= expression )
                    {
                    match(input,119,FOLLOW_119_in_expression1686); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1690);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1694);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new GTNode(a, b);

                    }
                    break;
                case 10 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:246:6: ^( '<' a= expression b= expression )
                    {
                    match(input,115,FOLLOW_115_in_expression1722); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1726);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1730);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new LTNode(a, b);

                    }
                    break;
                case 11 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:247:6: ^( '+' a= expression b= expression )
                    {
                    match(input,108,FOLLOW_108_in_expression1758); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1762);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1766);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new AddNode(a, b);

                    }
                    break;
                case 12 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:248:6: ^( '-' a= expression b= expression )
                    {
                    match(input,110,FOLLOW_110_in_expression1794); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1798);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1802);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new SubNode(a, b);

                    }
                    break;
                case 13 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:249:6: ^( '*' a= expression b= expression )
                    {
                    match(input,107,FOLLOW_107_in_expression1830); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1834);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1838);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new MulNode(a, b);

                    }
                    break;
                case 14 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:250:6: ^( '/' a= expression b= expression )
                    {
                    match(input,112,FOLLOW_112_in_expression1866); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1870);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1874);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new DivNode(a, b);

                    }
                    break;
                case 15 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:251:6: ^( '%' a= expression b= expression )
                    {
                    match(input,103,FOLLOW_103_in_expression1902); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1906);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1910);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new ModNode(a, b);

                    }
                    break;
                case 16 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:252:6: ^( '^' a= expression b= expression )
                    {
                    match(input,124,FOLLOW_124_in_expression1938); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1942);
                    a=expression();

                    state._fsp--;


                    pushFollow(FOLLOW_expression_in_expression1946);
                    b=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new PowNode(a, b);

                    }
                    break;
                case 17 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:253:6: ^( UNARY_MIN a= expression )
                    {
                    match(input,UNARY_MIN,FOLLOW_UNARY_MIN_in_expression1974); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression1978);
                    a=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new UnaryMinusNode(a);

                    }
                    break;
                case 18 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:254:6: ^( NEGATE a= expression )
                    {
                    match(input,NEGATE,FOLLOW_NEGATE_in_expression2013); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_expression_in_expression2017);
                    a=expression();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new NegateNode(a);

                    }
                    break;
                case 19 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:255:6: Number
                    {
                    Number77=(CommonTree)match(input,Number,FOLLOW_Number_in_expression2054); 

                    node = new AtomNode(Double.parseDouble((Number77!=null?Number77.getText():null)));

                    }
                    break;
                case 20 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:256:6: Bool
                    {
                    Bool78=(CommonTree)match(input,Bool,FOLLOW_Bool_in_expression2106); 

                    node = new AtomNode(Boolean.parseBoolean((Bool78!=null?Bool78.getText():null)));

                    }
                    break;
                case 21 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:257:6: Null
                    {
                    match(input,Null,FOLLOW_Null_in_expression2160); 

                    node = new AtomNode(null);

                    }
                    break;
                case 22 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:258:6: String
                    {
                    String79=(CommonTree)match(input,String,FOLLOW_String_in_expression2214); 

                    node = new AtomNode((String79!=null?String79.getText():null));

                    }
                    break;
                case 23 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:259:6: arraycall
                    {
                    pushFollow(FOLLOW_arraycall_in_expression2266);
                    arraycall80=arraycall();

                    state._fsp--;


                    node = arraycall80;

                    }
                    break;
                case 24 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:260:6: identifier
                    {
                    pushFollow(FOLLOW_identifier_in_expression2300);
                    identifier81=identifier();

                    state._fsp--;


                    node = identifier81;

                    }
                    break;
                case 25 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:261:6: nameClass
                    {
                    pushFollow(FOLLOW_nameClass_in_expression2328);
                    nameClass82=nameClass();

                    state._fsp--;


                    node = nameClass82;

                    }
                    break;
                case 26 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:262:6: objectInvocation2
                    {
                    pushFollow(FOLLOW_objectInvocation2_in_expression2356);
                    objectInvocation283=objectInvocation2();

                    state._fsp--;


                    node = objectInvocation283;

                    }
                    break;
                case 27 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:263:6: ^( CTOR 'Form' paramList )
                    {
                    match(input,CTOR,FOLLOW_CTOR_in_expression2370); 

                    match(input, Token.DOWN, null); 
                    match(input,Form,FOLLOW_Form_in_expression2372); 

                    pushFollow(FOLLOW_paramList_in_expression2374);
                    paramList84=paramList();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = new FormNode(paramList84);

                    }
                    break;
                case 28 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:264:6: ^( FUNC_DEF block )
                    {
                    match(input,FUNC_DEF,FOLLOW_FUNC_DEF_in_expression2410); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_block_in_expression2412);
                    block85=block();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = block85;

                    }
                    break;
                case 29 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:265:6: ^( EXP functionCall )
                    {
                    match(input,EXP,FOLLOW_EXP_in_expression2455); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_functionCall_in_expression2457);
                    functionCall86=functionCall();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = functionCall86;

                    }
                    break;
                case 30 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:266:6: ^( EXPARRAY array )
                    {
                    match(input,EXPARRAY,FOLLOW_EXPARRAY_in_expression2478); 

                    match(input, Token.DOWN, null); 
                    pushFollow(FOLLOW_array_in_expression2480);
                    array87=array();

                    state._fsp--;


                    match(input, Token.UP, null); 


                    node = array87;

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "expression"



    // $ANTLR start "list"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:269:1: list returns [AstNode node] : ^( LIST ( exprList )? ) ;
    public final AstNode list() throws RecognitionException {
        AstNode node = null;


        java.util.List<AstNode> exprList88 =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:270:3: ( ^( LIST ( exprList )? ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:270:6: ^( LIST ( exprList )? )
            {
            match(input,LIST,FOLLOW_LIST_in_list2520); 

            if ( input.LA(1)==Token.DOWN ) {
                match(input, Token.DOWN, null); 
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:270:13: ( exprList )?
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( (LA33_0==EXP_LIST) ) {
                    alt33=1;
                }
                switch (alt33) {
                    case 1 :
                        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:270:13: exprList
                        {
                        pushFollow(FOLLOW_exprList_in_list2522);
                        exprList88=exprList();

                        state._fsp--;


                        }
                        break;

                }


                match(input, Token.UP, null); 
            }


            node = new ListNode(exprList88);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "list"



    // $ANTLR start "lookup"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:273:1: lookup returns [AstNode node] : ^( LOOKUP i= idList ) ;
    public final AstNode lookup() throws RecognitionException {
        AstNode node = null;


        String i =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:274:3: ( ^( LOOKUP i= idList ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:274:7: ^( LOOKUP i= idList )
            {
            match(input,LOOKUP,FOLLOW_LOOKUP_in_lookup2546); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_idList_in_lookup2550);
            i=idList();

            state._fsp--;


            match(input, Token.UP, null); 


            node = new IdentifierNode(i, currentScope); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "lookup"



    // $ANTLR start "formlookup"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:277:1: formlookup returns [AstNode node] : ^( LOOKUP i= idList ) ;
    public final AstNode formlookup() throws RecognitionException {
        AstNode node = null;


        String i =null;


        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:278:3: ( ^( LOOKUP i= idList ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:278:7: ^( LOOKUP i= idList )
            {
            match(input,LOOKUP,FOLLOW_LOOKUP_in_formlookup2575); 

            match(input, Token.DOWN, null); 
            pushFollow(FOLLOW_idList_in_formlookup2579);
            i=idList();

            state._fsp--;


            match(input, Token.UP, null); 


            node = new IdentifierNode(i, currentScope); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "formlookup"



    // $ANTLR start "identifier"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:281:1: identifier returns [AstNode node] : ^( IDENTIFIER Identifier ) ;
    public final AstNode identifier() throws RecognitionException {
        AstNode node = null;


        CommonTree Identifier89=null;

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:282:3: ( ^( IDENTIFIER Identifier ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:282:5: ^( IDENTIFIER Identifier )
            {
            match(input,IDENTIFIER,FOLLOW_IDENTIFIER_in_identifier2602); 

            match(input, Token.DOWN, null); 
            Identifier89=(CommonTree)match(input,Identifier,FOLLOW_Identifier_in_identifier2604); 

            match(input, Token.UP, null); 


            node = new IdentifierNode((Identifier89!=null?Identifier89.getText():null), currentScope);

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "identifier"



    // $ANTLR start "nameClass"
    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:285:1: nameClass returns [AstNode node] : ( ^( NAMECLASS Array ) | ^( NAMECLASS Form ) | ^( NAMECLASS Subwindow ) | ^( NAMECLASS Button ) | ^( NAMECLASS File ) | ^( NAMECLASS Panel ) | ^( NAMECLASS TextLabel ) | ^( NAMECLASS TextField ) );
    public final AstNode nameClass() throws RecognitionException {
        AstNode node = null;


        CommonTree Array90=null;
        CommonTree Form91=null;
        CommonTree Subwindow92=null;
        CommonTree Button93=null;
        CommonTree File94=null;
        CommonTree Panel95=null;
        CommonTree TextLabel96=null;
        CommonTree TextField97=null;

        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:286:3: ( ^( NAMECLASS Array ) | ^( NAMECLASS Form ) | ^( NAMECLASS Subwindow ) | ^( NAMECLASS Button ) | ^( NAMECLASS File ) | ^( NAMECLASS Panel ) | ^( NAMECLASS TextLabel ) | ^( NAMECLASS TextField ) )
            int alt34=8;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==NAMECLASS) ) {
                int LA34_1 = input.LA(2);

                if ( (LA34_1==DOWN) ) {
                    switch ( input.LA(3) ) {
                    case Array:
                        {
                        alt34=1;
                        }
                        break;
                    case Form:
                        {
                        alt34=2;
                        }
                        break;
                    case Subwindow:
                        {
                        alt34=3;
                        }
                        break;
                    case Button:
                        {
                        alt34=4;
                        }
                        break;
                    case File:
                        {
                        alt34=5;
                        }
                        break;
                    case Panel:
                        {
                        alt34=6;
                        }
                        break;
                    case TextLabel:
                        {
                        alt34=7;
                        }
                        break;
                    case TextField:
                        {
                        alt34=8;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 34, 2, input);

                        throw nvae;

                    }

                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 34, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;

            }
            switch (alt34) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:286:5: ^( NAMECLASS Array )
                    {
                    match(input,NAMECLASS,FOLLOW_NAMECLASS_in_nameClass2627); 

                    match(input, Token.DOWN, null); 
                    Array90=(CommonTree)match(input,Array,FOLLOW_Array_in_nameClass2629); 

                    match(input, Token.UP, null); 


                    node = new IdentifierNode((Array90!=null?Array90.getText():null), currentScope);

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:287:5: ^( NAMECLASS Form )
                    {
                    match(input,NAMECLASS,FOLLOW_NAMECLASS_in_nameClass2639); 

                    match(input, Token.DOWN, null); 
                    Form91=(CommonTree)match(input,Form,FOLLOW_Form_in_nameClass2641); 

                    match(input, Token.UP, null); 


                    node = new IdentifierNode((Form91!=null?Form91.getText():null), currentScope);

                    }
                    break;
                case 3 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:288:5: ^( NAMECLASS Subwindow )
                    {
                    match(input,NAMECLASS,FOLLOW_NAMECLASS_in_nameClass2653); 

                    match(input, Token.DOWN, null); 
                    Subwindow92=(CommonTree)match(input,Subwindow,FOLLOW_Subwindow_in_nameClass2655); 

                    match(input, Token.UP, null); 


                    node = new IdentifierNode((Subwindow92!=null?Subwindow92.getText():null), currentScope);

                    }
                    break;
                case 4 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:289:5: ^( NAMECLASS Button )
                    {
                    match(input,NAMECLASS,FOLLOW_NAMECLASS_in_nameClass2666); 

                    match(input, Token.DOWN, null); 
                    Button93=(CommonTree)match(input,Button,FOLLOW_Button_in_nameClass2668); 

                    match(input, Token.UP, null); 


                    node = new IdentifierNode((Button93!=null?Button93.getText():null), currentScope);

                    }
                    break;
                case 5 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:290:5: ^( NAMECLASS File )
                    {
                    match(input,NAMECLASS,FOLLOW_NAMECLASS_in_nameClass2679); 

                    match(input, Token.DOWN, null); 
                    File94=(CommonTree)match(input,File,FOLLOW_File_in_nameClass2681); 

                    match(input, Token.UP, null); 


                    node = new IdentifierNode((File94!=null?File94.getText():null), currentScope);

                    }
                    break;
                case 6 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:291:5: ^( NAMECLASS Panel )
                    {
                    match(input,NAMECLASS,FOLLOW_NAMECLASS_in_nameClass2692); 

                    match(input, Token.DOWN, null); 
                    Panel95=(CommonTree)match(input,Panel,FOLLOW_Panel_in_nameClass2694); 

                    match(input, Token.UP, null); 


                    node = new IdentifierNode((Panel95!=null?Panel95.getText():null), currentScope);

                    }
                    break;
                case 7 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:292:5: ^( NAMECLASS TextLabel )
                    {
                    match(input,NAMECLASS,FOLLOW_NAMECLASS_in_nameClass2705); 

                    match(input, Token.DOWN, null); 
                    TextLabel96=(CommonTree)match(input,TextLabel,FOLLOW_TextLabel_in_nameClass2707); 

                    match(input, Token.UP, null); 


                    node = new IdentifierNode((TextLabel96!=null?TextLabel96.getText():null), currentScope);

                    }
                    break;
                case 8 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\BarsicTreeWalker.g:293:5: ^( NAMECLASS TextField )
                    {
                    match(input,NAMECLASS,FOLLOW_NAMECLASS_in_nameClass2718); 

                    match(input, Token.DOWN, null); 
                    TextField97=(CommonTree)match(input,TextField,FOLLOW_TextField_in_nameClass2720); 

                    match(input, Token.UP, null); 


                    node = new IdentifierNode((TextField97!=null?TextField97.getText():null), currentScope);

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
        }
        return node;
    }
    // $ANTLR end "nameClass"

    // Delegated rules


 

    public static final BitSet FOLLOW_block_in_walk50 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BLOCK_in_block80 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_STATEMENTS_in_block83 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_statement_in_block86 = new BitSet(new long[]{0x0060118000040048L,0x0000001000000100L});
    public static final BitSet FOLLOW_RETURN_in_block94 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_block97 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_assignment_in_statement121 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_objectInvocation_in_statement134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ifStatement_in_statement145 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_forStatement_in_statement157 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_whileStatement_in_statement168 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_procedure_in_statement177 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_functionCall_in_statement191 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_caseAssigment_in_statement202 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FUNCTIONCALL_in_functionCall225 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Identifier_in_functionCall227 = new BitSet(new long[]{0x0000000200000008L});
    public static final BitSet FOLLOW_exprList_in_functionCall229 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PROCEDURE_in_procedure254 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Identifier_in_procedure256 = new BitSet(new long[]{0x0004000000000880L,0x0000000000000200L});
    public static final BitSet FOLLOW_procedureParameter_in_procedure258 = new BitSet(new long[]{0x0004000000000880L});
    public static final BitSet FOLLOW_identifiertList_in_procedure263 = new BitSet(new long[]{0x0000000000000880L});
    public static final BitSet FOLLOW_paramList_in_procedure267 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_block_in_procedure271 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_PROCEDUREPARAMETER_in_procedureParameter302 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_identifiertList_and_type_in_procedureParameter305 = new BitSet(new long[]{0x0002000000000008L});
    public static final BitSet FOLLOW_IDENTIFIERLIST_AND_TYPE_in_identifiertList_and_type328 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_identifiertList_in_identifiertList_and_type330 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
    public static final BitSet FOLLOW_Type_in_identifiertList_and_type332 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_IDENTIFIER_LIST_in_identifiertList359 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Identifier_in_identifiertList362 = new BitSet(new long[]{0x0080000000000008L});
    public static final BitSet FOLLOW_ASSIGNMENT_in_assignment386 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_lookuparr_in_assignment388 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_assignment390 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_ASSIGNMENT_in_assignment401 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_identifier_in_assignment403 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_paramList_in_assignment405 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_TYPECASTEXP_in_typeCast1428 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_typeCast1430 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_TYPECASTID_in_typeCast1440 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_identifier_in_typeCast1443 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_TYPE1_in_typeCast2465 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_typeCast2467 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_TYPE2_in_typeCast2476 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_functionCall_in_typeCast2478 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_TYPE3_in_typeCast2487 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_identifier_in_typeCast2490 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_TYPECAST2LIST_in_typeCast2List522 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_typeCast2_in_typeCast2List525 = new BitSet(new long[]{0x0000000000000008L,0x0000000003800000L});
    public static final BitSet FOLLOW_FUNC_EXP_in_objectInvocation2550 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_typeCast1_in_objectInvocation2552 = new BitSet(new long[]{0x0000000000000000L,0x0000000008000000L});
    public static final BitSet FOLLOW_typeCast2List_in_objectInvocation2554 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_FUNC_CALL_in_objectInvocationMethod576 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_FormType_in_objectInvocationMethod578 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_Identifier_in_objectInvocationMethod580 = new BitSet(new long[]{0x0000000200000008L});
    public static final BitSet FOLLOW_exprList_in_objectInvocationMethod582 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_FUNC_CALL_in_objectInvocationMethod593 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_FormType_in_objectInvocationMethod595 = new BitSet(new long[]{0x0080000000000000L});
    public static final BitSet FOLLOW_Identifier_in_objectInvocationMethod597 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_paramList_in_objectInvocationMethod599 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_FUNC_CALL_in_objectInvocationMethod610 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_FormType_in_objectInvocationMethod612 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_block_in_objectInvocationMethod614 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_FUNC_CALL_in_objectInvocation637 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_FormType_in_objectInvocation639 = new BitSet(new long[]{0x0000000000000000L,0x0000000030000000L});
    public static final BitSet FOLLOW_typeCast1_in_objectInvocation641 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_objectInvocationMethod_in_objectInvocation643 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_IF1_in_ifStatement677 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ifStat_in_ifStatement679 = new BitSet(new long[]{0x0000000080000008L});
    public static final BitSet FOLLOW_elseStat_in_ifStatement682 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_IF2_in_ifStatement693 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ifsStat_in_ifStatement695 = new BitSet(new long[]{0x0000000080000008L});
    public static final BitSet FOLLOW_elseStat_in_ifStatement699 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_EXP_in_ifStat718 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ifexpression_in_ifStat720 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_block_in_ifStat722 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_EXP_in_ifsStat740 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_ifexpression_in_ifsStat743 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_block_in_ifsStat745 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_EXP_in_elseStat763 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_block_in_elseStat765 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_belongs_in_ifexpression786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_ifexpression795 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BELONGS_in_belongs816 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_lookup_in_belongs819 = new BitSet(new long[]{0x0000000000000000L,0x0000000000008002L});
    public static final BitSet FOLLOW_sizeBelongs_in_belongs821 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_size_in_sizeBelongs843 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_numList_in_sizeBelongs852 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SIZE_in_size882 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Number_in_size887 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_Number_in_size891 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NUMLIST_in_numList919 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Number_in_numList922 = new BitSet(new long[]{0x0000000000000008L,0x0000000000000010L});
    public static final BitSet FOLLOW_CASEASSIGMENT_in_caseAssigment944 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_lookup_in_caseAssigment946 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_caseStatement_in_caseAssigment948 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CASE1_in_caseStatement981 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_caseStat_in_caseStatement983 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_elsecaseStat_in_caseStatement986 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CASE2_in_caseStatement996 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_casesStat_in_caseStatement998 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_elsecaseStat_in_caseStatement1002 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CASEEXP_in_caseStat1021 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_caseStat1023 = new BitSet(new long[]{0x8401060180832020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_caseStatOrExpr_in_caseStat1025 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CASEEXP_in_casesStat1044 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_casesStat1047 = new BitSet(new long[]{0x8401060180832020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_caseStatOrExpr_in_casesStat1049 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_CASEEXPELSE_in_elsecaseStat1070 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_elsecaseStat1072 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_caseStatement_in_caseStatOrExpr1093 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_expression_in_caseStatOrExpr1102 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_For_in_forStatement1126 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Identifier_in_forStatement1128 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_forStatement1132 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_forStatement1136 = new BitSet(new long[]{0x8401060180802820L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_forStatement1140 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_block_in_forStatement1143 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_While_in_whileStatement1165 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_whileStatement1167 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_block_in_whileStatement1169 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_ID_LIST_in_idList1201 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Identifier_in_idList1204 = new BitSet(new long[]{0x0080000000000008L});
    public static final BitSet FOLLOW_EXP_LIST_in_exprList1234 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_exprList1237 = new BitSet(new long[]{0x8401060180802028L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_ASSIGNMENT_LIST_in_paramList1267 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_assignment_in_paramList1270 = new BitSet(new long[]{0x0000000000000048L});
    public static final BitSet FOLLOW_ARRAY_in_array1294 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_sizes_in_array1296 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
    public static final BitSet FOLLOW_Type_in_array1298 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_SIZES_in_sizes1329 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_size_in_sizes1332 = new BitSet(new long[]{0x0000000000000008L,0x0000000000008000L});
    public static final BitSet FOLLOW_formlookup_in_lookuparr1355 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_arraycall_in_lookuparr1365 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ARRAYCALL_in_arraycall1387 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Identifier_in_arraycall1389 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_exprList_in_arraycall1391 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_TERNARY_in_expression1416 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1420 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1424 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1428 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_In_in_expression1439 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1443 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1447 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_128_in_expression1476 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1480 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1484 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_104_in_expression1511 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1515 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1519 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_118_in_expression1546 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1550 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1554 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_102_in_expression1581 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1585 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1589 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_120_in_expression1616 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1620 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1624 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_116_in_expression1651 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1655 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1659 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_119_in_expression1686 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1690 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1694 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_115_in_expression1722 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1726 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1730 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_108_in_expression1758 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1762 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1766 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_110_in_expression1794 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1798 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1802 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_107_in_expression1830 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1834 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1838 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_112_in_expression1866 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1870 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1874 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_103_in_expression1902 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1906 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1910 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_124_in_expression1938 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1942 = new BitSet(new long[]{0x8401060180802020L,0x11D959C400500019L,0x0000000000000001L});
    public static final BitSet FOLLOW_expression_in_expression1946 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_UNARY_MIN_in_expression1974 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression1978 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NEGATE_in_expression2013 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_expression_in_expression2017 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_Number_in_expression2054 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Bool_in_expression2106 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_Null_in_expression2160 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_String_in_expression2214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_arraycall_in_expression2266 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_identifier_in_expression2300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_nameClass_in_expression2328 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_objectInvocation2_in_expression2356 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CTOR_in_expression2370 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Form_in_expression2372 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_paramList_in_expression2374 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_FUNC_DEF_in_expression2410 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_block_in_expression2412 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_EXP_in_expression2455 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_functionCall_in_expression2457 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_EXPARRAY_in_expression2478 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_array_in_expression2480 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LIST_in_list2520 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_exprList_in_list2522 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LOOKUP_in_lookup2546 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_idList_in_lookup2550 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_LOOKUP_in_formlookup2575 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_idList_in_formlookup2579 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_IDENTIFIER_in_identifier2602 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Identifier_in_identifier2604 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NAMECLASS_in_nameClass2627 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Array_in_nameClass2629 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NAMECLASS_in_nameClass2639 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Form_in_nameClass2641 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NAMECLASS_in_nameClass2653 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Subwindow_in_nameClass2655 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NAMECLASS_in_nameClass2666 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Button_in_nameClass2668 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NAMECLASS_in_nameClass2679 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_File_in_nameClass2681 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NAMECLASS_in_nameClass2692 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_Panel_in_nameClass2694 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NAMECLASS_in_nameClass2705 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_TextLabel_in_nameClass2707 = new BitSet(new long[]{0x0000000000000008L});
    public static final BitSet FOLLOW_NAMECLASS_in_nameClass2718 = new BitSet(new long[]{0x0000000000000004L});
    public static final BitSet FOLLOW_TextField_in_nameClass2720 = new BitSet(new long[]{0x0000000000000008L});

}