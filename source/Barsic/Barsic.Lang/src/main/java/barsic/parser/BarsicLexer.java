// $ANTLR 3.4 C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g 2018-05-29 20:22:11

  package barsic.parser;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class BarsicLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__101=101;
    public static final int T__102=102;
    public static final int T__103=103;
    public static final int T__104=104;
    public static final int T__105=105;
    public static final int T__106=106;
    public static final int T__107=107;
    public static final int T__108=108;
    public static final int T__109=109;
    public static final int T__110=110;
    public static final int T__111=111;
    public static final int T__112=112;
    public static final int T__113=113;
    public static final int T__114=114;
    public static final int T__115=115;
    public static final int T__116=116;
    public static final int T__117=117;
    public static final int T__118=118;
    public static final int T__119=119;
    public static final int T__120=120;
    public static final int T__121=121;
    public static final int T__122=122;
    public static final int T__123=123;
    public static final int T__124=124;
    public static final int T__125=125;
    public static final int T__126=126;
    public static final int T__127=127;
    public static final int T__128=128;
    public static final int ARRAY=4;
    public static final int ARRAYCALL=5;
    public static final int ASSIGNMENT=6;
    public static final int ASSIGNMENT_LIST=7;
    public static final int And=8;
    public static final int Array=9;
    public static final int BELONGS=10;
    public static final int BLOCK=11;
    public static final int Begin=12;
    public static final int Bool=13;
    public static final int Button=14;
    public static final int CASE=15;
    public static final int CASE1=16;
    public static final int CASE2=17;
    public static final int CASEASSIGMENT=18;
    public static final int CASEEXP=19;
    public static final int CASEEXPELSE=20;
    public static final int CBrace=21;
    public static final int CLOSE=22;
    public static final int CTOR=23;
    public static final int Case=24;
    public static final int Close=25;
    public static final int CloseIdentifier=26;
    public static final int Comment=27;
    public static final int Def=28;
    public static final int Digit=29;
    public static final int Do=30;
    public static final int EXP=31;
    public static final int EXPARRAY=32;
    public static final int EXP_LIST=33;
    public static final int Else=34;
    public static final int End=35;
    public static final int EndCase=36;
    public static final int EndDo=37;
    public static final int EndIf=38;
    public static final int FUNCTIONCALL=39;
    public static final int FUNC_CALL=40;
    public static final int FUNC_DEF=41;
    public static final int FUNC_EXP=42;
    public static final int File=43;
    public static final int For=44;
    public static final int Form=45;
    public static final int FormType=46;
    public static final int Hence=47;
    public static final int IDENTIFIER=48;
    public static final int IDENTIFIERLIST_AND_TYPE=49;
    public static final int IDENTIFIER_LIST=50;
    public static final int IDFORMLIST=51;
    public static final int ID_LIST=52;
    public static final int IF1=53;
    public static final int IF2=54;
    public static final int Identifier=55;
    public static final int If=56;
    public static final int Import=57;
    public static final int In=58;
    public static final int Infinity=59;
    public static final int Int=60;
    public static final int LIST=61;
    public static final int LOOKUP=62;
    public static final int NAMECLASS=63;
    public static final int NEGATE=64;
    public static final int NUMLIST=65;
    public static final int New=66;
    public static final int Null=67;
    public static final int Number=68;
    public static final int OBrace=69;
    public static final int Of=70;
    public static final int Or=71;
    public static final int PROCEDURE=72;
    public static final int PROCEDUREPARAMETER=73;
    public static final int Panel=74;
    public static final int Println=75;
    public static final int Procedure=76;
    public static final int RETURN=77;
    public static final int Return=78;
    public static final int SIZE=79;
    public static final int SIZES=80;
    public static final int STATEMENTS=81;
    public static final int Space=82;
    public static final int Step=83;
    public static final int String=84;
    public static final int Subwindow=85;
    public static final int TERNARY=86;
    public static final int TYPE1=87;
    public static final int TYPE2=88;
    public static final int TYPE3=89;
    public static final int TYPE4=90;
    public static final int TYPECAST2LIST=91;
    public static final int TYPECASTEXP=92;
    public static final int TYPECASTID=93;
    public static final int TextField=94;
    public static final int TextLabel=95;
    public static final int To=96;
    public static final int Type=97;
    public static final int UNARY_MIN=98;
    public static final int Variables=99;
    public static final int While=100;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public BarsicLexer() {} 
    public BarsicLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public BarsicLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g"; }

    // $ANTLR start "T__101"
    public final void mT__101() throws RecognitionException {
        try {
            int _type = T__101;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:6:8: ( '!' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:6:10: '!'
            {
            match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__101"

    // $ANTLR start "T__102"
    public final void mT__102() throws RecognitionException {
        try {
            int _type = T__102;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:7:8: ( '!=' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:7:10: '!='
            {
            match("!="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__102"

    // $ANTLR start "T__103"
    public final void mT__103() throws RecognitionException {
        try {
            int _type = T__103;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:8:8: ( '%' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:8:10: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__103"

    // $ANTLR start "T__104"
    public final void mT__104() throws RecognitionException {
        try {
            int _type = T__104;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:9:8: ( '&&' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:9:10: '&&'
            {
            match("&&"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__104"

    // $ANTLR start "T__105"
    public final void mT__105() throws RecognitionException {
        try {
            int _type = T__105;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:10:8: ( '(' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:10:10: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__105"

    // $ANTLR start "T__106"
    public final void mT__106() throws RecognitionException {
        try {
            int _type = T__106;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:11:8: ( ')' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:11:10: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__106"

    // $ANTLR start "T__107"
    public final void mT__107() throws RecognitionException {
        try {
            int _type = T__107;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:12:8: ( '*' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:12:10: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__107"

    // $ANTLR start "T__108"
    public final void mT__108() throws RecognitionException {
        try {
            int _type = T__108;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:13:8: ( '+' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:13:10: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__108"

    // $ANTLR start "T__109"
    public final void mT__109() throws RecognitionException {
        try {
            int _type = T__109;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:14:8: ( ',' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:14:10: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__109"

    // $ANTLR start "T__110"
    public final void mT__110() throws RecognitionException {
        try {
            int _type = T__110;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:15:8: ( '-' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:15:10: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__110"

    // $ANTLR start "T__111"
    public final void mT__111() throws RecognitionException {
        try {
            int _type = T__111;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:16:8: ( '.' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:16:10: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__111"

    // $ANTLR start "T__112"
    public final void mT__112() throws RecognitionException {
        try {
            int _type = T__112;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:17:8: ( '/' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:17:10: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__112"

    // $ANTLR start "T__113"
    public final void mT__113() throws RecognitionException {
        try {
            int _type = T__113;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:18:8: ( ':' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:18:10: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__113"

    // $ANTLR start "T__114"
    public final void mT__114() throws RecognitionException {
        try {
            int _type = T__114;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:19:8: ( ';' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:19:10: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__114"

    // $ANTLR start "T__115"
    public final void mT__115() throws RecognitionException {
        try {
            int _type = T__115;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:20:8: ( '<' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:20:10: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__115"

    // $ANTLR start "T__116"
    public final void mT__116() throws RecognitionException {
        try {
            int _type = T__116;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:21:8: ( '<=' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:21:10: '<='
            {
            match("<="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__116"

    // $ANTLR start "T__117"
    public final void mT__117() throws RecognitionException {
        try {
            int _type = T__117;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:22:8: ( '=' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:22:10: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__117"

    // $ANTLR start "T__118"
    public final void mT__118() throws RecognitionException {
        try {
            int _type = T__118;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:23:8: ( '==' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:23:10: '=='
            {
            match("=="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__118"

    // $ANTLR start "T__119"
    public final void mT__119() throws RecognitionException {
        try {
            int _type = T__119;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:24:8: ( '>' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:24:10: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__119"

    // $ANTLR start "T__120"
    public final void mT__120() throws RecognitionException {
        try {
            int _type = T__120;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:25:8: ( '>=' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:25:10: '>='
            {
            match(">="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__120"

    // $ANTLR start "T__121"
    public final void mT__121() throws RecognitionException {
        try {
            int _type = T__121;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:26:8: ( '?' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:26:10: '?'
            {
            match('?'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__121"

    // $ANTLR start "T__122"
    public final void mT__122() throws RecognitionException {
        try {
            int _type = T__122;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:27:8: ( '[' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:27:10: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__122"

    // $ANTLR start "T__123"
    public final void mT__123() throws RecognitionException {
        try {
            int _type = T__123;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:28:8: ( ']' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:28:10: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__123"

    // $ANTLR start "T__124"
    public final void mT__124() throws RecognitionException {
        try {
            int _type = T__124;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:29:8: ( '^' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:29:10: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__124"

    // $ANTLR start "T__125"
    public final void mT__125() throws RecognitionException {
        try {
            int _type = T__125;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:30:8: ( '_output' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:30:10: '_output'
            {
            match("_output"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__125"

    // $ANTLR start "T__126"
    public final void mT__126() throws RecognitionException {
        try {
            int _type = T__126;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:31:8: ( 'output' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:31:10: 'output'
            {
            match("output"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__126"

    // $ANTLR start "T__127"
    public final void mT__127() throws RecognitionException {
        try {
            int _type = T__127;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:32:8: ( '|' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:32:10: '|'
            {
            match('|'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__127"

    // $ANTLR start "T__128"
    public final void mT__128() throws RecognitionException {
        try {
            int _type = T__128;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:33:8: ( '||' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:33:10: '||'
            {
            match("||"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__128"

    // $ANTLR start "Type"
    public final void mType() throws RecognitionException {
        try {
            int _type = Type;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:365:7: ( 'Number' | 'Bool' | 'String' )
            int alt1=3;
            switch ( input.LA(1) ) {
            case 'N':
                {
                alt1=1;
                }
                break;
            case 'B':
                {
                alt1=2;
                }
                break;
            case 'S':
                {
                alt1=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;

            }

            switch (alt1) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:365:9: 'Number'
                    {
                    match("Number"); 



                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:365:19: 'Bool'
                    {
                    match("Bool"); 



                    }
                    break;
                case 3 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:365:27: 'String'
                    {
                    match("String"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Type"

    // $ANTLR start "Array"
    public final void mArray() throws RecognitionException {
        try {
            int _type = Array;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:367:9: ( 'Array' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:367:11: 'Array'
            {
            match("Array"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Array"

    // $ANTLR start "Form"
    public final void mForm() throws RecognitionException {
        try {
            int _type = Form;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:368:10: ( 'Form' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:368:12: 'Form'
            {
            match("Form"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Form"

    // $ANTLR start "Subwindow"
    public final void mSubwindow() throws RecognitionException {
        try {
            int _type = Subwindow;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:369:11: ( 'Subwindow' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:369:12: 'Subwindow'
            {
            match("Subwindow"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Subwindow"

    // $ANTLR start "Button"
    public final void mButton() throws RecognitionException {
        try {
            int _type = Button;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:370:8: ( 'Button' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:370:10: 'Button'
            {
            match("Button"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Button"

    // $ANTLR start "File"
    public final void mFile() throws RecognitionException {
        try {
            int _type = File;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:371:7: ( 'File' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:371:9: 'File'
            {
            match("File"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "File"

    // $ANTLR start "Panel"
    public final void mPanel() throws RecognitionException {
        try {
            int _type = Panel;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:372:7: ( 'Panel' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:372:9: 'Panel'
            {
            match("Panel"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Panel"

    // $ANTLR start "TextLabel"
    public final void mTextLabel() throws RecognitionException {
        try {
            int _type = TextLabel;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:373:11: ( 'TextLabel' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:373:13: 'TextLabel'
            {
            match("TextLabel"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TextLabel"

    // $ANTLR start "TextField"
    public final void mTextField() throws RecognitionException {
        try {
            int _type = TextField;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:374:11: ( 'TextField' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:374:17: 'TextField'
            {
            match("TextField"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TextField"

    // $ANTLR start "Of"
    public final void mOf() throws RecognitionException {
        try {
            int _type = Of;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:376:5: ( 'of' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:376:7: 'of'
            {
            match("of"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Of"

    // $ANTLR start "Import"
    public final void mImport() throws RecognitionException {
        try {
            int _type = Import;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:377:9: ( 'import' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:377:11: 'import'
            {
            match("import"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Import"

    // $ANTLR start "Variables"
    public final void mVariables() throws RecognitionException {
        try {
            int _type = Variables;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:378:13: ( 'variables' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:378:14: 'variables'
            {
            match("variables"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Variables"

    // $ANTLR start "Procedure"
    public final void mProcedure() throws RecognitionException {
        try {
            int _type = Procedure;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:380:10: ( 'procedure' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:380:12: 'procedure'
            {
            match("procedure"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Procedure"

    // $ANTLR start "Begin"
    public final void mBegin() throws RecognitionException {
        try {
            int _type = Begin;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:381:7: ( 'begin' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:381:10: 'begin'
            {
            match("begin"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Begin"

    // $ANTLR start "Infinity"
    public final void mInfinity() throws RecognitionException {
        try {
            int _type = Infinity;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:384:10: ( 'infinity' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:384:12: 'infinity'
            {
            match("infinity"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Infinity"

    // $ANTLR start "New"
    public final void mNew() throws RecognitionException {
        try {
            int _type = New;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:385:10: ( 'new' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:385:12: 'new'
            {
            match("new"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "New"

    // $ANTLR start "FormType"
    public final void mFormType() throws RecognitionException {
        try {
            int _type = FormType;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:388:10: ( 'FormType' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:388:12: 'FormType'
            {
            match("FormType"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FormType"

    // $ANTLR start "Println"
    public final void mPrintln() throws RecognitionException {
        try {
            int _type = Println;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:389:10: ( 'println' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:389:12: 'println'
            {
            match("println"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Println"

    // $ANTLR start "Def"
    public final void mDef() throws RecognitionException {
        try {
            int _type = Def;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:391:10: ( 'def' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:391:12: 'def'
            {
            match("def"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Def"

    // $ANTLR start "Return"
    public final void mReturn() throws RecognitionException {
        try {
            int _type = Return;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:393:10: ( 'return' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:393:12: 'return'
            {
            match("return"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Return"

    // $ANTLR start "For"
    public final void mFor() throws RecognitionException {
        try {
            int _type = For;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:394:10: ( 'for' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:394:12: 'for'
            {
            match("for"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "For"

    // $ANTLR start "While"
    public final void mWhile() throws RecognitionException {
        try {
            int _type = While;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:395:10: ( 'while' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:395:12: 'while'
            {
            match("while"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "While"

    // $ANTLR start "Step"
    public final void mStep() throws RecognitionException {
        try {
            int _type = Step;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:397:6: ( 'step' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:397:8: 'step'
            {
            match("step"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Step"

    // $ANTLR start "To"
    public final void mTo() throws RecognitionException {
        try {
            int _type = To;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:399:10: ( '..' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:399:12: '..'
            {
            match(".."); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "To"

    // $ANTLR start "Do"
    public final void mDo() throws RecognitionException {
        try {
            int _type = Do;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:400:10: ( 'do' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:400:12: 'do'
            {
            match("do"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Do"

    // $ANTLR start "EndDo"
    public final void mEndDo() throws RecognitionException {
        try {
            int _type = EndDo;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:401:10: ( '_do' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:401:12: '_do'
            {
            match("_do"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EndDo"

    // $ANTLR start "End"
    public final void mEnd() throws RecognitionException {
        try {
            int _type = End;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:403:10: ( 'end' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:403:12: 'end'
            {
            match("end"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "End"

    // $ANTLR start "In"
    public final void mIn() throws RecognitionException {
        try {
            int _type = In;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:404:10: ( 'in' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:404:12: 'in'
            {
            match("in"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "In"

    // $ANTLR start "Null"
    public final void mNull() throws RecognitionException {
        try {
            int _type = Null;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:405:10: ( 'null' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:405:12: 'null'
            {
            match("null"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Null"

    // $ANTLR start "Hence"
    public final void mHence() throws RecognitionException {
        try {
            int _type = Hence;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:406:10: ( '=>' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:406:12: '=>'
            {
            match("=>"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Hence"

    // $ANTLR start "If"
    public final void mIf() throws RecognitionException {
        try {
            int _type = If;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:408:10: ( 'if' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:408:12: 'if'
            {
            match("if"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "If"

    // $ANTLR start "Else"
    public final void mElse() throws RecognitionException {
        try {
            int _type = Else;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:409:10: ( 'else' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:409:12: 'else'
            {
            match("else"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Else"

    // $ANTLR start "EndIf"
    public final void mEndIf() throws RecognitionException {
        try {
            int _type = EndIf;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:410:10: ( '_if' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:410:12: '_if'
            {
            match("_if"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EndIf"

    // $ANTLR start "Case"
    public final void mCase() throws RecognitionException {
        try {
            int _type = Case;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:412:10: ( 'case' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:412:12: 'case'
            {
            match("case"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Case"

    // $ANTLR start "EndCase"
    public final void mEndCase() throws RecognitionException {
        try {
            int _type = EndCase;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:413:10: ( '_case' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:413:13: '_case'
            {
            match("_case"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EndCase"

    // $ANTLR start "OBrace"
    public final void mOBrace() throws RecognitionException {
        try {
            int _type = OBrace;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:415:10: ( '{' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:415:12: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OBrace"

    // $ANTLR start "CBrace"
    public final void mCBrace() throws RecognitionException {
        try {
            int _type = CBrace;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:416:10: ( '}' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:416:12: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CBrace"

    // $ANTLR start "Close"
    public final void mClose() throws RecognitionException {
        try {
            int _type = Close;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:417:7: ( '_' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:417:9: '_'
            {
            match('_'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Close"

    // $ANTLR start "Bool"
    public final void mBool() throws RecognitionException {
        try {
            int _type = Bool;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:419:7: ( 'true' | 'false' )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='t') ) {
                alt2=1;
            }
            else if ( (LA2_0=='f') ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }
            switch (alt2) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:419:10: 'true'
                    {
                    match("true"); 



                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:419:19: 'false'
                    {
                    match("false"); 



                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Bool"

    // $ANTLR start "And"
    public final void mAnd() throws RecognitionException {
        try {
            int _type = And;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:421:6: ( 'and' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:421:8: 'and'
            {
            match("and"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "And"

    // $ANTLR start "Or"
    public final void mOr() throws RecognitionException {
        try {
            int _type = Or;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:422:5: ( 'or' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:422:7: 'or'
            {
            match("or"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Or"

    // $ANTLR start "Number"
    public final void mNumber() throws RecognitionException {
        try {
            int _type = Number;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:425:3: ( Int ( '.' ( Digit )* )? )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:425:6: Int ( '.' ( Digit )* )?
            {
            mInt(); 


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:425:10: ( '.' ( Digit )* )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='.') ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:425:11: '.' ( Digit )*
                    {
                    match('.'); 

                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:425:15: ( Digit )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Number"

    // $ANTLR start "Identifier"
    public final void mIdentifier() throws RecognitionException {
        try {
            int _type = Identifier;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:429:3: ( ( 'a' .. 'z' | 'A' .. 'Z' | '#' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | Digit )* )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:429:5: ( 'a' .. 'z' | 'A' .. 'Z' | '#' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | Digit )*
            {
            if ( input.LA(1)=='#'||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:429:35: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | Digit )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0 >= '0' && LA5_0 <= '9')||(LA5_0 >= 'A' && LA5_0 <= 'Z')||LA5_0=='_'||(LA5_0 >= 'a' && LA5_0 <= 'z')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Identifier"

    // $ANTLR start "CloseIdentifier"
    public final void mCloseIdentifier() throws RecognitionException {
        try {
            int _type = CloseIdentifier;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:433:3: ( ( Close ) Identifier )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:433:5: ( Close ) Identifier
            {
            if ( input.LA(1)=='_' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            mIdentifier(); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CloseIdentifier"

    // $ANTLR start "String"
    public final void mString() throws RecognitionException {
        try {
            int _type = String;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:440:3: ( '\"' (~ ( '\"' | '\\\\' ) | '\\\\' ( '\\\\' | '\"' ) )* '\"' | '\\'' (~ ( '\\'' | '\\\\' ) | '\\\\' ( '\\\\' | '\\'' ) )* '\\'' )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='\"') ) {
                alt8=1;
            }
            else if ( (LA8_0=='\'') ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;

            }
            switch (alt8) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:440:6: '\"' (~ ( '\"' | '\\\\' ) | '\\\\' ( '\\\\' | '\"' ) )* '\"'
                    {
                    match('\"'); 

                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:440:11: (~ ( '\"' | '\\\\' ) | '\\\\' ( '\\\\' | '\"' ) )*
                    loop6:
                    do {
                        int alt6=3;
                        int LA6_0 = input.LA(1);

                        if ( ((LA6_0 >= '\u0000' && LA6_0 <= '!')||(LA6_0 >= '#' && LA6_0 <= '[')||(LA6_0 >= ']' && LA6_0 <= '\uFFFF')) ) {
                            alt6=1;
                        }
                        else if ( (LA6_0=='\\') ) {
                            alt6=2;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:440:12: ~ ( '\"' | '\\\\' )
                    	    {
                    	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:440:29: '\\\\' ( '\\\\' | '\"' )
                    	    {
                    	    match('\\'); 

                    	    if ( input.LA(1)=='\"'||input.LA(1)=='\\' ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    match('\"'); 

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:441:6: '\\'' (~ ( '\\'' | '\\\\' ) | '\\\\' ( '\\\\' | '\\'' ) )* '\\''
                    {
                    match('\''); 

                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:441:11: (~ ( '\\'' | '\\\\' ) | '\\\\' ( '\\\\' | '\\'' ) )*
                    loop7:
                    do {
                        int alt7=3;
                        int LA7_0 = input.LA(1);

                        if ( ((LA7_0 >= '\u0000' && LA7_0 <= '&')||(LA7_0 >= '(' && LA7_0 <= '[')||(LA7_0 >= ']' && LA7_0 <= '\uFFFF')) ) {
                            alt7=1;
                        }
                        else if ( (LA7_0=='\\') ) {
                            alt7=2;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:441:12: ~ ( '\\'' | '\\\\' )
                    	    {
                    	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '&')||(input.LA(1) >= '(' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;
                    	case 2 :
                    	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:441:29: '\\\\' ( '\\\\' | '\\'' )
                    	    {
                    	    match('\\'); 

                    	    if ( input.LA(1)=='\''||input.LA(1)=='\\' ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);


                    match('\''); 

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;

              setText(getText().substring(1, getText().length()-1).replaceAll("\\\\(.)", "$1"));

        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "String"

    // $ANTLR start "Comment"
    public final void mComment() throws RecognitionException {
        try {
            int _type = Comment;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:445:3: ( '//' (~ ( '\\r' | '\\n' ) )* | '/*' ( . )* '*/' )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0=='/') ) {
                int LA11_1 = input.LA(2);

                if ( (LA11_1=='/') ) {
                    alt11=1;
                }
                else if ( (LA11_1=='*') ) {
                    alt11=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 11, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;

            }
            switch (alt11) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:445:6: '//' (~ ( '\\r' | '\\n' ) )*
                    {
                    match("//"); 



                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:445:11: (~ ( '\\r' | '\\n' ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( ((LA9_0 >= '\u0000' && LA9_0 <= '\t')||(LA9_0 >= '\u000B' && LA9_0 <= '\f')||(LA9_0 >= '\u000E' && LA9_0 <= '\uFFFF')) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:
                    	    {
                    	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);


                    skip();

                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:446:6: '/*' ( . )* '*/'
                    {
                    match("/*"); 



                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:446:11: ( . )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0=='*') ) {
                            int LA10_1 = input.LA(2);

                            if ( (LA10_1=='/') ) {
                                alt10=2;
                            }
                            else if ( ((LA10_1 >= '\u0000' && LA10_1 <= '.')||(LA10_1 >= '0' && LA10_1 <= '\uFFFF')) ) {
                                alt10=1;
                            }


                        }
                        else if ( ((LA10_0 >= '\u0000' && LA10_0 <= ')')||(LA10_0 >= '+' && LA10_0 <= '\uFFFF')) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:446:11: .
                    	    {
                    	    matchAny(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);


                    match("*/"); 



                    skip();

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Comment"

    // $ANTLR start "Space"
    public final void mSpace() throws RecognitionException {
        try {
            int _type = Space;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:450:3: ( ( ' ' | '\\t' | '\\r' | '\\n' | '\\u000C' ) )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:450:6: ( ' ' | '\\t' | '\\r' | '\\n' | '\\u000C' )
            {
            if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||(input.LA(1) >= '\f' && input.LA(1) <= '\r')||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            skip();

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Space"

    // $ANTLR start "Int"
    public final void mInt() throws RecognitionException {
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:454:3: ( '1' .. '9' ( Digit )* | '0' )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( ((LA13_0 >= '1' && LA13_0 <= '9')) ) {
                alt13=1;
            }
            else if ( (LA13_0=='0') ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;

            }
            switch (alt13) {
                case 1 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:454:6: '1' .. '9' ( Digit )*
                    {
                    matchRange('1','9'); 

                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:454:15: ( Digit )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( ((LA12_0 >= '0' && LA12_0 <= '9')) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:
                    	    {
                    	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:455:6: '0'
                    {
                    match('0'); 

                    }
                    break;

            }

        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Int"

    // $ANTLR start "Digit"
    public final void mDigit() throws RecognitionException {
        try {
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:459:3: ( '0' .. '9' )
            // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:
            {
            if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "Digit"

    public void mTokens() throws RecognitionException {
        // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:8: ( T__101 | T__102 | T__103 | T__104 | T__105 | T__106 | T__107 | T__108 | T__109 | T__110 | T__111 | T__112 | T__113 | T__114 | T__115 | T__116 | T__117 | T__118 | T__119 | T__120 | T__121 | T__122 | T__123 | T__124 | T__125 | T__126 | T__127 | T__128 | Type | Array | Form | Subwindow | Button | File | Panel | TextLabel | TextField | Of | Import | Variables | Procedure | Begin | Infinity | New | FormType | Println | Def | Return | For | While | Step | To | Do | EndDo | End | In | Null | Hence | If | Else | EndIf | Case | EndCase | OBrace | CBrace | Close | Bool | And | Or | Number | Identifier | CloseIdentifier | String | Comment | Space )
        int alt14=75;
        alt14 = dfa14.predict(input);
        switch (alt14) {
            case 1 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:10: T__101
                {
                mT__101(); 


                }
                break;
            case 2 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:17: T__102
                {
                mT__102(); 


                }
                break;
            case 3 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:24: T__103
                {
                mT__103(); 


                }
                break;
            case 4 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:31: T__104
                {
                mT__104(); 


                }
                break;
            case 5 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:38: T__105
                {
                mT__105(); 


                }
                break;
            case 6 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:45: T__106
                {
                mT__106(); 


                }
                break;
            case 7 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:52: T__107
                {
                mT__107(); 


                }
                break;
            case 8 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:59: T__108
                {
                mT__108(); 


                }
                break;
            case 9 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:66: T__109
                {
                mT__109(); 


                }
                break;
            case 10 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:73: T__110
                {
                mT__110(); 


                }
                break;
            case 11 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:80: T__111
                {
                mT__111(); 


                }
                break;
            case 12 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:87: T__112
                {
                mT__112(); 


                }
                break;
            case 13 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:94: T__113
                {
                mT__113(); 


                }
                break;
            case 14 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:101: T__114
                {
                mT__114(); 


                }
                break;
            case 15 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:108: T__115
                {
                mT__115(); 


                }
                break;
            case 16 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:115: T__116
                {
                mT__116(); 


                }
                break;
            case 17 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:122: T__117
                {
                mT__117(); 


                }
                break;
            case 18 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:129: T__118
                {
                mT__118(); 


                }
                break;
            case 19 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:136: T__119
                {
                mT__119(); 


                }
                break;
            case 20 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:143: T__120
                {
                mT__120(); 


                }
                break;
            case 21 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:150: T__121
                {
                mT__121(); 


                }
                break;
            case 22 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:157: T__122
                {
                mT__122(); 


                }
                break;
            case 23 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:164: T__123
                {
                mT__123(); 


                }
                break;
            case 24 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:171: T__124
                {
                mT__124(); 


                }
                break;
            case 25 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:178: T__125
                {
                mT__125(); 


                }
                break;
            case 26 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:185: T__126
                {
                mT__126(); 


                }
                break;
            case 27 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:192: T__127
                {
                mT__127(); 


                }
                break;
            case 28 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:199: T__128
                {
                mT__128(); 


                }
                break;
            case 29 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:206: Type
                {
                mType(); 


                }
                break;
            case 30 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:211: Array
                {
                mArray(); 


                }
                break;
            case 31 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:217: Form
                {
                mForm(); 


                }
                break;
            case 32 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:222: Subwindow
                {
                mSubwindow(); 


                }
                break;
            case 33 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:232: Button
                {
                mButton(); 


                }
                break;
            case 34 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:239: File
                {
                mFile(); 


                }
                break;
            case 35 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:244: Panel
                {
                mPanel(); 


                }
                break;
            case 36 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:250: TextLabel
                {
                mTextLabel(); 


                }
                break;
            case 37 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:260: TextField
                {
                mTextField(); 


                }
                break;
            case 38 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:270: Of
                {
                mOf(); 


                }
                break;
            case 39 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:273: Import
                {
                mImport(); 


                }
                break;
            case 40 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:280: Variables
                {
                mVariables(); 


                }
                break;
            case 41 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:290: Procedure
                {
                mProcedure(); 


                }
                break;
            case 42 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:300: Begin
                {
                mBegin(); 


                }
                break;
            case 43 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:306: Infinity
                {
                mInfinity(); 


                }
                break;
            case 44 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:315: New
                {
                mNew(); 


                }
                break;
            case 45 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:319: FormType
                {
                mFormType(); 


                }
                break;
            case 46 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:328: Println
                {
                mPrintln(); 


                }
                break;
            case 47 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:336: Def
                {
                mDef(); 


                }
                break;
            case 48 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:340: Return
                {
                mReturn(); 


                }
                break;
            case 49 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:347: For
                {
                mFor(); 


                }
                break;
            case 50 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:351: While
                {
                mWhile(); 


                }
                break;
            case 51 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:357: Step
                {
                mStep(); 


                }
                break;
            case 52 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:362: To
                {
                mTo(); 


                }
                break;
            case 53 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:365: Do
                {
                mDo(); 


                }
                break;
            case 54 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:368: EndDo
                {
                mEndDo(); 


                }
                break;
            case 55 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:374: End
                {
                mEnd(); 


                }
                break;
            case 56 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:378: In
                {
                mIn(); 


                }
                break;
            case 57 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:381: Null
                {
                mNull(); 


                }
                break;
            case 58 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:386: Hence
                {
                mHence(); 


                }
                break;
            case 59 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:392: If
                {
                mIf(); 


                }
                break;
            case 60 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:395: Else
                {
                mElse(); 


                }
                break;
            case 61 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:400: EndIf
                {
                mEndIf(); 


                }
                break;
            case 62 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:406: Case
                {
                mCase(); 


                }
                break;
            case 63 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:411: EndCase
                {
                mEndCase(); 


                }
                break;
            case 64 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:419: OBrace
                {
                mOBrace(); 


                }
                break;
            case 65 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:426: CBrace
                {
                mCBrace(); 


                }
                break;
            case 66 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:433: Close
                {
                mClose(); 


                }
                break;
            case 67 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:439: Bool
                {
                mBool(); 


                }
                break;
            case 68 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:444: And
                {
                mAnd(); 


                }
                break;
            case 69 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:448: Or
                {
                mOr(); 


                }
                break;
            case 70 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:451: Number
                {
                mNumber(); 


                }
                break;
            case 71 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:458: Identifier
                {
                mIdentifier(); 


                }
                break;
            case 72 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:469: CloseIdentifier
                {
                mCloseIdentifier(); 


                }
                break;
            case 73 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:485: String
                {
                mString(); 


                }
                break;
            case 74 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:492: Comment
                {
                mComment(); 


                }
                break;
            case 75 :
                // C:\\data\\test\\barsic\\source\\Barsic\\Barsic.Lang\\src\\main\\antlr\\Barsic.g:1:500: Space
                {
                mSpace(); 


                }
                break;

        }

    }


    protected DFA14 dfa14 = new DFA14(this);
    static final String DFA14_eotS =
        "\1\uffff\1\64\10\uffff\1\66\1\70\2\uffff\1\72\1\75\1\77\4\uffff"+
        "\1\104\1\60\1\112\23\60\2\uffff\2\60\21\uffff\4\105\2\uffff\1\60"+
        "\1\156\1\157\2\uffff\13\60\1\174\1\175\6\60\1\u0085\12\60\1\105"+
        "\1\u0091\1\u0092\1\105\1\60\2\uffff\14\60\2\uffff\4\60\1\u00a5\1"+
        "\60\1\u00a7\1\uffff\1\60\1\u00a9\3\60\1\u00ad\3\60\1\u00b1\1\105"+
        "\2\uffff\1\105\2\60\1\u00b6\4\60\1\u00bc\1\u00bd\10\60\1\uffff\1"+
        "\u00c7\1\uffff\1\60\1\uffff\2\60\1\u00cb\1\uffff\1\u00cc\1\u00cd"+
        "\1\u00ce\1\uffff\1\105\1\u00d0\2\60\1\uffff\3\60\1\u00d6\1\60\2"+
        "\uffff\1\u00d8\7\60\1\u00e0\1\uffff\1\60\1\u00ce\1\u00e2\4\uffff"+
        "\1\105\1\uffff\1\u00e4\1\u00b6\1\u00e5\1\u00b6\1\60\1\uffff\1\60"+
        "\1\uffff\2\60\1\u00ea\4\60\1\uffff\1\u00ef\1\uffff\1\u00f0\2\uffff"+
        "\4\60\1\uffff\3\60\1\u00f8\2\uffff\1\60\1\u00fa\2\60\1\u00fd\2\60"+
        "\1\uffff\1\u0100\1\uffff\1\u0101\1\u0102\1\uffff\1\u0103\1\u0104"+
        "\5\uffff";
    static final String DFA14_eofS =
        "\u0105\uffff";
    static final String DFA14_minS =
        "\1\11\1\75\10\uffff\1\56\1\52\2\uffff\3\75\4\uffff\1\43\1\146\1"+
        "\174\1\165\1\157\1\164\1\162\1\151\1\141\1\145\1\146\1\141\1\162"+
        "\4\145\1\141\1\150\1\164\1\154\1\141\2\uffff\1\162\1\156\21\uffff"+
        "\1\165\1\157\1\146\1\141\2\uffff\1\164\2\60\2\uffff\1\155\1\157"+
        "\1\164\1\162\1\142\2\162\1\154\1\156\1\170\1\160\2\60\1\162\1\151"+
        "\1\147\1\167\1\154\1\146\1\60\1\164\1\162\1\154\1\151\1\145\1\144"+
        "\2\163\1\165\1\144\1\164\2\60\1\163\1\160\2\uffff\1\142\1\154\1"+
        "\164\1\151\1\167\1\141\1\155\2\145\1\164\1\157\1\151\2\uffff\1\151"+
        "\1\143\1\156\1\151\1\60\1\154\1\60\1\uffff\1\165\1\60\1\163\1\154"+
        "\1\160\1\60\3\145\1\60\1\160\2\uffff\1\145\1\165\1\145\1\60\1\157"+
        "\1\156\1\151\1\171\2\60\1\154\1\106\1\162\1\156\1\141\1\145\1\164"+
        "\1\156\1\uffff\1\60\1\uffff\1\162\1\uffff\2\145\1\60\1\uffff\3\60"+
        "\1\uffff\1\165\1\60\1\164\1\162\1\uffff\1\156\1\147\1\156\1\60\1"+
        "\171\2\uffff\1\60\1\141\1\151\1\164\1\151\1\142\1\144\1\154\1\60"+
        "\1\uffff\1\156\2\60\4\uffff\1\164\1\uffff\4\60\1\144\1\uffff\1\160"+
        "\1\uffff\1\142\1\145\1\60\1\164\1\154\1\165\1\156\1\uffff\1\60\1"+
        "\uffff\1\60\2\uffff\1\157\2\145\1\154\1\uffff\1\171\1\145\1\162"+
        "\1\60\2\uffff\1\167\1\60\1\154\1\144\1\60\1\163\1\145\1\uffff\1"+
        "\60\1\uffff\2\60\1\uffff\2\60\5\uffff";
    static final String DFA14_maxS =
        "\1\175\1\75\10\uffff\1\56\1\57\2\uffff\1\75\1\76\1\75\4\uffff\1"+
        "\172\1\165\1\174\3\165\1\162\1\157\1\141\1\145\1\156\1\141\1\162"+
        "\1\145\1\165\1\157\1\145\1\157\1\150\1\164\1\156\1\141\2\uffff\1"+
        "\162\1\156\21\uffff\1\165\1\157\1\146\1\141\2\uffff\1\164\2\172"+
        "\2\uffff\1\155\1\157\1\164\1\162\1\142\2\162\1\154\1\156\1\170\1"+
        "\160\2\172\1\162\1\157\1\147\1\167\1\154\1\146\1\172\1\164\1\162"+
        "\1\154\1\151\1\145\1\144\2\163\1\165\1\144\1\164\2\172\1\163\1\160"+
        "\2\uffff\1\142\1\154\1\164\1\151\1\167\1\141\1\155\2\145\1\164\1"+
        "\157\1\151\2\uffff\1\151\1\143\1\156\1\151\1\172\1\154\1\172\1\uffff"+
        "\1\165\1\172\1\163\1\154\1\160\1\172\3\145\1\172\1\160\2\uffff\1"+
        "\145\1\165\1\145\1\172\1\157\1\156\1\151\1\171\2\172\1\154\1\114"+
        "\1\162\1\156\1\141\1\145\1\164\1\156\1\uffff\1\172\1\uffff\1\162"+
        "\1\uffff\2\145\1\172\1\uffff\3\172\1\uffff\1\165\1\172\1\164\1\162"+
        "\1\uffff\1\156\1\147\1\156\1\172\1\171\2\uffff\1\172\1\141\1\151"+
        "\1\164\1\151\1\142\1\144\1\154\1\172\1\uffff\1\156\2\172\4\uffff"+
        "\1\164\1\uffff\4\172\1\144\1\uffff\1\160\1\uffff\1\142\1\145\1\172"+
        "\1\164\1\154\1\165\1\156\1\uffff\1\172\1\uffff\1\172\2\uffff\1\157"+
        "\2\145\1\154\1\uffff\1\171\1\145\1\162\1\172\2\uffff\1\167\1\172"+
        "\1\154\1\144\1\172\1\163\1\145\1\uffff\1\172\1\uffff\2\172\1\uffff"+
        "\2\172\5\uffff";
    static final String DFA14_acceptS =
        "\2\uffff\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\2\uffff\1\15\1\16\3"+
        "\uffff\1\25\1\26\1\27\1\30\26\uffff\1\100\1\101\2\uffff\1\106\1"+
        "\107\1\111\1\113\1\2\1\1\1\64\1\13\1\112\1\14\1\20\1\17\1\22\1\72"+
        "\1\21\1\24\1\23\4\uffff\1\102\1\110\3\uffff\1\34\1\33\43\uffff\1"+
        "\46\1\105\14\uffff\1\70\1\73\7\uffff\1\65\13\uffff\1\66\1\75\22"+
        "\uffff\1\54\1\uffff\1\57\1\uffff\1\61\3\uffff\1\67\3\uffff\1\104"+
        "\4\uffff\1\35\5\uffff\1\37\1\42\11\uffff\1\71\3\uffff\1\63\1\74"+
        "\1\76\1\103\1\uffff\1\77\5\uffff\1\36\1\uffff\1\43\7\uffff\1\52"+
        "\1\uffff\1\62\1\uffff\1\32\1\41\4\uffff\1\47\4\uffff\1\60\1\31\7"+
        "\uffff\1\56\1\uffff\1\55\2\uffff\1\53\2\uffff\1\40\1\44\1\45\1\50"+
        "\1\51";
    static final String DFA14_specialS =
        "\u0105\uffff}>";
    static final String[] DFA14_transitionS = {
            "\2\62\1\uffff\2\62\22\uffff\1\62\1\1\1\61\1\60\1\uffff\1\2\1"+
            "\3\1\61\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13\12\57\1\14\1\15"+
            "\1\16\1\17\1\20\1\21\1\uffff\1\33\1\31\3\60\1\34\7\60\1\30\1"+
            "\60\1\35\2\60\1\32\1\36\6\60\1\22\1\uffff\1\23\1\24\1\25\1\uffff"+
            "\1\56\1\42\1\52\1\44\1\51\1\46\2\60\1\37\4\60\1\43\1\26\1\41"+
            "\1\60\1\45\1\50\1\55\1\60\1\40\1\47\3\60\1\53\1\27\1\54",
            "\1\63",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\65",
            "\1\67\4\uffff\1\67",
            "",
            "",
            "\1\71",
            "\1\73\1\74",
            "\1\76",
            "",
            "",
            "",
            "",
            "\1\105\35\uffff\32\105\6\uffff\2\105\1\103\1\101\4\105\1\102"+
            "\5\105\1\100\13\105",
            "\1\107\13\uffff\1\110\2\uffff\1\106",
            "\1\111",
            "\1\113",
            "\1\114\5\uffff\1\115",
            "\1\116\1\117",
            "\1\120",
            "\1\122\5\uffff\1\121",
            "\1\123",
            "\1\124",
            "\1\127\6\uffff\1\125\1\126",
            "\1\130",
            "\1\131",
            "\1\132",
            "\1\133\17\uffff\1\134",
            "\1\135\11\uffff\1\136",
            "\1\137",
            "\1\141\15\uffff\1\140",
            "\1\142",
            "\1\143",
            "\1\145\1\uffff\1\144",
            "\1\146",
            "",
            "",
            "\1\147",
            "\1\150",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\151",
            "\1\152",
            "\1\153",
            "\1\154",
            "",
            "",
            "\1\155",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "",
            "\1\160",
            "\1\161",
            "\1\162",
            "\1\163",
            "\1\164",
            "\1\165",
            "\1\166",
            "\1\167",
            "\1\170",
            "\1\171",
            "\1\172",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\5\60\1\173\24\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\176",
            "\1\u0080\5\uffff\1\177",
            "\1\u0081",
            "\1\u0082",
            "\1\u0083",
            "\1\u0084",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u0086",
            "\1\u0087",
            "\1\u0088",
            "\1\u0089",
            "\1\u008a",
            "\1\u008b",
            "\1\u008c",
            "\1\u008d",
            "\1\u008e",
            "\1\u008f",
            "\1\u0090",
            "\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff\32\105",
            "\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff\32\105",
            "\1\u0093",
            "\1\u0094",
            "",
            "",
            "\1\u0095",
            "\1\u0096",
            "\1\u0097",
            "\1\u0098",
            "\1\u0099",
            "\1\u009a",
            "\1\u009b",
            "\1\u009c",
            "\1\u009d",
            "\1\u009e",
            "\1\u009f",
            "\1\u00a0",
            "",
            "",
            "\1\u00a1",
            "\1\u00a2",
            "\1\u00a3",
            "\1\u00a4",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00a6",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\1\u00a8",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00aa",
            "\1\u00ab",
            "\1\u00ac",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00ae",
            "\1\u00af",
            "\1\u00b0",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00b2",
            "",
            "",
            "\1\u00b3",
            "\1\u00b4",
            "\1\u00b5",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00b7",
            "\1\u00b8",
            "\1\u00b9",
            "\1\u00ba",
            "\12\60\7\uffff\23\60\1\u00bb\6\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00be",
            "\1\u00c0\5\uffff\1\u00bf",
            "\1\u00c1",
            "\1\u00c2",
            "\1\u00c3",
            "\1\u00c4",
            "\1\u00c5",
            "\1\u00c6",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\1\u00c8",
            "",
            "\1\u00c9",
            "\1\u00ca",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\1\u00cf",
            "\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff\32\105",
            "\1\u00d1",
            "\1\u00d2",
            "",
            "\1\u00d3",
            "\1\u00d4",
            "\1\u00d5",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00d7",
            "",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00d9",
            "\1\u00da",
            "\1\u00db",
            "\1\u00dc",
            "\1\u00dd",
            "\1\u00de",
            "\1\u00df",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\1\u00e1",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "",
            "",
            "",
            "\1\u00e3",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00e6",
            "",
            "\1\u00e7",
            "",
            "\1\u00e8",
            "\1\u00e9",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00eb",
            "\1\u00ec",
            "\1\u00ed",
            "\1\u00ee",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\12\105\7\uffff\32\105\4\uffff\1\105\1\uffff\32\105",
            "",
            "",
            "\1\u00f1",
            "\1\u00f2",
            "\1\u00f3",
            "\1\u00f4",
            "",
            "\1\u00f5",
            "\1\u00f6",
            "\1\u00f7",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "",
            "\1\u00f9",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00fb",
            "\1\u00fc",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\1\u00fe",
            "\1\u00ff",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "\12\60\7\uffff\32\60\4\uffff\1\60\1\uffff\32\60",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
    static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
    static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
    static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
    static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
    static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
    static final short[][] DFA14_transition;

    static {
        int numStates = DFA14_transitionS.length;
        DFA14_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
        }
    }

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = DFA14_eot;
            this.eof = DFA14_eof;
            this.min = DFA14_min;
            this.max = DFA14_max;
            this.accept = DFA14_accept;
            this.special = DFA14_special;
            this.transition = DFA14_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__101 | T__102 | T__103 | T__104 | T__105 | T__106 | T__107 | T__108 | T__109 | T__110 | T__111 | T__112 | T__113 | T__114 | T__115 | T__116 | T__117 | T__118 | T__119 | T__120 | T__121 | T__122 | T__123 | T__124 | T__125 | T__126 | T__127 | T__128 | Type | Array | Form | Subwindow | Button | File | Panel | TextLabel | TextField | Of | Import | Variables | Procedure | Begin | Infinity | New | FormType | Println | Def | Return | For | While | Step | To | Do | EndDo | End | In | Null | Hence | If | Else | EndIf | Case | EndCase | OBrace | CBrace | Close | Bool | And | Or | Number | Identifier | CloseIdentifier | String | Comment | Space );";
        }
    }
 

}