package barsic.classloader;

import barsic.exceptions.BarsicException;

import java.io.File;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ru
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public interface IBarsicClassLoader {
    void load(File classFile) throws BarsicException;
}
