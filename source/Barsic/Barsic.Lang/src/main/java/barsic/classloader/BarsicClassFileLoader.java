package barsic.classloader;

import barsic.Barsic;
import barsic.exceptions.BarsicException;
import barsic.util.FileUtils;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ru
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public class BarsicClassFileLoader implements IBarsicClassLoader {

    @Override
    public void load(File classFile) throws BarsicException {
        if (!classFile.getName().toLowerCase().endsWith(".class")) {
            throw new BarsicException("Wrong file provided. Only class files are allowed.");
        }
        File classDir = classFile.getParentFile();
        if (classDir == null) {
            classDir = classFile.getAbsoluteFile().getParentFile();
            if (classDir == null) {
                throw new BarsicException("class file directory is not found. Try to provide absolute path to the class file");
            }
        }
        String className = FileUtils.getFileNameWithoutExt(classFile);
        load(classDir, className);
    }

    private void load(File classesDir, String className) throws BarsicException {
        try {
            URL[] urls = new URL[1];
            urls[0] = classesDir.toURL();
            ClassLoader parentLoader = Barsic.class.getClassLoader();
            URLClassLoader classLoader = new URLClassLoader(urls, parentLoader);
            Class cls1 = classLoader.loadClass(className);
            Method main = cls1.getMethod("main", String[].class);
            String[] args = new String[0];
            main.invoke(null, main.invoke(null, new Object[]{args}));
        } catch (NoSuchMethodException e) {
            throw new BarsicException("No such method: " + e.getMessage() + ".", e);
        } catch (IllegalAccessException e) {
            throw new BarsicException("Illegal access to method: " + e.getMessage() + ". The most common problem is" +
                    "related with you trying to access to private/protected method.", e);
        } catch (InvocationTargetException e) {
            throw new BarsicException("Invocation target error: " + e.getMessage() + ".", e);
        } catch (MalformedURLException e) {
            throw new BarsicException("Wrong path or url to class file provided" + e.getMessage(), e);
        } catch (ClassNotFoundException e) {
            throw new BarsicException("The class file is not found: " + e.getMessage(), e);
        }
    }
}
