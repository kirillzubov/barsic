package barsic;

import barsic.exceptions.BarsicException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProjectLoader {
    private static final String REGEX = "^Module\\s([^\\s]*)\\sin\\s\"([^\"]*)\"$";

    public static List<File> listFiles(File projectPath) throws BarsicException, FileNotFoundException {
        File file = !projectPath.isAbsolute() ? projectPath.getAbsoluteFile() : projectPath;
        Scanner in = new Scanner(new FileInputStream(file));
        List<File> ret = new ArrayList<File>();

        while (in.hasNextLine()) {
            String line = in.nextLine();
            Matcher matcher = Pattern.compile(REGEX).matcher(line);
            if (matcher.find()) {
                String relativePath = matcher.group(2);
                File moduleFile = new File(file.getParent(), relativePath);
                if (!moduleFile.exists()) {
                    throw new BarsicException(String.format("file \"%s\" does not exists", moduleFile.getPath()));
                }
                ret.add(moduleFile);
                final String moduleName = FileUtils.getNameWithoutExtension(moduleFile);
                File formFile = new File(new File(moduleFile.getParent()), moduleName + ".bfm");
                if (formFile.exists()) {
                    ret.add(formFile);
                }
            } else {
                if (!line.isEmpty()) {
                    throw new BarsicException(String.format("%s does not match %s", line, REGEX));
                }
            }
        }

        return ret;
    }
}
