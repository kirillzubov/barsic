package barsic.codegen;

import barsic.exceptions.BarsicException;
import barsic.tree.primitives.AstNode;
import barsic.tree.primitives.BlockNode;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ru
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public class JavaCodegen {
    //todo:dirty hack
    public static String CurrentModule;

    private static void writeAllText(File file, String content) throws IOException {
        File parentFile = file.getParentFile();
        if (parentFile != null) {
            file.getParentFile().mkdirs();
        }
        PrintWriter out = new PrintWriter(file);
        out.println(content);
        out.close();
    }

    public static void generateCode(AstNode node, SourceType sourceType, String className, File outputFile) throws BarsicException, IOException {
        JavaCodegen.CurrentModule = className.replaceAll("Form", "").replaceAll("Module", "");
        RootBlockRenderer app = new RootBlockRenderer(sourceType, className, (BlockNode) node);
        StringBuilder sb = new StringBuilder();
        app.emit(sb);
        writeAllText(outputFile, sb.toString());
    }
}
