package barsic.codegen;

import barsic.codegen.java.FormVisitor;

public class Util {
    public static void emitMethodHeader(StringBuilder sb, String id, String retType) {
        Util.emitTab(sb);
        if (retType != null) {
            boolean isStatic = false;//!FormVisitor.AsClass;
            if(retType.contains("Form")){
                isStatic = FormVisitor.AsClass;
            }
            sb.append("\n    public " + (isStatic ? "static " : "") + retType + " create" + id + "(){\n");
        } else {
            sb.append("\n    public void " + id + "(){\n");
        }
        Util.emitTab(sb, 2);
    }

    public static  void emitTab(StringBuilder sb) {
        sb.append("    ");
    }

    public static void emitTab(StringBuilder sb, int n) {
        for (int i = 0; i < n; i++) {
            emitTab(sb);
        }
    }
}
