package barsic.codegen;

import sun.plugin.javascript.navig.Link;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GlobalVariable {
    private final String name;
    private final String type;
    private final String form;

    public GlobalVariable(String name, String type, String form) {
        this.name = name;
        this.type = type;
        this.form =  form;
    }


    public String getName() {
        return name;
    }
    public String getType() {
        return type;
    }
    public String getForm() {
        return form;
    }


}
