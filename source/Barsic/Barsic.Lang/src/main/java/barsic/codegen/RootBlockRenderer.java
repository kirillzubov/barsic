package barsic.codegen;

import barsic.codegen.java.AssignmentVisitor;
import barsic.codegen.java.FormVisitor;
import barsic.exceptions.BarsicException;
import barsic.tree.form.FormCallNode;
import barsic.tree.primitives.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ru
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public class RootBlockRenderer {
    private final String key;
    private final SourceType srcType;
    private final List<AstNode> childs;
    public static boolean IsAndroid = true;

    public RootBlockRenderer(SourceType srcType, String key, BlockNode root) {
        this.key = key;
        this.srcType = srcType;
        this.childs = root.getStatements();
    }


    public void emit(StringBuilder sb) throws BarsicException {
        emitHeader(sb);

        sb.append("public class " + key);
        if (srcType == SourceType.Module) {
            sb.append(" implements barsic.core.IBarsicModule {\n");
            sb.append("    private static " + key + " instance;\n" +
                    "    public " + key + "()\n" +
                    "    {\n" +
                    "        instance = this;\n" +
                    "    }\n" +
                    "\n" +
                    "    public static " + key + " getInstance(){\n" +
                    "        return instance;\n" +
                    "    }\n");
        } else {
            sb.append("{\n");
            sb.append("\n" +
                    "    public static MainModule getModule()\n" +
                    "    {\n" +
                    "        return MainModule.getInstance();\n" +
                    "    }\n");
        }
        emitBody(sb);

        if (srcType == SourceType.Module) {
            emitGlobalVariables(sb);
            emitInit(sb);
        }
        sb.append("\n}\n\n\n");
    }

    private void emitBody(StringBuilder sb) throws BarsicException {
        for (AstNode child : childs) {
            if (child instanceof AssignmentNode) {
                AssignmentNode assignment = (AssignmentNode) child;
                String type = assignment.getType();
                String id = assignment.getIdentifier();
                if (type == "IForm") {
                    AssignmentVisitor.emit(sb, assignment, this);
                    emitMethodBody(sb, id);
                    if (type != null && !IsAndroid) {
                        emitMethodReturn(sb, id);
                    }
                    FormVisitor.emitFooter(sb);
                } else {
                    if (assignment.getType() == null || assignment.getType().toLowerCase().contains("form")) {
                        Util.emitMethodHeader(sb, id, assignment.getType());
                        AssignmentVisitor.emit(sb, assignment, this);
                        emitMethodBody(sb, id);
                        if (type != null) {
                            emitMethodReturn(sb, id);
                        }
                        sb.append("    }\n");
                    } else {
                        AssignmentVisitor.emit(sb, assignment, this);
                    }
                }
            }
            if (child instanceof ProcedureNode) {
                ProcedureNode procedure = (ProcedureNode) child;
                procedure.printValues(procedure.getValue());
                Util.emitMethodHeader(sb, procedure.getName(), "void");
                sb.append("    }\n");
            }
            if (child instanceof ProcedureFuncNode) {
                ProcedureFuncNode procedure = (ProcedureFuncNode) child;
                sb.append(procedure);
            }

        }
    }

    private void emitMethodBody(StringBuilder sb, String id) throws BarsicException {
        for (AstNode statement : childs) {
            if (statement instanceof FormCallNode) {
                FormCallNode callNode = (FormCallNode) statement;
                if (id.equals(callNode.getIdentifier())) {
                    callNode.emit(sb, this);
                }
            }
        }
        sb.append("\n");
    }

    private List<String[]> init = new ArrayList<String[]>();

    private void emitInit(StringBuilder sb) {
        sb.append("void initForm(){\n");
        for (String[] in : init) {
            sb.append("    " + in[0] + "= (" + in[1] + ")"+in[2]+".getElementById(\"" + in[0] + "\");\n");
        }
        sb.append("}\n ");

    }

    private void emitGlobalVariables(StringBuilder sb) {
        for (GlobalVariable cur : Scope.getVars()) {
            sb.append(cur.getType() + " " + cur.getName() + ";\n");
            if (cur.getType()!="IForm") {
                init.add(new String[]{cur.getName(), cur.getType(), cur.getForm()});
            }
        }
        sb.append("\n");
    }

    private void emitMethodReturn(StringBuilder sb, String id) {
        Util.emitTab(sb, 4);
        sb.append("return " + id + ";\n");
    }

    private void emitHeader(StringBuilder sb) {
        sb.append("package barsic.app;\n\n" +
                "import barsic.*;\n" +
                "import barsic.math.*;\n" +
                "import barsic.standartClass.*;\n"+
                "import barsic.ui.*;\n" +
                "import barsic.ui.swing.form.Form;\n\n" +
                "import java.util.Arrays;\n");
    }
}