package barsic.codegen.java;

import barsic.codegen.RootBlockRenderer;
import barsic.codegen.Util;
import barsic.tree.BarsicValue;
import barsic.tree.form.FormNode;
import barsic.tree.primitives.AssignmentNode;
import barsic.tree.primitives.AstNode;

import java.util.List;

public class FormVisitor {

    public static final boolean AsClass = RootBlockRenderer.IsAndroid;

    public static void emitInnerClassHeader(StringBuilder sb, String id, String retType) {
        Util.emitTab(sb);
        if (retType != null) {
            sb.append("\npublic static class " + id + " extends Form {");
        }
        Util.emitTab(sb, 2);
        emitCtor(sb);
    }


    public static void emit(StringBuilder sb, AssignmentNode assignment, FormNode formNode) {
        if (!AsClass) {
            Util.emitMethodHeader(sb, assignment.getIdentifier(), assignment.getType());
            sb.append("IForm " + assignment.getIdentifier() + " = Bootstrapper.resolve(IForm.class);\n");
        } else {
            Util.emitMethodHeader(sb, assignment.getIdentifier(), assignment.getType());
            sb.append("return new MainForm." + assignment.getIdentifier() + "();\n");

            sb.append("}\n\n");

            FormVisitor.emitInnerClassHeader(sb, assignment.getIdentifier(), assignment.getType());
            sb.append("IForm " + assignment.getIdentifier() + " = this;\n");
        }
        emitMembers(sb, formNode.assignments, assignment.getIdentifier());

    }

    public static void emitFooter(StringBuilder sb) {
        if (AsClass) {
            sb.append("}\n");
            sb.append("}\n");
        } else {
            sb.append("}\n\n");
        }
    }

    public static void emitCtor(StringBuilder sb) {
        sb.append("\n" +
                "       form1() {\n" +
                "            createComponents();\n" +
                "        }\n" +

                "        protected void createComponents() {\n"
        );
    }

    public static void emitMembers(StringBuilder sb, List<AssignmentNode> assignments, String id) {
        for (AstNode node : assignments) {
            AssignmentNode asNode = (AssignmentNode) node;

            BarsicValue eval = asNode.evaluate();
            if (eval == BarsicValue.VOID) {
                eval = asNode.scope.resolve(asNode.getIdentifier());
            }

            if (!asNode.getIdentifier().toLowerCase().startsWith("on")) {
                sb.append(id + "." + formatSetterName(asNode.getIdentifier()) + "(" + eval.emitValue() + ");\n");
            } else {
                String val = eval.emitValue();
                //if(!"".equals(val) && !"\"\"".equals(val) ){
                sb.append(id + "." + formatSetterName(asNode.getIdentifier()) + "(" + val + ");\n");
                //}
            }
        }
        sb.append("\n");
    }

    private static String formatSetterName(String propertyName) {
        String trimmed = propertyName.trim();
        String capitol = Character.toString(trimmed.charAt(0)).toUpperCase();
        String firstLetterCapitalized = capitol + trimmed.substring(1, trimmed.length());
        return "set" + firstLetterCapitalized;
    }

}
