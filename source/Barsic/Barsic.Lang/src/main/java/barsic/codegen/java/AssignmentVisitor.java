package barsic.codegen.java;

import barsic.codegen.RootBlockRenderer;
import barsic.exceptions.BarsicException;
import barsic.tree.form.FormCallNode;
import barsic.tree.form.FormNode;
import barsic.tree.primitives.*;

import java.util.List;


public class AssignmentVisitor {

    public static void emit(StringBuilder sb, AssignmentNode assignment, RootBlockRenderer ctx) throws BarsicException {
//        sb.append("    ");
        AstNode value = assignment.value;
        if (value instanceof FormNode) {
            FormVisitor.emit(sb, assignment, (FormNode) value);
        } else if (value instanceof BlockNode) {
            List<AstNode> statements = ((BlockNode) value).getStatements();
            for (AstNode statement : statements) {
                if (statement instanceof FormCallNode) {
                    FormCallNode callNode = (FormCallNode) statement;
                    callNode.emit(sb, ctx);
                } else if (statement instanceof AssignmentNode) {
                    emit(sb, (AssignmentNode) statement, ctx);
                } else if (statement instanceof FunctionCallNode) {
                    FunctionCallNode func = (FunctionCallNode) statement;
                    sb.append(func+ ";\n");
                } else if (statement instanceof IfNode) {
                    IfNode ifn = (IfNode) statement;
                    sb.append(ifn + "\n");
                } else if (statement instanceof WhileStatementNode) {
                    WhileStatementNode whilestat = (WhileStatementNode) statement;
                    sb.append(whilestat.toString());
                } else if (statement instanceof ForStatementNode) {
                    ForStatementNode forstat = (ForStatementNode) statement;
                    sb.append(forstat.toString());
                }else if (statement instanceof CaseAssigmentNode) {
                    CaseAssigmentNode casestat = (CaseAssigmentNode) statement;
                    sb.append(casestat);
                }
            }
        }
        else if (value instanceof FunctionCallNode){
            sb.append(assignment.getType() + " " + assignment.getIdentifier() + " = " + value + ";\n");
        }
        else {
            //sb.append(assignment.toString()+";\n");
            sb.append(assignment.getType() + " " + assignment.getIdentifier() + " = " + value.evaluate().emitValue() + ";\n");
        }
    }
}
