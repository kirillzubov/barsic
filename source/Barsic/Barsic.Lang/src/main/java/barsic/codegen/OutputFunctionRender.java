package barsic.codegen;

import barsic.tree.form.AssignmentsWalker;
import barsic.tree.form.BlockFuncCallArgument;
import barsic.tree.primitives.AssignmentNode;
import barsic.tree.primitives.AstNode;

public class OutputFunctionRender {


    public static String emit(String id, BlockFuncCallArgument argument) {
        StringBuilder sb = new StringBuilder();
        for (AstNode statement : argument.getBlock().getStatements()) {
            if (statement instanceof AssignmentNode) {
                AssignmentNode assignment = (AssignmentNode) statement;
                String assignmentId = assignment.getIdentifier();

                String[] parts = assignmentId.split("\\.");
                sb.append("form1.getElementById(\"" + id + "\").");

                //todo:dehardcode
                String val = "\"#AAAAAA\"";

                for (int i = 0; i < parts.length - 1; i++) {
                    sb.append(AssignmentsWalker.formatGetterName(parts[i]) + "().");
                }

                sb.append(AssignmentsWalker.formatSetterName(parts[parts.length - 1]) + "(" + val + ");");
                return sb.toString();
            }
        }

        return "";
    }
}
