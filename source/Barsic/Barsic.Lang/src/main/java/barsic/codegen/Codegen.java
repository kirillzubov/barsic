package barsic.codegen;

import barsic.exceptions.BarsicException;
import barsic.tree.primitives.AstNode;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ru
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public class Codegen {

    private static Codegen instance;
    private final StringBuilder sb = new StringBuilder();
    private String inputSource;
    private String className;

    private Codegen() {
    }

    public static Codegen getInstance() {
        if (instance == null) {
            instance = new Codegen();
        }
        return instance;
    }

    private void renderHeader(String className) {
        emit(".class public " + className + "\n" +
                ".super java/lang/Object\n" +
                "\n");
    }

    private void renderMain(AstNode node) {
        generateMethod(node, true, "public", "main", "[Ljava/lang/String;", "V");
    }

    public void generateStatements(AstNode node) {
        emit(".limit stack 200\n" +
                "  .limit locals 100\n" +
                "  \n");

//        if (node instanceof BlockNode) {
//            BlockNode block = (BlockNode) node;
//
//            for(AstNode n : block.getStatements()){
//                if(n instanceof FormNode){
//                    if(n instanceof FormNode){
//                        ((FormNode) n).emit(sb);
//                    }else{
//                        emitln(String.format("   getstatic %s/interp Lbarsic/Barsic;\n", className));
//                        pushString(inputSource);
//                        emit(" invokevirtual barsic/Barsic/eval(Ljava/lang/String;)V \n");
//                    }
//                }
//            }
//
//        }

        emitln("return\n");
    }

//    private void pushString(String value) {
//        emitln("  ldc  \" " + value.replace("\"", "\\\"") + "\"\n");
//    }

    private void renderField(Boolean isStatic, String modifier, String name, String type) {
        emit(".field " + modifier + " ");
        if (isStatic) {
            emit("static ");
        }
        emitln(name + " " + type + ";\n");
    }

    private void putStaticField(String type, String name) {
        emitln(String.format("   new %s\n" +
                "   dup\n" +
                "   invokespecial %s/<init>()V\n" +
                "   putstatic %s/%s L%s;\n", type, type, className, name, type));
    }

    private void generateStaticContstructor() {
        emitln(".method static public <clinit>()V\n" +
                "   .limit stack 5\n" +
                "   ldc 0\n");

        putStaticField("barsic/math/BarsicMath", "math");
        putStaticField("barsic/Barsic", "interp");
        emitln("   return\n" +
                ".end method");
    }

    private void generateConstructor() {
        emit("\n" +
                ".method public init()V\n" +
                "    .limit stack 5\n" +
                "    return\n" +
                ".end method\n" +
                "\n");
    }

    private void generateMethod(AstNode node, boolean isStatic, String accessModifier, String name, String args, String retType) {
        emit(".method ");
        emit(accessModifier + " ");
        if (isStatic) {
            emit("static ");
        }
        emit(name);

        emit("(");
        emit(args);
        emit(")");
        emit(retType);
        emit("\n");

        generateStatements(node);
        emit("\n.end method\n");
    }

    private void generateClass(AstNode input) {
        renderHeader(className);
        renderField(true, "public", "interp", "Lbarsic/Barsic");
        renderField(true, "public", "math", "Lbarsic/math/BarsicMath");
        generateStaticContstructor();
        generateConstructor();
        renderMain(input);
    }

    public String generateAsm(AstNode input, String inputSource, String className) throws BarsicException {
        this.inputSource = inputSource;
        this.className = className;
        generateClass(input);
        return sb.toString();
    }

    public void emitln(String code) {
        sb.append(code + "\n");
    }

    public void emit(String code) {
        sb.append(code);
    }

    public static String interpret(String code) {
        return " getstatic Calculator/interp Lbarsic/Barsic;\n" +
                " ldc \" " +
                //---start
                code + "\n" +
                //---end
                "\" \n" +
                " invokevirtual barsic/Barsic/eval(Ljava/lang/String;)V \n" +
                "";

    }
}
