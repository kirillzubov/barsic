package barsic.codegen;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 05/31/2016.
 */
public class Scope {
    public static List<GlobalVariable> variables = new ArrayList<GlobalVariable>();

    public static boolean isVarDefined(String name)
    {
        for (GlobalVariable g: variables){
            if (g.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    public static void defineVar(GlobalVariable var) {
        variables.add(var);
    }

    public static List<GlobalVariable> getVars()
    {
        return variables;
    }
}
