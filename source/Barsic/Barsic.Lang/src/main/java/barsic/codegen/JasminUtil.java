package barsic.codegen;

import barsic.exceptions.BarsicException;
import barsic.util.FileUtils;
import jasmin.ClassFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ru
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public class JasminUtil {
    public static void saveToClassFile(StringReader bytecodeAsm, File output) throws BarsicException {
        try {
            String className = FileUtils.getFileNameWithoutExt(output);
            ClassFile classFile = new ClassFile();
            classFile.readJasmin(bytecodeAsm, className, false);

            if (classFile.errorCount() > 0) {
                throw new BarsicException("Jasmin error: unable to compileByteCode the file.");
            }

            FileOutputStream stream = new FileOutputStream(output);
            classFile.write(stream);
            stream.close();
        } catch (IOException ex) {
            throw new BarsicException("An I/O exception");
        } catch (Exception ex) {
            throw new BarsicException("Unknown jasmin bytecode generation error");
        }
    }
}
