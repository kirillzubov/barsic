package barsic;

import barsic.tree.BarsicValue;

import java.util.HashMap;
import java.util.Map;

public class Scope {

    private Scope parent;
    public Map<String, BarsicValue> variables;

    public Scope() {
        // only for the global scope, the parent is null
        this(null);
    }

    public Scope(Scope parent) {
        this.parent = parent;
        this.variables = new HashMap<String, BarsicValue>();
    }

    public void assign(String var, BarsicValue value) {
        if (resolve(var) != null) {
            // There is already such a variable, re-assign it
            this.reAssign(var, value);
        } else {
            // A newly declared variable
            variables.put(var, value);
        }
    }

    public Scope copy() {
        // Create a shallow copy of this scope. Used in case functions are
        // are recursively called. If we wouldn't create a copy in such cases,
        // changing the variables would result in changes ro the Maps from
        // other "recursive scopes".
        Scope s = new Scope();
        s.variables = new HashMap<String, BarsicValue>(this.variables);
        return s;
    }

    public boolean isGlobalScope() {
        return parent == null;
    }

    public Scope parent() {
        return parent;
    }

    private void reAssign(String identifier, BarsicValue value) {
        if (variables.containsKey(identifier)) {
            // The variable is declared in this scope
            variables.put(identifier, value);
        } else if (parent != null) {
            // The variable was not declared in this scope, so let
            // the parent scope re-assign it
            parent.reAssign(identifier, value);
        }
    }

    public BarsicValue resolve(String var) {
        BarsicValue value = variables.get(var);
        if (value != null) {
            // The variable resides in this scope
            return value;
        } else if (!isGlobalScope()) {
            // Let the parent scope look for the variable
            return parent.resolve(var);
        } else {
            // Unknown variable
            return null;
        }
    }
}
