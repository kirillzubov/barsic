package barsic;

import barsic.classloader.BarsicClassFileLoader;
import barsic.classloader.IBarsicClassLoader;
import barsic.codegen.Codegen;
import barsic.codegen.JasminUtil;
import barsic.codegen.JavaCodegen;
import barsic.codegen.SourceType;
import barsic.exceptions.BarsicException;
import barsic.parser.BarsicLexer;
import barsic.parser.BarsicParser;
import barsic.parser.BarsicTreeWalker;
import barsic.tree.BarsicValue;
import barsic.tree.primitives.AstNode;
import barsic.util.FileUtils;
import org.antlr.runtime.*;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.List;

/**
 * Maxim Maximov, 2012
 * 2xmax@mail.ru
 * cph.maxim.maximov
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 * <p/>
 * Provides access to Barsic class compilation/loading
 */
public class Barsic {

    private IBarsicClassLoader classLoader;

    public Barsic() {
        classLoader = new BarsicClassFileLoader();
    }

    public void run(final File classFile) throws BarsicException {
        classLoader.load(classFile);
    }

    public void compileByteCode(final File inputFile) throws BarsicException {
        final File outputFile = new File(inputFile.getAbsoluteFile().getParent(),
                FileUtils.getFileNameWithoutExt(inputFile) + ".class");
        compileByteCode(inputFile, outputFile);
    }

    public void compileByteCode(File file, final File classFile) throws BarsicException {
        if (!file.getName().endsWith(".brm")) {
            throw new BarsicException("Only brm files are allowed for input program");
        }

        String inputName = FileUtils.getFileNameWithoutExt(file);
        String outputName = FileUtils.getFileNameWithoutExt(file);
        if (!inputName.equals(outputName)) {
            throw new BarsicException(String.format("Names of the files should be the same (excluding extensions)," +
                    "i.e.: %s.brm and %s.class or %s.brm and %s.class", inputName, inputName, outputName, outputName));
        }
        CharStream fileStream;
        String inputSource;
        try {
            inputSource = FileUtils.readAllText(file);
            fileStream = new ANTLRFileStream(file.getAbsolutePath());
        } catch (IOException e) {
            throw new BarsicException("An I/O error has been occurred");
        }
        compileStream(fileStream, inputSource, classFile);
    }

    public void compileByteCode(String inputProgram, File classFile) throws BarsicException {
        compileStream(new ANTLRStringStream(inputProgram), inputProgram, classFile);
    }

    public void eval(String expression) throws BarsicException {
        AstNode node = parse(new ANTLRStringStream(expression));
        BarsicValue result = node.evaluate();
        if (result != null) {
            System.out.println(result);
        }
    }

    public void translateToJava(final File projectFile, final File outputDir) throws BarsicException, IOException {
        if (!(projectFile.getName().endsWith(".bpr"))) {
            throw new BarsicException("Only brc files are allowed for input program");
        }

        if (!outputDir.isDirectory()) {
            throw new BarsicException("output dir is not a directory");
        }

        List<File> files = ProjectLoader.listFiles(projectFile);
        // for (File file : files) {
        for (int i = files.size()-1; i >=0 ; i--) {
            File file = files.get(i);
            String suffix = barsic.FileUtils.getExtension(file).equals("brm") ? "Module" : "Form";
            translateModuleToJava(file, new File(outputDir, barsic.FileUtils.getNameWithoutExtension(file) + suffix + ".java"));
        }
    }

    public void translateModuleToJava(final File src, final File output) throws BarsicException, IOException {
        if (!(src.getName().endsWith(".brm") || src.getName().endsWith(".bfm"))) {
            throw new BarsicException("Only brm files are allowed for input program");
        }
        CharStream fileStream;
        try {
            fileStream = new ANTLRFileStream(src.getAbsolutePath());
        } catch (IOException e) {
            throw new BarsicException("An I/O error has been occurred. File " + src.getAbsolutePath() + " not found");
        }
        AstNode astRoot = parse(fileStream);
        String outputName = FileUtils.getFileNameWithoutExt(output);
        SourceType srcType = src.getName().endsWith(".brm") ? SourceType.Module : SourceType.Form;
        JavaCodegen.generateCode(astRoot, srcType, outputName, output);
    }

    private void compileStream(CharStream program, String inputSource, File classFile) throws BarsicException {
        AstNode node = parse(program);

        String asm = Codegen.getInstance().generateAsm(node, inputSource, FileUtils.getFileNameWithoutExt(classFile));
        JasminUtil.saveToClassFile(new StringReader(asm), classFile);
    }

    private AstNode parse(CharStream stream) throws BarsicException {
        try {
            BarsicLexer lex = new BarsicLexer(stream);
            CommonTokenStream ts = new CommonTokenStream(lex);
            BarsicParser parser = new BarsicParser(ts);
            // Abstract Syntax Tree
            CommonTree ast = (CommonTree) parser.parse().getTree();

            // node stream from AST
            CommonTreeNodeStream nodeStream = new CommonTreeNodeStream(ast);

            // pass the reference to the Map of functions to the tree walker
            BarsicTreeWalker walker = new BarsicTreeWalker(nodeStream,
                    parser.functions);

            // make transform of AST and search
            return walker.walk();
        } catch (RecognitionException re) {
            throw new BarsicException("Error during parsing file", re);
        }
    }

    public AstNode getNode(String str) throws BarsicException {
        return parse(new ANTLRStringStream(str));

    }
}
