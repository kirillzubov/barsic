package barsic.tree.primitives;

import barsic.Scope;
import barsic.tree.BarsicValue;

public class ForStatementNode implements AstNode {

    protected Scope scope;
    private String identifier;
    private AstNode startExpr;
    private AstNode stopExpr;
    private AstNode block;
    private AstNode ste;

    public ForStatementNode(String id, AstNode start, AstNode stop,AstNode st, AstNode bl, Scope s) {
        identifier = id;
        startExpr = start;
        stopExpr = stop;
        ste = st;
        block = bl;
        scope = s;
    }

    @Override
    public BarsicValue evaluate() {

        int start = startExpr.evaluate().asDouble().intValue();
        int stop = stopExpr.evaluate().asDouble().intValue();

        for (int i = start; i <= stop; i++) {
            scope.assign(identifier, new BarsicValue(i));
            BarsicValue returnValue = block.evaluate();
            if (returnValue != BarsicValue.VOID) {
                return returnValue;
            }
        }

        return BarsicValue.VOID;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("for(int %s=%s;%s<=%s;%s++)", identifier, startExpr, identifier, stopExpr, identifier));
        sb.append("{\n");
        sb.append(block);
        sb.append("}\n");
        return sb.toString();
    }
}
