package barsic.tree.primitives;

import barsic.tree.BarsicValue;

import java.util.List;
/**
 * Created by Kirill Zubov on 2018.
 */
public class ProcedureParameterListNode implements AstNode {
    public ProcedureParameterListNode(List<AstNode> procedureParameter19) {
    }

    public String emitType(String type) {
        if (type.equals("Number")) {
            return "int";
        } else if (type.equals("String")) {
            return "String";
        } else if (type.equals("Bool")) {
            return "boolean";
        } else
            return "Object";
    }

    @Override
    public BarsicValue evaluate() {
        return null;
    }
}
