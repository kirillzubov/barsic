package barsic.tree.functions;

/**
 * Maxim Maximov, 2012
 * 2xmax@mail.ru
 * cph.maxim.maximov
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */
public class MathFunctionType {
    public final static int
            PI = 1,
            INFINITY = 2,
            SIN = 3,
            COS = 4,
            TAN = 5,
            COT = 6,
            LN = 7,
            LOG10 = 8,
            SQUARE = 9,
            SQRT = 10,
            ATAN = 11,
            ASIN = 12,
            ACOS = 13,
            ACOT = 14,
            SH = 15,
            CH = 16,
            ABS = 17,
            ROUND = 18,
            FLOOR = 19,
            CEIL = 20,
            TRUNC = 21,
            FRAC = 22,
            SIGN = 23,
            MIN = 24,
            MAX = 25,
            RANDOM = 26,
            POWER = 27,
            EXP = 28;
}
