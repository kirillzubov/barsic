package barsic.tree.primitives;

import barsic.tree.BarsicValue;

import java.util.List;

public class LTNode implements AstNode {

    private AstNode lhs;
    private AstNode rhs;

    public LTNode(AstNode lhs, AstNode rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public BarsicValue evaluate() {

        BarsicValue a = lhs.evaluate();
        BarsicValue b = rhs.evaluate();

        if (a.isNumber() && b.isNumber()) {
            return new BarsicValue(a.asDouble() < b.asDouble());
        }

        if (a.isString() && b.isString()) {
            return new BarsicValue(a.asString().compareTo(b.asString()) < 0);
        }
        // list + any
        if (a.isList()) {
            List<BarsicValue> list = a.asList();
            list.add(b);
            return new BarsicValue(list);
        }

        // string + any or any + string
        if (a.isString() || b.isString()) {
            return new BarsicValue(a.asString() + "<" + b.toString(), true);
        }

        throw new RuntimeException("illegal expression: " + this);
    }

    @Override
    public String toString() {
        return String.format("(%s < %s)", lhs, rhs);
    }
}
