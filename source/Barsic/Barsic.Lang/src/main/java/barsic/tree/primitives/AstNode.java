package barsic.tree.primitives;

import barsic.tree.BarsicValue;

public interface AstNode {

    BarsicValue evaluate();
}
