package barsic.tree;

import java.lang.reflect.Array;
import java.util.List;

public class BarsicValue implements Comparable<BarsicValue> {

    public static final BarsicValue NULL = new BarsicValue();
    public static final BarsicValue VOID = new BarsicValue();
    private BarsicValue a,b;
    private Object value;
    private boolean isIdentifier;

    private BarsicValue() {
        value = new Object();
    }

    public BarsicValue(Object v) {
        if (v == null) {
            throw new RuntimeException("v == null");
        }
        value = v;
        // only accept boolean, list, number or string types
        if (!(isBoolean() || isList() || isNumber() || isString())) {
            throw new RuntimeException("invalid data type: " + v + " (" + v.getClass() + ")");
        }
    }

    public BarsicValue(Object v, Boolean IsIdentifier) {
        this.isIdentifier = IsIdentifier;
        if (v == null) {
            throw new RuntimeException("v == null");
        }
        value = v;
        // only accept boolean, list, number or string types
        if (!(isBoolean() || isList() || isNumber() || isString())) {
            throw new RuntimeException("invalid data type: " + v + " (" + v.getClass() + ")");
        }
    }

    public BarsicValue(Object v, BarsicValue a, BarsicValue b) {
        this.isIdentifier = true;
        if (v == null) {
            throw new RuntimeException("v == null");
        }
        value = v;
        this.a = a;
        this.b = b;
    }

    public Boolean asBoolean() {
        return (Boolean) value;
    }

    public Double asDouble() {
        return ((Number) value).doubleValue();
    }

    public Long asLong() {
        return ((Number) value).longValue();
    }

    @SuppressWarnings("unchecked")
    public List<BarsicValue> asList() {
        return (List<BarsicValue>) value;
    }

    public String asString() {
        String val = (String)value;
        if(val.startsWith("#")){
            val = "0x" + val.substring(1,val.length()-1);
        }
        return val;
    }

    @Override
    public int compareTo(BarsicValue that) {
        if (this.isNumber() && that.isNumber()) {
            if (this.equals(that)) {
                return 0;
            } else {
                return this.asDouble().compareTo(that.asDouble());
            }
        } else if (this.isString() && that.isString()) {
            return this.asString().compareTo(that.asString());
        } else {
            throw new RuntimeException("illegal expression: can't compare `" + this + "` to `" + that + "`");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == VOID || o == VOID) {
            throw new RuntimeException("can't use VOID: " + this + " ==/!= " + o);
        }
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        BarsicValue that = (BarsicValue) o;
        if (this.isNumber() && that.isNumber()) {
            double diff = Math.abs(this.asDouble() - that.asDouble());
            return diff < 0.00000000001;
        } else {
            return this.value.equals(that.value);
        }
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    public boolean isBoolean() {
        if(a != null)
            return a.isNumber();
        return value instanceof Boolean;
    }

    public boolean isNumber() {
        if(a != null)
            return a.isNumber();
        return value instanceof Number;
    }

    public boolean isList() {
        return value instanceof List<?>;
    }

    public boolean isNull() {
        return this == NULL;
    }

    public boolean isVoid() {
        return this == VOID;
    }

    public boolean isString() {
        if(a != null)
            return a.isString();
        return value instanceof String;
    }

    public boolean isIdentifier() {
        return isIdentifier;
    }

    @Override
    public String toString() {
        return isNull() ? "NULL" : isVoid() ? "VOID" : formatValue();
    }

    private String formatValue()
    {
        String str = String.valueOf(value);
        if(isNumber() && str.endsWith(".0"))
        {
            return str.substring(0, str.length() - 2);
        }
        return str;
    }

    public String emitValue() {
        return trimValue(emittedValue());
    }

    public String emitType() {
        if(a != null)
            return a.emitType();

        if (isNumber())
            return "double";

        if (isBoolean())
            return "boolean";

        if (isString())
            return "String";
        return "Object";
    }
    public Class getClassType(){
        if(a != null)
            return null ;

        if (isNumber())
            return double.class;

        if (isBoolean())
            return boolean.class;

        if (isString())
            return String.class;

        return Object.class;

    }

    public Object getValue() {
        return value;
    }

    private String emittedValue() {
        if(isIdentifier()){
            return asString();
        }
        if (isString()) {
            return "\"" + asString() + "\"";
        }

        if (isNumber()) {
            int i = asDouble().intValue();
            return String.valueOf(i);
        }
        if (isBoolean()) {
            return String.valueOf(value);
        }

        if (isNull()) {
            return "null";
        }

        return "\"" + String.valueOf(value) + "\"";
    }

    private static String trimValue(String value) {
        String trimmed = value.trim();
        if (value.contains("\'")) {
            return "\"" + trimmed.substring(1, trimmed.length() - 1) + "\"";
        }
        return trimmed;
    }


}
