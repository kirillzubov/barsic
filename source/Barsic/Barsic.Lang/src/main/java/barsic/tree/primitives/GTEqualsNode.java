package barsic.tree.primitives;

import barsic.tree.BarsicValue;

public class GTEqualsNode implements AstNode {

    private AstNode lhs;
    private AstNode rhs;

    public GTEqualsNode(AstNode lhs, AstNode rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public BarsicValue evaluate() {

        BarsicValue a = lhs.evaluate();
        BarsicValue b = rhs.evaluate();

        if (a.isNumber() && b.isNumber()) {
            return new BarsicValue(a.asDouble() >= b.asDouble());
        }

        if (a.isString() && b.isString()) {
            return new BarsicValue(a.asString().compareTo(b.asString()) >= 0);
        }

        throw new RuntimeException("illegal expression: " + this);
    }

    @Override
    public String toString() {
        return String.format("(%s >= %s)", lhs, rhs);
    }
}
