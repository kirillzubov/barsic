package barsic.tree.primitives;

import barsic.tree.BarsicValue;

public class ModNode implements AstNode {

    private AstNode lhs;
    private AstNode rhs;

    public ModNode(AstNode lhs, AstNode rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public BarsicValue evaluate() {

        BarsicValue a = lhs.evaluate();
        BarsicValue b = rhs.evaluate();

        // number % number
        if (a.isNumber() && b.isNumber()) {
            return new BarsicValue(a.asDouble() % b.asDouble());
        }

        throw new RuntimeException("illegal expression: " + this);
    }

    @Override
    public String toString() {
        return String.format("(%s mod %s)", lhs, rhs);
    }
}
