package barsic.tree.primitives;

import barsic.Scope;
import barsic.tree.BarsicValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kirill Zubov on 2018.
 */

public class IfNode implements AstNode {
    private Choice IF;
    private Choice ELSE;
    private List<Choice> choices;

    public IfNode() {
        choices = new ArrayList<Choice>();
    }

    public void addChoices(String t, AstNode e, AstNode b, Scope currentScope) {
        choices.add(new Choice(t, e, b));
    }

    public void addIf(String t, AstNode e, AstNode b, Scope currentScope) {
        IF = new Choice(t, e, b);
    }

    public void addElse(String t, AstNode b, Scope currentScope) {
        ELSE = new Choice(t, null, b);
    }


    @Override
    public BarsicValue evaluate() {
        //If
//        for (Choice ch : choices) {
//            BarsicValue value = ch.expression.evaluate();
//
//            if (!value.isBoolean()) {
//                throw new RuntimeException("illegal boolean expression " +
//                        "inside if-statement: " + ch.expression);
//            }
//
//            if (value.asBoolean()) {
//                return ch.block.evaluate();
//            }
//        }
        //ELSE
        return new BarsicValue(this.toString(), true);

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (IF != null) {
            sb.append(IF.token + " (" + IF.expression + ")\n{     " + IF.block + "\n}");
        }
        for (Choice ifs : choices) {
            if (ifs != null) {
                sb.append(ifs.token + " (" + ifs.expression + ")\n{     " + ifs.block + "\n}");
            }
        }
        if (ELSE != null) {
            sb.append(ELSE.token + "\n{     " + ELSE.block + "\n}");
        }
        return sb.toString();
    }

    private class Choice {
        AstNode expression;
        AstNode block;
        String token;

        Choice(String t, AstNode e, AstNode b) {
            token = t;
            expression = e;
            block = b;
        }
    }
}
