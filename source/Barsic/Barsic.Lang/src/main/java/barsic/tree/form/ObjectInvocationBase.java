package barsic.tree.form;

import barsic.tree.BarsicValue;
import barsic.tree.primitives.AstNode;

public abstract class ObjectInvocationBase implements AstNode {
    private final String methodName;

    public ObjectInvocationBase(String methodName) {
        this.methodName = methodName;
    }

    public String getMethodName() {
        if (methodName.equals("show")) {
            return "showForm";
        }
        return methodName;
    }

    @Override
    public BarsicValue evaluate() {
        return null;
    }
}
