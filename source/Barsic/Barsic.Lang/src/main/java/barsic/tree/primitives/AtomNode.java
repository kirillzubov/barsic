package barsic.tree.primitives;
import barsic.tree.BarsicValue;

public class AtomNode implements AstNode {

    private BarsicValue value;

    public AtomNode(Object v) {
        value = v == null ? BarsicValue.NULL : new BarsicValue(v);
    }

    @Override
    public BarsicValue evaluate() {
        return value;
    }

    @Override
    public String toString() {
        return value.emitValue();
    }

}
