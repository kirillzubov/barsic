package barsic.tree.form;

import barsic.codegen.java.FormVisitor;
import barsic.tree.BarsicValue;
import barsic.tree.primitives.AssignmentNode;
import barsic.tree.primitives.AstNode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormNode implements AstNode {

    public final List<AssignmentNode> assignments;

    public FormNode(List<AssignmentNode> assignments) {
        this.assignments = assignments;
    }

    @Override
    public BarsicValue evaluate() {
        Map<String, BarsicValue> map = new HashMap<String, BarsicValue>();
        for (AstNode node : assignments) {
            AssignmentNode asNode = (AssignmentNode) node;

            BarsicValue eval = asNode.evaluate();
            if (eval == BarsicValue.VOID) {
                eval = asNode.scope.resolve(asNode.getIdentifier());
            }
            map.put(asNode.getIdentifier(), eval);
        }
        return BarsicValue.VOID;
    }

    private String formatSetterName(String propertyName) {
        String trimmed = propertyName.trim();
        String capitol = Character.toString(trimmed.charAt(0)).toUpperCase();
        String firstLetterCapitalized = capitol + trimmed.substring(1, trimmed.length());
        return "set" + firstLetterCapitalized;
    }
}
