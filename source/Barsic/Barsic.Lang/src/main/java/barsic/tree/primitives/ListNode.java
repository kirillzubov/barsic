package barsic.tree.primitives;


import barsic.tree.BarsicValue;

import java.util.ArrayList;
import java.util.List;

public class ListNode implements AstNode {

    private List<AstNode> expressionNodes;

    public ListNode(List<AstNode> nodes) {
        expressionNodes = (nodes == null) ? new ArrayList<AstNode>() : nodes;
    }


    @Override
    public BarsicValue evaluate() {
        List<BarsicValue> evaluated = new ArrayList<BarsicValue>();
        for (AstNode node : expressionNodes) {
            evaluated.add(node.evaluate());
        }
        return new BarsicValue(evaluated);
    }

    @Override
    public String toString() {
        return expressionNodes.toString();
    }
}
