package barsic.tree.primitives;

import barsic.tree.BarsicValue;

public class NotEqualsNode implements AstNode {

    private AstNode lhs;
    private AstNode rhs;

    public NotEqualsNode(AstNode lhs, AstNode rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public BarsicValue evaluate() {

        BarsicValue a = lhs.evaluate();
        BarsicValue b = rhs.evaluate();

        return new BarsicValue(!a.equals(b));
    }

    @Override
    public String toString() {
        return String.format("(%s != %s)", lhs, rhs);
    }
}
