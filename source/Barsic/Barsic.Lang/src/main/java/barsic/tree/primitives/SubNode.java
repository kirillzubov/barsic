package barsic.tree.primitives;


import barsic.tree.BarsicValue;

import java.util.List;

public class SubNode implements AstNode {

    private AstNode lhs;
    private AstNode rhs;

    public SubNode(AstNode lhs, AstNode rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public BarsicValue evaluate() {

        BarsicValue a = lhs.evaluate();
        BarsicValue b = rhs.evaluate();

//        // number - number
//        if (a.isNumber() && b.isNumber()) {
//            return new BarsicValue(a.asDouble() - b.asDouble());
//        }
//
//        // list - any
//        if (a.isList()) {
//            List<BarsicValue> list = a.asList();
//            list.remove(b);
//            return new BarsicValue(list);
//        }

        if(a.isIdentifier() || b.isIdentifier()){
            return new BarsicValue(a.emitValue() + "-" + b.emitValue(), a, b);
        }

        // number + number
        if (a.isNumber() && b.isNumber()) {
            return new BarsicValue(a.asDouble() -  b.asDouble());
        }

        // list + any
        if (a.isList()) {
            List<BarsicValue> list = a.asList();
            list.add(b);
            return new BarsicValue(list);
        }

        // string + any
        if (a.isString()) {
            return new BarsicValue(a.asString() + "" + b.toString());
        }

        // any + string
        if (b.isString()) {
            return new BarsicValue(a.toString() + "" + b.asString());
        }


        throw new RuntimeException("illegal expression: " + this);
    }

    @Override
    public String toString() {
        return String.format("(%s - %s)", lhs, rhs);
    }
}
