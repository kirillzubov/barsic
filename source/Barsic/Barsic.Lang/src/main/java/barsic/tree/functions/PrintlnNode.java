package barsic.tree.functions;

import barsic.tree.BarsicValue;
import barsic.tree.primitives.AstNode;

import java.io.PrintStream;


public class PrintlnNode implements AstNode {

    private AstNode expression;
    private PrintStream out;

    public PrintlnNode(AstNode e) {
        this(e, System.out);
    }

    public PrintlnNode(AstNode e, PrintStream o) {
        expression = e;
        out = o;
    }

    @Override
    public BarsicValue evaluate() {
        BarsicValue value = expression.evaluate();
        out.println(value);
        return BarsicValue.VOID;
    }
}
