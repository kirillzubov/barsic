package barsic.tree.form;

import barsic.codegen.*;
import barsic.exceptions.BarsicException;
import barsic.tree.BarsicValue;
import barsic.tree.primitives.AssignmentNode;
import barsic.tree.primitives.AstNode;
import barsic.tree.primitives.IdentifierNode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormCallNode implements AstNode {

    private final ObjectInvocationBase argument;
    private final AstNode expression;
    private Map<String, String> typeMapping = new HashMap<String, String>();
    private String typeGl = null;
    {
        typeMapping.put("button", "IButton");
        typeMapping.put("textfield", "ITextField");
        typeMapping.put("textlabel", "ITextLabel");
        typeMapping.put("textarea", "ITextArea");
        typeMapping.put("browser", "IBrowser");
        typeMapping.put("form", "IForm");
        typeMapping.put("subwindow", "ISubwindow");
        typeMapping.put("panel", "IPanel");
    }

    public FormCallNode(ObjectInvocationBase paramsNode, AstNode expression) {
        this.argument = paramsNode;
        this.expression = expression;
    }


    public String getIdentifier() {
        if (expression instanceof IdentifierNode) {
            return expression.toString();
        }
        return null;
    }

    @Override
    public BarsicValue evaluate() {
        Map<String, BarsicValue> map = new HashMap<String, BarsicValue>();

        if (argument instanceof ParameterListFuncCallArgument) {
            for (AstNode node : ((ParameterListFuncCallArgument) argument).getAssignments()) {
                AssignmentNode asNode = (AssignmentNode) node;

                BarsicValue eval = asNode.evaluate();
                if (eval == BarsicValue.VOID) {
                    eval = asNode.scope.resolve(asNode.getIdentifier());
                }
                map.put(asNode.getIdentifier(), eval);
            }
        }
        return BarsicValue.VOID;
    }

    public void emit(StringBuilder sb, RootBlockRenderer ctx) throws BarsicException {

        if (expression instanceof IdentifierNode) {
            String objectId = expression.toString();
            if (argument instanceof FunctionCallArguments) {
                if (!Scope.isVarDefined(objectId)) {
                    Scope.defineVar(new GlobalVariable(objectId, "IForm", objectId));
                    sb.append(String.format("%s = " + JavaCodegen.CurrentModule + "Form.create%s();\n", objectId, objectId));
                    sb.append("initForm();\n");
                }
                FunctionCallArguments params = (FunctionCallArguments) argument;
                List<AstNode> arguments = params.getArguments();
                String prefix = String.format("%s.%s(", objectId, params.getMethodName());
                sb.append(prefix + emitParamList(arguments) + ");");
                sb.append("\n");
            } else if (argument instanceof ParameterListFuncCallArgument) {
                ParameterListFuncCallArgument params = (ParameterListFuncCallArgument) argument;
                List<AssignmentNode> assignments = params.getAssignments();
                String prefix = String.format("%s.%s(", objectId, params.getMethodName());
                if (assignments.size() == 0) {
                    if (!Scope.isVarDefined(objectId)) {
                        Scope.defineVar(new GlobalVariable(objectId, "IForm", objectId));
                        sb.append(String.format("%s = " + JavaCodegen.CurrentModule + "Form.create%s();\n", objectId, objectId));
                    }

                    sb.append(prefix + ");");
                } else {
                    String elementId = emitNewElement(sb, params.getAssignments());
                    sb.append(prefix + elementId + ");");
                    Scope.defineVar(new GlobalVariable(elementId, typeMapping.get(typeGl), objectId));
                }
                sb.append("\n");
            }
            if (argument instanceof BlockFuncCallArgument) {
                sb.append(OutputFunctionRender.emit(objectId, (BlockFuncCallArgument) argument));
            }
        }
    }

    private String emitNewElement(StringBuilder sb, List<AssignmentNode> assignments) throws BarsicException {
        Map<String, BarsicValue> map = AssignmentsWalker.Find(assignments);
        String id = map.get("name").toString();
        String type = map.get("type").toString().trim().toLowerCase();
        emitCtor(sb, id, type);
        AssignmentsWalker.emit(sb, map, id);
        typeGl = type;
        return id;
    }

    private String emitParamList(List<AstNode> exprList) throws BarsicException {
        if (exprList != null && exprList.size() > 0) {
            StringBuilder sb = new StringBuilder();
            int index = 0;
            for (AstNode expr : exprList) {
                sb.append(expr.evaluate().emitValue());
                boolean isLast = index == exprList.size() - 1;
                if (!isLast) {
                    sb.append(",");
                }
                index++;
            }
            return sb.toString();
        }
        return "";
    }

    private void emitCtor(StringBuilder sb, String id, String type) throws BarsicException {
        if (typeMapping.containsKey(type.trim().toLowerCase())) {
            String inter = typeMapping.get(type);
            sb.append(inter + " " + id + " = Bootstrapper.resolve(" + inter + ".class);\n");
        } else {
            throw new BarsicException("unknown component:" + type);
        }
    }
}

