package barsic.tree.primitives;

import barsic.tree.BarsicValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kirill Zubov on 2018.
 */

public class CaseNode implements AstNode {

    public Choice IF;
    public Choice ELSE;
    public List<Choice> choices;
    public static String lookup;

    public void setLookup(String l) {
        lookup = l;
    }

    public CaseNode() {
        choices = new ArrayList<Choice>();

    }

    public void addChoiceCase(String t, AstNode e, AstNode b) {
        choices.add(new Choice(t, e, b));
    }

    public void addCase(String t, AstNode e, AstNode b) {
        IF = new Choice(t, e, b);
    }

    public void addDefault(String t, AstNode b) {
        ELSE = new Choice(t, null, b);
    }

    @Override
    public BarsicValue evaluate() {
        return new BarsicValue(this.toString(), true);
    }


    private class Choice {
        AstNode expression1;
        AstNode expression2;
        String token;

        Choice(String t, AstNode e1, AstNode e2) {
            token = t;
            expression1 = e1;
            expression2 = e2;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String assig = "";
        String bra = "";
        if (IF != null) {
            assig = IF.expression2 instanceof CaseNode ? "" : lookup+ " = ";
            sb.append(IF.token + " (" + IF.expression1 + ")\n{     " + assig + IF.expression2 + ";\n}");
        }
        for (Choice ifs : choices) {
            if (ifs != null) {
                if (!(ifs.expression2 instanceof CaseNode)) {
                    assig = lookup + " = ";
                    bra = ";";
                }
                sb.append(ifs.token + " (" + ifs.expression1 + ")\n{     " + assig + ifs.expression2 + bra + "\n}");
            }
        }
        if (ELSE != null) {
            if (!(ELSE.expression2 instanceof CaseNode)) {
                assig = lookup + " = ";
                bra = ";";
            }
            sb.append(ELSE.token + "\n{     " + assig + ELSE.expression2 + bra + "\n}");
        }
        return sb.toString();
    }
}
