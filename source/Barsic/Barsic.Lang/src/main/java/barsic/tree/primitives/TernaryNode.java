package barsic.tree.primitives;

import barsic.tree.BarsicValue;

public class TernaryNode implements AstNode {

    private AstNode condition;
    private AstNode ifTrue;
    private AstNode ifFalse;

    public TernaryNode(AstNode c, AstNode t, AstNode f) {
        condition = c;
        ifTrue = t;
        ifFalse = f;
    }

    @Override
    public BarsicValue evaluate() {

        BarsicValue a = condition.evaluate();

        if (!a.isBoolean()) {
            throw new RuntimeException("not a boolean expression: " + condition + ", in: " + this);
        }

        return a.asBoolean() ? ifTrue.evaluate() : ifFalse.evaluate();
    }

    @Override
    public String toString() {
        return String.format("(%s ? %s : %s)", condition, ifTrue, ifFalse);
    }
}
