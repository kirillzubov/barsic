package barsic.tree.primitives;


import barsic.tree.BarsicValue;

import java.util.ArrayList;
import java.util.List;

public class BlockNode implements AstNode {

    private List<AstNode> statements;
    private AstNode returnStatement;

    public BlockNode() {
        statements = new ArrayList<AstNode>();
        returnStatement = null;
    }

    public void addReturn(AstNode stat) {
        returnStatement = stat;
    }

    public void addStatement(AstNode stat) {
        statements.add(stat);
    }

    public List<AstNode> getStatements() {
        return statements;
    }

    @Override
    public BarsicValue evaluate() {
        for (AstNode stat : statements) {
            BarsicValue value = stat.evaluate();
            if (value != BarsicValue.VOID) {
                return value;
            }
        }
        return returnStatement == null ? BarsicValue.VOID : returnStatement.evaluate();
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        for (AstNode stat : statements) {
            if (stat.toString() != "") {
                String end = (stat instanceof IfNode || stat instanceof CaseNode )? "\n" : ";\n";
                b.append(stat).append(end);
            }
        }
        if (returnStatement != null) {
            b.append("return ").append(returnStatement).append("\n");
        }
        return b.toString();
    }
}
