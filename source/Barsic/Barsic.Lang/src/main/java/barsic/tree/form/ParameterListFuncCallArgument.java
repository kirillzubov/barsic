package barsic.tree.form;

import barsic.tree.BarsicValue;
import barsic.tree.primitives.AssignmentNode;

import java.util.ArrayList;
import java.util.List;

public class ParameterListFuncCallArgument extends ObjectInvocationBase {
    private final List<AssignmentNode> assignments;

    public ParameterListFuncCallArgument(String methodName, List<AssignmentNode> assignments) {
        super(methodName);
        this.assignments = assignments;
    }

    public ParameterListFuncCallArgument(String methodName) {
        super(methodName);
        this.assignments = new ArrayList<AssignmentNode>();
    }

    public List<AssignmentNode> getAssignments() {
        return assignments;
    }

    @Override
    public BarsicValue evaluate() {
        return null;
    }
}

