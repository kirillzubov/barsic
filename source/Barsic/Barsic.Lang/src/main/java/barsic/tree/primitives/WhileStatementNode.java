package barsic.tree.primitives;

import barsic.tree.BarsicValue;

public class WhileStatementNode implements AstNode {

    private AstNode expression;
    private AstNode block;

    public WhileStatementNode(AstNode exp, AstNode bl) {
        expression = exp;
        block = bl;
    }

    @Override
    public BarsicValue evaluate() {

        while (expression.evaluate().asBoolean()) {

            BarsicValue returnValue = block.evaluate();

            if (returnValue != BarsicValue.VOID) {
                return returnValue;
            }
        }

        return BarsicValue.VOID;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("while ");
        sb.append("("+expression+")");
        sb.append("{\n");
        sb.append(block);
        sb.append("}\n");
        return sb.toString();
    }
}
