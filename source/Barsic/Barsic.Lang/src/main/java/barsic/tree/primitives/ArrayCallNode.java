package barsic.tree.primitives;

import barsic.Scope;
import barsic.tree.BarsicValue;

import java.util.List;
/**
 * Created by Kirill Zubov on 2018.
 */

public class ArrayCallNode implements AstNode {
    public Scope scope;
    protected String name;
    public List<AstNode>values;


    public ArrayCallNode(String name, List<AstNode> val, Scope scope) {
        this.name = name;
        values = val;
        this.scope = scope;
    }

    @Override
    public BarsicValue evaluate() {
//        value.evaluate();
        return new BarsicValue(this.toString(), true);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(String.format("%s.%s", name, getNameMethod(values.size())));
        for (AstNode value:values) {
            sb.append(String.format("[(int)%s]", value));
        }
        return sb.toString();
    }

    String getNameMethod(int size){
        switch (size){
            case (1): return "getSArray()";
            case (2): return "getDArray()";
            case (3): return "getTArray()";
            default : return "getOArray()";
        }
    }
}
