package barsic.tree.primitives;

import barsic.Scope;
import barsic.Bootstrapper;
import barsic.core.IBarsicLib;
import barsic.exceptions.BarsicException;
import barsic.tree.BarsicValue;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kirill Zubov on 27.03.2015.
 */

public class FunctionCallNode implements AstNode {
    public Scope scope;
    protected String name;
    protected List<AstNode> value;

    public FunctionCallNode(String name, List<AstNode> nodes, Scope scope) {
        this.name = name;
        value = (nodes == null) ? new ArrayList<AstNode>() : nodes;
        this.scope = scope;
    }

    @Override
    public BarsicValue evaluate() {
        Class<?>[] classes = new Class[value.size()];
        Object[] v = new Object[value.size()];
        int i = 0;
        for (AstNode node : value) {
            BarsicValue value = node.evaluate();
            Class clas = value.getClassType();
            v[i] = value.getValue();
            classes[i] = clas;
            i++;
        }
        Class<IBarsicLib> moduleName = Bootstrapper.getModuleByExport(name).getNameClass();
        IBarsicLib o = null;
        Object a = null;
        Method m = null;
        try {
            if (Bootstrapper.getModuleByExport(name) != null) {
                m = moduleName.getMethod(name, classes);
            } else {
                System.out.println("not support evalute() for user's function");
            }
            o = moduleName.newInstance();
            a = m.invoke(o, v);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return new BarsicValue(a);
    }

    @Override
    public String toString() {
        String gen;
        if (Bootstrapper.getModuleByExport(name) == null) { //  user's function
            gen = String.format("%s(%s)", name, printValues(value));
        } else {                                                                  //default barsic function
            String moduleName = Bootstrapper.getModuleByExport(name).getName();
            gen = String.format("Bootstrapper.resolve(%s.class).%s(%s)", moduleName, name, printValues(value));
        }
        return gen;
    }

    public String printValues(List<AstNode> values) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.size() - 1; i++) {
            sb.append(values.get(i) + ",");
        }
        if (values.size() > 0) {
            sb.append(values.get(values.size() - 1));
        }
        return sb.toString();
    }

    public String getName() {
        return name;
    }






}
