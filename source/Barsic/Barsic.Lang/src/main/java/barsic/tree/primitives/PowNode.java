package barsic.tree.primitives;

import barsic.tree.BarsicValue;

public class PowNode implements AstNode {

    private AstNode lhs;
    private AstNode rhs;

    public PowNode(AstNode lhs, AstNode rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public BarsicValue evaluate() {

        BarsicValue a = lhs.evaluate();
        BarsicValue b = rhs.evaluate();

        // number ^ number
        if (a.isNumber() && b.isNumber()) {
            return new BarsicValue(Math.pow(a.asDouble(), b.asDouble()));
        }

        throw new RuntimeException("iIllegal expression: " + this);
    }

    @Override
    public String toString() {
        return String.format("(%s^%s)", lhs, rhs);
    }
}
