package barsic.tree.form;

import barsic.tree.primitives.AstNode;
import barsic.tree.primitives.BlockNode;

public class BlockFuncCallArgument extends ObjectInvocationBase {
    private final BlockNode block;

    public BlockFuncCallArgument(String methodName, AstNode block) {
        super(methodName);
        this.block = (BlockNode) block;
    }

    public BlockNode getBlock() {
        return block;
    }

}
