package barsic.tree.primitives;

import barsic.Scope;
import barsic.tree.BarsicValue;

import java.util.ArrayList;
import java.util.List;

public class IdentifiertList_and_typeNode implements AstNode {
    protected String type;
    protected  List<String> identifiertList;


    public IdentifiertList_and_typeNode(List<String> nodeIdentifiertList, String type) {
        identifiertList = (nodeIdentifiertList==null)?  new ArrayList<String>(): nodeIdentifiertList;
        this.type = type;
    }

    @Override
    public BarsicValue evaluate() {
        StringBuilder sb = new StringBuilder();
        for (String identifier : identifiertList) {
            sb.append(emitType(type)+" " +identifier+",");
        }
        sb.setLength(sb.length()-1);

        return new BarsicValue(sb.toString(),true);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public String emitType(String type) {
        if (type.equals("Number")) {
            return "double";
        } else if (type.equals("String")) {
            return "String";
        } else if (type.equals("Bool")) {
            return "boolean";
        } else
            return "Object";
    }

}
