package barsic.tree.functions;

import barsic.Bootstrapper;
import barsic.math.IBarsicMath;
import barsic.tree.BarsicValue;
import barsic.tree.primitives.AstNode;

import java.util.ArrayList;
import java.util.List;

public class MathFunctionNode implements AstNode {

    private List<AstNode> expressions;
    private int funcType;

    public MathFunctionNode(int type) {
        this.funcType = type;
    }

    public MathFunctionNode(int type, List<AstNode> nodes) {
        this.funcType = type;
        expressions = nodes;
    }

    public MathFunctionNode(int type, AstNode e) {
        this(type);
        expressions = new ArrayList<AstNode>();
        expressions.add(e);
    }

    @Override
    public BarsicValue evaluate() {
        BarsicValue[] value;

        Double singleDouble = Double.NaN;

        if (this.expressions != null) {
            value = new BarsicValue[this.expressions.size()];
            for (int i = 0; i < expressions.size(); i++) {
                value[i] = expressions.get(i).evaluate();
            }

            if (value.length > 0) {
                singleDouble = value[0].asDouble();
            }
        } else {
            value = new BarsicValue[0];
        }

        double[] dValues;
        IBarsicMath math = Bootstrapper.resolve(IBarsicMath.class);
        switch (this.funcType) {
            case MathFunctionType.PI:
                return new BarsicValue(math.pi());
            case MathFunctionType.INFINITY:
                return new BarsicValue(math.infinity());
            case MathFunctionType.SIN:
                return new BarsicValue(math.sin(singleDouble));
            case MathFunctionType.COS:
                return new BarsicValue(math.cos(singleDouble));
            case MathFunctionType.TAN:
                return new BarsicValue(math.tan(singleDouble));
            case MathFunctionType.COT:
                return new BarsicValue(math.cot(singleDouble));
            case MathFunctionType.LOG10:
                return new BarsicValue(math.log10(singleDouble));
            case MathFunctionType.SQUARE:
                return new BarsicValue(math.square(singleDouble));
            case MathFunctionType.SQRT:
                return new BarsicValue(math.sqrt(singleDouble));
            case MathFunctionType.ATAN:
                return new BarsicValue(math.atan(singleDouble));
            case MathFunctionType.ASIN:
                return new BarsicValue(math.asin(singleDouble));
            case MathFunctionType.ACOS:
                return new BarsicValue(math.acos(singleDouble));
            case MathFunctionType.ACOT:
                return new BarsicValue(math.acot(singleDouble));
            case MathFunctionType.SH:
                return new BarsicValue(math.sh(singleDouble));
            case MathFunctionType.CH:
                return new BarsicValue(math.ch(singleDouble));
            case MathFunctionType.ABS:
                return new BarsicValue(math.abs(singleDouble));
            case MathFunctionType.ROUND:
                return new BarsicValue(math.round(singleDouble));
            case MathFunctionType.FLOOR:
                return new BarsicValue(math.floor(singleDouble));
            case MathFunctionType.CEIL:
                return new BarsicValue(math.ceil(singleDouble));
            case MathFunctionType.TRUNC:
                return new BarsicValue(math.trunc(value[0].asDouble(), value[1].asDouble().byteValue()));
            case MathFunctionType.FRAC:
                return new BarsicValue(math.frac(singleDouble));
            case MathFunctionType.SIGN:
                return new BarsicValue(math.sign(singleDouble));
            case MathFunctionType.MIN:
                dValues = new double[value.length];
                for (int i = 0; i < value.length; i++) {
                    dValues[i] = value[i].asDouble();
                }
                return new BarsicValue(math.min(dValues));//todo:DRY
            case MathFunctionType.MAX:
                dValues = new double[value.length];
                for (int i = 0; i < value.length; i++) {
                    dValues[i] = value[i].asDouble();
                }
                return new BarsicValue(math.max(dValues));//todo:DRY
            case MathFunctionType.RANDOM:
                return new BarsicValue(math.random());
            case MathFunctionType.POWER:
                return new BarsicValue(math.power(value[0].asDouble(), value[1].asDouble()));//todo:
            case MathFunctionType.EXP:
                return new BarsicValue(math.exp(singleDouble));
        }
        return BarsicValue.VOID;
    }
}