package barsic.tree.primitives;


import barsic.tree.BarsicValue;

import java.util.ArrayList;
import java.util.List;

public class MulNode implements AstNode {

    private AstNode lhs;
    private AstNode rhs;

    public MulNode(AstNode lhs, AstNode rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public BarsicValue evaluate() {
        BarsicValue a = lhs.evaluate();
        BarsicValue b = rhs.evaluate();

        if(a.isIdentifier() || b.isIdentifier()){
            return new BarsicValue(a.emitValue() + "*" + b.emitValue(), a, b);
        }

        // number * number
        if (a.isNumber() && b.isNumber()) {
            return new BarsicValue(a.asDouble() * b.asDouble());
        }

        // string * number
        if (a.isString() && b.isNumber()) {
            StringBuilder str = new StringBuilder();
            int stop = b.asDouble().intValue();
            for (int i = 0; i < stop; i++) {
                str.append(a.asString());
            }
            return new BarsicValue(str.toString());
        }

        // list * number
        if (a.isList() && b.isNumber()) {
            List<BarsicValue> total = new ArrayList<BarsicValue>();
            int stop = b.asDouble().intValue();
            for (int i = 0; i < stop; i++) {
                total.addAll(a.asList());
            }
            return new BarsicValue(total);
        }

        throw new RuntimeException("illegal expression: " + this);
    }

    @Override
    public String toString() {
        return String.format("(%s * %s)", lhs, rhs);
    }
}
