package barsic.tree.primitives;

import java.util.ArrayList;
import java.util.List;

import barsic.tree.BarsicValue;
/**
 * Created by Kirill Zubov on 2017.
 */

public class ArrayNode implements AstNode {
    List<String> scopes;
    List<Integer> sizes = new ArrayList<Integer>();
    protected String type;

    public ArrayNode(List<String> s, String t) {
        type = t;
        scopes = s;

        for (String scope : s) {
            String[] border = scope.split("\\..");
            int size = Integer.parseInt(border[1]) + 1;
            sizes.add(size);
        }
    }

    @Override
    public BarsicValue evaluate() {
        return new BarsicValue(this.toString(), true);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(String.format("new BArray(%s.class,", emitType()));
        for (Integer size : sizes) {
            sb.append(String.format("%s,", size));
        }
        sb.deleteCharAt(sb.length()-1);
        sb.append(")");
        return sb.toString();
    }

    public String emitType() {
        if (type.equals("Number")) {
            return "Integer";
        } else if (type.equals("String")) {
            return "String";
        } else if (type.equals("Bool")) {
            return "Boolean";
        } else
            return "Object";
    }
}
