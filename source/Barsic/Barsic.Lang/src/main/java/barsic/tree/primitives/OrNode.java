package barsic.tree.primitives;

import barsic.tree.BarsicValue;

public class OrNode implements AstNode {

    private AstNode lhs;
    private AstNode rhs;

    public OrNode(AstNode lhs, AstNode rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public BarsicValue evaluate() {

        BarsicValue a = lhs.evaluate();
        BarsicValue b = rhs.evaluate();

        if (!a.isBoolean() || !b.isBoolean()) {
            throw new RuntimeException("illegal expression: " + this);
        }

        return new BarsicValue(a.asBoolean() || b.asBoolean());
    }

    @Override
    public String toString() {
        return String.format("(%s || %s)", lhs, rhs);
    }
}
