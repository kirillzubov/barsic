package barsic.tree.primitives;

import barsic.tree.BarsicValue;

import java.util.ArrayList;
import java.util.List;

public class LookupNode implements AstNode {

    private AstNode expression;
    private List<AstNode> indexes;

    public LookupNode(AstNode e, List<AstNode> i) {
        expression = e;
        indexes = i;
    }

    @Override
    public BarsicValue evaluate() {

        BarsicValue value = expression.evaluate();

        List<BarsicValue> indexValues = new ArrayList<BarsicValue>();

        for (AstNode indexNode : indexes) {
            indexValues.add(indexNode.evaluate());
        }

        for (BarsicValue index : indexValues) {

            if (!index.isNumber() || !(value.isList() || value.isString())) {
                throw new RuntimeException("illegal expression: " + expression + "[" + index + "]");
            }

            int idx = index.asLong().intValue();

            if (value.isList()) {
                value = value.asList().get(idx);
            } else if (value.isString()) {
                value = new BarsicValue(String.valueOf(value.asString().charAt(idx)));
            }
        }

        return value;
    }
}
