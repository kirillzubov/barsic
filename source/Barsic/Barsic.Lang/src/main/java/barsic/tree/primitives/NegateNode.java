package barsic.tree.primitives;

import barsic.tree.BarsicValue;

public class NegateNode implements AstNode {

    private AstNode exp;

    public NegateNode(AstNode e) {
        exp = e;
    }

    @Override
    public BarsicValue evaluate() {

        BarsicValue v = exp.evaluate();

        if (!v.isBoolean()) {
            throw new RuntimeException("illegal expression: " + this);
        }

        return new BarsicValue(!v.asBoolean());
    }

    @Override
    public String toString() {
        return String.format("(!%s)", exp);
    }
}
