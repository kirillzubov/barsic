package barsic.tree.primitives;
import barsic.Scope;
import barsic.tree.BarsicValue;
import java.util.List;
/**
 * Created by Kirill Zubov on 2018.
 */
public class BelongsNode implements AstNode {
    AstNode value;
    List<String>  sizes;
    Scope scope;

    public BelongsNode(AstNode id, List<String> sis, Scope s){
        value = id;
        sizes = sis;
        scope =s;
    }

    @Override
    public BarsicValue evaluate() {
//        BarsicValue value = scope.resolve(this.value.toString());
//        if (value == null) {
//            return new BarsicValue(this.value, true);
//        }
        return new BarsicValue(this.value);
    }

    @Override
    public String toString() {
        String ans;
        if (sizes.size()==2){
            ans = String.format("%s >= %s && %s <= %s ",value, sizes.get(0),value, sizes.get(1));
        }else {

            ans = String.format("Arrays.asList(new Integer[]{%s}).contains((int)%s)",
                    sizes.toString().substring(1,sizes.toString().length()-1), value);
            //Arrays.asList(new Integer[]{1,2,3}).contains((int)a)
        }
        return ans;
    }
}
