package barsic.tree.form;

import barsic.tree.primitives.AstNode;
import java.util.List;

/**
 * Created by user on 05/30/2016.
 */
public class FunctionCallArguments extends ParameterListFuncCallArgument  {

    private List<AstNode> args;
    public FunctionCallArguments(String methodName, List<AstNode> arguments) {
        super(methodName);
        this.args = arguments;
    }

    public List<AstNode> getArguments()
    {
        return args;
    }

}
