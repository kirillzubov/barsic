package barsic.tree.primitives;


import barsic.tree.BarsicValue;

import java.util.List;

public class InNode implements AstNode {

    private AstNode lhs;
    private AstNode rhs;

    public InNode(AstNode lhs, AstNode rhs) {
        this.lhs = lhs;
        this.rhs = rhs;
    }

    @Override
    public BarsicValue evaluate() {

        BarsicValue a = lhs.evaluate();
        BarsicValue b = rhs.evaluate();

        if (!rhs.evaluate().isList()) {
            throw new RuntimeException("illegal expression: " + this);
        }

        List<BarsicValue> list = b.asList();

        for (BarsicValue val : list) {
            if (val.equals(a)) {
                return new BarsicValue(true);
            }
        }

        return new BarsicValue(false);
    }

    @Override
    public String toString() {
        return String.format("(%s in %s)", lhs, rhs);
    }
}
