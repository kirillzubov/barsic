package barsic.tree.primitives;

import barsic.Scope;
import barsic.exceptions.BarsicException;
import barsic.tree.BarsicValue;
import barsic.tree.form.FormNode;
import barsic.tree.form.ObjectInvocationBase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AssignmentNode implements AstNode {

    public Scope scope;
    protected AstNode identifier;
    protected List<AstNode> indexNodes;
    protected List<AssignmentNode> childs;
    public AstNode value;
    public static Map<String, String> mapType = new HashMap<String, String>();


    public AssignmentNode(AstNode identifier, AstNode value, List<AssignmentNode> childs, Scope scope) {
        this.identifier = identifier;
        this.value = value;
        this.indexNodes = new ArrayList<AstNode>();
        this.childs = childs;
        this.scope = scope;
    }

    public String getName() {
        return ((FunctionCallNode) value).name;
    }

    public String getIdentifier() {
        return this.identifier.toString();
    }

    public String getType() {
        if (identifier instanceof IdentifierNode) {
            CharSequence com = ".";
            if (identifier.toString().contains(com)) {
                return "";
            }
        }
        if (identifier instanceof ArrayCallNode) {
            return "";
        }
        if (value instanceof ArrayCallNode) {
            String type = mapType.get(((ArrayCallNode) value).name);
            if (type == null) {
                try {
                    throw new BarsicException("Not initialized array for value is  " + value);
                } catch (BarsicException e) {
                    e.printStackTrace();
                }
            }
            return mapType.get(((ArrayCallNode) value).name);
        }

        if (value instanceof BlockNode) {
            return null;
        }
        if (value instanceof FormNode) {
            return "IForm";
        }

        if (value instanceof AtomNode) {
            if (identifier.toString().startsWith("str")) {
                return "String";
            }
            //todo:remove this creepy hack
            if (identifier.toString().startsWith("i") && !identifier.toString().startsWith("is")) {
                return "int";
            }
            return value.evaluate().emitType();
        }

        if (value instanceof FunctionCallNode) {
            if (getName().equals("str")) {

                return "String";
            } else {
                return "double";
            }
        }

        if (value instanceof ArrayNode) {
            StringBuilder sb= new StringBuilder(" ");
           // for (int i = 0; i < ((ArrayNode) value).sizes.size(); i++) {
          //      sb.append("[]");
          //  }
           // String br = sb.toString();
            mapType.put(identifier.toString(), ((ArrayNode) value).emitType());
            return "IArray<"+((ArrayNode) value).emitType()+">";
        }
        if (value instanceof IdentifierNode) {

            if (value.toString().startsWith("str")) {
                return "String";
            } else if (value.toString().startsWith("i") && !value.toString().startsWith("is")) {
                return "int";
            }
            return "double";

        }

   //     if (value instanceof ObjectInvocation2Node) {
    //        return "";
    //    }

//        if (value instanceof CaseNode) {
//            return "double";
//        }
        return value.evaluate().emitType();
    }

    @Override
    public BarsicValue evaluate() {
        if (value != null) {
            BarsicValue value = this.value.evaluate();

            if (value == BarsicValue.VOID) {
                throw new RuntimeException("can't assign VOID to " + identifier);
            }
            scope.assign(identifier.toString(), value);
        }
        if (childs != null) {
            StringBuilder calls = new StringBuilder();
            for (int i = 0; i < childs.size(); i++) {
                AssignmentNode asNode = childs.get(i);
                BarsicValue eval = asNode.evaluate();
                if (eval == BarsicValue.VOID) {
                    eval = asNode.scope.resolve(asNode.getIdentifier());
                }
                calls.append(eval);
                if (i < childs.size() - 1) {
                    calls.append(",");
                }
            }
            scope.assign(identifier.toString(), new BarsicValue(calls.toString()));
        }
        return BarsicValue.VOID;
    }

    @Override
    public String toString() {
        return String.format("%s %s = %s", getType(), getIdentifier(), value.evaluate().emitValue());
    } //indexNodes,
}
