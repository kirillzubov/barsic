package barsic.tree.primitives;

import barsic.tree.BarsicValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kirill Zubov on 2018.
 */

public class ProcedureFuncNode implements AstNode {
    protected String name;
    protected List<IdentifiertList_and_typeNode> procedureParamList;
    protected List<String> identifiertList;
    protected List<AssignmentNode> varibleList;
    protected AstNode block;

    public ProcedureFuncNode(String name, List<IdentifiertList_and_typeNode> procedureParamListNode, List<String> identifiertListNode, List<AssignmentNode> varibleListNode, AstNode block) {
        this.name = name;
        procedureParamList = (procedureParamListNode == null) ? new ArrayList<IdentifiertList_and_typeNode>() : procedureParamListNode;
        identifiertList = (identifiertListNode == null) ? new ArrayList<String>() : identifiertListNode;
        varibleList = (varibleListNode == null) ? new ArrayList<AssignmentNode>() : varibleListNode;
        this.block = block;
    }

    public String getVaribleList() {
        StringBuilder sb = new StringBuilder();
        for (AssignmentNode varible : varibleList) {
            sb.append(varible.toString() + ";\n");
        }
        return sb.toString();

    }

    public String getProcedureParam() {
        StringBuilder sb = new StringBuilder();
        for (IdentifiertList_and_typeNode param : procedureParamList) {
            sb.append(param.evaluate() + ",");
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    @Override
    public BarsicValue evaluate() {
        return new BarsicValue(this.toString(), true);
    }

    @Override
    public String toString() {
        StringBuilder gen = new StringBuilder();
        gen.append("public void " + name + " (");
        gen.append(getProcedureParam() + "){\n");
        gen.append(getVaribleList() + '\n');
        gen.append(block);
        gen.append("    }\n");
        return gen.toString();
    }



}
