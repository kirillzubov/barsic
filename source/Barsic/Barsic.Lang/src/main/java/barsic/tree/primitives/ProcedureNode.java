package barsic.tree.primitives;

import barsic.tree.BarsicValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 14.03.2015.
 */
public class ProcedureNode implements AstNode {
    protected String type;
    protected String name;
    protected List<AstNode> value;
    protected AstNode index;
    protected AstNode block;

    public ProcedureNode(String name, List<AstNode> nodesExp, AstNode index, AstNode nodesBlock) {
        this.name = name;
        value = (nodesExp == null) ? new ArrayList<AstNode>() : nodesExp;
       this.block = nodesBlock;
        this.index = index;
    }


    @Override
    public BarsicValue evaluate() {
        List<BarsicValue> evaluated = new ArrayList<BarsicValue>();
        for (AstNode node : value) {
            evaluated.add(node.evaluate());
        }
        return new BarsicValue(evaluated);
    }

    public String printValues(List<AstNode> values) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.size() - 1; i++) {
            sb.append(values.get(i) + ",");
        }

        if (values.size() > 0) {
            sb.append(values.get(values.size() - 1));
        }

        return sb.toString();

    }


    public String getName() {
        return name;
    }

    public List<AstNode> getValue() {
        return value;
    }

}
