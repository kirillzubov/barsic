package barsic.tree.primitives;

import barsic.Scope;
import barsic.tree.BarsicValue;
/**
 * Created by Kirill Zubov on 2018.
 */
public class CaseAssigmentNode extends  CaseNode implements AstNode {
    AstNode lookup;
    CaseNode caseStatement;
    Scope currentScope;

    public CaseAssigmentNode(AstNode lookup, AstNode caseStatement, Scope currentScope) {
        this.lookup = lookup;
        this.caseStatement = (CaseNode) caseStatement;
        this.currentScope = currentScope;
        this.caseStatement.setLookup(lookup.toString());
    }

    @Override
    public BarsicValue evaluate() {
        return null;
    }
    public String getType(){
        return null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Object "+lookup+";\n");
        caseStatement.setLookup(lookup.toString());
        sb.append(caseStatement);
        return sb.toString();
    }
}
