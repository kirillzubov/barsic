package barsic.tree.primitives;

import antlr.collections.AST;
import barsic.tree.BarsicValue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/**
 * Created by Kirill Zubov on 2018.
 */
public class ObjectInvocation2Node implements AstNode {
    AstNode typeCast;
    List<AstNode> typeList;

    public ObjectInvocation2Node(AstNode tc1, List<AstNode> list) {
        typeCast = tc1;
        typeList = (list == null) ? new ArrayList<AstNode>() : list;
    }

    @Override
    public BarsicValue evaluate() {
        return new BarsicValue(toString(), true);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(typeCast);
        sb.append(".");
        sb.append(getStringList(typeList));
        return sb.toString();
    }

    public String getStringList(List<AstNode> list) {
        StringBuilder sb = new StringBuilder();
        for (AstNode l : list) {
            if (l instanceof IdentifierNode) {
                sb.append("get" + upperCaseFirst(l.toString()) + "().");
            }else {
                sb.append(l + ".");
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public static String upperCaseFirst(String value) {
        char[] array = value.toCharArray();
        array[0] = Character.toUpperCase(array[0]);
        return new String(array);
    }

}
