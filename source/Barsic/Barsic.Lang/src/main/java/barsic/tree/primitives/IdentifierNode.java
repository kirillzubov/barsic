package barsic.tree.primitives;

import barsic.Scope;
import barsic.tree.BarsicValue;

public class IdentifierNode implements AstNode {

    private String identifier;
    public Scope scope;

    public IdentifierNode(String id, Scope s) {
        identifier = id;
        scope = s;
    }

    @Override
    public BarsicValue evaluate() {
        BarsicValue value = scope.resolve(identifier);
        if (value == null) {
            return new BarsicValue(this.identifier, true);
        }
        return value;
    }

    public Scope getScope() {
        return scope;
    }

    @Override
    public String toString() {
        return identifier;
    }
}
