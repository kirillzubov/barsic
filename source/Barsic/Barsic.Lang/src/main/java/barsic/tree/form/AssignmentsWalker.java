package barsic.tree.form;
import barsic.tree.BarsicValue;
import barsic.tree.primitives.AssignmentNode;
import barsic.tree.primitives.AstNode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ru
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public final class AssignmentsWalker {
    public static Map<String, BarsicValue> Find(List<AssignmentNode> assignments) {
        Map<String, BarsicValue> map = new HashMap<String, BarsicValue>();
        for (AstNode node : assignments) {
            if (node instanceof AssignmentNode) {
                AssignmentNode asNode = (AssignmentNode) node;
                BarsicValue eval = asNode.evaluate();
                if (eval == BarsicValue.VOID) {
                    eval = asNode.scope.resolve(asNode.getIdentifier());
                }
                map.put(asNode.getIdentifier(), eval);
            }
        }
        return map;
    }

    public static void emit(StringBuilder sb, Map<String, BarsicValue> items, String id) {
        for (Map.Entry<String, BarsicValue> entry : items.entrySet()) {
            String setter = formatSetterName(entry.getKey());
            Boolean isEventHandler = entry.getKey().trim().toLowerCase().startsWith("on");
            if (!"setType".equals(setter) && !isEventHandler) {
                sb.append(id + "." + formatSetterName(entry.getKey()) + "(" + entry.getValue().emitValue() + ");\n");
            }else{
                if(isEventHandler) {
                    sb.append(id + "." + formatSetterName(entry.getKey()) + "(" + "new Runnable() {@Override public void run() { getModule().form1_button1_onClick(); }   }" + ");\n");
                }
            }
        }
    }

    public static String formatSetterName(String propertyName) {
        return "set" + formatPropName(propertyName);
    }

    public static String formatGetterName(String propertyName) {
        return "get" + formatPropName(propertyName);
    }

    private static String formatPropName(String propertyName)
    {
        String trimmed = propertyName.trim();
        String capitalLetter = Character.toString(trimmed.charAt(0)).toUpperCase();
        String firstLetterCapitalized = capitalLetter + trimmed.substring(1, trimmed.length());
        Map<String, String> replaceMap = new HashMap<String, String>();
        Map<String, String> typeMapping = new HashMap<String, String>();
        typeMapping.put("Top", "TopPos");
        typeMapping.put("Left", "LeftPos");
        if (typeMapping.containsKey(firstLetterCapitalized)) {
            firstLetterCapitalized = typeMapping.get(firstLetterCapitalized);
        }
        return firstLetterCapitalized;
    }
}
