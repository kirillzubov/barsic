package barsic.tree.form;

import barsic.tree.BarsicValue;
import barsic.tree.primitives.AssignmentNode;
import barsic.tree.primitives.AstNode;

import java.util.List;

public class ButtonNode implements AstNode {

    private final List<AssignmentNode> assignments;

    public ButtonNode(List<AssignmentNode> assignments) {
        this.assignments = assignments;
    }

    @Override
    public BarsicValue evaluate() {
        return BarsicValue.VOID;
    }
}
