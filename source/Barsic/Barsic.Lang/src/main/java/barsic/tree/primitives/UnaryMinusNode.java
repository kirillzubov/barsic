package barsic.tree.primitives;

import barsic.tree.BarsicValue;

public class UnaryMinusNode implements AstNode {

    private AstNode exp;

    public UnaryMinusNode(AstNode e) {
        exp = e;
    }

    @Override
    public BarsicValue evaluate() {

        BarsicValue v = exp.evaluate();

        if (!v.isNumber()) {
            throw new RuntimeException("illegal expression: " + this);
        }

        return new BarsicValue(-v.asDouble());
    }

    @Override
    public String toString() {
        return String.format("(-%s)", exp);
    }
}
