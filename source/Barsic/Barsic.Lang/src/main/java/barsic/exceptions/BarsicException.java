package barsic.exceptions;

public class BarsicException extends Exception {
    public BarsicException(String message) {
        super(message);
    }

    public BarsicException(String message, Exception inner) {
        super(message, inner);
    }
}
