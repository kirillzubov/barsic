package barsic.modules;


import barsic.ui.*;
import barsic.ui.swing.form.*;
import com.google.inject.Binder;
import com.google.inject.Module;

/**
 * Maxim Maximov, 2012
 * 2xmax@mail.ru
 * cph.maxim.maximov
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */
public class SwingUIModule implements Module {

    public void configure(Binder binder) {
        binder.bind(IForm.class).to(Form.class);
        binder.bind(IButton.class).to(Button.class);
        binder.bind(ITextField.class).to(TextBox.class);
        binder.bind(ITextLabel.class).to(TextLabel.class);
        binder.bind(ITextArea.class).to(TextArea.class);
        binder.bind(ISubwindow.class).to(Subwindow.class);
        binder.bind(IBrowser.class).to(Browser.class);
    }
}
