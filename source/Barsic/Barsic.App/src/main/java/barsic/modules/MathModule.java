package barsic.modules;

import barsic.math.BarsicMath;
import barsic.math.IBarsicMath;
import com.google.inject.Binder;
import com.google.inject.Module;

/**
 * Maxim Maximov, 2012
 * 2xmax@mail.ru
 * cph.maxim.maximov
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */
public class MathModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bind(IBarsicMath.class).to(BarsicMath.class);
    }
}
