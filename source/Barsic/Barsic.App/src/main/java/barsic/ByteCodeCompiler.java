package barsic;

import barsic.Bootstrapper;
import barsic.exceptions.BarsicException;
import barsic.modules.MathModule;
import barsic.modules.SwingUIModule;

import java.io.File;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ru
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public class ByteCodeCompiler {
    public static void main(String[] args) throws BarsicException {
        Bootstrapper.init(new MathModule(), new SwingUIModule());
        Barsic barsic = new Barsic();

        switch (args.length) {
            case 0:
                System.out.println("Signature: input-file [optional custom-output-class-file-path]");
                System.out.println("Running program in demo mode:");
                System.out.println("input program:");
                String inputProgram =
                        "Form \n" +
                                "width=1000;\n" +
                                "height=300;\n" +
                                "title=\"AAA\";\n" +
                                "_Form;\n";
                System.out.println(inputProgram);

                File outputFile = new File("Calculator.class");
                System.out.println("Output file has been compiled at " + outputFile.getAbsolutePath());
                barsic.compileByteCode(inputProgram, outputFile);
                barsic.run(outputFile);
                break;
            case 1:
                barsic.compileByteCode(new File(args[0]));
                break;
            case 2:
                barsic.compileByteCode(new File(args[0]), new File(args[1]));
                break;
            default:
                System.out.println("Wrong number of arguments provided");
                break;
        }
    }

}
