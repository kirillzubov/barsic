package barsic;

import barsic.Bootstrapper;
import barsic.exceptions.BarsicException;
import barsic.modules.MathModule;
import barsic.modules.SwingUIModule;

import java.io.File;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ru
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public class ByteCodeRunner {
    public static void main(String[] args) throws BarsicException {
        Bootstrapper.init(new MathModule(), new SwingUIModule());
        Barsic barsic = new Barsic();
        switch (args.length) {
            case 0:
                System.out.println("Signature: file-path");
                File inputFile = new File("Calculator.class");
                if (inputFile.exists()) {
                    System.out.println("Running program in demo mode");
                    barsic.run(inputFile);
                }
                break;
            case 1:
                barsic.run(new File(args[0]));
                break;
            default:
                System.out.println("Wrong number of arguments provided.");
                System.out.println("Signature: file-path");

        }

    }
}
