package barsic;

import barsic.Bootstrapper;
import barsic.exceptions.BarsicException;
import barsic.math.BarsicMath;
import barsic.math.StdLib;

import java.io.File;
import java.io.IOException;

/**
 * Maxim Maximov, 2013
 * 2xmax@mail.ruN
 * MSc, 2nd year
 * St Petersburg State University
 * Physics Faculty
 * Department of Computational Physics
 */

public class JavaTranslator {
    public static void main(String[] args) throws IOException, BarsicException {
        Bootstrapper.registerModule(new BarsicMath());
        Bootstrapper.registerModule(new StdLib());
        Barsic barsic = new Barsic();
        File inputFile;
        File outputDir;
        if(args.length == 0){
            inputFile = new File("Barsic.App/Samples/Forms/Project.bpr");
            outputDir = new File("Barsic.App.CodegenNew/src/barsic/app");
            barsic.translateToJava(inputFile, outputDir);
        } else {
            inputFile = new File(args[0]);
            outputDir = new File(args[1]);
            barsic.translateToJava(inputFile, outputDir);
        }
    }
}
