package barsic.app;

import barsic.core.Bootstrapper;
import UI.spbu.barsic.*;
public class MainForm{
    
public static double createversion(){
        double version = 1185.0;

                return version;
}

    
public static IForm createform1(){
        IForm form1 = Bootstrapper.resolve(IForm.class);
form1.setTitle("Components task");
form1.setLeft(221);
form1.setTop(135);
form1.setClientWidth(355);
form1.setClientHeight(261);
form1.setHint("");
form1.setFont("Courier New,15.0,false,false,false,false,FontScript.CYRILLIC,clWindowText");
form1.setBackColor("#FFFFFF");
form1.setIsBorderCloseIcon(true);
form1.setIsResizeable(false);
form1.setIsEnabledOnCalc(false);

ITextField textField1 = Bootstrapper.resolve(ITextField.class);
textField1.setAssociatedVariable("");
textField1.setIsEnabledOnCalc(false);
textField1.setIsParentBackColor(false);
textField1.setIsVisible(true);
textField1.setIsParentFont(true);
textField1.setIsReadOnly(true);
textField1.setTop(15);
textField1.setBackColor("#FFFFFF");
textField1.setLeft(91);
textField1.setIsSourceCode(false);
textField1.setHint("HINT!!");
textField1.setIsEnabled(false);
textField1.setName("textField1");
textField1.setWidth(240);
textField1.setText("42");
textField1.setHeight(27);
form1.addElement(textField1);
ITextLabel textLabel1 = Bootstrapper.resolve(ITextLabel.class);
textLabel1.setTextAlign(15);
textLabel1.setIsEnabledOnCalc(false);
textLabel1.setIsParentBackColor(true);
textLabel1.setAssociatedExpression("");
textLabel1.setIsVisible(true);
textLabel1.setIsParentFont(true);
textLabel1.setTop(19);
textLabel1.setLeft(15);
textLabel1.setHint("tooltip");
textLabel1.setIsEnabled(true);
textLabel1.setName("textLabel1");
textLabel1.setWidth(71);
textLabel1.setText("Answer=");
textLabel1.setHeight(24);
form1.addElement(textLabel1);
ITextArea textArea1 = Bootstrapper.resolve(ITextArea.class);
textArea1.setIsScrollBars(true);
textArea1.setAssociatedVariable("");
textArea1.setIsEnabledOnCalc(false);
textArea1.setIsParentBackColor(false);
textArea1.setIsVisible(true);
textArea1.setIsParentFont(true);
textArea1.setIsReadOnly(false);
textArea1.setTop(64);
textArea1.setBackColor("#F3F3AB");
textArea1.setIsWordWrap(false);
textArea1.setLeft(19);
textArea1.setIsSourceCode(false);
textArea1.setHint("");
textArea1.setIsEnabled(true);
textArea1.setName("textArea1");
textArea1.setWidth(312);
textArea1.setText("My review goes here...");
textArea1.setHeight(123);
form1.addElement(textArea1);
IButton button2 = Bootstrapper.resolve(IButton.class);
button2.setIsDefaultOK(false);
button2.setIsEnabledOnCalc(false);
button2.setIsVisible(false);
button2.setIsParentFont(true);
button2.setTop(204);
button2.setLeft(24);
button2.setHint("");
button2.setIsEnabled(true);
button2.setName("button2");
button2.setWidth(172);
button2.setText("invisible button");
button2.setHeight(27);
form1.addElement(button2);
IButton button1 = Bootstrapper.resolve(IButton.class);
button1.setIsDefaultOK(false);
button1.setIsEnabledOnCalc(false);
button1.setIsVisible(true);
button1.setIsParentFont(true);
button1.setTop(204);
button1.setLeft(207);
button1.setHint("another tooltip here");
button1.setIsEnabled(false);
button1.setName("button1");
button1.setWidth(128);
button1.setText("Submit");
button1.setHeight(27);
form1.addElement(button1);

                return form1;
}

    
public static IForm createform2(){
        IForm form2 = Bootstrapper.resolve(IForm.class);
form2.setTitle("Layout task");
form2.setLeft(59);
form2.setTop(29);
form2.setClientWidth(441);
form2.setClientHeight(398);
form2.setHint("");
form2.setFont("Courier New,15.0,false,false,false,false,FontScript.CYRILLIC,clWindowText");
form2.setBackColor("clBtnFace");
form2.setIsBorderCloseIcon(true);
form2.setIsResizeable(false);
form2.setIsEnabledOnCalc(false);

IButton button1 = Bootstrapper.resolve(IButton.class);
button1.setIsDefaultOK(false);
button1.setIsEnabledOnCalc(false);
button1.setIsVisible(true);
button1.setIsParentFont(true);
button1.setTop(12);
button1.setLeft(24);
button1.setHint("");
button1.setIsEnabled(true);
button1.setName("button1");
button1.setWidth(115);
button1.setText("Each");
button1.setHeight(76);
form2.addElement(button1);
IButton button4 = Bootstrapper.resolve(IButton.class);
button4.setIsDefaultOK(false);
button4.setIsEnabledOnCalc(false);
button4.setIsVisible(true);
button4.setIsParentFont(true);
button4.setTop(36);
button4.setLeft(155);
button4.setHint("");
button4.setIsEnabled(true);
button4.setName("button4");
button4.setWidth(51);
button4.setText("Button");
button4.setHeight(19);
form2.addElement(button4);
IButton button2 = Bootstrapper.resolve(IButton.class);
button2.setIsDefaultOK(false);
button2.setIsEnabledOnCalc(false);
button2.setIsVisible(true);
button2.setIsParentFont(true);
button2.setTop(59);
button2.setLeft(271);
button2.setHint("");
button2.setIsEnabled(true);
button2.setName("button2");
button2.setWidth(128);
button2.setText("have");
button2.setHeight(27);
form2.addElement(button2);
IButton button3 = Bootstrapper.resolve(IButton.class);
button3.setIsDefaultOK(false);
button3.setIsEnabledOnCalc(false);
button3.setIsVisible(true);
button3.setIsParentFont(true);
button3.setTop(115);
button3.setLeft(68);
button3.setHint("");
button3.setIsEnabled(true);
button3.setName("button3");
button3.setWidth(335);
button3.setText("fixed");
button3.setHeight(27);
form2.addElement(button3);
IButton button5 = Bootstrapper.resolve(IButton.class);
button5.setIsDefaultOK(false);
button5.setIsEnabledOnCalc(false);
button5.setIsVisible(true);
button5.setIsParentFont(true);
button5.setTop(152);
button5.setLeft(167);
button5.setHint("");
button5.setIsEnabled(true);
button5.setName("button5");
button5.setWidth(76);
button5.setText("position");
button5.setHeight(27);
form2.addElement(button5);
IButton button6 = Bootstrapper.resolve(IButton.class);
button6.setIsDefaultOK(false);
button6.setIsEnabledOnCalc(false);
button6.setIsVisible(true);
button6.setIsParentFont(true);
button6.setTop(187);
button6.setLeft(135);
button6.setHint("");
button6.setIsEnabled(true);
button6.setName("button6");
button6.setWidth(184);
button6.setText("Size");
button6.setHeight(155);
form2.addElement(button6);
IButton button7 = Bootstrapper.resolve(IButton.class);
button7.setIsDefaultOK(false);
button7.setIsEnabledOnCalc(false);
button7.setIsVisible(true);
button7.setIsParentFont(true);
button7.setTop(207);
button7.setLeft(91);
button7.setHint("");
button7.setIsEnabled(true);
button7.setName("button7");
button7.setWidth(68);
button7.setText("and");
button7.setHeight(39);
form2.addElement(button7);

                return form2;
}


}



