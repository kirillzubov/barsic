package barsic.app;

import barsic.core.Bootstrapper;
import barsic.app.modules.*;
import barsic.core.IBarsicModule;

public final class ModuleLoader {
    private static void registerModules() {
        Bootstrapper.init(new MathModule(), new SwingUIModule());
    }

    public static void main(String[] args) {
        registerModules();
        IBarsicModule module = new MainModule();
        module.onProgramStart();
    }
}