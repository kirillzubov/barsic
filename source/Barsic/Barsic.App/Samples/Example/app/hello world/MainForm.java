package barsic.app;

import barsic.*;
import barsic.math.*;
import barsic.standartClass.*;
import barsic.ui.*;
import barsic.ui.swing.form.Form;

import java.util.Arrays;
public class MainForm{

    public static MainModule getModule()
    {
        return MainModule.getInstance();
    }
double version = 1187;
    
    public static IForm createform1(){
        return new MainForm.form1();
}

    
public static class form1 extends Form {        
       form1() {
            createComponents();
        }
        protected void createComponents() {
IForm form1 = this;
form1.setTitle("Простая программа");
form1.setOnShow("");
form1.setLeft(235);
form1.setTop(232);
form1.setClientWidth(401);
form1.setClientHeight(250);
form1.setHint("");
form1.setFont("Courier New,16,false,false,false,false,FontScript.getCYRILLIC(),clWindowText");
form1.setIsBorderCloseIcon(true);
form1.setIsResizeable(false);
form1.setIsEnabledOnCalc(false);

ITextLabel textLabel1 = Bootstrapper.resolve(ITextLabel.class);
textLabel1.setIsEnabledOnCalc(false);
textLabel1.setIsParentBackColor(true);
textLabel1.setAssociatedExpression("");
textLabel1.setIsVisible(true);
textLabel1.setIsParentFont(true);
textLabel1.setTopPos(24);
textLabel1.setLeftPos(76);
textLabel1.setHint("");
textLabel1.setIsEnabled(true);
textLabel1.setName("textLabel1");
textLabel1.setWidth(232);
textLabel1.setText("--------------------------");
textLabel1.setHeight(40);
form1.addElement(textLabel1);
IButton button2 = Bootstrapper.resolve(IButton.class);
button2.setOnClick(new Runnable() {@Override public void run() { getModule().form1_button2_onClick(); }   });
button2.setIsDefaultOK(false);
button2.setIsEnabledOnCalc(false);
button2.setIsVisible(true);
button2.setIsParentFont(false);
button2.setTopPos(96);
button2.setLeftPos(8);
button2.setHint("");
button2.setIsEnabled(true);
button2.setName("button2");
button2.setWidth(248);
button2.setText("Нажми меня");
button2.setHeight(28);
button2.setFont("Courier New,16,false,false,false,false,FontScript.getCYRILLIC(),clWindowText");
form1.addElement(button2);
IButton button1 = Bootstrapper.resolve(IButton.class);
button1.setOnClick(new Runnable() {@Override public void run() { getModule().form1_button1_onClick(); }   });
button1.setIsDefaultOK(false);
button1.setIsEnabledOnCalc(false);
button1.setIsVisible(true);
button1.setIsParentFont(true);
button1.setTopPos(96);
button1.setLeftPos(264);
button1.setHint("");
button1.setIsEnabled(true);
button1.setName("button1");
button1.setWidth(128);
button1.setText("OK");
button1.setHeight(28);
form1.addElement(button1);

}
}

}



