package barsic.app;
import barsic.*;
import barsic.math.*;
import barsic.standartClass.*;
import barsic.ui.*;
import barsic.ui.swing.form.Form;
import java.util.Arrays;

public class MainModule implements barsic.core.IBarsicModule {
    private static MainModule instance;
    public MainModule() {
        instance = this;
    }
    public static MainModule getInstance() {
        return instance;
    }

    public void onProgramStart() {
        form1 = MainForm.createform1();
        initForm();
        form1.showForm();

    }

    public void form1_button2_onClick() {
        textLabel1.setText("hello world!");

    }

    public void form1_button1_onClick() {
        barsic = MainForm.createform1();
        initForm();
        barsic.close();

    }

    ITextLabel textLabel1;
    IButton button2;
    IButton button1;
    IForm form1;
    IForm barsic;

    void initForm() {
        textLabel1 = (ITextLabel) form1.getElementById("textLabel1");
        button2 = (IButton) form1.getElementById("button2");
        button1 = (IButton) form1.getElementById("button1");
    }

}



