package barsic.app;

import barsic.*;
import barsic.math.*;
import barsic.standartClass.*;
import barsic.ui.*;
import barsic.ui.swing.form.Form;

import java.util.Arrays;

public class MainForm {

    public static MainModule getModule() {
        return MainModule.getInstance();
    }

    double version = 1187;

    public static IForm createform1() {
        return new MainForm.form1();
    }


    public static class form1 extends Form {
        form1() {
            createComponents();
        }

        protected void createComponents() {
            IForm form1 = this;
            form1.setTitle("application");
            form1.setOnShow("");
            form1.setLeft(235);
            form1.setTop(232);
            form1.setClientWidth(401);
            form1.setClientHeight(250);
            form1.setHint("");
            form1.setFont("Courier New,16,false,false,false,false,FontScript.getCYRILLIC(),clWindowText");
            form1.setIsBorderCloseIcon(true);
            form1.setIsResizeable(false);
            form1.setIsEnabledOnCalc(false);

            ITextLabel textLabel1 = Bootstrapper.resolve(ITextLabel.class);
            textLabel1.setIsEnabledOnCalc(false);
            textLabel1.setIsParentBackColor(true);
            textLabel1.setAssociatedExpression("");
            textLabel1.setIsVisible(true);
            textLabel1.setIsParentFont(true);
            textLabel1.setTopPos(24);
            textLabel1.setLeftPos(76);
            textLabel1.setHint("");
            textLabel1.setIsEnabled(true);
            textLabel1.setName("textLabel1");
            textLabel1.setWidth(232);
            textLabel1.setText("--------------------------");
            textLabel1.setHeight(40);
            form1.addElement(textLabel1);
            IButton button1 = Bootstrapper.resolve(IButton.class);
            button1.setOnClick(new Runnable() {
                @Override
                public void run() {
                    getModule().form1_button1_onClick();
                }
            });
            button1.setIsDefaultOK(false);
            button1.setIsEnabledOnCalc(false);
            button1.setIsVisible(true);
            button1.setIsParentFont(true);
            button1.setTopPos(96);
            button1.setLeftPos(8);
            button1.setHint("");
            button1.setIsEnabled(true);
            button1.setName("button1");
            button1.setWidth(128);
            button1.setText("calculate");
            button1.setHeight(28);
            form1.addElement(button1);
            IButton button2 = Bootstrapper.resolve(IButton.class);
            button2.setOnClick(new Runnable() {
                @Override
                public void run() {
                    getModule().form1_button2_onClick();
                }
            });
            button2.setIsDefaultOK(false);
            button2.setIsEnabledOnCalc(false);
            button2.setIsVisible(true);
            button2.setIsParentFont(false);
            button2.setTopPos(96);
            button2.setLeftPos(150);
            button2.setHint("");
            button2.setIsEnabled(true);
            button2.setName("button2");
            button2.setWidth(128);
            button2.setText("set index");
            button2.setHeight(28);
            button2.setFont("Courier New,16,false,false,false,false,FontScript.getCYRILLIC(),clWindowText");
            form1.addElement(button2);
            ITextField textField1 = Bootstrapper.resolve(ITextField.class);
            textField1.setAssociatedVariable("s");
            textField1.setIsEnabledOnCalc(false);
            textField1.setIsParentBackColor(false);
            textField1.setIsVisible(true);
            textField1.setIsParentFont(true);
            textField1.setIsReadOnly(false);
            textField1.setTopPos(16);
            textField1.setLeftPos(250);
            textField1.setIsSourceCode(true);
            textField1.setHint("");
            textField1.setIsEnabled(true);
            textField1.setName("textField1");
            textField1.setWidth(141);
            textField1.setText("1");
            textField1.setHeight(31);
            form1.addElement(textField1);
            ITextField textField2 = Bootstrapper.resolve(ITextField.class);
            textField2.setAssociatedVariable("s");
            textField2.setIsEnabledOnCalc(false);

            textField2.setIsParentBackColor(false);
            textField2.setIsVisible(true);
            textField2.setIsParentFont(true);
            textField2.setIsReadOnly(false);
            textField2.setTopPos(60);
            textField2.setLeftPos(250);
            textField2.setIsSourceCode(true);
            textField2.setHint("");
            textField2.setIsEnabled(true);
            textField2.setName("textField2");
            textField2.setWidth(141);
            textField2.setText("2");
            textField2.setHeight(31);
            form1.addElement(textField2);

        }
    }

}



