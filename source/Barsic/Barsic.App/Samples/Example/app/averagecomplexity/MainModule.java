package barsic.app;

import barsic.*;
import barsic.math.*;
import barsic.standartClass.*;
import barsic.ui.*;
import barsic.ui.swing.form.Form;

import java.util.Arrays;

public class MainModule implements barsic.core.IBarsicModule {
    private static MainModule instance;

    public MainModule() {
        instance = this;
    }

    public static MainModule getInstance() {
        return instance;
    }

    public void onProgramStart() {
        form1 = MainForm.createform1();
        initForm();
        form1.showForm();
        fib(2);

    }

    String text = "0";
    String index = "0";
    IArray<Integer> fibNums = new BArray(Integer.class, 41);
    IArray<Integer> sumNums = new BArray(Integer.class, 41);

    public void ifbox() {
        int lasti = (int) Bootstrapper.resolve(IStdLib.class).eval(index);
        String lastfib = Bootstrapper.resolve(IStdLib.class).str(fibNums.getSArray()[(int) lasti]);
        sum(0, lasti);
        String strsum = Bootstrapper.resolve(IStdLib.class).str(sum);
        if ((text.equals("fib"))) {
            textLabel1.setText(lastfib);
        } else if ((text.equals("sum"))) {
            textLabel1.setText(strsum);
        } else {
            textLabel1.setText("error");
        }

    }

    public void fib(double start) {
        double i = start;
        fibNums.getSArray()[(int) 0] = 1;
        fibNums.getSArray()[(int) 1] = 1;
        while ((i < fibNums.getLength())) {
            fibNums.getSArray()[(int) i] = fibNums.getSArray()[(int) (i - 1)] + fibNums.getSArray()[(int) (i - 2)];
            i = i + 1;
        }
        ;
    }

    double sum = 0;

    public void sum(int first, int last) {
        sum = 0;
        for (int i = first; i <= last; i++) {
            sumNums.getSArray()[(int) i] = (int) Bootstrapper.resolve(IBarsicMath.class).random(0, 10);
            sum = sum + sumNums.getSArray()[(int) i];
        }
    }

    public void form1_button1_onClick() {
        text = textField1.getText();
        ifbox();
    }

    public void form1_button2_onClick() {
        index = textField2.getText();
    }

    ITextLabel textLabel1;
    IButton button1;
    IButton button2;
    ITextField textField1;
    ITextField textField2;
    IForm form1;

    void initForm() {
        textLabel1 = (ITextLabel) form1.getElementById("textLabel1");
        button1 = (IButton) form1.getElementById("button1");
        button2 = (IButton) form1.getElementById("button2");
        textField1 = (ITextField) form1.getElementById("textField1");
        textField2 = (ITextField) form1.getElementById("textField2");
    }

}



