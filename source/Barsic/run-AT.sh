#!/bin/bash

# Acceptance test:
# Given Barsic source code
# When generate java code and compile it
# Then no errors in build logs occurred

projects=(Forms SubWindow Android Graphics)

for project in "${projects[@]}"
do
    rm -rf Barsic.App.CodegenNew/src/barsic/app/*Module.java
    rm -rf Barsic.App.CodegenNew/src/barsic/app/*Form.java
    java -cp "Barsic.App/target/Barsic.App-compiler-1.0-SNAPSHOT.jar:Barsic.App/target/Barsic.App-1.0-SNAPSHOT.jar" barsic.JavaTranslator "Barsic.App/Samples/$project/Project.bpr" "Barsic.App.CodegenNew/src/barsic/app"

    find -name "*.java" | grep Barsic.App.CodegenNew > sources.txt

    CLASS_PATH=`find -name "*.jar" | grep ./target/.*jar | tr '\n' ':'`
    javac -cp $CLASS_PATH @sources.txt > build-$project.log 2>&1
    cat build-$project.log
done
#java -cp "out/production/Barsic.App.CodegenNew;Barsic.App/target/barsic.app-1.0-SNAPSHOT.jar;Barsic.App/target/barsic.app-compiler-1.0-SNAPSHOT.jar;Barsic.Core/target/barsic.core-1.0-SNAPSHOT.jar;Barsic.Lang/target/barsic.lang-1.0-SNAPSHOT.jar;Barsic.Lang/target/barsic.lang-compiler-1.0-SNAPSHOT.jar;Barsic.Math/target/barsic.math-1.0-SNAPSHOT.jar;Barsic.Math/target/barsic.math-compiler-1.0-SNAPSHOT.jar;Barsic.UI/target/barsic.ui-1.0-SNAPSHOT.jar" barsic.app.ModuleLoader
