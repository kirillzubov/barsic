package ru.spbu.barsicmodel;

import android.app.Activity;
import android.os.Bundle;

import ru.spbu.abarsic.Model;

public final class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        if (state == null) {
            Model model = new ModelCar();
            //Model model = new ModelPendulum();
            getFragmentManager().beginTransaction().add(android.R.id.content, model).commit();
        }
    }
}
