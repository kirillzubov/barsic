package com.barsic;


import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jetbrains.annotations.*;

import javax.swing.*;

public class BarsicFileType extends LanguageFileType {
    public static final BarsicFileType INSTANCE = new BarsicFileType();

    private BarsicFileType() {
        super(BarsicLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public String getName() {
        return "Barsic file";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Barsic language file";
    }

    @NotNull
    @Override
    public String getDefaultExtension() {
        return "brm";
    }

    @Nullable
    @Override
    public Icon getIcon() {
        return BarsicIcons.FILE;
    }
}