package com.barsic;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import com.barsic.psi.BarsicTypes;
import com.intellij.psi.TokenType;

%%

%class BarsicLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{  return;
%eof}

CRLF= \n|\r|\r\n

DIGIT=[0-9]
NUMBER=({DIGIT})+|({DIGIT})+"."({DIGIT})+
ATOMIC={NUMBER}|"true"|"false"

ESCAPE_SEQUENCE=\\[^\r\n]
STRING="'"([^\\\'\r\n]|{ESCAPE_SEQUENCE}|\\{CRLF})*("'"|\\)?


SEMICOLON=[;,]|CRLF
WHITE_SPACE=[\ \t\f]
PAREN=[\(\)\[\]]
BIN_OP=[/\*\+\-\.><\|] | "=>"
OPERATOR={BIN_OP}|{PAREN}
ID=[a-zA-Z0-9_]
COMMENT=("//")[^\r\n]*
SEPARATOR=[=]
KEYWORD="import"|"variables"|"begin"|"end"|"do"|"_do"|"for"|"procedure"|"case"|"_case"|"new"|"if"|"_if"|"else"|"of"
TYPE="Array"|"Form"|"_Form;"|"Number"|"String"|"Button"


%state WAITING_VALUE, COMMENT

%%


<YYINITIAL, WAITING_VALUE> {
    {ATOMIC}                                    { return BarsicTypes.NUMBER; }
    {KEYWORD}                                   { return BarsicTypes.KEYWORD; }
    {TYPE}                                      { return BarsicTypes.TYPE; }
    {STRING}                                    { return BarsicTypes.STRING; }
    {OPERATOR}+                                 { return TokenType.WHITE_SPACE; }
    {COMMENT}                                   { yybegin(COMMENT); return BarsicTypes.COMMENT; }
    {SEMICOLON}                                 { yybegin(YYINITIAL); return TokenType.WHITE_SPACE; }
}

<YYINITIAL> {
    {ID}+                                       { return BarsicTypes.ID; }
    {SEPARATOR}                                 { yybegin(WAITING_VALUE); return BarsicTypes.SEPARATOR; }
}

<WAITING_VALUE> {
    {ID}+                                       { return BarsicTypes.IDREF; }
}

({CRLF}|{WHITE_SPACE})+                         { yybegin(YYINITIAL); return TokenType.WHITE_SPACE; }

.                                               { return TokenType.BAD_CHARACTER; }
