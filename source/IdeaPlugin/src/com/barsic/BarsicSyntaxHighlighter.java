package com.barsic;


import com.barsic.psi.BarsicTypes;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class BarsicSyntaxHighlighter extends SyntaxHighlighterBase {
    public static final TextAttributesKey SEPARATOR =
            createTextAttributesKey("BARSIC_SEPARATOR", DefaultLanguageHighlighterColors.OPERATION_SIGN);
    public static final TextAttributesKey ID =
            createTextAttributesKey("BARSIC_ID", DefaultLanguageHighlighterColors.IDENTIFIER);

    public static final TextAttributesKey REF =
            createTextAttributesKey("BARSIC_REF", DefaultLanguageHighlighterColors.CLASS_REFERENCE);

    public static final TextAttributesKey STRING =
            createTextAttributesKey("BARSIC_VALUE", DefaultLanguageHighlighterColors.STRING);

    public static final TextAttributesKey NUMBER =
            createTextAttributesKey("BARSIC_NUMBER", DefaultLanguageHighlighterColors.NUMBER);

    public static final TextAttributesKey TYPE =
            createTextAttributesKey("BARSIC_TYPE", DefaultLanguageHighlighterColors.CLASS_NAME);

    public static final TextAttributesKey KEYWORD =
            createTextAttributesKey("BARSIC_KEYWORD", DefaultLanguageHighlighterColors.KEYWORD);

    public static final TextAttributesKey COMMENT =
            createTextAttributesKey("BARSIC_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);

    public static final TextAttributesKey BAD_CHARACTER =
            createTextAttributesKey("BARSIC_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);

    private static final TextAttributesKey[] BAD_CHAR_KEYS = new TextAttributesKey[]{BAD_CHARACTER};
    private static final TextAttributesKey[] SEPARATOR_KEYS = new TextAttributesKey[]{SEPARATOR};
    private static final TextAttributesKey[] ID_KEYS = new TextAttributesKey[]{ID};
    private static final TextAttributesKey[] REF_KEYS = new TextAttributesKey[]{REF};
    private static final TextAttributesKey[] VALUE_KEYS = new TextAttributesKey[]{STRING};
    private static final TextAttributesKey[] NUMBER_KEYS = new TextAttributesKey[]{NUMBER};
    private static final TextAttributesKey[] TYPE_KEYS = new TextAttributesKey[]{TYPE};
    private static final TextAttributesKey[] KEYWORD_KEYS = new TextAttributesKey[]{KEYWORD};
    private static final TextAttributesKey[] COMMENT_KEYS = new TextAttributesKey[]{COMMENT};
    private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];

    @NotNull
    @Override
    public Lexer getHighlightingLexer() {
        return new BarsicLexerAdapter();
    }

    @NotNull
    @Override
    public TextAttributesKey[] getTokenHighlights(IElementType tokenType) {
        if (tokenType.equals(BarsicTypes.SEPARATOR)) {
            return SEPARATOR_KEYS;
        } else if (tokenType.equals(BarsicTypes.ID)) {
            return ID_KEYS;
        } else if (tokenType.equals(BarsicTypes.IDREF)) {
            return REF_KEYS;
        } else if (tokenType.equals(BarsicTypes.STRING)) {
            return VALUE_KEYS;
        } else if (tokenType.equals(BarsicTypes.NUMBER)) {
            return NUMBER_KEYS;
        } else if (tokenType.equals(BarsicTypes.COMMENT)) {
            return COMMENT_KEYS;
        } else if (tokenType.equals(BarsicTypes.TYPE)) {
            return TYPE_KEYS;
        } else if (tokenType.equals(BarsicTypes.KEYWORD)) {
            return KEYWORD_KEYS;
        } else if (tokenType.equals(TokenType.BAD_CHARACTER)) {
            return BAD_CHAR_KEYS;
        } else {
            return EMPTY_KEYS;
        }
    }
}
