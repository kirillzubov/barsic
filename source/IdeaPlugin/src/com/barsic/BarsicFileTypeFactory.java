package com.barsic;

import com.intellij.openapi.fileTypes.*;
import org.jetbrains.annotations.NotNull;

public class BarsicFileTypeFactory extends FileTypeFactory {
    @Override
    public void createFileTypes(@NotNull FileTypeConsumer fileTypeConsumer) {
        fileTypeConsumer.consume(BarsicFileType.INSTANCE, "brm");
        fileTypeConsumer.consume(BarsicFileType.INSTANCE, "bfm");
    }
}