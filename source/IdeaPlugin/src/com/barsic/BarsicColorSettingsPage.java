package com.barsic;


import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Map;

public class BarsicColorSettingsPage implements ColorSettingsPage {
    private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
            new AttributesDescriptor("Keyword", BarsicSyntaxHighlighter.KEYWORD),
            new AttributesDescriptor("Identifier", BarsicSyntaxHighlighter.ID),
            new AttributesDescriptor("Separator", BarsicSyntaxHighlighter.SEPARATOR),
            new AttributesDescriptor("Number", BarsicSyntaxHighlighter.NUMBER),
            new AttributesDescriptor("String", BarsicSyntaxHighlighter.STRING),
            new AttributesDescriptor("Type", BarsicSyntaxHighlighter.TYPE),
            new AttributesDescriptor("Reference", BarsicSyntaxHighlighter.REF),
    };

    @Nullable
    @Override
    public Icon getIcon() {
        return BarsicIcons.FILE;
    }

    @NotNull
    @Override
    public SyntaxHighlighter getHighlighter() {
        return new BarsicSyntaxHighlighter();
    }

    @NotNull
    @Override
    public String getDemoText() {
        return "procedure plotScale ()\n" +
                "import\n" +
                "  markScheme\n" +
                "variables\n" +
                " i=0\n" +
                " z=0\n" +
                " len=2\n" +
                "begin\n" +
                " graph.pen.width=widthMark;\n" +
                " for i=0..length\n" +
                " do\n" +
                "     z=floor(i/Array(markScheme).length);\n" +
                "     len=markScheme[Array(markScheme).firstIndex+i-z*Array(markScheme).length];\n" +
                "     graph.line((left_x+i*(widthPt),top_y),(left_x+i*(widthPt),top_y-len));\n" +
                " _do;\n" +
                "end;\n" +
                "\n" +
                "//------------------------------------------------------------------------------\n" +
                "form1=new\n" +
                " Form\n" +
                "  title='Components task',\n" +
                "  left= 221.866666666667,\n" +
                "  top= 135.111111111111,\n" +
                "  clientWidth= 355.555555555556,\n" +
                "  clientHeight= 261.688888888889,\n" +
                "  hint='',\n" +
                "  font\n" +
                "    face='Courier New',\n" +
                "    height= 15,\n" +
                "    isBold=false,\n" +
                "    isItalic=false,\n" +
                "    isUnderline=false,\n" +
                "    isStrikeout=false,\n" +
                "    script=FontScript.CYRILLIC,\n" +
                "    color=clWindowText\n" +
                "  _font,\n" +
                "  isBorderCloseIcon=true,\n" +
                "  isResizeable=false,\n" +
                "  isEnabledOnCalc=false\n" +
                " _Form;";
    }

    @Nullable
    @Override
    public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
        return null;
    }

    @NotNull
    @Override
    public AttributesDescriptor[] getAttributeDescriptors() {
        return DESCRIPTORS;
    }

    @NotNull
    @Override
    public ColorDescriptor[] getColorDescriptors() {
        return ColorDescriptor.EMPTY_ARRAY;
    }

    @NotNull
    @Override
    public String getDisplayName() {
        return "Barsic";
    }
}