package com.barsic;

import com.intellij.lexer.FlexAdapter;
import java.io.Reader;

public class BarsicLexerAdapter extends FlexAdapter {
    public BarsicLexerAdapter() {
        super(new BarsicLexer((Reader) null));
    }
}
