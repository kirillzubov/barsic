package com.barsic;

import com.intellij.lang.Language;

public class BarsicLanguage extends Language {
    public static final BarsicLanguage INSTANCE = new BarsicLanguage();

    private BarsicLanguage() {
        super("Barsic");
    }
}