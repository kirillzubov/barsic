package com.barsic.psi;

import com.intellij.psi.tree.IElementType;
import com.barsic.BarsicLanguage;
import org.jetbrains.annotations.*;

public class BarsicTokenType extends IElementType {
    public BarsicTokenType(@NotNull @NonNls String debugName) {
        super(debugName, BarsicLanguage.INSTANCE);
    }

    @Override
    public String toString() {
        return "BarsicTokenType." + super.toString();
    }
}