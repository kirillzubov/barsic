package com.barsic.psi;

import com.barsic.BarsicFileType;
import com.barsic.BarsicLanguage;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import com.barsic.*;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class BarsicFile extends PsiFileBase {
    public BarsicFile(@NotNull FileViewProvider viewProvider) {
        super(viewProvider, BarsicLanguage.INSTANCE);
    }

    @NotNull
    @Override
    public FileType getFileType() {
        return BarsicFileType.INSTANCE;
    }

    @Override
    public String toString() {
        return "Barsic File";
    }

    @Override
    public Icon getIcon(int flags) {
        return super.getIcon(flags);
    }
}