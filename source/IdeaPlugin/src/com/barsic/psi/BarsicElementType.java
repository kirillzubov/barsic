package com.barsic.psi;

import com.intellij.psi.tree.IElementType;
import com.barsic.BarsicLanguage;
import org.jetbrains.annotations.*;

public class BarsicElementType extends IElementType {
    public BarsicElementType(@NotNull @NonNls String debugName) {
        super(debugName, BarsicLanguage.INSTANCE);
    }
}