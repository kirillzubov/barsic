package ru.spbu.barsic;

//todo:rename to dropndown?
public interface IFallingList extends IElement {
    void setIsParentBackColor(boolean value);

    void setIsParentFont(boolean value);

    void setAssociatedVariable(String value);

    void setIsEnabledOnCalc(boolean value);
}