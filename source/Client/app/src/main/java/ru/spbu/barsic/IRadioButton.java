package ru.spbu.barsic;

public interface IRadioButton extends IElement {
    void setIsParentBackColor(boolean value);

    void setText(String value);

    void setIsChecked(boolean value);

    void setIsParentFont(boolean value);

    void setIsEnabledOnCalc(boolean value);
}