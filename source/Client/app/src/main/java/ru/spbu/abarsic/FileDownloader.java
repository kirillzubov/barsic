package ru.spbu.abarsic;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 *Created by Владислав on 25.07.2015.
 *Загрузка модели с сервера
 */

public class FileDownloader extends AsyncTask<String,Integer,String> {

    /**
     * @param strings : modelUrl, directoryPath
     * @return filePath
     */
    @Override
    protected String doInBackground(String... strings) {
        if (strings.length!=2){
            return null;
        }
        this.publishProgress(0);

        String modelUrl = strings[0];
        String directoryPath = strings[1];

        String[] urlPart = modelUrl.split("/");
        String filename=urlPart[urlPart.length-1];

        File file = new File(directoryPath);
        if (!file.exists()){
            file.mkdirs();
        }

        int count;

        try{
            URL url = new URL(modelUrl);
            URLConnection connection = url.openConnection();
            connection.connect();

            int lenghtOfFile = connection.getContentLength();

            String filePath = directoryPath + "/" + filename;

            InputStream inputStream = new BufferedInputStream(url.openStream());
            OutputStream outputStream = new FileOutputStream(filePath);

            byte data[] = new byte[1024];

            long total = 0;

            while((count = inputStream.read(data)) != -1){
                total+=count;
                this.publishProgress((int) (total*100/lenghtOfFile));
                outputStream.write(data,0,count);
            }

            outputStream.flush();
            outputStream.close();
            inputStream.close();

            return filePath;

        }catch (Exception e) {
            Log.e("soInBackGround",e.toString());
        }

        this.publishProgress(100);

        return  null;
    }

}
