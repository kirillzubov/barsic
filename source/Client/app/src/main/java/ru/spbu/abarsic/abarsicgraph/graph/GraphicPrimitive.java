package ru.spbu.abarsic.abarsicgraph.graph;

/**
 * Created by Мартынюк on 07.12.2014.
 */

import android.graphics.Paint;

public abstract class GraphicPrimitive extends GraphicObject {

    int fillColor;
    int strokeColor;

    protected GraphicPrimitive(double xCenter, double yCenter, int fillColor, int strokeColor) {
        super(xCenter, yCenter);
        this.fillColor = fillColor;
        this.strokeColor = strokeColor;
    }

    protected void setFillPaint(Paint p) {
        p.setStyle(Paint.Style.FILL);
        p.setColor(fillColor);
    }

    protected void setStrokePaint(Paint p) {
        p.setStyle(Paint.Style.STROKE);
        p.setColor(strokeColor);
    }

}
