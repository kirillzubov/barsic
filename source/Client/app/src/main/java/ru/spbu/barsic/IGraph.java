package ru.spbu.barsic;

public interface IGraph {
    
    /*
    "arrow"
    "canvas.clear"
    "floodFill"
    "group"
    "objects"
    */
    
    public IBrush brush();

    public IPen pen();
    
    public IFont font();
    
    public double[] bounds();
    
    public void setBounds(double xLeft, double xRight, double yBottom, double yTop);
    
    public void arc(double xCenter, double yCenter, double width, double height,
            double startAngle, double endAngle);
    
    public void chord(double xCenter, double yCenter, double width, double height,
            double startAngle, double endAngle);
    
    public void circle(double xCenter, double yCenter, double radius);
    
    public void ellipse(double xCenter, double yCenter, double width, double height);
    
    public void line(double x1, double y1, double x2, double y2);
    
    public void picture(String file, double xLeft, double yTop, double xRight, double yBottom);
    
    public void pie(double xCenter, double yCenter, double width, double height,
            double startAngle, double endAngle);
    
    public void polygon(double[] xyPoints);
    
    public void polyLine(double[] xyPoints);
    
    public void rectangle(double xCenter, double yCenter, double width, double height);
    
    public void roundRectangle(double xCenter, double yCenter, double width, double height,
            double radiusX, double radiusY);
    
    public void text(String s, double xLeft, double yTop);
    
    public void update();
    
}
