package ru.spbu.abarsic.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ru.spbu.abarsic.R;


/**
 * Created by Владислав on 08.05.2015.
 * Changed on 30.07.2015.
 */
public class FragmentSettings extends Fragment {

    private static final String ARG_URL = "URL";
    private String url="";


    private static Button buttonSave,buttonDefault;
    private static EditText editText;

    private static FragmentSettings instance = null;

    public static FragmentSettings getInstance(String url){
        if (instance == null){
            instance = new FragmentSettings();
            Bundle args = new Bundle();
            args.putString(ARG_URL,url);
            instance.setArguments(args);
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            url = getArguments().getString(ARG_URL);
        }
    }

    public interface OnSettingsChangeListener{
        public void onSettingsChange(String s);
    }

    OnSettingsChangeListener settingsChangeListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            settingsChangeListener = (OnSettingsChangeListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings,container,false);


        buttonSave = (Button) view.findViewById(R.id.buttonSave);
        buttonDefault = (Button) view.findViewById(R.id.buttonDefault);
        editText = (EditText) view.findViewById(R.id.editText);
//TODO ListView: item (url of server) with abibility to add it to local lib. OnItemCLickLstener - editText.setText(item.text.toSring());
//TODO Remove debug lines
        Button button = (Button) view.findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText("http://distolymp2.spbu.ru/www/olymp/");
            }
        });
        button = (Button) view.findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText("http://distolymp2.spbu.ru/distolymp/new/dev/");
            }
        });
        button = (Button) view.findViewById(R.id.button3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText("http://192.168.0.101/new/");
            }
        });
        button = (Button) view.findViewById(R.id.button4);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText("http://192.168.0.101/dev/");
            }
        });
//End debug lines

        if (url != ""){
            editText.setText(url);
        }else{
            url = getString(R.string.home_url_default);
            editText.setText(url);
        }

        buttonSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                url = editText.getText().toString();
                settingsChangeListener.onSettingsChange(url);
                Toast.makeText(getActivity(), "Настройки сохранены! ", Toast.LENGTH_SHORT).show();
            }
        });

        buttonDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                url = getString(R.string.home_url_default);
                editText.setText(url);
                settingsChangeListener.onSettingsChange(url);
                Toast.makeText(getActivity(), "Адрес установлен по умолчанию!", Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }


}
