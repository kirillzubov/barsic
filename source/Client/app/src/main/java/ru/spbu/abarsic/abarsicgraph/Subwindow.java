package ru.spbu.abarsic.abarsicgraph;

import static ru.spbu.abarsic.abarsicgraph.CartTransform.*;

import ru.spbu.barsic.ISubwindow;

import static java.lang.Math.round;

/**
 * Created by Мартынюк on 09.09.2015.
 */

public final class Subwindow implements ISubwindow {

    SubwindowView mSubwindowView;

    @Override
    public double xMin() {
        return mSubwindowView.mXMin;
    }

    @Override
    public double xMax() {
        return mSubwindowView.mXMax;
    }

    @Override
    public double yMin() {
        return mSubwindowView.mYMin;
    }

    @Override
    public double yMax() {
        return mSubwindowView.mYMax;
    }

    @Override
    public int pixels(char coordinate, double value) {
        switch (coordinate) {
            case 'x':
                return (int)round(mSubwindowView.mCoeffs[AX]*value + mSubwindowView.mCoeffs[BX]);
            case 'y':
                return (int)round(mSubwindowView.mCoeffs[AY]*value + mSubwindowView.mCoeffs[BY]);
            default:
                return 0;
        }
    }

    @Override
    public double x(int pixels) {
        return (pixels - mSubwindowView.mCoeffs[BX])/ mSubwindowView.mCoeffs[AX];
    }

    @Override
    public double y(int pixels) {
        return (pixels - mSubwindowView.mCoeffs[BY])/ mSubwindowView.mCoeffs[AY];
    }

    @Override
    public void setClipping(String value) {

    }

    @Override
    public void setContext(String value) {

    }

    @Override
    public void setIsParentBackColor(boolean value) {

    }

    @Override
    public void clear() {
        //mPlot.clear(); //TODO
        mSubwindowView.mGraph.canvasClear();
    }

    @Override
    public void update() {
        mSubwindowView.mUpdating = true;
        //mPlot.calculateParams(); //TODO
        mSubwindowView.invalidate();
    }

}
