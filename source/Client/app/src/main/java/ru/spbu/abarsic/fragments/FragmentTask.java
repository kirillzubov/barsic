package ru.spbu.abarsic.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ru.spbu.abarsic.R;


/**
 * Created by Владислав on 27.05.2015.
 */
public class FragmentTask extends Fragment {


    private static final String URL_OF_HOME_PAGE = "URL_OF_HOME_PAGE";
    private String urlOfHomePage = "http://distolymp.spbu.ru/phys/olymp/";

    private WebView webView;



    private static FragmentTask instance = null;

    public static FragmentTask getInstance(String urlOfHomePage){
        if (instance == null){
            instance = new FragmentTask();
            Bundle args = new Bundle();
            args.putString(URL_OF_HOME_PAGE, urlOfHomePage);
            instance.setArguments(args);
        }
        return  instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments()!=null){
            urlOfHomePage = getArguments().getString(URL_OF_HOME_PAGE);
        }
        super.onCreate(savedInstanceState);
    }

    public FragmentTask() {
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_task,container,false);

        webView = (WebView) view.findViewById(R.id.webViewTask);

        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setInitialScale(1);

        webView.loadUrl(urlOfHomePage + "p-model/text/");


        return view;
    }



}
