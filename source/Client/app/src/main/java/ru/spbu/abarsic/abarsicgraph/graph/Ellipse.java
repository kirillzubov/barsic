package ru.spbu.abarsic.abarsicgraph.graph;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import static ru.spbu.abarsic.abarsicgraph.CartTransform.*;

/**
 * Created by Мартынюк on 17.04.2015.
 */
public final class Ellipse extends GraphicPrimitive {

    double w;
    double h;

    private RectF boundRect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);

    public Ellipse(double xCenter, double yCenter, double width, double height, int fillColor, int strokeColor) {
        super(xCenter, yCenter, fillColor, strokeColor);
        if ((width < 0.0) || (height < 0.0)) {
            throw new IllegalArgumentException(String.format("Width and height must be" +
                    "larger or equal to zero: w=%f, h=%f", width, height));
        }
        this.w = width;
        this.h = height;
    }

    @Override
    public void draw(Canvas c, Paint p, double[] coeffs) {
        boundRect.set((float)(coeffs[AX]*(x0-w*0.5)+coeffs[BX]), (float)(coeffs[AY]*(y0+h*0.5)+coeffs[BY]),
                      (float)(coeffs[AX]*(x0+w*0.5)+coeffs[BX]), (float)(coeffs[AY]*(y0-h*0.5)+coeffs[BY]));
        setFillPaint(p);
        c.drawOval(boundRect, p);
        setStrokePaint(p);
        c.drawOval(boundRect, p);
    }
}
