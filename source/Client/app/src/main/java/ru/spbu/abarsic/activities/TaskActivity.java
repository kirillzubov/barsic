package ru.spbu.abarsic.activities;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import ru.spbu.abarsic.IModel;
import ru.spbu.abarsic.ModelDexClassLoader;
import ru.spbu.abarsic.R;
import ru.spbu.abarsic.fragments.FragmentReport;
import ru.spbu.abarsic.fragments.FragmentTask;

public class TaskActivity extends AppCompatActivity {


    private IModel iModel;

    private final String URL_OF_HOME_PAGE = "URL_OF_HOME_PAGE";
    private String urlOfHomePage;

    private Drawer drawer;

    private Toolbar toolbar;

    private  Menu menu = null;
    private final int groupId=1;
    private final int action_close = 1;

    private FragmentManager fragmentManager;

    private Fragment fragmentModel;
    private FragmentReport fragmentReport;
    private FragmentTask fragmentTask;


    private final String CLASS_NAME = "CLASS_NAME";
    private String className;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        toolbar = (Toolbar) findViewById(R.id.toolbartaskactivity);
        setSupportActionBar(toolbar);

        if (getIntent().getExtras().getString(URL_OF_HOME_PAGE)!=null)
        {
            urlOfHomePage = getIntent().getExtras().getString(URL_OF_HOME_PAGE);
        } else {
            urlOfHomePage = getString(R.string.home_url_default);
        }

        fragmentManager = getFragmentManager();

        loadModel();

        fragmentReport = FragmentReport.getInstance(urlOfHomePage);
        fragmentTask = FragmentTask.getInstance(urlOfHomePage);


        fragmentManager
                .beginTransaction()
                .add(R.id.taskactivity_fragment, fragmentTask)
                .hide(fragmentTask)
                .add(R.id.taskactivity_fragment, fragmentReport)
                .hide(fragmentReport)
                .commit();

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withHeader(R.layout.drawer_header)
                .withDisplayBelowToolbar(false)
                .withDisplayBelowStatusBar(true)
                .withTranslucentActionBarCompatibility(false)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_model).withIcon(FontAwesome.Icon.faw_cube).withIdentifier(1),
                        new PrimaryDrawerItem().withName(R.string.drawer_task).withIcon(FontAwesome.Icon.faw_question).withIdentifier(2),
                        new PrimaryDrawerItem().withName(R.string.drawer_report).withIcon(FontAwesome.Icon.faw_send).withIdentifier(3),
                        new DividerDrawerItem(),
                        new PrimaryDrawerItem().withName(R.string.action_close_taskactivity).withIcon(FontAwesome.Icon.faw_times_circle).withIdentifier(4)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                        int id = iDrawerItem.getIdentifier();

                        toolbar.setTitle(getPageTitle(id));

                        switch (id){
                            case 1:
                                  fragmentManager.beginTransaction()
                                        .hide(fragmentTask)
                                        .hide(fragmentReport)
                                        .show(fragmentModel)
                                        .commit();
                                break;
                            case 2:
                                fragmentManager.beginTransaction()
                                        .show(fragmentTask)
                                        .hide(fragmentReport)
                                        .hide(fragmentModel)
                                        .commit();
                                break;
                            case 3:
                                fragmentManager.beginTransaction()
                                        .show(fragmentReport)
                                        .hide(fragmentTask)
                                        .hide(fragmentModel)
                                        .commit();
                                break;
                            case 4:
                                onModelClose();
                                break;

                        }
                        return false;
                    }
                })

                .build();
    }

    private void loadModel() {
        if (getIntent().getExtras().getString(CLASS_NAME) !=null){

            className = this.getIntent().getStringExtra(CLASS_NAME);

            try {
                Class<Fragment> fragmentModelImp;
                fragmentModelImp = (Class<Fragment>) ModelDexClassLoader.getInstance().loadClass(className);
                fragmentModel = fragmentModelImp.newInstance();

                Bundle args = new Bundle();
                fragmentModel.setArguments(args);
                fragmentManager
                        .beginTransaction()
                        .add(R.id.taskactivity_dynamicfragment, fragmentModel)
                        .commit();

                iModel = (IModel) fragmentModel;
                Log.d("TaskActUrl=",urlOfHomePage);
                iModel.setServerUrl(urlOfHomePage);

            }catch (Exception e) {
                Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                TaskActivity.this.finish();
            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.add(groupId,action_close,1,R.string.action_close_taskactivity).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == action_close) {
            onModelClose();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        if (drawer != null && drawer.isDrawerOpen()){
            drawer.closeDrawer();
            return;
        }
        onModelClose();
    }

    public void onModelClose(){
        AlertDialog alertDialog =
                new AlertDialog.Builder(TaskActivity.this)
                .setTitle(R.string.alert_ontaskclose_title)
                .setMessage(R.string.alert_ontaskclose_message)
                .setCancelable(true)
                .setNegativeButton(R.string.alert_ontaskclose_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.alert_ontaskclose_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .create();
        alertDialog.show();

    }

    public CharSequence getPageTitle(int id) {
        switch (id) {
            case 1:
                return getString(R.string.section_model);
            case 2:
                return getString(R.string.section_task);
            case 3:
                return getString(R.string.section_report);
        }
        return "";
    }

    public ClassLoader getClassLoader() {
        return ModelDexClassLoader.getInstance();
    }


}
