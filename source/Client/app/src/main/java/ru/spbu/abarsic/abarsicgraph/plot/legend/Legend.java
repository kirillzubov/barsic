/*
 * Legend.java
 */
package ru.spbu.abarsic.abarsicgraph.plot.legend;

/**
 * Porting to Android performed by Martynyuk
 */

import ru.spbu.abarsic.abarsicgraph.plot.Margin;
import ru.spbu.abarsic.abarsicgraph.plot.axis.AxisValue;
import ru.spbu.abarsic.abarsicgraph.plot.axis.AxisValue.AlignH;
import ru.spbu.abarsic.abarsicgraph.plot.axis.AxisValue.AlignW;
import ru.spbu.abarsic.abarsicgraph.plot.curve.CurveStyle;

import android.graphics.Color;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Shader.TileMode;
import android.graphics.LinearGradient;
import ru.spbu.abarsic.abarsicgraph.awtandroid.BasicStroke;
import ru.spbu.abarsic.abarsicgraph.awtandroid.Font;

import ru.spbu.abarsic.abarsicgraph.awtandroid.geom.Line2D;
import ru.spbu.abarsic.abarsicgraph.awtandroid.geom.Point2D;
import ru.spbu.abarsic.abarsicgraph.awtandroid.geom.Rectangle2D;
import ru.spbu.abarsic.abarsicgraph.awtandroid.geom.Ellipse2D;

/**
 *
 * @author Max
 */
public class Legend {

    private Margin padding = new Margin(10, 5, 10, 5);

    /**
     * sets 
     * @param padding
     * (Anita's correction)
     */
    public void setPadding(Margin padding) {
        this.padding = padding;
    }

    /**
     * 
     * @return
     */
    public Margin getPadding() {
        return padding;
    }

    private Point2D pos;

    /**
     * 
     * @return
     */
    public Point2D getPos() {
        return pos;
    }

    /**
     * 
     * @param pos
     * (Anita's correction)
     */
    public void setPos(Point2D pos) {
        this.pos = pos;
    }

    /**
     * 
     */
    protected AxisValue.AlignW alignW = AxisValue.AlignW.Right;

    /**
     * 
     * @return
     */
    public AxisValue.AlignW getAlignW() {
        return alignW;
    }

    /**
     * 
     * @param alignW
     * (Anita's correction)
     */
    public void setAlignW(AxisValue.AlignW alignW) {
        this.alignW = alignW;
    }

    /**
     * 
     */
    protected AxisValue.AlignH alignH = AxisValue.AlignH.Bottom;

    /**
     * 
     * @return
     */
    public AxisValue.AlignH getAlignH() {
        return alignH;
    }

    /**
     * 
     * @param alignH 
     */
    public void setAlignH(AxisValue.AlignH alignH) {
        this.alignH = alignH;
    }

    /**
     * width of picrogram of line with point
     */
    private double pictogramWidth=40;

    /**
     * 
     * @param pictogramWidth 
     */
    public void setPictogramWidth(double pictogramWidth) {
        this.pictogramWidth = pictogramWidth;
    }

    private Font font = new Font(Font.SANS_SERIF, Font.PLAIN, 12);

    /**
     * 
     * @return
     */
    public Font getfont() {
        return this.font;
    }

    /**
     * sets font
     * @param font
     * (Anita's correction)
     */
    public void setFont(Font font) {
        this.font = font;
    }
    /*
     * gradient color1
     */
    private int color1 = Color.WHITE;

    /**
     * 
     * @return
     */
    public int getColor1() {
        return color1;
    }

    /**
     * 
     * @param color1
     * (Anita's correction)
     */
    public void setColor1(int color1) {
        this.color1 = color1;
    }

    /*
     * gradient color2
     */
    private int color2 = Color.rgb(200, 200, 230);

    /**
     * 
     * @return
     */
    public int getColor2() {
        return color2;
    }

    /**
     * 
     * @param color2
     * (Anita's correction)
     */
    public void setColor2(int color2) {
        this.color2 = color2;
    }

    /**
     * sets identical color to color1 and color2
     * @param color
     */
    public void setColor(int color) {
        this.color1 = color;
        this.color2 = color;
    }

    /**
     * sets different color to color1 and color2
     * @param color1
     * @param color2
     */
    public void setColor(int color1, int color2) {
        this.color1 = color1;
        this.color2 = color2;
    }

    /**
     * 
     */
    public enum GradientDirection {

        /**
         * 
         */
        Horizontal,
        /**
         * 
         */
        Vertical,
        /**
         * 
         */
        Sloped,
        /**
         * 
         */
        SlopedAlt
    }

    GradientDirection gradientDirection = GradientDirection.Sloped;

    /**
     * sets gradient direction
     * @param gradientDirection
     * (Anita's correction)
     */
    public void setGradientDirection(GradientDirection gradientDirection) {
        this.gradientDirection = gradientDirection;
    }

    /**
     *returns Gradient Direction
     * @return
     */
    public GradientDirection getGradientDirection() {
        return gradientDirection;
    }

    private LinearGradient getGradient(float width, float height) {
        switch (gradientDirection) {
            case Sloped:
                return new LinearGradient(0, 0, width, height, color1, color2, TileMode.CLAMP);
            case SlopedAlt:
                return new LinearGradient(width, 0, 0, height, color1, color2, TileMode.CLAMP);
            case Horizontal:
                return new LinearGradient(0, 0, width, 0, color1, color2, TileMode.CLAMP);
            case Vertical:
                return new LinearGradient(0, 0, 0, height, color1, color2, TileMode.CLAMP);
        }
        return null;
    }

    java.util.ArrayList<LegendItem> items = new java.util.ArrayList<LegendItem>();

    /**
     * 
     * @return
     */
    public int getItemsCount()
    {
        return items.size();
    }

    /**
     * 
     */
    public void clear() {
        items.clear();
    }

    /**
     * 
     * @param curve
     * @param title
     */
    public void addCurve(CurveStyle curve, String title) {
        items.add(new LegendItem(curve, title));
    }

    /**
     * @param g
     * @return rectangle that have width with
     */
    private Rectangle2D getTextBounds(Paint p) {
        double xMax = 0;
        double yMax = 0;

        int size = items.size();

        for (int i = 0; i < size; i++) {
            LegendItem item = items.get(i);
            Rectangle2D bounds = new Rectangle2D(0, 0, p.measureText(item.getTitle()), p.getTextSize());
            //max=java.lang.Math.max(glyphVector.getVisualBounds().getBounds2D().getWidth(),max);
            xMax = java.lang.Math.max(bounds.getWidth(), xMax);
            yMax = java.lang.Math.max(bounds.getHeight(), yMax);
        }

        return new Rectangle2D(0, 0, xMax, yMax);
    }

    private void setTestData() {
        clear();
        CurveStyle style = new CurveStyle();
        style.setLineColor(Color.RED);
        //style.setLineWidth(5.0);
        //style.setPointsSize(7.0);
        style.setLineStyle(CurveStyle.LineStyle.SOLID);
        style.setPointsColor(Color.BLUE);
        addCurve(style, "sin(x)/x");
        style = new CurveStyle();
        style.setLineColor(Color.BLUE);
        style.setLineWidth(2.0);
        style.setPointStyle(CurveStyle.PointStyle.RECTANGLE);
        style.setPointsSize(5.0);
        addCurve(style, "exp(x)");
        /*style=new CurveStyle();
        style.setLineColor(Color.GRAY);
        style.setLineStyle(CurveStyle.LineStyle.DASHDOT);
        style.setLineWidth(1.0);
        style.setPointStyle(CurveStyle.PointStyle.RECTANGLE);
        addCurve(style, "testing");
        //addCurve(new CurveStyle(), "very looooooooooong title");
        //addCurve(new CurveStyle(), "very looooooooooong title");
        style=new CurveStyle();
        style.setLineColor(Color.cyan);
        style.setPointsSize(4.0);
        style.setLineWidth(1.5);
        style.setPointsColor(Color.BLACK);
        addCurve(style, "item 1");
        addCurve(new CurveStyle(), "item 2");*/
    }

    private boolean transparentPane=false;
    /**
     * 
     * @param isTransparentPane
     */
    public void setTransparentField(boolean isTransparentPane) {
        color1 = 0;
        color2 = 0;
        this.transparentPane=isTransparentPane;
    }

    private boolean visible=true;

    /**
     * 
     * @param visible
     * (Anita's correction)
     */
    public void setVisible(boolean visible) {
        this.visible=visible;
    }

    private void drawDiagram(Canvas g, Paint p, CurveStyle Style, double width, double x, double y) {
        BasicStroke save = BasicStroke.get(p);

        p.setColor(Style.getLineColor());

        switch (Style.getLineStyle()) {
            case SOLID:
                new BasicStroke((float) Style.getLineWidth()).set(p);
                new Line2D(x + 0.5 * Style.getLineWidth(), y, x + width - 0.5 * Style.getLineWidth(), y).draw(g, p);
                break;
            case DASH:
                float dash[] = {10.0f};
                new BasicStroke((float) Style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f).set(p);
                new Line2D(x, y, x + width, y).draw(g, p);
                break;
            case DASHDOT:
                float dashdot[] = {10.0f, 9.0f, 2.0f, 9.0f};
                new BasicStroke((float) Style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dashdot, 0.0f).set(p);
                new Line2D(x, y, x + width, y).draw(g, p);
                break;
            case DOT:
                float dot[] = {2.0f};
                new BasicStroke((float) Style.getLineWidth(), BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER, 10.0f, dot, 0.0f).set(p);
                new Line2D(x, y, x + width, y).draw(g, p);
                break;
        }


        //g.draw(new Line2D.Double(x+0.5*Style.getLineWidth(),y,x+width-0.5*Style.getLineWidth(),y));
        //g.draw(new Line2D.Double(x,y,x+width,y));

        p.setColor(Style.getPointsColor());
        double size = Style.getPointsSize();

        switch (Style.getPointStyle()) {
            case CIRCLE:
                new Ellipse2D(
                        x + 0.5 * width - size,
                        y - size,
                        2 * size, 2 * size).draw(g, p); //fill

                break;

            case RECTANGLE:
                new Rectangle2D(
                        x + 0.5 * width - size,
                        y - size,
                        2 * size, 2 * size).draw(g, p); //fill
                break;
        }


        save.set(p);
    }

    private void fillPane(Canvas g, Paint p, float width, float height)
    {
        if ((color1 != 0) && (color2 != 0)) {
            if (color1 != color2) {
                p.setShader(getGradient(width, height));
            } else {
                p.setShader(null);
                p.setColor(color1);
            }
            if (!transparentPane)
                new Rectangle2D(0, 0, width, height).draw(g, p); //fill
        }

        p.setShader(null);
        p.setStyle(Style.STROKE);

        p.setColor(Color.BLACK);

        new Rectangle2D(0, 0, width, height).draw(g, p);        
    }
    
    /**
     * 
     * @param g
     */
    public void draw(Canvas g, Paint p) {
        if(!visible)return;
        g.save();

        font.set(p);

        //setTestData();
        int itemsCount = items.size();

        double xGap = 5;
        double yGap = 5;
       
        Rectangle2D textBounds = getTextBounds(p);

        double yStep = 2 * yGap + textBounds.getHeight();

        double width = pictogramWidth + xGap + textBounds.getWidth() + padding.getLeft() + padding.getRight();
        double height = padding.getTop() + padding.getBottom() + yStep * itemsCount;


        double splitX = 0;
        double splitY = 0;

        switch (alignW) {
            case Right:
                splitX = 0;
                break;
            case Center:
                splitX = -0.5 * width;
                break;
            case Left:
                splitX = -width;
                break;
        }


        switch (alignH) {
            case Bottom:
                splitY = 0;
                break;
            case Center:
                splitY = -0.5 * height;
                break;
            case Top:
                splitY = -height;
                break;
        }

        g.translate(pos.getX() + (float)splitX, pos.getY() + (float)splitY);

        //int startColor = Color.WHITE;
        //int endColor = Color.rgb(200, 200, 230);

        //if ((startColor != null) && (endColor != null)) {
        //if (startColor != endColor) {
        // g.setPaint(new java.awt.GradientPaint(0, 0, startColor, width, height, endColor));
            /*} else {
        g.setPaint(null);
        g.setColor(startColor);
        }*/
        // g.fillRect(margin.getLeft(), margin.getTop(), width - margin.getLeft() - margin.getRight(),
        //        height - margin.getTop() - margin.getBottom());
        //
        //}
        // g.setPaint(null);

        //g.setColor(Color.yellow);
        //g.setColor(Color.WHITE);


        /*.setPaint(new java.awt.GradientPaint(0, 0, startColor, (float) width, (float) height, endColor));
        g.fill(new Rectangle2D.Double(0, 0, width, height));
        g.setPaint(null);


        g.setColor(Color.black);
        g.draw(new Rectangle2D.Double(0, 0, width, height));*/

        fillPane(g, p, (float)width, (float)height);
        AxisValue val = new AxisValue();
        val.alignH = AlignH.Center;
        val.alignW = AlignW.Center;
        val.setFont(font);

        for (int i = 0; i < itemsCount; i++) {
            p.setColor(Color.BLACK);
            double x = padding.getLeft() + pictogramWidth + 0.5 * textBounds.getWidth() + xGap;
            double y = padding.getTop() + (i + 0.5) * yStep;

            val.setValue(items.get(i).getTitle());
            val.draw(g, p, new Point2D(x, y));

            /*g.setColor(Color.magenta);
            g.draw(new Rectangle2D.Double(padding.getLeft(),padding.getTop()+(i)*yStep,pictogramWidth,yStep));
            g.setColor(Color.black);
            g.draw(new Line2D.Double(0, padding.getTop()+(i+1)*yStep, width, padding.getTop()+(i+1)*yStep));
            g.setColor(Color.red);*/
            // g.draw(new Line2D.Double(0, padding.getTop()+(i+0.5)*yStep, width, padding.getTop()+(i+0.5)*yStep));

            /*g.setColor(Color.BLUE);
            g.draw(new Line2D.Double(padding.getLeft()+pictogramWidth+xGap,padding.getTop(),padding.getLeft()+pictogramWidth+xGap,height-padding.getBottom()));
             */

            drawDiagram(g, p, items.get(i).getCurveStyle(), pictogramWidth, padding.getLeft(), padding.getTop() + (i + 0.5) * yStep);
        }

        g.restore();

    }
}
