package ru.spbu.abarsic;

import android.content.Context;
import android.util.Log;

import java.io.File;

import dalvik.system.DexClassLoader;

/**
 * Created by Владислав on 24.07.2015.
 */
public class ModelDexClassLoader extends ClassLoader {

    private static ModelDexClassLoader instance = null;
    private DexClassLoader dexClassLoader;

    public ModelDexClassLoader() {
        super(ClassLoader.getSystemClassLoader());
    }

    public static ModelDexClassLoader getInstance(){
        if (instance == null) {
            instance = new ModelDexClassLoader();
        }
        return instance;
    }

    /**
     * Подготовка к загрузке класса, после загрузки модели во внешнее хранилище
     * @param context
     * @param modelPath путь, где сохранятеся нераспакованная модель
     */
    public void install(Context context,String modelPath){
        File dexClassLoaderOutputDir = context.getDir("models",0);
        DexClassLoader dexClassLoader = new DexClassLoader(modelPath,
                dexClassLoaderOutputDir.getAbsolutePath(),
                null, context.getClassLoader());
        this.dexClassLoader = dexClassLoader;
    }

    @Override
    protected Class<?> findClass(String className) throws ClassNotFoundException{
        Class<?> model = null;
        try {
            model = dexClassLoader.loadClass(className);
        } catch (ClassNotFoundException e) {
            Log.e("loadClass()",e.toString());
        }
        if (model != null){
            return model;
        } else {
            return null;
        }
    }
}
