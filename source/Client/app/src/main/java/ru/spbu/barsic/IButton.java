package ru.spbu.barsic;

public interface IButton extends IElement {

    void setWidth(int value);

    void setHeight(int value);

    void reload();

    void setHint(String value);

    void setTextSize(int value);

    void setBackColor(String value); //android.R.color value

    String getTitle();

    void setTitle(String value);

    void setText(String value);

    void setIsDefaultOK(boolean value);

    void setIsParentFont(boolean value);

    void setIsEnabledOnCalc(boolean value);

    void setFont(String value);

    void setOnClick(Runnable eventHandler);
}
