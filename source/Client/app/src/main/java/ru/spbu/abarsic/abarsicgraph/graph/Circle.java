package ru.spbu.abarsic.abarsicgraph.graph;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

import static ru.spbu.abarsic.abarsicgraph.CartTransform.*;

/**
 * Created by Мартынюк on 17.04.2015.
 */
public final class Circle extends GraphicPrimitive {

    double r;

    private RectF boundRect = new RectF(0.0f, 0.0f, 0.0f, 0.0f);

    public Circle(double xCenter, double yCenter, double radius, int fillColor, int strokeColor) throws IllegalArgumentException {
        super(xCenter, yCenter, fillColor, strokeColor);
        if (radius < 0.0) {
            throw new IllegalArgumentException(String.format("Radius must be larger or egual to zero: r=%f", r));
        }
        this.r = radius;
    }

    @Override
    public void draw(Canvas c, Paint p, double[] coeffs) {
        boundRect.set((float)(coeffs[AX]*(x0-r)+coeffs[BX]), (float)(coeffs[AY]*(y0+r)+coeffs[BY]),
                      (float)(coeffs[AX]*(x0+r)+coeffs[BX]), (float)(coeffs[AY]*(y0-r)+coeffs[BY]));
        setFillPaint(p);
        c.drawOval(boundRect, p);
        setStrokePaint(p);
        c.drawOval(boundRect, p);
    }
}
