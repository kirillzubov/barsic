/*
 * Ellipse2D.java
 */

package ru.spbu.abarsic.abarsicgraph.awtandroid.geom;

/**
 * @author Martynyuk
 * */

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

public class Ellipse2D extends Figure2D {
	
	float xLeft;
	float xRight;
	float yTop;
	float yBottom;

	public Ellipse2D(float xLeft, float yTop, float width, float height) {
		super();
		this.xLeft = xLeft;
		this.yTop = yTop;
		this.xRight = xLeft + width;
		this.yBottom = yTop + height;
	}
	
	public Ellipse2D(double xLeft, double yTop, double width, double height) {
		super();
		this.xLeft = (float)xLeft;
		this.yTop = (float)yTop;
		this.xRight = (float)(xLeft+width);
		this.yBottom = (float)(yTop+height);
	}
	
	@Override
	public void draw(Canvas g, Paint p) {
		g.drawOval(new RectF(xLeft, yTop, xRight, yBottom), p);
	}
	
	public float getMinX() {
		return xLeft;
	}
	
	public float getMaxX() {
		return xRight;
	}
	
	public float getMinY() {
		return yTop;
	}
	
	public float getMaxY() {
		return yBottom;
	}
	
	public float getWidth() {
		return xRight-xLeft;
	}

	public float getHeight() {
		return yBottom-yTop;
	}
}
