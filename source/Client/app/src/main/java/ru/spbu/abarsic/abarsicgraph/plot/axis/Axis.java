/*
 * Axis.java
 */

package ru.spbu.abarsic.abarsicgraph.plot.axis;

/**
 *
 * @author Max
 */

/**
 * Porting to Android performed by Martynyuk
 */

import android.graphics.Color;
import android.graphics.Canvas;
import android.graphics.Paint;
import ru.spbu.abarsic.abarsicgraph.awtandroid.Font;
import ru.spbu.abarsic.abarsicgraph.awtandroid.geom.Point2D;
import ru.spbu.abarsic.abarsicgraph.plot.Margin;
import ru.spbu.abarsic.abarsicgraph.plot.Range;

/**
 * 
 * @author Анита
 */
public abstract class Axis {

    /**
     * Enumeration of the mark type of axis
     */
    public enum MarksType {

        /**
         * Axis may be up or down
         */
        UpDown,
        /**
         * Axis may be up only
         */
        UpOnly,
        /**
         * Axis may be down only
         */
        DownOnly
    };
    /**
     * The alignment type of axis values relative to marks by x
     */
    protected AxisValue.AlignW alignW = AxisValue.AlignW.Center;

    /**
     * Returns the alignment type of axis values relative to marks by x
     * @return the alignment type of axis values relative to marks by x, <code>AxisValue.AlignW</code>, one of the
     *
     * <ul>
     * <li>{@code AxisValue.AlignW.Left}
     * <li>{@code AxisValue.AlignW.Center}
     * <li>{@code AxisValue.AlignWType.Right}
     * </ul>
     */
    public AxisValue.AlignW getAlignW() {
        return alignW;
    }

    /**
     * Sets the alignment type of axis values relative to marks by x.
     * @param value the alignment type of axis values relative to marks by x, <code>AxisValue.AlignW</code>, one of the
     *
     * <ul>
     * <li>{@code AxisValue.AlignW.Left}
     * <li>{@code AxisValue.AlignW.Center}
     * <li>{@code AxisValue.AlignW.Right}
     * </ul>
     */
    public void setAlignW(AxisValue.AlignW value) {
        this.alignW = value;
    }
    /**
     * The alignment type of axis values relative to marks by y
     */
    protected AxisValue.AlignH alignH = AxisValue.AlignH.Bottom;

    /**
     * Returns the alignment type of axis values relative to marks by y
     * @return the alignment type of axis values relative to marks by y, <code>AxisValue.AlignH</code>, one of the
     *
     * <ul>
     * <li>{@code AxisValue.AlignH.Top}
     * <li>{@code AxisValue.AlignH.Center}
     * <li>{@code AxisValue.AlignH.Baseline}
     * <li>{@code AxisValue.AlignH.Bottom}
     * </ul>
     */
    public AxisValue.AlignH getAlignH() {
        return alignH;
    }

    /**
     * Sets the alignment type of axis values relative to marks by y.
     * @param value the alignment type of axis values relative to marks by y, <code>AxisValue.AlignH</code>, one of the
     *
     * <ul>
     * <li>{@code AxisValue.AlignH.Top}
     * <li>{@code AxisValue.AlignH.Center}
     * <li>{@code AxisValue.AlignH.Baseline}
     * <li>{@code AxisValue.AlignH.Bottom}
     * </ul>
     */
    public void setAlignH(AxisValue.AlignH value) {
        this.alignH = value;
    }
    /**
     * Title's name of the axis
     */
    protected String title = "not set";

    /**
     * Returns title of the axis
     * @return title of the axis
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Sets title of the axis
     * @param value title of the axis
     */
    public void setTitle(String value) {
        this.title = value;
    }
    /**
     *  Title is visible
     */
    protected boolean showTitle = true;

    /**
     * Returns true if title is visible
     * @return true if title is visible
     */
    public boolean getShowTitle() {
        return this.showTitle;
    }

    /**
     * Sets visibility of title
     * @param value visibility of title
     */
    public void setShowTitle(boolean value) {
        this.showTitle = value;
    }
    /**
     *  Units name of the axis values
     */
    protected String units = "";

    /**
     * Returns units name of the axis values (e.g. cm, eV, etc.)
     * @return units name of the axis values (e.g. cm, eV, etc.)
     */
    public String getUnits() {
        return this.units;
    }

    /**
     * Sets units name of the axis values (e.g. cm, eV, etc.)
     * @param value 
     */
    public void setUnits(String value) {
        this.units = value;
    }
    /**
     * The font of the axis values
     */
    protected Font font = new Font(Font.MONOSPACED, Font.PLAIN, 15);

    /**
     * Returns the font of the axis values
     * @return the font of the axis values
     */
    public Font getFont() {
        return this.font;
    }

    /**
     * Sets the font of the axis values. The default value is new Font(Font.MONOSPACED, Font.PLAIN,14)
     * @param value the font of the axis values
     */
    public void setFont(Font value) {
        this.font = value;
    }
    /**
     * The font of the axis title
     */
    protected Font titleFont = new Font(Font.SANS_SERIF, Font.PLAIN, 20);

    /**
     * Returns the font of the axis title
     * @return the font of the axis title
     */
    public Font getTitleFont() {
        return this.titleFont;
    }

    /**
     * Sets the font of the axis title. The default value is new Font(Font.SANS_SERIF,Font.PLAIN,20);
     * @param value the font of the axis title
     */
    public void setTitleFont(Font value) {
        this.titleFont = value;
    }
    /**
     *  The foreground color of axis values constant
     */
    protected int foreground = Color.BLACK;

    /**
     * Returns the foreground color of axis values
     * @return the foreground color of axis values
     */
    public int getForeground() {
        return this.foreground;
    }

    /**
     * Sets the foreground color of axis values. The dafault value is Color.Black
     * @param value 
     */
    public void setForeground(int value) {
        this.foreground = value;
    }
    /**
     * The foreground color of axis title constant
     */
    protected int titleForeground = Color.BLACK;

    /**
     * Returns the foreground color of axis title
     * @return the foreground color of axis title
     */
    public int getTitleForeground() {
        return this.titleForeground;
    }

    /**
     * Sets the foreground color of axis title. The dafault value is Color.Black
     * @param value 
     */
    public void setTitleForeground(int value) {
        this.titleForeground = value;
    }
    /**
     * The axis line margin from left and right sides constant
     */
    protected Margin margin = new Margin(20, 20);

    /**
     * Returns the axis line margin from left and right sides
     * @return the axis line margin from left and right sides
     */
    public Margin getMargin() {
        return this.margin;
    }

    /**
     * Sets the axis line margin from left and right sides
     * @param value 
     */
    public void setMargin(Margin value) {
        this.margin = value;
    }
    /**
     * The position of the top left point BEFORE Transfoming
     */
    protected Point2D pos;

    /**
     * Returns the position of the top left point BEFORE Transfoming (e.g. translate or rotation)
     * @return the position of the top left point BEFORE Transfoming (e.g. translate or rotation)
     */
    public Point2D getPos() {
        return this.pos;
    }

    /**
     * Sets the position of the top left point BEFORE Transfoming (e.g. translate or rotation)
     * @param value
     */
    public void setPos(Point2D value) {
        this.pos = value;
    }
    /**
     * The width of the axis canvas
     */
    protected double width;

    /**
     * Returns the width of the axis canvas(with spacing from left and right)
     * @return the width of the axis canvas
     */
    public double getWidth() {
        return this.width;
    }

    /**
     * Sets the width of the axis canvas(with spacing from left and right))
     * @param value the width of the axis canvas(with spacing from left and right)
     */
    public void setWidth(double value) {
        this.width = value;
    }
    /**
     * The height of the axis canvas
     */
    protected double height;

    /**
     * Returns the height of the axis canvas(with spacing from left and right)
     * @return the height of the axis canvas
     */
    public double getHeight() {
        return this.height;
    }

    /**
     * Sets the height of the axis canvas(with spacing from left and right))
     * @param value the height of the axis canvas(with spacing from left and right)
     */
    public void setHeight(double value) {
        this.height = value;
    }
    /**
     * Constructor of the segment of values that we want to display
     */
    protected Range dataSegment = new Range(-10, 10);

    /**
     * Returns the segment of values that we want to display
     * @return the segment of values that we want to display
     */
    public Range getDataSegment() {
        return this.dataSegment;
    }

    /**
     * Sets the segment of values that we want to display
     * @param value the segment of values that we want to display
     */
    public void setDataRange(Range value) {
        this.dataSegment = value;
    }
    /**
     *The count of small marks between big marks
    
     */
    protected int smallMarksCount = 3;

    /**
     * Returns the count of small marks between big marks
     * @return the count of small marks between big marks
     */
    public int getSmallMarksCount() {
        return this.smallMarksCount;
    }

    /**
     * Sets the count of small marks between big marks
     * @param value 
     */
    public void setSmallMarksCount(int value) {
        this.smallMarksCount = value;
    }
    /**
     *  The marks type of axis
     */
    protected MarksType marksType = MarksType.UpDown;

    /**
     * Returns the marks type of axis
     * @return the <code>MarksType</code>, one of the
     *        following values:
     * <ul>
     * <li>{@code MarksType.UpDown}
     * <li>{@code MarksType.UpOnly}
     * <li>{@code MarksType.DownOnly}
     * </ul>
     */
    public MarksType getMarkstype() {
        return this.marksType;
    }

    /**
     * Sets the the marks type of axis
     * @param value 
     */
    public void setMarkstype(MarksType value) {
        this.marksType = value;
    }
    /**
     * The tilt angle of the axis 
     */
    protected double angle = 0;

    /**
     * Returns the tilt angle of the axis (for example, for vertical axis this is +-0.5*Math.PI)
     * @return tilt angle
     */
    public double getAngle() {
        return this.angle;
    }

    /**
     * Sets the tilt angle of the axis (for example, for vertical axis this is +-0.5*Math.PI)
     * @param value the tilt angle of the axis
     */
    public void setAngle(double value) {
        this.angle = value;
    }
    /**
     * The orientation (angle) of the axis values
     */
    protected AxisValue.Orientation digitsOrientation = AxisValue.Orientation.Horizontal;

    /**
     * Returns the orientation (angle) of the axis values
     * @return the orientation (angle) of the axis values
     */
    public AxisValue.Orientation getDigitsOrientation() {
        return this.digitsOrientation;
    }

    /**
     * Sets the orientation (angle) of the axis values
     * @param value 
     */
    public void setDigitsOrientation(AxisValue.Orientation value) {
        this.digitsOrientation = value;
    }
    /**
     * Parametr is true if only the axis is rendering in the reverse format
     */
    protected boolean reverse = false;

    /**
     * Returns if the values of the axis is rendering in the reverse format(
     * For example, the values renders as 5 0 -5 -10 instead of -10 -5 0 5)
     * @return if the values of the axis is rendering in the reverse format
     */
    public boolean getReverse() {
        return this.reverse;
    }

    /**
     * Sets if the values of the axis is rendering in the reverse format(
     * For example, the values renders as 5 0 -5 -10 instead of -10 -5 0 5)
     * @param value 
     */
    public void setReverse(boolean value) {
        this.reverse = value;
    }
    /**
     * Color of gridlines
     */
    protected int gridLinesColor = Color.LTGRAY;

    /**
     * Returns color of gridlines
     * @return
     */
    public int getGridLinesColor() {
        return this.gridLinesColor;
    }

    /**
     * Sets color of gridlines
     * @param value
     */
    public void setGridLinesColor(int value) {
        this.gridLinesColor = value;
    }
    /**
     * This is true if gridlines are shown
     */
    protected boolean showGridLines = true;

    /**
     * 
     * @return 
     */
    public boolean getShowGridLines() {
        return this.showGridLines;
    }

    /**
     * sets 
     * @param value
     */
    public void setShowGridLines(boolean value) {
        this.showGridLines = value;
    }
    /**
     * Length of gridlines
     */
    protected double gridLinesLength = 0;

    /**
     * Returns length of gridlines
     * @return length of gridlines
     */
    public double getGridLinesLength() {
        return this.gridLinesLength;
    }

    /**
     *Sets length of gridlines 
     * @param value
     */
    public void setGridLinesLength(double value) {
        this.gridLinesLength = value;
    }

    /**
     * Main paint event. Draws the axis
     */
    public abstract void draw(Canvas c, Paint p);
}
