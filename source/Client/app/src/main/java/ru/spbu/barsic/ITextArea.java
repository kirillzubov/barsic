package ru.spbu.barsic;

public interface ITextArea extends IElement {
    void setText(String value);

    void setIsParentBackColor(boolean value);

    void setIsScrollBars(boolean value);

    void setIsSourceCode(boolean value);

    void setIsWordWrap(boolean value);

    void setIsReadOnly(boolean value);

    void setIsParentFont(boolean value);

    void setAssociatedVariable(String value);

    void setIsEnabledOnCalc(boolean value);
}
