package ru.spbu.abarsic;

import ru.spbu.barsic.ITextArea;

public class TextArea implements ITextArea {

    @Override
    public void setWidth(int value) {

    }

    @Override
    public void setHeight(int value) {

    }

    @Override
    public void setAlign(String value) {

    }

    @Override
    public void setOnHelp(final Runnable eventHandler) {

    }

    @Override
    public void setName(String value) {

    }

    @Override
    public void setLeftPos(int value) {

    }

    @Override
    public void setTopPos(int value) {

    }

    @Override
    public void setHint(String value) {

    }

    @Override
    public void setIsEnabled(boolean value) {

    }

    @Override
    public void setIsVisible(boolean value) {

    }

    @Override
    public void setText(String value) {

    }

    @Override
    public void setIsParentBackColor(boolean value) {

    }

    @Override
    public void setIsScrollBars(boolean value) {

    }

    @Override
    public void setIsSourceCode(boolean value) {

    }

    @Override
    public void setIsWordWrap(boolean value) {

    }

    @Override
    public void setIsReadOnly(boolean value) {

    }

    @Override
    public void setIsParentFont(boolean value) {

    }

    @Override
    public void setAssociatedVariable(String value) {

    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {

    }

}