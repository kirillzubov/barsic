package ru.spbu.abarsic.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import ru.spbu.abarsic.R;


/**
 * Created by Владислав on 08.05.2015.
 */
public class FragmentReport extends Fragment {


    private static final String URL_OF_HOME_PAGE = "URL_OF_HOME_PAGE";
    private String urlOfHomePage = "http://distolymp.spbu.ru/phys/olymp/";

    private WebView webView;
    private Button button;


    private static FragmentReport instance = null;

    public static FragmentReport getInstance(String urlOfHomePage){
        if (instance == null){
            instance = new FragmentReport();
            Bundle args = new Bundle();
            args.putString(URL_OF_HOME_PAGE, urlOfHomePage);
            instance.setArguments(args);
        }
        return  instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments()!=null){
            urlOfHomePage = getArguments().getString(URL_OF_HOME_PAGE);
        }
        super.onCreate(savedInstanceState);
    }

    public FragmentReport() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_report,container,false);

        webView = (WebView) view.findViewById(R.id.webViewReport);
        button = (Button) view.findViewById(R.id.buttonRefreshReport);

        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());



        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setInitialScale(1);

        webView.loadUrl(urlOfHomePage + "p-model/res/");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                webView.loadUrl(urlOfHomePage + "p-model/res/");
            }
        });
        return view;
    }



}

