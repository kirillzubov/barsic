package ru.spbu.abarsic.abarsicgraph;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AbsoluteLayout;

import static ru.spbu.abarsic.abarsicgraph.CartTransform.*;

import ru.spbu.barsic.ISubwindowView;

/**
 * Created by Мартынюк on 20.09.2015.
 */
public final class SubwindowView extends View implements ISubwindowView {

    Subwindow mSubwindow;
    Graph mGraph;
    Plot mPlot;

    double mXMin, mXMax, mYMin, mYMax;
    int mWidth, mHeight;
    double[] mCoeffs = new double[4];

    boolean mUpdating, mEndOutputCalled;

    public SubwindowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mXMin = DEF_BOUNDS[0];
        mXMax = DEF_BOUNDS[1];
        mYMin = DEF_BOUNDS[2];
        mYMax = DEF_BOUNDS[3];
        mWidth = DEF_VIEW_SIZES[0];
        mHeight = DEF_VIEW_SIZES[1];
        calculateCoeffs(mXMin, mXMax, mYMin, mYMax, mWidth, mHeight, mCoeffs);
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mWidth = getWidth();
                mHeight = getHeight();
                if ((mWidth > 0) && (mHeight > 0)) {
                    setViewSizes(mWidth, mHeight);
                }
            }
        });
    }

    //necessary method which must be called after creation of subwindow, plot and graph objects.
    public void bindWithCommonObjects(Subwindow subwindow, Plot plot, Graph graph) {
        if ((subwindow == null) || (plot == null) || (graph == null))
            throw new IllegalArgumentException("Subwindow, plot and graph objects must be created in advance.");
        mSubwindow = subwindow;
        mPlot = plot;
        mGraph = graph;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //TODO implement method drawTo
        //mPlot.drawTo(canvas);
        mGraph.drawTo(canvas);

        mUpdating = false;
        if (mEndOutputCalled) {
            endOutput();
            mEndOutputCalled = false;
        }
    }

    @Override
    public void output() {
        mSubwindow.mSubwindowView = this;
        mGraph.mSubwindowView = this;
        mPlot.mSubwindowView = this;
    }

    @Override
    public void endOutput() {
        if (mUpdating) {
            mEndOutputCalled = true;
        } else {
            mSubwindow.mSubwindowView = null;
            mGraph.mSubwindowView = null;
            mPlot.mSubwindowView = null;
        }
    }

    //this method is called in onWindowFocusChanged of Activity class for every view to
    //determine transformation coefficients correctly.
    public void setViewSizes(int width, int height) {
        mWidth = width;
        mHeight = height;
        calculateCoeffs(mXMin, mXMax, mYMin, mYMax, mWidth, mHeight, mCoeffs);
    }

    @Override
    public void setName(String value) {

    }

    @Override
    public void setLeftPos(int value) {
        setLeft(value);
    }

    @Override
    public void setTopPos(int value) {
        setTop(value);
    }

    public int width() {
        return mWidth;
    }

    @Override
    public void setWidth(int value) {
        /*if (value <= 0) return;
        setMeasuredDimension(value, getHeight());
        mWidth = value;*/

        AbsoluteLayout.LayoutParams params =
                new AbsoluteLayout.LayoutParams(value, getHeight(), getLeft(), getTop());
        super.setLayoutParams(params);
    }

    @Override
    public void setHeight(int value) {
        /*if (value <= 0) return;
        setMeasuredDimension(getWidth(), value);
        mHeight = value;*/

        AbsoluteLayout.LayoutParams parampampam =
                new AbsoluteLayout.LayoutParams(getWidth(), value, getLeft(), getTop());
        super.setLayoutParams(parampampam);
    }

    public int height() {
        return mHeight;
    }

    @Override
    public void setHint(String value) {
        //setTag(value);
    }

    @Override
    public void setIsEnabled(boolean value) {
        setEnabled(value);
    }

    @Override
    public void setIsVisible(boolean value) {
        //setVisibility();
    }

    @Override
    public void setAlign(String value) {
        //setLayoutParams();
    }

    @Override
    public void setIsParentBackColor(boolean value) {

    }

    @Override
    public void setBackColor(String value) {
        //setBackgroundColor();
    }

    @Override
    public void setOnHelp(final Runnable eventHandler) {

    }

    @Override
    public void setOnMouseDown(final Runnable eventHandler) {
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    eventHandler.run();
                }
                return true;
            }
        });
    }

    @Override
    public void setOnDrag(final Runnable eventHandler) {
        setOnDragListener(new OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                if (event.getAction() == DragEvent.ACTION_DRAG_STARTED)
                    eventHandler.run();
                return true;
            }
        });
    }

    @Override
    public void setOnDragDrop(final Runnable eventHandler) {
        setOnDragListener(new OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                if (event.getAction() == DragEvent.ACTION_DROP)
                    eventHandler.run();
                return true;
            }
        });
    }

    @Override
    public void setOnMouseOver(final Runnable eventHandler) {

    }

    @Override
    public void setOnMouseScale(final Runnable eventHandler) {

    }

}
