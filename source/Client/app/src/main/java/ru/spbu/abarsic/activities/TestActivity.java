package ru.spbu.abarsic.activities;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import barsic.Bootstrapper;
import barsic.app.MainModule;
import ru.spbu.abarsic.R;
import ru.spbu.barsic.IModule;

public class TestActivity extends AppCompatActivity {
    private boolean alreadyInit =false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(alreadyInit){
            return;
        }
        alreadyInit = true;
        Bootstrapper.setFragmentManger(getFragmentManager());

        setContentView(R.layout.activity_task);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        IModule module = new MainModule();
        module.onProgramStart();
    }
}