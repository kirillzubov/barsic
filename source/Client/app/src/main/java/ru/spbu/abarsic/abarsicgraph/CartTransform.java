package ru.spbu.abarsic.abarsicgraph;

/**
 * Created by Мартынюк on 03.12.2014.
 */

//transformation from cartesian coordinate system to screen one

public final class CartTransform {

    public static final int AX = 0;
    public static final int BX = 1;
    public static final int AY = 2;
    public static final int BY = 3;

    public static int[] DEF_VIEW_SIZES = {320, 160};
    public static double[] DEF_BOUNDS = {-10.0, 10.0, -5.0, 5.0};

    public static void calculateCoeffs(double xLeft, double xRight, double yBottom, double yTop,
                                       int width, int height, double[] coeffs) {

        coeffs[AX] = (double)width / (xRight - xLeft);
        coeffs[BX] = -coeffs[AX] * xLeft;
        coeffs[AY] = (double)height / (yBottom - yTop);
        coeffs[BY] = -coeffs[AY] * yTop;

    }

    public enum PivotLocationX {
        LEFT, CENTER, RIGHT
    }

    public enum PivotLocationY {
        BOTTOM, CENTER, TOP
    }

    boolean autoscaling = true;
    PivotLocationX pivotLocationX = PivotLocationX.CENTER;
    PivotLocationY pivotLocationY = PivotLocationY.CENTER;

    double x1, x2, y1, y2;
    double w, h;
    double ax, bx, ay, by;
    double[] coeffs = new double[4];

    private CartTransform() {
        super();
    }

    public CartTransform(double xLeft, double xRight, double yBottom, double yTop, float width, float height) {
        super();
        if ((width <= 0) || (height <= 0) || (xRight <= xLeft) || (yTop <= yBottom)) {
            throw new IllegalArgumentException("Illegal arguments are used in constructor \"CartTransform\"");
        }

        x1 = xLeft;
        x2 = xRight;
        y1 = yBottom;
        y2 = yTop;
        w = (double)width;
        h = (double)height;

        recalculateCoeffs();
    }

    private void recalculateCoeffs() {

        ax = w / (x2 - x1);
        bx = -ax * x1;
        ay = h / (y1 - y2);
        by = -ay * y2;

        if (autoscaling) {
            if (ax >= -ay) {
                ax = -ay;
                switch (pivotLocationX) {
                    case LEFT:
                        bx = ay * x1;
                        x2 = (bx - w) / ay;
                        break;
                    case CENTER:
                        bx = (w + ay * (x1 + x2)) * 0.5;
                        x1 = bx / ay;
                        x2 = (bx - w) / ay;
                        break;
                    case RIGHT:
                        bx = w + x2 * ay;
                        x1 = bx / ay;
                        break;
                }
            } else {
                ay = -ax;
                switch (pivotLocationY) {
                    case BOTTOM:
                        by = h + ax * y1;
                        y2 = by / ax;
                        break;
                    case CENTER:
                        by = (h + ax * (y1 + y2)) * 0.5;
                        y1 = (by - h) / ax;
                        y2 = by / ax;
                        break;
                    case TOP:
                        by = ax * y2;
                        y1 = (by - h) / ax;
                        break;
                }
            }
        }

        coeffs[AX] = ax;
        coeffs[AY] = ay;
        coeffs[BX] = bx;
        coeffs[BY] = by;
    }

    public double[] getCoeffs() {
        return coeffs;
    }

    public void setWindowSizes(float width, float height) {
        if ((width <= 0) || (height <= 0)) {
            throw new IllegalArgumentException("Illegal arguments are used in setter \"setWindowSizes\"");
        }

        w = (double)width;
        h = (double)height;

        recalculateCoeffs();
    }

    public void setRegionBounds(double xLeft, double xRight, double yBottom, double yTop) {
        if ((xRight <= xLeft) || (yTop <= yBottom)) {
            throw new IllegalArgumentException("Illegal arguments are used in setter \"setRegionBounds\"");
        }

        x1 = xLeft;
        x2 = xRight;
        y1 = yBottom;
        y2 = yTop;

        recalculateCoeffs();
    }

    public void setAutoscaling(boolean autoscaling) {
        this.autoscaling = autoscaling;
    }

    public void setPivotLocations(PivotLocationX pivotLocationX, PivotLocationY pivotLocationY) {
        this.pivotLocationX = pivotLocationX;
        this.pivotLocationY = pivotLocationY;
    }

    public float xCartToScreen(double xCart) {
        return (float)(ax*xCart+bx);
    }

    public float yCartToScreen(double yCart) {
        return (float)(ay*yCart+by);
    }

    public double xScreenToCart(float xScreen) {
        return ((double)xScreen-bx)/ax;
    }

    public double yScreenToCart(float yScreen) {
        return ((double)yScreen-by)/ay;
    }

}
