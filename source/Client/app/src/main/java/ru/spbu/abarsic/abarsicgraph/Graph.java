package ru.spbu.abarsic.abarsicgraph;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.Arrays;

import static ru.spbu.abarsic.abarsicgraph.CartTransform.*;
import static ru.spbu.abarsic.abarsicgraph.ColorUtils.formatColorAsInt;
import static ru.spbu.abarsic.abarsicgraph.ColorUtils.formatColorAsString;
import static java.lang.Math.abs;
import static java.lang.Math.max;

import ru.spbu.barsic.IBrush;
import ru.spbu.barsic.IFont;
import ru.spbu.barsic.IGraph;
import ru.spbu.abarsic.abarsicgraph.graph.Circle;
import ru.spbu.abarsic.abarsicgraph.graph.Ellipse;
import ru.spbu.abarsic.abarsicgraph.graph.GraphicObject;
import ru.spbu.abarsic.abarsicgraph.graph.Line;
import ru.spbu.abarsic.abarsicgraph.graph.PolyLine;
import ru.spbu.abarsic.abarsicgraph.graph.Polygon;
import ru.spbu.abarsic.abarsicgraph.graph.Rectangle;
import ru.spbu.barsic.IPen;

/**
 * Created by Мартынюк on 08.12.2014.
 */
public final class Graph implements IGraph {

    ArrayList<GraphicObject> mGraphicObjects = new ArrayList<GraphicObject>(256);
    ArrayList<Group> mGroups = new ArrayList<Group>(32);

    SubwindowView mSubwindowView;

    Brush mBrush;
    Pen mPen;
    Font mFont;

    private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    public Graph() {
        super();
        mBrush = new Brush(this);
        mPen = new Pen(this);
        mFont = new Font(this);
    }

    @Override
    public void update() {
        mSubwindowView.mUpdating = true;
        mSubwindowView.invalidate();
    }

    public void canvasClear() {
        //TODO implement clearing of SubwindowView canvas
        mGraphicObjects.clear();
    }

    public void drawTo(Canvas c) {
        if (mSubwindowView == null) return;
        double[] coeffs = Arrays.copyOf(mSubwindowView.mCoeffs, 4);
        for (int i=0; i<mGraphicObjects.size(); i++)
            mGraphicObjects.get(i).draw(c, mPaint, coeffs);
    }

    @Override
    public Brush brush() {
        return mBrush;
    }

    @Override
    public Pen pen() {
        return mPen;
    }

    @Override
    public Font font() {
        return null; //TODO implement method font
    }

    public Group group(String name) {
        Group g = findGroup(name);
        if (g == null) {
            g = new Group(name, mGroups.size());
            mGroups.add(g);
        }
        return g;
    }

    public Group objects(int index) {
        Group g;
        if ((index < mGraphicObjects.size()) && (index > 0)) {
            g = new Group("group"+String.valueOf(mGroups.size()+1), -1);
            g.mMinIndex = mGraphicObjects.size()-1;
            g.mMaxIndex = mGraphicObjects.size()-1;
            return g;
        } else {
            return null;
        }
    }

    @Override
    public double[] bounds() {
        return new double[]{mSubwindowView.mXMin, mSubwindowView.mXMax,
                            mSubwindowView.mYMin, mSubwindowView.mYMax};
    }

    @Override
    public void setBounds(double xLeft, double xRight, double yBottom, double yTop) {
        mSubwindowView.mXMin = xLeft;
        mSubwindowView.mXMax = xRight;
        mSubwindowView.mYMin = yBottom;
        mSubwindowView.mYMax = yTop;
        calculateCoeffs(xLeft, xRight, yBottom, yTop,
                mSubwindowView.mWidth, mSubwindowView.mHeight, mSubwindowView.mCoeffs);
    }

    @Override
    public void arc(double xCenter, double yCenter, double width, double height, double startAngle, double endAngle) {

    }

    @Override
    public void chord(double xCenter, double yCenter, double width, double height, double startAngle, double endAngle) {

    }

    @Override
    public void circle(double xCenter, double yCenter, double radius) {
        mGraphicObjects.add(new Circle(xCenter, yCenter, radius, mBrush.colorInt(), mPen.colorInt()));
    }

    @Override
    public void ellipse(double xCenter, double yCenter, double width, double height) {
        mGraphicObjects.add(new Ellipse(xCenter, yCenter, width, height, mBrush.colorInt(), mPen.colorInt()));
    }

    @Override
    public void line(double x1, double y1, double x2, double y2) {
        mGraphicObjects.add(new Line(x1, y1, x2, y2, mBrush.colorInt(), mPen.colorInt()));
    }

    @Override
    public void picture(String file, double xLeft, double yTop, double xRight, double yBottom) {

    }

    @Override
    public void pie(double xCenter, double yCenter, double width, double height, double startAngle, double endAngle) {

    }

    @Override
    public void rectangle(double xCenter, double yCenter, double width, double height) {
        mGraphicObjects.add(new Rectangle(xCenter, yCenter, width, height, mBrush.colorInt(), mPen.colorInt()));
    }

    @Override
    public void roundRectangle(double xCenter, double yCenter, double width, double height, double radiusX, double radiusY) {

    }

    @Override
    public void text(String s, double xLeft, double yTop) {

    }

    @Override
    public void polygon(double[] xyPoints) {
        mGraphicObjects.add(new Polygon(xyPoints, mBrush.colorInt(), mPen.colorInt()));
    }

    @Override
    public void polyLine(double[] xyPoints) {
        mGraphicObjects.add(new PolyLine(xyPoints, mBrush.colorInt(), mPen.colorInt()));
    }

    /**
     * Created by Мартынюк on 09.02.2015.
     */
    public static final class Pen implements IPen {

        Graph mGraph;
        Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        double width = 0.0;

        public Pen(Graph graph) {
            super();
            mGraph = graph;
            mPaint.setStyle(Paint.Style.STROKE);
        }

        @Override
        public String color() { //'#RRGGBB' format
            return formatColorAsString(mPaint.getColor());
        }

        public int colorInt() {
            return mPaint.getColor();
        }

        @Override
        public void setColor(String color) { //'#RRGGBB' format
            mPaint.setColor(formatColorAsInt(color));
        }

        public void setColorInt(int color) {
            mPaint.setColor(color);
        }

        @Override
        public double width() {
            return width;
        }

        @Override
        public void setWidth(double value) {
            if (value < 0.0) {
                throw new IllegalArgumentException("Width must be non-negative");
            }
            float screenWidth = (float)(abs(mGraph.mSubwindowView.mCoeffs[AX])*value);
            mPaint.setStrokeWidth(max(screenWidth, 1.0f));
            width = value;
        }

    }

    /**
     * Created by Мартынюк on 09.02.2015.
     */
    public static final class Brush implements IBrush {

        Graph mGraph;
        Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        public Brush(Graph graph) {
            super();
            mGraph = graph;
            mPaint.setStyle(Paint.Style.FILL);
        }

        @Override
        public String color() { //'#RRGGBB' format
            return formatColorAsString(mPaint.getColor());
        }

        public int colorInt() {
            return mPaint.getColor();
        }

        @Override
        public void setColor(String color) { //'#RRGGBB' format
            mPaint.setColor(formatColorAsInt(color));
        }

        public void setColorInt(int color) {
            mPaint.setColor(color);
        }

        @Override
        public String style() {
            switch (mPaint.getStyle()) {
                case STROKE:
                    return "transparent";
                case FILL: case FILL_AND_STROKE: default:
                    return "solid";
            }
        }

        @Override
        public void setStyle(String style) {
            if (style.equals("transparent")) {
                mPaint.setStyle(Paint.Style.STROKE);
            } else if (style.equals("solid")) {
                mPaint.setStyle(Paint.Style.FILL);
            } else {
                mPaint.setStyle(Paint.Style.FILL);
            }
        }

    }

    /**
     * Created by Мартынюк on 09.02.2015.
     */
    public static final class Font implements IFont { //TODO complete the class 'Font'

        Graph mGraph;

        //protected Typeface font;
        //float size;

        Font(Graph graph) {
            super();
            mGraph = graph;
            //font = Typeface.create(face, Typeface.NORMAL);
            //this.size = (float)size;
        }

        @Override
        public String color() {
            return null;
        }

        @Override
        public void setColor(String value) {

        }

        @Override
        public String face() {
            return null;
        }

        @Override
        public void setFace(String value) {

        }

        @Override
        public double height() {
            return 0;
        }

        @Override
        public void setHeight(double value) {

        }

        @Override
        public boolean isBold() {
            return false;
        }

        @Override
        public void setBold(boolean value) {

        }

        @Override
        public boolean isItalic() {
            return false;
        }

        @Override
        public void setItalic(boolean value) {

        }

        @Override
        public boolean isStrikeout() {
            return false;
        }

        @Override
        public void setStrikeout(boolean value) {

        }

        @Override
        public boolean isUnderline() {
            return false;
        }

        @Override
        public void setUnderline(boolean value) {

        }
    }

    private Group findGroup(String name) {
        Group group;
        for (int i=0; i<mGroups.size(); i++) {
            group = mGroups.get(i);
            if (group.mName.equals(name))
                return group;
        }
        return null;
    }

    /*private*/
    public final class Group /*implements IGroup*/ {

        int mGroupIndex = -1;

        String mName;
        int mMinIndex = -1;
        int mMaxIndex = -1;

        Group(String name, int groupIndex) {
            super();
            mName = name;
            mGroupIndex = groupIndex;
        }

        void output() {
            if (mMinIndex == -1)
                mMinIndex = mGraphicObjects.size();
        }

        void endOutput() {
            if (mMaxIndex == -1)
                mMaxIndex = mGraphicObjects.size()-1;
        }

        int minIndex() {
            return mMinIndex;
        }

        int maxIndex() {
            return mMaxIndex;
        }

        int count() {
            return mMaxIndex - mMinIndex + 1;
        }

        void delete() {
            mGroups.remove(mGroupIndex);
            mGroups.trimToSize();
        }

        boolean hasIndex() {
            return mGroupIndex != -1;
        }

        void moveBy(double deltaX, double deltaY) {
            if ((mMinIndex != -1) && (mMaxIndex != -1))
            for (int i=mMinIndex; i<=mMaxIndex; i++) {
                mGraphicObjects.get(i).moveBy(deltaX, deltaY);
            }
        }

    }
}
