package ru.spbu.abarsic.activities;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SwitchDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.OnCheckedChangeListener;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import ru.spbu.abarsic.FileDownloader;
import ru.spbu.abarsic.ModelDexClassLoader;
import ru.spbu.abarsic.R;
import ru.spbu.abarsic.fragments.FragmentHelp;
import ru.spbu.abarsic.fragments.FragmentSettings;
import ru.spbu.abarsic.fragments.FragmentWebView;

public class MainActivity extends ActionBarActivity
                          implements FragmentSettings.OnSettingsChangeListener,FragmentWebView.OnModelUrlFoundListener {

    private SharedPreferences sharedPreferences;

    //TODO настройки
    private final static int PROGRESS_DIALOG = 1;
    private final static String APP_DIRECTORY = "Barsic";
    private ProgressDialog progressDialog;

    private final String APP_SETTINGS = "APP_SETTINGS";
    private final String SAVED_URL = "SAVED_URL";
    private final String IS_SHOW_URL = "IS_SHOW_URL";
    private final String CLASS_NAME = "CLASS_NAME";

    private final String URL_OF_HOME_PAGE = "URL_OF_HOME_PAGE";

    private Drawer drawer;

    private  Menu menu = null;
    private final int groupId=1;
    private final int action_refresh = 1;
    private final int action_go_home = 2;


    private FragmentTransaction ft;
    private FragmentManager fragmentManager;

    private FragmentWebView fragmentWebView;
    private FragmentSettings fragmentSettings;
    private FragmentHelp fragmentHelp;


    private String urlOfHomePage;
    private String className;
    private String modelUrl;
    private boolean isShowUrl=false;

    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        urlOfHomePage =getString(R.string.home_url_default);

        sharedPreferences = getSharedPreferences(APP_SETTINGS, MODE_PRIVATE);

        if (!sharedPreferences.contains(SAVED_URL)) {
            Toast.makeText(getApplicationContext(), "Файла настроек нет", Toast.LENGTH_SHORT).show();
            saveUrl(urlOfHomePage);
            saveSwitchDrawerItemState(isShowUrl);
        }else{
            urlOfHomePage = sharedPreferences.getString(SAVED_URL, "");
            isShowUrl = sharedPreferences.getBoolean(IS_SHOW_URL,false);
        }

        fragmentWebView =  FragmentWebView.getInstance(urlOfHomePage, isShowUrl);
        fragmentSettings = FragmentSettings.getInstance(urlOfHomePage);
        fragmentHelp = FragmentHelp.getInstance(getString(R.string.help_url));

        fragmentManager = getFragmentManager();
        fragmentManager
                .beginTransaction()
                .add(R.id.mainactivity_fragment, fragmentWebView)
                .add(R.id.mainactivity_fragment, fragmentSettings)
                .hide(fragmentSettings)
                .add(R.id.mainactivity_fragment, fragmentHelp)
                .hide(fragmentHelp)
                .commit();



        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = new DrawerBuilder()
                    .withActivity(this)
                    .withToolbar(toolbar)
                    .withHeader(R.layout.drawer_header)
                    .withDisplayBelowToolbar(false)
                    .withDisplayBelowStatusBar(true)
                    .withTranslucentActionBarCompatibility(false)
                    .addDrawerItems(
                            new PrimaryDrawerItem().withName(R.string.drawer_home).withIcon(FontAwesome.Icon.faw_home).withIdentifier(1),
                            new DividerDrawerItem(),
                            new SecondaryDrawerItem().withName(R.string.drawer_settings).withIcon(FontAwesome.Icon.faw_cog).withIdentifier(2),
                            new SecondaryDrawerItem().withName(R.string.drawer_help).withIcon(FontAwesome.Icon.faw_life_bouy).withIdentifier(3),
                            new DividerDrawerItem(),
                            new SwitchDrawerItem().withName(R.string.drawer_ShowUrl).withIcon(FontAwesome.Icon.faw_info).withChecked(false).withIdentifier(4)
                                    .withOnCheckedChangeListener(new OnCheckedChangeListener() {
                                        @Override
                                        public void onCheckedChanged(IDrawerItem iDrawerItem, CompoundButton compoundButton, boolean b) {
                                            int id = iDrawerItem.getIdentifier();
                                            if (id == 4) {
                                                SwitchDrawerItem switchDrawerItem = (SwitchDrawerItem) iDrawerItem;
                                                isShowUrl = b;
                                                fragmentWebView.onUrlStateChange(isShowUrl);

                                            }
                                        }
                                    })
                    )
                    .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                        @Override
                        public boolean onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                            if (iDrawerItem != null) {
                                int id = iDrawerItem.getIdentifier();
                                switch (id) {
                                    case 1:
                                        fragmentManager.beginTransaction()
                                                .hide(fragmentHelp)
                                                .hide(fragmentSettings)
                                                .show(fragmentWebView)
                                                .commit();
                                        changeMenuVisible(true);
                                        toolbar.setTitle(R.string.title_activity_main);
                                        break;
                                    case 2:
                                        fragmentManager.beginTransaction()
                                                .hide(fragmentWebView)
                                                .hide(fragmentHelp)
                                                .show(fragmentSettings)
                                                .commit();
                                        changeMenuVisible(false);
                                        toolbar.setTitle(R.string.section_settnigs);
                                        break;
                                    case 3:
                                        fragmentManager.beginTransaction()
                                                .hide(fragmentWebView)
                                                .hide(fragmentSettings)
                                                .show(fragmentHelp)
                                                .commit();
                                        changeMenuVisible(false);
                                        toolbar.setTitle(R.string.section_help);
                                        break;
                                        }
                            }
                            return false;
                        }
                    })
                    .build();


    }

    public void saveUrl(String url){
        sharedPreferences = getSharedPreferences(APP_SETTINGS,MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putString(SAVED_URL, url);
        sharedPreferencesEditor.apply();
    }

    public void saveSwitchDrawerItemState(boolean b){
        sharedPreferences = getSharedPreferences(APP_SETTINGS,MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putBoolean(IS_SHOW_URL, b);
        sharedPreferencesEditor.apply();
    }

    @Override
    public void onSettingsChange(String s) {
        urlOfHomePage = s;
        saveUrl(s);
        fragmentWebView.setUrl(s);
        fragmentWebView.loadUrl(s);
    }

    private  void changeMenuVisible (boolean b){
            menu.setGroupVisible(groupId, b);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.add(groupId,action_refresh,1,R.string.action_refresh).setIcon(R.drawable.ico_refresh).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        menu.add(groupId,action_go_home,2,R.string.action_go_home).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case action_refresh:
                fragmentWebView.fragmentWebViewRefresh();
                Toast.makeText(getApplicationContext(),"Страница обновлена",Toast.LENGTH_SHORT).show();
            return true;
            case action_go_home:
                fragmentWebView.fragmentWebViewGoHome();
                Toast.makeText(getApplicationContext(),"Переход на главную страницу",Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed() {
        if (drawer != null && drawer.isDrawerOpen()){
            drawer.closeDrawer();
            return;
        }
        if(fragmentWebView!= null && fragmentWebView.fragmentWebViewCanGoBack()) {
            fragmentWebView.fragmentWebViewGoBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case PROGRESS_DIALOG:
                this.progressDialog = new ProgressDialog(this);
                return this.progressDialog;
            default:
                return null;
        }
    }


    public class AbrcDownloader extends FileDownloader{
        public void download(String abrcUrl) {
            Log.d("download mU=",abrcUrl );
            String abrcDirectory = Environment.getExternalStorageDirectory() + "/" + APP_DIRECTORY;
            this.execute(abrcUrl, abrcDirectory);
        }
        @Override
        protected String doInBackground(String... strings) {
            if (strings.length!=2){
                return null;
            }
            this.publishProgress(0);

            String modelUrl = strings[0];
            String directoryPath = strings[1];

            String[] urlPart = modelUrl.split("/");
            //TODO убрать этот костыль
            String temp=urlPart[urlPart.length-1].replace("[","_");


            int count;

            try{
                URL url = new URL(modelUrl);
                URLConnection connection = url.openConnection();
                connection.connect();

                InputStream inputStream = new BufferedInputStream(url.openStream());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);

                byte data[] = new byte[1024];

                while((count = inputStream.read(data)) != -1){
                   dataOutputStream.write(data,0,count);
                }

                String className=byteArrayOutputStream.toString();

                dataOutputStream.close();
                inputStream.close();

                return  className;

            }catch (Exception e) {
                Log.e("soInBackGround",e.toString());
            }

            return  null;
        }
        @Override
        protected void onPostExecute(String abrcPath) {
            if (abrcPath != null) {
               onAbrcDownloadFinish(abrcPath);
            }
        }
    }

    public class FragmentDownloader extends FileDownloader {

        public void download(String modelUrl) {

            Log.d("download mU=",modelUrl );
            String modelDirectory = Environment.getExternalStorageDirectory() + "/" + APP_DIRECTORY;
            this.execute(modelUrl, modelDirectory);
        }

        @Override
        protected void onPreExecute() {
            showDialog(PROGRESS_DIALOG);
            progressDialog.setMessage("Загрузка модели...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMax(100);

        }

        @Override
        protected void onProgressUpdate(final Integer... values) {
            progressDialog.setProgress(values[0]);
        }

        @Override
        protected void onPostExecute(String modelPath) {
            if (modelPath != null) {
                // Add the APK/JAR/ZIP file's classes to the class loader.
                ModelDexClassLoader.getInstance().install(MainActivity.this, modelPath);
                onModelDownloadFinish();
            }
            dismissDialog(PROGRESS_DIALOG);
        }

    }

    @Override
    public void onModelUrlFound(String modelUrl) {
        String str[]=modelUrl.split(".brc");
        modelUrl=str[0]+".abrc";
        modelUrl=modelUrl.replace("%5B","[");
        modelUrl=modelUrl.replace("%5D","]");
        this.modelUrl=modelUrl.replace(".abrc",".apk");
        Log.d("onModelUrlF mU=", modelUrl);
        new AbrcDownloader().download(modelUrl);
    }

    private void onAbrcDownloadFinish(String abrcPath){
        Log.d("abrc", abrcPath);
        Toast.makeText(this,abrcPath,Toast.LENGTH_LONG).show();
        className=abrcPath;

        new FragmentDownloader().download(modelUrl);

    }

    private void onModelDownloadFinish(){
        Intent intent = new Intent(MainActivity.this,TaskActivity.class);
        intent.putExtra(CLASS_NAME, className);
        intent.putExtra(URL_OF_HOME_PAGE, urlOfHomePage);
        startActivity(intent);
    }

}
