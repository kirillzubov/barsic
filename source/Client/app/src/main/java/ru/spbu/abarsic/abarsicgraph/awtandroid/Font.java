/*
 * Font.java
 */

package ru.spbu.abarsic.abarsicgraph.awtandroid;

/**
 * @author Martynyuk
 * */

import android.graphics.Paint;
import android.graphics.Typeface;

public class Font {

	Typeface font;
	float size;
	
	public static Typeface SERIF = Typeface.SERIF;
	public static Typeface SANS_SERIF = Typeface.SANS_SERIF;
	public static Typeface MONOSPACED = Typeface.MONOSPACE;
	public static Typeface DEFAULT = Typeface.DEFAULT;
	
	public static int PLAIN = Typeface.NORMAL;
	public static int BOLD = Typeface.BOLD;
	public static int ITALIC = Typeface.ITALIC;
	
	public Font(Typeface family, int style, int size) {
		super();
		this.font = Typeface.create(family, style);
		this.size = size;
	}
	
	private Font(Typeface family, float size) {
		super();
		this.font = Typeface.create(family, family.getStyle());
		this.size = size;
	}
	
	public void set(Paint p) {
		p.setTypeface(font);
		p.setTextSize(size);
	}
	
	public int getSize() {
		return (int)size;
	}
	
	public Typeface getFamily() {
		return font;
	}
	
	public static Font get(Paint p) {
		Typeface font = p.getTypeface();
		if (font != null)
		    return new Font(font, p.getTextSize());
		else
			return new Font(Typeface.DEFAULT, p.getTextSize());
	}
	
}
