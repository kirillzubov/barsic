package ru.spbu.barsic;

public interface IHyperLink extends IElement {
    void setText(String value);

    void setFont(String value);

    void setIsParentFont(boolean value);

    void setIsEnabledOnCalc(boolean value);
}
