package ru.spbu.abarsic.fragments;

import android.app.Fragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ru.spbu.abarsic.R;

/**
 * Created by Владислав on 29.07.2015.
 */
public class FragmentHelp extends Fragment {

    private static final String ARG_URL = "URL";
    private String url="";

    private static FragmentHelp instance = null;


    public static FragmentHelp getInstance(String url) {
        if (instance == null){
            FragmentHelp fragment = new FragmentHelp();
            Bundle args = new Bundle();
            args.putString(ARG_URL,url);
            fragment.setArguments(args);
            instance = fragment;
        }
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            url = getArguments().getString(ARG_URL);
        }
    }

    private WebView webView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help,container,false);
        webView = (WebView) view.findViewById(R.id.webViewHelp);

        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient());

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setInitialScale(1);

        if (url != ""){
            webView.loadUrl(url);
        }else{
            webView.loadUrl(getString(R.string.help_url));
        }

        return view;
    }
}
