package ru.spbu.abarsic;

import ru.spbu.barsic.IBrowser;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.AbsoluteLayout;
import android.webkit.WebView;
//import ru.barsic.model.FormsActivity;

/**
 * Created by Faze on 16.04.2015.
 */
public class Browser extends WebView implements IBrowser {

    /*public Browser(){
        this(FormsActivity.instance);
    }*/

    public Browser(final Context context) {
        super(context);
        WebSettings webSettings = getSettings();
        webSettings.setJavaScriptEnabled(true);
        getSettings().setBuiltInZoomControls(true);
        setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains(".brc")) {
                    url = url.replace(".brc", ".apk");
                    Uri source = Uri.parse(url);
                    // Make a new request pointing to the .apk url
                    DownloadManager.Request request = new DownloadManager.Request(source);
                    // appears the same in Notification bar while downloading
                    request.setDescription("Description");
                    request.setTitle("App.apk");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        request.allowScanningByMediaScanner();
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    }
                    // save the file in the "Downloads" folder of SDCARD
                    request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "downloads");
                    // get download service and enqueue file
                    DownloadManager manager = (DownloadManager) context.getSystemService(context.DOWNLOAD_SERVICE);
                    manager.enqueue(request);
                }
                else view.loadUrl(url);
                return true;
            }
        });
        setWebChromeClient(new WebChromeClient());
    }

    DisplayMetrics display = this.getResources().getDisplayMetrics();
    public int width=display.widthPixels, height=display.heightPixels, x=0, y=0;
    public void setUrl(String value){
        loadUrl(value);
    }

    public void reload(){
        loadUrl(getUrl());
    }

    @Override
    public void executeJavascript(String code) {

    }

    public void setName(String value){

    }

    @Override
    public void setLeftPos(int value) {

    }

    @Override
    public void setTopPos(int value) {

    }

    public void setX(int value){
        x=value;
        AbsoluteLayout.LayoutParams param = new Browser.LayoutParams(width, height, x, y);
        setLayoutParams(param);
    }
    public void setY(int value){
        y=value;
        AbsoluteLayout.LayoutParams param = new Browser.LayoutParams(width, height, x, y);
        setLayoutParams(param);
    }

    public void setWidth(int value){
        width=value;
        AbsoluteLayout.LayoutParams param = new Browser.LayoutParams(width, height, x, y);
        setLayoutParams(param);
    }

    @Override
    public void setHint(String value) {

    }

    @Override
    public void setIsEnabled(boolean value) {

    }

    @Override
    public void setIsVisible(boolean value) {

    }

    public void setHeight(int value){
        height=value;
        AbsoluteLayout.LayoutParams param = new Browser.LayoutParams(width, height, x, y);
        setLayoutParams(param);
    }

    public void setAlign(String value){

    }

    @Override
    public void setOnHelp(final Runnable eventHandler) {

    }

    public void setBackColor(String value){
        //no need
    }

    public void setAssociatedArray(String value){

    }

}
