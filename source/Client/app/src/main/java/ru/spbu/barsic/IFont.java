package ru.spbu.barsic;

public interface IFont {
    
    /*
    "name"
    "script"
    */
    
    public String color();
    
    public void setColor(String value);
    
    public String face();
    
    public void setFace(String value);
    
    public double height();
    
    public void setHeight(double value);
    
    public boolean isBold();
    
    public void setBold(boolean value);
    
    public boolean isItalic();
    
    public void setItalic(boolean value);
    
    public boolean isStrikeout();
    
    public void setStrikeout(boolean value);
    
    public boolean isUnderline();
    
    public void setUnderline(boolean value);
    
}
