package ru.spbu.abarsic.abarsicgraph.graph;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;

import static ru.spbu.abarsic.abarsicgraph.CartTransform.*;

/**
 * Created by Мартынюк on 17.04.2015.
 */
public final class Polygon extends GraphicPrimitive {

    double[] xyPoints; //odd elements - x, even elements - y
    Path path = new Path();

    public Polygon(double[] xyPoints, int fillColor, int strokeColor) {
        super(0.0, 0.0, fillColor, strokeColor); //TODO add determining of polygon center
        if (xyPoints.length % 2 != 0) {
            throw new IllegalArgumentException(String.format("Points array length must be even. Length=%d", xyPoints.length));
        }
        this.xyPoints = xyPoints;
    }

    @Override
    public void draw(Canvas c, Paint p, double[] coeffs) {
        path.moveTo((float)(coeffs[AX]*xyPoints[0]+coeffs[BX]), (float)(coeffs[AY]*xyPoints[1]+coeffs[BY]));
        for (int i=2; i<xyPoints.length; i+=2) {
            path.lineTo((float)(coeffs[AX]*xyPoints[i]+coeffs[BX]), (float)(coeffs[AY]*xyPoints[i+1]+coeffs[BY]));
        }
        path.lineTo((float)(coeffs[AX]*xyPoints[0]+coeffs[BX]), (float)(coeffs[AY]*xyPoints[1]+coeffs[BY]));

        setFillPaint(p);
        c.drawPath(path, p);
        setStrokePaint(p);
        c.drawPath(path, p);
    }
}
