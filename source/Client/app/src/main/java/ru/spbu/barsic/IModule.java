package ru.spbu.barsic;

public interface IModule {
    void onProgramStart();
}
