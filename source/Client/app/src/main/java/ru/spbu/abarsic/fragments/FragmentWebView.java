package ru.spbu.abarsic.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;



import ru.spbu.abarsic.R;


/**
 * Created by Владислав on 08.05.2015.
 */
public class FragmentWebView extends Fragment{

    private static final String ARG_URL = "URL";
    private static final String ARG_IS_SHOW_URL = "IS_SHOW_URL";

    private String url="";
    private boolean isShowUrl=false;

    private  WebView webView;
    private  TextView textViewURL;
    private Button button;


    private static FragmentWebView instance = null;

    /**
     *
     * @param url - адрес для первой загрузки
     * @param isShowUrl - показывать ли адрес
     * @return
     */

    public static FragmentWebView getInstance(String url, boolean isShowUrl){
       if (instance == null){
           FragmentWebView fragment = new FragmentWebView();
           Bundle args = new Bundle();
           args.putString(ARG_URL,url);
           args.putBoolean(ARG_IS_SHOW_URL,isShowUrl);
           fragment.setArguments(args);
           instance = fragment;
       }
       return instance;
    }

    public FragmentWebView(){

    }


    public void loadUrl(String s){
        webView.loadUrl(s);
    }

    public interface OnModelUrlFoundListener{
        public void onModelUrlFound(String modelUrl);
    }

    OnModelUrlFoundListener onModelUrlFoundListener;

    @Override
    public void onAttach(Activity activity) {
        try{
        onModelUrlFoundListener = (OnModelUrlFoundListener) activity;
    } catch (ClassCastException e) {
        throw new ClassCastException(activity.toString());
    }
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            url = getArguments().getString(ARG_URL);
            isShowUrl = getArguments().getBoolean(ARG_IS_SHOW_URL);
        }
    }

    //TODO:  Страницы ошибок;
    public class MainWebViewClient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            textViewURL.setText(webView.getUrl());
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if (url.contains(".brc")) {
               onModelUrlFoundListener.onModelUrlFound(url);
            }
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_webview,container,false);

        webView = (WebView) view.findViewById(R.id.webView);
        textViewURL = (TextView) view.findViewById(R.id.textViewBrowser);
        button = (Button) view.findViewById(R.id.buttonGo);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str = textViewURL.getText().toString();
                webView.loadUrl(str);
            }
        });


        MainWebViewClient mainWebViewClient = new MainWebViewClient();
        webView.setWebViewClient(mainWebViewClient);
        webView.setWebChromeClient(new WebChromeClient());

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSavePassword(true);
        webView.setInitialScale(1);


        if (url != ""){
            webView.loadUrl(url);
        }else{
            webView.loadUrl(getString(R.string.home_url_default));
        }
        if (isShowUrl){
            textViewURL.setVisibility(View.VISIBLE);
            button.setVisibility(View.VISIBLE);
        }else{
            textViewURL.setVisibility(View.GONE);
            button.setVisibility(View.GONE);
        }


        return view;
    }

    public void fragmentWebViewGoHome(){
        webView.loadUrl(url);
    }

    public void setUrl(String url){
        this.url = url;
    }

    public void onUrlStateChange (boolean b){

        if (b)
        {
            textViewURL.setVisibility(View.VISIBLE);
            button.setVisibility(View.VISIBLE);
        }else{
            textViewURL.setVisibility(View.GONE);
            button.setVisibility(View.GONE);}
    }

    public void fragmentWebViewRefresh(){
        webView.reload();
    }

    public boolean fragmentWebViewCanGoBack(){
        return webView.canGoBack();
    }

    public void fragmentWebViewGoBack(){
        webView.goBack();
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {

      super.onViewStateRestored(savedInstanceState);

        if ((url!="")){
            webView.loadUrl(url);
            webView.restoreState(savedInstanceState);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        webView.saveState(outState);
        url=webView.getUrl();
    }


}

