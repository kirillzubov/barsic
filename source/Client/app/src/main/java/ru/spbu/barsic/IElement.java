package ru.spbu.barsic;

public interface IElement {

    void setName(String value);

    void setLeftPos(int value); //renamed to avoid conflict with the "setLeft" method of the View class

    void setTopPos(int value); //renamed to avoid conflict with the "setTop" method of the View class

    void setWidth(int value);

    void setHeight(int value);

    void setHint(String value);

    void setIsEnabled(boolean value);

    void setIsVisible(boolean value);

    void setAlign(String value);

    void setOnHelp(final Runnable eventHandler);

}