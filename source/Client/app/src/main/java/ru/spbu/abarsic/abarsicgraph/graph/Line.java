package ru.spbu.abarsic.abarsicgraph.graph;

import android.graphics.Canvas;
import android.graphics.Paint;

import static ru.spbu.abarsic.abarsicgraph.CartTransform.*;

/**
 * Created by Мартынюк on 17.04.2015.
 */
public final class Line extends GraphicPrimitive {

    double x1, y1, x2, y2;

    public Line(double x1, double y1, double x2, double y2, int fillColor, int strokeColor) {
        super(0.5*(x1+x2), 0.5*(y1+y2), fillColor, strokeColor);
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    @Override
    public void draw(Canvas c, Paint p, double[] coeffs) {
        setStrokePaint(p);
        c.drawLine((float)(coeffs[AX]*x1+coeffs[BX]), (float)(coeffs[AY]*y1+coeffs[BY]),
                   (float)(coeffs[AX]*x2+coeffs[BX]), (float)(coeffs[AY]*y2+coeffs[BY]), p);
    }
}
