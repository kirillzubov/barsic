package ru.spbu.barsic;

public interface ITextField extends IElement {
    void setText(String value);

    void setIsSourceCode(boolean value);

    void setIsParentBackColor(boolean value);

    void setIsReadOnly(boolean value);

    void setIsParentFont(boolean value);

    void setAssociatedVariable(String value);

    void setIsEnabledOnCalc(boolean value);

    void setTextSize(int value);
}
