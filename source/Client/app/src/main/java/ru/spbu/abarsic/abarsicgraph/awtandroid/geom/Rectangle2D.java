/*
 * Rectangle2D.java
 */

package ru.spbu.abarsic.abarsicgraph.awtandroid.geom;

/**
 * @author Martynyuk
 * */

import android.graphics.Canvas;
import android.graphics.Paint;

public class Rectangle2D extends Figure2D {
	
	float xLeft;
	float xRight;
	float yTop;
	float yBottom;

	public Rectangle2D(float xLeft, float yTop, float width, float height) {
		super();
		this.xLeft = xLeft;
		this.yTop = yTop;
		this.xRight = xLeft+width;
		this.yBottom = yTop+height;
	}
	
	public Rectangle2D(double xLeft, double yTop, double width, double height) {
		super();
		this.xLeft = (float)xLeft;
		this.yTop = (float)yTop;
		this.xRight = (float)(xLeft+width);
		this.yBottom = (float)(yTop+height);
	}
	
	@Override
	public void draw(Canvas g, Paint p) {
		g.drawRect(xLeft, yTop, xRight, yBottom, p);
	}
	
	public float getMinX() {
		return xLeft;
	}
	
	public float getMaxX() {
		return xRight;
	}
	
	public float getMinY() {
		return yTop;
	}
	
	public float getMaxY() {
		return yBottom;
	}
	
	public float getWidth() {
		return xRight-xLeft;
	}

	public float getHeight() {
		return yBottom-yTop;
	}

	public void setRect(double xLeft, double yTop, double width, double height) {
		this.xLeft = (float)xLeft;
		this.yTop = (float)yTop;
		this.xRight = (float)(xLeft+width);
		this.yBottom = (float)(yTop+height);
	}
	
	/* taken from the file java.awt.geom.Rectangle2D.java */
    public Rectangle2D createUnion(Rectangle2D r) {
        float x1 = Math.min(this.getMinX(), r.getMinX());
        float y1 = Math.min(this.getMinY(), r.getMinY());
        float x2 = Math.max(this.getMaxX(), r.getMaxX());
        float y2 = Math.max(this.getMaxY(), r.getMaxY());
        Rectangle2D dest = new Rectangle2D(0, 0, 0, 0);
        dest.setFrameFromDiagonal(x1, y1, x2, y2);
        return dest;
    }
    
    /* taken from the file java.awt.geom.RectangularShape.java */
    private void setFrameFromDiagonal(float x1, float y1, float x2, float y2) {
        if (x2 < x1) {
            float t = x1;
            x1 = x2;
            x2 = t;
        }
        if (y2 < y1) {
            float t = y1;
            y1 = y2;
            y2 = t;
        }
        setRect(x1, y1, x2-x1, y2-y1);
    }
    
    
}
