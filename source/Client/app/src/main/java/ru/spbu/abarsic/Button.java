package ru.spbu.abarsic;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.*;
import ru.spbu.barsic.IButton;

/**
 * Created by nik on 18.04.2015.
 */
public class Button extends android.widget.Button implements IButton {

    /*public Button() {
        super(FormsActivity.instance);
    }*/

    public Button(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    //public int width=100, height=100, x=300, y=500;


    public void reload(){
        this.refreshDrawableState();
        //this.invalidate();
    }

    public void executeJavascript(String code){

    }

    public void setName(String value){

    }

    @Override
    public void setLeftPos(int value) {
        setLeft(value);
    }

    @Override
    public void setTopPos(int value) {
        setTop(value);
    }

    /*AbsoluteLayout.LayoutParams params =
            new AbsoluteLayout.LayoutParams(value, getHeight(), getLeft(), getTop());
    super.setLayoutParams(params);*/

    /*public void setX(int value){
        x=value;
        //AbsoluteLayout.LayoutParams layoutParams = (AbsoluteLayout.LayoutParams) Button.getLayoutParams();
        //layoutParams.x = 20;
        //Button.setLayoutParams(layoutParams);
        AbsoluteLayout.LayoutParams parampampam = new AbsoluteLayout.LayoutParams(width, height, x, y);
        //AbsoluteLayout.LayoutParams parampampam = (AbsoluteLayout.LayoutParams) Button.SCALE_X = 5;
                setLayoutParams(parampampam);

    }


    public void setY(int value){
        y=value;
        AbsoluteLayout.LayoutParams parampampam = new AbsoluteLayout.LayoutParams(width, height, x, y);
        setLayoutParams(parampampam);
    }*/

    @Override
    public void setWidth(int value) {
        //width=value;
        AbsoluteLayout.LayoutParams params =
                new AbsoluteLayout.LayoutParams(value, getHeight(), getLeft(), getTop());
        super.setLayoutParams(params);
        //super.setWidth(value);

    }


    public void setHeight(int value){
        //height=value;
        AbsoluteLayout.LayoutParams parampampam =
                new AbsoluteLayout.LayoutParams(getWidth(), value, getLeft(), getTop());
        super.setLayoutParams(parampampam);
    }

    @Override
    public void setHint(String value){
        int hintColor = android.R.color.holo_blue_dark;
        super.setHintTextColor(getResources().getColor(hintColor));
        super.setHint(value);
    }

    public void setIsEnabled(boolean value){
        setEnabled(value);
    }

    public void setIsVisible(boolean value){
        if (value==true) {
            this.setVisibility(this.VISIBLE);
        }
        else
            this.setVisibility(this.INVISIBLE);
    }


    public void setAlign(String value){
        switch(value) {
            case "left":
                AbsoluteLayout.LayoutParams parampampam1 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam1);
                break;
            case "right":
                AbsoluteLayout.LayoutParams parampampam2 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam2);
                break;
            case "top":
                AbsoluteLayout.LayoutParams parampampam3 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam3);
                break;
            case "bottom":
                AbsoluteLayout.LayoutParams parampampam4 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam4);
                break;
            default:
                AbsoluteLayout.LayoutParams parampampam5 =
                        new AbsoluteLayout.LayoutParams(getWidth(), getHeight(), getLeft(), getTop());
                super.setLayoutParams(parampampam5);
                break;
        }

    }

    @Override
    public void setOnHelp(Runnable eventHandler) {

    }

    public int textSize;
    @Override
    public void setTextSize(int value){
        textSize=value;
    }


    @Override
    public void setBackColor(String value){
        //int color = android.R.color.value;
        int color = android.R.color.holo_red_light;

        super.setBackgroundColor(getResources().getColor(color));
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void setTitle(String value) {
        this.setText(value);
    }

    @Override
    public void setText(String value) {
        super.setTextSize(textSize);
        super.setText(value);
    }

    @Override
    public void setIsDefaultOK(boolean value) {

    }

    @Override
    public void setIsParentFont(boolean value) {

    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {

    }

    @Override
    public void setFont(String value) {

    }

    @Override
    public void setOnClick(final Runnable eventHandler) {
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                eventHandler.run();
            }
        });
    }

}
