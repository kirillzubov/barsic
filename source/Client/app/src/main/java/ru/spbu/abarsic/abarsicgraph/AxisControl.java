/*
 * AxisControl.java
 */

package ru.spbu.abarsic.abarsicgraph;

/**
 * Porting to Android performed by Martynyuk
 */

import android.util.AttributeSet;
import android.view.View;

import ru.spbu.abarsic.abarsicgraph.plot.Margin;
import ru.spbu.abarsic.abarsicgraph.plot.Range;
import ru.spbu.abarsic.abarsicgraph.plot.axis.SciAxis;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Canvas;
import android.graphics.Paint;
import ru.spbu.abarsic.abarsicgraph.awtandroid.geom.Rectangle2D;
import ru.spbu.abarsic.abarsicgraph.awtandroid.geom.Point2D;
import ru.spbu.abarsic.abarsicgraph.awtandroid.geom.Line2D;

/**
 * Plot Control class
 */
public class AxisControl extends View {

    /**
     * 
     */
    Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
    
    public AxisControl(Context context, AttributeSet attrs) {
       super(context, attrs);
       InitializeComponent();
    }

 
    /**
     * 
     */
    public SciAxis axisX;


    /**
     * Margin of the plot field borders on the display that we want to display
     */
    private Margin margin = new Margin(75, 20, 25, 45);

    /**
     * 
     * @param m
     */
    public void setMargin(Margin m) {
        renderArea = new Rectangle2D(margin.getLeft(), margin.getTop(),
                getWidth() - margin.getLeft() - margin.getRight(), getHeight() - margin.getTop() - margin.getBottom());
    }

    /**
     * 
     * @return
     */
    public Margin getMargin() {
        return margin;
    }
    /**
     * default field of the values that we want to display
     */
    public final static Rectangle2D DEFAULT_DATA_AREA = new Rectangle2D(-10, -10, 20, 20);
    /**
     * 
     */
    public static final double MAX_SCALE_FACTOR = 5E7;
    /**
     * 
     */
    public static final double MIN_SCALE_FACTOR = 1E-7;
    /**
     * field of the values that we want to display
     */
    private Rectangle2D DataArea = new Rectangle2D(-10, -10, 20, 20);
    /**
     * field on the display that we want to display
     */
    private Rectangle2D renderArea = new Rectangle2D(-10, -10, 20, 20);

    private void InitializeComponent() {

        axisX = new SciAxis();

    }

    //debug
    /**
     * 
     * @param theMessage
     */
    public static void showException(String theMessage) {
        AxisControl.showException(theMessage);
        //javax.swing.JOptionPane.showMessageDialog((java.awt.Component) null, theMessage, "alert", javax.swing.JOptionPane.ERROR_MESSAGE);
    }

    @Override
    public void draw(Canvas g) {

     long time = System.currentTimeMillis();
        //System.out.println("timer started");
        super.draw(g);

        renderArea = new Rectangle2D(margin.getLeft(), margin.getTop(),
                getWidth() - margin.getLeft() - margin.getRight(), getHeight() - margin.getTop() - margin.getBottom());


        double x=400;
        double y=400;

        axisX.setWidth(350);
        axisX.setHeight(50);
        axisX.setTopLeft(new Point2D(x,y));
        axisX.setDataRange(new Range(DataArea.getMinX(), DataArea.getMaxX()));
       
        //System.out.println("time_0=" + (System.currentTimeMillis() - time));

        axisX.draw(g, p);

        p.setColor(Color.BLUE);
        new Line2D(x, 0, x, 1000).draw(g, p);
        new Line2D(0, y, 1000, y).draw(g, p);

        p.setColor(Color.RED);
        axisX.getBorders().draw(g, p);
        //System.out.println("time_2=" + (System.currentTimeMillis() - time));

    }

    //public Dimension getMinimumSize() {
    //    return new Dimension(350, 250);
    //}

    /**
     * 
     * @return
     */
    //public Dimension getPrefferedSize() {
    //    return new Dimension(350, 250);
    //}
}
