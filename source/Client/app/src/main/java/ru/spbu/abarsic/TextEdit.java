package ru.spbu.abarsic;

import android.content.Context;
import android.widget.*;

//import ru.barsic.model.FormsActivity;
import ru.spbu.barsic.ITextField;


/**
 * Created by nik on 18.04.2015.
 */
public class TextEdit extends EditText implements ITextField {

    /*public TextEdit(){
        //this(FormsActivity.instance);
    }*/

    public TextEdit(Context context) {
        super(context);
    }

    public int width=100, height=100, x=300, y=500;


    public void reload(){

    }

    public void executeJavascript(String code){

    }

    public void setName(String value){

    }

    @Override
    public void setLeftPos(int value) {
        setLeft(value);
    }

    @Override
    public void setTopPos(int value) {
        setTop(value);
    }

    public void setX(int value){
        x=value;
        //AbsoluteLayout.LayoutParams layoutParams = (AbsoluteLayout.LayoutParams) Button.getLayoutParams();
        //layoutParams.x = 20;
        //Button.setLayoutParams(layoutParams);
        AbsoluteLayout.LayoutParams parampampam = new AbsoluteLayout.LayoutParams(width, height, x, y);
        //AbsoluteLayout.LayoutParams parampampam = (AbsoluteLayout.LayoutParams) Button.SCALE_X = 5;
        setLayoutParams(parampampam);

    }

    public void setY(int value){
        y=value;
        AbsoluteLayout.LayoutParams parampampam = new AbsoluteLayout.LayoutParams(width, height, x, y);
        setLayoutParams(parampampam);
    }

    @Override
    public void setWidth(int value){
        AbsoluteLayout.LayoutParams params1 =
                new AbsoluteLayout.LayoutParams(value, getHeight(), getLeft(), getTop());
        super.setLayoutParams(params1);
    }


    public void setHeight(int value){
        AbsoluteLayout.LayoutParams parampampam =
                new AbsoluteLayout.LayoutParams(getWidth(), value, getLeft(), getTop());
        super.setLayoutParams(parampampam);
    }

    @Override
    public void setHint(String value){
        int hintColor = android.R.color.holo_blue_dark;
        super.setHintTextColor(getResources().getColor(hintColor));
        super.setHint(value);
    }

    public void setIsEnabled(boolean value){
        setEnabled(value);
    }

    public void setIsVisible(boolean value){
        if (value==true) {
            this.setVisibility(this.VISIBLE);
        }
        else
            this.setVisibility(this.INVISIBLE);
    }


    public void setAlign(String value){

    }

    @Override
    public void setOnHelp(final Runnable eventHandler) {

    }

    public int textSize1;

    @Override
    public void setTextSize(int value){
        textSize1=value;
    }

    public void setBackColor(String value){
        //int color = android.R.color.value;
        int color = android.R.color.holo_blue_light;
        setBackgroundColor(getResources().getColor(color));
    }

    @Override
    public void setText(String value) {
        super.setTextSize(textSize1);
        super.setText(value);
        int backgroundcolor = android.R.color.background_dark;
        int textcolor = android.R.color.background_light;
        super.setBackgroundColor(getResources().getColor(backgroundcolor));
        super.setTextColor(getResources().getColor(textcolor));

    }

    @Override
    public void setIsSourceCode(boolean value) {

    }

    @Override
    public void setIsParentBackColor(boolean value) {

    }

    @Override
    public void setIsReadOnly(boolean value) {

    }

    @Override
    public void setIsParentFont(boolean value) {

    }

    @Override
    public void setAssociatedVariable(String value) {

    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {

    }
}
