package ru.spbu.barsic;

public interface IBrush {
    
    public String color();
    
    public void setColor(String value);
    
    public String style();
    
    public void setStyle(String style);
    
}
