package ru.spbu.abarsic.abarsicgraph.graph;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;

import ru.spbu.abarsic.abarsicgraph.CartTransform;

/**
 * Created by Мартынюк on 09.02.2015.
 */
public final class GraphicGroup extends GraphicObject { //TODO complete the class

    private static final int DEF_CAPACITY = 128;

    ArrayList<GraphicPrimitive> primitives;

    CartTransform transform;

    public GraphicGroup() {
        super();
        primitives = new ArrayList<GraphicPrimitive>(DEF_CAPACITY);
    }

    public GraphicGroup(ArrayList<GraphicPrimitive> primitives) { //TODO add deep copying in the constructor
        this();
        for (GraphicPrimitive primitive:primitives) {
            //this.primitives.add();
        }
    }

    @Override
    public void draw(Canvas c, Paint p, double[] coeffs) {
        c.save();
        c.translate((float) x0, (float) y0);
        for (GraphicPrimitive primitive:primitives) {
            primitive.draw(c, p, coeffs);
        }
        c.restore();
    }

    public void addGraphicPrimitive(GraphicPrimitive primitive) {
        primitives.add(primitive);
    }

    public void removeGraphicPrimitive(int index) {
        primitives.remove(index);
        primitives.trimToSize();
    }

    public void removeGraphicPrimitive() {
        removeGraphicPrimitive(primitives.size()-1);
    }

}
