package ru.spbu.abarsic;

import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;

import java.util.ArrayList;

import android.widget.AbsoluteLayout;
//import android.widget.LinearLayout;
import android.widget.LinearLayout;
import ru.spbu.abarsic.abarsicgraph.Graph;
import ru.spbu.abarsic.abarsicgraph.Plot;
import ru.spbu.abarsic.abarsicgraph.Subwindow;
import ru.spbu.abarsic.abarsicgraph.SubwindowView;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import ru.spbu.barsic.IButton;
import ru.spbu.barsic.IElement;
import ru.spbu.barsic.ITextField;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Мартынюк on 30.09.2015.
 */
public abstract class Model extends Fragment implements IModel {

    private void addSubWindow(IElement subwin1)
    {
        /*LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        mRootLayout.setLayoutParams(layoutParams);

        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.gravity = Gravity.TOP;
        layoutParams.weight = 1;
        ((View)subwin1).setLayoutParams(layoutParams);
        mRootLayout.addView((View)subwin1);*/

        AbsoluteLayout.LayoutParams layoutParams = new AbsoluteLayout.LayoutParams
                (AbsoluteLayout.LayoutParams.MATCH_PARENT,AbsoluteLayout.LayoutParams.MATCH_PARENT,AbsoluteLayout.LayoutParams.MATCH_PARENT,AbsoluteLayout.LayoutParams.MATCH_PARENT);
        /*layoutParams.width = AbsoluteLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = AbsoluteLayout.LayoutParams.MATCH_PARENT;*/
        mRootLayout.setLayoutParams(layoutParams);

        ((View)subwin1).setLayoutParams(layoutParams);
        mRootLayout.addView((View)subwin1);

    }

    private void setAbsLayout(IElement view)
    {
        AbsoluteLayout absoluteLayout = new AbsoluteLayout(getActivity());
        mRootLayout.addView(absoluteLayout);
        absoluteLayout.addView((View) view);
    }


    //TODO SM determine width and height of fragment correctly

    protected ViewGroup mRootLayout;

    ArrayList<View> mViews = new ArrayList<View>(128);

    Subwindow mSubwindow = new Subwindow();
    Plot mPlot = new Plot();
    Graph mGraph = new Graph();

    boolean mFirstRun = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        createComponents();
        return mRootLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mFirstRun) {
            initializeComponents();
            mFirstRun = false;
        }
    }

    protected abstract void createComponents();

    protected abstract void initializeComponents();


    public void addElement(IElement view) {
        if(view instanceof IButton || view instanceof ITextField)
        {
            setAbsLayout(view);
        }
        if(view instanceof SubwindowView)
        {
            addSubWindow(view);
        }
        mViews.add((View)view);
        if (view instanceof SubwindowView) {
            ((SubwindowView) view).bindWithCommonObjects(mSubwindow, mPlot, mGraph);
        }
    }


    protected void addElements(View... views) {
        for (int i=0; i<views.length; i++) {
            this.mViews.add(views[i]);
            if (views[i] instanceof SubwindowView) {
                ((SubwindowView) views[i]).bindWithCommonObjects(mSubwindow, mPlot, mGraph);
            }
        }
    }

    public Subwindow subwindow() {
        return mSubwindow;
    }

    public Plot plot() {
        return mPlot;
    }

    public Graph graph() {
        return mGraph;
    }

    public long time() {
        return System.currentTimeMillis();
    }

    public void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {}
    }

    /*@Override
    public void onWindowFocusChanged(boolean hasFocus) {
        SubwindowView subwindow;
        if (hasFocus) {
            for (int i = 0; i < mViews.size(); i++) {
                if (mViews.get(i) instanceof SubwindowView) {
                    subwindow = (SubwindowView) mViews.get(i);
                    subwindow.setViewSizes(subwindow.getWidth(), subwindow.getHeight());
                }
            }
            if (mFirstRun) {
                initializeComponents();
                mFirstRun = false;
            }
        }
    }*/

    @Override
    public void onDestroy() {
        mViews.clear();
        super.onDestroy();
    }


    /**
     * Initialization section of model
     *
     *
     */


    public Map<String, Double> variables;

    public String modelName="";

    public boolean isRemote = true;

    public String serverUrlParam ="";
    public String serverUrlMain ="";



    // public String serverUrlParam ="http://distolymp.spbu.ru/phys/olymp/p-model/param/";
    // public String serverUrlMain ="http://distolymp.spbu.ru/";

    @Override
    public void setServerUrl(String serverUrl) {

        try {
            this.serverUrlMain =serverUrl;
            this.serverUrlParam = serverUrl + "p-model/param/";
            Log.d("setservURL=",this.serverUrlParam);
        }catch (Exception e){
            Log.e("setServerUrl",e.toString());
            return;
        }
        if (isRemote) {new AsyncParamLoad().execute(this.serverUrlParam);}
    }


    public abstract void paramVariableSet();
    //Inlineclass: loading params from server
    public class AsyncParamLoad extends AsyncTask<String, Void, String> {

        final int CONNECTION_OK=200;
        @Override
        protected String doInBackground(String... urls) {
            Log.d("Start Async", "Yes");

            InputStream inputStream = null;

            HttpURLConnection urlConnection = null;

            String s="0";

            try {
                Log.d("Try block Async","OK");

                URL url = new URL(urls[0]);

                urlConnection = (HttpURLConnection) url.openConnection();

                String urlString = url.toString();
                String tmp[]=urlString.split("/",4);
                String siteName=tmp[2];


                Log.d("Cookie siteName= ", siteName);
                CookieManager cookieManager = CookieManager.getInstance();
                String cookieValue = cookieManager.getCookie(siteName);
                Log.d("Cookie= ", cookieValue);

                urlConnection.setRequestProperty("Cookie",cookieValue);

            //    urlConnection.setRequestProperty("Cookie",getCookieValue(tmp[2]));

                urlConnection.setRequestMethod("GET");

                int statusCode = urlConnection.getResponseCode();

                Log.d("Try if block Async","OK");

                if (statusCode==CONNECTION_OK){
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                    s=readStream(inputStream);
                    Log.d("Try if block true Async","OK");
                }

                urlConnection.disconnect();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return s;
        }


        @Override
        protected void onPostExecute(String result) {

            parsParam(result);
            paramVariableSet();

            Log.d("Result= ", result);
        }

        private String readStream(InputStream is) {
            try {
                ByteArrayOutputStream bo = new ByteArrayOutputStream();
                int i = is.read();
                while(i != -1) {
                    bo.write(i);
                    i = is.read();
                }
                return bo.toString();
            } catch (IOException e) {
                return "";
            }
        }



        private void parsParam(String input){

            Document document=null;
            Elements var=null;
            int max=0;
            try {
                document = Jsoup.parse(input);
                var = document.select("td");
                Log.d("var= ",var.text());
                max = Integer.parseInt(var.last().text());
            }catch (NullPointerException e){
                Log.e("PARSParm doc.sel=",e.toString());
            }

            variables = new LinkedHashMap<String, Double>();

            int k=0,l=1;
            try {
                for (int i = 0; i <= max - 1; i++) {
                    variables.put(var.get(k).text(), Double.parseDouble(var.get(l).text()));
                    k+=2;
                    l+=2;
                }
            }catch (NumberFormatException e){
                Log.e("for", e.toString());
            }
//begin debug
//            StringBuilder stringBuilder = new StringBuilder();
//            for( Map.Entry<String, Double> entry : variables.entrySet() ) {
//                stringBuilder.append(entry.getKey());
//                stringBuilder.append("=");
//                stringBuilder.append(entry.getValue());
//                stringBuilder.append("\n");
//                Log.d("strb =",stringBuilder.toString());
//            }
//            Log.d("strbF =", stringBuilder.toString());
//end debug

        }

    }

//    private static String getCookieValue(String siteName){
//        String cookieValue = "";
//        Log.d("Cookie siteName= ", siteName);
//        CookieManager cookieManager = CookieManager.getInstance();
//        String cookies = cookieManager.getCookie(siteName);
//        int i=cookies.lastIndexOf("=");
//        cookieValue="PHPSESSID"+cookies.substring(i);
//
//        Log.d("Cookie= ", cookieValue);
//
//        return cookieValue;
//    }


}
