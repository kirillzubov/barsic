package ru.spbu.abarsic.abarsicgraph.graph;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Мартынюк on 09.02.2015.
 */
public abstract class GraphicObject {

    double x0; //coordinates of the center
    double y0;

    public abstract void draw(Canvas c, Paint p, double[] coeffs);

    protected GraphicObject(double xCenter, double yCenter) {
        super();
        this.x0 = xCenter;
        this.y0 = yCenter;
    }

    protected GraphicObject() {
        this(0.0, 0.0);
    }

    public void moveBy(double deltaX, double deltaY) {
        x0 += deltaX;
        y0 += deltaY;
    }

}
