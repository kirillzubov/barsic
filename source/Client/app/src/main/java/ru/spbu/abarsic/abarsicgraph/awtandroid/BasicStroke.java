/*
 * BasicStroke.java
 * */

package ru.spbu.abarsic.abarsicgraph.awtandroid;

/**
 * @author Martynyuk
 * */

import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;

public class BasicStroke {
	
	float width;
	Cap cap;
	Join join;
	float miterLimit;
	DashPathEffect pathEffect;
	
	public static int CAP_BUTT = 0;
	public static int CAP_ROUND = 1;
	public static int CAP_SQUARE = 2;
	private Cap[] caps = {Cap.BUTT, Cap.ROUND, Cap.SQUARE};
	
	public static int JOIN_MITER = 0;
	public static int JOIN_ROUND = 1;
	public static int JOIN_BEVEL = 2;
	private Join[] joins = {Join.MITER, Join.BEVEL, Join.ROUND};

	public BasicStroke(float width, int cap, int join, float miterLimit, float[] dash, float dashPhase) {
		super();
		this.width = width;
		this.cap = caps[cap];
		this.join = joins[join];
		this.miterLimit = miterLimit;
		this.pathEffect = new DashPathEffect(dash, dashPhase);
	}
	
	private BasicStroke(float width, Cap cap, Join join, float miterLimit, DashPathEffect pathEffect) {
		super();
		this.width = width;
		this.cap = cap;
		this.join = join;
		this.miterLimit = miterLimit;
		this.pathEffect = pathEffect;
	}
	
	public BasicStroke(float width, int cap, int join, float miterLimit) {
		super();
		this.width = width;
		this.cap = caps[cap];
		this.join = joins[join];
		this.miterLimit = miterLimit;
	}
	
	public BasicStroke(float width, int cap, int join) {
		this(width, cap, join, 10.0f);
	}
	
	public BasicStroke(float width) {
		this(width, CAP_SQUARE, JOIN_MITER);
	}
	
	public BasicStroke() {
		this(1.0f);
	}
	
	public void set(Paint p) {
		p.setStrokeWidth(width);
		p.setStrokeCap(cap);
		p.setStrokeJoin(join);
		p.setStrokeMiter(miterLimit);
		p.setPathEffect(pathEffect);
	}
	
	public static BasicStroke get(Paint p) {
		return new BasicStroke(p.getStrokeWidth(), p.getStrokeCap(), p.getStrokeJoin(),
				               p.getStrokeMiter(), (DashPathEffect)p.getPathEffect());
	}

}
