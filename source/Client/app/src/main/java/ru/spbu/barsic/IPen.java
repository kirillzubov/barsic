package ru.spbu.barsic;

public interface IPen {
    
    public String color();
    
    public void setColor(String value);
    
    public double width();
    
    public void setWidth(double value);
    
}
