package barsic;

import android.app.Activity;
import android.app.FragmentManager;
import ru.spbu.abarsic.*;
import ru.spbu.abarsic.abarsicgraph.SubwindowView;

public class Bootstrapper {
    private static Activity activity;
    private static FragmentManager fragmentManger;

    public static void setActivity(Activity activity)
    {
        Bootstrapper.activity = activity;
    }
    public static void setFragmentManger(FragmentManager fragmentManger)
    {
        Bootstrapper.fragmentManger = fragmentManger;
    }

    public static FragmentManager getFragmentManger()
    {
        return Bootstrapper.fragmentManger;
    }

    public static <T> T resolve(java.lang.Class<T> tClass) {
        if (tClass.getCanonicalName().contains("IForm")) {
            return (T) new Form();
        }
        if (tClass.getCanonicalName().contains("IButton")) {
            return (T) new Button(activity.getBaseContext(), null);
        }

        if (tClass.getCanonicalName().contains("ISubwindowView")) {
            return (T) new SubwindowView(activity.getBaseContext(), null);
        }

        if (tClass.getCanonicalName().contains("ITextArea")) {
            return (T) new TextArea();
        }

        if (tClass.getCanonicalName().contains("ITextField")) {
            return (T) new TextEdit(activity.getBaseContext());
        }

        if (tClass.getCanonicalName().contains("ITextEdit")) {
            return (T) new TextEdit(activity.getBaseContext());
        }

        return null;
    }
}
