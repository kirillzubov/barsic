package barsic.app;

import ru.spbu.barsic.IForm;
import ru.spbu.barsic.IModule;

/**
 * Created by Мартынюк on 12.10.2015.
 */
public class MainModule implements IModule {

    IForm form1;

    @Override
    public void onProgramStart() {
        form1 = new MainForm().createform1();
        form1.showForm();
    }
}
