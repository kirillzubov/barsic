package barsic.app;

import barsic.Bootstrapper;
import barsic.Form;
import ru.spbu.barsic.IButton;

/**
 * Created by Мартынюк on 12.10.2015.
 */
public class Form1 extends Form  {

    @Override
    protected void createComponents() {
        super.createComponents();
        IButton button1 = Bootstrapper.resolve(IButton.class);
        button1.setIsDefaultOK(false);
        this.addElement(button1);

        IButton button2 = Bootstrapper.resolve(IButton.class);
        button2.setIsDefaultOK(false);
        this.addElement(button2);
    }


    public static double createversion(){
        double version = 1185;

        return version;
    }


    public static String createstringVar(){
        String stringVar = "value";

        return stringVar;
    }


    public static Object createboolVar(){
        Object boolVar = true;

        return boolVar;
    }
}
