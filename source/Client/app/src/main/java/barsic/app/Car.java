package barsic.app;

import android.view.Gravity;
import android.widget.LinearLayout;

import java.util.Map;

import barsic.Bootstrapper;
import ru.spbu.abarsic.Button;
import ru.spbu.abarsic.Model;
import ru.spbu.abarsic.abarsicgraph.SubwindowView;
import ru.spbu.barsic.IButton;
import ru.spbu.barsic.ISubwindowView;

import static java.lang.Math.max;

/**
 * Created by Мартынюк on 12.10.2015.
 */
public class Car extends barsic.Form {

    ISubwindowView subwin1;
    IButton button1;
    IButton button2;

    private static final long FRAME_DURATION = 40L;

    double time;
    int iFrame = 0;
    double xCar = 0.0;
    double vCar = 10.0;
    public double[] pars = new double[100];

    @Override
    protected void createComponents() {
        super.createComponents();

        subwin1 = Bootstrapper.resolve(ISubwindowView.class);
        addElement(subwin1);

        button1 = Bootstrapper.resolve(IButton.class);
        button1.setOnClick(run);
        button1.setText("Start");
        addElement(button1);

        button1 = Bootstrapper.resolve(IButton.class);
        button1.setOnClick(run);
        button1.setText("Reset");
        addElement(button1);
    }

    @Override
    protected void initializeComponents() {
        subwin1.output(); {
            graph().setBounds(-5.0, 55.0,-30.0*(double)((SubwindowView)subwin1).height()/((SubwindowView)subwin1).width(), 30.0*(double)((SubwindowView)subwin1).height()/((SubwindowView)subwin1).width());
            drawBackground();
            drawRail();
            drawScale();
            drawCar();
            graph().update();
        } subwin1.endOutput();
    }

    @Override
    public void paramVariableSet() {
        int i = 0;
        for( Map.Entry<String, Double> entry : variables.entrySet() ) {
            pars[i] = entry.getValue();
            i++;
        }
        button2.setText(String.format("par1 = "+pars[0]));
    }

    void drawCar() {
        //car
        graph().brush().setColor("#00FF00");
        graph().pen().setColor("#000000");
        graph().polygon(new double[]{xCar - 2.5, 1.0, xCar + 2.5, 1.0, xCar + 2.5, 3.5, xCar + 1.5, 3.5,
                xCar + 1.5, 2.5, xCar + 1.0, 2.0, xCar - 1.0, 2.0, xCar - 1.5, 2.5, xCar - 1.5, 3.5, xCar - 2.5, 3.5});

        //car wheels
        graph().brush().setColor("#808080");
        graph().circle(xCar - 1.5, 0.5, 0.5);
        graph().circle(xCar + 1.5, 0.5, 0.5);

        //dye
        graph().brush().setColor("#FF0000");
        graph().pen().setColor("#FF0000");
        graph().polygon(new double[]{xCar + 1.5, 3.5, xCar + 1.5, 2.5, xCar + 1.0, 2.0, xCar - 1.0, 2.0, xCar - 1.5, 2.5, xCar - 1.5, 3.5});

        //arrow
        graph().brush().setColor("#FF0000");
        graph().pen().setColor("#FF0000");
        graph().polygon(new double[]{xCar + 0.1, 2, xCar - 0.1, 2, xCar - 0.1, 0.5, xCar + 0.2, 0.5, xCar + 0., 0.0, xCar - 0.2, 0.5, xCar + 0.1, 0.5});

    }

    void drawRail() {

        //rail
        graph().brush().setColor("#C0C0C0");
        graph().pen().setColor("#C0C0C0");
        graph().rectangle(25, -2.5, 56, 5.0);
        graph().rectangle(-3.5, -1.25, 2, 7.5);
        graph().rectangle(53.75, -1.25, 2, 7.5);
        graph().pen().setColor("#000000");
        graph().line(-2.5, 0.0, 52.75, 0.0);
        graph().line(-2.5, 0.0, -2.5, 2.5);
        graph().line(-2.5, 2.5, -4.5, 2.5);
        graph().line(-4.5, 2.5, -4.5, -5);
        graph().line(-4.5, -5, 54.75, -5);
        graph().line(54.75, -5, 54.75, 2.5);
        graph().line(54.75, 2.5, 52.75, 2.5);
        graph().line(52.75, 2.5, 52.75, 0);

    }

    void drawBackground() {
        //Back ground
        graph().brush().setColor("#00FFFF");
        graph().pen().setColor("#00FFFF");
        graph().rectangle(25.0, 0.0, 150.0, 50.0);
    }

    void drawScale() {
        //Small scale
        graph().pen().setColor("#000000");
        float i;
        for (i=0; i<=50; i= (float) (i+0.5)) {
            graph().line(i, 0, i, -1);
        }

        //Medium scale
        float k;
        for (k=5; k<=50; k+=10) {
            graph().line(k, 0, k, -1.5);
        }

        //Big scale
        float j;
        for (j=0; j<=50; j+=10) {
            graph().line(j, 0, j, -2);
        }
    }

    boolean running = false;
    boolean paused = true;
    long frameStartTime;

    Runnable run = new Runnable() {

        @Override
        public void run() {
            if (!running) {
                running = true;
                paused = false;
                iFrame = 0;
                long sleepTime;
                button1.setText("Pause");
                subwin1.output();
                {
                    while ((xCar < 50.0) && (running)) {
                        if (!paused) {
                            frameStartTime = time();
                            time = (double) (iFrame) * FRAME_DURATION / 1000.0;
                            xCar = vCar * time;
                            subwindow().clear();
                            drawBackground();
                            drawRail();
                            drawScale();
                            drawCar();
                            graph().update();
                            iFrame++;
                            sleepTime = Math.min(0, Math.max(FRAME_DURATION, frameStartTime - time()) + FRAME_DURATION);
                            sleep(sleepTime);
                        } else {
                            sleep(FRAME_DURATION);
                        }
                    }

                    iFrame = 0;
                    running = false;
                    paused = true;
                    button1.setText("Start");

                }
                subwin1.endOutput();
            } else {
                if (!paused) {
                    paused = true;
                    button1.setText("Start");
                } else {
                    paused = false;
                    button1.setText("Pause");
                }
            }
        }
    };

    Runnable reset = new Runnable() {
        @Override
        public void run() {
            running = false;
            subwin1.output(); {
                xCar = 0.0;
                subwindow().clear();
                drawBackground();
                drawRail();
                drawScale();
                drawCar();
                graph().update();
            } subwin1.endOutput();
            button1.setText("Start");
        }
    };

}
