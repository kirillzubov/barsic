package barsic.app;

import barsic.*;

import ru.spbu.abarsic.Button;
import ru.spbu.barsic.*;
public class MainForm{

    public double createversion(){
        double version = 1185;

        return version;
    }


    public String createstringVar(){
        String stringVar = "value";

        return stringVar;
    }


    public Object createboolVar(){
        Object boolVar = true;

        return boolVar;
    }


    public IForm createform1(){
        return new MainForm.form1();
    }

    public class form1 extends Form {
        @Override
        protected void createComponents() {
            super.createComponents();
            IForm form1 = this;
            ISubwindowView subwin1;
            form1.setTitle("Components task");

            IButton button1 = Bootstrapper.resolve(IButton.class);
            button1.setIsDefaultOK(false);
            button1.setIsEnabledOnCalc(true);
            button1.setIsVisible(true);
            button1.setIsParentFont(true);
            button1.setTopPos(10);
            button1.setLeftPos(50);
            button1.setHint("1");
            button1.setIsEnabled(true);
            button1.setBackColor("red");
            button1.setName("button1");
            button1.setWidth(10);
            button1.setHeight(130);
            button1.setTextSize(50);
            button1.setText("push");
            button1.reload();
            form1.addElement(button1);

            ITextField textEdit1 = Bootstrapper.resolve(ITextField.class);
            textEdit1.setTopPos(50);
            textEdit1.setLeftPos(60);
            textEdit1.setHint("text");
            textEdit1.setIsEnabled(true);
            textEdit1.setName("textEdit1");
            //textEdit1.setBackColor("blue");
            textEdit1.setWidth(10);
            textEdit1.setHeight(100);
            textEdit1.setTextSize(30);
            textEdit1.setText("");
            textEdit1.setIsVisible(true);
            form1.addElement(textEdit1);

        }

    }


}



