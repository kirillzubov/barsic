package barsic;

import android.app.ActionBar;
import android.view.Gravity;
import android.widget.LinearLayout;
import ru.spbu.abarsic.Button;
import ru.spbu.abarsic.Model;
import ru.spbu.abarsic.R;
import ru.spbu.barsic.IButton;
import ru.spbu.barsic.IElement;
import ru.spbu.barsic.IForm;
import ru.spbu.barsic.ISubwindow;

public class Form extends Model implements IForm {

    public Form()
    {
        Bootstrapper.setActivity(getActivity());
    }

    @Override
    protected void createComponents() {
        LinearLayout.LayoutParams layoutParams;
        Bootstrapper.setActivity(getActivity());
        mRootLayout = new LinearLayout(getActivity());
        ((LinearLayout)mRootLayout).setOrientation(LinearLayout.VERTICAL);

        layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT;
        layoutParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
        mRootLayout.setLayoutParams(layoutParams);


    }

    @Override
    protected void initializeComponents() {

    }

    @Override
    public void paramVariableSet() {

    }

    @Override
    public void showForm() {
        Bootstrapper.getFragmentManger()
                .beginTransaction()
                .add(R.id.taskactivity_fragment, this)
                .commit();
    }

    @Override
    public void setTitle(String value) {

    }

    @Override
    public void setName(String value) {

    }

    @Override
    public void setLeftPos(int value) {

    }

    @Override
    public void setTopPos(int value) {

    }

    @Override
    public void setWidth(int value) {

    }

    @Override
    public void setHeight(int value) {

    }

    @Override
    public void setLeft(int value) {

    }

    @Override
    public void setTop(int value) {

    }

    @Override
    public void setClientWidth(int value) {

    }

    @Override
    public void setHint(String value) {

    }

    @Override
    public void setIsEnabled(boolean value) {

    }

    @Override
    public void setIsVisible(boolean value) {

    }

    @Override
    public void setAlign(String value) {

    }

    @Override
    public void setOnHelp(Runnable eventHandler) {

    }

    @Override
    public void setClientHeight(int value) {

    }

    @Override
    public void setBackColor(String value) {

    }

    @Override
    public void setIsResizeable(Boolean value) {

    }

    @Override
    public void setFont(String value) {

    }

    @Override
    public void setIsBorderCloseIcon(boolean value) {

    }

    @Override
    public void setIsEnabledOnCalc(boolean value) {

    }

    @Override
    public ISubwindow getElementById(String id) {
        return null;
    }
}
