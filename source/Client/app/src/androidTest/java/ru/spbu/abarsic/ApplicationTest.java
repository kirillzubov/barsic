package ru.spbu.abarsic;

import android.app.Application;
import android.test.ApplicationTestCase;

import ru.spbu.abarsic.activities.MainActivity;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {

        super(Application.class);
        MainActivity mainActivity = new MainActivity();
        mainActivity.onModelUrlFound("http://distolymp.spbu.ru/phys/olymp/data/models/rel_va.%5B325985703%5D.brc?ca940e8ee92e26b5410371e991875201");
    }
}